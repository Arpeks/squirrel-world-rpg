LinkLuaModifier( "modifier_boss_golem_rock_throw", "bosses/boss_golems/boss_golem_rock_throw.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if boss_golem_rock_throw == nil then
	boss_golem_rock_throw = class({})
end
function boss_golem_rock_throw:GetIntrinsicModifierName()
	return "modifier_boss_golem_rock_throw"
end
---------------------------------------------------------------------
--Modifiers
if modifier_boss_golem_rock_throw == nil then
	modifier_boss_golem_rock_throw = class({})
end
function modifier_boss_golem_rock_throw:OnCreated(params)
	if IsServer() then
	end
end
function modifier_boss_golem_rock_throw:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_boss_golem_rock_throw:OnDestroy()
	if IsServer() then
	end
end
function modifier_boss_golem_rock_throw:DeclareFunctions()
	return {
	}
end