modifier_projectile_effect_43 = class({})
--Classifications template
function modifier_projectile_effect_43:IsHidden()
    return true
end

function modifier_projectile_effect_43:IsDebuff()
    return false
end

function modifier_projectile_effect_43:IsPurgable()
    return false
end

function modifier_projectile_effect_43:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_43:IsStunDebuff()
    return false
end

function modifier_projectile_effect_43:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_43:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_43:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_43:GetModifierProjectileName()
    return "particles/winter_fx/winter_present_projectile.vpcf"
end

function modifier_projectile_effect_43:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end