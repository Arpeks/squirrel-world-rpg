modifier_projectile_effect_17 = class({})
--Classifications template
function modifier_projectile_effect_17:IsHidden()
    return true
end

function modifier_projectile_effect_17:IsDebuff()
    return false
end

function modifier_projectile_effect_17:IsPurgable()
    return false
end

function modifier_projectile_effect_17:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_17:IsStunDebuff()
    return false
end

function modifier_projectile_effect_17:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_17:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_17:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_17:GetModifierProjectileName()
    return "particles/units/heroes/hero_alchemist/alchemist_berserk_potion_projectile.vpcf"
end

function modifier_projectile_effect_17:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end