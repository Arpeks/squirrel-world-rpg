modifier_projectile_effect_13 = class({})
--Classifications template
function modifier_projectile_effect_13:IsHidden()
    return true
end

function modifier_projectile_effect_13:IsDebuff()
    return false
end

function modifier_projectile_effect_13:IsPurgable()
    return false
end

function modifier_projectile_effect_13:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_13:IsStunDebuff()
    return false
end

function modifier_projectile_effect_13:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_13:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_13:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_13:GetModifierProjectileName()
    return "particles/econ/events/ti9/ti9_monkey_projectile.vpcf"
end

function modifier_projectile_effect_13:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end