modifier_following_effect_24 = class({})

function modifier_following_effect_24:IsHidden()
	return true
end

function modifier_following_effect_24:IsPurgable()
	return false
end

function modifier_following_effect_24:IsPermanent()
	return true
end

function modifier_following_effect_24:OnCreated( kv )
	print("on created")
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/chc/collection/special/game_breaker.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	-- self:StartIntervalThink(FrameTime())
	
end

function modifier_following_effect_24:OnIntervalThink()
	position = self:GetParent():GetAbsOrigin()
	position.z = position.z + 1500
	position.y = position.y + 300
	ParticleManager:SetParticleControl(self.particleLeader, 1, Vector(position.x, position.y, position.z))
end
function modifier_following_effect_24:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
