modifier_following_effect_17 = class({})

function modifier_following_effect_17:IsHidden()
	return true
end

function modifier_following_effect_17:IsPurgable()
	return false
end

function modifier_following_effect_17:IsPermanent()
	return true
end

function modifier_following_effect_17:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/necrolyte/necro_ti9_immortal/necro_ti9_immortal_shroud_model.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
end

function modifier_following_effect_17:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
