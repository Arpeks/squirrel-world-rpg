modifier_projectile_effect_28 = class({})
--Classifications template
function modifier_projectile_effect_28:IsHidden()
    return true
end

function modifier_projectile_effect_28:IsDebuff()
    return false
end

function modifier_projectile_effect_28:IsPurgable()
    return false
end

function modifier_projectile_effect_28:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_28:IsStunDebuff()
    return false
end

function modifier_projectile_effect_28:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_28:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_28:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_28:GetModifierProjectileName()
    return "particles/econ/items/skywrath_mage/skywrath_arcana/skywrath_arcana_rod_of_atos_projectile_v2.vpcf"
end

function modifier_projectile_effect_28:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end