modifier_projectile_effect_29 = class({})
--Classifications template
function modifier_projectile_effect_29:IsHidden()
    return true
end

function modifier_projectile_effect_29:IsDebuff()
    return false
end

function modifier_projectile_effect_29:IsPurgable()
    return false
end

function modifier_projectile_effect_29:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_29:IsStunDebuff()
    return false
end

function modifier_projectile_effect_29:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_29:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_29:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_29:GetModifierProjectileName()
    return "particles/econ/items/queen_of_pain/qop_2022_immortal/queen_2022_scream_of_pain_projectile_blue.vpcf"
end

function modifier_projectile_effect_29:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end