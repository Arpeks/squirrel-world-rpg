modifier_following_effect_13 = class({})

function modifier_following_effect_13:IsHidden()
	return true
end

function modifier_following_effect_13:IsPurgable()
	return false
end

function modifier_following_effect_13:IsPermanent()
	return true
end

function modifier_following_effect_13:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_bounty_first.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
end

function modifier_following_effect_13:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
