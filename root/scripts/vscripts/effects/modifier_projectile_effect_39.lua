modifier_projectile_effect_39 = class({})
--Classifications template
function modifier_projectile_effect_39:IsHidden()
    return true
end

function modifier_projectile_effect_39:IsDebuff()
    return false
end

function modifier_projectile_effect_39:IsPurgable()
    return false
end

function modifier_projectile_effect_39:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_39:IsStunDebuff()
    return false
end

function modifier_projectile_effect_39:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_39:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_39:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_39:GetModifierProjectileName()
    return "particles/econ/items/snapfire/snapfire_fall20_immortal/snapfire_fall20_immortal_lil_projectile.vpcf"
end

function modifier_projectile_effect_39:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end