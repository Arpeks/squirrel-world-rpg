modifier_projectile_effect_10 = class({})
--Classifications template
function modifier_projectile_effect_10:IsHidden()
    return true
end

function modifier_projectile_effect_10:IsDebuff()
    return false
end

function modifier_projectile_effect_10:IsPurgable()
    return false
end

function modifier_projectile_effect_10:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_10:IsStunDebuff()
    return false
end

function modifier_projectile_effect_10:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_10:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_10:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_10:GetModifierProjectileName()
    return "particles/world_tower/tower_upgrade/ti7_radiant_tower_proj.vpcf"
end

function modifier_projectile_effect_10:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end