modifier_following_effect_34 = class({})

function modifier_following_effect_34:IsHidden()
	return true
end

function modifier_following_effect_34:IsPurgable()
	return false
end

function modifier_following_effect_34:IsPermanent()
	return true
end

function modifier_following_effect_34:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/omniknight/omniknight_fall20_immortal/omniknight_fall20_immortal_degen_aura_debuff.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_34:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader) 
end
