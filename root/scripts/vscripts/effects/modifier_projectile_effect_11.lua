modifier_projectile_effect_11 = class({})
--Classifications template
function modifier_projectile_effect_11:IsHidden()
    return true
end

function modifier_projectile_effect_11:IsDebuff()
    return false
end

function modifier_projectile_effect_11:IsPurgable()
    return false
end

function modifier_projectile_effect_11:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_11:IsStunDebuff()
    return false
end

function modifier_projectile_effect_11:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_11:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_11:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_11:GetModifierProjectileName()
    return "particles/econ/events/ti9/rock_golem_tower/radiant_tower_attack.vpcf"
end

function modifier_projectile_effect_11:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end