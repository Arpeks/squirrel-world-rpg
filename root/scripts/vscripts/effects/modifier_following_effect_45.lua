modifier_following_effect_45 = class({})

function modifier_following_effect_45:IsHidden()
	return true
end

function modifier_following_effect_45:IsPurgable()
	return false
end

function modifier_following_effect_45:IsPermanent()
	return true
end

function modifier_following_effect_45:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/units/heroes/hero_morphling/morphling_ambient_new.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_45:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
