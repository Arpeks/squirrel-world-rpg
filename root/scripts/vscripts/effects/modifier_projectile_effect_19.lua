modifier_projectile_effect_19 = class({})
--Classifications template
function modifier_projectile_effect_19:IsHidden()
    return true
end

function modifier_projectile_effect_19:IsDebuff()
    return false
end

function modifier_projectile_effect_19:IsPurgable()
    return false
end

function modifier_projectile_effect_19:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_19:IsStunDebuff()
    return false
end

function modifier_projectile_effect_19:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_19:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_19:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_19:GetModifierProjectileName()
    return "particles/units/heroes/hero_medusa/medusa_mystic_snake_projectile_return.vpcf"
end

function modifier_projectile_effect_19:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end