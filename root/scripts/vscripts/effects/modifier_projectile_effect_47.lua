modifier_projectile_effect_47 = class({})
--Classifications template
function modifier_projectile_effect_47:IsHidden()
    return true
end

function modifier_projectile_effect_47:IsDebuff()
    return false
end

function modifier_projectile_effect_47:IsPurgable()
    return false
end

function modifier_projectile_effect_47:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_47:IsStunDebuff()
    return false
end

function modifier_projectile_effect_47:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_47:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_47:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_47:GetModifierProjectileName()
    return "particles/econ/items/rubick/rubick_staff_wandering/rubick_base_attack_whset.vpcf"
end

function modifier_projectile_effect_47:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end