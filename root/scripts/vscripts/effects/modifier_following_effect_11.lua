modifier_following_effect_11 = class({})

function modifier_following_effect_11:IsHidden()
	return true
end

function modifier_following_effect_11:IsPurgable()
	return false
end

function modifier_following_effect_11:IsPermanent()
	return true
end

function modifier_following_effect_11:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/events/fall_major_2016/radiant_fountain_regen_fm06_lvl3.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
end

function modifier_following_effect_11:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
