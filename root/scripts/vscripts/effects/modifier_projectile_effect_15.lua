modifier_projectile_effect_15 = class({})
--Classifications template
function modifier_projectile_effect_15:IsHidden()
    return true
end

function modifier_projectile_effect_15:IsDebuff()
    return false
end

function modifier_projectile_effect_15:IsPurgable()
    return false
end

function modifier_projectile_effect_15:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_15:IsStunDebuff()
    return false
end

function modifier_projectile_effect_15:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_15:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_15:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_15:GetModifierProjectileName()
    return "particles/act_2/winters_curse_projectile.vpcf"
end

function modifier_projectile_effect_15:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end