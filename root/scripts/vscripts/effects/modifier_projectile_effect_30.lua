modifier_projectile_effect_30 = class({})
--Classifications template
function modifier_projectile_effect_30:IsHidden()
    return true
end

function modifier_projectile_effect_30:IsDebuff()
    return false
end

function modifier_projectile_effect_30:IsPurgable()
    return false
end

function modifier_projectile_effect_30:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_30:IsStunDebuff()
    return false
end

function modifier_projectile_effect_30:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_30:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_30:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_30:GetModifierProjectileName()
    return "particles/econ/items/vengeful/vs_ti8_immortal_shoulder/vs_ti8_immortal_magic_missle.vpcf"
end

function modifier_projectile_effect_30:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end