modifier_projectile_effect_20 = class({})
--Classifications template
function modifier_projectile_effect_20:IsHidden()
    return true
end

function modifier_projectile_effect_20:IsDebuff()
    return false
end

function modifier_projectile_effect_20:IsPurgable()
    return false
end

function modifier_projectile_effect_20:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_20:IsStunDebuff()
    return false
end

function modifier_projectile_effect_20:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_20:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_20:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_20:GetModifierProjectileName()
    return "particles/units/heroes/hero_invoker_kid/invoker_kid_forged_spirit_projectile.vpcf"
end

function modifier_projectile_effect_20:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end