modifier_following_effect_32 = class({})

function modifier_following_effect_32:IsHidden()
	return true
end

function modifier_following_effect_32:IsPurgable()
	return false
end

function modifier_following_effect_32:IsPermanent()
	return true
end

function modifier_following_effect_32:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/dark_willow/dark_willow_ti8_immortal_head/dw_crimson_ti8_immortal_cursed_crownenergy.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	
end

function modifier_following_effect_32:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
