modifier_projectile_effect_25 = class({})
--Classifications template
function modifier_projectile_effect_25:IsHidden()
    return true
end

function modifier_projectile_effect_25:IsDebuff()
    return false
end

function modifier_projectile_effect_25:IsPurgable()
    return false
end

function modifier_projectile_effect_25:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_25:IsStunDebuff()
    return false
end

function modifier_projectile_effect_25:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_25:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_25:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_25:GetModifierProjectileName()
    return "particles/econ/items/dragon_knight/dk_2022_immortal/dk_2022_immortal_dragon_tail_dragon_projectile.vpcf"
end

function modifier_projectile_effect_25:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end