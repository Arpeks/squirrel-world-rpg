modifier_following_effect_41 = class({})

function modifier_following_effect_41:IsHidden()
	return true
end

function modifier_following_effect_41:IsPurgable()
	return false
end

function modifier_following_effect_41:IsPermanent()
	return true
end

function modifier_following_effect_41:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/chc/creature/ritual_caster.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_41:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
