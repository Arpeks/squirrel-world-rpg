modifier_following_effect_37 = class({})

function modifier_following_effect_37:IsHidden()
	return true
end

function modifier_following_effect_37:IsPurgable()
	return false
end

function modifier_following_effect_37:IsPermanent()
	return true
end

function modifier_following_effect_37:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/act_2/ice_boss_channel_ground.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_37:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
