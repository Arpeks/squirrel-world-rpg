modifier_projectile_effect_22 = class({})
--Classifications template
function modifier_projectile_effect_22:IsHidden()
    return true
end

function modifier_projectile_effect_22:IsDebuff()
    return false
end

function modifier_projectile_effect_22:IsPurgable()
    return false
end

function modifier_projectile_effect_22:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_22:IsStunDebuff()
    return false
end

function modifier_projectile_effect_22:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_22:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_22:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_22:GetModifierProjectileName()
    return "particles/hw_fx/greevil_orange_lava_projectile.vpcf"
end

function modifier_projectile_effect_22:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end