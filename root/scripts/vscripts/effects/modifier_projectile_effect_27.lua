modifier_projectile_effect_27 = class({})
--Classifications template
function modifier_projectile_effect_27:IsHidden()
    return true
end

function modifier_projectile_effect_27:IsDebuff()
    return false
end

function modifier_projectile_effect_27:IsPurgable()
    return false
end

function modifier_projectile_effect_27:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_27:IsStunDebuff()
    return false
end

function modifier_projectile_effect_27:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_27:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_27:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_27:GetModifierProjectileName()
    return "particles/econ/items/dark_willow/dark_willow_immortal_2021/dw_2021_willow_wisp_bedlam_projectile.vpcf"
end

function modifier_projectile_effect_27:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end