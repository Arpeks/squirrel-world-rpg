modifier_projectile_effect_33 = class({})
--Classifications template
function modifier_projectile_effect_33:IsHidden()
    return true
end

function modifier_projectile_effect_33:IsDebuff()
    return false
end

function modifier_projectile_effect_33:IsPurgable()
    return false
end

function modifier_projectile_effect_33:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_33:IsStunDebuff()
    return false
end

function modifier_projectile_effect_33:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_33:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_33:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_33:GetModifierProjectileName()
    return "particles/units/heroes/hero_phantom_lancer/phantomlancer_spiritlance_projectile.vpcf"
end

function modifier_projectile_effect_33:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end