modifier_following_effect_44 = class({})

function modifier_following_effect_44:IsHidden()
	return true
end

function modifier_following_effect_44:IsPurgable()
	return false
end

function modifier_following_effect_44:IsPermanent()
	return true
end

function modifier_following_effect_44:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/units/heroes/hero_medusa/medusa_mana_shield_buff.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_44:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
