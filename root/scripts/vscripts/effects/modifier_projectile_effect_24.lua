modifier_projectile_effect_24 = class({})
--Classifications template
function modifier_projectile_effect_24:IsHidden()
    return true
end

function modifier_projectile_effect_24:IsDebuff()
    return false
end

function modifier_projectile_effect_24:IsPurgable()
    return false
end

function modifier_projectile_effect_24:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_24:IsStunDebuff()
    return false
end

function modifier_projectile_effect_24:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_24:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_24:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_24:GetModifierProjectileName()
    return "particles/items3_fx/gleipnir_projectile.vpcf"
end

function modifier_projectile_effect_24:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end