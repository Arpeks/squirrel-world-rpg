modifier_following_effect_33 = class({})

function modifier_following_effect_33:IsHidden()
	return true
end

function modifier_following_effect_33:IsPurgable()
	return false
end

function modifier_following_effect_33:IsPermanent()
	return true
end

function modifier_following_effect_33:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/espirit_ti6_rollingboulder.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	
end

function modifier_following_effect_33:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
