modifier_projectile_effect_38 = class({})
--Classifications template
function modifier_projectile_effect_38:IsHidden()
    return true
end

function modifier_projectile_effect_38:IsDebuff()
    return false
end

function modifier_projectile_effect_38:IsPurgable()
    return false
end

function modifier_projectile_effect_38:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_38:IsStunDebuff()
    return false
end

function modifier_projectile_effect_38:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_38:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_38:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_38:GetModifierProjectileName()
    return "particles/units/heroes/hero_invoker_kid/invoker_kid_forged_spirit_projectile.vpcf"
end

function modifier_projectile_effect_38:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end