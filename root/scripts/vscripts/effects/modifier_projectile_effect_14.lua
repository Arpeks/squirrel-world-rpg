modifier_projectile_effect_14 = class({})
--Classifications template
function modifier_projectile_effect_14:IsHidden()
    return true
end

function modifier_projectile_effect_14:IsDebuff()
    return false
end

function modifier_projectile_effect_14:IsPurgable()
    return false
end

function modifier_projectile_effect_14:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_14:IsStunDebuff()
    return false
end

function modifier_projectile_effect_14:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_14:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_14:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_14:GetModifierProjectileName()
    return "particles/econ/events/snowball/snowball_projectile.vpcf"
end

function modifier_projectile_effect_14:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end