modifier_projectile_effect_12 = class({})
--Classifications template
function modifier_projectile_effect_12:IsHidden()
    return true
end

function modifier_projectile_effect_12:IsDebuff()
    return false
end

function modifier_projectile_effect_12:IsPurgable()
    return false
end

function modifier_projectile_effect_12:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_12:IsStunDebuff()
    return false
end

function modifier_projectile_effect_12:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_12:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_12:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_12:GetModifierProjectileName()
    return "particles/econ/items/puck/puck_alliance_set/puck_base_attack_aproset.vpcf"
end

function modifier_projectile_effect_12:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end