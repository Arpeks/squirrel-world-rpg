modifier_following_effect_40 = class({})

function modifier_following_effect_40:IsHidden()
	return true
end

function modifier_following_effect_40:IsPurgable()
	return false
end

function modifier_following_effect_40:IsPermanent()
	return true
end

function modifier_following_effect_40:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/chc/creature/acid_aura_sigil.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_40:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
