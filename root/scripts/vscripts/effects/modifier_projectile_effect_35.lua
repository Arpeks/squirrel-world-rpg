modifier_projectile_effect_35 = class({})
--Classifications template
function modifier_projectile_effect_35:IsHidden()
    return true
end

function modifier_projectile_effect_35:IsDebuff()
    return false
end

function modifier_projectile_effect_35:IsPurgable()
    return false
end

function modifier_projectile_effect_35:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_35:IsStunDebuff()
    return false
end

function modifier_projectile_effect_35:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_35:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_35:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_35:GetModifierProjectileName()
    return "particles/econ/items/bristleback/ti7_head_nasal_goo/bristleback_ti7_crimson_nasal_goo_proj.vpcf"
end

function modifier_projectile_effect_35:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end