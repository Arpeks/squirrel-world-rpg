modifier_projectile_effect_40 = class({})
--Classifications template
function modifier_projectile_effect_40:IsHidden()
    return true
end

function modifier_projectile_effect_40:IsDebuff()
    return false
end

function modifier_projectile_effect_40:IsPurgable()
    return false
end

function modifier_projectile_effect_40:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_40:IsStunDebuff()
    return false
end

function modifier_projectile_effect_40:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_40:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_40:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_40:GetModifierProjectileName()
    return "particles/econ/items/earthshaker/earthshaker_arcana/earthshaker_arcana_echoslam_proj_v2.vpcf"
end

function modifier_projectile_effect_40:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end