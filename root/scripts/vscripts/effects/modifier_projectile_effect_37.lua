modifier_projectile_effect_37 = class({})
--Classifications template
function modifier_projectile_effect_37:IsHidden()
    return true
end

function modifier_projectile_effect_37:IsDebuff()
    return false
end

function modifier_projectile_effect_37:IsPurgable()
    return false
end

function modifier_projectile_effect_37:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_37:IsStunDebuff()
    return false
end

function modifier_projectile_effect_37:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_37:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_37:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_37:GetModifierProjectileName()
    return "particles/neutral_fx/satyr_trickster_projectile.vpcf"
end

function modifier_projectile_effect_37:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end