modifier_following_effect_25 = class({})

function modifier_following_effect_25:IsHidden()
	return true
end

function modifier_following_effect_25:IsPurgable()
	return false
end

function modifier_following_effect_25:IsPermanent()
	return true
end

function modifier_following_effect_25:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/chc/collection/auras/season_23_2/season_23_2_top_3_ribbon.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	-- self:StartIntervalThink(FrameTime())
	
end

function modifier_following_effect_25:OnIntervalThink()
	position = self:GetParent():GetAbsOrigin()
	ParticleManager:SetParticleControl(self.particleLeader, 0, Vector(position.x, position.y, position.z))
end
function modifier_following_effect_25:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
