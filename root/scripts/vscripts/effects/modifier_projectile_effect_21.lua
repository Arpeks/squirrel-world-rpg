modifier_projectile_effect_21 = class({})
--Classifications template
function modifier_projectile_effect_21:IsHidden()
    return true
end

function modifier_projectile_effect_21:IsDebuff()
    return false
end

function modifier_projectile_effect_21:IsPurgable()
    return false
end

function modifier_projectile_effect_21:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_21:IsStunDebuff()
    return false
end

function modifier_projectile_effect_21:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_21:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_21:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_21:GetModifierProjectileName()
    return "particles/units/heroes/hero_clinkz/clinkz_tar_bomb_projectile.vpcf"
end

function modifier_projectile_effect_21:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end