modifier_projectile_effect_46 = class({})
--Classifications template
function modifier_projectile_effect_46:IsHidden()
    return true
end

function modifier_projectile_effect_46:IsDebuff()
    return false
end

function modifier_projectile_effect_46:IsPurgable()
    return false
end

function modifier_projectile_effect_46:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_46:IsStunDebuff()
    return false
end

function modifier_projectile_effect_46:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_46:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_46:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_46:GetModifierProjectileName()
    return "particles/econ/items/skywrath_mage/skywrath_arcana/skywrath_arcana_base_attack.vpcf"
end

function modifier_projectile_effect_46:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end