modifier_following_effect_43 = class({})

function modifier_following_effect_43:IsHidden()
	return true
end

function modifier_following_effect_43:IsPurgable()
	return false
end

function modifier_following_effect_43:IsPermanent()
	return true
end

function modifier_following_effect_43:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/units/heroes/hero_mars/mars_arena_of_blood_heal.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_43:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
