modifier_following_effect_48 = class({})

function modifier_following_effect_48:IsHidden()
	return true
end

function modifier_following_effect_48:IsPurgable()
	return false
end

function modifier_following_effect_48:IsPermanent()
	return true
end

function modifier_following_effect_48:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/silencer/silencer_ti6/silencer_last_word_status_ti6.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_48:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
