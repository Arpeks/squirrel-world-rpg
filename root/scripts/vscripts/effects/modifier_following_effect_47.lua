modifier_following_effect_47 = class({})

function modifier_following_effect_47:IsHidden()
	return true
end

function modifier_following_effect_47:IsPurgable()
	return false
end

function modifier_following_effect_47:IsPermanent()
	return true
end

function modifier_following_effect_47:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/shadow_demon/gate_of_hell/gate_of_hell_disruption_ray.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_47:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
