modifier_projectile_effect_31 = class({})
--Classifications template
function modifier_projectile_effect_31:IsHidden()
    return true
end

function modifier_projectile_effect_31:IsDebuff()
    return false
end

function modifier_projectile_effect_31:IsPurgable()
    return false
end

function modifier_projectile_effect_31:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_31:IsStunDebuff()
    return false
end

function modifier_projectile_effect_31:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_31:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_31:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_31:GetModifierProjectileName()
    return "particles/units/heroes/hero_venomancer/venomancer_noxious_plague_projectile.vpcf"
end

function modifier_projectile_effect_31:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end