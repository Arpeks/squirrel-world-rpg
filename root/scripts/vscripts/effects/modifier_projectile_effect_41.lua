modifier_projectile_effect_41 = class({})
--Classifications template
function modifier_projectile_effect_41:IsHidden()
    return true
end

function modifier_projectile_effect_41:IsDebuff()
    return false
end

function modifier_projectile_effect_41:IsPurgable()
    return false
end

function modifier_projectile_effect_41:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_41:IsStunDebuff()
    return false
end

function modifier_projectile_effect_41:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_41:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_41:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_41:GetModifierProjectileName()
    return "particles/units/heroes/hero_chen/chen_penitence_proj.vpcf"
end

function modifier_projectile_effect_41:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end