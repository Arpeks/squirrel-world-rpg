modifier_following_effect_38 = class({})

function modifier_following_effect_38:IsHidden()
	return true
end

function modifier_following_effect_38:IsPurgable()
	return false
end

function modifier_following_effect_38:IsPermanent()
	return true
end

function modifier_following_effect_38:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/act_2/siltbreaker_channel.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_38:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
