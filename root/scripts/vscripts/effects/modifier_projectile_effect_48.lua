modifier_projectile_effect_48 = class({})
--Classifications template
function modifier_projectile_effect_48:IsHidden()
    return true
end

function modifier_projectile_effect_48:IsDebuff()
    return false
end

function modifier_projectile_effect_48:IsPurgable()
    return false
end

function modifier_projectile_effect_48:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_48:IsStunDebuff()
    return false
end

function modifier_projectile_effect_48:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_48:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_48:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_48:GetModifierProjectileName()
    return "particles/econ/items/rubick/rubick_arcana/rbck_arc_skeletonking_hellfireblast.vpcf"
end

function modifier_projectile_effect_48:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end