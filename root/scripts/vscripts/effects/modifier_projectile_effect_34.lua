modifier_projectile_effect_34 = class({})
--Classifications template
function modifier_projectile_effect_34:IsHidden()
    return true
end

function modifier_projectile_effect_34:IsDebuff()
    return false
end

function modifier_projectile_effect_34:IsPurgable()
    return false
end

function modifier_projectile_effect_34:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_34:IsStunDebuff()
    return false
end

function modifier_projectile_effect_34:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_34:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_34:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_34:GetModifierProjectileName()
    return "particles/econ/items/skywrath_mage/skywrath_arcana/skywrath_arcana_concussive_shot.vpcf"
end

function modifier_projectile_effect_34:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end