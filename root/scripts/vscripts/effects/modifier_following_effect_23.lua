modifier_following_effect_23 = class({})

function modifier_following_effect_23:IsHidden()
	return true
end

function modifier_following_effect_23:IsPurgable()
	return false
end

function modifier_following_effect_23:IsPermanent()
	return true
end

function modifier_following_effect_23:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/chc/cosmetic/items/items_mael_mjoll_ti6/items_mael_mjoll_ti6_d.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	-- self:StartIntervalThink(FrameTime())
	
end

function modifier_following_effect_23:OnIntervalThink()
	position = self:GetParent():GetAbsOrigin()
	position.z = position.z + 1500
	position.y = position.y + 300
	ParticleManager:SetParticleControl(self.particleLeader, 1, Vector(position.x, position.y, position.z))
end
function modifier_following_effect_23:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
