modifier_following_effect_36 = class({})

function modifier_following_effect_36:IsHidden()
	return true
end

function modifier_following_effect_36:IsPurgable()
	return false
end

function modifier_following_effect_36:IsPermanent()
	return true
end

function modifier_following_effect_36:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/oracle/oracle_ti10_immortal/oracle_ti10_immortal_purifyingflames.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_36:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
