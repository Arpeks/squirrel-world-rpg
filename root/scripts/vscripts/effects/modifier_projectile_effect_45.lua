modifier_projectile_effect_45 = class({})
--Classifications template
function modifier_projectile_effect_45:IsHidden()
    return true
end

function modifier_projectile_effect_45:IsDebuff()
    return false
end

function modifier_projectile_effect_45:IsPurgable()
    return false
end

function modifier_projectile_effect_45:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_45:IsStunDebuff()
    return false
end

function modifier_projectile_effect_45:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_45:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_45:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_45:GetModifierProjectileName()
    return "particles/units/heroes/hero_jakiro/jakiro_liquid_ice_projectile_attack.vpcf"
end

function modifier_projectile_effect_45:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end