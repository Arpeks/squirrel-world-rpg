modifier_following_effect_35 = class({})

function modifier_following_effect_35:IsHidden()
	return true
end

function modifier_following_effect_35:IsPurgable()
	return false
end

function modifier_following_effect_35:IsPermanent()
	return true
end

function modifier_following_effect_35:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/oracle/oracle_fortune_ti7/oracle_fortune_ti7_purge_root_ring_rev.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_35:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
