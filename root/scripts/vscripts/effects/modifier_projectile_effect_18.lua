modifier_projectile_effect_18 = class({})
--Classifications template
function modifier_projectile_effect_18:IsHidden()
    return true
end

function modifier_projectile_effect_18:IsDebuff()
    return false
end

function modifier_projectile_effect_18:IsPurgable()
    return false
end

function modifier_projectile_effect_18:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_18:IsStunDebuff()
    return false
end

function modifier_projectile_effect_18:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_18:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_18:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_18:GetModifierProjectileName()
    return "particles/units/heroes/hero_bane/bane_projectile.vpcf"
end

function modifier_projectile_effect_18:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end