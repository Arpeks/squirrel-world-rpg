modifier_following_effect_39 = class({})

function modifier_following_effect_39:IsHidden()
	return true
end

function modifier_following_effect_39:IsPurgable()
	return false
end

function modifier_following_effect_39:IsPermanent()
	return true
end

function modifier_following_effect_39:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/act_2/storegga_channel_elec.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_39:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
