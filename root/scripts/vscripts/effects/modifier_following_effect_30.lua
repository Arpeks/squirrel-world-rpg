modifier_following_effect_30 = class({})

function modifier_following_effect_30:IsHidden()
	return true
end

function modifier_following_effect_30:IsPurgable()
	return false
end

function modifier_following_effect_30:IsPermanent()
	return true
end

function modifier_following_effect_30:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/hold_the_dore/particles/low_poly_fx/archer_a_fire_2.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	
end

function modifier_following_effect_30:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
