modifier_following_effect_42 = class({})

function modifier_following_effect_42:IsHidden()
	return true
end

function modifier_following_effect_42:IsPurgable()
	return false
end

function modifier_following_effect_42:IsPermanent()
	return true
end

function modifier_following_effect_42:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/courier/courier_trail_orbit/courier_trail_orbit.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_42:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
