modifier_following_effect_46 = class({})

function modifier_following_effect_46:IsHidden()
	return true
end

function modifier_following_effect_46:IsPurgable()
	return false
end

function modifier_following_effect_46:IsPermanent()
	return true
end

function modifier_following_effect_46:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/riki/riki_crownfall_immortal_weapon/riki_crownfall_immortal_tricksd.vpcf", PATTACH_POINT_FOLLOW, self.caster )
end

function modifier_following_effect_46:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
