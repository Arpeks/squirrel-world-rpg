modifier_following_effect_21 = class({})

function modifier_following_effect_21:IsHidden()
	return true
end

function modifier_following_effect_21:IsPurgable()
	return false
end

function modifier_following_effect_21:IsPermanent()
	return true
end

function modifier_following_effect_21:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/phoenix/phoenix_solar_forge/phoenix_sunray_solar_forge.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	self:StartIntervalThink(FrameTime())
	
end

function modifier_following_effect_21:OnIntervalThink()
	position = self:GetParent():GetAbsOrigin()
	position.z = position.z + 1500
	position.y = position.y + 300
	ParticleManager:SetParticleControl(self.particleLeader, 1, Vector(position.x, position.y, position.z))
end
function modifier_following_effect_21:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
