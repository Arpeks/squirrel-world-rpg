modifier_following_effect_29 = class({})

function modifier_following_effect_29:IsHidden()
	return true
end

function modifier_following_effect_29:IsPurgable()
	return false
end

function modifier_following_effect_29:IsPermanent()
	return true
end

function modifier_following_effect_29:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/hold_the_dore/particles/fx_w1/durable_m.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	-- self:StartIntervalThink(0.5)
	
end
function modifier_following_effect_29:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
