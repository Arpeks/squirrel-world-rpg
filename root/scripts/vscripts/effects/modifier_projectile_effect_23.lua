modifier_projectile_effect_23 = class({})
--Classifications template
function modifier_projectile_effect_23:IsHidden()
    return true
end

function modifier_projectile_effect_23:IsDebuff()
    return false
end

function modifier_projectile_effect_23:IsPurgable()
    return false
end

function modifier_projectile_effect_23:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_23:IsStunDebuff()
    return false
end

function modifier_projectile_effect_23:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_23:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_23:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_23:GetModifierProjectileName()
    return "particles/items2_fx/paintball.vpcf"
end

function modifier_projectile_effect_23:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end