modifier_projectile_effect_26 = class({})
--Classifications template
function modifier_projectile_effect_26:IsHidden()
    return true
end

function modifier_projectile_effect_26:IsDebuff()
    return false
end

function modifier_projectile_effect_26:IsPurgable()
    return false
end

function modifier_projectile_effect_26:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_26:IsStunDebuff()
    return false
end

function modifier_projectile_effect_26:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_26:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_26:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_26:GetModifierProjectileName()
    return "particles/econ/events/ti10/hot_potato/hot_potato_projectile.vpcf"
end

function modifier_projectile_effect_26:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end