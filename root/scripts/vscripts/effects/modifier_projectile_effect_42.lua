modifier_projectile_effect_42 = class({})
--Classifications template
function modifier_projectile_effect_42:IsHidden()
    return true
end

function modifier_projectile_effect_42:IsDebuff()
    return false
end

function modifier_projectile_effect_42:IsPurgable()
    return false
end

function modifier_projectile_effect_42:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_42:IsStunDebuff()
    return false
end

function modifier_projectile_effect_42:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_42:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_42:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_42:GetModifierProjectileName()
    return "particles/units/heroes/hero_demonartist/demonartist_engulf_proj.vpcf"
end

function modifier_projectile_effect_42:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end