modifier_projectile_effect_16 = class({})
--Classifications template
function modifier_projectile_effect_16:IsHidden()
    return true
end

function modifier_projectile_effect_16:IsDebuff()
    return false
end

function modifier_projectile_effect_16:IsPurgable()
    return false
end

function modifier_projectile_effect_16:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_16:IsStunDebuff()
    return false
end

function modifier_projectile_effect_16:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_16:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_16:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_16:GetModifierProjectileName()
    return "particles/econ/items/queen_of_pain/qop_2022_immortal/queen_2022_scream_of_pain_projectile.vpcf"
end

function modifier_projectile_effect_16:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end