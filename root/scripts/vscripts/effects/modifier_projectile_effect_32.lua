modifier_projectile_effect_32 = class({})
--Classifications template
function modifier_projectile_effect_32:IsHidden()
    return true
end

function modifier_projectile_effect_32:IsDebuff()
    return false
end

function modifier_projectile_effect_32:IsPurgable()
    return false
end

function modifier_projectile_effect_32:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_32:IsStunDebuff()
    return false
end

function modifier_projectile_effect_32:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_32:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_32:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_32:GetModifierProjectileName()
    return "particles/units/heroes/hero_sven/sven_spell_storm_bolt.vpcf"
end

function modifier_projectile_effect_32:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end