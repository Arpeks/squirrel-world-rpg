modifier_projectile_effect_44 = class({})
--Classifications template
function modifier_projectile_effect_44:IsHidden()
    return true
end

function modifier_projectile_effect_44:IsDebuff()
    return false
end

function modifier_projectile_effect_44:IsPurgable()
    return false
end

function modifier_projectile_effect_44:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_44:IsStunDebuff()
    return false
end

function modifier_projectile_effect_44:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_44:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_44:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_44:GetModifierProjectileName()
    return "particles/units/heroes/hero_snapfire/hero_snapfire_cookie_projectile.vpcf"
end

function modifier_projectile_effect_44:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end