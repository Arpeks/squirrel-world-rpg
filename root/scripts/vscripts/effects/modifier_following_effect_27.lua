modifier_following_effect_27 = class({})

function modifier_following_effect_27:IsHidden()
	return true
end

function modifier_following_effect_27:IsPurgable()
	return false
end

function modifier_following_effect_27:IsPermanent()
	return true
end

function modifier_following_effect_27:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/sweep_generic/sweep_1.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	self:StartIntervalThink(0.5)
	
end

function modifier_following_effect_27:OnIntervalThink()
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
	self.particleLeader = ParticleManager:CreateParticle( "particles/sweep_generic/sweep_1.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
end
function modifier_following_effect_27:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
