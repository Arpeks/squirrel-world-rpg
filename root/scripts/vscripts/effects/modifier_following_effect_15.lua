modifier_following_effect_15 = class({})

function modifier_following_effect_15:IsHidden()
	return true
end

function modifier_following_effect_15:IsPurgable()
	return false
end

function modifier_following_effect_15:IsPermanent()
	return true
end

function modifier_following_effect_15:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/econ/items/bane/bane_fall20_immortal/bane_fall20_immortal_grip.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
end

function modifier_following_effect_15:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
