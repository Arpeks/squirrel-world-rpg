modifier_following_effect_28 = class({})

function modifier_following_effect_28:IsHidden()
	return true
end

function modifier_following_effect_28:IsPurgable()
	return false
end

function modifier_following_effect_28:IsPermanent()
	return true
end

function modifier_following_effect_28:OnCreated( kv )
	self.caster = self:GetCaster()
	self.particleLeader = ParticleManager:CreateParticle( "particles/hold_the_dore/particles/ability01/omni_2021_immortal_buff_lv4.vpcf", PATTACH_POINT_FOLLOW, self.caster )
	-- ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
	-- self:StartIntervalThink(0.5)
	
end

-- function modifier_following_effect_28:OnIntervalThink()
-- 	ParticleManager:DestroyParticle(self.particleLeader, true)
--     ParticleManager:ReleaseParticleIndex(self.particleLeader)
-- 	self.particleLeader = ParticleManager:CreateParticle( "particles/sweep_generic/sweep_1.vpcf", PATTACH_POINT_FOLLOW, self.caster )
-- 	ParticleManager:SetParticleControlEnt( self.particleLeader, PATTACH_OVERHEAD_FOLLOW, self.caster, PATTACH_OVERHEAD_FOLLOW, "PATTACH_OVERHEAD_FOLLOW", self.caster:GetAbsOrigin(), true )
-- end
function modifier_following_effect_28:OnDestroy( kv )
	ParticleManager:DestroyParticle(self.particleLeader, true)
    ParticleManager:ReleaseParticleIndex(self.particleLeader)
end
