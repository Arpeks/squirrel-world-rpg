modifier_projectile_effect_36 = class({})
--Classifications template
function modifier_projectile_effect_36:IsHidden()
    return true
end

function modifier_projectile_effect_36:IsDebuff()
    return false
end

function modifier_projectile_effect_36:IsPurgable()
    return false
end

function modifier_projectile_effect_36:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_projectile_effect_36:IsStunDebuff()
    return false
end

function modifier_projectile_effect_36:RemoveOnDeath()
    return false
end

function modifier_projectile_effect_36:DestroyOnExpire()
    return false
end

function modifier_projectile_effect_36:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_NAME
    }
end

function modifier_projectile_effect_36:GetModifierProjectileName()
    return "particles/econ/items/sniper/sniper_fall20_immortal/sniper_fall20_immortal_assassinate.vpcf"
end

function modifier_projectile_effect_36:GetPriority()
    return MODIFIER_PRIORITY_HIGH
end