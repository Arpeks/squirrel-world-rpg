modifier_phantom_assassin_coup_de_grace_lua = class({})
-- LinkLuaModifier( "modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50", "heroes/hero_phantom/phantom_assassin_coup_de_grace_lua/modifier_phantom_assassin_coup_de_grace_lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------
-- Classifications
function modifier_phantom_assassin_coup_de_grace_lua:IsHidden()
	-- actual true
	return not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_phantom_assassin_agi50") or self:GetStackCount() == 0
end

function modifier_phantom_assassin_coup_de_grace_lua:IsPurgable()
	return false
end

function modifier_phantom_assassin_coup_de_grace_lua:OnCreated( kv )
	self.crit_chance = self:GetAbility():GetSpecialValueFor( "crit_chance" )
	self.crit_bonus = self:GetAbility():GetSpecialValueFor( "crit_bonus" )
	
	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi10") ~= nil then
		self.crit_chance = self.crit_chance + 10
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi_last") ~= nil then
		self.crit_chance = self.crit_chance + 35
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi9") ~= nil then
		self.crit_bonus = self:GetAbility():GetSpecialValueFor( "crit_bonus" ) + 200
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_phantom_assassin_agi50") ~= nil then
		-- self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50", {})
	end
	self:StartIntervalThink(1)
end

function modifier_phantom_assassin_coup_de_grace_lua:OnRefresh( kv )
	self.crit_chance = self:GetAbility():GetSpecialValueFor( "crit_chance" )
	self.crit_bonus = self:GetAbility():GetSpecialValueFor( "crit_bonus" )

	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi10") ~= nil then
		self.crit_chance = self.crit_chance + 10
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi_last") ~= nil then
		self.crit_chance = self.crit_chance + 35
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_agi9") ~= nil then
		self.crit_bonus = self:GetAbility():GetSpecialValueFor( "crit_bonus" ) + 200
	end
end


function modifier_phantom_assassin_coup_de_grace_lua:OnIntervalThink()
self:OnRefresh()
end

function modifier_phantom_assassin_coup_de_grace_lua:OnDestroy( kv )

end

--------------------------------------------------------------------------------
-- Modifier Effects
function modifier_phantom_assassin_coup_de_grace_lua:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
		MODIFIER_PROPERTY_PROCATTACK_FEEDBACK,
		-- MODIFIER_EVENT_ON_DEATH,
		-- MODIFIER_EVENT_ON_ATTACK_FINISHED,
		MODIFIER_PROPERTY_TOOLTIP,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}
	return funcs
end

function modifier_phantom_assassin_coup_de_grace_lua:OnTooltip()
    return self:GetStackCount()
end

function modifier_phantom_assassin_coup_de_grace_lua:GetModifierPreAttack_CriticalStrike( params )
	if IsServer() and (not self:GetParent():PassivesDisabled()) then
		if self:RollChance( self.crit_chance ) then
			self.record = params.record
			if self:GetCaster():FindAbilityByName("npc_dota_hero_phantom_assassin_int9") ~= nil	then 
			--if params.inflictor ~= nil and params.inflictor:GetAbilityName() == "phantom_assassin_knifes" then return end
				local abil2 = self:GetCaster():FindAbilityByName("phantom_assassin_knifes")
					if abil2 ~= nil and abil2:GetLevel() > 0 then
					local r2 = RandomInt(1,4)
					if r2 == 1 and not self:GetCaster():HasModifier("modifier_phantom_assassin_knifes_attack") then
						abil2:OnSpellStart()
					end
				end
			end
			self.critical_strike = true
			local crti_damage = self.crit_bonus
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_phantom_assassin_agi50") then
				crti_damage = crti_damage + self:GetStackCount()
			end
			return crti_damage
		end
	end
end

function modifier_phantom_assassin_coup_de_grace_lua:GetModifierProcAttack_Feedback( params )
	if IsServer() then
		if self.record then
			self.record = nil
			self:PlayEffects( params.target )
		end
	end
end

function modifier_phantom_assassin_coup_de_grace_lua:RollChance( chance )
	local rand = math.random()
	if rand<chance/100 then
		return true
	end
	return false
end

function modifier_phantom_assassin_coup_de_grace_lua:PlayEffects( target )
	local particle_cast = "particles/units/heroes/hero_phantom_assassin/phantom_assassin_crit_impact.vpcf"
	local sound_cast = "Hero_PhantomAssassin.CoupDeGrace"

	local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControlEnt(
		effect_cast,
		0,
		target,
		PATTACH_POINT_FOLLOW,
		"attach_hitloc",
		target:GetOrigin(), -- unknown
		true -- unknown, true
	)
	ParticleManager:SetParticleControlForward( effect_cast, 1, (self:GetParent():GetOrigin()-target:GetOrigin()):Normalized() )
	ParticleManager:ReleaseParticleIndex( effect_cast )

	EmitSoundOn( sound_cast, target )
end

function modifier_phantom_assassin_coup_de_grace_lua:CustomOnDeath(data)
	if self:GetParent() == data.attacker then
		-- if self.critical_strike and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_phantom_assassin_agi50") and table.has_value(bosses_names, data.unit:GetUnitName()) then
		if self.critical_strike and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_phantom_assassin_agi50") then
			self.stack_damage = self.stack_damage or 0
			self.stack_damage = self.stack_damage + 0.96
			self:SetStackCount(self.stack_damage)
		end
	end
end

function modifier_phantom_assassin_coup_de_grace_lua:CustomOnAttackFinished(data)
	if self:GetParent() == data.attacker then
		self.critical_strike = false
	end
end

function modifier_phantom_assassin_coup_de_grace_lua:CustomOnAttackLanded(keys)
	if keys.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() and keys.attacker:HasModifier("modifier_npc_dota_hero_phantom_assassin_buff_1") then

				local cleave_damage_percent = 15 + self:GetAbility():GetLevel() * 2

				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (cleave_damage_percent/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )

			end
		end
	end
end