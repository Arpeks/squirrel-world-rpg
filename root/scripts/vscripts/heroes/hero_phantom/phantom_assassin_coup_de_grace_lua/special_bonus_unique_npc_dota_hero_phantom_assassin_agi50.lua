LinkLuaModifier( "modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50", "heroes/hero_phantom/phantom_assassin_coup_de_grace_lua/special_bonus_unique_npc_dota_hero_phantom_assassin_agi50", LUA_MODIFIER_MOTION_NONE )
special_bonus_unique_npc_dota_hero_phantom_assassin_agi50 = class({})

function special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:GetIntrinsicModifierName()
	return "modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50"
end


modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50 = class({})
--Classifications template
function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:IsHidden()
	return false
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:IsDebuff()
	return false
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:IsPurgable()
	return false
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:RemoveOnDeath()
	return false
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_DEATH
	}
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:CustomOnDeath(data)
	if self:GetParent() == data.attacker then
		if data.original_damage > self:GetCaster():GetAverageTrueAttackDamage(nil) * 1.1 and data.unit:IsBoss() then
			self:IncrementStackCount()
			if not self.interval then
				self.interval = true
				self:StartIntervalThink(120)
			end
		end
	end
end

function modifier_special_bonus_unique_npc_dota_hero_phantom_assassin_agi50:OnIntervalThink()
	self:DecrementStackCount()
	if self:GetStackCount() == 0 then
		self.interval = false
		self:StartIntervalThink(-1)
	end
end