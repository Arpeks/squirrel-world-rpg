"DOTAAbilities"
{
    //=================================================================================================================
	// Death Prophet: Carrion Swarm
	//=================================================================================================================
	"death_prophet_occultism"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_death_prophet/death_prophet_occultism/death_prophet_occultism"
		"AbilityTextureName"			"undying_tombstone"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AOE"
		"FightRecapLevel"				"1"
		"MaxLevel"						"1"
		"InnateAbility"					"1"
		"AbilityCooldown"				"60"
		"AbilityCastPoint"				"0.2"
		"AbilityCastRange"				"900"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"duration"
			{
				"value"						"60"
			}
			"radius"
			{
				"value"						"900"
				"affected_by_aoe_increase"	"1"
			}
			"ghost_count"
			{
				"value"						"5"
			}
		}
	}
}