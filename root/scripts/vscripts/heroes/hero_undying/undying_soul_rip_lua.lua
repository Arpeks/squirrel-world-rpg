LinkLuaModifier("modifier_undying_soul_rip_lua", "heroes/hero_undying/undying_soul_rip_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_soul_rip_lua_str11", "heroes/hero_undying/undying_soul_rip_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_soul_rip_lua_str11_buff", "heroes/hero_undying/undying_soul_rip_lua", LUA_MODIFIER_MOTION_NONE)

undying_soul_rip_lua = class({})

function undying_soul_rip_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function undying_soul_rip_lua:GetIntrinsicModifierName()
    return "modifier_undying_soul_rip_lua"
end

function undying_soul_rip_lua:OnAbilityPhaseStart()
	EmitSoundOn("Hero_Undying.SoulRip.Cast", self:GetCaster() )
	return true
end

function undying_soul_rip_lua:OnAbilityPhaseInterrupted()
	StopSoundOn("Hero_Undying.SoulRip.Cast", self:GetCaster() )
end

function undying_soul_rip_lua:OnSpellStart()
    self:OnSpellStart_target(self:GetCursorTarget())
end

function undying_soul_rip_lua:OnSpellStart_target(target)
	local radius = self:GetSpecialValueFor("radius")
	local damage_per_unit = self:GetSpecialValueFor("damage_per_unit")
	local max_units = self:GetSpecialValueFor("max_units")
	local counter = 0
    local total_damage = 0
    self.special_bonus_unique_npc_dota_hero_undying_agi13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi13") ~= nil
    local special_bonus_unique_npc_dota_hero_undying_str11 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_str11") ~= nil
	EmitSoundOn("Hero_Undying.SoulRip.Enemy", target)
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_soul_rip_damage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControl(pcf, 0, self:GetCaster():GetOrigin())
	ParticleManager:SetParticleControl(pcf, 1, target:GetOrigin())
	ParticleManager:SetParticleControl(pcf, 2, self:GetCaster():GetOrigin())
    ParticleManager:ReleaseParticleIndex(pcf)

	local flags = DOTA_DAMAGE_FLAG_HPLOSS + DOTA_DAMAGE_FLAG_REFLECTION
	local totalValue = 0
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _, unit in ipairs( units ) do
        if maxUnits == counter then
            break
        end
        total_damage = total_damage + ApplyDamageRDA({
            victim = unit,
            attacker = self:GetCaster(),
            damage = damage_per_unit,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = flags,
            ability = self
        })
        if special_bonus_unique_npc_dota_hero_undying_str11 then
            self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_soul_rip_lua_str11", {duration = 180})
            self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_soul_rip_lua_str11_buff", {duration = 180})
        end
		counter = counter + 1
        if self.special_bonus_unique_npc_dota_hero_undying_agi13 then
            self:GetCaster():PerformAttack(unit, true, true, true, true, false, false, true)
        end
	end
	SendOverheadEventMessage( self:GetCaster():GetPlayerOwner(), OVERHEAD_ALERT_HEAL , self:GetCaster(), total_damage, nil )
    self:GetCaster():HealWithParams(math.min(math.abs(total_damage), 2^30), self, false, false, self:GetCaster(), false)
    if target:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
        ApplyDamageRDA({
            victim = self:GetCursorTarget(),
            attacker = self:GetCaster(),
            damage = counter * damage_per_unit,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self
        })
    end
end

modifier_undying_soul_rip_lua = class({})
--Classifications template
function modifier_undying_soul_rip_lua:IsHidden()
    return true
end

function modifier_undying_soul_rip_lua:IsDebuff()
    return false
end

function modifier_undying_soul_rip_lua:IsPurgable()
    return false
end

function modifier_undying_soul_rip_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_soul_rip_lua:IsStunDebuff()
    return false
end

function modifier_undying_soul_rip_lua:RemoveOnDeath()
    return false
end

function modifier_undying_soul_rip_lua:DestroyOnExpire()
    return false
end

function modifier_undying_soul_rip_lua:DestroyOnExpire()
    return false
end

function modifier_undying_soul_rip_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(0.2)
end

function modifier_undying_soul_rip_lua:OnIntervalThink()
    if self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() then
        local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
        if #units > 0 then
            self:GetAbility():UseResources(true, false, false, true)
            self:GetAbility():OnSpellStart_target(self:GetParent())
        end
    end
end

function modifier_undying_soul_rip_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_undying_soul_rip_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_per_unit" then
			return 1
		end
	end
	return 0
end

function modifier_undying_soul_rip_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_per_unit" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage_per_unit", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_str8") then
                value = value + self:GetCaster():GetStrength()
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int8") then
                value = value + self:GetCaster():GetIntellect(true)
            end
            return value
		end
	end
	return 0
end

modifier_undying_soul_rip_lua_str11 = class({})
--Classifications template
function modifier_undying_soul_rip_lua_str11:IsHidden()
    return false
end

function modifier_undying_soul_rip_lua_str11:IsDebuff()
    return false
end

function modifier_undying_soul_rip_lua_str11:IsPurgable()
    return false
end

function modifier_undying_soul_rip_lua_str11:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_soul_rip_lua_str11:IsStunDebuff()
    return false
end

function modifier_undying_soul_rip_lua_str11:RemoveOnDeath()
    return true
end

function modifier_undying_soul_rip_lua_str11:DestroyOnExpire()
    return true
end

function modifier_undying_soul_rip_lua_str11:OnCreated()
    if not IsServer() then return end
    self:SetStackCount(1)
    self:StartIntervalThink(FrameTime())
end

function modifier_undying_soul_rip_lua_str11:OnIntervalThink()
    local stack = self:GetParent():FindAllModifiersByName("modifier_undying_soul_rip_lua_str11_buff")
    self:SetStackCount(#stack * 3)
end

function modifier_undying_soul_rip_lua_str11:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS
    }
end

function modifier_undying_soul_rip_lua_str11:GetModifierBonusStats_Strength()
    return self:GetStackCount()
end

modifier_undying_soul_rip_lua_str11_buff = class({})

function modifier_undying_soul_rip_lua_str11_buff:IsHidden() 
    return true 
end

function modifier_undying_soul_rip_lua_str11_buff:IsPurgable() 
    return false 
end

function modifier_undying_soul_rip_lua_str11_buff:GetAttributes() 
    return MODIFIER_ATTRIBUTE_MULTIPLE 
end