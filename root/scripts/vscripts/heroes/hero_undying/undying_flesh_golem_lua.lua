undying_flesh_golem_lua = class({})

LinkLuaModifier("modifier_undying_flesh_golem_lua", "heroes/hero_undying/undying_flesh_golem_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_flesh_golem_lua_debuff", "heroes/hero_undying/undying_flesh_golem_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_flesh_golem_lua_int7", "heroes/hero_undying/undying_flesh_golem_lua", LUA_MODIFIER_MOTION_NONE)

function undying_flesh_golem_lua:GetManaCost(iLevel)
	return 200 + math.min(65000, self:GetCaster():GetIntellect(true) / 50)
end

function undying_flesh_golem_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int7") then
        return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
    end
	return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
end

function undying_flesh_golem_lua:OnSpellStart()
    if self:GetCursorTarget() ~= nil then
        self:GetCursorTarget():AddNewModifier(self:GetCaster(), self, "modifier_undying_flesh_golem_lua", {duration = self:GetSpecialValueFor("duration")})
        self.mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_flesh_golem_lua", {duration = self:GetSpecialValueFor("duration")})
    else
        self.mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_flesh_golem_lua", {duration = self:GetSpecialValueFor("duration")})
    end
end

modifier_undying_flesh_golem_lua = class({})
--Classifications template
function modifier_undying_flesh_golem_lua:IsHidden()
    return false
end

function modifier_undying_flesh_golem_lua:IsDebuff()
    return false
end

function modifier_undying_flesh_golem_lua:IsPurgable()
    return false
end

function modifier_undying_flesh_golem_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_flesh_golem_lua:IsStunDebuff()
    return false
end

function modifier_undying_flesh_golem_lua:RemoveOnDeath()
    return true
end

function modifier_undying_flesh_golem_lua:DestroyOnExpire()
    return true
end

function modifier_undying_flesh_golem_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:GetParent():StartGesture(ACT_DOTA_SPAWN)
    self.special_bonus_unique_npc_dota_hero_undying_agi9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi9") ~= nil
    self.special_bonus_unique_npc_dota_hero_undying_agi12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi12") ~= nil
end

function modifier_undying_flesh_golem_lua:OnRefresh()
    self:OnCreated()
end

function modifier_undying_flesh_golem_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_MODEL_CHANGE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED
    }
end

function modifier_undying_flesh_golem_lua:GetModifierBonusStats_Strength()
    return 0
    -- if self:GetParent().calc_str then
    --     return 0 
    -- end
	-- self:GetParent().calc_str = true
    -- local s = self:GetParent():GetStrength() * self:GetAbility():GetSpecialValueFor("str_percentage") * 0.01
	-- self:GetParent().calc_str = false
    -- return s
end

function modifier_undying_flesh_golem_lua:GetModifierBonusStats_Agility()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi10") then
        if self:GetParent().calc_agi then
            return 0 
        end
        self:GetParent().calc_agi = true
        local s = self:GetParent():GetAgility() * self:GetAbility():GetSpecialValueFor("str_percentage") * 0.01
        self:GetParent().calc_agi = false
        return s
    end
    return 0
end

function modifier_undying_flesh_golem_lua:GetModifierModelChange()
    return "models/heroes/undying/undying_flesh_golem.vmdl"
end

function modifier_undying_flesh_golem_lua:CustomOnAttackLanded( data )
    if not IsServer() then
        return
    end
    if data.attacker ~= self:GetParent() then
        return
    end
    if data.target:IsBuilding() then
        return
    end
    data.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_undying_flesh_golem_lua_debuff", {duration = 3})
    if self.special_bonus_unique_npc_dota_hero_undying_agi9 then
        if data.target ~= nil and data.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
            local direction = data.target:GetOrigin()-self:GetParent():GetOrigin()
            direction.z = 0
            direction = direction:Normalized()
            local facing_direction = data.attacker:GetAnglesAsVector().y
            local cleave_targets = FindUnitsInRadius(data.attacker:GetTeamNumber(),data.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
            for _,target in pairs(cleave_targets) do
                if target ~= data.target then
                    local attacker_vector = (target:GetOrigin() - data.attacker:GetOrigin())
                    local attacker_direction = VectorToAngles( attacker_vector ).y
                    local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
                    if angle_diff < 45 then
                        ApplyDamageRDA({
                            victim = target,
                            attacker = data.attacker,
                            damage = data.damage * 0.7,
                            damage_type = DAMAGE_TYPE_PHYSICAL,
                            damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
                            ability = self:GetAbility()
                        })
                    end
                end
            end
            local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )
            ParticleManager:SetParticleControl( effect_cast, 0, self:GetParent():GetOrigin() )
            ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
            ParticleManager:ReleaseParticleIndex( effect_cast )
        end
    end
    if self.special_bonus_unique_npc_dota_hero_undying_agi12 then
        ApplyDamageRDA({
            victim = data.target,
            attacker = data.attacker,
            damage = self:GetCaster():GetAgility(),
            damage_type = DAMAGE_TYPE_PURE,
            damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
            ability = self:GetAbility()
        })
    end
end

--------------------------------------------------------------------------------
-- Aura Effects
function modifier_undying_flesh_golem_lua:IsAura()
    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int6") ~= nil
end

function modifier_undying_flesh_golem_lua:GetModifierAura()
    return "modifier_undying_flesh_golem_lua_int7"
end

function modifier_undying_flesh_golem_lua:GetAuraRadius()
    return 700
end

function modifier_undying_flesh_golem_lua:GetAuraDuration()
    return 0.5
end

function modifier_undying_flesh_golem_lua:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_undying_flesh_golem_lua:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_undying_flesh_golem_lua:GetAuraSearchFlags()
    return 0
end

modifier_undying_flesh_golem_lua_int7 = class({})
--Classifications template
function modifier_undying_flesh_golem_lua_int7:IsHidden()
    return false
end

function modifier_undying_flesh_golem_lua_int7:IsDebuff()
    return true
end

function modifier_undying_flesh_golem_lua_int7:IsPurgable()
    return false
end

function modifier_undying_flesh_golem_lua_int7:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_flesh_golem_lua_int7:IsStunDebuff()
    return false
end

function modifier_undying_flesh_golem_lua_int7:RemoveOnDeath()
    return true
end

function modifier_undying_flesh_golem_lua_int7:DestroyOnExpire()
    return true
end

function modifier_undying_flesh_golem_lua_int7:OnCreated()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(1)
end

function modifier_undying_flesh_golem_lua_int7:OnIntervalThink()
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self:GetCaster():GetIntellect(true),
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self:GetAbility()
    })
end











modifier_undying_flesh_golem_lua_debuff = class({})
--Classifications template
function modifier_undying_flesh_golem_lua_debuff:IsHidden()
    return false
end

function modifier_undying_flesh_golem_lua_debuff:IsDebuff()
    return true
end

function modifier_undying_flesh_golem_lua_debuff:IsPurgable()
    return false
end

function modifier_undying_flesh_golem_lua_debuff:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_flesh_golem_lua_debuff:IsStunDebuff()
    return false
end

function modifier_undying_flesh_golem_lua_debuff:RemoveOnDeath()
    return true
end

function modifier_undying_flesh_golem_lua_debuff:DestroyOnExpire()
    return true
end

function modifier_undying_flesh_golem_lua_debuff:OnCreated()
    if not IsServer() then
        return
    end
    self.spell_amp = self:GetAbility():GetSpecialValueFor("damage_increace")
    self:GetParent():IncreaceDamageDifficult(self.spell_amp, "all")
end

function modifier_undying_flesh_golem_lua_debuff:OnDestroy()
	if not IsServer() then
		return
	end
	self:GetParent():RefreshDamageDifficult()
end

function modifier_undying_flesh_golem_lua_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
	}
end

function modifier_undying_flesh_golem_lua_debuff:GetModifierIncomingDamage_Percentage(data)
	return 0
end