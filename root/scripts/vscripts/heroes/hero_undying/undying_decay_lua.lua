LinkLuaModifier("modifier_undying_decay_lua", "heroes/hero_undying/undying_decay_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_decay_lua_buff", "heroes/hero_undying/undying_decay_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_decay_lua_buff_counter", "heroes/hero_undying/undying_decay_lua", LUA_MODIFIER_MOTION_NONE)

undying_decay_lua = class({}) 

function undying_decay_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function undying_decay_lua:GetIntrinsicModifierName()
    return "modifier_undying_decay_lua"
end

function undying_decay_lua:Precache(context)
    PrecacheResource( "particle", "particles/units/heroes/hero_undying/undying_decay.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_undying/undying_decay_strength_xfer.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_undying/undying_decay_strength_buff.vpcf", context )
end

function undying_decay_lua:GetAOERadius()
    return self:GetSpecialValueFor("radius")
end

function undying_decay_lua:OnSpellStart()
    self:DecayCast(self:GetCursorPosition())
end

function undying_decay_lua:DecayCast(point)
    local count = 1
    local undying_flesh_golem_lua = self:GetCaster():FindAbilityByName("undying_flesh_golem_lua")
    local special_bonus_unique_npc_dota_hero_undying_int12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int12") ~= nil
    if special_bonus_unique_npc_dota_hero_undying_int12 and undying_flesh_golem_lua:GetLevel() > 0 and undying_flesh_golem_lua.mod ~= nil and not undying_flesh_golem_lua.mod:IsNull() then
        count = 2
    end
    for i=1,count do
        local radius = self:GetSpecialValueFor("radius")
        local damage = self:GetSpecialValueFor("decay_damage")
        local duration = self:GetSpecialValueFor("decay_duration")
        self.special_bonus_unique_npc_dota_hero_undying_agi13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi13") ~= nil
        self:GetCaster():EmitSound("Hero_Undying.Decay.Cast")
        local decay_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_decay.vpcf", PATTACH_WORLDORIGIN, self:GetCaster())
        ParticleManager:SetParticleControl(decay_particle, 0, point)
        ParticleManager:SetParticleControl(decay_particle, 1, Vector(radius, 0, 0))
        ParticleManager:SetParticleControl(decay_particle, 2, self:GetCaster():GetAbsOrigin())
        ParticleManager:ReleaseParticleIndex(decay_particle)
        local target_count = 0
        for _, enemy in pairs(FindUnitsInRadius(self:GetCaster():GetTeamNumber(), point, nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)) do
            enemy:EmitSound("Hero_Undying.Decay.Target")
            self:GetCaster():EmitSound("Hero_Undying.Decay.Transfer")
            local strength_transfer_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_decay_strength_xfer.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy)
            ParticleManager:SetParticleControlEnt(strength_transfer_particle, 0, enemy, PATTACH_POINT_FOLLOW, "attach_hitloc", enemy:GetAbsOrigin(), true)
            ParticleManager:SetParticleControlEnt(strength_transfer_particle, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
            ParticleManager:ReleaseParticleIndex(strength_transfer_particle)
            if target_count < self:GetSpecialValueFor("max_targets_steal") then
                self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_decay_lua_buff_counter", {duration = duration })
            end
            -- self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_undying_decay_lua_buff", {duration = duration })
            self:GetCaster():CalculateStatBonus(true)
            ApplyDamageRDA({ victim = enemy, damage = damage, damage_type = self:GetAbilityDamageType(), damage_flags = DOTA_DAMAGE_FLAG_NONE, attacker = self:GetCaster(), ability = self})
            if self.special_bonus_unique_npc_dota_hero_undying_agi13 then
                self:GetCaster():PerformAttack(unit, true, true, true, true, false, false, true)
            end
            target_count = target_count + 1
        end
    end
end

modifier_undying_decay_lua_buff = class({})

function modifier_undying_decay_lua_buff:IsHidden() 
    return true 
end

function modifier_undying_decay_lua_buff:IsPurgable() 
    return false 
end

function modifier_undying_decay_lua_buff:GetAttributes() 
    return MODIFIER_ATTRIBUTE_MULTIPLE 
end

modifier_undying_decay_lua_buff_counter = class({})

function modifier_undying_decay_lua_buff_counter:OnCreated(kv)
	self.str_steal = self:GetAbility():GetSpecialValueFor("str_steal")
    if not IsServer() then
		return
	end
    local duration = kv.duration
	self.stack_table = {}
    self.decay_remove_count = {}
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
	table.insert(self.decay_remove_count, self.str_steal)
	self:SetStackCount(self.str_steal)
	Timers:CreateTimer(0.1,function()
        local duration = self:GetAbility():GetSpecialValueFor("decay_duration")
		while self.stack_table[1] and (self.stack_table[1] + duration) <= GameRules:GetDOTATime(false, false) do
            self:SetStackCount(self:GetStackCount() - self.decay_remove_count[1])
			table.remove(self.stack_table, 1)
			table.remove(self.decay_remove_count, 1)
		end
		return 0.1
	end)
	self:SetHasCustomTransmitterData( true )
end

function modifier_undying_decay_lua_buff_counter:OnRefresh()
    self.str_steal = self:GetAbility():GetSpecialValueFor("str_steal")
	if not IsServer() then
		return
	end
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
    table.insert(self.decay_remove_count, self.str_steal)
	self:SetStackCount(self.str_steal + self:GetStackCount())
	self:SendBuffRefreshToClients()
end

function modifier_undying_decay_lua_buff_counter:IsPurgable()  
    return false 
end

function modifier_undying_decay_lua_buff_counter:GetEffectName()
    return "particles/units/heroes/hero_undying/undying_decay_strength_buff.vpcf"
end

function modifier_undying_decay_lua_buff_counter:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
    }
end

function modifier_undying_decay_lua_buff_counter:GetModifierBonusStats_Strength()
    return self:GetStackCount()
end

function modifier_undying_decay_lua_buff_counter:GetModifierBonusStats_Agility()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_agi6") ~= nil and self:GetStackCount() or 0
end

function modifier_undying_decay_lua_buff_counter:GetModifierBonusStats_Intellect()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int13") ~= nil and self:GetStackCount() or 0
end

modifier_undying_decay_lua = class({})
--Classifications template
function modifier_undying_decay_lua:IsHidden()
    return true
end

function modifier_undying_decay_lua:IsDebuff()
    return false
end

function modifier_undying_decay_lua:IsPurgable()
    return false
end

function modifier_undying_decay_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_decay_lua:IsStunDebuff()
    return false
end

function modifier_undying_decay_lua:RemoveOnDeath()
    return false
end

function modifier_undying_decay_lua:DestroyOnExpire()
    return false
end

function modifier_undying_decay_lua:DestroyOnExpire()
    return false
end

function modifier_undying_decay_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(0.2)
end

function modifier_undying_decay_lua:OnIntervalThink()
    if self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() then
        local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
        if #units > 0 then
            self:GetAbility():DecayCast(self:GetParent():GetAbsOrigin())
            self:GetAbility():UseResources(true, false, false, true)
        end
    end
end

function modifier_undying_decay_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_undying_decay_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "decay_damage" then
			return 1
		end
	end
	return 0
end

function modifier_undying_decay_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "decay_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "decay_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_str9") then
                value = value + self:GetCaster():GetStrength()
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int9") then
                value = value + self:GetCaster():GetIntellect(true)
            end
            return value
		end
	end
	return 0
end