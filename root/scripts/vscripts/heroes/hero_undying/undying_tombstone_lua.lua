LinkLuaModifier("modifier_undying_tombstone_lua_hidden", "heroes/hero_undying/undying_tombstone_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_tombstone_lua_live_time", "heroes/hero_undying/undying_tombstone_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_tombstone_lua_aura_enemy", "heroes/hero_undying/undying_tombstone_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_tombstone_lua_aura_enemy_aura_effect", "heroes/hero_undying/undying_tombstone_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_undying_tombstone_lua_ally_buff", "heroes/hero_undying/undying_tombstone_lua.lua", LUA_MODIFIER_MOTION_NONE)

undying_tombstone_lua = class({})

function undying_tombstone_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function undying_tombstone_lua:OnSpellStart()
    if not self.tomb_unit then
        self.tomb_unit = CreateUnitByName("npc_dota_undying_tombstone", self:GetCursorPosition(), true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
        self.tomb_unit:AddNewModifier(self:GetCaster(), self, "modifier_undying_tombstone_lua_aura_enemy", {})
    else
        local m = self.tomb_unit:FindModifierByName("modifier_undying_tombstone_lua_hidden")
        if m then
            m:Destroy()
        end
    end
    self.tomb_unit:AddNewModifier(self:GetCaster(), self, "modifier_undying_tombstone_lua_live_time", {duration = self:GetSpecialValueFor("duration")})
    self.tomb_unit:SetAbsOrigin(self:GetCursorPosition())
end

modifier_undying_tombstone_lua_hidden = class({})
--Classifications template
function modifier_undying_tombstone_lua_hidden:IsHidden()
    return true
end

function modifier_undying_tombstone_lua_hidden:IsDebuff()
    return false
end

function modifier_undying_tombstone_lua_hidden:IsPurgable()
    return false
end

function modifier_undying_tombstone_lua_hidden:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_tombstone_lua_hidden:IsStunDebuff()
    return false
end

function modifier_undying_tombstone_lua_hidden:RemoveOnDeath()
    return false
end

function modifier_undying_tombstone_lua_hidden:DestroyOnExpire()
    return false
end

function modifier_undying_tombstone_lua_hidden:OnCreated()
    if not IsServer() then
        return
    end
    self:GetParent():AddNoDraw()
    self:GetParent():SetDayTimeVisionRange(0)
    self:GetParent():SetNightTimeVisionRange(0)
end

function modifier_undying_tombstone_lua_hidden:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetParent():RemoveNoDraw()
    self:GetParent():SetDayTimeVisionRange(600)
    self:GetParent():SetNightTimeVisionRange(600)
end

function modifier_undying_tombstone_lua_hidden:CheckState()
    return {
        [MODIFIER_STATE_ROOTED] = true,
        [MODIFIER_STATE_STUNNED] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
        [MODIFIER_STATE_MAGIC_IMMUNE] = true,
        [MODIFIER_STATE_SILENCED] = true,
        [MODIFIER_STATE_MUTED] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_UNSELECTABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
        [MODIFIER_STATE_OUT_OF_GAME] = true,
        [MODIFIER_STATE_MUTED] = true,
    }
end

modifier_undying_tombstone_lua_aura_enemy = class({})
--Classifications template
function modifier_undying_tombstone_lua_aura_enemy:IsHidden()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:IsDebuff()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:IsPurgable()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_tombstone_lua_aura_enemy:IsStunDebuff()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:RemoveOnDeath()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:DestroyOnExpire()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy:OnCreated()
    if not IsServer() then
        return
    end
    self.special_bonus_unique_npc_dota_hero_undying_str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_str13") ~= nil
    self.undying_decay_lua = self:GetCaster():FindAbilityByName("undying_decay_lua")
    self:SetStackCount(0)
    self:StartIntervalThink(1)
end

function modifier_undying_tombstone_lua_aura_enemy:OnRefresh()
    self:OnCreated()
end

function modifier_undying_tombstone_lua_aura_enemy:OnIntervalThink()
    if self:GetParent():HasModifier("modifier_undying_tombstone_lua_hidden") then
        return
    end
    local units1 = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor( "radius" ),  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor( "radius" ), DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    local buff = self:GetStackCount() / #units
    for _,unit in pairs(units) do
        local mod = unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_undying_tombstone_lua_ally_buff", {})
        mod:SetStackCount(buff)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_str10") then
        for _,unit in pairs(units1) do
            ApplyDamageRDA({
                victim = unit,
                attacker = self:GetCaster(),
                damage = self:GetStackCount(),
                damage_type = DAMAGE_TYPE_MAGICAL,
                damage_flags = 0,
                ability = self:GetAbility()
            })
        end
    end
    if self.undying_decay_lua:GetLevel() > 0 and self.special_bonus_unique_npc_dota_hero_undying_str13 then
        if #units1 > 0 then
            self.undying_decay_lua:DecayCast(units1[1]:GetAbsOrigin())
        else
            self.undying_decay_lua:DecayCast(self:GetParent():GetAbsOrigin() + Vector(RandomInt(-900,900),RandomInt(-900,900),0))
        end
    end
end
--------------------------------------------------------------------------------
-- Aura Effects
function modifier_undying_tombstone_lua_aura_enemy:IsAura()
    return not self:GetParent():HasModifier("modifier_undying_tombstone_lua_hidden")
end

function modifier_undying_tombstone_lua_aura_enemy:GetModifierAura()
    return "modifier_undying_tombstone_lua_aura_enemy_aura_effect"
end

function modifier_undying_tombstone_lua_aura_enemy:GetAuraRadius()
    return self:GetAbility():GetSpecialValueFor( "radius" )
end

function modifier_undying_tombstone_lua_aura_enemy:GetAuraDuration()
    return 0.5
end

function modifier_undying_tombstone_lua_aura_enemy:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_undying_tombstone_lua_aura_enemy:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_undying_tombstone_lua_aura_enemy:GetAuraSearchFlags()
    return 0
end

modifier_undying_tombstone_lua_aura_enemy_aura_effect = class({})
--Classifications template
function modifier_undying_tombstone_lua_aura_enemy_aura_effect:IsHidden()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:IsDebuff()
    return true
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:IsPurgable()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_tombstone_lua_aura_enemy_aura_effect:IsStunDebuff()
    return false
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:RemoveOnDeath()
    return true
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:DestroyOnExpire()
    return true
end

function modifier_undying_tombstone_lua_aura_enemy_aura_effect:OnDestroy()
    if not IsServer() then
        return
    end
    if not self:GetParent():IsAlive() then
        self:GetAuraOwner():FindModifierByName("modifier_undying_tombstone_lua_aura_enemy"):IncrementStackCount()
    end
end

modifier_undying_tombstone_lua_live_time = class({})
--Classifications template
function modifier_undying_tombstone_lua_live_time:IsHidden()
    return true
end

function modifier_undying_tombstone_lua_live_time:IsDebuff()
    return false
end

function modifier_undying_tombstone_lua_live_time:IsPurgable()
    return false
end

function modifier_undying_tombstone_lua_live_time:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_tombstone_lua_live_time:IsStunDebuff()
    return false
end

function modifier_undying_tombstone_lua_live_time:RemoveOnDeath()
    return false
end

function modifier_undying_tombstone_lua_live_time:DestroyOnExpire()
    return true
end

function modifier_undying_tombstone_lua_live_time:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_undying_tombstone_lua_hidden", {})
end

function modifier_undying_tombstone_lua_live_time:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_LIFETIME_FRACTION
    }
end

function modifier_undying_tombstone_lua_live_time:GetUnitLifetimeFraction( params )
	return ( ( self:GetDieTime() - GameRules:GetGameTime() ) / self:GetDuration() )
end

function modifier_undying_tombstone_lua_live_time:CheckState()
    return {
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
    }
end

modifier_undying_tombstone_lua_ally_buff = class({})
--Classifications template
function modifier_undying_tombstone_lua_ally_buff:IsHidden()
    return false
end

function modifier_undying_tombstone_lua_ally_buff:IsDebuff()
    return false
end

function modifier_undying_tombstone_lua_ally_buff:IsPurgable()
    return false
end

function modifier_undying_tombstone_lua_ally_buff:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_undying_tombstone_lua_ally_buff:IsStunDebuff()
    return false
end

function modifier_undying_tombstone_lua_ally_buff:RemoveOnDeath()
    return true
end

function modifier_undying_tombstone_lua_ally_buff:DestroyOnExpire()
    return true
end

function modifier_undying_tombstone_lua_ally_buff:OnCreated()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(1.5)
end

function modifier_undying_tombstone_lua_ally_buff:OnRefresh()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(1.5)
end

function modifier_undying_tombstone_lua_ally_buff:OnIntervalThink()
    self:Destroy()
end

function modifier_undying_tombstone_lua_ally_buff:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
    }
end

function modifier_undying_tombstone_lua_ally_buff:GetModifierBonusStats_Strength()
    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int10") == nil and self:GetStackCount() or 0
end

function modifier_undying_tombstone_lua_ally_buff:GetModifierBonusStats_Intellect()
    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_undying_int10") ~= nil and self:GetStackCount() or 0
end