queenofpain_shadow_strike_lua = class({})

LinkLuaModifier( "modifier_queenofpain_shadow_strike_lua", "heroes/hero_queenofpain/queenofpain_shadow_strike_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_queenofpain_shadow_strike_instic_lua", "heroes/hero_queenofpain/queenofpain_shadow_strike_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_queenofpain_shadow_strike_agi6_lua", "heroes/hero_queenofpain/queenofpain_shadow_strike_lua", LUA_MODIFIER_MOTION_NONE )

function queenofpain_shadow_strike_lua:GetIntrinsicModifierName()
	return "modifier_queenofpain_shadow_strike_instic_lua"
end

function queenofpain_shadow_strike_lua:GetHealthCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return math.min(65000, self:GetCaster():GetStrength() * 8)
	end
	return 0
end

function queenofpain_shadow_strike_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return 0
	end
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function queenofpain_shadow_strike_lua:GetAOERadius()
    return self:GetSpecialValueFor("radius")
end

function queenofpain_shadow_strike_lua:OnSpellStart()
    local info = {
		-- Target = target,
		Source = self:GetCaster(),
		Ability = self,	
		EffectName = "particles/units/heroes/hero_queenofpain/queen_shadow_strike.vpcf",
		iMoveSpeed = self:GetSpecialValueFor( "projectile_speed" ),
		bReplaceExisting = false,
		bProvidesVision = false,
	}
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCursorPosition(), nil, self:GetSpecialValueFor("radius"),  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,unit in pairs(units) do
        info.Target = unit
        ProjectileManager:CreateTrackingProjectile(info)
    end

	EmitSoundOn( "Hero_QueenOfPain.ShadowStrike", self:GetCaster() )
end

function queenofpain_shadow_strike_lua:OnProjectileHit( target, location )
	if target == nil or target:IsInvulnerable() then
		return
	end
	target:AddNewModifier(self:GetCaster(), self, "modifier_queenofpain_shadow_strike_lua", { duration = self:GetDuration() })	
end

modifier_queenofpain_shadow_strike_lua = class({})

function modifier_queenofpain_shadow_strike_lua:IsHidden()
	return false
end

function modifier_queenofpain_shadow_strike_lua:IsDebuff()
	return true
end

function modifier_queenofpain_shadow_strike_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_shadow_strike_lua:IsPurgable()
	return true
end

function modifier_queenofpain_shadow_strike_lua:OnCreated( kv )
	self.max_slow = self:GetAbility():GetSpecialValueFor( "movement_slow" )
	local init_damage = self:GetAbility():GetSpecialValueFor( "strike_damage" )
	self.tick_damage = self:GetAbility():GetSpecialValueFor( "duration_damage" )
	self.multip = 1
	self.max_armor_reduction = self:GetAbility():GetSpecialValueFor("max_armor_reduction")
	if self.max_armor_reduction > 0 then
		self.armor = 0
	end
	if not IsServer() then
        return
    end
    self.damageTable = {
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = init_damage,
        damage_type = DAMAGE_TYPE_MAGICAL,
        ability = self:GetAbility(),
    }
    ApplyDamageRDA( self.damageTable )

    self.damageTable.damage = self.tick_damage

	local interval = self:GetAbility():GetSpecialValueFor( "damage_interval" )
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int8") then
		interval = interval -0.5
	end
    self:StartIntervalThink(interval)
	self:SetHasCustomTransmitterData(true)

	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_shadow_strike_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
	ParticleManager:SetParticleControlEnt(effect_cast, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetOrigin(), true)
	self:AddParticle(effect_cast, false, false, -1, false, false)
end

function modifier_queenofpain_shadow_strike_lua:OnRefresh( kv )
	if not IsServer() then
		return
	end
	self.tick_damage = self:GetAbility():GetSpecialValueFor( "duration_damage" )
    self.damageTable.damage = self:GetAbility():GetSpecialValueFor( "strike_damage" )
    ApplyDamageRDA( self.damageTable )
    self.damageTable.damage = self.tick_damage * self.multip
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int11") then
		local abi = self:GetCaster():FindAbilityByName("queenofpain_scream_of_pain_lua")
		if abi and abi:GetLevel() > 0 then
			abi:OnSpellStart(self:GetParent())
		end
	end
end

function modifier_queenofpain_shadow_strike_lua:OnIntervalThink()
	ApplyDamageRDA( self.damageTable )
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int6") then
		self.damageTable.damage = self.tick_damage * self.multip
		self.multip = self.multip + 0.1
	end
	if self.armor then
		self.armor = math.min(self.armor + 1, self.max_armor_reduction)
		self:SendBuffRefreshToClients()
	end
end

function modifier_queenofpain_shadow_strike_lua:AddCustomTransmitterData()
	return {
		armor = self.armor,
	}
end

function modifier_queenofpain_shadow_strike_lua:HandleCustomTransmitterData( data )
	self.armor = data.armor
end

function modifier_queenofpain_shadow_strike_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_queenofpain_shadow_strike_lua:GetModifierMoveSpeedBonus_Percentage()
	return self.max_slow * (self:GetRemainingTime()/self:GetDuration())
end

function modifier_queenofpain_shadow_strike_lua:CustomOnAttackLanded(data)
	if data.target == self:GetParent() and self:GetAbility():GetSpecialValueFor("agi6") == 1 then
		data.attacker:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_queenofpain_shadow_strike_agi6_lua", { duration = 1 })
	end
end

function modifier_queenofpain_shadow_strike_lua:GetModifierPhysicalArmorBonus()
	if self.armor then
		return -self.armor
	end
end

modifier_queenofpain_shadow_strike_agi6_lua = class({})
--Classifications template
function modifier_queenofpain_shadow_strike_agi6_lua:IsHidden()
	return false
end

function modifier_queenofpain_shadow_strike_agi6_lua:IsDebuff()
	return false
end

function modifier_queenofpain_shadow_strike_agi6_lua:IsPurgable()
	return true
end

function modifier_queenofpain_shadow_strike_agi6_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_queenofpain_shadow_strike_agi6_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_shadow_strike_agi6_lua:RemoveOnDeath()
	return true
end

function modifier_queenofpain_shadow_strike_agi6_lua:DestroyOnExpire()
	return true
end

function modifier_queenofpain_shadow_strike_agi6_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_queenofpain_shadow_strike_agi6_lua:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("attack_speed")
end

modifier_queenofpain_shadow_strike_instic_lua = class({})
--Classifications template
function modifier_queenofpain_shadow_strike_instic_lua:IsHidden()
	return true
end

function modifier_queenofpain_shadow_strike_instic_lua:IsDebuff()
	return false
end

function modifier_queenofpain_shadow_strike_instic_lua:IsPurgable()
	return false
end

function modifier_queenofpain_shadow_strike_instic_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_queenofpain_shadow_strike_instic_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_shadow_strike_instic_lua:RemoveOnDeath()
	return false
end

function modifier_queenofpain_shadow_strike_instic_lua:DestroyOnExpire()
	return false
end

function modifier_queenofpain_shadow_strike_instic_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(1)
end

function modifier_queenofpain_shadow_strike_instic_lua:OnIntervalThink()
	if self:GetAbility():GetSpecialValueFor("agi13") == 1 then
		if self:GetCaster():GetIntellect(true) > self:GetCaster():GetStrength() and self:GetCaster():GetIntellect(true) > self:GetCaster():GetAgility() then
			self:GetCaster():SetPrimaryAttribute(DOTA_ATTRIBUTE_INTELLECT)
		elseif self:GetCaster():GetStrength() > self:GetCaster():GetIntellect(true) and self:GetCaster():GetStrength() > self:GetCaster():GetAgility() then
			self:GetCaster():SetPrimaryAttribute(DOTA_ATTRIBUTE_STRENGTH)
		elseif self:GetCaster():GetAgility() > self:GetCaster():GetStrength() and self:GetCaster():GetAgility() > self:GetCaster():GetIntellect(true) then
			self:GetCaster():SetPrimaryAttribute(DOTA_ATTRIBUTE_AGILITY)
		else
			self:GetCaster():SetPrimaryAttribute(DOTA_ATTRIBUTE_ALL)
		end
	end
end

function modifier_queenofpain_shadow_strike_instic_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE
	}
end

function modifier_queenofpain_shadow_strike_instic_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "strike_damage" then
			return 1
		end
	end
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "duration_damage" then
			return 1
		end
	end
	return 0
end

function modifier_queenofpain_shadow_strike_instic_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "strike_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "strike_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int9") then
                value = value + self:GetCaster():GetIntellect(true) * 0.75
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.35
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 0.65
            end
            return value
		end
	end
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "duration_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "duration_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int9") then
                value = value + self:GetCaster():GetIntellect(true) * 0.75
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.35
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 0.65
            end
            return value
		end
	end
	return 0
end