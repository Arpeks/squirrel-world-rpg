queenofpain_scream_of_pain_lua = class({})

LinkLuaModifier( "modifier_queenofpain_scream_of_pain_lua", "heroes/hero_queenofpain/queenofpain_scream_of_pain_lua", LUA_MODIFIER_MOTION_NONE )

function queenofpain_scream_of_pain_lua:GetIntrinsicModifierName()
	return "modifier_queenofpain_scream_of_pain_lua"
end

function queenofpain_scream_of_pain_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_agi8") then
		return self.BaseClass.GetBehavior(self) + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return self.BaseClass.GetBehavior(self)
end

function queenofpain_scream_of_pain_lua:GetHealthCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return math.min(65000, self:GetCaster():GetStrength() * 8)
	end
	return 0
end

function queenofpain_scream_of_pain_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return 0
	end
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function queenofpain_scream_of_pain_lua:GetCastRange(vLocation, hTarget)
	return self:GetSpecialValueFor("area_of_effect")
end

function queenofpain_scream_of_pain_lua:OnSpellStart(parent)
	local radius = self:GetSpecialValueFor("area_of_effect")
	self.screamDamage = self:GetSpecialValueFor("damage")
	self.agi8 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_agi8") ~= nil
	-- self.int8 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int8") ~= nil
	local unit = parent or self:GetCaster()
	local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),unit:GetOrigin(),nil,radius,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)

	local info = {
		-- Target = target,
		Source = unit,
		Ability = self,	
		EffectName = "particles/units/heroes/hero_queenofpain/queen_scream_of_pain.vpcf",
		iMoveSpeed = self:GetSpecialValueFor("projectile_speed"),
		vSourceLoc= unit:GetOrigin(),
		bDodgeable = false,
		bVisibleToEnemies = true,
		bReplaceExisting = false,
		bProvidesVision = false,
	}

	for _,enemy in pairs(enemies) do
		info.Target = enemy
		ProjectileManager:CreateTrackingProjectile(info)
	end
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_scream_of_pain_owner.vpcf", PATTACH_ABSORIGIN, unit )
	ParticleManager:ReleaseParticleIndex( effect_cast )

	EmitSoundOn( "Hero_QueenOfPain.ScreamOfPain", unit )
end

function queenofpain_scream_of_pain_lua:OnProjectileHit( target, location )
	ApplyDamageRDA({
		victim = target,
		attacker = self:GetCaster(),
		damage = self.screamDamage,
		damage_type = DAMAGE_TYPE_MAGICAL,
		ability = self
	})
	if self.agi8 then
		self:GetCaster():PerformAttack(target, true, false, true, true, false, false, true)
	end
end

modifier_queenofpain_scream_of_pain_lua = class({})
--Classifications template
function modifier_queenofpain_scream_of_pain_lua:IsHidden()
	return true
end

function modifier_queenofpain_scream_of_pain_lua:IsDebuff()
	return false
end

function modifier_queenofpain_scream_of_pain_lua:IsPurgable()
	return false
end

function modifier_queenofpain_scream_of_pain_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_queenofpain_scream_of_pain_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_scream_of_pain_lua:RemoveOnDeath()
	return false
end

function modifier_queenofpain_scream_of_pain_lua:DestroyOnExpire()
	return false
end

function modifier_queenofpain_scream_of_pain_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(0.1)
end

function modifier_queenofpain_scream_of_pain_lua:OnIntervalThink()
	if self:GetAbility():IsFullyCastable() and self:GetAbility():GetAutoCastState() then
		self:OnSpellStart()
		self:UseResources(true, false, false, true)
	end
end

function modifier_queenofpain_scream_of_pain_lua:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE
	}
end

function modifier_queenofpain_scream_of_pain_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "area_of_effect" then
			return 1
		end
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_queenofpain_scream_of_pain_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "area_of_effect" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "area_of_effect", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_agi8") then
                value = self:GetParent():Script_GetAttackRange() + 100
            end
            return value
		end
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str9") then
                value = value + self:GetCaster():GetStrength() * 0.75
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.35
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 0.65
            end
            return value
		end
	end
	return 0
end

function modifier_queenofpain_scream_of_pain_lua:CustomOnAttack(data)
	if data.attacker == self:GetParent() and RollPercentage(self:GetAbility():GetSpecialValueFor("agi9")) then
		self:GetAbility():OnSpellStart()
	end
end