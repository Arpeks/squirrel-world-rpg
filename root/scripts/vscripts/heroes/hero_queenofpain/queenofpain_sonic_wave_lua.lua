queenofpain_sonic_wave_lua = class({})

LinkLuaModifier( "modifier_queenofpain_sonic_wave_lua", "heroes/hero_queenofpain/queenofpain_sonic_wave_lua", LUA_MODIFIER_MOTION_NONE )

function queenofpain_sonic_wave_lua:GetIntrinsicModifierName()
	return "modifier_queenofpain_sonic_wave_lua"
end

function queenofpain_sonic_wave_lua:OnUpgrade()
	if self:GetLevel() == 1 then
		self:RefreshCharges()
	end
end

function queenofpain_sonic_wave_lua:GetCooldown(level)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int10") then
		return 0.1
	end
	return self.BaseClass.GetCooldown(self, level)
end

function queenofpain_sonic_wave_lua:GetHealthCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return math.min(65000, self:GetCaster():GetStrength() * 20)
	end
	return 0
end

function queenofpain_sonic_wave_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return 0
	end
	return 150 + math.min(65000, self:GetCaster():GetIntellect(true) / 50)
end

function queenofpain_sonic_wave_lua:OnSpellStart(point, damage)
    local forward = ((point or self:GetCursorPosition())-self:GetCaster():GetOrigin()):Normalized()
	-- self.int11 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int11") ~= nil
	
	local info = {
    	Source = self:GetCaster(),
		Ability = self,
    	vSpawnOrigin = self:GetCaster():GetAbsOrigin(),
    	
    	EffectName = "particles/units/heroes/hero_queenofpain/queen_sonic_wave.vpcf",
    	fDistance = self:GetSpecialValueFor("distance"),
    	fStartRadius = self:GetSpecialValueFor("starting_aoe"),
    	fEndRadius = self:GetSpecialValueFor("final_aoe"),
    	-- bHasFrontalCone = false,
		vVelocity = forward * self:GetSpecialValueFor("speed"),
    	
		bDeleteOnHit = false,
    	bReplaceExisting = false,
    	-- fExpireTime = GameRules:GetGameTime() + 10.0,
    	
    	iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
    	iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_NONE,
    	iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		
		bProvidesVision = false,
		ExtraData = {damage = damage or self:GetSpecialValueFor("damage")}
	}
	ProjectileManager:CreateLinearProjectile(info)
	-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int10") then
	-- 	for i=1,3 do
	-- 		local new_direction = RotatePosition(Vector(0,0,0), QAngle(0,i*90), forward)
	-- 		info.vVelocity = new_direction * self:GetSpecialValueFor("speed")
	-- 		ProjectileManager:CreateLinearProjectile(info)
	-- 	end
	-- end
	-- local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_sonic_wave.vpcf", PATTACH_ABSORIGIN, self:GetCaster() )
	-- ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
	-- ParticleManager:SetParticleControlForward( effect_cast, 0, forward )
	-- ParticleManager:SetParticleControl( effect_cast, 1, forward * self:GetSpecialValueFor("speed") )
	-- ParticleManager:ReleaseParticleIndex( effect_cast )
	EmitSoundOn( "Hero_QueenOfPain.SonicWave", self:GetCaster() )
end

function queenofpain_sonic_wave_lua:OnProjectileHit_ExtraData(target, location, ExtraData)
    if not target then return end
	ApplyDamageRDA({
		victim = target,
		attacker = self:GetCaster(),
		damage = ExtraData.damage,
		damage_type = DAMAGE_TYPE_MAGICAL,
		ability = self
	})
	if self.int11 then
		local abi = self:GetCaster():FindAbilityByName("queenofpain_shadow_strike_lua")
		if abi and abi:GetLevel() > 0 then
			abi:OnProjectileHit( target, location )
		end
	end
end

modifier_queenofpain_sonic_wave_lua = class({})
--Classifications template
function modifier_queenofpain_sonic_wave_lua:IsHidden()
	return true
end

function modifier_queenofpain_sonic_wave_lua:IsDebuff()
	return false
end

function modifier_queenofpain_sonic_wave_lua:IsPurgable()
	return false
end

function modifier_queenofpain_sonic_wave_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_queenofpain_sonic_wave_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_sonic_wave_lua:RemoveOnDeath()
	return false
end

function modifier_queenofpain_sonic_wave_lua:DestroyOnExpire()
	return false
end

function modifier_queenofpain_sonic_wave_lua:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,

		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE
	}
end

function modifier_queenofpain_sonic_wave_lua:CustomOnAttackLanded(data)
	if data.attacker == self:GetParent() and self:GetAbility():GetSpecialValueFor("agi11") == 1 and RollPercentage(15) then
		self:GetAbility():OnSpellStart(data.target:GetAbsOrigin(), self:GetAbility():GetSpecialValueFor("damage")*0.25)
	end
end

function modifier_queenofpain_sonic_wave_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
		if data.ability_special_value == "AbilityCharges" then
			return 1
		end
	end
	return 0
end

function modifier_queenofpain_sonic_wave_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int7") then
                value = value + self:GetCaster():GetIntellect(true) * 2.0
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.35
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 0.65
            end
            return value
		end
		if data.ability_special_value == "AbilityCharges" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "AbilityCharges", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int10") then
                value = 3
            end
            return value
		end
	end
	return 0
end

special_bonus_unique_npc_dota_hero_queenofpain_int10 = class({})

function special_bonus_unique_npc_dota_hero_queenofpain_int10:OnUpgrade()
    self:GetCaster():FindAbilityByName("queenofpain_sonic_wave_lua"):RefreshCharges()
end