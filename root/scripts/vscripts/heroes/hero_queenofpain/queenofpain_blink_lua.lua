queenofpain_blink_lua = class({})

LinkLuaModifier( "modifier_queenofpain_blink_lua", "heroes/hero_queenofpain/queenofpain_blink_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_queenofpain_blink_str8_lua", "heroes/hero_queenofpain/queenofpain_blink_lua", LUA_MODIFIER_MOTION_NONE )

function queenofpain_blink_lua:GetIntrinsicModifierName()
    return "modifier_queenofpain_blink_lua"
end

function queenofpain_blink_lua:GetHealthCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return math.min(65000, self:GetCaster():GetStrength() * 8)
	end
	return 0
end

function queenofpain_blink_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str8") then
		return 0
	end
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function queenofpain_blink_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str6") then
        return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE + DOTA_ABILITY_BEHAVIOR_IGNORE_PSEUDO_QUEUE + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
    end
    return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES + DOTA_ABILITY_BEHAVIOR_AOE
end

function queenofpain_blink_lua:OnSpellStart()
    if GridNav:CanFindPath(self:GetCaster():GetOrigin(), self:GetCursorPosition()) == false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        if player then
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_cast_here" })
            -- self:EndCooldown()
            -- self:RefundManaCost()
            return
        end
    end

    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str6") then
        self:GetCaster():Purge(false, true, false, true, false)
    end

    local radius = self:GetSpecialValueFor("radius")
	local max_range = self:GetCastRange(self:GetCursorPosition(), nil) + self:GetCaster():GetCastRangeBonus()
    local damage = self:GetSpecialValueFor("damage")
	local direction = (self:GetCursorPosition() - self:GetCaster():GetOrigin())
	if direction:Length2D() > max_range then
		direction = direction:Normalized() * max_range
	end

    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, radius,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        ApplyDamageRDA({
            victim = unit,
            attacker = self:GetCaster(),
            damage = damage,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self
        })
    end

	local effect_cast_a = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_blink_start.vpcf", PATTACH_ABSORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl(effect_cast_a, 0, self:GetCaster():GetOrigin())
	ParticleManager:SetParticleControlForward(effect_cast_a, 0, direction:Normalized())
	ParticleManager:SetParticleControl(effect_cast_a, 1, self:GetCaster():GetOrigin() + direction)
	ParticleManager:ReleaseParticleIndex(effect_cast_a)

    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_queenofpain/queen_blink_shard_start.vpcf", PATTACH_ABSORIGIN, self:GetCaster())
    ParticleManager:SetParticleControl(pcf, 0, self:GetCaster():GetOrigin())
    ParticleManager:SetParticleControl(pcf, 1, Vector(radius,0,0))
    ParticleManager:SetParticleControl(pcf, 2, Vector(radius,0,0))
	ParticleManager:ReleaseParticleIndex(pcf)

	EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_QueenOfPain.Blink_out", self:GetCaster() )

	FindClearSpaceForUnit( self:GetCaster(), self:GetCaster():GetOrigin() + direction, true )

    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, radius,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        ApplyDamageRDA({
            victim = unit,
            attacker = self:GetCaster(),
            damage = damage,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self
        })
    end

	local effect_cast_b = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_blink_end.vpcf", PATTACH_ABSORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl(effect_cast_b, 0, self:GetCaster():GetOrigin())
	ParticleManager:SetParticleControlForward(effect_cast_b, 0, direction:Normalized())
	ParticleManager:ReleaseParticleIndex(effect_cast_b)

    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_queenofpain/queen_blink_shard_end.vpcf", PATTACH_ABSORIGIN, self:GetCaster())
    ParticleManager:SetParticleControl(pcf, 0, self:GetCaster():GetOrigin())
    ParticleManager:SetParticleControl(pcf, 1, Vector(radius,0,0))
    ParticleManager:SetParticleControl(pcf, 2, Vector(radius,0,0))
	ParticleManager:ReleaseParticleIndex(pcf)

	EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_QueenOfPain.Blink_in", self:GetCaster() )
end

modifier_queenofpain_blink_lua = class({})
--Classifications template
function modifier_queenofpain_blink_lua:IsHidden()
	return self:GetStackCount() < 0
end

function modifier_queenofpain_blink_lua:IsDebuff()
	return false
end

function modifier_queenofpain_blink_lua:IsPurgable()
	return false
end

function modifier_queenofpain_blink_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_queenofpain_blink_lua:IsStunDebuff()
	return false
end

function modifier_queenofpain_blink_lua:RemoveOnDeath()
	return false
end

function modifier_queenofpain_blink_lua:DestroyOnExpire()
	return false
end

function modifier_queenofpain_blink_lua:OnCreated()
    self.str_calc = 0
    if not IsServer() then
        return
    end
    -- if self:GetAbility():GetSpecialValueFor("agi12") == 1 then
    --     self.agi12 = self:GetCaster():GetSpellAmplification(false) * 100
    --     self:SetHasCustomTransmitterData( true )
    --     self:StartIntervalThink(1)
    -- end
end

function modifier_queenofpain_blink_lua:OnIntervalThink()
    -- self.agi12 = self:GetCaster():GetSpellAmplification(false) * 100
    -- self:SendBuffRefreshToClients()
end


-- function modifier_queenofpain_blink_lua:AddCustomTransmitterData()
--     return {
--         agi12 = self.agi12,
--     }
-- end

-- function modifier_queenofpain_blink_lua:HandleCustomTransmitterData( data )
--     self.agi12 = data.agi12
-- end

function modifier_queenofpain_blink_lua:DeclareFunctions()
	return {
        -- MODIFIER_EVENT_ON_DEATH,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        -- MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,

		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE
	}
end

function modifier_queenofpain_blink_lua:CustomOnAttackLanded(data)
    local damage = 0
	if data.attacker == self:GetParent() and self:GetAbility():GetSpecialValueFor("agi12") == 1 then
        if self:GetCaster():GetIntellect(true) >= self:GetCaster():GetStrength() and self:GetCaster():GetIntellect(true) >= self:GetCaster():GetAgility() then
            damage = damage + self:GetCaster():GetIntellect(true) * 0.15
        elseif self:GetCaster():GetStrength() >= self:GetCaster():GetIntellect(true) and self:GetCaster():GetStrength() >= self:GetCaster():GetAgility() then
            damage = damage + self:GetCaster():GetStrength() * 0.15
        elseif self:GetCaster():GetAgility() >= self:GetCaster():GetStrength() and self:GetCaster():GetAgility() >= self:GetCaster():GetIntellect(true) then
            damage = damage + self:GetCaster():GetAgility() * 0.15
        end
	end
    if data.attacker == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_agi13") then
        damage = damage + self:GetCaster():GetBaseDamageMax() * 0.2
    end
    if damage > 0 then
        ApplyDamageRDA({victim = data.target, attacker = self:GetCaster(), damage = damage, damage_type = DAMAGE_TYPE_MAGICAL, ability = self:GetAbility()})
    end
end

function modifier_queenofpain_blink_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_queenofpain_blink_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str10") then
                value = value + self:GetCaster():GetStrength()
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.35
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 0.65
            end
            return value
		end
	end
	return 0
end

function modifier_queenofpain_blink_lua:GetModifierBonusStats_Strength()
    return self:GetAbility():GetSpecialValueFor("str12") == 1 and self:GetStackCount() or 0
end

function modifier_queenofpain_blink_lua:GetModifierBonusStats_Intellect()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_queenofpain_str13") then
        return self:GetCaster():GetStrength()
    end
    return 0
end

function modifier_queenofpain_blink_lua:CustomOnDeath(data)
    if data.attacker == self:GetParent() then
        if data.inflictor and data.inflictor:GetCaster() == self:GetParent() and not data.inflictor:IsItem() then
            self.str_calc = self.str_calc + 0.2
            self:SetStackCount(self.str_calc)
        end
    end
end

function modifier_queenofpain_blink_lua:GetModifierPhysicalArmorBonus()
    -- return self:GetAbility():GetSpecialValueFor("str9") == 1 and self:GetStackCount() / 4 or 0
end

function modifier_queenofpain_blink_lua:GetModifierTotalDamageOutgoing_Percentage()
    -- if self:GetAbility():GetSpecialValueFor("str13") == 1 then
    --     if self:GetCaster():GetStrength() > ((self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility()) * 5) then
    --         return self:GetCaster():GetStrength() - ((self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility()) * 5) / 100
    --     end
    -- end
    return 0
end

function modifier_queenofpain_blink_lua:GetModifierPreAttack_BonusDamage()
    -- if self:GetAbility():GetSpecialValueFor("agi12") == 1 then
    --     return self.agi12
    -- end
end

function modifier_queenofpain_blink_lua:CustomOnAbilityFullyCast(data)
    if data.unit==self:GetParent() and data.ability and not data.ability:IsItem() then
        local stack = self:GetAbility():GetHealthCost(1) / 5
        if stack > 0 then
            if self:GetCaster():FindModifierByName("modifier_queenofpain_blink_str11_lua") then
                stack = stack * 3
            end
            local mod = self:GetCaster():FindModifierByName("modifier_queenofpain_blink_str8_lua")
            if mod then
                mod:SetStackCount(mod:GetStackCount() + stack)
            else
                local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_queenofpain_blink_str8_lua", {duration = 10})
                mod:SetStackCount(mod:GetStackCount() + stack)
            end
        end
    end
end

modifier_queenofpain_blink_str8_lua = class({})
--Classifications template
function modifier_queenofpain_blink_str8_lua:IsHidden()
    return false
end

function modifier_queenofpain_blink_str8_lua:IsDebuff()
    return false
end

function modifier_queenofpain_blink_str8_lua:IsPurgable()
    return false
end

function modifier_queenofpain_blink_str8_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_queenofpain_blink_str8_lua:IsStunDebuff()
    return false
end

function modifier_queenofpain_blink_str8_lua:RemoveOnDeath()
    return true
end

function modifier_queenofpain_blink_str8_lua:DestroyOnExpire()
    return true
end

function modifier_queenofpain_blink_str8_lua:OnStackCountChanged(iStackCount)
    if not IsServer() then
        return
    end
    self:GetParent():CalculateStatBonus(false)
end

function modifier_queenofpain_blink_str8_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS
    }
end

function modifier_queenofpain_blink_str8_lua:GetModifierBonusStats_Strength()
    return self:GetStackCount()
end