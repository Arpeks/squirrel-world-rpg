"DOTAAbilities"
{
	"special_bonus_unique_npc_dota_hero_queenofpain_str6"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_queenofpain_str7"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_str8"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_str9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_str10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_str11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_queenofpain_agi6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi7"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_queenofpain_int6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_int7"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_int8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_int9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_int10"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_queenofpain/queenofpain_sonic_wave_lua"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_int11"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}

	//last
	"special_bonus_unique_npc_dota_hero_queenofpain_str12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi12"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_queenofpain_int12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	//last
	"special_bonus_unique_npc_dota_hero_queenofpain_str13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_queenofpain_agi13"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_queenofpain_int13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
}