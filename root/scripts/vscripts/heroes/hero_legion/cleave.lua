npc_dota_hero_mars_agi10 = class({})
LinkLuaModifier( "modifier_mars_cleave", "heroes/hero_mars/cleave", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------
function npc_dota_hero_mars_agi10:GetIntrinsicModifierName()
	return "modifier_mars_cleave"
end

-------------------------------------------
-------------------------------------------

modifier_mars_cleave = class({})

function modifier_mars_cleave:IsHidden()
	return true
end

function modifier_mars_cleave:IsPurgable()
	return false
end

function modifier_mars_cleave:OnCreated( kv )
end

function modifier_mars_cleave:DeclareFunctions()
	local funcs =
	{
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		
	}
	return funcs
end

function modifier_mars_cleave:CustomOnAttackLanded(keys)
	if keys.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (100/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )

			end
		end
	end
end