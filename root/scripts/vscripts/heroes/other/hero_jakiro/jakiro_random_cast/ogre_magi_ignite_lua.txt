"DOTAAbilities"
{
	"ogre_magi_ignite_lua"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"lua_abilities/ogre_magi_ignite_lua/ogre_magi_ignite_lua"
		"AbilityTextureName"			"ogre_magi_ignite_lua"
		"FightRecapLevel"				"1"
		"MaxLevel"						"4"
		"precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_ogre_magi.vsndevts"
			"particle"	"particles/units/heroes/hero_ogre_magi/ogre_magi_ignite.vpcf"
			"particle"	"particles/units/heroes/hero_ogre_magi/ogre_magi_ignite_debuff.vpcf"
		}
		
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_AOE"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"

		"AbilityCastRange"				"700 800 900 1000"
		"AbilityCastPoint"				"0.45"

		"AbilityCooldown"				"15"
		"AbilityManaCost"				"90"

		"AbilityValues"
		{
			"duration"
			{
				"value"                   "5 6 7 8"
				"affected_by_aoe_increase" "1"
			}
			"burn_damage"
			{
				"value"                   "26 34 42 50"
			}
			"slow_movement_speed_pct"
			{
				"value"                   "-20 -22 -24 -26"
			}
			"projectile_speed"
			{
				"value"                   "1000"
			}
			"multicast_delay"
			{
				"value"                   "0.5"
			}
		}
	}
}
