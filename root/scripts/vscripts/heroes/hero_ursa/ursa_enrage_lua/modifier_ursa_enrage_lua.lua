modifier_ursa_enrage_lua = class({})

--------------------------------------------------------------------------------

function modifier_ursa_enrage_lua:IsHidden()
	return false
end

function modifier_ursa_enrage_lua:IsDebuff()
	return false
end

function modifier_ursa_enrage_lua:IsPurgable()
	return false
end
--------------------------------------------------------------------------------

function modifier_ursa_enrage_lua:OnCreated( kv )
	-- get reference
	self.damage_reduction = self:GetAbility():GetSpecialValueFor("damage_reduction")
	self.status_resistance = self:GetAbility():GetSpecialValueFor("status_resistance")
	self.stack_multiplier = 1
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ursa_str13") then
		self.stack_multiplier = 2
	end

	-- change fury swipes modifier
	if IsServer() then
		local modifier = self:GetParent():FindModifierByNameAndCaster("modifier_ursa_fury_swipes_lua", self:GetAbility():GetCaster())
		if modifier~=nil then
			modifier.damage_per_stack = modifier:GetAbility():GetSpecialValueFor("damage_per_stack") * self.stack_multiplier
		end
        self:GetParent():SetRenderColor(255,0,0)
	end
end

function modifier_ursa_enrage_lua:OnRefresh( kv )
	-- get reference
	self.damage_reduction = self:GetAbility():GetSpecialValueFor("damage_reduction")
    self.status_resistance = self:GetAbility():GetSpecialValueFor("status_resistance")
	self.stack_multiplier = 1
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ursa_str13") then
		self.stack_multiplier = 2
	end

	-- change fury swipes modifier
	if IsServer() then
		local modifier = self:GetParent():FindModifierByNameAndCaster("modifier_ursa_fury_swipes_lua", self:GetAbility():GetCaster())
		if modifier~=nil then
			modifier.damage_per_stack = modifier:GetAbility():GetSpecialValueFor("damage_per_stack") * self.stack_multiplier
		end
	end
end

function modifier_ursa_enrage_lua:OnDestroy( kv )
	-- get reference
	self.damage_reduction = self:GetAbility():GetSpecialValueFor("damage_reduction")
    self.status_resistance = self:GetAbility():GetSpecialValueFor("status_resistance")
	self.stack_multiplier = self:GetAbility():GetSpecialValueFor("enrage_multiplier")

	-- change fury swipes modifier
	if IsServer() then
		local modifier = self:GetParent():FindModifierByNameAndCaster("modifier_ursa_fury_swipes_lua", self:GetAbility():GetCaster())
		if modifier~=nil then
			modifier.damage_per_stack = modifier:GetAbility():GetSpecialValueFor("damage_per_stack")
		end
        self:GetParent():SetRenderColor(255,255,255)
	end
end
--------------------------------------------------------------------------------

function modifier_ursa_enrage_lua:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
        MODIFIER_PROPERTY_STATUS_RESISTANCE,

		MODIFIER_PROPERTY_MODEL_SCALE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_ursa_enrage_lua:GetModifierIncomingDamage_Percentage( params )
	return -self.damage_reduction
end

function modifier_ursa_enrage_lua:GetModifierStatusResistance( params )
	return self.status_resistance
end

function modifier_ursa_enrage_lua:GetModifierModelScale( params )
	return 30
end

--------------------------------------------------------------------------------
-- Graphics & Animations
function modifier_ursa_enrage_lua:GetEffectName()
	return "particles/units/heroes/hero_ursa/ursa_enrage_buff.vpcf"
end

function modifier_ursa_enrage_lua:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_ursa_enrage_lua:CustomOnAttackLanded( params )
	if not IsServer() then return end
	if self:GetParent() ~= params.attacker then return end
	if not self:GetParent():FindAbilityByName("npc_dota_hero_ursa_agi9") then return end
	if params.target ~= nil and params.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
		if params.attacker == self:GetParent() then
			if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
				if params.target ~= nil and params.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
					local direction = params.target:GetOrigin()-self:GetParent():GetOrigin()
					direction.z = 0
					direction = direction:Normalized()
					local range = self:GetParent():GetOrigin() + direction*650/2
	
					local facing_direction = params.attacker:GetAnglesAsVector().y
					local cleave_targets = FindUnitsInRadius(params.attacker:GetTeamNumber(),params.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
					for _,target in pairs(cleave_targets) do
						if target ~= params.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
							local attacker_vector = (target:GetOrigin() - params.attacker:GetOrigin())
							local attacker_direction = VectorToAngles( attacker_vector ).y
							local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
							if angle_diff < 45 then
								ApplyDamageRDA({
									victim = target,
									attacker = params.attacker,
									damage = params.damage * (100/100),
									damage_type = DAMAGE_TYPE_PHYSICAL,
									damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
									ability = self:GetAbility()
								})
							end
						end
					end
					local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
					ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
					ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
					ParticleManager:ReleaseParticleIndex( effect_cast )
	
				end
			end
		end
	end
end