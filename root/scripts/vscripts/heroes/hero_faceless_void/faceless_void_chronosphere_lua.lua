faceless_void_chronosphere_lua = class({})

function faceless_void_chronosphere_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 30)
end

function faceless_void_chronosphere_lua:OnSpellStart()
	CreateModifierThinker(self:GetCaster(), self, "modifier_faceless_void_chronosphere_lua", {duration = self:GetSpecialValueFor("duration")}, self:GetCursorPosition(), self:GetCaster():GetTeamNumber(), false)
end

function faceless_void_chronosphere_lua:GetIntrinsicModifierName()
	return "modifier_faceless_void_chronosphere_lua_inst"
end

function faceless_void_chronosphere_lua:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

LinkLuaModifier( "modifier_faceless_void_chronosphere_lua_inst", "heroes/hero_faceless_void/faceless_void_chronosphere_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_chronosphere_lua", "heroes/hero_faceless_void/faceless_void_chronosphere_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_chronosphere_lua_aura", "heroes/hero_faceless_void/faceless_void_chronosphere_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_faceless_void_chronosphere_lua = class({})
--Classifications template
function modifier_faceless_void_chronosphere_lua:IsHidden()
	return false
end

function modifier_faceless_void_chronosphere_lua:IsDebuff()
	return true
end

function modifier_faceless_void_chronosphere_lua:IsPurgable()
	return false
end

function modifier_faceless_void_chronosphere_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_chronosphere_lua:IsStunDebuff()
	return true
end

function modifier_faceless_void_chronosphere_lua:RemoveOnDeath()
	return true
end

function modifier_faceless_void_chronosphere_lua:DestroyOnExpire()
	return true
end

function modifier_faceless_void_chronosphere_lua:OnCreated(data)
	if not IsServer() then
		return
	end
	local caster = self:GetCaster()
	local point = self:GetParent():GetAbsOrigin()
	local radius = self:GetAbility():GetSpecialValueFor("radius")

	local nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere_surface.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx, 0, point)
	ParticleManager:SetParticleControl(nfx, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx, false, false, -1, false, false)

	local nfx2 = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere_rim.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx2, 0, point)
	ParticleManager:SetParticleControl(nfx2, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx2, false, false, -1, false, false)

	local nfx3 = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx3, 0, point)
	ParticleManager:SetParticleControl(nfx3, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx3, false, false, -1, false, false)

	EmitSoundOn( "Hero_FacelessVoid.Chronosphere", self:GetParent() )
end

function modifier_faceless_void_chronosphere_lua:OnDestroy()
	if not IsServer() then
		return
	end
	UTIL_Remove(self:GetParent())
end
--------------------------------------------------------------------------------
-- Aura Effects
function modifier_faceless_void_chronosphere_lua:IsAura()
	return true
end

function modifier_faceless_void_chronosphere_lua:GetModifierAura()
	return "modifier_faceless_void_chronosphere_lua_aura"
end

function modifier_faceless_void_chronosphere_lua:GetAuraRadius()
	return self:GetAbility():GetSpecialValueFor("radius")
end

function modifier_faceless_void_chronosphere_lua:GetAuraDuration()
	return 0.1
end

function modifier_faceless_void_chronosphere_lua:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_BOTH
end

function modifier_faceless_void_chronosphere_lua:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_faceless_void_chronosphere_lua:GetAuraSearchFlags()
	return 0
end

modifier_faceless_void_chronosphere_lua_aura = class({})
--Classifications template
function modifier_faceless_void_chronosphere_lua_aura:IsHidden()
	return false
end

function modifier_faceless_void_chronosphere_lua_aura:IsDebuff()
	return not self:NotAffected()
end

function modifier_faceless_void_chronosphere_lua_aura:IsPurgable()
	return false
end

function modifier_faceless_void_chronosphere_lua_aura:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_chronosphere_lua_aura:IsStunDebuff()
	return not self:NotAffected()
end

function modifier_faceless_void_chronosphere_lua_aura:RemoveOnDeath()
	return true
end

function modifier_faceless_void_chronosphere_lua_aura:DestroyOnExpire()
	return true
end

function modifier_faceless_void_chronosphere_lua_aura:NotAffected()
	if self:GetParent()==self:GetCaster() then return true end
	if self:GetParent():GetTeamNumber() == self:GetCaster():GetTeamNumber() then
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str7") then return true end
	end
end

function modifier_faceless_void_chronosphere_lua_aura:OnCreated(kv)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi8") then
		self.agi = self:GetParent():GetAgility() * 0.25
		self.str = self:GetParent():GetStrength() * 0.25
		self.int = self:GetParent():GetIntellect(true) * 0.25
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str13") then
		self.damage_dealt_by_caster = 0
		self.damage_dealt_by_others = 0
	end
	if not IsServer() then
		return
	end
	self:GetParent():InterruptMotionControllers(false)
	if self:NotAffected() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int10") then
		local pcf = ParticleManager:CreateParticle("particles/items_fx/black_king_bar_avatar.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
		self:AddParticle(pcf, false, false, -1, false, false)
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi11") and self:GetParent() == self:GetCaster() then
		self.AverageTrueAttackDamage = self:GetParent():GetAverageTrueAttackDamage(self:GetParent())
		self.creationtime = GameRules:GetGameTime()
		self:StartIntervalThink(0.1)
	end
end

function modifier_faceless_void_chronosphere_lua_aura:OnIntervalThink()
	self:SetStackCount(self.AverageTrueAttackDamage * ((GameRules:GetGameTime() - self.creationtime) * 0.15))
end

function modifier_faceless_void_chronosphere_lua_aura:OnDestroy()
	if not IsServer() then
		return
	end
	FindClearSpaceForUnit(self:GetParent(), self:GetParent():GetAbsOrigin(), true)
	if self:GetParent() ~= self:GetCaster():GetTeamNumber() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str13") then
		local damage = self.damage_dealt_by_caster * 0.6 + self.damage_dealt_by_others * 0.3
		if damage > 0 then
			local current_health = self:GetParent():GetHealth()
			if current_health > damage then
				self:GetParent():SetHealth(current_health - damage)
				self:GetParent():CalculateStatBonus(true)
			else
				self:GetParent():Kill(self:GetAbility(), self:GetCaster())
			end
		end
	end
end

function modifier_faceless_void_chronosphere_lua_aura:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE_MIN,

		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,

		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,

		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,

		-- MODIFIER_EVENT_ON_TAKEDAMAGE,
	}
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierMoveSpeed_AbsoluteMin()
	if self:NotAffected() then 
		return 1000 
	end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierAttackSpeedBonus_Constant()
	if self:NotAffected() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str10") then 
		return 80 * self:GetAbility():GetLevel()
	end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierPreAttack_BonusDamage()
	return self:GetStackCount()
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierPhysicalArmorBonus()
	if not self:NotAffected() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int7") then 
		return -20 * self:GetAbility():GetLevel()
	end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierBonusStats_Strength()
	if self:NotAffected() then 
		return self.str
	end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierBonusStats_Intellect()
	if self:NotAffected() then 
		return self.int
	end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierBonusStats_Agility()
	if self:NotAffected() then 
		return self.agi
	end
end

function modifier_faceless_void_chronosphere_lua_aura:CustomOnTakeDamage( data )
    local unit = data.unit
    local attacker = data.attacker
    local damage_taken = data.damage

	if unit:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() and attacker:GetTeamNumber() == self:GetCaster():GetTeamNumber() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str13") then
		if attacker == self:GetCaster() then
			self.damage_dealt_by_caster = self.damage_dealt_by_caster + damage_taken
		else
			self.damage_dealt_by_others = self.damage_dealt_by_others + damage_taken
		end
	end
end

-- function modifier_faceless_void_chronosphere_lua_aura:GetModifierBaseAttack_BonusDamage()
-- 	return self:GetStackCount()
-- end

function modifier_faceless_void_chronosphere_lua_aura:CheckState()
	local state1 = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int10") then
		state1[MODIFIER_STATE_DEBUFF_IMMUNE] = true
	end
	local state2 = {
		[MODIFIER_STATE_STUNNED] = true,
		[MODIFIER_STATE_FROZEN] = true,
		[MODIFIER_STATE_INVISIBLE] = false,
	}

	if self:NotAffected() then return state1 else return state2 end
end

function modifier_faceless_void_chronosphere_lua_aura:GetModifierConstantManaRegen()
	if self.lock then
		return 0
	end
	if self:NotAffected() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str12") then 
		self.lock = true
		local regen = self:GetParent():GetManaRegen()
		self.lock = false
		return regen
	end
end

modifier_faceless_void_chronosphere_lua_inst = class({})
--Classifications template
function modifier_faceless_void_chronosphere_lua_inst:IsHidden()
	return true
end

function modifier_faceless_void_chronosphere_lua_inst:IsDebuff()
	return false
end

function modifier_faceless_void_chronosphere_lua_inst:IsPurgable()
	return false
end

function modifier_faceless_void_chronosphere_lua_inst:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_chronosphere_lua_inst:IsStunDebuff()
	return false
end

function modifier_faceless_void_chronosphere_lua_inst:RemoveOnDeath()
	return false
end

function modifier_faceless_void_chronosphere_lua_inst:DestroyOnExpire()
	return false
end

function modifier_faceless_void_chronosphere_lua_inst:DeclareFunctions()
	local modifierfunction = {
		-- MODIFIER_EVENT_ON_DEATH,
	}
	return modifierfunction
end

function modifier_faceless_void_chronosphere_lua_inst:CustomOnDeath(data)
	if data.attacker == self:GetParent() and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str7") then
		if not self:GetAbility():IsCooldownReady() then
			local flCooldown = self:GetAbility():GetCooldownTimeRemaining()
			self:GetAbility():EndCooldown()
			self:GetAbility():StartCooldown(flCooldown - 0.5)
		end
	end
end