faceless_void_time_lock_lua = class({})
LinkLuaModifier( "modifier_faceless_void_time_lock_lua_handle", "heroes/hero_faceless_void/faceless_void_time_lock_lua",LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_lock_lua_int11", "heroes/hero_faceless_void/faceless_void_time_lock_lua",LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_lock_lua_agi13", "heroes/hero_faceless_void/faceless_void_time_lock_lua",LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_lock_lua_str8", "heroes/hero_faceless_void/faceless_void_time_lock_lua",LUA_MODIFIER_MOTION_NONE )

function faceless_void_time_lock_lua:GetIntrinsicModifierName()
	return "modifier_faceless_void_time_lock_lua_handle"
end

function faceless_void_time_lock_lua:IsStealable()
	return false
end

function faceless_void_time_lock_lua:TimeLock(target)
	if RollPercentage(self:GetSpecialValueFor("chance_pct")) then
		local caster = self:GetCaster()
		EmitSoundOn("Hero_FacelessVoid.TimeLockImpact", target)
		
		local nFX = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_time_lock_bash.vpcf", PATTACH_ABSORIGIN, caster)
		ParticleManager:SetParticleControl(nFX, 0, target:GetAbsOrigin() )
		ParticleManager:SetParticleControl(nFX, 1, target:GetAbsOrigin() )
		ParticleManager:SetParticleControlEnt(nFX, 2, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
		ParticleManager:SetParticleControl(nFX, 4, target:GetAbsOrigin() )
		ParticleManager:SetParticleControl(nFX, 5, Vector(1,1,1) )
		ParticleManager:ReleaseParticleIndex(nFX)
		
		local duration = self:GetSpecialValueFor("duration")
		local stun_cooldown = self:GetSpecialValueFor("stun_cooldown")
		local damage = self:GetSpecialValueFor("damage")
		
		local delay = self:GetSpecialValueFor("damage_delay")

		if not IsServer() then
			return
		end

		Timers:CreateTimer(delay, function()
			if not target or not target.IsAlive or not target:IsAlive() then
				return
			end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi10") then
				ApplyDamageRDA({
					victim = target, 
					attacker = self:GetCaster(), 
					damage = self:GetCaster():GetBaseDamageMax() * 0.8, 
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = self,
					damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
				})
			else
				ApplyDamageRDA({
					victim = target, 
					attacker = self:GetCaster(), 
					damage = self:GetSpecialValueFor("bonus_damage"), 
					damage_type = DAMAGE_TYPE_MAGICAL,
					ability = self
				})
			end
			
			
			self:GetCaster():PerformAttack(target, true, true, true, true, false, false, false)
			if target.time_lock_stunned_time == nil or target.time_lock_stunned_time + stun_cooldown < GameRules:GetGameTime() then
				target:AddNewModifier(caster, self, "modifier_faceless_void_time_lock_lua_stun", {duration = duration})
			end

			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi13") then
				self.modifier_time_lock_agi13:AddStacks()
			end
		end)
	end
end

modifier_faceless_void_time_lock_lua_handle = class({}) 
function modifier_faceless_void_time_lock_lua_handle:IsPurgable()  return false end
function modifier_faceless_void_time_lock_lua_handle:IsDebuff()    return false end
function modifier_faceless_void_time_lock_lua_handle:IsHidden()    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi7") == nil end

function modifier_faceless_void_time_lock_lua_handle:OnCreated()
	if not IsServer() then
		return
	end
	self:GetAbility().modifier_time_lock_agi13 = self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_faceless_void_time_lock_lua_agi13", {})
	self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_faceless_void_time_lock_lua_str8", {})
end

function modifier_faceless_void_time_lock_lua_handle:DeclareFunctions()
    return {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_faceless_void_time_lock_lua_handle:CustomOnAttackLanded(params)
	if params.attacker == self:GetParent() and not params.target:IsBuilding() then
		self:GetAbility():TimeLock(params.target)
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int11") then
			params.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_faceless_void_time_lock_lua_int11", {duration = 3})
		end
		if not params.no_attack_cooldown and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi7") then
			self:IncrementStackCount()
			if self:GetStackCount() > 5 then
				local radius = 600
				local hEnemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )
				local hit = 1
				for i = 1, #hEnemies do
					if hEnemies[i] ~= params.target then
						self:GetAbility():TimeLock(hEnemies[i])
						hit = hit + 1
					end
					if hit >= 3 then break end
				end
				self:SetStackCount(1)
			end
		end
		if params.no_attack_cooldown and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str11") then
			local heal = params.damage * 30/100
			self:GetParent():HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), false)
			local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
			local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			ParticleManager:SetParticleControl( effect_cast, 1, self:GetParent():GetOrigin() )
			ParticleManager:ReleaseParticleIndex( effect_cast )
		end
	end
end

function modifier_faceless_void_time_lock_lua_handle:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "chance_pct" then
			return 1
		end
	end
	return 0
end

function modifier_faceless_void_time_lock_lua_handle:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "chance_pct" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "chance_pct", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi12") then
                value = value + 20
				if self:GetCaster():HasModifier("modifier_faceless_void_time_rift_lua_aura") then
					value = value + 5
				end
            end
            return value
		end
	end
	return 0
end

function modifier_faceless_void_time_lock_lua_handle:GetModifierAttackSpeedBonus_Constant()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int8") then
		return self:GetAbility():GetLevel() * 40
	end
end

LinkLuaModifier( "modifier_faceless_void_time_lock_lua_stun", "heroes/hero_faceless_void/faceless_void_time_lock_lua",LUA_MODIFIER_MOTION_NONE )

modifier_faceless_void_time_lock_lua_stun = class({})
--Classifications template
function modifier_faceless_void_time_lock_lua_stun:IsHidden()
	return false
end

function modifier_faceless_void_time_lock_lua_stun:IsDebuff()
	return true
end

function modifier_faceless_void_time_lock_lua_stun:IsPurgable()
	return false
end

function modifier_faceless_void_time_lock_lua_stun:IsPurgeException()
	return true
end

-- Optional Classifications
function modifier_faceless_void_time_lock_lua_stun:IsStunDebuff()
	return true
end

function modifier_faceless_void_time_lock_lua_stun:RemoveOnDeath()
	return true
end

function modifier_faceless_void_time_lock_lua_stun:DestroyOnExpire()
	return true
end

function modifier_faceless_void_time_lock_lua_stun:OnCreated()
	self:GetParent().time_lock_stunned_time = GameRules:GetGameTime()
end

function modifier_faceless_void_time_lock_lua_stun:CheckState()
	return {
		[MODIFIER_STATE_STUNNED] = true,
		[MODIFIER_STATE_FROZEN] = true,
	}
end

function modifier_faceless_void_time_lock_lua_stun:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}
end

function modifier_faceless_void_time_lock_lua_stun:GetOverrideAnimation( params )
	return ACT_DOTA_DISABLED
end

function modifier_faceless_void_time_lock_lua_stun:GetEffectName()
	return "particles/generic_gameplay/generic_stunned.vpcf"
end

function modifier_faceless_void_time_lock_lua_stun:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end

modifier_faceless_void_time_lock_lua_int11 = class({})
--Classifications template
function modifier_faceless_void_time_lock_lua_int11:IsHidden()
	return false
end

function modifier_faceless_void_time_lock_lua_int11:IsDebuff()
	return true
end

function modifier_faceless_void_time_lock_lua_int11:IsPurgable()
	return true
end

function modifier_faceless_void_time_lock_lua_int11:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_lock_lua_int11:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_lock_lua_int11:RemoveOnDeath()
	return true
end

function modifier_faceless_void_time_lock_lua_int11:DestroyOnExpire()
	return true
end

function modifier_faceless_void_time_lock_lua_int11:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_faceless_void_time_lock_lua_int11:GetModifierPhysicalArmorBonus()
	return -self:GetAbility():GetLevel() * 10
end

modifier_faceless_void_time_lock_lua_agi13 = class({})
--Classifications template
function modifier_faceless_void_time_lock_lua_agi13:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_faceless_void_time_lock_lua_agi13:IsDebuff()
	return false
end

function modifier_faceless_void_time_lock_lua_agi13:IsPurgable()
	return true
end

function modifier_faceless_void_time_lock_lua_agi13:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_lock_lua_agi13:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_lock_lua_agi13:RemoveOnDeath()
	return false
end

function modifier_faceless_void_time_lock_lua_agi13:DestroyOnExpire()
	return false
end

function modifier_faceless_void_time_lock_lua_agi13:OnCreated( kv )
	self.max_stacks = 360
	self.add_stacks = 9
	self.duration = 10
	self.stack_table = {}
	self:StartIntervalThink(0.2)
end

function modifier_faceless_void_time_lock_lua_agi13:OnIntervalThink()
	local repeat_needed = true
	while repeat_needed do
		local item_time = self.stack_table[1]
		if item_time and GameRules:GetGameTime() - item_time >= self.duration then
			table.remove(self.stack_table, 1)
			self:SetStackCount( self:GetStackCount() - self.add_stacks )
		else
			repeat_needed = false
		end
	end
end

function modifier_faceless_void_time_lock_lua_agi13:AddStacks()
	self:SetStackCount(self:GetStackCount() + self.add_stacks)
end

function modifier_faceless_void_time_lock_lua_agi13:OnStackCountChanged(prev_stacks)
	if not IsServer() then return end
	local stacks = self:GetStackCount()
	if stacks > prev_stacks then
		table.insert(self.stack_table, GameRules:GetGameTime())
		if stacks > self.max_stacks then
			table.remove(self.stack_table, 1)
			self:SetStackCount( self:GetStackCount() - self.add_stacks )
		end
		self:SetDuration(self.duration, true)
		self:ForceRefresh()
	end
end

function modifier_faceless_void_time_lock_lua_agi13:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE
	}
end

function modifier_faceless_void_time_lock_lua_agi13:GetModifierDamageOutgoing_Percentage()
	return self:GetStackCount()
end

modifier_faceless_void_time_lock_lua_str8 = class({})
--Classifications template
function modifier_faceless_void_time_lock_lua_str8:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_faceless_void_time_lock_lua_str8:IsDebuff()
	return false
end

function modifier_faceless_void_time_lock_lua_str8:IsPurgable()
	return true
end

function modifier_faceless_void_time_lock_lua_str8:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_lock_lua_str8:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_lock_lua_str8:RemoveOnDeath()
	return false
end

function modifier_faceless_void_time_lock_lua_str8:DestroyOnExpire()
	return false
end

function modifier_faceless_void_time_lock_lua_str8:OnCreated( kv )

end

function modifier_faceless_void_time_lock_lua_str8:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	}
end

function modifier_faceless_void_time_lock_lua_str8:CustomOnDeath(data)
	if data.attacker == self:GetParent() and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str8") then
		if self:GetAbility():GetLevel() == 15 then
			self:SetStackCount(self:GetStackCount() + 15)
		end
	end
end

function modifier_faceless_void_time_lock_lua_str8:GetModifierPreAttack_BonusDamage()
	return self:GetStackCount()
end