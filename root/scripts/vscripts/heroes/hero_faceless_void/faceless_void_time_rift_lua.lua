faceless_void_time_rift_lua = class({})

function faceless_void_time_rift_lua:OnSpellStart()
    if self.ModifierThinker then
        UTIL_Remove(self.ModifierThinker)
    end
    self.ModifierThinker = CreateModifierThinker(self:GetCaster(), self, "modifier_faceless_void_time_rift_lua", {duration = self:GetSpecialValueFor("duration")}, self:GetCaster():GetCursorPosition(), self:GetCaster():GetTeamNumber(), false)
end

function faceless_void_time_rift_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    local manacost = 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 30)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int7") then
        return manacost * 0.2
    end
    return manacost
end

function faceless_void_time_rift_lua:OnUpgrade()
    if self:GetLevel() == 1 then
        self:ToggleAutoCast()
    end
end

function faceless_void_time_rift_lua:GetIntrinsicModifierName()
	return "modifier_faceless_void_time_rift_lua_inst"
end

function faceless_void_time_rift_lua:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function faceless_void_time_rift_lua:GetCooldown(iLevel)
    local cooldown_value = self.BaseClass.GetCooldown(self, iLevel)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi11") then
        return math.max(cooldown_value - self:GetCaster():GetLastHits() * 0.25, 3)
    end
    return cooldown_value
end

LinkLuaModifier( "modifier_faceless_void_time_rift_lua_inst", "heroes/hero_faceless_void/faceless_void_time_rift_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_rift_lua", "heroes/hero_faceless_void/faceless_void_time_rift_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_rift_lua_aura", "heroes/hero_faceless_void/faceless_void_time_rift_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_faceless_void_time_rift_lua_elapsed_time", "heroes/hero_faceless_void/faceless_void_time_rift_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_generic_knockback_lua", "heroes/generic/modifier_generic_knockback_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_faceless_void_time_rift_lua = class({})
--Classifications template
function modifier_faceless_void_time_rift_lua:IsHidden()
	return false
end

function modifier_faceless_void_time_rift_lua:IsDebuff()
	return true
end

function modifier_faceless_void_time_rift_lua:IsPurgable()
	return false
end

function modifier_faceless_void_time_rift_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_rift_lua:IsStunDebuff()
	return true
end

function modifier_faceless_void_time_rift_lua:RemoveOnDeath()
	return true
end

function modifier_faceless_void_time_rift_lua:DestroyOnExpire()
	return true
end

function modifier_faceless_void_time_rift_lua:OnCreated(data)
	if not IsServer() then
		return
	end
	local caster = self:GetCaster()
	local point = self:GetParent():GetAbsOrigin()
	local radius = self:GetAbility():GetSpecialValueFor("radius")

	local nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere_surface.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx, 0, point)
	ParticleManager:SetParticleControl(nfx, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx, false, false, -1, false, false)

	local nfx2 = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere_rim.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx2, 0, point)
	ParticleManager:SetParticleControl(nfx2, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx2, false, false, -1, false, false)

	local nfx3 = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chronosphere.vpcf", PATTACH_POINT, caster)
	ParticleManager:SetParticleControl(nfx3, 0, point)
	ParticleManager:SetParticleControl(nfx3, 1, Vector(radius, radius, radius))
	self:AddParticle(nfx3, false, false, -1, false, false)

	EmitSoundOn( "Hero_FacelessVoid.Chronosphere", self:GetParent() )

    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str12") then
        self:StartIntervalThink(1)
        self:OnIntervalThink()
    end
end

function modifier_faceless_void_time_rift_lua:OnIntervalThink()
    local hEnemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 1200, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_ANY_ORDER, false )
    for _, hEnemy in pairs(hEnemies) do
        if not table.has_value(bosses_names, hEnemy:GetUnitName()) then
            local distance_to_time_rift = (hEnemy:GetOrigin() - self:GetParent():GetOrigin()):Length2D()
            if distance_to_time_rift > self:GetAbility():GetSpecialValueFor("radius") then
                local direction = (hEnemy:GetOrigin() - self:GetParent():GetOrigin()):Normalized() * -1
                hEnemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_generic_knockback_lua", {
                    distance = 200,
                    duration = 1,
                    direction_x = direction.x,
                    direction_y = direction.y,
                    tree_destroy_radius = 200,
                })
            end
        end
    end
end

function modifier_faceless_void_time_rift_lua:OnDestroy()
	if not IsServer() then
		return
	end
	UTIL_Remove(self:GetParent())
end
--------------------------------------------------------------------------------
-- Aura Effects
function modifier_faceless_void_time_rift_lua:IsAura()
	return true
end

function modifier_faceless_void_time_rift_lua:GetModifierAura()
	return "modifier_faceless_void_time_rift_lua_aura"
end

function modifier_faceless_void_time_rift_lua:GetAuraRadius()
	return self:GetAbility():GetSpecialValueFor("radius")
end

function modifier_faceless_void_time_rift_lua:GetAuraDuration()
	return 0.1
end

function modifier_faceless_void_time_rift_lua:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_BOTH
end

function modifier_faceless_void_time_rift_lua:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_faceless_void_time_rift_lua:GetAuraSearchFlags()
	return DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES
end

modifier_faceless_void_time_rift_lua_aura = class({})
--Classifications template
function modifier_faceless_void_time_rift_lua_aura:IsHidden()
	return false
end

function modifier_faceless_void_time_rift_lua_aura:IsDebuff()
	return self:GetParent():GetTeamNumber() ~= self:GetCaster():GetTeamNumber()
end

function modifier_faceless_void_time_rift_lua_aura:IsPurgable()
	return false
end

function modifier_faceless_void_time_rift_lua_aura:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_rift_lua_aura:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_rift_lua_aura:RemoveOnDeath()
	return true
end

function modifier_faceless_void_time_rift_lua_aura:DestroyOnExpire()
	return true
end

function modifier_faceless_void_time_rift_lua_aura:OnCreated(kv)
    self.damage = self:GetAbility():GetSpecialValueFor("damage")
    self.interval = self:GetAbility():GetSpecialValueFor("interval")
    self.ms_slow_prc = self:GetAbility():GetSpecialValueFor("ms_slow_prc")
	if not IsServer() then
        return
    end
    if self:GetParent():GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str13") then
            self.modifier_time_rift_elapsed_time = self:GetParent():FindModifierByName("modifier_faceless_void_time_rift_lua_elapsed_time")
            if not self.modifier_time_rift_elapsed_time then
                self.modifier_time_rift_elapsed_time = self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_faceless_void_time_rift_lua_elapsed_time", {})
                self.modifier_time_rift_elapsed_time:SetStackCount(200)
            end
            self.modifier_time_rift_elapsed_time:SetDuration(40, true)
        end
        self:StartIntervalThink(self.interval)
    end
end

function modifier_faceless_void_time_rift_lua_aura:OnRefresh(kv)
    self.damage = self:GetAbility():GetSpecialValueFor("damage")
    self.interval = self:GetAbility():GetSpecialValueFor("interval")
    self.ms_slow_prc = self:GetAbility():GetSpecialValueFor("ms_slow_prc")
end

function modifier_faceless_void_time_rift_lua_aura:GetDamageMultiplyer()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str13") then
        return math.min(self.modifier_time_rift_elapsed_time:GetStackCount()/100, 4)
    end
    return 1
end

function modifier_faceless_void_time_rift_lua_aura:OnIntervalThink()
    if self.modifier_time_rift_elapsed_time then
        local new_stacks = self.modifier_time_rift_elapsed_time:GetStackCount() + 20 * self.interval
        self.modifier_time_rift_elapsed_time:SetStackCount(math.min(new_stacks, 400))
        self.modifier_time_rift_elapsed_time:SetDuration(40, true)
    end
	ApplyDamageRDA({
        victim = self:GetParent(), 
        attacker = self:GetCaster(), 
        damage = self.damage * self.interval * self:GetDamageMultiplyer(), 
        damage_type = DAMAGE_TYPE_MAGICAL,
        ability = self:GetAbility(),
    })
end

function modifier_faceless_void_time_rift_lua_aura:OnDestroy()
	if not IsServer() then
		return
	end
end

function modifier_faceless_void_time_rift_lua_aura:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
        MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_faceless_void_time_rift_lua_aura:GetModifierMagicalResistanceBonus()
    if self:GetParent() == self:GetCaster() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi8") then
        return 30 + self:GetAbility():GetLevel() * 2
    end
    return 0
end

function modifier_faceless_void_time_rift_lua_aura:GetModifierHealthRegenPercentage()
    if self:GetParent():GetTeamNumber() == self:GetCaster():GetTeamNumber() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str7") then
        return 5
    end
    return 0
end

function modifier_faceless_void_time_rift_lua_aura:GetModifierMoveSpeedBonus_Percentage()
    if self:GetParent():GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
        return -self.ms_slow_prc
    end
    return 0
end

modifier_faceless_void_time_rift_lua_inst = class({})
--Classifications template
function modifier_faceless_void_time_rift_lua_inst:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_faceless_void_time_rift_lua_inst:IsDebuff()
	return false
end

function modifier_faceless_void_time_rift_lua_inst:IsPurgable()
	return false
end

function modifier_faceless_void_time_rift_lua_inst:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_rift_lua_inst:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_rift_lua_inst:RemoveOnDeath()
	return false
end

function modifier_faceless_void_time_rift_lua_inst:DestroyOnExpire()
	return false
end

function modifier_faceless_void_time_rift_lua_inst:DeclareFunctions()
	local modifierfunction = {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        -- MODIFIER_EVENT_ON_DEATH,
	}
	return modifierfunction
end

function modifier_faceless_void_time_rift_lua_inst:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_faceless_void_time_rift_lua_inst:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            value = value + self:GetStackCount()
            
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int10") then
                value = value + self:GetCaster():GetIntellect(true)
            end
            
            return value
		end
	end
	return 0
end


function modifier_faceless_void_time_rift_lua_inst:OnCreated( kv )
    if not IsServer() then
        return
    end
    self:StartIntervalThink(0.2)
end

function modifier_faceless_void_time_rift_lua_inst:OnIntervalThink()
    if self:GetAbility():IsFullyCastable() and self:GetCaster():IsAlive() and self:GetAbility():GetAutoCastState() then
        local hEnemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetAbility():GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )
        if hEnemies[1] then
            if self:GetAbility().ModifierThinker then
                UTIL_Remove(self:GetAbility().ModifierThinker)
            end
            self:GetAbility().ModifierThinker = CreateModifierThinker(self:GetCaster(), self:GetAbility(), "modifier_faceless_void_time_rift_lua", {duration = self:GetAbility():GetSpecialValueFor("duration")}, hEnemies[1]:GetAbsOrigin(), self:GetCaster():GetTeamNumber(), false)
            self:GetAbility():UseResources(true, true, true, true)
        end
    end
end

function modifier_faceless_void_time_rift_lua_inst:CustomOnDeath(data)
	if data.attacker == self:GetParent() and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str10") then
		self:SetStackCount(self:GetStackCount() + 10)
	end
end


modifier_faceless_void_time_rift_lua_elapsed_time = class({})
--Classifications template
function modifier_faceless_void_time_rift_lua_elapsed_time:IsHidden()
	return false
end

function modifier_faceless_void_time_rift_lua_elapsed_time:IsDebuff()
	return true
end

function modifier_faceless_void_time_rift_lua_elapsed_time:IsPurgable()
	return false
end

function modifier_faceless_void_time_rift_lua_elapsed_time:RemoveOnDeath()
	return true
end