faceless_void_time_dilation_lua = class({})

function faceless_void_time_dilation_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
		return 0
	elseif self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int12") then
		return 0
	end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function faceless_void_time_dilation_lua:GetCastRange(vLocation, hTarget)
	return self:GetSpecialValueFor("radius")
end

function faceless_void_time_dilation_lua:GetIntrinsicModifierName()
	return "modifier_faceless_void_time_dilation_lua_inst"
end

function faceless_void_time_dilation_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int12") or self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
		return DOTA_ABILITY_BEHAVIOR_PASSIVE
	end
	return self.BaseClass.GetBehavior(self)
end

function faceless_void_time_dilation_lua:GetCooldown(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
		return 0.5
	elseif self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int12") then
		return 1.5
	end
	return self.BaseClass.GetCooldown(self, iLevel)
end

function faceless_void_time_dilation_lua:IsStealable()
	return true
end

function faceless_void_time_dilation_lua:IsHiddenWhenStolen()
	return false
end

function faceless_void_time_dilation_lua:OnSpellStart()
	local caster = self:GetCaster()

	local duration = self:GetSpecialValueFor("duration")
	local radius = self:GetSpecialValueFor("radius")
	local cd_increase = self:GetSpecialValueFor("cd_increase")
	local damage = self:GetSpecialValueFor("damage")
	
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, radius,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)
	local faceless_void_int9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int9")

	for unit_index ,unit in pairs(units) do
		unit:AddNewModifier(caster, self, "modifier_faceless_void_time_dilation_lua", {duration = duration})
		if not unit.time_dilation then unit.time_dilation = {} end
		for i = 0, 23 do
			local current_ability = unit:GetAbilityByIndex(i)
			if current_ability and not current_ability:IsPassive() and not current_ability:IsAttributeBonus() and not current_ability:IsCooldownReady() then
				local entindex = current_ability:entindex()
				local new_cooldown = current_ability:GetCooldownTimeRemaining() + cd_increase
				if not unit.time_dilation[entindex] then unit.time_dilation[entindex] = {} end
				if (not unit.time_dilation[entindex]['new_cooldown'] and not unit.time_dilation[entindex]['game_time']) or unit.time_dilation[entindex]['game_time'] + unit.time_dilation[entindex]['new_cooldown'] < GameRules:GetGameTime() then
					unit.time_dilation[entindex]['new_cooldown'] = new_cooldown
					unit.time_dilation[entindex]['game_time'] = GameRules:GetGameTime()
					current_ability:EndCooldown()
					current_ability:StartCooldown( new_cooldown )
				end
			end
		end
		if damage > 0 then
			local damageTable = {
				victim = unit,
				attacker = self:GetCaster(),
				damage = damage,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = 0,
				ability = self
			}
			if faceless_void_int9 and unit_index == 1 then
				damageTable.damage = damage * 2
			end
			ApplyDamageRDA(damageTable)
		end
	end

	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_timedialate.vpcf", PATTACH_POINT_FOLLOW, caster)
	ParticleManager:SetParticleControl(pcf, 1, Vector(radius, radius, radius))
	EmitSoundOn("Hero_FacelessVoid.TimeDilation.Cast", caster)

	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str9") then
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_faceless_void_time_dilation_lua_str9", {})
	end
end

function faceless_void_time_dilation_lua:OnSpellStart_target(target)
	local caster = self:GetCaster()

	local duration = self:GetSpecialValueFor("duration")
	local radius = self:GetSpecialValueFor("radius")
	local cd_increase = self:GetSpecialValueFor("cd_increase")
	local damage = self:GetSpecialValueFor("damage")

	self.cast_timestamps = self.cast_timestamps or {}
	local current_time = GameRules:GetGameTime()

    for i = #self.cast_timestamps, 1, -1 do
        if current_time - self.cast_timestamps[i] > 1 then
            table.remove(self.cast_timestamps, i)
        end
    end

    if #self.cast_timestamps >= 2 then
        return false
    end

	table.insert(self.cast_timestamps, current_time)

	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), target:GetAbsOrigin(), nil, radius,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	local faceless_void_int9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int9")
	if faceless_void_int9 then
		local caster_abs = self:GetCaster():GetOrigin()
		table.sort(units, function(a, b)
			local distanceA = (a:GetOrigin() - caster_abs):Length2D()
			local distanceB = (b:GetOrigin() - caster_abs):Length2D()
			return distanceA < distanceB
		end)
	end

	for unit_index,unit in pairs(units) do
		unit:AddNewModifier(caster, self, "modifier_faceless_void_time_dilation_lua", {duration = duration})
		if not unit.time_dilation then unit.time_dilation = {} end
		for i = 0, 23 do
			local current_ability = unit:GetAbilityByIndex(i)
			if current_ability and not current_ability:IsPassive() and not current_ability:IsAttributeBonus() and not current_ability:IsCooldownReady() then
				local entindex = current_ability:entindex()
				local new_cooldown = current_ability:GetCooldownTimeRemaining() + cd_increase
				if not unit.time_dilation[entindex] then unit.time_dilation[entindex] = {} end
				if (not unit.time_dilation[entindex]['new_cooldown'] and not unit.time_dilation[entindex]['game_time']) or unit.time_dilation[entindex]['game_time'] + unit.time_dilation[entindex]['new_cooldown'] < GameRules:GetGameTime() then
					unit.time_dilation[entindex]['new_cooldown'] = new_cooldown
					unit.time_dilation[entindex]['game_time'] = GameRules:GetGameTime()
					current_ability:EndCooldown()
					current_ability:StartCooldown( new_cooldown )
				end
			end
		end
		if damage > 0 then
			local damageTable = {
				victim = unit,
				attacker = self:GetCaster(),
				damage = damage,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = 0,
				ability = self
			}
			if (faceless_void_int9 and unit_index == 1) or table.has_value(bosses_names, unit:GetUnitName()) then
				damageTable.damage = damage * 2
			end
			ApplyDamageRDA(damageTable)
		end
	end

	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_timedialate.vpcf", PATTACH_WORLDORIGIN, caster)
	ParticleManager:SetParticleControl(pcf, 0, target:GetAbsOrigin())
	ParticleManager:SetParticleControl(pcf, 1, Vector(radius, radius, radius))
	EmitSoundOn("Hero_FacelessVoid.TimeDilation.Cast", target)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str9") then
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_faceless_void_time_dilation_lua_str9", {})
	end
end

LinkLuaModifier("modifier_faceless_void_time_dilation_lua", "heroes/hero_faceless_void/faceless_void_time_dilation_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_faceless_void_time_dilation_lua_str9", "heroes/hero_faceless_void/faceless_void_time_dilation_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_faceless_void_time_dilation_lua_inst", "heroes/hero_faceless_void/faceless_void_time_dilation_lua", LUA_MODIFIER_MOTION_NONE)

modifier_faceless_void_time_dilation_lua = class({})
--Classifications template
function modifier_faceless_void_time_dilation_lua:IsHidden()
	return false
end

function modifier_faceless_void_time_dilation_lua:IsDebuff()
	return true
end

function modifier_faceless_void_time_dilation_lua:IsPurgable()
	return true
end

function modifier_faceless_void_time_dilation_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_dilation_lua:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_dilation_lua:RemoveOnDeath()
	return true
end

function modifier_faceless_void_time_dilation_lua:DestroyOnExpire()
	return true
end

function modifier_faceless_void_time_dilation_lua:OnCreated()
	if not IsServer() then
		return
	end
	self.damage_per_stack = self:GetAbility():GetSpecialValueFor("damage_per_stack")
	self:SetStackCount(1)
	self.base_damage = self:GetAbility():GetSpecialValueFor("base_damage")
	self.damage_per_stack = self:GetAbility():GetSpecialValueFor("damage_per_stack")

	self.interval = self:GetAbility():GetSpecialValueFor("interval")
	if self.interval > 0 then
		self:StartIntervalThink(self.interval)
		self:OnIntervalThink()
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
		self:SetStackCount(8)
	end
end

function modifier_faceless_void_time_dilation_lua:OnRefresh()
	if not IsServer() then
		return
	end
	self.base_damage = self:GetAbility():GetSpecialValueFor("base_damage")
	self.damage_per_stack = self:GetAbility():GetSpecialValueFor("damage_per_stack")
	local max_stacks = self:GetAbility():GetSpecialValueFor("max_stacks")
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
		self:SetStackCount(math.min(self:GetStackCount() + 3, max_stacks))
	else
		self:SetStackCount(math.min(self:GetStackCount() + 1, max_stacks))
	end
	
	-- self.damage_per_stack = self:GetAbility():GetSpecialValueFor("damage_per_stack")
	-- self:SetStackCount(math.min(self:GetStackCount() + 1, 10))
	-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
	-- 	self.damage_per_stack = self.damage_per_stack * 0.2
	-- 	self:OnIntervalThink()
	-- end
end

function modifier_faceless_void_time_dilation_lua:OnIntervalThink()
	-- local c = 1
	-- for i=0,10 do
	-- 	local abi = self:GetParent():GetAbilityByIndex(i)
	-- 	if abi then
	-- 		if not abi:IsCooldownReady() then
	-- 			c = c + 1
	-- 		end
	-- 	end
	-- end
	ApplyDamageRDA({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = (self.base_damage + self:GetStackCount() * self.damage_per_stack) * self.interval,
		damage_type = DAMAGE_TYPE_MAGICAL,
		damage_flags = 0,
		ability = self:GetAbility()
	})
end

function modifier_faceless_void_time_dilation_lua:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_faceless_void_time_dilation_lua:CustomOnDeath(params)
	if params.unit:GetTeamNumber() == self:GetParent():GetTeamNumber() and params.unit == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int9") then
		local distance = (self:GetParent():GetOrigin() - self:GetCaster():GetOrigin()):Length2D()
		if distance < self:GetAbility():GetSpecialValueFor("radius") then
			self:GetAbility():OnSpellStart_target(params.unit)
		end
	end
end

function modifier_faceless_void_time_dilation_lua:GetModifierMoveSpeedBonus_Percentage()
	return -self:GetAbility():GetSpecialValueFor("slow")
end

modifier_faceless_void_time_dilation_lua_inst = class({})
--Classifications template
function modifier_faceless_void_time_dilation_lua_inst:IsHidden()
	return true
end

function modifier_faceless_void_time_dilation_lua_inst:IsDebuff()
	return false
end

function modifier_faceless_void_time_dilation_lua_inst:IsPurgable()
	return false
end

function modifier_faceless_void_time_dilation_lua_inst:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_dilation_lua_inst:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_dilation_lua_inst:RemoveOnDeath()
	return false
end

function modifier_faceless_void_time_dilation_lua_inst:DestroyOnExpire()
	return false
end

function modifier_faceless_void_time_dilation_lua_inst:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(1)
end

function modifier_faceless_void_time_dilation_lua_inst:OnIntervalThink()
	if self:GetAbility():GetBehavior() == DOTA_ABILITY_BEHAVIOR_PASSIVE and self:GetCaster():IsAlive() then
		self:GetAbility():OnSpellStart()
		self:GetAbility():UseResources(false, false, false, true)
		self:StartIntervalThink(self:GetAbility():GetCooldownTimeRemaining())
	else
		self:StartIntervalThink(1)
	end
end

function modifier_faceless_void_time_dilation_lua_inst:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

		MODIFIER_PROPERTY_HEALTH_BONUS
	}
end

function modifier_faceless_void_time_dilation_lua_inst:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
		if data.ability_special_value == "base_damage" then
			return 1
		end
		if data.ability_special_value == "damage_per_stack" then
			return 1
		end
		if data.ability_special_value == "interval" then
			return 1
		end
		if data.ability_special_value == "max_stacks" then
			return 1
		end
	end
	return 0
end

function modifier_faceless_void_time_dilation_lua_inst:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "base_damage", data.ability_special_level )
            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi6") then
            --     value = value + self:GetStackCount() * 0.3
            -- end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int6") then
				value = value + self:GetCaster():GetIntellect(true)
			end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
				value = value + self:GetCaster():GetIntellect(true)
			end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi6") then
				value = value + self:GetCaster():GetDamageMax() * 0.2
			end
			
            return value
		end
		if data.ability_special_value == "base_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "base_damage", data.ability_special_level )
            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi6") then
            --     value = value + self:GetStackCount() * 0.3
            -- end
			-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int6") then
			-- 	value = value + self:GetCaster():GetIntellect(true) * 0.7
			-- end
            return value
		end
		if data.ability_special_value == "damage_per_stack" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage_per_stack", data.ability_special_level )
            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi6") then
            --     value = value + self:GetStackCount() * 0.1
            -- end
			-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int6") then
			-- 	value = value + self:GetCaster():GetIntellect(true) * 0.3
			-- end
            return value
		end
		if data.ability_special_value == "interval" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "interval", data.ability_special_level )
            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_agi6") then
            --     value = 0.5
            -- end
			-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int6") then
			-- 	value = 0.5
			-- end
            return value
		end
		if data.ability_special_value == "max_stacks" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "max_stacks", data.ability_special_level )
            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_int13") then
            --     value = 35
            -- end
            return value
		end
	end
	return 0
end

function modifier_faceless_void_time_dilation_lua_inst:GetModifierHealthBonus()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_faceless_void_str6") then
		return 35 * self:GetParent():GetLevel() * self:GetAbility():GetLevel()
	end
end

modifier_faceless_void_time_dilation_lua_str9 = class({})
--Classifications template
function modifier_faceless_void_time_dilation_lua_str9:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_faceless_void_time_dilation_lua_str9:IsDebuff()
	return false
end

function modifier_faceless_void_time_dilation_lua_str9:IsPurgable()
	return false
end

function modifier_faceless_void_time_dilation_lua_str9:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_faceless_void_time_dilation_lua_str9:IsStunDebuff()
	return false
end

function modifier_faceless_void_time_dilation_lua_str9:RemoveOnDeath()
	return false
end

function modifier_faceless_void_time_dilation_lua_str9:DestroyOnExpire()
	return false
end

function modifier_faceless_void_time_dilation_lua_str9:OnCreated()
	if not IsServer() then
		return
	end
	self:SetStackCount(1)
end

function modifier_faceless_void_time_dilation_lua_str9:OnRefresh()
	if not IsServer() then
		return
	end
	self:IncrementStackCount()
end

function modifier_faceless_void_time_dilation_lua_str9:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_faceless_void_time_dilation_lua_str9:GetModifierPhysicalArmorBonus()
	return self:GetStackCount() * 0.5
end