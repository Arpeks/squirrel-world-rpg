faceless_void_time_walk_lua = class({})
LinkLuaModifier( "modifier_faceless_void_time_walk_lua", "heroes/hero_faceless_void/faceless_void_time_walk_lua", LUA_MODIFIER_MOTION_HORIZONTAL )
LinkLuaModifier( "modifier_faceless_void_time_walk_lua_counter", "heroes/hero_faceless_void/faceless_void_time_walk_lua" ,LUA_MODIFIER_MOTION_NONE )

function faceless_void_time_walk_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function faceless_void_time_walk_lua:GetCastRange()
    if IsClient() then
        return self:GetSpecialValueFor("range")
    end
    return 99999
end

function faceless_void_time_walk_lua:GetIntrinsicModifierName()
    if not self:GetCaster():IsIllusion() then
        return "modifier_faceless_void_time_walk_lua_counter"
    end
end

function faceless_void_time_walk_lua:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_faceless_void_time_walk_lua", {duration = self:GetSpecialValueFor("range")/self:GetSpecialValueFor("speed")})
    
    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_time_walk_slow.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster())
    ParticleManager:SetParticleControl(pcf, 1, Vector(self:GetSpecialValueFor("radius"), 0, 0))
    EmitSoundOn("Hero_FacelessVoid.TimeWalk", self:GetCaster())

    ProjectileManager:ProjectileDodge(self:GetCaster())
end

function faceless_void_time_walk_lua:OnAbilityPhaseStart()
    if GridNav:CanFindPath(self:GetCaster():GetOrigin(), self:GetCursorPosition()) == false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        if player then
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_cast_here" })
            -- self:EndCooldown()
            -- self:RefundManaCost()
            return false
        end
    end
end

modifier_faceless_void_time_walk_lua = class({})

function modifier_faceless_void_time_walk_lua:OnCreated(table)
    if not IsServer() then
        return
    end
    local point = self:GetAbility():GetCursorPosition()
    self.range = self:GetAbility():GetSpecialValueFor("range")
    local calc = point - self:GetCaster():GetAbsOrigin()
    self.direction = calc:Normalized()
    self.len = math.min(calc:Length2D(), self.range)
    local end_point = self:GetCaster():GetAbsOrigin() + self.direction * self.len
    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_time_walk_preimage.vpcf", PATTACH_ABSORIGIN, self:GetParent())
    ParticleManager:SetParticleControl(particle, 0, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 1, self:GetParent():GetAbsOrigin() + self.direction * self.len)
    ParticleManager:SetParticleControlEnt(particle, 2, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetForwardVector(), true)
    ParticleManager:ReleaseParticleIndex(particle)

    local nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_faceless_void/faceless_void_chrono_speed.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster())
    self:AddParticle(nfx, false, false, -1, false, false)

    self:ApplyHorizontalMotionController()
end

function modifier_faceless_void_time_walk_lua:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetCaster():HealWithParams(math.min(math.abs(self:GetCaster().time_walk_damage_taken), 2^30), self:GetAbility(), false, false, self:GetParent(), false)
    self:GetCaster():StartGesture(ACT_DOTA_CAST_ABILITY_1_END)
end

function modifier_faceless_void_time_walk_lua:UpdateHorizontalMotion( me, dt )
    local caster = self:GetParent()
    if self.len > 0 then
        local pos = GetGroundPosition(self:GetParent():GetAbsOrigin(), self:GetParent())
        self:GetParent():SetAbsOrigin(pos + self.direction * self:GetSpecialValueFor("speed")*dt)
        self.len = self.len - self:GetSpecialValueFor("speed")*dt
    else
        FindClearSpaceForUnit(self:GetParent(), self:GetParent():GetAbsOrigin(), true)
        self:Destroy()
    end
end

function modifier_faceless_void_time_walk_lua:CheckState()
    return { 
        [MODIFIER_STATE_COMMAND_RESTRICTED] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_FLYING_FOR_PATHING_PURPOSES_ONLY] = true,
        [MODIFIER_STATE_INVULNERABLE] = true
    }
end

function modifier_faceless_void_time_walk_lua:GetEffectName()
    return "particles/units/heroes/hero_faceless_void/faceless_void_time_walk.vpcf"
end

function modifier_faceless_void_time_walk_lua:GetStatusEffectName()
    return "particles/status_fx/status_effect_faceless_timewalk.vpcf"
end

function modifier_faceless_void_time_walk_lua:StatusEffectPriority()
    return 10
end

function modifier_faceless_void_time_walk_lua:IsHidden()
    return true
end

modifier_faceless_void_time_walk_lua_counter = class({}) 
function modifier_faceless_void_time_walk_lua_counter:IsPurgable()  return false end
function modifier_faceless_void_time_walk_lua_counter:IsDebuff()    return false end
function modifier_faceless_void_time_walk_lua_counter:IsHidden()    return true end

function modifier_faceless_void_time_walk_lua_counter:DeclareFunctions()
    return { 
        -- MODIFIER_EVENT_ON_TAKEDAMAGE
    }
end

function modifier_faceless_void_time_walk_lua_counter:OnCreated()
    self.caster = self:GetCaster()
    self.damage_time = self:GetAbility():GetSpecialValueFor("backtrack_duration")
    self.caster.time_walk_damage_taken = 0
end

function modifier_faceless_void_time_walk_lua_counter:CustomOnTakeDamage( data )
    local unit = data.unit
    local damage_taken = data.damage
    if unit == self.caster then
        self.caster.time_walk_damage_taken = self.caster.time_walk_damage_taken + damage_taken
        Timers:CreateTimer(self.damage_time, function()
            if self.caster.time_walk_damage_taken then
                self.caster.time_walk_damage_taken = self.caster.time_walk_damage_taken - damage_taken
            end
        end)
    end
end