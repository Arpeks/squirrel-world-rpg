LinkLuaModifier( "modifier_riki_custom_backstab", "heroes\hero_riki\riki_custom_backstab.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if riki_custom_backstab == nil then
	riki_custom_backstab = class({})
end
function riki_custom_backstab:GetIntrinsicModifierName()
	return "modifier_riki_custom_backstab"
end
---------------------------------------------------------------------
--Modifiers
if modifier_riki_custom_backstab == nil then
	modifier_riki_custom_backstab = class({})
end
function modifier_riki_custom_backstab:OnCreated(params)
	if IsServer() then
	end
end
function modifier_riki_custom_backstab:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_riki_custom_backstab:OnDestroy()
	if IsServer() then
	end
end
function modifier_riki_custom_backstab:DeclareFunctions()
	return {
	}
end