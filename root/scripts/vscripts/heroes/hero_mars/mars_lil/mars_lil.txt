"DOTAAbilities"
{
	"mars_lil"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_mars/mars_lil/mars_lil"
		"AbilityTextureName"			"mars_spear"
		"FightRecapLevel"				"1"
		"MaxLevel"						"15"
		"precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_snapfire.vsndevts"
			"particle"	"particles/units/heroes/hero_snapfire/hero_snapfire_shells_projectile.vpcf"
			"particle"	"particles/units/heroes/hero_snapfire/hero_snapfire_shells_buff.vpcf"
			"particle"	"particles/units/heroes/hero_snapfire/hero_snapfire_slow_debuff.vpcf"
			"particle"	"particles/units/heroes/hero_sniper/sniper_headshot_slow.vpcf"
		}
		
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"

		"AbilityCastRange"				"500"
		"AbilityDuration"				"8"

		"AbilityCooldown"				"8"
		"AbilityManaCost"				"100"

        "AbilityValues"
        {
            "damage"
            {
                "value"                 "50 100 150 200 250 300 350 400 450 500 550 600 650 700 750"
                "CalculateSpellDamageTooltip" "0"
            }
            "buffed_attacks"
            {
                "value"                 "6"
                "LinkedSpecialBonus"    "npc_dota_hero_mars_agi11"
            }
            "attack_speed_bonus"
            {
                "value"                 "300 315 330 345 360 375 390 405 420 435 450 465 480 495 510"
            }
            "attack_range_bonus"
            {
                "value"                 "100"
            }
            "buff_duration_tooltip"
            {
                "value"                 "8"
            }
            "base_attack_time"
            {
                "value"                 "1.0"
            }
            "loss_armor"
            {
                "value"                 "3"
                "LinkedSpecialBonus"    "npc_dota_hero_mars_agi7"
                "LinkedSpecialBonus"    "npc_dota_hero_mars_agi_last"
            }
            "loss_duration"
            {
                "value"                 "3.5"
            }
        }
	}
}