"DOTAAbilities"
{
    "void_spirit_aether_remnant_custom"
	{
        "BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_void_spirit/void_spirit_aether_remnant_custom"
		"AbilityTextureName"			"void_spirit_aether_remnant"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_DONT_RESUME_MOVEMENT | DOTA_ABILITY_BEHAVIOR_VECTOR_TARGETING"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"	
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"FightRecapLevel"				"1"
		"AbilitySound"					"Hero_VoidSpirit.AetherRemnant.Cast"
		"HasShardUpgrade"				"1"
		"AbilityCastPoint"				"0.0"
		"AbilityCastRange"				"850"
		"AbilityManaCost"				"75 80 85 90 95 100 105 110 115 120 120 120 120 120 120"
        "AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"
		"AbilityCastGestureSlot"		"DEFAULT"
        "MaxLevel"                      "15"
		"AbilityValues"
		{
			"start_radius"				
			{
				"value"	"90"
				"affected_by_aoe_increase"	"1"		
			}
			"end_radius"				
			{
				"value"	"90"
				"affected_by_aoe_increase"	"1"		
			}
			"radius"					
			{
				"value"		"300"
				"affected_by_aoe_increase"	"1"		
			}
			"projectile_speed"			"900"
			"remnant_watch_distance"
			{
				"value"						"450"
				"special_bonus_shard"		"+150"
				"affected_by_aoe_increase"	"1"		
			}
			"remnant_watch_radius"		
			{
				"value"		"130"
				"affected_by_aoe_increase"	"1"		
			}
			"watch_path_vision_radius"	
			{
				"value"		"200"
				"affected_by_aoe_increase"	"1"		
			}
			"activation_delay"		"0.4"
			"impact_damage"
			{
				"value"			"70 120 170 220"
				"special_bonus_unique_void_spirit_2"	"+50"
			}
			"pull_duration"			"1.0 1.0 1.0 1.0 1.2 1.2 1.2 1.2 1.4 1.4 1.4 1.4 1.6 1.6 1.6"
			"pull_destination"		"44 44 44 44 50 50 50 50 56 56 56 56 62 62 62"
			"duration"				"20.0"
			"think_interval"		"0.1"
			"pierces_creeps"
			{
				"value"							"0"
				"special_bonus_shard"			"+1"
			}
			"AbilityCooldown"				
			{
				"value"							"17.0 17.0 17.0 17.0 15.0 15.0 15.0 13.0 13.0 11.0 11.0 10.0 9.0 8.0 6.0"
				"special_bonus_shard"			"-2"
			}
			"damage_tick_rate"				
			{
				"value"						"0"
				"special_bonus_shard"		"+1"
			}
			"creep_damage_pct"					
			{
				"value"						"0"
				"special_bonus_shard"		"+30"
			}
			"artifice_duration_override"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+4.0"		// artifice_duration_override_tooltip
			}
			"artifice_pct_effectiveness"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+70.0"		// artifice_pct_effectiveness_tooltip
			}
		}
	}

    "void_spirit_dissimilate_custom"
	{
        "BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_void_spirit/void_spirit_dissimilate_custom"
		"AbilityTextureName"			"void_spirit_dissimilate"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilitySound"					"Hero_VoidSpirit.Dissimilate.Cast"
		"AbilityCastPoint"				"0.3"
		"AbilityCooldown"				"20 20 20 18 18 18 17 17 17 16 16 15 13 12 11"
		"AbilityDamage"					"120 120 200 200 280 280 360 360 440 440 520 520 520 520 600"
		"AbilityManaCost"				"120"
        "AbilityCastAnimation"		    "ACT_DOTA_CAST_ABILITY_3"
        "MaxLevel"                      "15"
		"AbilityValues"
		{
			"damage"					"120 120 200 200 280 280 360 360 440 440 520 520 520 520 600"
			"phase_duration"		"1.1"
			"destination_fx_radius" // proportional to distance_offset
			{
				"value"						"183"
				"affected_by_aoe_increase"	"1"		
			}
			"portals_per_ring"		"6"
			"angle_per_ring_portal"	"60"
			"first_ring_distance_offset"	
			{
				"value"		"520"
				"affected_by_aoe_increase"	"1"		
			}
			"damage_radius"	// proportional to distance_offset
			{
				"value"						"275"
				"special_bonus_unique_npc_dota_hero_void_spirit_int12" "+525"
				"affected_by_aoe_increase"	"1"		
			}
			"has_outer_ring"
			{
				"value"				"0"
				"special_bonus_unique_void_spirit_dissimilate_outerring"			"+1"
			}
			"aether_remnant_count"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+3"			// NOTE: Only works for +3 currently
			}
			"artifice_duration_override_tooltip"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+4.0"			// TOOLTIP ONLY
			}
			"artifice_pct_effectiveness_tooltip"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+70.0"			// TOOLTIP ONLY
			}
			"artifice_extra_offset"		// extra offset, in addition to remnant_watch_distance, from the vector drawn from center of middle portal to center of outer portal. Used to spread out remnants a bit so they don't "double pull"
			{
				"value"												"0"
				"special_bonus_facet_void_spirit_aether_artifice"	"+125"
			}
		}
	}

    "void_spirit_resonant_pulse_custom"
	{
        "BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_void_spirit/void_spirit_resonant_pulse_custom"
		"AbilityTextureName"			"void_spirit_resonant_pulse"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"	
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"FightRecapLevel"				"1"
		"AbilitySound"					"Hero_VoidSpirit.Pulse.Cast"
		"HasScepterUpgrade"			    "1"
		"AbilityCastRange"				"0"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_4"
		"AbilityCastGestureSlot"		"DEFAULT"
		"AbilityManaCost"				"120 120 120 120 120 120 120 120 120 120 120 120 120 120 120"
        "MaxLevel"                      "15"
		"AbilityValues"
		{
            "radius"
            {
                "value"	"500"
                "affected_by_aoe_increase"	"1"		
            }
            "speed"					"1200"
            "damage"				
            {
                "value"			"60 100 140 180 220 260 300 340 380 420 460 500 540 580 620"
            }
            "buff_duration"		
            {
                "value"			"10"
            }
            "base_absorb_amount"	
            {
                "value"			"25 50 75 100 125 150 175 200 225 250 275 300 325 350 400"
            }
            "absorb_per_hero_hit"	
            {
                "value"												"35 50 65 80 100 120 140 160 180 190 200 220 240 260 280"
                "special_bonus_facet_void_spirit_sanctuary"			"+15 +20 +25 +30 +30 +30 +30 +30 +30 +30 +30 +30 +30 +30 +30"
            }
            "is_all_barrier"
            {
                "value"												"0"
                "special_bonus_facet_void_spirit_sanctuary"			"+1"
            }
            "return_projectile_speed"	"900"
            "max_charges"		
            {
                "value"		"2"
                "RequiresScepter"			"1"
            }
            "charge_restore_time"
            {
                "value"		"18"
                "RequiresScepter"			"1"
            }
            "silence_duration_scepter"			
            {
                "value"		"2.0"
                "RequiresScepter"			"1"
            }
            "AbilityCharges"		
            {
                "value"				"1"
                "special_bonus_scepter" "=2"
            }
            "AbilityChargeRestoreTime"				
            {
                "value"				"18"
            }
		}
	}

    "void_spirit_astral_step_custom"
	{
        "BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_void_spirit/void_spirit_astral_step_custom"
		"AbilityTextureName"			"void_spirit_astral_step"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING | DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"FightRecapLevel"				"1"
		"AbilitySound"					"Hero_VoidSpirit.AstralStep.Start"
		"AbilityCastPoint"				"0.2"
		"AbilityCooldown"				"0"
		// "AbilityCharges"		        "2"
		"AbilityManaCost"				"50"
        "AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_2"
        "MaxLevel"                      "10"
		"AbilityValues"
		{
            "radius"				
            {
                "value"	"170"
                "affected_by_aoe_increase"	"1"		
            }
            "AbilityCooldown"	
            {
                "value"			"25 23 21 19 17 15 14 13 12 10"
                "special_bonus_unique_npc_dota_hero_void_spirit_agi9"	"=5"
            }
            "min_travel_distance"	"200"
            "max_travel_distance"	"800 800 800 900 900 900 1000 1000 1000 1200"
            "pop_damage_delay"		
            {
                "value"	"1.25"
                "DamageTypeTooltip"		"DAMAGE_TYPE_NONE"
            }
            "pop_damage"			
            {
                "value"					"130 200 270 340 410 480 550 650 750 800"
                "CalculateSpellDamageTooltip"	"1"
            }
            "movement_slow_pct"		"40 40 40 40 60 60 60 80 80 80"
            "ability_chance_pct"
            {
                "value"				"0"
            }
            "attack_chance_pct"
            {
                "value"				"0"
            }
		}
	}
}