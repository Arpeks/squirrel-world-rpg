LinkLuaModifier("modifier_void_spirit_astral_step_custom", "heroes/hero_void_spirit/void_spirit_astral_step_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_void_spirit_astral_step_custom_handler", "heroes/hero_void_spirit/void_spirit_astral_step_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_void_spirit_int11_bkb", "heroes/hero_void_spirit/void_spirit_astral_step_custom", LUA_MODIFIER_MOTION_NONE)

void_spirit_astral_step_custom = class({})

function void_spirit_astral_step_custom:Precache(context)
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_impact.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_debuff.vpcf", context )
    PrecacheResource( "particle", "particles/status_fx/status_effect_void_spirit_astral_step_debuff.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_dmg.vpcf", context )
end

function void_spirit_astral_step_custom:GetIntrinsicModifierName()
    return "modifier_void_spirit_astral_step_custom_handler"
end

function void_spirit_astral_step_custom:GetAbilityDamageType()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi12") then return DAMAGE_TYPE_PHYSICAL end
    return DAMAGE_TYPE_MAGICAL
end

function void_spirit_astral_step_custom:AddMark(enemy)
    local delay = self:GetSpecialValueFor( "pop_damage_delay" )
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi12") then
        delay = FrameTime()
    end
    enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay} )
    local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_impact.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy )
    ParticleManager:ReleaseParticleIndex( effect_cast )
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi13") then
        local rand = RandomInt(1, 100)
        if rand <= 5 then
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 2} )
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 3} )
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 4} )
        elseif rand <= 20 then
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 2} )
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 3} )
        elseif rand <= 35 then
            enemy:AddNewModifier( self:GetCaster(), self, "modifier_void_spirit_astral_step_custom",  {duration = delay * 2} )
        end
    end
end

function void_spirit_astral_step_custom:OnSpellStart()
    self:CastSpell(self:GetCaster(), self:GetCursorPosition())
end

function void_spirit_astral_step_custom:CastSpell(caster, point)
    local origin = caster:GetOrigin()
    local max_dist = self:GetSpecialValueFor( "max_travel_distance" )
    local min_dist = self:GetSpecialValueFor( "min_travel_distance" )
    if point == origin then 
        point = self:GetCaster():GetAbsOrigin() + self:GetCaster():GetForwardVector()*10
    end
    local vec = (point - caster:GetAbsOrigin())
    if vec:Length2D() > max_dist + caster:GetCastRangeBonus() then 
        point = caster:GetAbsOrigin() + vec:Normalized()*(max_dist + caster:GetCastRangeBonus())
    else 
        if vec:Length2D() < min_dist then 
            point = caster:GetAbsOrigin() + vec:Normalized()*min_dist
        end
    end
    if GridNav:CanFindPath(self:GetCaster():GetOrigin(), point) == false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        if player then
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_cast_here" })
            -- self:EndCooldown()
            -- self:RefundManaCost()
            return
        end
    end
    self:Strike(point)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int11") then
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_void_spirit_int11_bkb", {duration = 3})
    end
end

function void_spirit_astral_step_custom:Strike(point)
    local caster = self:GetCaster()
    local origin = self:GetCaster():GetAbsOrigin()
    local radius = self:GetSpecialValueFor( "radius" )
    local target = GetGroundPosition( point, nil )
    self:GetCaster():EmitSound("Hero_VoidSpirit.AstralStep.Start")
    FindClearSpaceForUnit( caster, target, true )
    self:GetCaster():EmitSound("Hero_VoidSpirit.AstralStep.End")
    local enemies = FindUnitsInLine( caster:GetTeamNumber(), origin, target, nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES )
    local no_cleave = caster:AddNewModifier(caster, self, "modifier_tidehunter_anchor_smash_caster", {})
    for _,enemy in pairs(enemies) do
        self:AddMark(enemy)
        caster:PerformAttack( enemy, true, true, true, false, true, false, true )
    end
    if no_cleave then 
        no_cleave:Destroy()
    end
    self:GetCaster():StartGesture(ACT_DOTA_CAST_ABILITY_2_END)
    local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( effect_cast, 0, origin )
	ParticleManager:SetParticleControl( effect_cast, 1, target )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

modifier_void_spirit_astral_step_custom = class({})
function modifier_void_spirit_astral_step_custom:IsHidden() return false end
function modifier_void_spirit_astral_step_custom:IsDebuff() return true end
function modifier_void_spirit_astral_step_custom:IsStunDebuff() return false end
function modifier_void_spirit_astral_step_custom:IsPurgable() return true end
function modifier_void_spirit_astral_step_custom:GetAttributes() return MODIFIER_ATTRIBUTE_MULTIPLE end
function modifier_void_spirit_astral_step_custom:OnCreated( kv )
    self.slow = -self:GetAbility():GetSpecialValueFor( "movement_slow_pct" )
    if not IsServer() then return end
end

function modifier_void_spirit_astral_step_custom:OnDestroy()
    if not IsServer() then return end
    if not self:GetParent() then return end
    if self:GetParent():IsNull() then return end
    if not self:GetParent():IsAlive() then return end
    self.damage = self:GetAbility():GetSpecialValueFor( "pop_damage" )
    local damage_type = self:GetAbility():GetAbilityDamageType()
    local special_bonus_unique_npc_dota_hero_void_spirit_agi12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi12")
    local bCritAttack = false
    if special_bonus_unique_npc_dota_hero_void_spirit_agi12 and RandomInt(1, 100) <= 100 then
        self.damage = self:GetCaster():GetAverageTrueAttackDamage(nil) * (1 + min(self:GetCaster():GetLevel() * 0.2, 60))
        bCritAttack = true
    end
    local damage_table = {
        attacker = self:GetCaster(), 
        damage = self.damage, 
        damage_type = self:GetAbility():GetAbilityDamageType(), 
        ability = self:GetAbility(), 
    }
    if self:GetAbility():GetAbilityDamageType() ~= DAMAGE_TYPE_MAGICAL then
        damage_table.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
    end


    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi11") then
        local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 400,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
        for _,enemy in pairs(enemies) do
            damage_table.victim = enemy
            local real_damage = ApplyDamageRDA(damage_table)
            if bCritAttack then
                local particle = ParticleManager:CreateParticle("particles/msg_fx/msg_crit.vpcf", PATTACH_OVERHEAD_FOLLOW, enemy)
                ParticleManager:SetParticleControl(particle, 1, Vector(9, math.floor(real_damage), 4))
                ParticleManager:SetParticleControl(particle, 2, Vector(2, string.len(tostring(math.floor(real_damage))) + 1, 0))
                ParticleManager:SetParticleControl(particle, 3, Vector(154, 12, 12))
                ParticleManager:ReleaseParticleIndex(particle)
            end
            local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_dmg.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy )
            ParticleManager:ReleaseParticleIndex( effect_cast )
        end
    else
        damage_table.victim = self:GetParent()
        local real_damage = ApplyDamageRDA(damage_table)
        if bCritAttack then
            local particle = ParticleManager:CreateParticle("particles/msg_fx/msg_crit.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
            ParticleManager:SetParticleControl(particle, 1, Vector(9, math.floor(real_damage), 4))
            ParticleManager:SetParticleControl(particle, 2, Vector(2, string.len(tostring(math.floor(real_damage))) + 1, 0))
            ParticleManager:SetParticleControl(particle, 3, Vector(154, 12, 12))
            ParticleManager:ReleaseParticleIndex(particle)
        end
        local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_dmg.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
        ParticleManager:ReleaseParticleIndex( effect_cast )
    end
    self:GetParent():EmitSound("Hero_VoidSpirit.AstralStep.MarkExplosion")
end

function modifier_void_spirit_astral_step_custom:DeclareFunctions()
	return
    {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_void_spirit_astral_step_custom:GetModifierMoveSpeedBonus_Percentage()
	return self.slow
end

function modifier_void_spirit_astral_step_custom:GetEffectName()
	return "particles/units/heroes/hero_void_spirit/astral_step/void_spirit_astral_step_debuff.vpcf"
end

function modifier_void_spirit_astral_step_custom:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_void_spirit_astral_step_custom:GetStatusEffectName()
	return "particles/status_fx/status_effect_void_spirit_astral_step_debuff.vpcf"
end

function modifier_void_spirit_astral_step_custom:StatusEffectPriority()
	return MODIFIER_PRIORITY_NORMAL
end

modifier_void_spirit_int11_bkb = class({})
function modifier_void_spirit_int11_bkb:IsHidden() return false end
function modifier_void_spirit_int11_bkb:IsDebuff() return false end
function modifier_void_spirit_int11_bkb:IsPurgable() return false end
function modifier_void_spirit_int11_bkb:IsPurgeException() return false end
function modifier_void_spirit_int11_bkb:IsStunDebuff() return false end
function modifier_void_spirit_int11_bkb:RemoveOnDeath() return true end
function modifier_void_spirit_int11_bkb:DestroyOnExpire() return true end

function modifier_void_spirit_int11_bkb:CheckState()
    return {
        [MODIFIER_STATE_DEBUFF_IMMUNE] = true,
    }
end

function modifier_void_spirit_int11_bkb:GetEffectName()
    return "particles/items_fx/black_king_bar_avatar.vpcf"
end

function modifier_void_spirit_int11_bkb:GetEffectAttachType()
    return PATTACH_ABSORIGIN_FOLLOW
end

modifier_void_spirit_astral_step_custom_handler = class({})
function modifier_void_spirit_astral_step_custom_handler:IsHidden() return true end
function modifier_void_spirit_astral_step_custom_handler:IsPurgable() return false end
function modifier_void_spirit_astral_step_custom_handler:IsPurgeException() return false end
function modifier_void_spirit_astral_step_custom_handler:RemoveOnDeath() return false end

function modifier_void_spirit_astral_step_custom_handler:OnCreated()
    self:SetHasCustomTransmitterData( true )
    if not IsServer() then return end
    self:StartIntervalThink(FrameTime())
end

function modifier_void_spirit_astral_step_custom_handler:OnIntervalThink()
    self.BaseDamage = self:GetParent():GetBaseDamageMin()
    self.DisplayAttackSpeed = self:GetParent():GetDisplayAttackSpeed()
    self.PrimaryAttribute = self:GetParent():GetPrimaryAttribute()
    self:SendBuffRefreshToClients()
end

function modifier_void_spirit_astral_step_custom_handler:AddCustomTransmitterData()
    return {
        BaseDamage = self.BaseDamage,
        DisplayAttackSpeed = self.DisplayAttackSpeed,
        PrimaryAttribute = self.PrimaryAttribute,
    }
end

function modifier_void_spirit_astral_step_custom_handler:HandleCustomTransmitterData( data )
    self.BaseDamage = data.BaseDamage
    self.DisplayAttackSpeed = data.DisplayAttackSpeed
    self.PrimaryAttribute = data.PrimaryAttribute
end

function modifier_void_spirit_astral_step_custom_handler:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
                
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,

        MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE,

        MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE
    }
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "pop_damage" then
			return 1
		end
	end
	return 0
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "pop_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "pop_damage", data.ability_special_level )

            -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str8") then
            --     value = value + self.BaseDamage
            -- end

            return value
		end
	end
	return 0
end

function modifier_void_spirit_astral_step_custom_handler:CustomOnAttackLanded(data)
    if data.attacker == self:GetParent() then
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int8") then
            local cd = self:GetAbility():GetCooldownTimeRemaining() - 0.1
            self:GetAbility():EndCooldown()
            self:GetAbility():StartCooldown(cd)
        end
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi8") then
            if RollPercentage(30) then
                self:GetAbility():AddMark(data.target)
            end
        end
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierSpellAmplify_Percentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi7") then
        return 2 * self.DisplayAttackSpeed
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierBonusStats_Strength()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi6") then
        return self:GetCaster():GetLevel() * 2
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierBonusStats_Agility()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi6") then
        return self:GetCaster():GetLevel() * 2
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierBonusStats_Intellect()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi6") then
        return self:GetCaster():GetLevel() * 2
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierPercentageCooldown(data)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_agi10") then
        if data and data.ability and data.ability:IsItem() then
            return 50
        end
    end
end

function modifier_void_spirit_astral_step_custom_handler:GetModifierBaseAttack_BonusDamage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str8") then
        return self:GetAbility():GetLevel() * 1000
    end
    return 0
end