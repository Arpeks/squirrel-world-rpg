LinkLuaModifier("modifier_custom_void_dissimilate", "heroes/hero_void_spirit/void_spirit_dissimilate_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_custom_void_dissimilate_intr", "heroes/hero_void_spirit/void_spirit_dissimilate_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_custom_void_dissimilate_cooldown", "heroes/hero_void_spirit/void_spirit_dissimilate_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_custom_void_dissimilate_int12", "heroes/hero_void_spirit/void_spirit_dissimilate_custom", LUA_MODIFIER_MOTION_NONE)

void_spirit_dissimilate_custom = class({})

function void_spirit_dissimilate_custom:Precache(context)
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_2.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_dmg.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_exit.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_int12.vpcf", context )
end

function void_spirit_dissimilate_custom:OnSpellStart()
    local caster = self:GetCaster()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int12") then
        if self.int12_thinker then
            UTIL_Remove(self.int12_thinker)
        end
        local duration = -1
        -- local duration = 13
        -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
        --     duration = -1
        -- end
        self.int12_thinker = CreateModifierThinker(caster, self, "modifier_custom_void_dissimilate_int12", {duration = duration}, caster:GetAbsOrigin(), caster:GetTeamNumber(), false)
        return
    end
    ProjectileManager:ProjectileDodge(caster)
    local duration = self:GetSpecialValueFor( "phase_duration" )
    caster:AddNewModifier( caster, self, "modifier_custom_void_dissimilate", { duration = duration } )
    self:GetCaster():EmitSound("Hero_VoidSpirit.Dissimilate.Cast")
end

function void_spirit_dissimilate_custom:GetIntrinsicModifierName()
    return "modifier_custom_void_dissimilate_intr"
end

function void_spirit_dissimilate_custom:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
        return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES + DOTA_ABILITY_BEHAVIOR_AUTOCAST
    end
    return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES
end

modifier_custom_void_dissimilate = class({})
function modifier_custom_void_dissimilate:IsHidden() return false end
function modifier_custom_void_dissimilate:IsDebuff() return false end
function modifier_custom_void_dissimilate:IsPurgable() return false end

function modifier_custom_void_dissimilate:OnCreated( data )
    self.portals = self:GetAbility():GetSpecialValueFor( "portals_per_ring" )
    self.angle = self:GetAbility():GetSpecialValueFor( "angle_per_ring_portal" )
    self.radius = self:GetAbility():GetSpecialValueFor( "damage_radius" )
    self.distance = self:GetAbility():GetSpecialValueFor( "first_ring_distance_offset" )
    self.target_radius = self:GetAbility():GetSpecialValueFor( "destination_fx_radius" )
    self.RemoveForDuel = true
    if not IsServer() then return end
    self.cast = data.not_cast == nil
    local origin = self:GetParent():GetOrigin()
    local direction = self:GetParent():GetForwardVector()
    local zero = Vector(0,0,0)
    self.selected = 1
    self.points = {}
    self.effects = {}
    table.insert( self.points, origin )
    table.insert( self.effects, self:PlayEffects1( origin, true ) )
    if self.cast then
        for i=1,self.portals do
            local new_direction = RotatePosition( zero, QAngle( 0, self.angle*i, 0 ), direction )
            local point = GetGroundPosition( origin + new_direction * self.distance, nil )
            if GridNav:CanFindPath(origin, point) then
                table.insert( self.points, point )
                table.insert( self.effects, self:PlayEffects1( point, false ) )
            end
        end
        if self:GetAbility():GetSpecialValueFor("has_outer_ring") == 1 then
            for i=1,self.portals do
                local new_direction = RotatePosition( zero, QAngle( 0, self.angle*i, 0 ), direction )
                local point = GetGroundPosition( origin + new_direction * self.distance * 2, nil )
                table.insert( self.points, point )
                table.insert( self.effects, self:PlayEffects1( point, false ) )
            end
        end
    end
    self.NoDraw = true
    self:GetParent():AddNoDraw()
end

function modifier_custom_void_dissimilate:OnIntervalThink()
    if not IsServer() then return end
    local damage = self:GetAbility():GetSpecialValueFor("damage")
    for _, point in pairs(self.points) do
        local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), point, nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
        for _,enemy in pairs(enemies) do
            local deal_damage = damage
            ApplyDamage({victim = enemy, attacker = self:GetCaster(), damage = deal_damage, damage_type = self:GetAbility():GetAbilityDamageType(), ability = self:GetAbility()})
        end
        self:PlayEffects2( point, #enemies )
    end
end

function modifier_custom_void_dissimilate:OnDestroy()
    if not IsServer() then return end
    self:GetParent():RemoveNoDraw()
    for _,effect in pairs(self.effects) do 
        ParticleManager:DestroyParticle(effect, true)
        ParticleManager:ReleaseParticleIndex(effect)
    end
    if not self.cast then
        return
    end
    local point = self.points[self.selected]
    local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), point, nil, self.radius,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
    local damage = self:GetAbility():GetSpecialValueFor("damage")
    local old_pos = self:GetCaster():GetAbsOrigin()
    FindClearSpaceForUnit( self:GetCaster(), point, true )
    for _,enemy in pairs(enemies) do
        local deal_damage = damage
        ApplyDamage({victim = enemy, attacker = self:GetCaster(), damage = deal_damage, damage_type = self:GetAbility():GetAbilityDamageType(), ability = self:GetAbility()})
    end
    self:PlayEffects2( point, #enemies )
    local void_spirit_aether_remnant_custom = self:GetCaster():FindAbilityByName("void_spirit_aether_remnant_custom")
    if void_spirit_aether_remnant_custom and void_spirit_aether_remnant_custom:GetSpecialValueFor("artifice_duration_override") > 0 and void_spirit_aether_remnant_custom:GetLevel() > 0 and self.selected ~= 1 then
        local points_checked = {}
        for id, point_checked in pairs(self.points) do
            if id ~= 1 and id ~= self.selected then
                table.insert(points_checked, point_checked)
            end
        end
        local random_points = {}
        for i=1, 3 do
            table.insert(random_points, table.remove(points_checked, RandomInt(1, #points_checked)))
        end
        table.sort(random_points, function(a, b)
            local distance_a = (self:GetCaster():GetAbsOrigin() - a):Length2D()
            local distance_b = (self:GetCaster():GetAbsOrigin() - b):Length2D()
            return distance_a > distance_b
        end)
        local max_spirits = 3
        for _, new_point in pairs(random_points) do
            if max_spirits <= 0 then break end
            max_spirits = max_spirits - 1
            local center = self.points[1]
            local direction = center - new_point
            direction.z = 0
            direction = direction:Normalized()
            CreateModifierThinker( self:GetCaster(), void_spirit_aether_remnant_custom, "modifier_custom_void_remnant_thinker", {
                dir_x = direction.x, 
                dir_y = direction.y, 
                dir_z = direction.z,
                new_point_x = new_point.x,
                new_point_y = new_point.y,
                new_point_z = new_point.z,
                new_duration = void_spirit_aether_remnant_custom:GetSpecialValueFor("artifice_duration_override")
        }, new_point, self:GetCaster():GetTeamNumber(), false)
        end
    end
end

function modifier_custom_void_dissimilate:DeclareFunctions()
	return
    {
		MODIFIER_EVENT_ON_ORDER,
		MODIFIER_PROPERTY_MOVESPEED_LIMIT,
	}
end

function modifier_custom_void_dissimilate:CustomOnOrder( params )
    if not self.cast then
        return
    end
	if params.unit~=self:GetParent() then return end
	if params.order_type==DOTA_UNIT_ORDER_MOVE_TO_POSITION then
		self:SetValidTarget( params.new_pos )
	elseif 
		params.order_type==DOTA_UNIT_ORDER_MOVE_TO_TARGET or
		params.order_type==DOTA_UNIT_ORDER_ATTACK_TARGET
	then
		self:SetValidTarget( params.target:GetOrigin() )
	end
end

function modifier_custom_void_dissimilate:GetModifierMoveSpeed_Limit()
	return 0.1
end

function modifier_custom_void_dissimilate:CheckState()
	return
	{
		[MODIFIER_STATE_DISARMED] = true,
		[MODIFIER_STATE_SILENCED] = true,
		[MODIFIER_STATE_MUTED] = true,
		[MODIFIER_STATE_OUT_OF_GAME] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true
	}
end

function modifier_custom_void_dissimilate:SetValidTarget( location )
	local max_dist = (location-self.points[1]):Length2D()
	local max_point = 1
	for i,point in ipairs(self.points) do
		local dist = (location-point):Length2D()
		if dist<max_dist then
			max_dist = dist
			max_point = i
		end
	end
	local old_select = self.selected
	self.selected = max_point
	self:ChangeEffects( old_select, self.selected )
end

function modifier_custom_void_dissimilate:PlayEffects1( point, main )
	local radius = self.radius + 25
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_int12.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
	ParticleManager:SetParticleControl( effect_cast, 0, point )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, self:GetRemainingTime(), 1 ) )
	if main then
		ParticleManager:SetParticleControl( effect_cast, 2, Vector( 1, self:GetRemainingTime(), 0 ) )
	end
	self:AddParticle( effect_cast, false, false, -1, false, false )
	EmitSoundOnLocationWithCaster( point, "Hero_VoidSpirit.Dissimilate.Portals", self:GetCaster() )
	return effect_cast
end

function modifier_custom_void_dissimilate:ChangeEffects( old, new )
	ParticleManager:SetParticleControl( self.effects[old], 2, Vector( 0, 0, 0 ) )
	ParticleManager:SetParticleControl( self.effects[new], 2, Vector( 1, 0, 0 ) )
end

function modifier_custom_void_dissimilate:PlayEffects2( point, hit )
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_dmg.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )
	ParticleManager:SetParticleControl( effect_cast, 0, point )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( self.target_radius, 0, 0 ) )
	ParticleManager:ReleaseParticleIndex( effect_cast )
	local particle_exit = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_exit.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
	ParticleManager:ReleaseParticleIndex( particle_exit )
	self:GetParent():StartGesture(ACT_DOTA_CAST_ABILITY_3_END)
	self:GetParent():EmitSound("Hero_VoidSpirit.Dissimilate.TeleportIn")
	if hit > 0 then
		self:GetParent():EmitSound("Hero_VoidSpirit.Dissimilate.Stun")
	end
end

modifier_custom_void_dissimilate_intr = class({})
function modifier_custom_void_dissimilate_intr:IsHidden() return true end
function modifier_custom_void_dissimilate_intr:IsDebuff() return false end
function modifier_custom_void_dissimilate_intr:IsPurgable() return false end
function modifier_custom_void_dissimilate_intr:IsPurgeException() return false end
function modifier_custom_void_dissimilate_intr:IsStunDebuff() return false end
function modifier_custom_void_dissimilate_intr:RemoveOnDeath() return false end
function modifier_custom_void_dissimilate_intr:DestroyOnExpire() return false end

function modifier_custom_void_dissimilate_intr:OnCreated()
    self.interval = 1
    if not IsServer() then return end
    
    self:StartIntervalThink(1)
end

function modifier_custom_void_dissimilate_intr:OnRefresh()
end

function modifier_custom_void_dissimilate_intr:OnIntervalThink()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
        if self.effect_cast then
            ParticleManager:DestroyParticle(self.effect_cast, false)
            ParticleManager:ReleaseParticleIndex(self.effect_cast)
        end

        if self:GetAbility():GetAutoCastState() and not self:GetCaster():IsSilenced() then
            self.radius = self:GetAbility():GetSpecialValueFor( "damage_radius" ) - 200
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
                self.damage = self:GetAbility():GetSpecialValueFor( "damage" ) / math.min(1, 1 / self:GetCaster():GetAttacksPerSecond(false) * 2)
            else
                self.damage = self:GetAbility():GetSpecialValueFor( "damage" )
            end
            local damage_table = {
                attacker = self:GetCaster(), 
                damage = self.damage, 
                damage_type = self:GetAbility():GetAbilityDamageType(), 
                ability = self:GetAbility()
            }
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int7") then
                damage_table.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
                damage_table.damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.7
            end

            local point = self:GetParent():GetAbsOrigin()
            local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), point, nil, self.radius,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
            for _,enemy in pairs(enemies) do
                damage_table.victim = enemy
                ApplyDamageRDA(damage_table)
            end
            
            local radius = self.radius + 25
            self.effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_int12.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
            ParticleManager:SetParticleControl( self.effect_cast, 0, point )
            ParticleManager:SetParticleControl( self.effect_cast, 1, Vector( radius, self.interval, 1 ) )
        end
        self.interval = 1
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
            self.interval = 0.5
        end
        self:StartIntervalThink(self.interval)
    else
        self:StartIntervalThink(1)
    end
end

function modifier_custom_void_dissimilate_intr:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_custom_void_dissimilate_intr:GetModifierHealthRegenPercentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str7") then
        return 0.2 * self:GetAbility():GetLevel()
    end
end

function modifier_custom_void_dissimilate_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_custom_void_dissimilate_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int7") then
                value = value + self:GetCaster():GetIntellect(true) * 2
            end

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str10") then
                value = value + self:GetCaster():GetStrength() * 0.5
            end

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int12") then
                value = value * 2
            end 

            return value
		end
	end
	return 0
end

function modifier_custom_void_dissimilate_intr:CustomOnTakeDamage(data)
    if data.unit == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int10") then
		if not self:GetParent():HasModifier("modifier_custom_void_dissimilate_cooldown") and self:GetParent():GetHealth() <= 5 then
			self:GetParent():SetHealth(self:GetParent():GetMaxHealth() * 0.6)
            self:GetCaster():AddNewModifier( self:GetCaster(), self:GetAbility(), "modifier_custom_void_dissimilate", { duration = 3, not_cast = true } )
            self:GetCaster():AddNewModifier( self:GetCaster(), self:GetAbility(), "modifier_custom_void_dissimilate_cooldown", { duration = 300 } )
		end
	end
end

modifier_custom_void_dissimilate_cooldown = class({})
function modifier_custom_void_dissimilate_cooldown:IsHidden() return false end
function modifier_custom_void_dissimilate_cooldown:IsDebuff() return false end
function modifier_custom_void_dissimilate_cooldown:IsPurgable() return false end
function modifier_custom_void_dissimilate_cooldown:IsPurgeException() return false end
function modifier_custom_void_dissimilate_cooldown:IsStunDebuff() return false end
function modifier_custom_void_dissimilate_cooldown:RemoveOnDeath() return false end
function modifier_custom_void_dissimilate_cooldown:DestroyOnExpire() return true end

modifier_custom_void_dissimilate_int12 = class({})
function modifier_custom_void_dissimilate_int12:IsHidden() return true end
function modifier_custom_void_dissimilate_int12:IsDebuff() return false end
function modifier_custom_void_dissimilate_int12:IsPurgable() return false end
function modifier_custom_void_dissimilate_int12:IsPurgeException() return false end
function modifier_custom_void_dissimilate_int12:IsStunDebuff() return false end
function modifier_custom_void_dissimilate_int12:RemoveOnDeath() return false end
function modifier_custom_void_dissimilate_int12:DestroyOnExpire() return true end

function modifier_custom_void_dissimilate_int12:OnCreated()
    self.radius = self:GetAbility():GetSpecialValueFor( "damage_radius" )
    self.interval = 1

    local radius = self.radius + 25
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_int12.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
    ParticleManager:SetParticleControl( effect_cast, 0, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, self.interval, 1 ) )

    if not IsServer() then return end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
        self.interval = 0.5
    end
    self:StartIntervalThink(self.interval)
end

function modifier_custom_void_dissimilate_int12:OnRefresh()
    self.radius = self:GetAbility():GetSpecialValueFor( "damage_radius" )
end

function modifier_custom_void_dissimilate_int12:OnIntervalThink()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int13") then
        self.damage = self:GetAbility():GetSpecialValueFor( "damage" ) / math.min(1, 1 / self:GetCaster():GetAttacksPerSecond(false) * 2)
    else
        self.damage = self:GetAbility():GetSpecialValueFor( "damage" )
    end
    
    local damage = self:GetAbility():GetSpecialValueFor("damage")
    local damage_table = {
        attacker = self:GetCaster(), 
        damage = self.damage, 
        damage_type = self:GetAbility():GetAbilityDamageType(), 
        ability = self:GetAbility()
    }
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int7") then
        damage_table.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
        damage_table.damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.7
    end
    local point = self:GetParent():GetAbsOrigin()
    local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), point, nil, self.radius,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
    for _,enemy in pairs(enemies) do
        damage_table.victim = enemy
        ApplyDamageRDA(damage_table)
    end
    if self.effect_cast then
        ParticleManager:DestroyParticle(self.effect_cast, false)
        ParticleManager:ReleaseParticleIndex(self.effect_cast)
    end
    local radius = self.radius + 25
	self.effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/dissimilate/void_spirit_dissimilate_int12.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
    ParticleManager:SetParticleControl( self.effect_cast, 0, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl( self.effect_cast, 1, Vector( radius, self.interval, 1 ) )
end

function modifier_custom_void_dissimilate_int12:OnDestroy()
    if not IsServer() then return end
    if self:GetParent():GetClassname() == "npc_dota_thinker" then
        UTIL_Remove(self:GetParent())
    end
end