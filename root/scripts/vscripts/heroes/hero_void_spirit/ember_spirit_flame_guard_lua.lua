LinkLuaModifier( "modifier_ember_spirit_flame_guard_lua", "heroes/hero_void_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if ember_spirit_flame_guard_lua == nil then
	ember_spirit_flame_guard_lua = class({})
end
function ember_spirit_flame_guard_lua:GetIntrinsicModifierName()
	return "modifier_ember_spirit_flame_guard_lua"
end
---------------------------------------------------------------------
--Modifiers
if modifier_ember_spirit_flame_guard_lua == nil then
	modifier_ember_spirit_flame_guard_lua = class({})
end
function modifier_ember_spirit_flame_guard_lua:OnCreated(params)
	if IsServer() then
	end
end
function modifier_ember_spirit_flame_guard_lua:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_ember_spirit_flame_guard_lua:OnDestroy()
	if IsServer() then
	end
end
function modifier_ember_spirit_flame_guard_lua:DeclareFunctions()
	return {
	}
end