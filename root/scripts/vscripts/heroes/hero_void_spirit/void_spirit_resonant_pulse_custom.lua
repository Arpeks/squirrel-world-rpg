LinkLuaModifier("modifier_void_spirit_resonant_pulse", "heroes/hero_void_spirit/void_spirit_resonant_pulse_custom", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier( "modifier_generic_ring_lua", "heroes/generic/modifier_generic_ring_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_void_spirit_resonant_pulse_custom_itnr", "heroes/hero_void_spirit/void_spirit_resonant_pulse_custom", LUA_MODIFIER_MOTION_NONE )

void_spirit_resonant_pulse_custom = class({})

function void_spirit_resonant_pulse_custom:Precache(context)
    PrecacheResource( "particle", "particles/status_fx/status_effect_void_spirit_pulse_buff.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_shield.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_buff.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_impact.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_absorb.vpcf", context )
    PrecacheResource( "particle", "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_shield_deflect.vpcf", context )
end

function void_spirit_resonant_pulse_custom:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str12") then
        return DOTA_ABILITY_BEHAVIOR_PASSIVE
    end
    return self.BaseClass.GetBehavior(self)
end

function void_spirit_resonant_pulse_custom:GetAbilityDamageType()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str6") then return DAMAGE_TYPE_PURE end
    return DAMAGE_TYPE_MAGICAL
end

function void_spirit_resonant_pulse_custom:OnSpellStart(ignore_pcf)
    local caster = self:GetCaster()
    local duration = self:GetSpecialValueFor( "buff_duration" )
    caster:AddNewModifier(caster, self, "modifier_void_spirit_resonant_pulse", { duration = duration, ignore_pcf = ignore_pcf })
end

function void_spirit_resonant_pulse_custom:GetIntrinsicModifierName()
    return "modifier_void_spirit_resonant_pulse_custom_itnr"
end

modifier_void_spirit_resonant_pulse = class({})

function modifier_void_spirit_resonant_pulse:CreateWave(caster, new_damage)
    self.max_shield = 1e5
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str6") then
        self.max_shield = 250000
    end

    self.radius = self:GetAbility():GetSpecialValueFor( "radius" ) + 90
    self.speed = self:GetAbility():GetSpecialValueFor( "speed" )
    self.damage = self:GetAbility():GetSpecialValueFor( "damage" )
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str12") then
        self.damage = self.damage + self:GetParent():GetDamageMax()
    end
    self.return_speed = self:GetAbility():GetSpecialValueFor( "return_projectile_speed" )
    self.absorb_per_hero_hit = self:GetAbility():GetSpecialValueFor("absorb_per_hero_hit")
    if not IsServer() then return end
    local damage_flags = DOTA_DAMAGE_FLAG_NONE
    if self:GetAbility():GetAbilityDamageType() == DAMAGE_TYPE_PURE then
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
        self.damage = self.damage * 0.7
    end
    self.damageTable = {attacker = caster, damage = self.damage, damage_type = self:GetAbility():GetAbilityDamageType(), ability = self:GetAbility(), damage_flags = damage_flags}
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str12") then
        self.damageTable.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str13") then
        self.damageTable.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
        self.damageTable.damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.05
    end
    local pulse = caster:AddNewModifier(caster, self:GetAbility(), "modifier_generic_ring_lua", {end_radius = self.radius, speed = self.speed, target_team = DOTA_UNIT_TARGET_TEAM_ENEMY, target_type = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC} )
    local pull_duration = self:GetAbility():GetSpecialValueFor( "duration" )
    local slow_duration = self:GetAbility():GetSpecialValueFor( "slow_duration" )
    local min_distance = self:GetAbility():GetSpecialValueFor( "min_distance" )
    local caster = self:GetCaster()
    pulse:SetCallback( function( enemy )
        self.damageTable.victim = enemy
        local new_damage = ApplyDamageRDA(self.damageTable)
        if pulse:GetCaster():HasModifier("modifier_void_spirit_resonant_pulse") then 
            self:PlayEffects3( enemy )
            self:PlayEffects4( pulse:GetCaster(), enemy )
            self:SetStackCount(min(self:GetStackCount() + self.absorb_per_hero_hit, self.max_shield))
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str6") then
                self:SetStackCount(min(self:GetStackCount() + new_damage * 0.5, self.max_shield))
            end
        end
    end)
    self:PlayEffects1(new_damage)
end

function modifier_void_spirit_resonant_pulse:OnCreated( kv )
    self.max_shield = 1e5
    self.max_shield = self:GetAbility():GetSpecialValueFor( "base_absorb_amount" )
    self.is_all_barrier = self:GetAbility():GetSpecialValueFor( "is_all_barrier" )
    self.parent = self:GetParent()
    if not IsServer() then return end
    self.shield = 0
    self.RemoveForDuel = true
    self:PlayEffects2()
    self:SetStackCount(self.max_shield)
    self:CreateWave(self:GetCaster())
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str6") then
        self.max_shield = 250000
    end
end

function modifier_void_spirit_resonant_pulse:OnRefresh(table)
    self.max_shield = self:GetAbility():GetSpecialValueFor( "base_absorb_amount" )
    self.parent = self:GetParent()
    if not IsServer() then return end
    if not table.ignore_pcf then
        self:PlayEffects2()
    end
    self.max_shield = 1e5
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str6") then
        self.max_shield = 250000
    end
    self:SetStackCount(min(self:GetStackCount() + self.max_shield, self.max_shield))
    self:CreateWave(self:GetCaster())
end

function modifier_void_spirit_resonant_pulse:OnDestroy()
    if not IsServer() then return end
    if self.effect_cast then
        ParticleManager:DestroyParticle(self.effect_cast, false)
        ParticleManager:ReleaseParticleIndex(self.effect_cast)
    end
    self:GetParent():EmitSound("Hero_VoidSpirit.Pulse.Destroy")
end

function modifier_void_spirit_resonant_pulse:DeclareFunctions()
    return
    {
        MODIFIER_PROPERTY_INCOMING_PHYSICAL_DAMAGE_CONSTANT,
        MODIFIER_PROPERTY_INCOMING_DAMAGE_CONSTANT,
        MODIFIER_PROPERTY_PROCATTACK_FEEDBACK
    }
end
function modifier_void_spirit_resonant_pulse:GetModifierIncomingPhysicalDamageConstant( params )
    if self.is_all_barrier > 0 then return end
    if IsClient() then 
        if params.report_max then 
            return self.max_shield 
        else 
            return self:GetStackCount()
        end 
    end
    self:PlayEffects5()
    if params.damage >= self:GetStackCount() then
        self:Destroy()
        return -self:GetStackCount()
    else
        self:SetStackCount(self:GetStackCount()-params.damage)
        return -params.damage
    end
end

function modifier_void_spirit_resonant_pulse:GetModifierIncomingDamageConstant( params )
    if self.is_all_barrier <= 0 then return end
    if IsClient() then 
        if params.report_max then 
            return self.max_shield 
        else 
            return self:GetStackCount()
        end 
    end
    self:PlayEffects5()
    if params.damage>=self:GetStackCount() then
        self:Destroy()
        return -self:GetStackCount()
    else
        self:SetStackCount(self:GetStackCount()-params.damage)
        return -params.damage
    end
end

function modifier_void_spirit_resonant_pulse:GetStatusEffectName()
	return "particles/status_fx/status_effect_void_spirit_pulse_buff.vpcf"
end

function modifier_void_spirit_resonant_pulse:StatusEffectPriority()
	return MODIFIER_PRIORITY_NORMAL
end

function modifier_void_spirit_resonant_pulse:PlayEffects1(new_damage)
    if not self then return end
    local radius = self.radius * 2
    local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse.vpcf", PATTACH_CUSTOMORIGIN, nil )
    ParticleManager:SetParticleControlEnt(effect_cast, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
    ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, radius, radius ) )
    ParticleManager:ReleaseParticleIndex( effect_cast )
    self:GetParent():EmitSound("Hero_VoidSpirit.Pulse")
end

function modifier_void_spirit_resonant_pulse:PlayEffects2()
    if not self then return end
    local radius = 130
    if self.effect_cast then
        ParticleManager:DestroyParticle(self.effect_cast, false)
        ParticleManager:ReleaseParticleIndex(self.effect_cast)
    end
    self.effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_shield.vpcf", PATTACH_POINT_FOLLOW, self:GetParent() )
    ParticleManager:SetParticleControl( self.effect_cast, 1, Vector( radius, radius, radius ) )
    ParticleManager:SetParticleControlEnt( self.effect_cast, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0), true )
    local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_buff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
    ParticleManager:ReleaseParticleIndex( effect_cast )
    self:GetParent():EmitSound("Hero_VoidSpirit.Pulse.Cast")
end

function modifier_void_spirit_resonant_pulse:PlayEffects3( target )
    if not self then return end
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_impact.vpcf", PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControlEnt( effect_cast, 0, target, PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0), true )
	ParticleManager:SetParticleControlEnt( effect_cast, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0), true )
	ParticleManager:ReleaseParticleIndex( effect_cast )
	target:EmitSound("Hero_VoidSpirit.Pulse.Target")
end

function modifier_void_spirit_resonant_pulse:PlayEffects4( parent, target )
    if not self then return end
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_absorb.vpcf", PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControlEnt( effect_cast, 0, target, PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0),  true )
	ParticleManager:SetParticleControlEnt( effect_cast, 1, parent, PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0), true  )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

function modifier_void_spirit_resonant_pulse:PlayEffects5()
    if not self then return end
	local radius = 100
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_void_spirit/pulse/void_spirit_pulse_shield_deflect.vpcf", PATTACH_POINT_FOLLOW, self:GetParent() )
	ParticleManager:SetParticleControlEnt( effect_cast, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", Vector(0,0,0), true )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, radius, radius ) )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

modifier_void_spirit_resonant_pulse_custom_itnr = class({})
function modifier_void_spirit_resonant_pulse_custom_itnr:IsHidden() return true end
function modifier_void_spirit_resonant_pulse_custom_itnr:IsDebuff() return false end
function modifier_void_spirit_resonant_pulse_custom_itnr:IsPurgable() return false end
function modifier_void_spirit_resonant_pulse_custom_itnr:IsPurgeException() return false end
function modifier_void_spirit_resonant_pulse_custom_itnr:IsStunDebuff() return false end
function modifier_void_spirit_resonant_pulse_custom_itnr:RemoveOnDeath() return false end
function modifier_void_spirit_resonant_pulse_custom_itnr:DestroyOnExpire() return false end

function modifier_void_spirit_resonant_pulse_custom_itnr:OnCreated()
    if not IsServer() then return end
    self:StartIntervalThink(0.3)
end

function modifier_void_spirit_resonant_pulse_custom_itnr:OnIntervalThink()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str12") then
        self:GetAbility():OnSpellStart(true)
    end
end

function modifier_void_spirit_resonant_pulse_custom_itnr:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
    }
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierBaseAttack_BonusDamage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str13") then
        return self:GetCaster():GetAttributesValue() * 10
    end
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int6") then
                value = value + self:GetCaster():GetIntellect(true)
            end

            return value
		end
	end
	return 0
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierMagicalResistanceBonus()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_int9") then
        return self:GetAbility():GetLevel() * 5
    end
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierAttackSpeedBonus_Constant()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str11") then
        return self:GetAbility():GetLevel() * 30
    end
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierBonusStats_Agility()
    return self:GetStackCount()
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierBonusStats_Intellect()
    return self:GetStackCount()
end

function modifier_void_spirit_resonant_pulse_custom_itnr:GetModifierBonusStats_Strength()
    return self:GetStackCount()
end

function modifier_void_spirit_resonant_pulse_custom_itnr:CustomOnDeath(data)
    if data.attacker == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_void_spirit_str9") and data.inflictor == self:GetAbility() then
        self:SetStackCount( self:GetStackCount() + RandomInt(0, 1))
    end
end