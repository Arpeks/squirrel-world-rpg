"DOTAAbilities"
{
	"juggernaut_requiem"
	{	
		"BaseClass"						            "ability_lua"
		"ScriptFile"		                        "heroes/hero_juggernaut/juggernaut_requiem/juggernaut_requiem"
		"AbilityTextureName"                        "jug"
		"AbilityBehavior"					        "DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitTargetTeam"				        "DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"				        "DOTA_UNIT_TARGET_ALL"
		"AbilityUnitDamageType"				        "DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"					        "SPELL_IMMUNITY_ENEMIES_NO"
		"AbilityCooldown"					        "16"
		"AbilityCastRange"					        "700"
		"MaxLevel"									"15"

		"AbilityValues"
		{
			"damage"
			{
				"value"                   "50 100 150 200 250 300 350 400 450 500 550 600 650 700 750"
			}
			"line"
			{
				"value"                   "1 1 2 2 3 3 4 4 5 5 6 6 7 7 8"
			}
			"lines_travel_speed"
			{
				"value"                   "1700"
			}
			"travel_distance"
			{
				"value"                   "700"
			}
			"lines_starting_width"
			{
				"value"                   "130"
				"affected_by_aoe_increase"	"1"
			}
			"lines_end_width"
			{
				"value"                   "130"
				"affected_by_aoe_increase"	"1"
			}
		}
	}	
}
