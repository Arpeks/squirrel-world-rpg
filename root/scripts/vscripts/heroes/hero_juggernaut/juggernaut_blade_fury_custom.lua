LinkLuaModifier( "modifier_juggernaut_blade_fury_custom", "heroes/hero_juggernaut/juggernaut_blade_fury_custom.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if juggernaut_blade_fury_custom == nil then
	juggernaut_blade_fury_custom = class({})
end
function juggernaut_blade_fury_custom:GetIntrinsicModifierName()
	return "modifier_juggernaut_blade_fury_custom"
end
---------------------------------------------------------------------
--Modifiers
if modifier_juggernaut_blade_fury_custom == nil then
	modifier_juggernaut_blade_fury_custom = class({})
end
function modifier_juggernaut_blade_fury_custom:OnCreated(params)
	if IsServer() then
	end
end
function modifier_juggernaut_blade_fury_custom:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_juggernaut_blade_fury_custom:OnDestroy()
	if IsServer() then
	end
end
function modifier_juggernaut_blade_fury_custom:DeclareFunctions()
	return {
	}
end