LinkLuaModifier( "modifier_muerta_dead_shot_custom", "heroes/hero_muerta/muerta_dead_shot_custom.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if muerta_dead_shot_custom == nil then
	muerta_dead_shot_custom = class({})
end
function muerta_dead_shot_custom:GetIntrinsicModifierName()
	return "modifier_muerta_dead_shot_custom"
end
---------------------------------------------------------------------
--Modifiers
if modifier_muerta_dead_shot_custom == nil then
	modifier_muerta_dead_shot_custom = class({})
end
function modifier_muerta_dead_shot_custom:OnCreated(params)
	if IsServer() then
	end
end
function modifier_muerta_dead_shot_custom:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_muerta_dead_shot_custom:OnDestroy()
	if IsServer() then
	end
end
function modifier_muerta_dead_shot_custom:DeclareFunctions()
	return {
	}
end