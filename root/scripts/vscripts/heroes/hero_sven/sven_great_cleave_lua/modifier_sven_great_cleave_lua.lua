LinkLuaModifier("modifier_magic_debuff", "heroes/hero_sven/modifier_magic_debuff.lua", LUA_MODIFIER_MOTION_NONE )

modifier_sven_great_cleave_lua = class({})

--------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:OnCreated( kv )
	self.great_cleave_damage = self:GetAbility():GetSpecialValueFor( "great_cleave_damage" )
	self.great_cleave_radius = self:GetAbility():GetSpecialValueFor( "great_cleave_radius" )
	self.cleave_starting_width = self:GetAbility():GetSpecialValueFor( "cleave_starting_width" )
	self.cleave_ending_width = self:GetAbility():GetSpecialValueFor( "cleave_ending_width" )
	self:StartIntervalThink(0.1)
end

--------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:OnRefresh( kv )
	self.great_cleave_damage = self:GetAbility():GetSpecialValueFor( "great_cleave_damage" )
	self.great_cleave_radius = self:GetAbility():GetSpecialValueFor( "great_cleave_radius" )
	self.cleave_starting_width = self:GetAbility():GetSpecialValueFor( "cleave_starting_width" )
	self.cleave_ending_width = self:GetAbility():GetSpecialValueFor( "cleave_ending_width" )
		local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_sven_agi9")
				if abil ~= nil then 
				self.great_cleave_damage = self.great_cleave_damage + 50
				end
		local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_sven_agi_last")
				if abil ~= nil then 
				self.great_cleave_damage = self.great_cleave_damage + 200
				end
end

function modifier_sven_great_cleave_lua:OnIntervalThink()
self:OnRefresh()
end


--------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:DeclareFunctions()
	local funcs = {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
		MODIFIER_PROPERTY_PROCATTACK_FEEDBACK,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:CustomOnAttackLanded( params )
	if IsServer() then
		if params.attacker == self:GetParent() and ( not self:GetParent():IsIllusion() ) then
			if self:GetParent():PassivesDisabled() then
				return 0
			end

			local target = params.target
			if target ~= nil and target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
			
				local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_sven_int7")
				if abil ~= nil then 
					params.target:AddNewModifier(self:GetCaster(), ability, "modifier_magic_debuff", {duration = 2})
				end
				
				local cleaveDamage = ( self.great_cleave_damage * params.damage ) / 100.0
				if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
					if params.target ~= nil and params.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
						local direction = params.target:GetOrigin()-self:GetParent():GetOrigin()
						direction.z = 0
						direction = direction:Normalized()
						local range = self:GetParent():GetOrigin() + direction*650/2
		
						local facing_direction = params.attacker:GetAnglesAsVector().y
						local cleave_targets = FindUnitsInRadius(params.attacker:GetTeamNumber(),params.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
						for _,target in pairs(cleave_targets) do
							if target ~= params.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
								local attacker_vector = (target:GetOrigin() - params.attacker:GetOrigin())
								local attacker_direction = VectorToAngles( attacker_vector ).y
								local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
								if angle_diff < 45 then
									ApplyDamageRDA({
										victim = target,
										attacker = params.attacker,
										damage = cleaveDamage,
										damage_type = DAMAGE_TYPE_PHYSICAL,
										damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
										ability = self:GetAbility()
									})
								end
							end
						end
						local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
						ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
						ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
						ParticleManager:ReleaseParticleIndex( effect_cast )
		
					end
				end
			end
		end
	end
end

----------------------------------------------------------------------------------------------------

function modifier_sven_great_cleave_lua:GetModifierPreAttack_CriticalStrike( params )
	if IsServer() and (not self:GetParent():PassivesDisabled()) then
	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_sven_agi8")
		if abil ~= nil then 
		if self:RollChance( 10) then
			self.record = params.record
			return 200
		end
		end
	end
end

function modifier_sven_great_cleave_lua:GetModifierProcAttack_Feedback( params )
	if IsServer() then
		if self.record then
			self.record = nil
		end
	end
end

function modifier_sven_great_cleave_lua:RollChance( chance )
	local rand = math.random()
	if rand<chance/100 then
		return true
	end
	return false
end