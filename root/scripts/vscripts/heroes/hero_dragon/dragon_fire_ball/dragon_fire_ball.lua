dragon_fire_ball_lua = class({})

LinkLuaModifier( "modifier_dragon_fire_ball_lua_instic", "heroes/hero_dragon/dragon_fire_ball/dragon_fire_ball", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_dragon_fire_ball_lua_thinker", "heroes/hero_dragon/dragon_fire_ball/modifier_dragon_fire_ball_lua_thinker", LUA_MODIFIER_MOTION_NONE )

function dragon_fire_ball_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function dragon_fire_ball_lua:GetBehavior()
	if self:GetCaster():HasModifier("modifier_hero_dragon_knight_buff_1") then
		return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
    return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
end

function dragon_fire_ball_lua:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function dragon_fire_ball_lua:GetIntrinsicModifierName()
	return "modifier_dragon_fire_ball_lua_instic"
end

function dragon_fire_ball_lua:OnSpellStart()
	self:OnSpellStart_point(self:GetCursorPosition())
end

function dragon_fire_ball_lua:OnSpellStart_point(point)
	local caster = self:GetCaster()
	duration = self:GetSpecialValueFor("duration")
	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_dragon_knight_int11")	
	if abil ~= nil then 
	duration = duration + 4
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_dragon_knight_int_last") ~= nil then
		duration = duration + 7
	end
	CreateModifierThinker(
		caster, -- player source
		self, -- ability source
		"modifier_dragon_fire_ball_lua_thinker", -- modifier name
		{ duration = duration }, -- kv
		point,
		caster:GetTeamNumber(),
		false
	)
end

modifier_dragon_fire_ball_lua_instic = class({})
--Classifications template
function modifier_dragon_fire_ball_lua_instic:IsHidden()
	return true
end

function modifier_dragon_fire_ball_lua_instic:IsDebuff()
	return false
end

function modifier_dragon_fire_ball_lua_instic:IsPurgable()
	return false
end

function modifier_dragon_fire_ball_lua_instic:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_dragon_fire_ball_lua_instic:IsStunDebuff()
	return false
end

function modifier_dragon_fire_ball_lua_instic:RemoveOnDeath()
	return false
end

function modifier_dragon_fire_ball_lua_instic:DestroyOnExpire()
	return false
end

function modifier_dragon_fire_ball_lua_instic:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(0.2)
end

function modifier_dragon_fire_ball_lua_instic:OnIntervalThink()
	if self:GetCaster():HasModifier("modifier_hero_dragon_knight_buff_1") and self:GetAbility():IsCooldownReady() and self:GetAbility():GetAutoCastState() then
		self:GetAbility():OnSpellStart_point(self:GetCaster():GetAbsOrigin())
        self:GetAbility():UseResources(true, false, false, true)
	end
end