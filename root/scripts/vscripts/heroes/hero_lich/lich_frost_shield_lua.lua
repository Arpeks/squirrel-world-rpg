LinkLuaModifier("modifier_lich_frost_shield_lua", "heroes/hero_lich/lich_frost_shield_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_frost_shield_lua_instic", "heroes/hero_lich/lich_frost_shield_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_frost_shield_lua_debuff", "heroes/hero_lich/lich_frost_shield_lua", LUA_MODIFIER_MOTION_NONE)

lich_frost_shield_lua = class({})

function lich_frost_shield_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi10") then
		return DOTA_ABILITY_BEHAVIOR_TOGGLE
	end
    return self.BaseClass.GetBehavior(self)
end

function lich_frost_shield_lua:GetManaCost(iLevel)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi10") then
		return 0
	end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function lich_frost_shield_lua:GetCastRange(vLocation, hTarget)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi10") then
		return 0
	end
    return self.BaseClass.GetCastRange(self, vLocation, hTarget)
end

function lich_frost_shield_lua:GetCooldown(iLevel)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi10") then
		return 0
	end
    return self.BaseClass.GetCooldown(self, iLevel)
end

function lich_frost_shield_lua:OnToggle()
    if self:GetToggleState() then
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_lich_frost_shield_lua", {})
    else
        self:GetCaster():RemoveModifierByName("modifier_lich_frost_shield_lua")
    end
end

function lich_frost_shield_lua:CastFilterResultTarget( hTarget )
    if not IsServer() then return UF_SUCCESS end
    local nResult = UnitFilter(
        hTarget,
        self:GetAbilityTargetTeam(),
        self:GetAbilityTargetType(),
        self:GetAbilityTargetFlags(),
        self:GetCaster():GetTeamNumber()
    )

    if hTarget:GetUnitName() == "npc_dota_lich_ice_spire" and hTarget:GetTeamNumber() == self:GetCaster():GetTeamNumber() then
        return UF_SUCCESS
    end
    if nResult ~= UF_SUCCESS then
        return nResult
    end

    return UF_SUCCESS
end

function lich_frost_shield_lua:OnSpellStart()
    if not IsServer() then return end
    local caster = self:GetCaster()
	local target = self:GetCursorTarget()
    local duration = self:GetSpecialValueFor("duration")
    target:AddNewModifier(self:GetCaster(), self, "modifier_lich_frost_shield_lua", {duration = duration})
    target:EmitSound("Hero_Lich.IceAge")
end

function lich_frost_shield_lua:GetIntrinsicModifierName()
    return "modifier_lich_frost_shield_lua_instic"
end

modifier_lich_frost_shield_lua = class({})

function modifier_lich_frost_shield_lua:IsPurgable() return true end
function modifier_lich_frost_shield_lua:IsPurgeException() return true end
function modifier_lich_frost_shield_lua:RemoveOnDeath() return true end

function modifier_lich_frost_shield_lua:OnCreated()
    self.bonus_damage = 0

    self.str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str13") ~= nil
    self.str12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str12") ~= nil

    if not IsServer() then return end
    self.damage_reduction = self:GetAbility():GetSpecialValueFor("damage_reduction")
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.slow_duration = self:GetAbility():GetSpecialValueFor("slow_duration")
    local interval = self:GetAbility():GetSpecialValueFor("interval")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str10") then
        if self:GetParent():GetUnitName() == "npc_dota_lich_ice_spire" then
            interval = math.max(0.3, math.min(1.3, 1 / self:GetCaster():GetAttacksPerSecond(false)))
        else
            interval = math.max(0.3, math.min(1.3, 1 / self:GetParent():GetAttacksPerSecond(false)))
        end
    end
    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_ice_age.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControlEnt(particle, 1, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetParent():GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(particle, 2, Vector(self.radius,self.radius,self.radius))
    self:AddParticle(particle, false, false, -1, false, false)

    self:StartIntervalThink(interval)

    self:SetHasCustomTransmitterData( true )
end

function modifier_lich_frost_shield_lua:OnRefresh()
    self.damage_reduction = self:GetAbility():GetSpecialValueFor("damage_reduction")
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.slow_duration = self:GetAbility():GetSpecialValueFor("slow_duration")
    self.str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str13") ~= nil
    self.str12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str12") ~= nil
end

function modifier_lich_frost_shield_lua:AddCustomTransmitterData()
	return {
		bonus_damage = self.bonus_damage,
	}
end

function modifier_lich_frost_shield_lua:HandleCustomTransmitterData( data )
	self.bonus_damage = data.bonus_damage
end

function modifier_lich_frost_shield_lua:IsAura() return true end
function modifier_lich_frost_shield_lua:GetModifierAura() return "modifier_lich_frost_shield_lua_debuff" end
function modifier_lich_frost_shield_lua:GetAuraRadius() return self.radius end
function modifier_lich_frost_shield_lua:GetAuraDuration() return 0.5 end
function modifier_lich_frost_shield_lua:GetAuraSearchTeam() return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_lich_frost_shield_lua:GetAuraSearchType() return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC end
function modifier_lich_frost_shield_lua:GetAuraSearchFlags() return 0 end

function modifier_lich_frost_shield_lua:GetStatusEffectName()
	return "particles/status_fx/status_effect_lich_ice_age.vpcf"
end

function modifier_lich_frost_shield_lua:GetEffectName()
    return "particles/units/heroes/hero_lich/lich_frost_armor.vpcf"
end

function modifier_lich_frost_shield_lua:GetEffectAttachType()
    return PATTACH_OVERHEAD_FOLLOW
end

function modifier_lich_frost_shield_lua:OnIntervalThink()
    if not IsServer() then return end

    if self:GetParent():IsHero() then
        self.bonus_damage = self:GetParent():GetDamageMax() * 0.7
    else
        self.bonus_damage = self:GetParent():GetDamageMax() * 0.35
    end
	self:SendBuffRefreshToClients()

    if not self:GetParent():IsAlive() then return end

    if not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi10") and self:GetDuration() < 0 then
        self:Destroy()
    end

    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_ice_age_dmg.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControlEnt(particle, 1, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetParent():GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(particle, 2, Vector(self.radius,self.radius,self.radius))
    ParticleManager:ReleaseParticleIndex(particle)
    self:GetParent():EmitSound("Hero_Lich.IceAge.Tick")
end

function modifier_lich_frost_shield_lua:DeclareFunctions()
    local dec = {
        MODIFIER_PROPERTY_INCOMING_PHYSICAL_DAMAGE_PERCENTAGE,
        MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
    }
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi6") then
        table.insert(dec, MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str8") then
        table.insert(dec, MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str11") then
        table.insert(dec, MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE)
    end
    return dec
end

function modifier_lich_frost_shield_lua:GetModifierIncomingPhysicalDamage_Percentage(params)
    return -(100 - self.damage_reduction)
end

function modifier_lich_frost_shield_lua:GetModifierDamageOutgoing_Percentage()
    if IsServer() then
        if self.str13_bonus and self.str13 then
            if self:GetParent() ~= self:GetCaster() then
                return 75
            end
            return 150
        end
    end
    return 0
end

function modifier_lich_frost_shield_lua:GetModifierPreAttack_BonusDamage()
    return self.bonus_damage
end

function modifier_lich_frost_shield_lua:GetModifierMagicalResistanceBonus()
    return self:GetAbility():GetLevel() * 3
end

function modifier_lich_frost_shield_lua:CustomOnAttackLanded( params )
	if self:GetParent() ~= params.attacker then
		return
	end
    if not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str7") then
        return
    end
	local heal = params.damage * 17/100
	self:GetParent():HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), false)
	self:PlayEffects( self:GetParent() )
end

function modifier_lich_frost_shield_lua:PlayEffects( target )
	local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
	local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControl( effect_cast, 1, target:GetOrigin() )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

function modifier_lich_frost_shield_lua:GetModifierPreAttack_CriticalStrike(keys)
	if (keys.target and not keys.target:IsOther() and not keys.target:IsBuilding() and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber()) and RandomInt(1, 100) <= 20 then
		return 200
	end
end

modifier_lich_frost_shield_lua_debuff = class({})

function modifier_lich_frost_shield_lua_debuff:OnCreated()
    self.movement_slow = self:GetAbility():GetSpecialValueFor("movement_slow")

    if not IsServer() then return end
    self.mod = self:GetAuraOwner():FindModifierByName("modifier_lich_frost_shield_lua")

    local interval = self:GetAbility():GetSpecialValueFor("interval")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str10") then
        if self:GetAuraOwner():GetUnitName() == "npc_dota_lich_ice_spire" then
            interval = math.max(0.3, math.min(1.3, 1 / self:GetCaster():GetAttacksPerSecond(false)))
        else
            interval = math.max(0.3, math.min(1.3, 1 / self:GetAuraOwner():GetAttacksPerSecond(false)))
        end
    end
    self:StartIntervalThink(interval)
end

function modifier_lich_frost_shield_lua_debuff:OnRefresh()
    self.movement_slow = self:GetAbility():GetSpecialValueFor("movement_slow")
    if not IsServer() then return end
end

function modifier_lich_frost_shield_lua_debuff:OnIntervalThink()
    local damage = self:GetAbility():GetSpecialValueFor("damage")

    ApplyDamageRDA({victim = self:GetParent(), attacker = self:GetCaster(), damage = damage, damage_type = DAMAGE_TYPE_MAGICAL, ability = self:GetAbility()})
    if self.mod and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str12") then
        self.mod.str13_bonus = true
        if self:GetAuraOwner():GetUnitName() == "npc_dota_lich_ice_spire" then
            self:GetCaster():PerformAttack(self:GetParent(), true, true, true, true, false, false, false)
        else
            self:GetAuraOwner():PerformAttack(self:GetParent(), true, false, true, true, false, false, false)
        end
        self.mod.str13_bonus = false
    end
end

function modifier_lich_frost_shield_lua_debuff:GetStatusEffectName()
	return "particles/status_fx/status_effect_lich_ice_age.vpcf"
end

function modifier_lich_frost_shield_lua_debuff:DeclareFunctions()
    local dec = {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
    }
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int9") then
        table.insert(dec, MODIFIER_PROPERTY_HP_REGEN_AMPLIFY_PERCENTAGE)
    end
    return dec
end

function modifier_lich_frost_shield_lua_debuff:GetModifierMoveSpeedBonus_Percentage()
    return self.movement_slow
end

function modifier_lich_frost_shield_lua_debuff:GetModifierHPRegenAmplify_Percentage()
    return -75
end

modifier_lich_frost_shield_lua_instic = class({})
function modifier_lich_frost_shield_lua_instic:IsHidden() return true end
function modifier_lich_frost_shield_lua_instic:IsDebuff() return false end
function modifier_lich_frost_shield_lua_instic:IsPurgable() return false end
function modifier_lich_frost_shield_lua_instic:IsPurgeException() return false end
function modifier_lich_frost_shield_lua_instic:IsStunDebuff() return false end
function modifier_lich_frost_shield_lua_instic:RemoveOnDeath() return false end
function modifier_lich_frost_shield_lua_instic:DestroyOnExpire() return false end

function modifier_lich_frost_shield_lua_instic:OnCreated()
    if not IsServer() then return end
    self:StartIntervalThink(1)
end

function modifier_lich_frost_shield_lua_instic:OnIntervalThink()
    if not IsServer() then return end
end

function modifier_lich_frost_shield_lua_instic:DeclareFunctions()
    return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_lich_frost_shield_lua_instic:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
        if data.ability_special_value == "duration" then
			return 1
		end
	end
	return 0
end

function modifier_lich_frost_shield_lua_instic:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int6") then
                value = value + self:GetCaster():GetIntellect(true) * 0.7
            end

            return value
		end
        if data.ability_special_value == "duration" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "duration", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi9") then
                value = 120
            end

            return value
		end
	end
	return 0
end