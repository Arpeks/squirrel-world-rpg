LinkLuaModifier("modifier_lich_frost_nova_lua", "heroes/hero_lich/lich_frost_nova_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_frost_nova_lua_hp_regen", "heroes/hero_lich/lich_frost_nova_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_frost_nova_lua_thinker_cast", "heroes/hero_lich/lich_frost_nova_lua", LUA_MODIFIER_MOTION_NONE)

lich_frost_nova_lua = class({})

function lich_frost_nova_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function lich_frost_nova_lua:OnSpellStart()
	if not IsServer() then return end
	local caster = self:GetCaster()
	local target = self:GetCursorTarget()
    local damage = self:GetAbilityDamage()
	local duration = self:GetDuration()
	local radius = self:GetSpecialValueFor("radius")
    local caster = self:GetCaster()

    if target:TriggerSpellAbsorb( self ) then
        self:PlayEffects()
        return
    end
    local enemies = FindUnitsInRadius( caster:GetTeamNumber(), target:GetOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
    local damageTable = { victim = target, attacker = caster, damage = damage, damage_type = DAMAGE_TYPE_MAGICAL, ability = self }
    if target:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
        ApplyDamageRDA(damageTable)
    end
    damageTable.damage = damage
    for _,enemy in pairs(enemies) do
        damageTable.victim = enemy
        ApplyDamageRDA( damageTable )
        enemy:AddNewModifier( caster, self, "modifier_lich_frost_nova_lua", { duration = duration * (1-enemy:GetStatusResistance()) } )
    end
    self:PlayEffects( target, radius )
end

function lich_frost_nova_lua:PlayEffects( target, radius )
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_lich/lich_frost_nova.vpcf", PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, radius, radius ) )
	ParticleManager:ReleaseParticleIndex( effect_cast )
	target:EmitSound("Ability.FrostNova")
end

function lich_frost_nova_lua:PlayEffectsPoint( point, radius )
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_lich/lich_frost_nova.vpcf", PATTACH_WORLDORIGIN, nil )
    ParticleManager:SetParticleControl( effect_cast, 0, point )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, radius, radius ) )
	ParticleManager:ReleaseParticleIndex( effect_cast )
	EmitSoundOnLocationWithCaster(point, "Ability.FrostNova", self:GetCaster())
end

modifier_lich_frost_nova_lua = class({})

function modifier_lich_frost_nova_lua:OnCreated( kv )
	self.as_slow = self:GetAbility():GetSpecialValueFor( "slow_attack_speed_primary" )
	self.ms_slow = self:GetAbility():GetSpecialValueFor( "slow_movement_speed" )
end

function modifier_lich_frost_nova_lua:OnRefresh( kv )
	self.as_slow = self:GetAbility():GetSpecialValueFor( "slow_attack_speed_primary" )
	self.ms_slow = self:GetAbility():GetSpecialValueFor( "slow_movement_speed" )
end

function modifier_lich_frost_nova_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end
function modifier_lich_frost_nova_lua:GetModifierMoveSpeedBonus_Percentage()
	return self.ms_slow
end
function modifier_lich_frost_nova_lua:GetModifierAttackSpeedBonus_Constant()
	return self.as_slow
end
function modifier_lich_frost_nova_lua:GetStatusEffectName()
	return "particles/status_fx/status_effect_frost_lich.vpcf"
end