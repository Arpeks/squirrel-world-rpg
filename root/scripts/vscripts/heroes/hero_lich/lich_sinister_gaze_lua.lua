LinkLuaModifier("modifier_lich_sinister_gaze_lua", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_sinister_gaze_lua_debuff", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_sinister_gaze_lua_buff", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_sinister_gaze_creep_thinker", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_sinister_gaze_attack_debuff", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_sinister_gaze_attack_buff", "heroes/hero_lich/lich_sinister_gaze_lua", LUA_MODIFIER_MOTION_NONE)

lich_sinister_gaze_lua = class({})

function lich_sinister_gaze_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function lich_sinister_gaze_lua:GetIntrinsicModifierName()
    return "modifier_lich_sinister_gaze_lua"
end

function lich_sinister_gaze_lua:GetCustomCastErrorTarget(target)
	if target:IsAncient() then
		return "cannot_be_used_on_ancients"
	end
end

function lich_sinister_gaze_lua:GetBehavior()
	local base_beh = self.BaseClass.GetBehavior(self)
	return base_beh
end

function lich_sinister_gaze_lua:OnSpellStart()
    if not IsServer() then return end
	self.target = self:GetCursorTarget()
    if self.target:TriggerSpellAbsorb( self ) then
		self:GetCaster():Stop()
        self:GetCaster():Interrupt()
		return
	end
    self:GetCaster():EmitSound("Hero_Lich.SinisterGaze.Cast")

    self.target:EmitSound("Hero_Lich.SinisterGaze.Target")
    self.target:AddNewModifier(self:GetCaster(), self, "modifier_lich_sinister_gaze_lua_debuff", {duration = self:GetChannelTime()})
    self.target:AddNewModifier(self:GetCaster(), nil, "modifier_truesight", {duration = self:GetChannelTime()})
end

function lich_sinister_gaze_lua:OnChannelFinish(bInterrupted)
	if not IsServer() then return end
    if self.target then
        self.target:RemoveModifierByName("modifier_lich_sinister_gaze_lua_debuff")
        self.target:RemoveModifierByName("modifier_truesight")
		if not bInterrupted then
			self:EatEnemy( self.target )
		end
    end
end

function lich_sinister_gaze_lua:EatEnemy( enemy )
	if enemy:IsAlive() and not enemy:IsAncient() and enemy:GetUnitName() ~= "npc_dota_hero_target_dummy" then
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi12") then
			self:CaptureCreep( enemy )
		end
		enemy:Kill(self, self:GetCaster())
		local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_lich_sinister_gaze_lua_buff", {})
		local add_stack = self:GetSpecialValueFor("bonus_intellect")
		mod:SetStackCount(mod:GetStackCount() + add_stack)
	end
end

function lich_sinister_gaze_lua:CaptureCreep( creep )
	if self.capture_creep and not self.capture_creep:IsNull() and self.capture_creep:IsAlive() then
		UTIL_Remove(self.capture_creep)
		self.capture_creep = nil
	end
	local unit_name = creep:GetUnitName()
	self.capture_creep = CreateUnitByName(unit_name, creep:GetAbsOrigin(), true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
	self.capture_creep:SetBaseAttackTime(1.5)
    for i = 0, self.capture_creep:GetAbilityCount() - 1 do
        local ability = self.capture_creep:GetAbilityByIndex(i)
        if ability then
            self.capture_creep:RemoveAbility(ability:GetAbilityName())
        end
    end
	for _, modifier in pairs(self.capture_creep:FindAllModifiers()) do
        self.capture_creep:RemoveModifierByName(modifier:GetName())
    end
	self.capture_creep:AddNewModifier(self:GetCaster(), self, "modifier_lich_sinister_gaze_creep_thinker", {  })
	self.capture_creep:SetRenderColor(0, 94, 255)
	for _, modifier_name in pairs({"modifier_gold_creep"}) do
		if creep:HasModifier(modifier_name) then
			self.capture_creep:AddNewModifier(self:GetCaster(), self, modifier_name, {  })
		end
	end
end

modifier_lich_sinister_gaze_lua = class({})
function modifier_lich_sinister_gaze_lua:IsHidden() return true end
function modifier_lich_sinister_gaze_lua:IsDebuff() return false end
function modifier_lich_sinister_gaze_lua:IsPurgable() return false end
function modifier_lich_sinister_gaze_lua:IsPurgeException() return false end
function modifier_lich_sinister_gaze_lua:IsStunDebuff() return false end
function modifier_lich_sinister_gaze_lua:RemoveOnDeath() return false end
function modifier_lich_sinister_gaze_lua:DestroyOnExpire() return false end

function modifier_lich_sinister_gaze_lua:OnCreated()
	if not IsServer() then return end
	self:StartIntervalThink(0.2)
end

function modifier_lich_sinister_gaze_lua:OnIntervalThink()
	self:SetStackCount(self:GetCaster():GetSecondaryAttributesValue())
end

function modifier_lich_sinister_gaze_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}
end

function modifier_lich_sinister_gaze_lua:GetModifierAttackSpeedBonus_Constant()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi13") then
		return 500
	end
end

function modifier_lich_sinister_gaze_lua:GetModifierProjectileSpeedBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi8") then
		return 1000
	end
	return 0
end

function modifier_lich_sinister_gaze_lua:GetModifierPreAttack_BonusDamage()
	local v = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi7") then
		v = v + 2 * (self:GetCaster():GetAttributesValue() - self:GetStackCount())
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi11") then
		v = v + 1 * self:GetCaster():GetAttributesValue()
	end
	return v
end

function modifier_lich_sinister_gaze_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "AbilityChannelTime" then
			return 1
		end
        if data.ability_special_value == "channel_duration" then
			return 1
		end
	end
	return 0
end

function modifier_lich_sinister_gaze_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "AbilityChannelTime" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "AbilityChannelTime", data.ability_special_level )
            return value
		end
        if data.ability_special_value == "channel_duration" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "channel_duration", data.ability_special_level )
            return value
		end
	end
	return 0
end

function modifier_lich_sinister_gaze_lua:GetModifierAttackRangeBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int7") then
		return 200
	end
	return 0
end

function modifier_lich_sinister_gaze_lua:CustomOnAttackLanded(keys)
	if not IsServer() then return end
	if keys.attacker:GetTeamNumber() == self:GetParent():GetTeamNumber() and keys.attacker == self:GetParent() then
		local target = keys.target
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int10") then
			target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_lich_sinister_gaze_attack_debuff", { duration = 10 })
		end
	end
end

modifier_lich_sinister_gaze_lua_buff = class({})

function modifier_lich_sinister_gaze_lua_buff:IsHidden() return false end

function modifier_lich_sinister_gaze_lua_buff:IsPurgable() return false end

function modifier_lich_sinister_gaze_lua_buff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
	}
end

function modifier_lich_sinister_gaze_lua_buff:GetModifierBonusStats_Intellect()
	return self:GetStackCount()
end

modifier_lich_sinister_gaze_lua_debuff = class({})

function modifier_lich_sinister_gaze_lua_debuff:GetStatusEffectName()
	return "particles/status_fx/status_effect_lich_gaze.vpcf"
end

function modifier_lich_sinister_gaze_lua_debuff:OnCreated()
	if not IsServer() then return end
    self.interval = FrameTime()
    self.current_mana = 0
    if self:GetParent().GetMana then
		self.current_mana = self:GetParent():GetMana()
	end
    self.destination = self:GetAbility():GetSpecialValueFor("destination")
    self.duration = self:GetRemainingTime()
    self.mana_drain = self:GetAbility():GetSpecialValueFor("mana_drain")
	self.mana_per_interval	= (self.current_mana * self.mana_drain * 0.01) / (self.duration / self.interval)
    self.damage = self:GetAbility():GetSpecialValueFor("creep_damage") * self.interval
    self.distance = CalcDistanceBetweenEntityOBB(self:GetCaster(), self:GetParent()) * (self.destination / 100)
    self.status_resistance = self:GetParent():GetStatusResistance()

	if self:GetCaster():GetName() == "npc_dota_hero_lich" then
		self.particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_gaze.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControlEnt(self.particle, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetParent():GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(self.particle, 2, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_portrait", self:GetCaster():GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(self.particle, 3, self:GetCaster(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetCaster():GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(self.particle, 10, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetParent():GetAbsOrigin(), true)
		self:AddParticle(self.particle, false, false, -1, false, false)
        ----------------------------------------------------------------------------------------------------------------------------------------------------------
		self.particle2 = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_gaze_eyes.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControlEnt(self.particle2, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_eye_l", self:GetCaster():GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(self.particle2, 2, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_eye_r", self:GetCaster():GetAbsOrigin(), true)
		self:AddParticle(self.particle2, false, false, -1, false, false)
	end

	self:GetParent():Interrupt()
	self:StartIntervalThink(self.interval)
end

function modifier_lich_sinister_gaze_lua_debuff:OnIntervalThink()
	if self:GetCaster():IsNull() or self:GetAbility():IsNull() or not self:GetAbility():IsChanneling() then
		self:Destroy()
	else
        if not self:GetParent():IsHero() then
            ApplyDamageRDA({victim = self:GetParent(), attacker = self:GetCaster(), damage = self.damage, damage_type = DAMAGE_TYPE_MAGICAL, ability = self:GetAbility()})
        end
	end
end

function modifier_lich_sinister_gaze_lua_debuff:OnDestroy()
	if not IsServer() then return end
	self:GetParent():Interrupt()
    self:GetParent():Stop()
	GridNav:DestroyTreesAroundPoint(self:GetParent():GetAbsOrigin(), 100, false)
end

function modifier_lich_sinister_gaze_lua_debuff:CheckState()
	return 
    {
		[MODIFIER_STATE_HEXED] = true,
		[MODIFIER_STATE_SILENCED] = true,
		[MODIFIER_STATE_MUTED] = true,
		[MODIFIER_STATE_COMMAND_RESTRICTED] = true,
		[MODIFIER_STATE_FLYING_FOR_PATHING_PURPOSES_ONLY] = true
	}
end

function modifier_lich_sinister_gaze_lua_debuff:DeclareFunctions()
	return {MODIFIER_PROPERTY_MOVESPEED_LIMIT}
end

function modifier_lich_sinister_gaze_lua_debuff:GetModifierMoveSpeed_Limit()
	if not IsServer() then return end
	return self.distance / (self:GetAbility():GetChannelTime() * (1 - math.min(self.status_resistance, 0.9999)))
end






modifier_lich_sinister_gaze_creep_thinker = class({})

function modifier_lich_sinister_gaze_creep_thinker:IsHidden() return true end
function modifier_lich_sinister_gaze_creep_thinker:IsPurgable() return false end
function modifier_lich_sinister_gaze_creep_thinker:RemoveOnDeath() return false end

function modifier_lich_sinister_gaze_creep_thinker:OnCreated()
	self.distance_to_owner = 0
	self.damage = 0
    if IsServer() then
        self.owner = self:GetCaster() -- Хозяин юнита
        self.follow_distance = 200 -- Расстояние следования за спиной
        self.attack_distance = 900 -- Дистанция обнаружения врагов
        self.attack_mode = false -- Активен ли режим атаки

        -- Начать обновление логики каждые 0.2 секунды
        self:StartIntervalThink(0.2)
		self:SetHasCustomTransmitterData( true )
    end
end

function modifier_lich_sinister_gaze_creep_thinker:OnDestroy()
	if IsServer() then
		UTIL_Remove(self:GetParent())
	end
end

function modifier_lich_sinister_gaze_creep_thinker:AddCustomTransmitterData()
	return {
		distance_to_owner = self.distance_to_owner,
		damage = self.damage,
	}
end

function modifier_lich_sinister_gaze_creep_thinker:HandleCustomTransmitterData( data )
	self.distance_to_owner = data.distance_to_owner
	self.damage = data.damage
end

function modifier_lich_sinister_gaze_creep_thinker:OnIntervalThink()
    if not IsServer() then return end


    local creep = self:GetParent()

	-- Если хозяин атакует, атаковать ту же цель
    local owner_target = self.owner:GetAggroTarget()
    
	local hero_position = self.owner:GetAbsOrigin()
    local creep_position = creep:GetAbsOrigin()
    local follow_position = hero_position - self.owner:GetForwardVector() * self.follow_distance
	self.distance_to_owner = (creep_position - hero_position):Length2D()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi13") then
		self.damage = self:GetCaster():GetDamageMax() + self:GetCaster():GetAverageTrueAttackDamage(self:GetCaster())
	else
		self.damage = self:GetCaster():GetDamageMax()
	end
	self:SendBuffRefreshToClients()
	

	if self.previous_position and self.previous_position == creep_position and self.distance_to_owner > 1000 then
        FindClearSpaceForUnit(creep, hero_position, true)
		self.previous_position = creep_position -- Обновляем позицию после телепортации
		return
    end

    self.previous_position = creep_position

	if owner_target and owner_target:IsAlive() and not self.attack_mode then
        -- Включить режим атаки
        self.attack_mode = true
		self.attack_unit_index = owner_target:GetEntityIndex()
        creep:MoveToTargetToAttack(owner_target)
        return
    end

    -- Если хозяин прекратил сражение, вернуть юнита к хозяину
    if not owner_target and self.attack_mode then
        self.attack_mode = false
    end

    if self.attack_mode then
        -- Проверить, если цель недоступна, выйти из режима атаки
        local creep_target = creep:GetAttackTarget()
        if not creep_target or not creep_target:IsAlive() or self.attack_unit_index ~= owner_target:GetEntityIndex() then
            self.attack_mode = false
			self:OnIntervalThink()
        end
        return
    end

    -- Если крип не на позиции, двигаться к хозяину
    if (creep_position - follow_position):Length2D() > 50 then
        creep:MoveToPosition(follow_position)
    end
end

function modifier_lich_sinister_gaze_creep_thinker:CheckState()
	local state = {
		-- [MODIFIER_STATE_INVULNERABLE] = true,
		-- [MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		-- [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		-- [MODIFIER_STATE_UNSELECTABLE] =1 false,
		[MODIFIER_STATE_FLYING_FOR_PATHING_PURPOSES_ONLY] = true,
		[MODIFIER_STATE_ATTACK_IMMUNE] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_INVISIBLE] = true,
	}
	return state
end

function modifier_lich_sinister_gaze_creep_thinker:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		-- MODIFIER_PROPERTY_LIFETIME_FRACTION,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
	}
end

function modifier_lich_sinister_gaze_creep_thinker:GetModifierMoveSpeed_Absolute()
	return math.max(600, self.distance_to_owner)
end

-- function modifier_lich_sinister_gaze_creep_thinker:GetUnitLifetimeFraction( params )
-- 	return ( ( self:GetDieTime() - GameRules:GetGameTime() ) / self:GetDuration() )
-- end

function modifier_lich_sinister_gaze_creep_thinker:GetModifierAttackSpeedBonus_Constant()
	local bonus = 0
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi12") then
		bonus = bonus + 100
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi13") then
		bonus = bonus + 400
	end
	return bonus
end

function modifier_lich_sinister_gaze_creep_thinker:GetModifierAttackRangeBonus()
	local bonus = 0
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi12") then
		bonus = bonus + 50
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_agi13") then
		bonus = bonus + 70
	end
	return bonus
end

function modifier_lich_sinister_gaze_creep_thinker:GetModifierBaseAttack_BonusDamage()
	return self.damage
end

function modifier_lich_sinister_gaze_creep_thinker:GetAbsoluteNoDamagePhysical()
    return 1
end

function modifier_lich_sinister_gaze_creep_thinker:GetAbsoluteNoDamageMagical()
    return 1
end

function modifier_lich_sinister_gaze_creep_thinker:GetAbsoluteNoDamagePure()
    return 1
end

modifier_lich_sinister_gaze_attack_debuff = class({})

function modifier_lich_sinister_gaze_attack_debuff:OnCreated( kv )
	if IsServer() then
		if not self:GetParent():IsBuilding() then
			self:StartIntervalThink(1)
		end
	end
end

function modifier_lich_sinister_gaze_attack_debuff:OnIntervalThink()
	ApplyDamageRDA({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = self:GetCaster():GetIntellect(false) * 0.5,
		damage_type = DAMAGE_TYPE_MAGICAL,
		ability = self:GetAbility(),
	})
end

function modifier_lich_sinister_gaze_attack_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		-- MODIFIER_EVENT_ON_ATTACK_START,
	}
end

function modifier_lich_sinister_gaze_attack_debuff:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetLevel() * -15
end

function modifier_lich_sinister_gaze_attack_debuff:CustomOnAttackStart(params)
	if params.target~=self:GetParent() then return end
		
	params.attacker:AddNewModifier(
		self:GetParent(),
		self:GetAbility(),
		"modifier_lich_sinister_gaze_attack_buff",
		nil
	)
end

modifier_lich_sinister_gaze_attack_buff = {}

function modifier_lich_sinister_gaze_attack_buff:IsHidden()
	return true
end

function modifier_lich_sinister_gaze_attack_buff:IsDebuff()
	return false
end

function modifier_lich_sinister_gaze_attack_buff:IsStunDebuff()
	return false
end

function modifier_lich_sinister_gaze_attack_buff:IsPurgable()
	return false
end

function modifier_lich_sinister_gaze_attack_buff:OnCreated( kv )
	self.slow = 15 * self:GetAbility():GetLevel()
	self.duration = 2
end

function modifier_lich_sinister_gaze_attack_buff:OnRefresh( kv )
	self.slow = 15 * self:GetAbility():GetLevel()
	self.duration = 2
end

function modifier_lich_sinister_gaze_attack_buff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PRE_ATTACK,
		-- MODIFIER_EVENT_ON_ATTACK,
		-- MODIFIER_EVENT_ON_ATTACK_FINISHED,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_lich_sinister_gaze_attack_buff:GetModifierPreAttack( params )
	if IsServer() then
		if not self.HasAttacked then
			self.record = params.record
		end

		if params.target~=self:GetCaster() then
			self.attackOther = true
		end
	end
end

function modifier_lich_sinister_gaze_attack_buff:CustomOnAttack( params )
	if IsServer() then
		if params.record~=self.record then return end

		self:SetDuration(self.duration, true)
		self.HasAttacked = true
	end
end

function modifier_lich_sinister_gaze_attack_buff:CustomOnAttackFinished( params )
	if IsServer() then
		if params.attacker~=self:GetParent() then return end
		
		if not self.HasAttacked then
			self:Destroy()
		end

		if self.attackOther then
			self:Destroy()
		end
	end
end

function modifier_lich_sinister_gaze_attack_buff:GetModifierAttackSpeedBonus_Constant()
	if IsServer() then
		if self:GetParent():GetAggroTarget()==self:GetCaster() then
			return self.slow
		else
			return 0
		end
	end

	return self.slow
end