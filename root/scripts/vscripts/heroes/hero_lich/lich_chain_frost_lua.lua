LinkLuaModifier( "modifier_lich_chain_frost_lua_instic", "heroes/hero_lich/lich_chain_frost_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_lich_chain_frost_lua_instic_int12", "heroes/hero_lich/lich_chain_frost_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_lich_chain_frost_lua_slow", "heroes/hero_lich/lich_chain_frost_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_chain_frost_lua_frostbound", "heroes/hero_lich/lich_chain_frost_lua", LUA_MODIFIER_MOTION_NONE)
-- StashOfSouls:RemoveRandomSoul(u:GetPlayerID())
lich_chain_frost_lua = class({})

function lich_chain_frost_lua:Precache( context )
    PrecacheResource( "particle", "particles/lich_ice_spire_outer_ring.vpcf", context )
end

function lich_chain_frost_lua:CastFilterResultTarget( hTarget )
    if hTarget:IsHero() and hTarget:GetTeamNumber() == self:GetCaster():GetTeamNumber() and hTarget:GetUnitName() ~= "npc_dota_lich_ice_spire" then
        return UF_FAIL_FRIENDLY
    end
    
    if hTarget:IsBuilding() and hTarget:GetUnitName() ~= "npc_dota_lich_ice_spire" then
        return UF_FAIL_BUILDING 
    end

    if hTarget:IsOther() and hTarget:GetUnitName() ~= "npc_dota_lich_ice_spire" then
        return UF_FAIL_OTHER
    end

    return UF_SUCCESS
end

function lich_chain_frost_lua:GetBehavior()
    local base_beh = self.BaseClass.GetBehavior(self)
    return base_beh
end

function lich_chain_frost_lua:GetManaCost(iLevel)
    return 150 + math.min(65000, self:GetCaster():GetIntellect(true) / 30)
end

function lich_chain_frost_lua:GetCastRange(vLocation, hTarget)
    return self.BaseClass.GetCastRange(self, vLocation, hTarget)
end

function lich_chain_frost_lua:GetCooldown(iLevel)
    return self.BaseClass.GetCooldown(self, iLevel)
end

function lich_chain_frost_lua:GetIntrinsicModifierName()
    return "modifier_lich_chain_frost_lua_instic"
end 

function lich_chain_frost_lua:OnSpellStart()
    local caster = self:GetCaster()
    local ability = self
    local target = self:GetCursorTarget()
    self:LaunchProjectile(caster, target)
end

function lich_chain_frost_lua:LaunchProjectile(source, target)
    if not IsServer() then return end
    local caster = self:GetCaster()
    
    local projectile_base_speed = self:GetSpecialValueFor("initial_projectile_speed")

    local projectile_vision = self:GetSpecialValueFor("vision_radius")

    local num_bounces = self:GetSpecialValueFor("jumps")

    self:GetCaster():EmitSound("Hero_Lich.ChainFrost")

    local damage = self:GetSpecialValueFor("damage")

    local chain_frost_projectile = 
    {
        Target = target,
        Source = source,
        Ability = self,
        EffectName = "particles/units/heroes/hero_lich/lich_chain_frost.vpcf",
        iMoveSpeed = projectile_base_speed,
        bDodgeable = false,
        bVisibleToEnemies = true,
        bReplaceExisting = false,
        bProvidesVision = true,
        iVisionRadius = projectile_vision,
        iVisionTeamNumber = caster:GetTeamNumber(),
        ExtraData = {bounces_left = num_bounces, current_projectile_speed = projectile_base_speed, damage = damage, start = 1}
    }

    ProjectileManager:CreateTrackingProjectile(chain_frost_projectile)
end

function lich_chain_frost_lua:OnProjectileHit_ExtraData(target, location, extradata)
    if not IsServer() then return end
    local caster = self:GetCaster()
    local slow_duration = self:GetSpecialValueFor("slow_duration")
    local bounce_range = self:GetSpecialValueFor("jump_range")
    local bonus_jump_damage = self:GetSpecialValueFor("bonus_jump_damage")
    local projectile_speed = self:GetSpecialValueFor("projectile_speed")
    if not target then return nil end
    target:EmitSound("Hero_Lich.ChainFrostImpact.Hero")

    local abs_origin = target:GetAbsOrigin()
    
    if extradata.bounces_left > 0 then
        local delay = 0.1
        if table.has_value(bosses_names, target:GetUnitName()) or target:GetUnitName() == "npc_dota_lich_ice_spire" then
            delay = 0.2
        end
        Timers:CreateTimer(delay, function()
            abs_origin = (target and not target:IsNull() and target:IsAlive() and target.GetAbsOrigin) and target:GetAbsOrigin() or abs_origin
            local enemies = FindUnitsInRadius(caster:GetTeamNumber(), abs_origin, nil, bounce_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE, FIND_CLOSEST, false)
            local spires = FindUnitsInRadius(caster:GetTeamNumber(), abs_origin, nil, bounce_range, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE, FIND_ANY_ORDER, false)
            for i = #enemies, 1, -1 do
                if enemies[i] ~= nil and (target == enemies[i] or enemies[i]:GetName() == "npc_dota_unit_undying_zombie") then
                    table.remove(enemies, i)
                elseif enemies[i] ~= nil and (enemies[i]:GetTeamNumber() == self:GetCaster():GetTeamNumber()) then
                    table.remove(enemies, i)
                elseif table.has_value({"npc_breakingvase_quest", "npc_breakingcrate_quest", "npc_mines_ore_quest_mega", "npc_mines_ore_quest", "npc_tombstone_quest", "npc_naga_quest", "npc_crystal_maiden_quest", "blacksmith"}, enemies[i]:GetUnitName()) then
                    table.remove(enemies, i)
                end
            end
            for _,spire in pairs(spires) do
                if spire:GetUnitName() == "npc_dota_lich_ice_spire" and target ~= spire then
                    table.insert(enemies, spire)
                end
            end
            
            if #enemies <= 0 then
                local modifier_lich_chain_frost_custom_frostbound = target:AddNewModifier(caster, self, "modifier_lich_chain_frost_lua_frostbound", {duration = 5})
                if modifier_lich_chain_frost_custom_frostbound then
                    modifier_lich_chain_frost_custom_frostbound.extradata = extradata
                    modifier_lich_chain_frost_custom_frostbound:StartSearch()
                end
                return nil
            end
            -- if bounce_target == nil then return end

            local bounce_target = enemies[1]
            local found = false
            for _, enemy in pairs(enemies) do
                if table.has_value(bosses_names, enemy:GetUnitName()) then
                    bounce_target = enemy
                    found = true
                    break
                end
            end
            if not found and #spires > 0 then
                for _,spire in pairs(spires) do
                    if spire:GetUnitName() == "npc_dota_lich_ice_spire" and spire ~= target then
                        bounce_target = spire
                    end
                end
            end
            ------------------------------------------------------
            extradata.bounces_left = extradata.bounces_left - 1
            extradata.current_projectile_speed = projectile_speed
            extradata.damage = extradata.damage + bonus_jump_damage
            ---------------------------------------------------------

            local chain_frost_projectile = 
            {
                Target = bounce_target,
                Source = target,
                Ability = self,
                EffectName = "particles/units/heroes/hero_lich/lich_chain_frost.vpcf",
                iMoveSpeed = extradata.current_projectile_speed,
                bDodgeable = false,
                bVisibleToEnemies = true,
                bReplaceExisting = false,
                bProvidesVision = true,
                iVisionRadius = projectile_vision,
                iVisionTeamNumber = caster:GetTeamNumber(),
                ExtraData = {bounces_left = extradata.bounces_left, current_projectile_speed = extradata.current_projectile_speed, damage = extradata.damage}
            }

            ProjectileManager:CreateTrackingProjectile(chain_frost_projectile)
        end)
    end

    if extradata and extradata.start == 1 then
        if target:TriggerSpellAbsorb(self) then
            return nil
        end
    end
    local real_damage = extradata.damage
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int7") then
        real_damage = real_damage * (self:GetLevel()* 0.02 + 1)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str6") then
        real_damage = real_damage + self:GetCaster():GetStrength() * 1.0
    end
    local damageTable = 
    {
        victim = target,
        damage = real_damage,
        damage_type = DAMAGE_TYPE_MAGICAL,
        attacker = self:GetCaster(),
        ability = self
    }

    if target:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
        ApplyDamageRDA(damageTable)
        target:AddNewModifier(self:GetCaster(), self, "modifier_lich_chain_frost_lua_slow", {duration = slow_duration * (1 - target:GetStatusResistance())})
    end
end

modifier_lich_chain_frost_lua_slow = class({})

function modifier_lich_chain_frost_lua_slow:OnCreated()
    self.caster = self:GetCaster()
    self.ability = self:GetAbility()
    self.ms_slow_pct = self.ability:GetSpecialValueFor("slow_movement_speed")
    self.as_slow = self.ability:GetSpecialValueFor("slow_attack_speed")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int7") then
        self.as_slow = self.as_slow - 30
    end
end

function modifier_lich_chain_frost_lua_slow:IsHidden() return false end
function modifier_lich_chain_frost_lua_slow:IsPurgable() return true end
function modifier_lich_chain_frost_lua_slow:IsDebuff() return true end

function modifier_lich_chain_frost_lua_slow:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
    }
end

function modifier_lich_chain_frost_lua_slow:GetModifierMoveSpeedBonus_Percentage()
    return self.ms_slow_pct
end

function modifier_lich_chain_frost_lua_slow:GetModifierAttackSpeedBonus_Constant()
    return self.as_slow
end

function modifier_lich_chain_frost_lua_slow:GetStatusEffectName()
    return "particles/status_fx/status_effect_frost_lich.vpcf"
end



modifier_lich_chain_frost_lua_instic = class({})
function modifier_lich_chain_frost_lua_instic:IsHidden() return self:GetStackCount() == 0 end
function modifier_lich_chain_frost_lua_instic:IsDebuff() return false end
function modifier_lich_chain_frost_lua_instic:IsPurgable() return false end
function modifier_lich_chain_frost_lua_instic:IsPurgeException() return false end
function modifier_lich_chain_frost_lua_instic:IsStunDebuff() return false end
function modifier_lich_chain_frost_lua_instic:RemoveOnDeath() return false end
function modifier_lich_chain_frost_lua_instic:DestroyOnExpire() return false end

function modifier_lich_chain_frost_lua_instic:IsAura() return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int12") ~= nil end
function modifier_lich_chain_frost_lua_instic:GetModifierAura() return "modifier_lich_chain_frost_lua_instic_int12" end
function modifier_lich_chain_frost_lua_instic:GetAuraRadius() return 500 end
function modifier_lich_chain_frost_lua_instic:GetAuraDuration() return 0.5 end
function modifier_lich_chain_frost_lua_instic:GetAuraSearchTeam() return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_lich_chain_frost_lua_instic:GetAuraSearchType() return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC end
function modifier_lich_chain_frost_lua_instic:GetAuraSearchFlags() return 0 end

function modifier_lich_chain_frost_lua_instic:OnCreated()
    if not IsServer() then return end
    self:SetStackCount(0)
    self:StartIntervalThink(0.5)
end

function modifier_lich_chain_frost_lua_instic:OnIntervalThink()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int12") and not self.particle then
        self.particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_chain_frost_frostbound.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
        ParticleManager:SetParticleControl(self.particle, 0, self:GetParent():GetAbsOrigin())
    elseif self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int12") == nil and self.particle then
        ParticleManager:DestroyParticle(self.particle, false)
        ParticleManager:ReleaseParticleIndex( self.particle )
        self.particle = nil
    end

    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int12") and not self.particle2 then
        self.particle2 = ParticleManager:CreateParticle("particles/lich_ice_spire_outer_ring.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
        ParticleManager:SetParticleControl(self.particle2, 0, self:GetParent():GetAbsOrigin())
        ParticleManager:SetParticleControl(self.particle2, 5, Vector(500,500,500))
    elseif self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int12") == nil and self.particle2 then
        ParticleManager:DestroyParticle(self.particle2, false)
        ParticleManager:ReleaseParticleIndex( self.particle2 )
        self.particle2 = nil
    end
    
end

function modifier_lich_chain_frost_lua_instic:DeclareFunctions()
    return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        -- MODIFIER_EVENT_ON_DEATH
    }
end

function modifier_lich_chain_frost_lua_instic:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
        if data.ability_special_value == "jumps" then
			return 1
		end
        if data.ability_special_value == "bonus_jump_damage" then
			return 1
		end
	end
	return 0
end

function modifier_lich_chain_frost_lua_instic:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int8") then
                value = value + self:GetCaster():GetIntellect(true) * 1
            end
            
            return value
		end
        if data.ability_special_value == "jumps" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "jumps", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str9") then
                value = 50
            end

            return value
		end
        if data.ability_special_value == "bonus_jump_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_jump_damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_str6") then
                value = value + self:GetCaster():GetIntellect(true) * 0.2
            end
            
            return value
		end
	end
	return 0
end

function modifier_lich_chain_frost_lua_instic:GetModifierBonusStats_Intellect()
    local v = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int13") then
        v= v + self:GetStackCount()
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int11") then
        v= v + 75 * self:GetAbility():GetLevel()
    end
    return v
end

function modifier_lich_chain_frost_lua_instic:CustomOnDeath(data)
    if data.attacker:GetTeamNumber() == self:GetParent():GetTeamNumber() and data.attacker == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_lich_int13") then
        local add_stack = math.min(5, math.max(1, math.floor(self:GetCaster():GetLevel() / 10)))
        self:SetStackCount( self:GetStackCount() + add_stack )
    end
end

modifier_lich_chain_frost_lua_instic_int12 = class({})
function modifier_lich_chain_frost_lua_instic_int12:IsHidden() return false end
function modifier_lich_chain_frost_lua_instic_int12:IsDebuff() return true end
function modifier_lich_chain_frost_lua_instic_int12:IsPurgable() return true end
function modifier_lich_chain_frost_lua_instic_int12:IsPurgeException() return false end
function modifier_lich_chain_frost_lua_instic_int12:IsStunDebuff() return false end
function modifier_lich_chain_frost_lua_instic_int12:RemoveOnDeath() return true end
function modifier_lich_chain_frost_lua_instic_int12:DestroyOnExpire() return true end

function modifier_lich_chain_frost_lua_instic_int12:OnCreated()
    if not IsServer() then return end

    self:StartIntervalThink(0.2)
end

function modifier_lich_chain_frost_lua_instic_int12:OnIntervalThink()
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self:GetAbility():GetSpecialValueFor("damage") * 0.2,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = DOTA_DAMAGE_FLAG_NONE,
        ability = self:GetAbility()
    })
end

function modifier_lich_chain_frost_lua_instic_int12:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_HP_REGEN_AMPLIFY_PERCENTAGE,
    }
end

function modifier_lich_chain_frost_lua_instic_int12:GetModifierHPRegenAmplify_Percentage()
    return -80
end




modifier_lich_chain_frost_lua_frostbound = class({})
function modifier_lich_chain_frost_lua_frostbound:IsPurgable() return false end
function modifier_lich_chain_frost_lua_frostbound:IsPurgeException() return false end

function modifier_lich_chain_frost_lua_frostbound:OnCreated()
    if not IsServer() then return end
    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_chain_frost_frostbound.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(particle, 0, self:GetParent():GetAbsOrigin())
    self:AddParticle(particle, false, false, -1, false, true)
end

function modifier_lich_chain_frost_lua_frostbound:StartSearch()
    if not IsServer() then return end
    self:StartIntervalThink(0.2)
end

function modifier_lich_chain_frost_lua_frostbound:OnIntervalThink()
    if not IsServer() then return end
    local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("jump_range"), DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE, FIND_ANY_ORDER, false)
    local spires = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("jump_range"), DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE, FIND_ANY_ORDER, false)
    
    for i = #enemies, 1, -1 do
        if enemies[i] ~= nil and (self:GetParent() == enemies[i] or enemies[i]:GetName() == "npc_dota_unit_undying_zombie") then
            table.remove(enemies, i)
        end
    end

    for i = #enemies, 1, -1 do
        if enemies[i] ~= nil and (enemies[i]:GetTeamNumber() == self:GetCaster():GetTeamNumber()) then
            table.remove(enemies, i)
        end
    end

    for _,spire in pairs(spires) do
        if spire:GetUnitName() == "npc_dota_lich_ice_spire" and self:GetParent() ~= spire then
            table.insert(enemies, spire)
        end
    end

    if self:GetCaster():HasModifier("modifier_lich_7") then
        if self:GetParent() ~= self:GetCaster() then
            if self:GetCaster():IsAlive() then
                table.insert(enemies, self:GetCaster())
            end
        end
    end

    if #enemies <= 0 then
        return
    end

    local bounce_target = enemies[1]

    local chain_frost_projectile = 
    {
        Target = bounce_target,
        Source = self:GetParent(),
        Ability = self:GetAbility(),
        EffectName = "particles/units/heroes/hero_lich/lich_chain_frost.vpcf",
        iMoveSpeed = self.extradata.current_projectile_speed,
        bDodgeable = false,
        bVisibleToEnemies = true,
        bReplaceExisting = false,
        bProvidesVision = true,
        iVisionRadius = self:GetAbility():GetSpecialValueFor("vision_radius"),
        iVisionTeamNumber = self:GetCaster():GetTeamNumber(),
        ExtraData = self.extradata
    }

    ProjectileManager:CreateTrackingProjectile(chain_frost_projectile)

    self:Destroy()
end