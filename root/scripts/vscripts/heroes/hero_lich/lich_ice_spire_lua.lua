LinkLuaModifier("modifier_lich_ice_spire_lua", "heroes/hero_lich/lich_ice_spire_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_lich_ice_spire_lua_aura", "heroes/hero_lich/lich_ice_spire_lua", LUA_MODIFIER_MOTION_NONE)

lich_ice_spire_lua = class({})

function lich_ice_spire_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function lich_ice_spire_lua:Spawn()
    if not IsServer() then return end
    self:SetLevel(1)
end

function lich_ice_spire_lua:OnSpellStart()
    if not IsServer() then return end
    local point = self:GetCursorPosition()
    local duration = self:GetSpecialValueFor("duration")
    local pilar = CreateUnitByName("npc_dota_lich_ice_spire", point, true, nil, nil, self:GetCaster():GetTeamNumber())
    ResolveNPCPositions( pilar:GetAbsOrigin(), 128 )
    EmitSoundOnLocationWithCaster(point, "Hero_Lich.IceSpire", self:GetCaster())
    local health = self:GetSpecialValueFor("max_creep_attacks")
    pilar:SetBaseMaxHealth(health)
    pilar:SetHealth(health)
    pilar:AddNewModifier(self:GetCaster(), self, "modifier_lich_ice_spire_lua", {duration = duration})
    pilar:AddNewModifier(self:GetCaster(), self, "modifier_kill", {duration = duration})
end

modifier_lich_ice_spire_lua = class({})
function modifier_lich_ice_spire_lua:IsHidden() return true end
function modifier_lich_ice_spire_lua:IsPurgable() return false end
function modifier_lich_ice_spire_lua:OnCreated()
    if not IsServer() then return end
    self.health = self:GetSpecialValueFor("max_creep_attacks")
    self.damage = 5
    self.aura_radius = self:GetAbility():GetSpecialValueFor("aura_radius")
    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_ice_spire.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(particle, 0, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 1, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 2, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 3, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 4, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 5, Vector(self.aura_radius,self.aura_radius,self.aura_radius))  
    self:AddParticle(particle, false, false, -1, true, false)
end

function modifier_lich_ice_spire_lua:HealPilar()
    if not IsServer() then return end
    self.health = math.min(self:GetSpecialValueFor("max_creep_attacks"), self.health + 1)
    self:GetParent():SetHealth(self.health)
end

function modifier_lich_ice_spire_lua:DeclareFunctions()
    return
    {
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_HEALTHBAR_PIPS,
        -- MODIFIER_EVENT_ON_DEATH
    } 
end

function modifier_lich_ice_spire_lua:CustomOnDeath(params)
    if not IsServer() then return end
    if params.unit ~= self:GetParent() then return end

    self:Destroy()
end

function modifier_lich_ice_spire_lua:GetModifierHealthBarPips(data)
    return self:GetParent():GetMaxHealth() / 5
end 

function modifier_lich_ice_spire_lua:GetAbsoluteNoDamageMagical() return 1 end
function modifier_lich_ice_spire_lua:GetAbsoluteNoDamagePhysical() return 1 end
function modifier_lich_ice_spire_lua:GetAbsoluteNoDamagePure() return 1 end

function modifier_lich_ice_spire_lua:CustomOnAttackLanded( params )
    if not IsServer() then return end
    if self:GetParent() ~= params.target then return end
    local damage = self.damage
    if not params.attacker:IsHero() then
        damage = 1
    end
    self.health = self.health - damage
    if self.health <= 0 then 
        self:GetParent():Kill(self:GetAbility(), params.attacker)
    else
        self:GetParent():SetHealth(self.health)
    end
end

function modifier_lich_ice_spire_lua:IsAura()
    return true
end

function modifier_lich_ice_spire_lua:GetModifierAura()
    return "modifier_lich_ice_spire_lua_aura"
end

function modifier_lich_ice_spire_lua:GetAuraRadius()
    return self.aura_radius
end

function modifier_lich_ice_spire_lua:GetAuraDuration()
    return self:GetAbility():GetSpecialValueFor("slow_duration")
end

function modifier_lich_ice_spire_lua:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_lich_ice_spire_lua:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

modifier_lich_ice_spire_lua_aura = class({})

function modifier_lich_ice_spire_lua_aura:IsPurgable()
    return false
end

function modifier_lich_ice_spire_lua_aura:OnCreated()
    if not IsServer() then return end
    local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_ice_spire_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(particle, 0, self:GetParent():GetOrigin())
    ParticleManager:SetParticleControl(particle, 1, self:GetAuraOwner():GetAbsOrigin())
    self:AddParticle(particle, false, false, -1, false, false)
end

function modifier_lich_ice_spire_lua_aura:DeclareFunctions()
    local funcs = 
    {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
    }
    return funcs
end

function modifier_lich_ice_spire_lua_aura:GetModifierMoveSpeedBonus_Percentage()
    return self:GetAbility():GetSpecialValueFor("bonus_movespeed")
end