dazzle_shadow_wave_lua = class({})
LinkLuaModifier( "modifier_dazzle_shadow_wave_lua", "heroes/hero_dazzle/dazzle_shadow_wave/dazzle_shadow_wave_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_grave_lua", "heroes/hero_dazzle/dazzle_shadow_wave/dazzle_shadow_wave_lua", LUA_MODIFIER_MOTION_NONE )

function dazzle_shadow_wave_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int7") ~= nil	then 
		return 50 + math.min(65000, self:GetCaster():GetIntellect(true)/200)
	end
    return 100+math.min(65000, self:GetCaster():GetIntellect(true)/100)
end


function dazzle_shadow_wave_lua:OnSpellStart()
	local caster = self:GetCaster()
	local target = caster

	self.radius = self:GetSpecialValueFor( "damage_radius" )
	self.bounce_radius = self:GetSpecialValueFor( "bounce_radius" )
	jumps = self:GetSpecialValueFor( "max_targets" )
	self.damage = self:GetSpecialValueFor( "damage" )
	
	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_str10")
	if abil ~= nil	then 
	self.damage = self:GetCaster():GetStrength()
	end
	
	
	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int10")
	if abil ~= nil	then 
	self.damage = self.damage + self:GetCaster():GetAttackDamage()
	end
	
	self.damage_type = DAMAGE_TYPE_PHYSICAL
	
	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int8")
	if abil ~= nil	then 
	self.damage_type = DAMAGE_TYPE_MAGICAL
	end

	self.healedUnits = {}
	self.healedUnits[caster] = true

	self:Jump( jumps, target )

	local sound_cast = "Hero_Dazzle.Shadow_Wave"
	EmitSoundOn( sound_cast, caster )

	local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int11")
	if abil ~= nil	then 
		local abil2 = self:GetCaster():FindAbilityByName("dazzle_poison_touch_lua")
		if abil2 ~= nil and abil2:GetLevel() > 0 then
			if enemies[1] ~= nil then
				caster:CastAbilityOnTarget(target, abil2, -1)
			end
		end
	end
	self.npc_dota_hero_dazzle_str9 = self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_str9") ~= nil
end


function dazzle_shadow_wave_lua:Jump( jumps, target ) 
	if self:GetCaster():GetTeamNumber() ~= target:GetTeamNumber() then
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), target:GetAbsOrigin(), nil, self.bounce_radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for _,unit in pairs(units) do
			ApplyDamageRDA({
				victim = target,
				attacker = self:GetCaster(),
				damage = self.damage,
				damage_type = self.damage_type,
				damage_flags = self.damage_type ~= DAMAGE_TYPE_PHYSICAL and DOTA_DAMAGE_FLAG_NONE or DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
				ability = self
			})
		end
	else
		target:HealWithParams(math.min(math.abs(self.damage),2^30), self, false, true, self:GetCaster(), false)
	end
	jumps = jumps - 1
	if jumps <= 0 and target ~= self:GetCaster() then
		self:Jump( jumps, self:GetCaster() )
		return
	end
	local nextTarget
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), target:GetAbsOrigin(), nil, self.bounce_radius, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,unit in pairs(units) do
		if unit ~= target and not self.healedUnits[unit] then
			self.healedUnits[unit] = true
			self:Jump( jumps, unit )
			nextTarget = unit
			break
		end
	end
	if nextTarget then
		self:PlayEffects1( target, nextTarget )
	elseif target ~= self:GetCaster() then
		self:Jump( jumps, self:GetCaster() )
	end
end

--------------------------------------------------------------------------------
function dazzle_shadow_wave_lua:PlayEffects1( source, target )
--local particle_cast = "particles/units/heroes/hero_dazzle/dazzle_shadow_wave_impact_damage.vpcf"
local particle_cast = "particles/units/heroes/hero_dazzle/dazzle_shadow_wave.vpcf"	

	if not target then
		target = source
	end


	local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, source )

	ParticleManager:SetParticleControlEnt(
		effect_cast,
		0,
		source,
		PATTACH_POINT_FOLLOW,
		"attach_hitloc",
		source:GetOrigin(), 
		true
	)
	ParticleManager:SetParticleControlEnt(
		effect_cast,
		1,
		target,
		PATTACH_POINT_FOLLOW,
		"attach_hitloc",
		target:GetOrigin(), 
		true 
	)
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

function dazzle_shadow_wave_lua:PlayEffects2( target )

caster = self:GetCaster()
	
local particle_cast = "particles/units/heroes/hero_dazzle/dazzle_shadow_wave.vpcf"

	 local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, target )
	 
	 	ParticleManager:SetParticleControlEnt(
		effect_cast,
		0,
		caster,
		PATTACH_POINT_FOLLOW,
		"attach_hitloc",
		caster:GetOrigin(), 
		true
	)

	ParticleManager:SetParticleControlEnt(
		effect_cast,
		1,
		target,
		PATTACH_POINT_FOLLOW,
		"attach_hitloc",
		target:GetOrigin(), 
		true 
	)
	ParticleManager:ReleaseParticleIndex( effect_cast )
end


--------------------------------------------------------
modifier_grave_lua = class({})

function modifier_grave_lua:IsHidden()
	return false
end

function modifier_grave_lua:IsDebuff()
	return false
end

function modifier_grave_lua:IsPurgable()
	return false
end

function modifier_grave_lua:OnCreated( kv )
		local sound_cast = "Hero_Dazzle.Shallow_Grave"
		local abil = self:GetCaster():FindAbilityByName("dazzle_shallow_grave_lua")
		if abil ~= nil and abil:GetLevel() > 0 then
		self.hp = abil:GetSpecialValueFor("hp")
		EmitSoundOn( sound_cast, self:GetParent() )
end
end
function modifier_grave_lua:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MIN_HEALTH,
		MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE,
	}

	return funcs
end

function modifier_grave_lua:GetMinHealth()
	return 1
end

function modifier_grave_lua:GetModifierHealthRegenPercentage()

	return self.hp

end
function modifier_grave_lua:GetEffectName()
	return "particles/units/heroes/hero_dazzle/dazzle_shallow_grave.vpcf"
end

function modifier_grave_lua:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end