dazzle_poison_touch_lua = class({})
LinkLuaModifier( "modifier_dazzle_poison_touch_lua", "heroes/hero_dazzle/dazzle_poison_touch/modifier_dazzle_poison_touch_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_dazzle_poison_touch_intrinsic", "heroes/hero_dazzle/dazzle_poison_touch/modifier_dazzle_poison_touch_lua", LUA_MODIFIER_MOTION_NONE )

function dazzle_poison_touch_lua:GetIntrinsicModifierName()
    return "modifier_dazzle_poison_touch_intrinsic"
end

function dazzle_poison_touch_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end 
	if self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int7") ~= nil	then 
		return 50 + math.min(65000, self:GetCaster():GetIntellect(true)/200)
	end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function dazzle_poison_touch_lua:GetBehavior()
	if self:GetCaster():HasModifier("modifier_hero_dazzle_buff_1") then
		return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
end

function dazzle_poison_touch_lua:GetCooldown(level)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_dazzle_int50") ~= nil	then 
		return 1
	end
    return self.BaseClass.GetCooldown(self, level)
end

function dazzle_poison_touch_lua:OnSpellStart()
	-- unit identifier
	local caster = self:GetCaster()
	local target = self:GetCursorTarget()
	local origin = caster:GetOrigin()
	



	-- cancel if linken
	--if target:TriggerSpellAbsorb( self ) then return end

	-- load data
	local max_targets = self:GetSpecialValueFor( "targets" )
	local distance = self:GetSpecialValueFor( "end_distance" )
	local start_radius = self:GetSpecialValueFor( "start_radius" )
	local end_radius = self:GetSpecialValueFor( "end_radius" )

	-- get direction
	local direction = target:GetOrigin()-origin
	direction.z = 0
	direction = direction:Normalized()

	local enemies = self:FindUnitsInCone(
		caster:GetTeamNumber(),	-- nTeamNumber
		target:GetOrigin(),	-- vCenterPos
		caster:GetOrigin(),	-- vStartPos
		caster:GetOrigin() + direction*distance,	-- vEndPos
		start_radius,	-- fStartRadius
		end_radius,	-- fEndRadius
		nil,	-- hCacheUnit
		DOTA_UNIT_TARGET_TEAM_ENEMY,	-- nTeamFilter
		DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,	-- nTypeFilter
		0,	-- nFlagFilter
		FIND_CLOSEST,	-- nOrderFilter
		false	-- bCanGrowCache
	)

	-- projectile data
	local projectile_name = "particles/units/heroes/hero_dazzle/dazzle_poison_touch.vpcf"
	local projectile_speed = self:GetSpecialValueFor( "projectile_speed" )

	-- precache projectile
	local info = {
		-- Target = target,
		Source = caster,
		Ability = self,	
		
		EffectName = projectile_name,
		iMoveSpeed = projectile_speed,
		bDodgeable = true,                           -- Optional
	
		bVisibleToEnemies = true,                         -- Optional
		bProvidesVision = false,                           -- Optional
	}

	-- create projectile
	local counter = 0
	for _,enemy in pairs(enemies) do
		info.Target = enemy
		ProjectileManager:CreateTrackingProjectile(info)

		counter = counter+1
		if counter>=max_targets then break end
	end

	-- Play effects
	local sound_cast = "Hero_Dazzle.Poison_Cast"
	EmitSoundOn( sound_cast, caster )
	
	
	if self:GetCaster():FindAbilityByName("npc_dota_hero_dazzle_int9") ~= nil then 
		local abil2 = self:GetCaster():FindAbilityByName("dazzle_shadow_wave_lua")
		if abil2 ~= nil and abil2:GetLevel() > 0 then
		abil2:OnSpellStart()
		end
	end
end

--------------------------------------------------------------------------------
-- Projectile
function dazzle_poison_touch_lua:OnProjectileHit( target, location )
	if not target then return end

	-- get data
	local duration = self:GetSpecialValueFor( "duration" )
	local limit = 1
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_dazzle_str50") or self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_dazzle_int50") then
		limit = 5
	end
	local count = target:FindAllModifiersByName("modifier_dazzle_poison_touch_lua")
	if #count >= limit then
		return
	end
	-- add debuff
	target:AddNewModifier(
		self:GetCaster(), -- player source
		self, -- ability source
		"modifier_dazzle_poison_touch_lua", -- modifier name
		{ duration = duration } -- kv
	)

	-- Play effects
	local sound_target = "Hero_Dazzle.Poison_Touch"
	EmitSoundOn( sound_target, target )
end

--------------------------------------------------------------------------------
-- Helper
function dazzle_poison_touch_lua:FindUnitsInCone( nTeamNumber, vCenterPos, vStartPos, vEndPos, fStartRadius, fEndRadius, hCacheUnit, nTeamFilter, nTypeFilter, nFlagFilter, nOrderFilter, bCanGrowCache )
	-- vCenterPos is used to determine searching center (FIND_CLOSEST will refer to units closest to vCenterPos)

	-- get cast direction and length distance
	local direction = vEndPos-vStartPos
	direction.z = 0

	local distance = direction:Length2D()
	direction = direction:Normalized()

	-- get max radius circle search
	local big_radius = distance + math.max(fStartRadius, fEndRadius)

	-- find enemies closest to primary target within max radius
	local units = FindUnitsInRadius(
		nTeamNumber,	-- int, your team number
		vCenterPos,	-- point, center point
		nil,	-- handle, cacheUnit. (not known)
		big_radius,	-- float, radius. or use FIND_UNITS_EVERYWHERE
		nTeamFilter,	-- int, team filter
		nTypeFilter,	-- int, type filter
		nFlagFilter,	-- int, flag filter
		nOrderFilter,	-- int, order filter
		bCanGrowCache	-- bool, can grow cache
	)

	-- Filter within cone
	local targets = {}
	for _,unit in pairs(units) do

		-- get unit vector relative to vStartPos
		local vUnitPos = unit:GetOrigin()-vStartPos

		-- get projection scalar of vUnitPos onto direction using dot-product
		local fProjection = vUnitPos.x*direction.x + vUnitPos.y*direction.y + vUnitPos.z*direction.z

		-- clamp projected scalar to [0,distance]
		fProjection = math.max(math.min(fProjection,distance),0)
		
		-- get projected vector of vUnitPos onto direction
		local vProjection = direction*fProjection

		-- calculate distance between vUnitPos and the projected vector
		local fUnitRadius = (vUnitPos - vProjection):Length2D()

		-- calculate interpolated search radius at projected vector
		local fInterpRadius = (fProjection/distance)*(fEndRadius-fStartRadius) + fStartRadius

		-- if unit is within distance, add them
		if fUnitRadius<=fInterpRadius then
			table.insert( targets, unit )
		end
	end

	return targets
end