"DOTAAbilities"
{
	"dazzle_poison_touch_lua"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_dazzle/dazzle_poison_touch/dazzle_poison_touch_lua"
		"AbilityTextureName"			"dazzle_poison_touch"
		"FightRecapLevel"				"1"
		"MaxLevel"						"15"

		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"

		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.3"

		"AbilityCooldown"				"27 26 25 24 23 22 21 20 19 18 17 16 15 14 13"
		"AbilityManaCost"				"150"
		
		"AbilityValues"
		{
			"start_radius"
			{
				"value"					"200"
				"affected_by_aoe_increase"	"1"
			}
			"end_radius"
			{
				"value"					"300"
				"affected_by_aoe_increase"	"1"
			}
			"end_distance"
			{
				"value"					"700"
			}
			"targets"
			{
				"value"					"2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"
			}
			"damage"
			{
				"value"					"25 35 45 55 65 75 85 95 105 115 125 135 145 155 165"
				"LinkedSpecialBonus"	"special_bonus_unique_dazzle_custom3"
			}
			"slow"
			{
				"value"					"-10 -12 -14 -16 -18 -20 -22 -24 -26 -28 -30 -32 -34 -36 -38"
			}
			"projectile_speed"
			{
				"value"					"1300"
			}
			"duration"
			{
				"value"					"5"
			}
			"range_tooltip"
			{
				"value"					"700"
			}
		}
	}
	
	"special_bonus_unique_dazzle_custom3"
	{
		"BaseClass"             		"special_bonus_undefined"
		"AbilityType"					"DOTA_ABILITY_TYPE_ATTRIBUTES"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		
		"AbilityValues"
		{
			"value"
			{
				"value"					"85"
			}
		}
	}
}