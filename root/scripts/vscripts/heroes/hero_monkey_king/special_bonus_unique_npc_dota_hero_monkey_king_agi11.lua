special_bonus_unique_npc_dota_hero_monkey_king_agi11 = class({})

LinkLuaModifier("modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11", "heroes/hero_monkey_king/special_bonus_unique_npc_dota_hero_monkey_king_agi11", LUA_MODIFIER_MOTION_NONE)

function special_bonus_unique_npc_dota_hero_monkey_king_agi11:GetIntrinsicModifierName()
    return "modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11"
end

modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11 = class({})
--Classifications template
function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:IsHidden()
    return true
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:IsDebuff()
    return false
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:IsPurgable()
    return false
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:IsStunDebuff()
    return false
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:RemoveOnDeath()
    return false
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:DestroyOnExpire()
    return false
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:OnCreated()
    if not IsServer() then
        return
    end
    local mod = self:GetCaster():FindModifierByName("modifier_talent_base_attack_time")
    if mod then
        mod.base_attack_time = mod.base_attack_time - 0.1
        mod:SetStackCount(mod:GetStackCount())
    end
end

function modifier_special_bonus_unique_npc_dota_hero_monkey_king_agi11:OnDestroy()
    if not IsServer() then
        return
    end
    local mod = self:GetCaster():FindModifierByName("modifier_talent_base_attack_time")
    if mod then
        mod.base_attack_time = mod.base_attack_time + 0.1
        mod:SetStackCount(mod:GetStackCount())
    end
end