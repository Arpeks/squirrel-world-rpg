monkey_king_wukongs_command_lua = class({})

LinkLuaModifier("modifier_monkey_king_wukongs_command_lua_auto_attack_talent", "heroes/hero_monkey_king/monkey_king_wukongs_command_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_wukongs_command_lua_solider_hidden", "heroes/hero_monkey_king/monkey_king_wukongs_command_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_wukongs_command_lua_solider_status", "heroes/hero_monkey_king/monkey_king_wukongs_command_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_wukongs_command_lua_solider_active", "heroes/hero_monkey_king/monkey_king_wukongs_command_lua.lua", LUA_MODIFIER_MOTION_NONE)

function monkey_king_wukongs_command_lua:OnAbilityUpgrade(hAbility)
	if hAbility:GetAbilityName() == "special_bonus_unique_npc_dota_hero_monkey_king_str9" then
        if self:GetCurrentAbilityCharges() == 3 then
            self:RefreshCharges()
        end
    end
end

function monkey_king_wukongs_command_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function monkey_king_wukongs_command_lua:GetIntrinsicModifierName()
    return "modifier_monkey_king_wukongs_command_lua_auto_attack_talent"
end

function monkey_king_wukongs_command_lua:CreateSolider(count, data)
    if self.soliders == nil then
        self.soliders = {}
    end
    local unit
    for i=1,count do
        unit = CreateUnitByName("npc_dota_monkey_clone_hero", self:GetCaster():GetOrigin(), false, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
        unit:AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_wukongs_command_lua_solider_hidden", {})
        unit:AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_wukongs_command_lua_solider_status", {})
        table.insert( data or self.soliders, unit )
    end
end

function monkey_king_wukongs_command_lua:GetFreeSolider(data)
    for _,u in pairs(data or self.soliders) do
        if u:HasModifier("modifier_monkey_king_wukongs_command_lua_solider_hidden") then
            return u
        end
    end
    return nil
end

function monkey_king_wukongs_command_lua:GetLastCreatedSolider(data)
    local min
    local solider
    for _,u in pairs(data or self.soliders) do
        local creation_time = u:FindModifierByName("modifier_monkey_king_wukongs_command_lua_solider_active"):GetCreationTime()
        if min == nil or creation_time < min then
            min = creation_time
            solider = u
        end
    end
    return solider
end

function monkey_king_wukongs_command_lua:OnAbilityPhaseStart()
    self:GetCaster():EmitSound("Hero_MonkeyKing.FurArmy.Channel")
    self.castHandle = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_fur_army_cast.vpcf", PATTACH_ABSORIGIN, self:GetCaster())
    return true
end


function monkey_king_wukongs_command_lua:OnAbilityPhaseInterrupted()
    self:GetCaster():StopSound("Hero_MonkeyKing.FurArmy.Channel")
    if self.castHandle then
        ParticleManager:DestroyParticle(self.castHandle, true)
        ParticleManager:ReleaseParticleIndex(self.castHandle)
        self.castHandle = nil
    end
end

function monkey_king_wukongs_command_lua:OnSpellStart()
    -- if self.soliders ~= nil then
    --     for k,v in pairs(self.soliders) do
    --         UTIL_Remove(v)
    --     end
    --     self.soliders = nil
    -- end
    if self.soliders == nil then
        self:CreateSolider(self:GetMaxAbilityCharges(self:GetLevel()))
    end
    --если во время игры изучат талант и изменят макс зарядов
    if #self.soliders < self:GetMaxAbilityCharges(self:GetLevel()) then
        self:CreateSolider(self:GetMaxAbilityCharges(self:GetLevel()) - #self.soliders)
    end
    local point = self:GetCursorPosition()
    local solider = self:GetFreeSolider() or self:GetLastCreatedSolider()
    solider:RemoveModifierByName("modifier_monkey_king_wukongs_command_lua_solider_active")
    solider:RemoveModifierByName("modifier_monkey_king_wukongs_command_lua_solider_hidden")
    local duration = self:GetSpecialValueFor("duration")
    local main_mod = solider:AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_wukongs_command_lua_solider_active", {duration = duration})
    main_mod.active = false
    --мувмент модифаер
    solider:StartGestureFadeWithSequenceSettings(ACT_DOTA_RUN)
    solider:SetAbsOrigin(self:GetCaster():GetAbsOrigin())
    solider:SetForwardVector((point - self:GetCaster():GetAbsOrigin()):Normalized())
    local arc = solider:AddNewModifier(self:GetCaster(),self,"modifier_generic_arc_lua",{
        target_x = point.x,
        target_y = point.y,
        distance = (point - self:GetCaster():GetAbsOrigin()):Length2D(),
        speed = 700,
        fix_end = false,
        isStun = true,
        -- activity = ACT_DOTA_RUN,
    })
    arc:SetEndCallback(function()
        main_mod.active = true
        main_mod:StartIntervalThink(FrameTime())
        solider:FadeGesture(ACT_DOTA_RUN)
        solider:StartGestureFadeWithSequenceSettings(ACT_DOTA_IDLE)
        solider:FadeGesture(ACT_DOTA_IDLE)
    end)
end

function monkey_king_wukongs_command_lua:CreateSoliderAutoAttack()
    if self.soliders_talent == nil then
        self.soliders_talent = {}
        self:CreateSolider(3, self.soliders_talent)
    end
    if #self.soliders_talent < self:GetSpecialValueFor("soliders_talent") then
        self:CreateSolider(self:GetSpecialValueFor("soliders_talent") - #self.soliders_talent)
    end
    local point = self:GetCaster():GetAbsOrigin() + self:GetCaster():GetForwardVector() * 30
    local solider = self:GetFreeSolider(self.soliders_talent) or self:GetLastCreatedSolider(self.soliders_talent)
    solider:RemoveModifierByName("modifier_monkey_king_wukongs_command_lua_solider_active")
    solider:RemoveModifierByName("modifier_monkey_king_wukongs_command_lua_solider_hidden")
    local duration = self:GetSpecialValueFor("duration")
    local main_mod = solider:AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_wukongs_command_lua_solider_active", {duration = duration})
    main_mod.active = true
    solider:SetAbsOrigin(point)
    main_mod:StartIntervalThink(FrameTime())
end






modifier_monkey_king_wukongs_command_lua_auto_attack_talent = class({})
--Classifications template
function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:IsHidden()
    return true
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:IsDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:IsPurgable()
    return false
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:IsStunDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:RemoveOnDeath()
    return false
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:DestroyOnExpire()
    return false
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:OnCreated()
    if not IsServer() then
        return
    end
    self.active = false
    self:StartIntervalThink(1)
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:OnIntervalThink()
    if self:GetAbility():GetSpecialValueFor("summon_chance") > 0 then
        self.active = true
    else
        self.active = false
    end
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED
    }
end

function modifier_monkey_king_wukongs_command_lua_auto_attack_talent:CustomOnAttackLanded(data)
    if data.attacker ~= self:GetParent() then return end
    if data.target:IsBuilding() then return end
    if self:GetParent():PassivesDisabled() then return end
    if RollPercentage(self:GetAbility():GetSpecialValueFor("summon_chance")) then
        self:GetAbility():CreateSoliderAutoAttack()
    end
end










function IsTranferableModifier_talent(name)
    if string.find(name, "modifier_talent") or string.find(name, "special_bonus_unique") then
        return true
    end
    return false
end

function IsTranferableModifier_pet(name)
    if string.find(name, "pet_rda") then
        return true
    end
    return false
end



modifier_monkey_king_wukongs_command_lua_solider_active = class({})
--Classifications template
function modifier_monkey_king_wukongs_command_lua_solider_active:IsHidden()
    return true
end

function modifier_monkey_king_wukongs_command_lua_solider_active:IsDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_active:IsPurgable()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_active:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_wukongs_command_lua_solider_active:IsStunDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_active:RemoveOnDeath()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_active:DestroyOnExpire()
    return true
end

function modifier_monkey_king_wukongs_command_lua_solider_active:OnCreated()
    if not IsServer() then
        return
    end
    self.special_bonus_unique_npc_dota_hero_monkey_king_int7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_int7") ~= nil
    self.attack_range = self:GetCaster():Script_GetAttackRange()
    self.attack_time = self:GetAbility():GetSpecialValueFor("attack_time")
    self.outgoing = self:GetAbility():GetSpecialValueFor("outgoing") - 100
    self.team = self:GetCaster():GetTeamNumber()
    if self.special_bonus_unique_npc_dota_hero_monkey_king_int7 then
        self:GetParent():SetBaseDamageMax(self:GetCaster():GetBaseDamageMax() + self:GetCaster():GetIntellect(true))
        self:GetParent():SetBaseDamageMin(self:GetCaster():GetBaseDamageMin() + self:GetCaster():GetIntellect(true))
    else
        self:GetParent():SetBaseDamageMax(self:GetCaster():GetBaseDamageMax())
        self:GetParent():SetBaseDamageMin(self:GetCaster():GetBaseDamageMin())
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_agi13") then
        local abi = self:GetCaster():FindAbilityByName("monkey_king_banana_attack")
        if abi:GetLevel() > 0 then
            self:GetParent():AddNewModifier(self:GetCaster(), abi, abi:GetIntrinsicModifierName(), {})
        end
        local abi = self:GetCaster():FindAbilityByName("monkey_king_jingu_mastery_lua")
        if abi:GetLevel() > 0 then
            self:GetParent():AddNewModifier(self:GetCaster(), abi, abi:GetIntrinsicModifierName(), {})
        end
        local abi = self:GetCaster():FindAbilityByName("monkey_king_boundless_strike_lua")
        if abi:GetLevel() > 0 then
            self:GetParent():AddNewModifier(self:GetCaster(), abi, abi:GetIntrinsicModifierName(), {})
        end
    else
        self:GetParent():RemoveModifierByName("modifier_monkey_king_banana_attack")
        self:GetParent():RemoveModifierByName("modifier_monkey_king_jingu_mastery_lua")
        self:GetParent():RemoveModifierByName("modifier_monkey_king_boundless_strike_lua_passive")
    end
    --обновляем предметы
    for i = 0,18 do
        local item = self:GetCaster():GetItemInSlot(i)
        if item then
            local new_item = CreateItem(item:GetName(), nil, nil)
            local soldier_item = self:GetParent():AddItem(new_item)
            soldier_item:SetPurchaser(self:GetParent())
            soldier_item:SetLevel(item:GetLevel())
            if item and item:GetCurrentCharges() > 0 and new_item and not new_item:IsNull() then
                new_item:SetCurrentCharges(item:GetCurrentCharges())
            end
            if new_item and not new_item:IsNull() then 
                self:GetParent():SwapItems(new_item:GetItemSlot(), i)
            end
        end
    end
    while self:GetCaster():GetLevel() > self:GetParent():GetLevel() do
        self:GetParent():CreatureLevelUp(1)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_int10") then
        for _,mod in pairs(self:GetCaster():FindAllModifiers()) do
            if IsTranferableModifier_pet(mod:GetName()) then
                if new_mod then
                    new_mod:Destroy()
                end
                new_mod = self:GetParent():AddNewModifier(self:GetParent(), mod:GetAbility(), mod:GetName(), {})
            end
            if IsTranferableModifier_talent(mod:GetName()) then
                local new_mod = self:GetParent():FindModifierByName(mod:GetName())
                if not new_mod then
                    new_mod = self:GetParent():AddNewModifier(self:GetParent(), nil, mod:GetName(), {})
                end
                new_mod:SetStackCount(mod:GetStackCount())
            end
        end
    end
end

function modifier_monkey_king_wukongs_command_lua_solider_active:OnIntervalThink()
    if not self.active then
        return
    end
    if self.target and not self.target:IsNull() and self.target:IsAlive() and self:CanAttackTarget(self.target) then
        return
    end
    local units = FindUnitsInRadius(self.team, self:GetParent():GetAbsOrigin(), nil, self.attack_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,u in pairs(units) do
        --если есть цель атаки будем атаковать ее пока не убьем или она не выйдет из радиуса атаки
        if self:GetParent():GetAggroTarget() == nil then
            self.target = u
            self:GetParent():SetAggroTarget(u)
            self:GetParent():MoveToTargetToAttack(u)
            self:StartIntervalThink(self.attack_time)
            return
        end
    end
    --если нет цели атаки принимаем айдл стойку
    -- self:GetParent():Stop()
	self:GetParent():SetAggroTarget(nil)
    self:StartIntervalThink(FrameTime())
end

function modifier_monkey_king_wukongs_command_lua_solider_active:CanAttackTarget(target)
    if (target:GetAbsOrigin() - self:GetParent():GetAbsOrigin()):Length2D() < self.attack_range then
        return true
    end
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_active:OnDestroy()
    if not IsServer() then
        return
    end
    for i = 0,18 do
        local item = self:GetParent():GetItemInSlot(i)
        if item then
            item:Destroy()
        end
    end
    self:GetParent():AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_wukongs_command_lua_solider_hidden", {})
end

function modifier_monkey_king_wukongs_command_lua_solider_active:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
        MODIFIER_PROPERTY_BASE_ATTACK_TIME_CONSTANT,
        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE
    }
end

function modifier_monkey_king_wukongs_command_lua_solider_active:CheckState()
    return {
        [MODIFIER_STATE_ROOTED] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
        [MODIFIER_STATE_UNSELECTABLE] = not IsInToolsMode(),
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
    }
end

function modifier_monkey_king_wukongs_command_lua_solider_active:GetActivityTranslationModifiers()
    return "run_fast"
end

function modifier_monkey_king_wukongs_command_lua_solider_active:GetModifierBaseAttackTimeConstant()
    return self.attack_time
end

function modifier_monkey_king_wukongs_command_lua_solider_active:GetModifierTotalDamageOutgoing_Percentage()
    return self.outgoing
end
--------------------------------------------------------------------------------------------------------------------------------------
--модифаер скрытия солдата когда он умирает
--------------------------------------------------------------------------------------------------------------------------------------

modifier_monkey_king_wukongs_command_lua_solider_hidden = class({})
--Classifications template
function modifier_monkey_king_wukongs_command_lua_solider_hidden:IsHidden()
    return true
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:IsDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:IsPurgable()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_wukongs_command_lua_solider_hidden:IsStunDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:RemoveOnDeath()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:DestroyOnExpire()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:OnCreated()
    if not IsServer() then
        return
    end
    self:GetParent():AddNoDraw()
    self:GetParent():SetDayTimeVisionRange(0)
    self:GetParent():SetNightTimeVisionRange(0)
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetParent():RemoveNoDraw()
    self:GetParent():SetDayTimeVisionRange(600)
    self:GetParent():SetNightTimeVisionRange(600)
end

function modifier_monkey_king_wukongs_command_lua_solider_hidden:CheckState()
    return {
        [MODIFIER_STATE_ROOTED] = true,
        [MODIFIER_STATE_STUNNED] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
        [MODIFIER_STATE_MAGIC_IMMUNE] = true,
        [MODIFIER_STATE_SILENCED] = true,
        [MODIFIER_STATE_MUTED] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_UNSELECTABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
        [MODIFIER_STATE_OUT_OF_GAME] = true,
        [MODIFIER_STATE_MUTED] = true,
    }
end

--------------------------------------------------------------------------------------------------------------------------------------
--статус эффект и активити
--------------------------------------------------------------------------------------------------------------------------------------

modifier_monkey_king_wukongs_command_lua_solider_status = class({})
--Classifications template
function modifier_monkey_king_wukongs_command_lua_solider_status:IsHidden()
    return true
end

function modifier_monkey_king_wukongs_command_lua_solider_status:IsDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_status:IsPurgable()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_status:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_wukongs_command_lua_solider_status:IsStunDebuff()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_status:RemoveOnDeath()
    return false
end

function modifier_monkey_king_wukongs_command_lua_solider_status:DestroyOnExpire()
    return true
end

function modifier_monkey_king_wukongs_command_lua_solider_status:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
    }
end
  
function modifier_monkey_king_wukongs_command_lua_solider_status:GetActivityTranslationModifiers()
    return "fur_army_soldier"
end

function modifier_monkey_king_wukongs_command_lua_solider_status:GetStatusEffectName()
    return "particles/status_fx/status_effect_monkey_king_fur_army.vpcf"
end