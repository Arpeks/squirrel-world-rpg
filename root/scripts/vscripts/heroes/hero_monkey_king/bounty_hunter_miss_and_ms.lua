LinkLuaModifier( "modifier_bounty_hunter_miss_and_ms", "heroes/hero_monkey_king/bounty_hunter_miss_and_ms.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if bounty_hunter_miss_and_ms == nil then
	bounty_hunter_miss_and_ms = class({})
end
function bounty_hunter_miss_and_ms:GetIntrinsicModifierName()
	return "modifier_bounty_hunter_miss_and_ms"
end
---------------------------------------------------------------------
--Modifiers
if modifier_bounty_hunter_miss_and_ms == nil then
	modifier_bounty_hunter_miss_and_ms = class({})
end
function modifier_bounty_hunter_miss_and_ms:OnCreated(params)
	if IsServer() then
	end
end
function modifier_bounty_hunter_miss_and_ms:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_bounty_hunter_miss_and_ms:OnDestroy()
	if IsServer() then
	end
end
function modifier_bounty_hunter_miss_and_ms:DeclareFunctions()
	return {
	}
end