monkey_king_jingu_mastery_lua = class({})

LinkLuaModifier("modifier_monkey_king_jingu_mastery_lua", "heroes/hero_monkey_king/monkey_king_jingu_mastery_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_jingu_mastery_lua_counter", "heroes/hero_monkey_king/monkey_king_jingu_mastery_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_jingu_mastery_lua_active", "heroes/hero_monkey_king/monkey_king_jingu_mastery_lua.lua", LUA_MODIFIER_MOTION_NONE)

function monkey_king_jingu_mastery_lua:GetIntrinsicModifierName()
    return "modifier_monkey_king_jingu_mastery_lua"
end

modifier_monkey_king_jingu_mastery_lua = class({})
--Classifications template
function modifier_monkey_king_jingu_mastery_lua:IsHidden()
    return true
end

function modifier_monkey_king_jingu_mastery_lua:IsDebuff()
    return false
end

function modifier_monkey_king_jingu_mastery_lua:IsPurgable()
    return false
end

function modifier_monkey_king_jingu_mastery_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_jingu_mastery_lua:IsStunDebuff()
    return false
end

function modifier_monkey_king_jingu_mastery_lua:RemoveOnDeath()
    return false
end

function modifier_monkey_king_jingu_mastery_lua:DestroyOnExpire()
    return false
end

function modifier_monkey_king_jingu_mastery_lua:OnCreated()
    self.counter_duration = self:GetAbility():GetSpecialValueFor("counter_duration")
end

function modifier_monkey_king_jingu_mastery_lua:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,

        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_monkey_king_jingu_mastery_lua:CustomOnAttackLanded(data)
    if data.attacker ~= self:GetParent() then return end
    if data.target:IsBuilding() then return end
    if self:GetParent():PassivesDisabled() then return end
    if self:GetParent():HasModifier("modifier_monkey_king_jingu_mastery_lua_active") then return end
    data.target:AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_monkey_king_jingu_mastery_lua_counter", {duration = self.counter_duration})
end

function modifier_monkey_king_jingu_mastery_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			return 1
		end
	end
	return 0
end

function modifier_monkey_king_jingu_mastery_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_agi10") then
                value = value + self:GetCaster():GetAgility()
            end
            return value
		end
	end
	return 0
end

modifier_monkey_king_jingu_mastery_lua_counter = class({})
--Classifications template
function modifier_monkey_king_jingu_mastery_lua_counter:IsHidden()
    return false
end

function modifier_monkey_king_jingu_mastery_lua_counter:IsDebuff()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_counter:IsPurgable()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_counter:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_jingu_mastery_lua_counter:IsStunDebuff()
    return false
end

function modifier_monkey_king_jingu_mastery_lua_counter:RemoveOnDeath()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_counter:DestroyOnExpire()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_counter:OnCreated()
    if not IsServer() then
        return
    end
    self.required_hits = self:GetAbility():GetSpecialValueFor("required_hits")
    self:SetStackCount(1)
    self.pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_quad_tap_stack.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(self.pcf, 1, Vector(0,self:GetStackCount(),0))
    self:AddParticle(self.pcf, false, false, -1, false, true)
end

function modifier_monkey_king_jingu_mastery_lua_counter:OnRefresh()
    if not IsServer() then
        return
    end
    self:IncrementStackCount()
    ParticleManager:SetParticleControl(self.pcf, 1, Vector(0,self:GetStackCount(),0))
end

function modifier_monkey_king_jingu_mastery_lua_counter:OnStackCountChanged()
    if self:GetStackCount() >= self.required_hits then
        self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_monkey_king_jingu_mastery_lua_active", {})
        EmitSoundOn("Hero_MonkeyKing.IronCudgel", self:GetCaster())
        self:Destroy()
    end
end

modifier_monkey_king_jingu_mastery_lua_active = class({})
--Classifications template
function modifier_monkey_king_jingu_mastery_lua_active:IsHidden()
    return false
end

function modifier_monkey_king_jingu_mastery_lua_active:IsDebuff()
    return false
end

function modifier_monkey_king_jingu_mastery_lua_active:IsPurgable()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_active:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_jingu_mastery_lua_active:IsStunDebuff()
    return false
end

function modifier_monkey_king_jingu_mastery_lua_active:RemoveOnDeath()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_active:DestroyOnExpire()
    return true
end

function modifier_monkey_king_jingu_mastery_lua_active:OnCreated()
    self.max_duration = self:GetAbility():GetSpecialValueFor("max_duration")
    self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
    self.charges = self:GetAbility():GetSpecialValueFor("charges")
    self.lifesteal = self:GetAbility():GetSpecialValueFor("lifesteal") / 100
    if not IsServer() then
        return
    end
    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_quad_tap_overhead.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
    self:AddParticle(pcf, false, false, -1, false, true)
    self.special_bonus_unique_npc_dota_hero_monkey_king_str9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_str9") ~= nil
    if self.special_bonus_unique_npc_dota_hero_monkey_king_str9 then
        self:SetDuration(-1, true)
        self:SetStackCount(self.charges)
        self:StartIntervalThink(1)
    else
        self:SetDuration(self.max_duration, true)
        self:SetStackCount(self.charges)
    end
end

function modifier_monkey_king_jingu_mastery_lua_active:OnIntervalThink()
    self.special_bonus_unique_npc_dota_hero_monkey_king_str9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_str9") ~= nil
    if not self.special_bonus_unique_npc_dota_hero_monkey_king_str9 then
        self:SetDuration(self.max_duration, true)
        self:SetStackCount(self.charges)
        self:StartIntervalThink(-1)
    end
end

function modifier_monkey_king_jingu_mastery_lua_active:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED
    }
end

function modifier_monkey_king_jingu_mastery_lua_active:GetModifierPreAttack_BonusDamage()
    return self.bonus_damage
end

function modifier_monkey_king_jingu_mastery_lua_active:CustomOnAttackLanded(data)
    if data.attacker == self:GetParent() and not data.target:IsBuilding() and not data.attacker:PassivesDisabled() then
        local lifePfx = ParticleManager:CreateParticle("particles/generic_gameplay/generic_lifesteal.vpcf", PATTACH_POINT_FOLLOW, data.attacker)
        ParticleManager:ReleaseParticleIndex(lifePfx)
        local hitPfx = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_quad_tap_hit.vpcf", PATTACH_POINT_FOLLOW, data.target)
        ParticleManager:SetParticleControl(hitPfx, 1, data.target:GetAbsOrigin())
        ParticleManager:ReleaseParticleIndex(hitPfx)
        data.attacker:HealWithParams(data.damage * self.lifesteal, self, true, true, data.attacker, false)
        if self:GetCaster().cast_boundless_strike == true or not self.special_bonus_unique_npc_dota_hero_monkey_king_str9 then
            self:DecrementStackCount()
        end
        if self:GetStackCount() <= 0 then
            self:Destroy()
        end
    end
end
