monkey_king_boundless_strike_lua = class({})

LinkLuaModifier("modifier_monkey_king_boundless_strike_lua_passive", "heroes/hero_monkey_king/monkey_king_boundless_strike_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_boundless_strike_lua", "heroes/hero_monkey_king/monkey_king_boundless_strike_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_monkey_king_boundless_strike_lua_crit", "heroes/hero_monkey_king/monkey_king_boundless_strike_lua", LUA_MODIFIER_MOTION_NONE)

function monkey_king_boundless_strike_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function monkey_king_boundless_strike_lua:GetIntrinsicModifierName()
    return "modifier_monkey_king_boundless_strike_lua_passive"
end

function monkey_king_boundless_strike_lua:OnAbilityPhaseStart()
	EmitSoundOn("Hero_MonkeyKing.Strike.Cast", self:GetCaster())

	self.pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_strike_cast.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControl(self.pfx, 0, self:GetCaster():GetAbsOrigin())
	ParticleManager:SetParticleControlEnt(self.pfx, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_weapon_bot", self:GetCaster():GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(self.pfx, 2, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_weapon_top", self:GetCaster():GetAbsOrigin(), true)
	ParticleManager:ReleaseParticleIndex(self.pfx)

	return true
end

function monkey_king_boundless_strike_lua:OnAbilityPhaseInterrupted()
    ParticleManager:DestroyParticle(self.pfx, true)
    ParticleManager:ReleaseParticleIndex(self.pfx)
end

function monkey_king_boundless_strike_lua:OnSpellStart()
    local start_point = self:GetCaster():GetAbsOrigin()
    local end_point = (self:GetCursorPosition() - start_point):Normalized() * self:GetCastRange(start_point, nil) + start_point
    local width = self:GetSpecialValueFor("strike_radius")
    local stun_duration = self:GetSpecialValueFor("stun_duration")
    local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_monkey_king_boundless_strike_lua_crit", {})
    local units = FindUnitsInLine(self:GetCaster():GetTeamNumber(), start_point, end_point, nil, width, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE)
    local m = self:GetCaster():FindModifierByName("modifier_monkey_king_jingu_mastery_lua_active")
    if m then
        m:DecrementStackCount()
    end
    self:GetCaster().cast_boundless_strike = true
    for _,u in pairs(units) do
        u:AddNewModifier(self:GetCaster(), self, "modifier_stunned", {duration = stun_duration})
        self:GetCaster():PerformAttack(u, true, true, true, true, false, false, true)
    end
    self:GetCaster().cast_boundless_strike = false
    mod:Destroy()

    EmitSoundOnLocationWithCaster(start_point, "Hero_MonkeyKing.Strike.Impact", self:GetCaster())
	EmitSoundOnLocationWithCaster(end_point, "Hero_MonkeyKing.Strike.Impact.EndPos", self:GetCaster())

	local pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_strike.vpcf", PATTACH_POINT, self:GetCaster())
	-- ParticleManager:SetParticleControlForward(pfx, 0, self:GetCaster())
	ParticleManager:SetParticleControl(pfx, 1, end_point)
	ParticleManager:ReleaseParticleIndex(pfx)
end

modifier_monkey_king_boundless_strike_lua_crit = class({})
--Classifications template
function modifier_monkey_king_boundless_strike_lua_crit:IsHidden()
    return true
end

function modifier_monkey_king_boundless_strike_lua_crit:IsDebuff()
    return false
end

function modifier_monkey_king_boundless_strike_lua_crit:IsPurgable()
    return false
end

function modifier_monkey_king_boundless_strike_lua_crit:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_boundless_strike_lua_crit:IsStunDebuff()
    return false
end

function modifier_monkey_king_boundless_strike_lua_crit:RemoveOnDeath()
    return false
end

function modifier_monkey_king_boundless_strike_lua_crit:DestroyOnExpire()
    return false
end

function modifier_monkey_king_boundless_strike_lua_crit:OnCreated()
    self.strike_crit_mult = self:GetAbility():GetSpecialValueFor("strike_crit_mult")
    self.strike_bonus_damage = self:GetAbility():GetSpecialValueFor("strike_bonus_damage")
    self.special_bonus_unique_npc_dota_hero_monkey_king_int9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_int9") ~= nil
end

function modifier_monkey_king_boundless_strike_lua_crit:DeclareFunctions()
   return {
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
        MODIFIER_PROPERTY_OVERRIDE_ATTACK_MAGICAL,
        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
    }
end

function modifier_monkey_king_boundless_strike_lua_crit:GetModifierPreAttack_BonusDamage()
    return self.strike_bonus_damage
end

function modifier_monkey_king_boundless_strike_lua_crit:GetModifierPreAttack_CriticalStrike()
    return self.strike_crit_mult
end

function modifier_monkey_king_boundless_strike_lua_crit:GetOverrideAttackMagical()
	return 1
end

function modifier_monkey_king_boundless_strike_lua_crit:GetModifierTotalDamageOutgoing_Percentage( data )
	if not self.special_bonus_unique_npc_dota_hero_monkey_king_int9 then return 0 end
	if data.inflictor then return 0 end
	if data.damage_category~=DOTA_DAMAGE_CATEGORY_ATTACK then return 0 end
	if data.damage_type~=DAMAGE_TYPE_PHYSICAL then return 0 end
	if not data.target:IsMagicImmune() then 
        local damage = data.original_damage * 0.5
		ApplyDamageRDA({
			victim = data.target,
			attacker = self:GetParent(),
			damage = damage,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flag = DOTA_DAMAGE_FLAG_MAGIC_AUTO_ATTACK,
			ability = self:GetAbility()
		})
	end
	return -200
end










modifier_monkey_king_boundless_strike_lua_passive = class({})
--Classifications template
function modifier_monkey_king_boundless_strike_lua_passive:IsHidden()
    return true
end

function modifier_monkey_king_boundless_strike_lua_passive:IsDebuff()
    return false
end

function modifier_monkey_king_boundless_strike_lua_passive:IsPurgable()
    return false
end

function modifier_monkey_king_boundless_strike_lua_passive:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_boundless_strike_lua_passive:IsStunDebuff()
    return false
end

function modifier_monkey_king_boundless_strike_lua_passive:RemoveOnDeath()
    return false
end

function modifier_monkey_king_boundless_strike_lua_passive:DestroyOnExpire()
    return false
end

function modifier_monkey_king_boundless_strike_lua_passive:OnCreated()
    if not IsServer() then
        return
    end
    self.can_proc = true
end

function modifier_monkey_king_boundless_strike_lua_passive:OnIntervalThink()
    local start_point = self:GetParent():GetAbsOrigin()
    local end_point = (self.point - start_point):Normalized() * self:GetAbility():GetCastRange(start_point, nil) + start_point
    local width = self:GetAbility():GetSpecialValueFor("strike_radius")
    local stun_duration = self:GetAbility():GetSpecialValueFor("stun_duration")
    local mod = self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_monkey_king_boundless_strike_lua_crit", {})
    local units = FindUnitsInLine(self:GetCaster():GetTeamNumber(), start_point, end_point, nil, width, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE)
    local m = self:GetCaster():FindModifierByName("modifier_monkey_king_jingu_mastery_lua_active")
    if m then
        m:DecrementStackCount()
    end
    self:GetParent().cast_boundless_strike = true
    for _,u in pairs(units) do
        self:GetParent():PerformAttack(u, true, true, true, true, false, false, true)
    end
    self:GetParent().cast_boundless_strike = false
    mod:Destroy()

	local pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_strike.vpcf", PATTACH_POINT, self:GetParent())
	-- ParticleManager:SetParticleControlForward(pfx, 0, self:GetCaster())
	ParticleManager:SetParticleControl(pfx, 1, end_point)
	ParticleManager:SetParticleControl(pfx, 2, end_point)
	ParticleManager:ReleaseParticleIndex(pfx)
    self.can_proc = true
    self:StartIntervalThink(-1)
end

function modifier_monkey_king_boundless_strike_lua_passive:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,

        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_monkey_king_boundless_strike_lua_passive:CustomOnAttackLanded(data)
    if data.attacker ~= self:GetParent() then return end
    if data.target:IsBuilding() then return end
    if self:GetParent():PassivesDisabled() then return end
    if self.can_proc and not self:GetParent().cast_boundless_strike and RollPercentage(self:GetAbility():GetSpecialValueFor("chance")) then
        self.can_proc = false
        self.point = data.target:GetAbsOrigin()
        self.pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_monkey_king/monkey_king_strike_cast.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
        ParticleManager:SetParticleControl(self.pfx, 0, self:GetParent():GetAbsOrigin())
        ParticleManager:SetParticleControlEnt(self.pfx, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_weapon_bot", self:GetParent():GetAbsOrigin(), true)
        ParticleManager:SetParticleControlEnt(self.pfx, 2, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_weapon_top", self:GetParent():GetAbsOrigin(), true)
        ParticleManager:ReleaseParticleIndex(self.pfx)
        self:StartIntervalThink(0.4)
    end
end

function modifier_monkey_king_boundless_strike_lua_passive:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "strike_bonus_damage" then
			return 1
		end
        if data.ability_special_value == "strike_crit_mult" then
			return 1
		end
	end
	return 0
end

function modifier_monkey_king_boundless_strike_lua_passive:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "strike_bonus_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "strike_bonus_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_agi9") then
                value = value + self:GetCaster():GetDamageMax()
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_str11") then
                value = value + self:GetCaster():GetStrength()
            end
            return value
		end
        if data.ability_special_value == "strike_crit_mult" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "strike_crit_mult", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_agi12") then
                value = value + self:GetCaster():GetLevel() * 2 + 400
            end
            return value
		end
	end
	return 0
end