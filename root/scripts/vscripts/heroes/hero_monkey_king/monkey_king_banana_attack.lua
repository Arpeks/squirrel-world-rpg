monkey_king_banana_attack = class({})

LinkLuaModifier("modifier_monkey_king_banana_attack", "heroes/hero_monkey_king/monkey_king_banana_attack.lua", LUA_MODIFIER_MOTION_NONE)

function monkey_king_banana_attack:GetIntrinsicModifierName()
    return "modifier_monkey_king_banana_attack"
end

modifier_monkey_king_banana_attack = class({})
--Classifications template
function modifier_monkey_king_banana_attack:IsHidden()
    return true
end

function modifier_monkey_king_banana_attack:IsDebuff()
    return false
end

function modifier_monkey_king_banana_attack:IsPurgable()
    return false
end

function modifier_monkey_king_banana_attack:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_monkey_king_banana_attack:IsStunDebuff()
    return false
end

function modifier_monkey_king_banana_attack:RemoveOnDeath()
    return false
end

function modifier_monkey_king_banana_attack:DestroyOnExpire()
    return false
end

function modifier_monkey_king_banana_attack:OnCreated()
    self.chance = self:GetAbility():GetSpecialValueFor("chance")
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.damage = self:GetAbility():GetSpecialValueFor("damage")
    self:StartIntervalThink(1)
end

function modifier_monkey_king_banana_attack:OnIntervalThink()
    self.chance = self:GetAbility():GetSpecialValueFor("chance")
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.damage = self:GetAbility():GetSpecialValueFor("damage")
end

function modifier_monkey_king_banana_attack:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,

        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
end

function modifier_monkey_king_banana_attack:CustomOnAttackLanded(data)
    if data.attacker ~= self:GetParent() then return end
    if data.target:IsBuilding() then return end
    if self:GetParent():PassivesDisabled() then return end
    if not self:GetAbility():IsCooldownReady() then return end
    if RollPercentage(self.chance) then
        local damage_table = {
            attacker = data.attacker,
            damage_type = DAMAGE_TYPE_PHYSICAL,
            ability = self:GetAbility(),
            damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
            damage = self.damage,
            damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.10
        }
        local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), data.target:GetAbsOrigin(), nil, self.radius,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_ANY_ORDER, false)
        for _,u in pairs(units) do
            damage_table.victim = u
            ApplyDamageRDA(damage_table)
        end
        self:GetAbility():UseResources(false, false, false, true)
    end
end

function modifier_monkey_king_banana_attack:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
        if data.ability_special_value == "special_bonus_unique_npc_dota_hero_monkey_king_str13" then
			return 1
		end
	end
	return 0
end

function modifier_monkey_king_banana_attack:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_int8") then
                value = value + self:GetCaster():GetIntellect(true) * 1.25
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_monkey_king_str13") then
                value = value * 2
            end
            return value
		end
	end
	return 0
end