"DOTAAbilities"
{
	"magnataur_empower_lua"
	{

        "BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_magnataur/magnataur_empower_lua/magnataur_empower_lua"
        "AbilityTextureName"			"magnataur_empower"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_DONT_RESUME_ATTACK"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ALLIES_YES"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"AbilitySound"					"Hero_Magnataur.Empower.Cast"

		"MaxLevel"						"15"
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"800"
		"AbilityCastPoint"				"0.3 0.3 0.3 0.3"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"12"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"45 60 75 90"

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportValue"	"0.3"	// Easy to spam 

		// Special
		//-------------------------------------------------------------------------------------------------------------
        "AbilityValues"
        {
            "empower_duration"
            {
                "value"                 "40"
            }
            "bonus_damage_pct"
            {
                "value"                 "12 14 16 18 20 22 24 26 28 30 32 34 36 38 40"
            }
            "cleave_damage_pct"
            {
                "value"                 "20 24 28 32 36 40 44 48 52 56 60 64 68 72 76"
            }
            "cleave_starting_width"
            {
                "value"                 "150"
                "affected_by_aoe_increase"	"1"
            }
            "cleave_ending_width"
            {
                "value"                 "360"
                "affected_by_aoe_increase"	"1"
            }
            "cleave_distance"
            {
                "value"                 "650"
				"affected_by_aoe_increase"	"1"
            }
            "self_multiplier"
            {
                "value"                 "1.75"
            }
        }
		"AbilityCastAnimation"		"ACT_DOTA_CAST_ABILITY_2"
	}
}