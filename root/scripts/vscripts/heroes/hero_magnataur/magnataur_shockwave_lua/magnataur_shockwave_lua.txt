"DOTAAbilities"
{
	//=================================================================================================================
	// Magnus: Shockwave (Lua version)
	//=================================================================================================================
	"magnataur_shockwave_lua"
	{
		// Ability Technical Aspect
		// base script folder	: scripts/vscripts
		// base texture folder	: resource/flash3/images/spellicons
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_magnataur/magnataur_shockwave_lua/magnataur_shockwave_lua"
		"AbilityTextureName"			"magnataur_shockwave"
		"FightRecapLevel"				"1"
		"MaxLevel"						"15"
		
		// Ability General
		//-------------------------------------------------------------------------------------------------------------
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"	
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"

		// Ability Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1200"
		"AbilityCastPoint"				"0.3 0.3 0.3 0.3"
		"AbilityDuration"				"0.6875 0.6875 0.6875 0.6875"

		// Ability Resource
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"15"
		"AbilityManaCost"				"90 100 110 120"

		// Damage
		//-------------------------------------------------------------------------------------------------------------

		// Special
		//-------------------------------------------------------------------------------------------------------------
       "AbilityValues"
        {
            "shock_speed"
            {
                "value"                 "900"
            }
            "shock_width"
            {
                "value"                 "250"
				"affected_by_aoe_increase"	"1"
            }
            "movement_slow"
            {
                "value"                 "75"
            }
            "slow_duration"
            {
                "value"                 "1.0"
            }
            "shock_damage"
            {
                "value"                 "50 100 150 200 250 300 350 400 450 500 550 600 650 700 750"
            }
            "pull_duration"
            {
                "value"                 "0.2"
            }
            "pull_distance"
            {
                "value"                 "150"
            }
            "wave_count"
            {
                "value"                 "1 1 1 2 2 2 3 3 3 4 4 4 5 5 5"
            }
        }
	}
}