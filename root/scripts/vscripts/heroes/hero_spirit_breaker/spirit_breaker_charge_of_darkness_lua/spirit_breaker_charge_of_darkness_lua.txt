"DOTAAbilities"
{
	//=================================================================================================================
	// Spirit Breaker: Charge of Darkness (Lua version)
	//=================================================================================================================
	"spirit_breaker_charge_of_darkness_lua"
	{
		// Ability Technical Aspect
		// base script folder	: scripts/vscripts
		// base texture folder	: resource/flash3/images/spellicons
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_spirit_breaker/spirit_breaker_charge_of_darkness_lua/spirit_breaker_charge_of_darkness_lua"
		"AbilityTextureName"			"spirit_breaker_charge_of_darkness"
		"FightRecapLevel"				"1"
		"MaxLevel"						"15"

		"HasScepterUpgrade"			"1"
		
		// Ability General
		//-------------------------------------------------------------------------------------------------------------
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_DONT_ALERT_TARGET | DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellDispellableType"			"SPELL_DISPELLABLE_NO"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"

		// Ability Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0.3"
		"AbilityCastRange"				"0"

		// Ability Resource
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"24 23 22 21 20 19 18 17 16 15 14 13 12 11 10"
		"AbilityManaCost"				"70 80 90 100"
		"AbilityCastRange"				"1500"
		// Damage
		//-------------------------------------------------------------------------------------------------------------

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"cast_range"
			{
				"value"					"1500"
			}
			"movement_speed"
			{
				"value"					"300 310 320 330 340 350 360 370 380 390 400 410 420 430 440"
			}
			"stun_duration"
			{
				"value"					"0.5 0.55 0.6 0.65 0.70 0.75 0.8 0.85 0.90 0.95 1.0 1.05 1.10 1.15 1.20"
			}
			"bash_radius"
			{
				"value"					"300"
				"affected_by_aoe_increase"	"1"
			}
			"vision_radius"
			{
				"value"					"400"
				"affected_by_aoe_increase"	"1"
			}
			"vision_duration"
			{
				"value"					"0.94"
			}
		}
	}
}