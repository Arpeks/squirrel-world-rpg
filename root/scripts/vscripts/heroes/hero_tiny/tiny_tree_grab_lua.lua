LinkLuaModifier( "modifier_tiny_tree_grab_lua", "heroes/hero_tiny/tiny_tree_grab_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_tiny_tree_grab_lua_armor_reduction", "heroes/hero_tiny/tiny_tree_grab_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_tiny_tree_grab_lua_tree", "heroes/hero_tiny/tiny_tree_grab_lua.lua", LUA_MODIFIER_MOTION_NONE )

tiny_toss_tree_lua = class({})

function tiny_toss_tree_lua:GetAbilityTextureName()
    if self:GetCaster():HasModifier("modifier_tiny_prestige_check") then
        return "tiny/ti9_immortal/tiny_toss_tree_immortal"
    end
    return "tiny_toss_tree"
end

function tiny_toss_tree_lua:Precache(context)
	PrecacheResource( "particle", "particles/units/heroes/hero_tiny/tiny_tree_grab.vpcf", context )
end

function tiny_toss_tree_lua:OnSpellStart()
	local target = self:GetCursorTarget()
	if target == nil then 
		velocity = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized() * self:GetSpecialValueFor("speed")
		local projectile =  
		{
			EffectName 			= self:GetCaster().tree_linear_effect,
			Ability 			= self,
			vSpawnOrigin 		= self:GetCaster():GetOrigin(),
			fDistance 			= self:GetSpecialValueFor("range"),
			fStartRadius 		= self:GetSpecialValueFor("splash_radius"),
			fEndRadius 			= self:GetSpecialValueFor("splash_radius"),
			Source 				= self:GetCaster(),
			bHasFrontalCone 	= true,
			bReplaceExisting 	= false,
			bProvidesVision 	= true,
			iVisionTeamNumber 	= self:GetCaster():GetTeam(),
			iVisionRadius 		= self:GetSpecialValueFor("splash_radius"),
			bDrawsOnMinimap 	= false,
			bVisibleToEnemies 	= true, 
			bDeleteOnHit		= true,
			iUnitTargetTeam 	= DOTA_UNIT_TARGET_TEAM_ENEMY,
			iUnitTargetFlags 	= DOTA_UNIT_TARGET_FLAG_NONE,
			iUnitTargetType 	= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
			vVelocity 			= Vector(velocity.x,velocity.y,0),
			fExpireTime 		= GameRules:GetGameTime() + 10,
			ExtraData 			= 
			{
				splash_radius 	= self:GetSpecialValueFor("splash_radius")
			}
		}				

		ProjectileManager:CreateLinearProjectile(projectile)
	else
		distance = (target:GetAbsOrigin() - self:GetCaster():GetAbsOrigin()):Length2D()
		local projectile =  
		{
			EffectName 			= self:GetCaster().tree_tracking_effect,
			Ability 			= self,
			fDistance 			= distance,
			fStartRadius 		= self:GetSpecialValueFor("splash_radius"),
			fEndRadius 			= self:GetSpecialValueFor("splash_radius"),
			Source 				= self:GetCaster(),
			Target 				= target,
			iMoveSpeed 			= self:GetSpecialValueFor("speed"),
			bHasFrontalCone 	= true,
			bReplaceExisting 	= false,
			bProvidesVision 	= true,
			iVisionTeamNumber 	= self:GetCaster():GetTeam(),
			iVisionRadius 		= self:GetSpecialValueFor("splash_radius"),
			bDrawsOnMinimap 	= false,
			bVisibleToEnemies 	= true, 
			bDeleteOnHit		= true,
			bDodgeable 			= true,
			fExpireTime 		= GameRules:GetGameTime() + 10,
			ExtraData 			= 
			{
				splash_radius 	= self:GetSpecialValueFor("splash_radius")
			}
		}				
		ProjectileManager:CreateTrackingProjectile(projectile)
	end
	if string.find( self:GetCaster():GetModelName(), "prestige") then
		EmitSoundOn("Hero_Tiny.Prestige.Throw", self:GetCaster())
		self.sound = "Hero_Tiny.Prestige.Target"
	else
		EmitSoundOn("Hero_Tiny.Tree.Throw", self:GetCaster())
		self.sound = "Hero_Tiny.Tree.Target"
	end
	self:GetCaster():RemoveModifierByName("modifier_tiny_tree_grab_lua_tree")
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi7") then
		local mod = self:GetCaster():FindModifierByName("modifier_tiny_tree_grab_lua")
		if mod then
			mod:SetStackCount(mod:GetStackCount() + 15)
		end
	end
end

function tiny_toss_tree_lua:OnProjectileHit( target, location )
	if not target then
		EmitSoundOnLocationWithCaster(location, self.sound, self:GetCaster())
		return nil
	end
	local cleave_targets = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),target:GetAbsOrigin(),nil,self:GetSpecialValueFor("splash_radius"),DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
	for _,unit in pairs(cleave_targets) do
		if unit ~= target then
			ApplyDamageRDA({
				victim = unit,
				attacker = self:GetCaster(),
				damage = self:GetCaster():GetDamageMax(),
				damage_type = DAMAGE_TYPE_PHYSICAL,
				damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
			})
		end
	end
	self:GetCaster():PerformAttack(target, true, true, true, true, true, false, true)
	return true
end

tiny_tree_grab_lua = class({})

function tiny_tree_grab_lua:GetAbilityTextureName()
    if self:GetCaster():HasModifier("modifier_tiny_prestige_check") then
        return "tiny/ti9_immortal/tiny_tree_grab_immortal"
    end
    return "tiny_tree_grab"
end

function tiny_tree_grab_lua:Spawn()
	if not IsServer() then
		return
	end
	Timers:CreateTimer(0.1,function()
		if not self.tree then
			local model = "models/heroes/tiny/tiny_tree/tiny_tree.vmdl"
			if string.find( self:GetCaster():GetModelName(), "prestige") then
				model = "models/items/tiny/tiny_prestige/tiny_prestige_sword.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "ancient") then
				model = "models/items/tiny/ancient_tresure_keeper_palm/ancient_tresure_keeper_palm.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "burning") then
				model = "models/items/tiny/burning_stone_giant_club/burning_stone_giant_club.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "frozen") then
				model = "models/items/tiny/frozen_stonehenge/frozen_stonehenge_weapon_frostivus.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "scarletquarry") then
				model = "models/items/tiny/scarletquarry_weapon/scarletquarry_weapon.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "anthozoa") then
				model = "models/items/tiny/tiny_anthozoa_giant_weapon/tiny_anthozoa_giant_weapon.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "astral") then
				model = "models/items/tiny/tiny_astral_order_weapon/tiny_astral_order_weapon.vmdl"
			end
			if string.find( self:GetCaster():GetModelName(), "bone") then
				model = "models/items/tiny/tiny_bad_to_the_bone_weapon/tiny_bad_to_the_bone_weapon.vmdl"
			end
			self.tree = SpawnEntityFromTableSynchronous("prop_dynamic", {model = model})
			self.tree:SetContextThink("HideTree", function(unit)
				if string.find(unit:GetModelName(), "courier") ~= nil or self.tree.ShouldHidden then
					self.tree:RemoveEffects(EF_NODRAW)
				else
					self.tree:AddEffects(EF_NODRAW)
				end
				return FrameTime()
			end, FrameTime())
			self.tree:FollowEntity(self:GetCaster(), true)
			-- self.tree:FollowEntityMerge(self:GetCaster(), "Tree_1")
			self.tree:AddEffects(EF_NODRAW)
		end
	end)
end

function tiny_tree_grab_lua:GetIntrinsicModifierName()
	return "modifier_tiny_tree_grab_lua"
end

function tiny_tree_grab_lua:OnUpgrade()
	local abi = self:GetCaster():FindAbilityByName("tiny_toss_tree_lua")
	if abi then
		abi:SetLevel(self:GetLevel())
	end
end

function tiny_tree_grab_lua:OnSpellStart()
	local target = self:GetCursorTarget()
	target:CutDown(self:GetCaster():GetTeamNumber())
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_tiny_tree_grab_lua_tree", {})
	self:GetCaster():SwapAbilities("tiny_tree_grab_lua", "tiny_toss_tree_lua", false, true)
	if string.find( self:GetCaster():GetModelName(), "prestige") then
		EmitSoundOn("Hero_Tiny.Prestige.Grab", self:GetCaster())
	else
		EmitSoundOn("Hero_Tiny.Tree.Grab", self:GetCaster())
	end
end

modifier_tiny_tree_grab_lua = class({})
--Classifications template
function modifier_tiny_tree_grab_lua:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_tiny_tree_grab_lua:IsDebuff()
	return false
end

function modifier_tiny_tree_grab_lua:IsPurgable()
	return false
end

function modifier_tiny_tree_grab_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_tree_grab_lua:IsStunDebuff()
	return false
end

function modifier_tiny_tree_grab_lua:RemoveOnDeath()
	return false
end

function modifier_tiny_tree_grab_lua:DestroyOnExpire()
	return true
end

function modifier_tiny_tree_grab_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
	}
end

function modifier_tiny_tree_grab_lua:GetModifierBonusStats_Intellect()
	return self:GetStackCount()
end

modifier_tiny_tree_grab_lua_tree = class({})
--Classifications template
function modifier_tiny_tree_grab_lua_tree:IsHidden()
	return false
end

function modifier_tiny_tree_grab_lua_tree:IsDebuff()
	return false
end

function modifier_tiny_tree_grab_lua_tree:IsPurgable()
	return false
end

function modifier_tiny_tree_grab_lua_tree:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_tree_grab_lua_tree:IsStunDebuff()
	return false
end

function modifier_tiny_tree_grab_lua_tree:RemoveOnDeath()
	return true
end

function modifier_tiny_tree_grab_lua_tree:DestroyOnExpire()
	return true
end

function modifier_tiny_tree_grab_lua_tree:OnStackCountChanged()
	if self:GetStackCount() == 0 and self.attack_count ~= 0 then
		self:Destroy()
	end
end

function modifier_tiny_tree_grab_lua_tree:OnCreated()
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.splash_range = self:GetAbility():GetSpecialValueFor("splash_range")
	self.splash_pct = self:GetAbility():GetSpecialValueFor("splash_pct")
	self.attack_count = self:GetAbility():GetSpecialValueFor("attack_count")
	self.attack_range = self:GetAbility():GetSpecialValueFor("attack_range") - 150
	self.tiny_avalanche_lua = self:GetCaster():FindAbilityByName("tiny_avalanche_lua")
	if not IsServer() then
		return
	end
	self:GetAbility().tree.ShouldHidden = true
	self:GetAbility().tree:RemoveEffects(EF_NODRAW)
	self:SetStackCount(self.attack_count)
	self.effect = "particles/units/heroes/hero_tiny/tiny_tree_grab.vpcf"
	self.sound = "Hero_Tiny_Tree.Attack"
	if string.find( self:GetCaster():GetModelName(), "prestige") then
		self.effect = "particles/econ/items/tiny/tiny_prestige/tiny_prestige_tree_melee_hit.vpcf"
		self.sound = "Hero_Tiny_Prestige.Attack"
	end
	self:StartIntervalThink(1)
	self:SetHasCustomTransmitterData(true)
	self:SendBuffRefreshToClients()
end

function modifier_tiny_tree_grab_lua_tree:OnIntervalThink()
	self.base_damage = self:GetParent():GetBaseDamageMax()
	self:SendBuffRefreshToClients()
end

function modifier_tiny_tree_grab_lua_tree:OnDestroy()
	if not IsServer() then
		return
	end
	self:GetCaster():SwapAbilities("tiny_tree_grab_lua", "tiny_toss_tree_lua", true, false)
	local abi = self:GetCaster():FindAbilityByName("tiny_tree_grab_lua")
	if abi then
		abi:StartCooldown(abi:GetCooldown(abi:GetLevel()))
	end
	self:GetAbility().tree.ShouldHidden = false
	self:GetAbility().tree:AddEffects(EF_NODRAW)
end

function modifier_tiny_tree_grab_lua_tree:AddCustomTransmitterData()
	return {
		effect = self.effect,
		sound = self.sound,
		base_damage = self.base_damage,
	}
end

function modifier_tiny_tree_grab_lua_tree:HandleCustomTransmitterData( data )
	self.effect = data.effect
	self.sound = data.sound
	self.base_damage = data.base_damage
end

function modifier_tiny_tree_grab_lua_tree:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
		MODIFIER_PROPERTY_TRANSLATE_ATTACK_SOUND,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,

		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	}
end

function modifier_tiny_tree_grab_lua_tree:CustomOnAttackLanded(data)
	if data.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if data.target ~= nil and data.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = data.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()

				local facing_direction = data.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(data.attacker:GetTeamNumber(),data.attacker:GetAbsOrigin(),nil,self.splash_range,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= data.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - data.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = data.attacker,
								damage = data.damage * (self.splash_pct/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				if not (self.attack_count == 0 or self:GetCaster().AvalancheActive or self.tiny_avalanche_lua:GetBehavior() == DOTA_ABILITY_BEHAVIOR_PASSIVE) then
					self:DecrementStackCount()
				end
				local nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_tiny/tiny_tree_grab.vpcf", PATTACH_POINT, self:GetCaster())
				ParticleManager:SetParticleControl(nfx, 0, data.target:GetAbsOrigin())
				ParticleManager:SetParticleControl(nfx, 1, data.target:GetAbsOrigin())
				ParticleManager:SetParticleControlForward(nfx, 2, self:GetCaster():GetForwardVector())
				ParticleManager:ReleaseParticleIndex(nfx)
				EmitSoundOn(self.sound, self:GetCaster())

				if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi12") then
					data.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_tiny_tree_grab_lua_armor_reduction", {duration = 3})
				end
			end
		end
	end
end

function modifier_tiny_tree_grab_lua_tree:GetModifierPreAttack_BonusDamage()
	if self.base_damage and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi13") then
		return self.base_damage * 3
	end
	return 0
end

function modifier_tiny_tree_grab_lua_tree:GetActivityTranslationModifiers()
	return "tree"
end

function modifier_tiny_tree_grab_lua_tree:GetAttackSound()
	return ""--"Hero_Tiny_Prestige.Attack" -- not working
end

function modifier_tiny_tree_grab_lua_tree:GetModifierAttackRangeBonus()
	return self.attack_range
end

function modifier_tiny_tree_grab_lua_tree:GetModifierBonusStats_Strength()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi6") ~= nil and self:GetAbility():GetLevel() * 100 or 0
end

function modifier_tiny_tree_grab_lua_tree:GetModifierBonusStats_Agility()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi6") ~= nil and self:GetAbility():GetLevel() * 100 or 0
end

function modifier_tiny_tree_grab_lua_tree:GetModifierBonusStats_Intellect()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi6") ~= nil and self:GetAbility():GetLevel() * 100 or 0
end

function modifier_tiny_tree_grab_lua_tree:GetModifierPreAttack_CriticalStrike()
	if RollPercentage(15) and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi11") then
		return 100 + (PlayerResource:GetLastHits(self:GetCaster():GetPlayerOwnerID()) * 0.8)
	end
end

function modifier_tiny_tree_grab_lua_tree:GetModifierBaseAttack_BonusDamage()
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi12") then
		return (self:GetParent():GetIntellect(true) + self:GetParent():GetStrength() + self:GetParent():GetAgility()) * 0.3 + self.bonus_damage
	end
	return self.bonus_damage
end

modifier_tiny_tree_grab_lua_armor_reduction = class({})
--Classifications template
function modifier_tiny_tree_grab_lua_armor_reduction:IsHidden()
	return false
end

function modifier_tiny_tree_grab_lua_armor_reduction:IsDebuff()
	return true
end

function modifier_tiny_tree_grab_lua_armor_reduction:IsPurgable()
	return false
end

function modifier_tiny_tree_grab_lua_armor_reduction:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_tree_grab_lua_armor_reduction:IsStunDebuff()
	return false
end

function modifier_tiny_tree_grab_lua_armor_reduction:RemoveOnDeath()
	return true
end

function modifier_tiny_tree_grab_lua_armor_reduction:DestroyOnExpire()
	return true
end

function modifier_tiny_tree_grab_lua_armor_reduction:OnCreated()
	local abi = self:GetCaster():FindAbilityByName("tiny_grow_lua")
	if abi then
		self.armor_reduction = abi:GetLevel() * 20 * -1
	end
end

function modifier_tiny_tree_grab_lua_armor_reduction:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_tiny_tree_grab_lua_armor_reduction:GetModifierPhysicalArmorBonus()
	return self.armor_reduction
end