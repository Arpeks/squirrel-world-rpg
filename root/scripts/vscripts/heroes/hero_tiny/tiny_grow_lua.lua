LinkLuaModifier( "modifier_tiny_grow_lua", "heroes/hero_tiny/tiny_grow_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_tiny_grow_lua_aura", "heroes/hero_tiny/tiny_grow_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_tiny_prestige_check", "heroes/hero_tiny/tiny_grow_lua.lua", LUA_MODIFIER_MOTION_NONE )

tiny_grow_lua = class({})

function tiny_grow_lua:GetAbilityTextureName()
    if self:GetCaster():HasModifier("modifier_tiny_prestige_check") then
        return "tiny/ti9_immortal/tiny_grow_prestige"
    end
    return "tiny_grow"
end

function tiny_grow_lua:Spawn()
	if not IsServer() then
		return
	end
	Timers:CreateTimer(0.1,function()
		self.death = "particles/units/heroes/hero_tiny/tiny01_death.vpcf"
		self:GetCaster().tree_linear_effect = "particles/units/heroes/hero_tiny/tiny_tree_linear_proj.vpcf"
		self:GetCaster().tree_tracking_effect = "particles/units/heroes/hero_tiny/tiny_tree_proj.vpcf"
		if string.find( self:GetCaster():GetModelName(), "prestige") then
			self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_tiny_prestige_check", {})
			self.death = "particles/econ/items/tiny/tiny_prestige/tiny_prestige_lvl1_death.vpcf"
			self:GetCaster().tree_linear_effect = "particles/econ/items/tiny/tiny_prestige/tiny_prestige_tree_linear_proj.vpcf"
			self:GetCaster().tree_tracking_effect = "particles/econ/items/tiny/tiny_prestige/tiny_prestige_tree_proj.vpcf"
		end
		self.model_original = self:GetCaster():GetModelName()
	end)
end

function tiny_grow_lua:GetIntrinsicModifierName()
	return "modifier_tiny_grow_lua"
end

function tiny_grow_lua:OnUpgrade()
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_tiny/tiny_transform_lvl"..math.min(math.floor(self:GetLevel() / 2 + 1), 4)..".vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
	ParticleManager:ReleaseParticleIndex(pcf)
	EmitSoundOn("Tiny.Grow", self:GetCaster())
end

function tiny_grow_lua:OnOwnerDied()

	local pfx_name = string.gsub(self.death, "lvl1", "lvl"..math.max(self:GetLevel(),1))

	local death_pfx = ParticleManager:CreateParticle(pfx_name, PATTACH_CUSTOMORIGIN, nil)
	ParticleManager:SetParticleControl(death_pfx, 0, self:GetCaster():GetAbsOrigin())
	ParticleManager:SetParticleControlForward(death_pfx, 0, self:GetCaster():GetForwardVector())
	ParticleManager:ReleaseParticleIndex(death_pfx)

	EmitSoundOn(string.gsub("Hero_Tiny.Death_01", "01", "0"..math.min(math.floor(self:GetLevel() / 2 + 1), 4)), self:GetCaster())
end

modifier_tiny_grow_lua = class({})
--Classifications template
function modifier_tiny_grow_lua:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_tiny_grow_lua:IsDebuff()
	return false
end

function modifier_tiny_grow_lua:IsPurgable()
	return false
end

function modifier_tiny_grow_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_grow_lua:IsStunDebuff()
	return false
end

function modifier_tiny_grow_lua:RemoveOnDeath()
	return false
end

function modifier_tiny_grow_lua:DestroyOnExpire()
	return true
end

function modifier_tiny_grow_lua:OnCreated()
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.attack_speed_reduction = self:GetAbility():GetSpecialValueFor("attack_speed_reduction")
	self.caster = self:GetCaster()
	if not IsServer() then
		return
	end
	self.primary_attribute = self.caster:GetPrimaryAttribute()
	self.model = self:GetParent():GetModelName()
	self:SetHasCustomTransmitterData( true )
end

function modifier_tiny_grow_lua:OnRefresh()
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.attack_speed_reduction = self:GetAbility():GetSpecialValueFor("attack_speed_reduction")
	if not IsServer() then
		return
	end
	self.primary_attribute = self.caster:GetPrimaryAttribute()
	local model_level = math.min(math.floor(self:GetAbility():GetLevel() / 2 + 1), 4)
	if string.find(self.model, "ancient_tresure_keeper_tiny") then
		if model_level == 2 then
			self.model = "models/items/tiny/ancient_tresure_keeper_small/ancient_tresure_keeper_small.vmdl"
		elseif model_level == 3 then
			self.model = "models/items/tiny/ancient_tresure_keeper_medium/ancient_tresure_keeper_medium.vmdl"
		elseif model_level == 4 then
			self.model = "models/items/tiny/ancient_tresure_keeper_large/ancient_tresure_keeper_large.vmdl"
		end
	elseif string.find(self:GetAbility().model_original, "scarlet_quarry") then
		if model_level == 1 then
			self.model = self.model_original
		else
			self.model = string.gsub(self:GetAbility().model_original, "scarlet_quarry01", "scarlet_quarry_0" .. model_level)
		end
	else
		self.model = string.gsub(self:GetAbility().model_original, "01", "0" .. model_level)
	end
	self:SendBuffRefreshToClients()
end

function modifier_tiny_grow_lua:AddCustomTransmitterData()
	return {
		model = self.model,
		primary_attribute = self.primary_attribute,
	}
end

function modifier_tiny_grow_lua:HandleCustomTransmitterData( data )
	self.model = data.model
	self.primary_attribute = data.primary_attribute
end
-- 
function modifier_tiny_grow_lua:DeclareFunctions()
	local t = {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_PERCENTAGE,
		MODIFIER_PROPERTY_MODEL_CHANGE,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE,
		-- MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
	}
	return t 
end

function modifier_tiny_grow_lua:CustomOnDeath(params)
	if params.unit:GetTeamNumber() ~= DOTA_TEAM_BADGUYS then
        return false
    end
    if self.caster == params.attacker and RandomInt(1, 100) <= 20 and self.caster:FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str8") then
        self:IncrementStackCount()
		self.caster:CalculateStatBonus(true)
    end
end

function modifier_tiny_grow_lua:GetModifierBonusStats_Strength()
    return self.primary_attribute ~= DOTA_ATTRIBUTE_STRENGTH and self:GetStackCount() or 0
end

function modifier_tiny_grow_lua:GetModifierBonusStats_Agility()
    return self.primary_attribute ~= DOTA_ATTRIBUTE_AGILITY and self:GetStackCount() or 0
end

function modifier_tiny_grow_lua:GetModifierBonusStats_Intellect()
    return self.primary_attribute ~= DOTA_ATTRIBUTE_INTELLECT and self:GetStackCount() or 0
end

function modifier_tiny_grow_lua:GetModifierPhysicalArmorBonus()
	local talent = 0
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str7") then
		talent = self:GetParent():GetStrength() / 10
	end
	if self:GetParent():HasModifier("modifier_tiny_tree_grab_lua_tree") and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi9") then
		return (self.bonus_armor + talent) * 2
	end
	return self.bonus_armor + talent
end

function modifier_tiny_grow_lua:GetModifierPreAttack_BonusDamage()
	local talent = 0
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str10") then
		talent = self:GetParent():GetStrength()
	end
	local talent3 = 0
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str11") then
		if self.primary_attribute == DOTA_ATTRIBUTE_STRENGTH then
			talent3 = self:GetParent():GetIntellect(true) + self:GetParent():GetAgility()
		elseif self.primary_attribute == DOTA_ATTRIBUTE_AGILITY then
			talent3 = self:GetParent():GetIntellect(true) + self:GetParent():GetStrength()
		elseif self.primary_attribute == DOTA_ATTRIBUTE_INTELLECT then
			talent3 = self:GetParent():GetStrength() + self:GetParent():GetAgility()
		end
	end
	if self:GetParent():HasModifier("modifier_tiny_tree_grab_lua_tree") and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi9") then
		return (self.bonus_damage + talent + talent3) * 2
	end
	return self.bonus_damage + talent + talent3
end

function modifier_tiny_grow_lua:GetModifierAttackSpeedPercentage()
	if self:GetCaster() ~= self:GetParent() then
		return math.max(0, self.attack_speed_reduction)
	end
	if self:GetParent():HasModifier("modifier_tiny_tree_grab_lua_tree") and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi9") then
		return math.max(self.attack_speed_reduction * 2, self.attack_speed_reduction)
	end
	return self.attack_speed_reduction
end

function modifier_tiny_grow_lua:GetModifierModelChange()
	return self.model
end

function modifier_tiny_grow_lua:CustomOnTakeDamage( keys )
	if keys.attacker == self:GetParent() then
		if not self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str6") then
			return 
		end
		if not keys.unit:IsBuilding() and not keys.unit:IsOther() and keys.unit:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
			if keys.damage_category == DOTA_DAMAGE_CATEGORY_SPELL and keys.inflictor and bit.band(keys.damage_flags, DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL) ~= DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL then			
				self.lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker)
				ParticleManager:SetParticleControl(self.lifesteal_pfx, 0, keys.attacker:GetAbsOrigin())
				ParticleManager:ReleaseParticleIndex(self.lifesteal_pfx)
				keys.attacker:HealWithParams(math.min(math.abs(keys.damage) * self:GetAbility():GetLevel() * 2, 2^30), self:GetAbility(), true, true, keys.attacker, true)
			end
		end
	end
end

function modifier_tiny_grow_lua:IsAura()
	return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str12") ~= nil
end

function modifier_tiny_grow_lua:GetModifierAura()
	return "modifier_tiny_grow_lua_aura"
end

function modifier_tiny_grow_lua:GetAuraRadius()
	return 999999
end

function modifier_tiny_grow_lua:GetAuraDuration()
	return 0.5
end

function modifier_tiny_grow_lua:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_FRIENDLY
end

function modifier_tiny_grow_lua:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO
end

function modifier_tiny_grow_lua:GetAuraSearchFlags()
	return 0
end

function modifier_tiny_grow_lua:GetAuraEntityReject(hEntity)
	if hEntity == self:GetCaster() then
		return true
	end
	return false
end

modifier_tiny_prestige_check = class({})
--Classifications template
function modifier_tiny_prestige_check:IsHidden()
	return true
end

function modifier_tiny_prestige_check:IsDebuff()
	return false
end

function modifier_tiny_prestige_check:IsPurgable()
	return false
end

function modifier_tiny_prestige_check:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_prestige_check:IsStunDebuff()
	return false
end

function modifier_tiny_prestige_check:RemoveOnDeath()
	return false
end

function modifier_tiny_prestige_check:DestroyOnExpire()
	return false
end

modifier_tiny_grow_lua_aura = class({})
--Classifications template
function modifier_tiny_grow_lua_aura:IsHidden()
	return false
end

function modifier_tiny_grow_lua_aura:IsDebuff()
	return false
end

function modifier_tiny_grow_lua_aura:IsPurgable()
	return false
end

function modifier_tiny_grow_lua_aura:OnCreated()
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.attack_speed_reduction = self:GetAbility():GetSpecialValueFor("attack_speed_reduction")
	self.special_bonus_unique_npc_dota_hero_tiny_str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str13")
	if not IsServer() then
		return
	end
	self.primary_attribute = self:GetParent():GetPrimaryAttribute()
	self:SetHasCustomTransmitterData( true )
end

function modifier_tiny_grow_lua_aura:OnRefresh()
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.attack_speed_reduction = self:GetAbility():GetSpecialValueFor("attack_speed_reduction")
	self.special_bonus_unique_npc_dota_hero_tiny_str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str13")
	if not IsServer() then
		return
	end
	self.primary_attribute = self:GetParent():GetPrimaryAttribute()
	self:SendBuffRefreshToClients()
end

function modifier_tiny_grow_lua_aura:AddCustomTransmitterData()
	return {
		primary_attribute = self.primary_attribute,
	}
end

function modifier_tiny_grow_lua_aura:HandleCustomTransmitterData( data )
	self.primary_attribute = data.primary_attribute
end

function modifier_tiny_grow_lua_aura:DeclareFunctions()
	local t = {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_TOOLTIP,
		MODIFIER_PROPERTY_TOOLTIP2,
	}
	return t 
end

function modifier_tiny_grow_lua_aura:OnTooltip()
	local talent = 0
	if self.special_bonus_unique_npc_dota_hero_tiny_str13 then
		local Strength = self:GetParent():GetStrength()
		local Agility = self:GetParent():GetAgility()
		local Intellect = self:GetParent():GetIntellect(true)
		if Strength >= Agility and Strength >= Intellect then
			talent = Strength
		elseif Agility >= Strength and Agility >= Intellect then
			talent = Agility
		elseif Intellect >= Strength and Intellect >= Agility then
			talent = Intellect
		end
	end
	return (self.bonus_damage + talent)
end

function modifier_tiny_grow_lua_aura:OnTooltip2()
	return (self.bonus_armor)
end

function modifier_tiny_grow_lua_aura:GetModifierPhysicalArmorBonus()
	return self:OnTooltip2()
end

function modifier_tiny_grow_lua_aura:GetModifierPreAttack_BonusDamage()
	return self:OnTooltip()
end

--------------------------------------------------------------------------------
-- Aura Effects