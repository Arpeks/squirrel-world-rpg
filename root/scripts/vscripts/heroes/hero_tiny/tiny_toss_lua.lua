-- LinkLuaModifier( "modifier_tiny_toss_lua", "heroes/hero_tiny/tiny_toss_lua.lua", LUA_MODIFIER_MOTION_NONE )

tiny_toss_lua = class({})

function tiny_toss_lua:GetAbilityTextureName()
    if self:GetCaster():HasModifier("modifier_tiny_prestige_check") then
        return "tiny/ti9_immortal/tiny_toss_immortal"
    end
    return "tiny_toss"
end

-- function tiny_toss_lua:GetIntrinsicModifierName()
-- 	return "modifier_tiny_toss_lua"
-- end

function tiny_toss_lua:GetAOERadius()
	return self:GetSpecialValueFor( "radius" )
end

function tiny_toss_lua:CastFilterResultTarget( hTarget )
	if self:GetCaster() == hTarget then
		return UF_FAIL_CUSTOM
	end
	return UF_SUCCESS
end

function tiny_toss_lua:GetCustomCastErrorTarget( hTarget )
	if self:GetCaster() == hTarget then
		return "#dota_hud_error_cant_cast_on_self"
	end

	return "#dota_hud_error_nothing_to_toss"
end

function tiny_toss_lua:OnSpellStart()
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),self:GetCaster():GetOrigin(),nil,self:GetSpecialValueFor( "grab_radius" ),DOTA_UNIT_TARGET_TEAM_BOTH,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS + DOTA_UNIT_TARGET_FLAG_NO_INVIS,FIND_CLOSEST,false)

	local target
	for _,unit in pairs(units) do
		if (unit~=self:GetCaster()) and (not unit:FindModifierByName( 'modifier_tiny_toss_lua' )) then
			target = unit
			break
		end
	end
	local arc = target:AddNewModifier(self:GetCaster(),self,"modifier_generic_arc_lua",{
		duration = self:GetSpecialValueFor( "duration" ),
		height = 700,
		start_offset = GetGroundHeight(target:GetAbsOrigin(), target),
		end_offset = GetGroundHeight(self:GetCursorTarget():GetAbsOrigin(), target),
		target = self:GetCursorTarget():entindex(),
		fix_duration = true,
		isStun = true,
		activity = ACT_DOTA_FLAIL,
	})
	arc.IsToss = true
	local radius = self:GetSpecialValueFor( "radius" )
	local abi = self:GetCaster():FindAbilityByName("tiny_grow_lua")
	local damage = self:GetSpecialValueFor( "toss_damage" ) 

	if abi and abi:GetLevel() > 0 then
		damage = damage + abi:GetSpecialValueFor("toss_bonus_damage")
	end

	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_tiny/tiny_toss_blur.vpcf", PATTACH_POINT_FOLLOW, target)
	arc:AddParticle(pcf, false, false, -1, false, false)
	EmitSoundOn("Ability.TossThrow",self:GetCaster())
	EmitSoundOn("Hero_Tiny.Toss.Target",target)

	arc:SetEndCallback(function( interrupted )
		if interrupted then return end

		local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),target:GetOrigin(),nil,radius,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)

		local damageTable = {
			attacker = self:GetCaster(),
			damage = damage,
			damage_type = self:GetAbilityDamageType(),
			ability = self
		}

		for _,enemy in pairs(enemies) do
			damageTable.victim = enemy
			if enemy == target then
				damageTable.damage = 2 * damage
			else
				damageTable.damage = damage
			end
			ApplyDamageRDA(damageTable)
		end

		GridNav:DestroyTreesAroundPoint( target:GetOrigin(), 250, false )

		EmitSoundOn( "Ability.TossImpact", target )
	end)

end

function tiny_toss_lua:OnAbilityPhaseStart()
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),self:GetCaster():GetOrigin(),nil,self:GetSpecialValueFor( "grab_radius" ),DOTA_UNIT_TARGET_TEAM_BOTH,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS + DOTA_UNIT_TARGET_FLAG_NO_INVIS,FIND_CLOSEST,false)
	local target
	for _,unit in pairs(units) do
		if (unit~=self:GetCaster()) and (not unit:FindModifierByName( 'modifier_generic_arc_lua' )) then
			target = unit
			break
		end
	end
	if target == nil then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
		CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_toss" })
	end

	return target ~= nil
end
