"DOTAAbilities"
{
	"special_bonus_unique_npc_dota_hero_tiny_str6"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_tiny_str9"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_str7"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_str10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_str8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_str11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_tiny_agi6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi9"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi7"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_tiny_int6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_int9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_int7"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_int10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_int8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_int11"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}

	//last
	"special_bonus_unique_npc_dota_hero_tiny_str12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi12"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_tiny_int12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	//last
	"special_bonus_unique_npc_dota_hero_tiny_str13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_tiny_agi13"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_tiny_int13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
}