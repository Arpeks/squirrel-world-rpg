LinkLuaModifier( "modifier_tiny_avalanche_lua", "heroes/hero_tiny/tiny_avalanche_lua.lua", LUA_MODIFIER_MOTION_NONE )

tiny_avalanche_lua = class({})

function tiny_avalanche_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int7") then
        return DOTA_ABILITY_BEHAVIOR_PASSIVE
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int12") then
        return DOTA_ABILITY_BEHAVIOR_PASSIVE
    end
    return DOTA_ABILITY_BEHAVIOR_AOE + DOTA_ABILITY_BEHAVIOR_POINT
end

function tiny_avalanche_lua:GetAbilityTextureName()
    if self:GetCaster():HasModifier("modifier_tiny_prestige_check") then
        return "tiny/ti9_immortal/tiny_avalanche_immortal"
    end
    return "tiny_avalanche"
end

function tiny_avalanche_lua:GetAOERadius()
    return self:GetSpecialValueFor("radius")
end

function tiny_avalanche_lua:GetIntrinsicModifierName()
	return "modifier_tiny_avalanche_lua"
end

function tiny_avalanche_lua:OnSpellStart()
	self.direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()
	self.distance = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Length2D()
	
    ProjectileManager:CreateLinearProjectile({
        Ability = self,
        EffectName = "particles/units/heroes/hero_tiny/tiny_avalanche_projectile.vpcf",
        vSpawnOrigin = self:GetCaster():GetAbsOrigin(),
        fDistance = self.distance,
        fStartRadius = 0,
        fEndRadius = 0,
        Source = self:GetCaster(),
        bHasFrontalCone = false,
        bReplaceExisting = false,
        iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
        iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_NONE,
        iUnitTargetType = DOTA_UNIT_TARGET_ALL,
        fExpireTime = GameRules:GetGameTime() + 10.0,
        bDeleteOnHit = false,
        vVelocity = self.direction * self.distance,
        bProvidesVision = true,
        iVisionRadius = 200,
        iVisionTeamNumber = self:GetCaster():GetTeamNumber()
    })
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_agi10") then
        self:GetCaster().AvalancheActive = true
    end
    EmitSoundOnLocationWithCaster(self:GetCaster():GetAbsOrigin(), "Ability.Avalanche", self:GetCaster())
end

function tiny_avalanche_lua:OnProjectileHit_ExtraData(hTarget, vLocation, extradata)
    local caster = self:GetCaster()

    local duration = self:GetSpecialValueFor("stun_duration")
    local radius = self:GetSpecialValueFor("radius")

    local interval = self:GetSpecialValueFor("tick_interval")

    local damage = self:GetSpecialValueFor("avalanche_damage")
    
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_str9") then
        local abi = self:GetCaster():FindAbilityByName("tiny_grow_lua")
        if abi and abi:GetLevel() > 0 then
            damage = damage + abi:GetSpecialValueFor("toss_bonus_damage") * 3
        end
    end
    damage = damage / self:GetSpecialValueFor("tick_count") 
    for _,mod in pairs(caster:FindAllModifiersByName("modifier_generic_arc_lua")) do
        if mod.IsToss then
            damage = damage * 2
        end
    end
    local avalanche = ParticleManager:CreateParticle("particles/units/heroes/hero_tiny/tiny_avalanche.vpcf", PATTACH_CUSTOMORIGIN, nil)
    ParticleManager:SetParticleControl(avalanche, 0, vLocation + self.direction)
    ParticleManager:SetParticleControlOrientation(avalanche, 0, self.direction, self.direction, self:GetCaster():GetUpVector())
    ParticleManager:SetParticleControl(avalanche, 1, Vector(radius, 1, radius/2))

    local ticks = self:GetSpecialValueFor("tick_count")

    local special_bonus_unique_npc_dota_hero_tiny_int10 = caster:FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int10")
    Timers:CreateTimer(function()
		local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), vLocation, nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )
        for _,enemy in pairs(enemies) do
			local real_damage = ApplyDamageRDA({
				victim = enemy,
				attacker = self:GetCaster(),
				damage = damage,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = 0,
				ability = self
			})
			enemy:AddNewModifier(self:GetCaster(), self, "modifier_stunned", {duration = duration})
            if special_bonus_unique_npc_dota_hero_tiny_int10 then
                self:GetCaster():HealWithParams(real_damage*0.25, self, false, false, self:GetCaster(), true)
                lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
                ParticleManager:SetParticleControl(lifesteal_pfx, 0, self:GetCaster():GetAbsOrigin())
                ParticleManager:ReleaseParticleIndex(lifesteal_pfx)
            end
        end
        ticks = ticks - 1
        if ticks > 0 then
            return interval
        else
            ParticleManager:DestroyParticle(avalanche, false)
            ParticleManager:ReleaseParticleIndex(avalanche)
            self:GetCaster().AvalancheActive = false
        end
    end)
end

modifier_tiny_avalanche_lua = class({})
--Classifications template
function modifier_tiny_avalanche_lua:IsHidden()
	return true
end

function modifier_tiny_avalanche_lua:IsDebuff()
	return false
end

function modifier_tiny_avalanche_lua:IsPurgable()
	return false
end

function modifier_tiny_avalanche_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_tiny_avalanche_lua:IsStunDebuff()
	return false
end

function modifier_tiny_avalanche_lua:RemoveOnDeath()
	return false
end

function modifier_tiny_avalanche_lua:DestroyOnExpire()
	return true
end

function modifier_tiny_avalanche_lua:OnCreated()
	if not IsServer() then
		return
	end
    self:StartIntervalThink(self:GetAbility():GetSpecialValueFor("tick_interval"))
    self.primary_attribute = self:GetCaster():GetPrimaryAttribute()
	self:SetHasCustomTransmitterData( true )
end

function modifier_tiny_avalanche_lua:OnRefresh()
	if not IsServer() then
		return
	end
	self.primary_attribute = self:GetCaster():GetPrimaryAttribute()
	self:SendBuffRefreshToClients()
end

function modifier_tiny_avalanche_lua:AddCustomTransmitterData()
	return {
		primary_attribute = self.primary_attribute,
	}
end

function modifier_tiny_avalanche_lua:HandleCustomTransmitterData( data )
	self.primary_attribute = data.primary_attribute
end

function modifier_tiny_avalanche_lua:OnIntervalThink()
    self:SetStackCount(self:GetParent():GetBaseDamageMax())
    if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int12") then
        local damage = (self:GetAbility():GetSpecialValueFor("avalanche_damage") + self:GetCaster():GetIntellect(true)) / self:GetAbility():GetSpecialValueFor("tick_count")
		local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )
        for _,enemy in pairs(enemies) do
            ApplyDamageRDA({
                victim = enemy,
                attacker = self:GetCaster(),
                damage = damage,
                damage_type = DAMAGE_TYPE_MAGICAL,
                damage_flags = 0,
                ability = self:GetAbility()
            })
        end
        local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_sandking/sandking_epicenter.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
        ParticleManager:SetParticleControl( effect_cast, 0, self:GetParent():GetAbsOrigin())
        ParticleManager:SetParticleControl( effect_cast, 1, Vector(300,1,1))
        ParticleManager:ReleaseParticleIndex( effect_cast )
    end
end

function modifier_tiny_avalanche_lua:OnDestroy()

	if not IsServer() then
		return
	end

end

function modifier_tiny_avalanche_lua:DeclareFunctions()
	return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
	}
end

function modifier_tiny_avalanche_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "avalanche_damage" then
			return 1
		end
	end
	return 0
end

function modifier_tiny_avalanche_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "avalanche_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "avalanche_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int6") then
                -- value = value + self:GetStackCount()
                value = value * 2.0
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int11") then
                value = value + self:GetCaster():GetIntellect(true) * 0.8
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int8") and self:GetCaster():HasModifier("modifier_tiny_tree_grab_lua_tree") then
                value = value * 1.2
            end
            return value
		end
	end
	return 0
end

function modifier_tiny_avalanche_lua:GetModifierBonusStats_Strength()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int6") then
        return self:GetAbility():GetLevelSpecialValueNoOverride( "avalanche_damage", self:GetAbility():GetLevel() )
    end
    return 0
end

function modifier_tiny_avalanche_lua:GetModifierBonusStats_Agility()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int6") then
        return self:GetAbility():GetLevelSpecialValueNoOverride( "avalanche_damage", self:GetAbility():GetLevel() )
    end
    return 0
end

function modifier_tiny_avalanche_lua:GetModifierBonusStats_Intellect()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int6") then
        return self:GetAbility():GetLevelSpecialValueNoOverride( "avalanche_damage", self:GetAbility():GetLevel() )
    end
    return 0
end

function modifier_tiny_avalanche_lua:GetModifierAttackSpeedBonus_Constant()
    return self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int7") ~= nil and self:GetAbility():GetLevel() * 40 or 0
end

function modifier_tiny_avalanche_lua:GetModifierSpellAmplify_Percentage()
    local value = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_tiny_int13") then
        if self.primary_attribute == DOTA_ATTRIBUTE_STRENGTH then
            value = self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility()
        elseif self.primary_attribute == DOTA_ATTRIBUTE_AGILITY then
            value = self:GetCaster():GetIntellect(true) + self:GetCaster():GetStrength()
        elseif self.primary_attribute == DOTA_ATTRIBUTE_INTELLECT then
            value = self:GetCaster():GetStrength() + self:GetCaster():GetAgility()
        end
        value = value * 0.5
    end
    return value
end