ember_spirit_sleight_of_fist_lua = class({})

function ember_spirit_sleight_of_fist_lua:Spawn()
	if IsServer() then
		self.original_model_name = self:GetCaster():GetModelName()
	end
end

function ember_spirit_sleight_of_fist_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 20 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function ember_spirit_sleight_of_fist_lua:GetBehavior()
	if self:GetCaster():HasModifier("modifier_fire_remnant_motion") then
		return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi9") then
		return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE + DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE + DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES
end

function ember_spirit_sleight_of_fist_lua:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function ember_spirit_sleight_of_fist_lua:OnSpellStart()
	self.cursor = self:GetCursorPosition()

	local ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")

	if #ability.remenants > 0 and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi13") then
		for _,rem in pairs(ability.remenants) do
            Timers:CreateTimer(RandomFloat(0.2, 0.6),function()
				self:CastFistRemenant(rem)
			end)
		end
	end

	local special_bonus_unique_npc_dota_hero_ember_spirit_str6 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str6")
	self:GetCaster():EmitSound("Hero_EmberSpirit.SleightOfFist.Cast")
    local cast_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_cast.vpcf", PATTACH_WORLDORIGIN, self:GetCaster())
    ParticleManager:SetParticleControl(cast_pfx, 0, self:GetCursorPosition())
    ParticleManager:SetParticleControl(cast_pfx, 1, Vector(self:GetSpecialValueFor("radius"), 0, 0))
    ParticleManager:ReleaseParticleIndex(cast_pfx)

	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCursorPosition(), nil, self:GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_CLOSEST, false)
	if #units > 0 then	
		local len
		local attack_interval = self:GetSpecialValueFor("attack_interval")
		local t = {}
		local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_caster", {})
		local max_shuffles = 1
		for _,unit in pairs(units) do
			if special_bonus_unique_npc_dota_hero_ember_spirit_str6 and unit:IsBoss() then
				max_shuffles = 3
			end
			for i=1,max_shuffles do
				unit:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_marker", {})
			end
			table.insert(t, unit)
		end
		mod.targets_list = t
		mod.targets = table.shuffle(t)
		mod.max_shuffles = max_shuffles
		mod:OnIntervalThink()
		mod:StartIntervalThink(attack_interval)
		if special_bonus_unique_npc_dota_hero_ember_spirit_str6 and #units == 1 then
			self:EndCooldown()
			self:StartCooldown(2)
		end
	end
end

function ember_spirit_sleight_of_fist_lua:CastFistRemenant(remenant)
	if not remenant.active then
		return
	end
	local direction = (self.cursor - remenant:GetAbsOrigin()):Normalized()

	local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_fire_remnant.vpcf", PATTACH_CUSTOMORIGIN, nil)
    ParticleManager:SetParticleControl(iParticleID, 0, remenant:GetAbsOrigin())
    ParticleManager:SetParticleFoWProperties(iParticleID, 0, -1, 500)
    ParticleManager:SetParticleControlEnt(iParticleID, 1, self:GetCaster(), PATTACH_CUSTOMORIGIN_FOLLOW, nil, remenant:GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(iParticleID, 2, Vector(4, 0, 0))
    remenant:AddNoDraw()
    Timers:CreateTimer(0.3,function()
        ParticleManager:DestroyParticle(iParticleID, true)
        ParticleManager:ReleaseParticleIndex(iParticleID)
        remenant:RemoveNoDraw()
    end)

	local len = math.min((self.cursor - remenant:GetAbsOrigin()):Length2D(), self:GetCastRange(self.cursor, nil))
	local cast_position = direction * len + remenant:GetAbsOrigin()
	remenant:EmitSound("Hero_EmberSpirit.SleightOfFist.Cast")
    local cast_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_cast.vpcf", PATTACH_WORLDORIGIN, remenant)
    ParticleManager:SetParticleControl(cast_pfx, 0, cast_position)
    ParticleManager:SetParticleControl(cast_pfx, 1, Vector(self:GetSpecialValueFor("radius"), 0, 0))
    ParticleManager:ReleaseParticleIndex(cast_pfx)

	local special_bonus_unique_npc_dota_hero_ember_spirit_str6 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str6")

	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), cast_position, nil, self:GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)
	if #units > 0 then	
		local target_near
		local len
		local attack_interval = self:GetSpecialValueFor("attack_interval")
		local t = {}
		local mod = remenant:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_remenant", {pos_x = remenant:GetAbsOrigin().x, pos_y = remenant:GetAbsOrigin().y, pos_z = remenant:GetAbsOrigin().z})
		local max_shuffles = 1
		for _,unit in pairs(units) do
			if special_bonus_unique_npc_dota_hero_ember_spirit_str6 and unit:IsBoss() then
				max_shuffles = 3
			end
			for i=1,max_shuffles do
				unit:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_marker", {})
			end
			table.insert(t, unit)
		end
		for _,unit in pairs(units) do
			for i=1,max_shuffles do
				unit:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_marker", {})
			end
			table.insert(t, unit)
		end
		mod.targets_list = t
		mod.targets = table.shuffle(t)
		mod.max_shuffles = max_shuffles
		
		mod.iParticleID = iParticleID
		mod:OnIntervalThink()
		mod:StartIntervalThink(attack_interval)
	end
end

function ember_spirit_sleight_of_fist_lua:GetIntrinsicModifierName()
	return "modifier_ember_spirit_sleight_of_fist_lua_intr"
end

LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_intr", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_marker_out", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_marker", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_agi13", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)

modifier_ember_spirit_sleight_of_fist_lua_marker = class({})
--Classifications template
function modifier_ember_spirit_sleight_of_fist_lua_marker:IsHidden()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:IsDebuff()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:IsPurgable()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_ember_spirit_sleight_of_fist_lua_marker:IsStunDebuff()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:RemoveOnDeath()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:DestroyOnExpire()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:GetEffectName()
	return "particles/units/heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_targetted_marker.vpcf"
end

function modifier_ember_spirit_sleight_of_fist_lua_marker:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end

LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_caster", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)

modifier_ember_spirit_sleight_of_fist_lua_caster = class({})
--Classifications template
function modifier_ember_spirit_sleight_of_fist_lua_caster:IsHidden()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:IsDebuff()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:IsPurgable()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_ember_spirit_sleight_of_fist_lua_caster:IsStunDebuff()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:RemoveOnDeath()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:DestroyOnExpire()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:OnCreated(data)
	self.damage_outgoing = 0
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi6") then
		self.damage_outgoing = self.damage_outgoing + 80
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi12") then
		self.damage_outgoing = self.damage_outgoing + 25 + 10 * math.min(120, self:GetCaster():GetLevel())
	end
	self.bonus_hero_damage = self:GetAbility():GetSpecialValueFor("bonus_hero_damage")

	if IsServer() then
		self.sleight_caster_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_caster.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
		ParticleManager:SetParticleControl(self.sleight_caster_particle, 0, self:GetCaster():GetAbsOrigin())
		ParticleManager:SetParticleControlForward(self.sleight_caster_particle, 1, self:GetCaster():GetForwardVector())
		
		self:GetParent():AddNoDraw()
		self:GetAbility():SetActivated(false)

		self.shuffle_count = 1
		self.original_position = self:GetCaster():GetAbsOrigin()
		self.last_position = self:GetCaster():GetAbsOrigin()

		self.special_bonus_unique_npc_dota_hero_ember_spirit_int9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int9")
		self.modifier_ember_spirit_sleight_of_fist_lua_intr = self:GetCaster():FindModifierByName("modifier_ember_spirit_sleight_of_fist_lua_intr")

		
	end
	if data.agi9 then
		self.damage_outgoing = (self.damage_outgoing + 100) * 0.5 - 100
		self.bonus_hero_damage = self.bonus_hero_damage * 0.5
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:OnDestroy()
	if IsServer() then
		ParticleManager:DestroyParticle(self.sleight_caster_particle, false)
		ParticleManager:ReleaseParticleIndex(self.sleight_caster_particle)
	
		self:GetParent():RemoveNoDraw()
		self:GetAbility():SetActivated(true)
		
		local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_trail.vpcf", PATTACH_CUSTOMORIGIN, nil)
		ParticleManager:SetParticleControl(trail_pfx, 0, self.last_position)
		ParticleManager:SetParticleControl(trail_pfx, 1, self.original_position)
		ParticleManager:ReleaseParticleIndex(trail_pfx)

		FindClearSpaceForUnit(self:GetParent(), self.original_position, true)
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:OnIntervalThink()
	--находим случайного живого врага для удара
	local target,key = table.random_with_condition(self.targets, function(tbl, k, current_target)
		if current_target and not current_target:IsNull() and current_target:IsAlive() and not current_target:IsAttackImmune() then
			return true
		end
		return false
	end)
	--если нет врага для удара или мы уже ударили всех врагов
	while not target or #self.targets == 0 do
		self.shuffle_count = self.shuffle_count + 1
		if self.shuffle_count > self.max_shuffles then
			self:Destroy()
			return
		end
		self.targets = table.shuffle(self.targets_list)
		target,key = table.random_with_condition(self.targets, function(tbl, k, current_target)
			if current_target and not current_target:IsNull() and current_target:IsAlive() and not current_target:IsAttackImmune() then
				return true
			end
			return false
		end)
	end

	self:GetParent():EmitSound("Hero_EmberSpirit.SleightOfFist.Damage")
	local slash_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_tgt.vpcf", PATTACH_ABSORIGIN_FOLLOW, target)
	ParticleManager:SetParticleControl(slash_pfx, 0, target:GetAbsOrigin())
	ParticleManager:ReleaseParticleIndex(slash_pfx)

	local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_trail.vpcf", PATTACH_CUSTOMORIGIN, nil)
	ParticleManager:SetParticleControl(trail_pfx, 0, target:GetAbsOrigin())
	ParticleManager:SetParticleControl(trail_pfx, 1, self.last_position)
	ParticleManager:ReleaseParticleIndex(trail_pfx)

	self.last_position = target:GetAbsOrigin() + RandomVector(64)
	self:GetParent():SetAbsOrigin(self.last_position)
	if self.special_bonus_unique_npc_dota_hero_ember_spirit_int9 then
		target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ember_spirit_sleight_of_fist_lua_marker_out", {duration = 1})
	end
	target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ember_spirit_sleight_of_fist_lua_agi13", {duration = 3})
	self:GetParent():PerformAttack(target, true, true, true, false, false, false, false)
	table.remove(self.targets, key)
	if target and not target:IsNull() and target.IsAlive and target:IsAlive() then
		target:RemoveModifierByName("modifier_ember_spirit_sleight_of_fist_lua_agi13")
		target:RemoveModifierByName("modifier_ember_spirit_sleight_of_fist_lua_marker")
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_ROOTED] = true,
		[MODIFIER_STATE_UNSELECTABLE] = true,
		[MODIFIER_STATE_STUNNED] = true
	}
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_IGNORE_CAST_ANGLE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:GetModifierPreAttack_BonusDamage(keys)
	return self.bonus_hero_damage
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:GetModifierIgnoreCastAngle()
	return 1
end

function modifier_ember_spirit_sleight_of_fist_lua_caster:GetModifierDamageOutgoing_Percentage()
	return self.damage_outgoing
end















LinkLuaModifier("modifier_ember_spirit_sleight_of_fist_lua_remenant", "heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_lua.lua", LUA_MODIFIER_MOTION_NONE)

modifier_ember_spirit_sleight_of_fist_lua_remenant = class({})
--Classifications template
function modifier_ember_spirit_sleight_of_fist_lua_remenant:IsHidden()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:IsDebuff()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:IsPurgable()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_ember_spirit_sleight_of_fist_lua_remenant:IsStunDebuff()
	return false
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:RemoveOnDeath()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:DestroyOnExpire()
	return true
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:OnCreated(data)
	if IsServer() then
		self.sleight_caster_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleight_of_fist_caster.vpcf", PATTACH_WORLDORIGIN, self:GetCaster())
		ParticleManager:SetParticleControl(self.sleight_caster_particle, 0, self:GetParent():GetAbsOrigin())
		ParticleManager:SetParticleControlForward(self.sleight_caster_particle, 1, self:GetParent():GetForwardVector())
		
		-- self:GetParent():AddNoDraw()
		-- self:GetAbility():SetActivated(false)

		self.shuffle_count = 1
		self.original_position = Vector(data.pos_x, data.pos_y, data.pos_z)
		self.last_position = Vector(data.pos_x, data.pos_y, data.pos_z)

		self.special_bonus_unique_npc_dota_hero_ember_spirit_int9 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int9")
		self.modifier_ember_spirit_sleight_of_fist_lua_intr = self:GetCaster():FindModifierByName("modifier_ember_spirit_sleight_of_fist_lua_intr")
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:OnDestroy()
	if IsServer() then
		ParticleManager:DestroyParticle(self.sleight_caster_particle, false)
		ParticleManager:ReleaseParticleIndex(self.sleight_caster_particle)
	
		-- self:GetParent():RemoveNoDraw()
		-- self:GetAbility():SetActivated(true)
		
		local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_trail.vpcf", PATTACH_CUSTOMORIGIN, nil)
		ParticleManager:SetParticleControl(trail_pfx, 0, self.last_position)
		ParticleManager:SetParticleControl(trail_pfx, 1, self.original_position)
		ParticleManager:ReleaseParticleIndex(trail_pfx)

		FindClearSpaceForUnit(self:GetParent(), self.original_position, true)
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_remenant:OnIntervalThink()

	--находим случайного живого врага для удара
	local target,key = table.random_with_condition(self.targets, function(tbl, k, current_target)
		if current_target and not current_target:IsNull() and current_target:IsAlive() and not current_target:IsAttackImmune() then
			return true
		end
		return false
	end)
	--если нет врага для удара или мы уже ударили всех врагов
	while not target or #self.targets == 0 do
		self.shuffle_count = self.shuffle_count + 1
		-- print(self.shuffle_count)
		if self.shuffle_count > self.max_shuffles then
			self:Destroy()
			return
		end
		self.targets = table.shuffle(self.targets_list)
		target,key = table.random_with_condition(self.targets, function(tbl, k, current_target)
			if current_target and not current_target:IsNull() and current_target:IsAlive() and not current_target:IsAttackImmune() then
				return true
			end
			return false
		end)
	end

	self:GetParent():EmitSound("Hero_EmberSpirit.SleightOfFist.Damage")
	local slash_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_tgt.vpcf", PATTACH_ABSORIGIN_FOLLOW, target)
	ParticleManager:SetParticleControl(slash_pfx, 0, target:GetAbsOrigin())
	ParticleManager:ReleaseParticleIndex(slash_pfx)

	local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_sleightoffist_trail.vpcf", PATTACH_CUSTOMORIGIN, nil)
	ParticleManager:SetParticleControl(trail_pfx, 0, target:GetAbsOrigin())
	ParticleManager:SetParticleControl(trail_pfx, 1, self.last_position)
	ParticleManager:ReleaseParticleIndex(trail_pfx)

	self.last_position = target:GetAbsOrigin() + RandomVector(64)
	self:GetParent():SetAbsOrigin(self.last_position)
	self:GetCaster():PerformAttack(target, true, true, true, false, false, false, false)
	table.remove(self.targets, key)
	target:RemoveModifierByName("modifier_ember_spirit_sleight_of_fist_lua_marker")
end

modifier_ember_spirit_sleight_of_fist_lua_marker_out = class({})
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:IsHidden() return false end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:IsDebuff() return true end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:IsPurgable() return false end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:IsPurgeException() return false end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:IsStunDebuff() return false end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:RemoveOnDeath() return true end
function modifier_ember_spirit_sleight_of_fist_lua_marker_out:DestroyOnExpire() return true end

function modifier_ember_spirit_sleight_of_fist_lua_marker_out:OnDestroy()
	if not IsServer() then return end
	local abi = self:GetAbility()
	if not self:GetParent():IsAlive() then
		local current_cooldown = abi:GetCooldownTimeRemaining()
		if current_cooldown > 0 then
			abi:EndCooldown()
			abi:StartCooldown(current_cooldown - 1)
		end
	end
end












modifier_ember_spirit_sleight_of_fist_lua_intr = class({})
function modifier_ember_spirit_sleight_of_fist_lua_intr:IsHidden() return self:GetStackCount() == 0 end
function modifier_ember_spirit_sleight_of_fist_lua_intr:IsDebuff() return false end
function modifier_ember_spirit_sleight_of_fist_lua_intr:IsPurgable() return false end
function modifier_ember_spirit_sleight_of_fist_lua_intr:IsPurgeException() return false end
function modifier_ember_spirit_sleight_of_fist_lua_intr:IsStunDebuff() return false end
function modifier_ember_spirit_sleight_of_fist_lua_intr:RemoveOnDeath() return false end
function modifier_ember_spirit_sleight_of_fist_lua_intr:DestroyOnExpire() return false end

function modifier_ember_spirit_sleight_of_fist_lua_intr:OnCreated()
	if IsServer() then
		self:OnIntervalThink()
	end
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:OnIntervalThink()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi9") and self:GetAbility():GetAutoCastState() then
		if self:GetCaster():HasModifier("modifier_ember_spirit_sleight_of_fist_lua_caster") then
			self:StartIntervalThink(0.2)
			return
		elseif self:GetCaster():IsSilenced() or self:GetCaster():IsStunned() then
			self:StartIntervalThink(0.2)
			return
		else
			local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_CLOSEST, false)
			if #units > 0 then	
				local len
				local attack_interval = self:GetAbility():GetSpecialValueFor("attack_interval") / 3
				local t = {}
				local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ember_spirit_sleight_of_fist_lua_caster", {agi9 = true})
				local max_shuffles = 1
				for _,unit in pairs(units) do
					unit:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_sleight_of_fist_lua_marker", {})
					table.insert(t, unit)
				end
				mod.targets_list = t
				mod.targets = table.shuffle(t)
				mod.max_shuffles = max_shuffles
				mod:OnIntervalThink()
				mod:StartIntervalThink(attack_interval)
			end
		end
	end
	self:StartIntervalThink(2)
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_EVENT_ON_DEATH,
	}
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:GetModifierPreAttack_BonusDamage()
	return self:GetStackCount()
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_hero_damage" then
			return 1
		end
	end
	return 0
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_hero_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_hero_damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str9") then
                value = value + self:GetCaster():GetAttributesValue() * 2
            end

            return value
		end
	end
	return 0
end

function modifier_ember_spirit_sleight_of_fist_lua_intr:OnDeath(keys)
   if keys.attacker == self:GetParent() then
	  if keys.attacker:FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int6") and keys.attacker:HasModifier("modifier_ember_spirit_sleight_of_fist_lua_caster") then
		self:SetStackCount( self:GetStackCount() + 5 + 2 * self:GetAbility():GetLevel())
	  end
   end
end


function modifier_ember_spirit_sleight_of_fist_lua_intr:OnAttackLanded(keys)
    if IsServer() then
		if keys.attacker == self:GetParent() then
			-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi9") then
			-- 	local current_cooldown = self:GetAbility():GetCooldownTimeRemaining()
			-- 	self:GetAbility():EndCooldown()
			-- 	self:GetAbility():StartCooldown(current_cooldown - 0.1)
			-- end
		end
	end
end

modifier_ember_spirit_sleight_of_fist_lua_agi13 = class({})
function modifier_ember_spirit_sleight_of_fist_lua_agi13:IsHidden() return false end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:IsDebuff() return true end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:IsPurgable() return true end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:IsPurgeException() return false end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:IsStunDebuff() return false end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:RemoveOnDeath() return true end
function modifier_ember_spirit_sleight_of_fist_lua_agi13:DestroyOnExpire() return true end

function modifier_ember_spirit_sleight_of_fist_lua_agi13:OnCreated()
	self.armor = -0.8 * self:GetCaster():GetLevel()
end

function modifier_ember_spirit_sleight_of_fist_lua_agi13:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_ember_spirit_sleight_of_fist_lua_agi13:GetModifierPhysicalArmorBonus()
	return self.armor
end
