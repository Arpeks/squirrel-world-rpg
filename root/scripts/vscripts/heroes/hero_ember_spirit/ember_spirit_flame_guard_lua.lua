ember_spirit_flame_guard_lua = class({})

function ember_spirit_flame_guard_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function ember_spirit_flame_guard_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
        return DOTA_ABILITY_BEHAVIOR_IMMEDIATE + DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_TOGGLE
    end
    return self.BaseClass.GetBehavior(self)
end

function ember_spirit_flame_guard_lua:OnAbilityUpgrade(hUpgradeAbility)
    if hUpgradeAbility:GetAbilityName() == "special_bonus_unique_npc_dota_hero_ember_spirit_str12" then
        self:RefreshIntrinsicModifier()
    end
end

function ember_spirit_flame_guard_lua:OnToggle()
    
end

function ember_spirit_flame_guard_lua:GetCooldown(level)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
        return 0
    end
    return self.BaseClass.GetCooldown( self, level )
end

function ember_spirit_flame_guard_lua:OnSpellStart()
    local ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
	if #ability.remenants > 0 and ability:bIsUpgraded() then
		for _,rem in pairs(ability.remenants) do
            Timers:CreateTimer(RandomFloat(0.2, 0.6),function()
                self:CastGuardRemenant(rem)
            end)
		end
	end
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_flame_guard_lua", {duration = self:GetSpecialValueFor("duration")})
end

function ember_spirit_flame_guard_lua:CastGuardRemenant(remenant)
    local direction = (self:GetCursorPosition() - remenant:GetAbsOrigin()):Normalized()
    local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_fire_remnant.vpcf", PATTACH_CUSTOMORIGIN, nil)
    ParticleManager:SetParticleControl(iParticleID, 0, remenant:GetAbsOrigin())
    ParticleManager:SetParticleFoWProperties(iParticleID, 0, -1, 500)
    ParticleManager:SetParticleControlEnt(iParticleID, 1, self:GetCaster(), PATTACH_CUSTOMORIGIN_FOLLOW, nil, remenant:GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(iParticleID, 2, Vector(3, 0, 0))
    remenant:AddNoDraw()
    Timers:CreateTimer(0.3,function()
        ParticleManager:DestroyParticle(iParticleID, true)
        ParticleManager:ReleaseParticleIndex(iParticleID)
        remenant:RemoveNoDraw()
    end)
    remenant:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_flame_guard_lua", {duration = self:GetSpecialValueFor("duration")})
end

function ember_spirit_flame_guard_lua:GetIntrinsicModifierName()
	return "modifier_ember_spirit_flame_guard_lua_intr"
end

LinkLuaModifier("modifier_ember_spirit_flame_guard_lua_intr", "heroes/hero_ember_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ember_spirit_flame_guard_lua", "heroes/hero_ember_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ember_spirit_flame_guard_lua_debuff", "heroes/hero_ember_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_bkb", "modifiers/modifier_bkb.lua", LUA_MODIFIER_MOTION_NONE)

modifier_ember_spirit_flame_guard_lua = class({})
--Classifications template
function modifier_ember_spirit_flame_guard_lua:IsHidden()
    return false
end

function modifier_ember_spirit_flame_guard_lua:IsDebuff()
    return false
end

function modifier_ember_spirit_flame_guard_lua:IsPurgable()
    return false
end

function modifier_ember_spirit_flame_guard_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ember_spirit_flame_guard_lua:IsStunDebuff()
    return false
end

function modifier_ember_spirit_flame_guard_lua:RemoveOnDeath()
    return false
end

function modifier_ember_spirit_flame_guard_lua:DestroyOnExpire()
    return true
end

function modifier_ember_spirit_flame_guard_lua:OnCreated()
    self.barrier_max = self:GetAbility():GetSpecialValueFor("absorb_amount")
    self.barrier_block = self:GetAbility():GetSpecialValueFor("absorb_amount")
    self.shield_pct_absorb = self:GetAbility():GetSpecialValueFor("shield_pct_absorb")
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.base_damage = self:GetCaster():GetDamageMin() * 0.5 * (self:GetAbility():GetLevel() / 15)
    if not IsServer() then
        return
    end
    self.bonus_attack_speed = self:GetCaster():GetDisplayAttackSpeed() * 0.5 * (self:GetAbility():GetLevel() / 15)
    local parent = self:GetParent()
    local nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_flameguard.vpcf", PATTACH_POINT_FOLLOW, parent)
    ParticleManager:SetParticleControlEnt(nfx, 0, parent, PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", parent:GetAbsOrigin(), true)
    ParticleManager:SetParticleControlEnt(nfx, 1, parent, PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", parent:GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(nfx, 2, Vector(self.radius, self.radius, self.radius))
    self:AddParticle(nfx, true, false, -1, false, false)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str7") and self:GetParent():GetClassname() == "npc_dota_hero_ember_spirit" then
        self:OnIntervalThink()
        self:StartIntervalThink(5)
    end
    self:SetHasCustomTransmitterData( true )
end

function modifier_ember_spirit_flame_guard_lua:OnRefresh( kv )
    if IsServer() then
        self:SendBuffRefreshToClients()
    end
end

function modifier_ember_spirit_flame_guard_lua:OnIntervalThink()
    self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_bkb", {duration = 2})
end

function modifier_ember_spirit_flame_guard_lua:AddCustomTransmitterData()
    return {
        barrier_max = self.barrier_max,
        barrier_block = self.barrier_block,
        bonus_attack_speed = self.bonus_attack_speed
    }
end

function modifier_ember_spirit_flame_guard_lua:HandleCustomTransmitterData( data )
    self.barrier_max = data.barrier_max
    self.barrier_block = data.barrier_block
    self.bonus_attack_speed = data.bonus_attack_speed
end

function modifier_ember_spirit_flame_guard_lua:DeclareFunctions()
    local t = {
		
	}
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") == nil then
        table.insert(t, MODIFIER_PROPERTY_INCOMING_SPELL_DAMAGE_CONSTANT)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi7") then
        table.insert(t, MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi10") then
        table.insert(t, MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT)
        table.insert(t, MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE)
    end
    return t
end

function modifier_ember_spirit_flame_guard_lua:GetModifierIncomingSpellDamageConstant(keys)
	if IsClient() then
		if keys.report_max then
			return self.barrier_max
		else
			return self.barrier_block
		end
	end
	if keys.damage_type == DAMAGE_TYPE_MAGICAL then
        local damage_to_block = keys.original_damage *  self.shield_pct_absorb / 100
		if damage_to_block >= self.barrier_block then
			self:Destroy()
            return self.barrier_block * (-1)
		else
			self.barrier_block = self.barrier_block - damage_to_block
            self:SendBuffRefreshToClients()
			return damage_to_block * (-1)
		end
	end
end

function modifier_ember_spirit_flame_guard_lua:GetModifierPreAttack_BonusDamage()
    if self:GetElapsedTime() < 3 or self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
        return self:GetCaster():GetAgility() * 5
    end
    return 0
end

function modifier_ember_spirit_flame_guard_lua:GetModifierAttackSpeedBonus_Constant()
	return self.bonus_attack_speed
end

function modifier_ember_spirit_flame_guard_lua:GetModifierBaseAttack_BonusDamage()
	return self.base_damage
end

--------------------------------------------------------------------------------
-- Aura Effects
function modifier_ember_spirit_flame_guard_lua:IsAura()
    return true
end

function modifier_ember_spirit_flame_guard_lua:GetModifierAura()
    return "modifier_ember_spirit_flame_guard_lua_debuff"
end

function modifier_ember_spirit_flame_guard_lua:GetAuraRadius()
    return self.radius
end

function modifier_ember_spirit_flame_guard_lua:GetAuraDuration()
    return 0.5
end

function modifier_ember_spirit_flame_guard_lua:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_ember_spirit_flame_guard_lua:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HEROES_AND_CREEPS 
end

function modifier_ember_spirit_flame_guard_lua:GetAuraSearchFlags()
    return 0
end

modifier_ember_spirit_flame_guard_lua_debuff = class({})
--Classifications template
function modifier_ember_spirit_flame_guard_lua_debuff:IsHidden()
    return false
end

function modifier_ember_spirit_flame_guard_lua_debuff:IsDebuff()
    return true
end

function modifier_ember_spirit_flame_guard_lua_debuff:IsPurgable()
    return false
end

function modifier_ember_spirit_flame_guard_lua_debuff:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ember_spirit_flame_guard_lua_debuff:IsStunDebuff()
    return false
end

function modifier_ember_spirit_flame_guard_lua_debuff:RemoveOnDeath()
    return true
end

function modifier_ember_spirit_flame_guard_lua_debuff:DestroyOnExpire()
    return true
end

function modifier_ember_spirit_flame_guard_lua_debuff:OnCreated()
    self.damage_per_second = self:GetAbility():GetSpecialValueFor("damage_per_second")
    if self:GetAuraOwner():GetClassname() == "npc_dota_base_additive" then
        self.damage_per_second = self.damage_per_second * 0.1
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi10") then
        self.armor = -50 * (self:GetAbility():GetLevel() / 15)
    else
        self.armor = 0
    end
    if not IsServer() then
        return
    end
    if self:GetAbility():GetToggleState() then
        self.damage_type = DAMAGE_TYPE_PHYSICAL
        self.additional_damage = self:GetCaster():GetAverageTrueAttackDamage(self:GetCaster()) * self:GetCaster():GetAttackSpeed(false)
        self.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
    else
        self.damage_type = DAMAGE_TYPE_MAGICAL
        self.additional_damage = 0
        self.damage_flags = 0
    end
    self.special_bonus_unique_npc_dota_hero_ember_spirit_agi10 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi10")
    self.special_bonus_unique_npc_dota_hero_ember_spirit_str7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str7")
    self.tick_interval = self:GetAbility():GetSpecialValueFor("tick_interval")
    self:StartIntervalThink(self.tick_interval)
end

function modifier_ember_spirit_flame_guard_lua_debuff:OnRefresh()
    if IsServer() then
        if self:GetAbility():GetToggleState() then
            self.damage_type = DAMAGE_TYPE_PHYSICAL
            self.additional_damage = self:GetCaster():GetAverageTrueAttackDamage(self:GetCaster()) * self:GetCaster():GetAttackSpeed(false)
            self.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
        else
            self.damage_type = DAMAGE_TYPE_MAGICAL
            self.additional_damage = 0
            self.damage_flags = 0
        end
    end
end

function modifier_ember_spirit_flame_guard_lua_debuff:OnIntervalThink()
    if not self:GetParent():CanBeSeenByAnyOpposingTeam() and not self:GetParent():IsCreep() then
        return
    end

    local particle = ParticleManager:CreateParticle("particles/ember_e/ember_e_lighting.vpcf", PATTACH_WORLDORIGIN, self:GetAuraOwner())
    ParticleManager:SetParticleControl(particle, 1, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleControl(particle, 2, self:GetAuraOwner():GetAbsOrigin() + Vector(0,0,200))
    ParticleManager:ReleaseParticleIndex(particle)
    if self.special_bonus_unique_npc_dota_hero_ember_spirit_str7 then
        self:GetCaster().pet_locked = true
        self:GetCaster():PerformAttack(self:GetParent(), true, true, true, true, false, true, true)
        self:GetCaster().pet_locked = false
    end
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self.damage_per_second + self.additional_damage * self.tick_interval,
        damage_type = self.damage_type,
        damage_flags = self.damage_flags,
        ability = self:GetAbility()
    })
end

function modifier_ember_spirit_flame_guard_lua_debuff:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
    }
end

function modifier_ember_spirit_flame_guard_lua_debuff:GetModifierPhysicalArmorBonus()
    return self.armor
end












modifier_ember_spirit_flame_guard_lua_intr = class({})
function modifier_ember_spirit_flame_guard_lua_intr:IsHidden() return true end
function modifier_ember_spirit_flame_guard_lua_intr:IsDebuff() return false end
function modifier_ember_spirit_flame_guard_lua_intr:IsPurgable() return false end
function modifier_ember_spirit_flame_guard_lua_intr:IsPurgeException() return false end
function modifier_ember_spirit_flame_guard_lua_intr:IsStunDebuff() return false end
function modifier_ember_spirit_flame_guard_lua_intr:RemoveOnDeath() return false end
function modifier_ember_spirit_flame_guard_lua_intr:DestroyOnExpire() return false end

function modifier_ember_spirit_flame_guard_lua_intr:OnCreated()
    -- self.radius = self:GetAbility():GetSpecialValueFor("radius")
    -- self.special_bonus_unique_npc_dota_hero_ember_spirit_str13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str13")
    -- if not IsServer() then return end
    -- if not self.nfx and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
    --     local parent = self:GetParent()
    --     self.nfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_flameguard.vpcf", PATTACH_POINT_FOLLOW, parent)
    --     ParticleManager:SetParticleControlEnt(self.nfx, 0, parent, PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", parent:GetAbsOrigin(), true)
    --     ParticleManager:SetParticleControlEnt(self.nfx, 1, parent, PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", parent:GetAbsOrigin(), true)
    --     ParticleManager:SetParticleControl(self.nfx, 2, Vector(self.radius, self.radius, self.radius))
    -- end
    -- if self.nfx and not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
    --     ParticleManager:DestroyParticle(self.nfx, false)
    --     ParticleManager:ReleaseParticleIndex(self.nfx)
    -- end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") then
        self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ember_spirit_flame_guard_lua", {})
    elseif self:GetCaster():HasModifier("modifier_ember_spirit_flame_guard_lua") then
        local mod = self:GetCaster():FindModifierByName("modifier_ember_spirit_flame_guard_lua")
        if mod:GetDuration() <= 0 then
            mod:Destroy()
        end
    end
end

function modifier_ember_spirit_flame_guard_lua_intr:OnRefresh()
    self:OnCreated()
end

function modifier_ember_spirit_flame_guard_lua_intr:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
    }
end

function modifier_ember_spirit_flame_guard_lua_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_per_second" then
			return 1
		end
	end
	return 0
end

function modifier_ember_spirit_flame_guard_lua_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_per_second" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage_per_second", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int7") then
                value = value + self:GetCaster():GetIntellect(true) * 1.0
            end

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str10") then
                value = value + self:GetCaster():GetDamageMin() * 0.35
            end

            return value
		end
	end
	return 0
end

function modifier_ember_spirit_flame_guard_lua_intr:GetModifierSpellAmplify_Percentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str13") and not self:GetAbility():GetToggleState() then
        return self:GetCaster():GetIntellect(true) * 0.3
    end
end

function modifier_ember_spirit_flame_guard_lua_intr:IsAura()
    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str12") ~= nil
end

function modifier_ember_spirit_flame_guard_lua_intr:GetModifierAura()
    return "modifier_ember_spirit_flame_guard_lua_debuff"
end

function modifier_ember_spirit_flame_guard_lua_intr:GetAuraRadius()
    return self.radius
end

function modifier_ember_spirit_flame_guard_lua_intr:GetAuraDuration()
    return 0.5
end

function modifier_ember_spirit_flame_guard_lua_intr:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_ember_spirit_flame_guard_lua_intr:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HEROES_AND_CREEPS 
end

function modifier_ember_spirit_flame_guard_lua_intr:GetAuraSearchFlags()
    return 0
end

LinkLuaModifier("modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12", "heroes/hero_ember_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE)

special_bonus_unique_npc_dota_hero_ember_spirit_str12 = class({})

function special_bonus_unique_npc_dota_hero_ember_spirit_str12:GetIntrinsicModifierName()
    return "modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12"
end

modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12 = class({})

function modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12:IsHidden() return true end

function modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12:OnCreated( kv )
    self:OnRefresh()
end

function modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12:OnDestroy()
    self:OnRefresh()
end

function modifier_special_bonus_unique_npc_dota_hero_ember_spirit_str12:OnRefresh()
    self:GetParent():FindModifierByName("modifier_ember_spirit_flame_guard_lua_intr"):OnRefresh()
end