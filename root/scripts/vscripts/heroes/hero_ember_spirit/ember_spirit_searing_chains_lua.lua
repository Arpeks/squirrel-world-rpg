ember_spirit_searing_chains_lua = class({})

function ember_spirit_searing_chains_lua:GetCastRange(vLocation, hTarget)
    return self:GetSpecialValueFor("radius")
end

function ember_spirit_searing_chains_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function ember_spirit_searing_chains_lua:OnSpellStart()
    if not self.cast_table then
        self.cast_table = {}
    end
    local ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
    local remnant_chains = self:GetSpecialValueFor("remnant_chains")
    local max_targets = self:GetSpecialValueFor("unit_count")
    EmitSoundOn("Hero_EmberSpirit.SearingChains.Cast", self:GetCaster())
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetSpecialValueFor("radius"),  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    if #units > max_targets then
        units = table.random_some(units, max_targets)
    end
    for _,unit in pairs(units) do
        unit:AddNewModifier(self:GetCaster(), self, "modifier_ember_spirit_searing_chains_lua", {duration = self:GetSpecialValueFor("duration")})
    end
    if #ability.remenants > 0 and (ability:bIsUpgraded() or remnant_chains) then
        for _,rem in pairs(ability.remenants) do
            if remnant_chains then
                self:CastChainsRemenant(rem)
            else
                Timers:CreateTimer(RandomFloat(0.2, 0.6),function()
                    self:CastChainsRemenant(rem)
                end)
            end
        end
    end
end

function ember_spirit_searing_chains_lua:CastChainsRemenant(remenant)
    local direction = (self:GetCursorPosition() - remenant:GetAbsOrigin()):Normalized()
    local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_fire_remnant.vpcf", PATTACH_CUSTOMORIGIN, nil)
    ParticleManager:SetParticleControl(iParticleID, 0, remenant:GetAbsOrigin())
    ParticleManager:SetParticleFoWProperties(iParticleID, 0, -1, 500)
    ParticleManager:SetParticleControlEnt(iParticleID, 1, self:GetCaster(), PATTACH_CUSTOMORIGIN_FOLLOW, nil, remenant:GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(iParticleID, 2, Vector(3, 0, 0))
    remenant:AddNoDraw()
    Timers:CreateTimer(0.3,function()
        ParticleManager:DestroyParticle(iParticleID, true)
        ParticleManager:ReleaseParticleIndex(iParticleID)
        remenant:RemoveNoDraw()
    end)
    local max_targets = self:GetSpecialValueFor("unit_count")
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), remenant:GetAbsOrigin(), nil, self:GetSpecialValueFor("radius"),  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    if #units > max_targets then
        units = table.random_some(units, max_targets)
    end
    for _,unit in pairs(units) do
        unit:AddNewModifier(remenant, self, "modifier_ember_spirit_searing_chains_lua", {duration = self:GetSpecialValueFor("duration")})
    end
end

LinkLuaModifier("modifier_ember_spirit_searing_chains_lua", "heroes/hero_ember_spirit/ember_spirit_searing_chains_lua", LUA_MODIFIER_MOTION_NONE)

modifier_ember_spirit_searing_chains_lua = class({})
--Classifications template
function modifier_ember_spirit_searing_chains_lua:IsHidden()
    return false
end

function modifier_ember_spirit_searing_chains_lua:IsDebuff()
    return true
end

function modifier_ember_spirit_searing_chains_lua:IsPurgable()
    return true
end

function modifier_ember_spirit_searing_chains_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ember_spirit_searing_chains_lua:IsStunDebuff()
    return false
end

function modifier_ember_spirit_searing_chains_lua:RemoveOnDeath()
    return true
end

function modifier_ember_spirit_searing_chains_lua:DestroyOnExpire()
    return true
end

function modifier_ember_spirit_searing_chains_lua:OnCreated(data)
    if not IsServer() then
        return
    end
    if data.mult and data.mult >= 1 then
        self.damage = self:GetAbility():GetSpecialValueFor("damage_per_second") * data.mult / 2
    else
        self.damage = self:GetAbility():GetSpecialValueFor("damage_per_second") / 2
    end
    self:StartIntervalThink(0.5)
    local impact_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_searing_chains_start.vpcf", PATTACH_ABSORIGIN, self:GetParent())
	ParticleManager:SetParticleControl(impact_pfx, 0, self:GetCaster():GetAbsOrigin())
	ParticleManager:SetParticleControl(impact_pfx, 1, self:GetParent():GetAbsOrigin())
	ParticleManager:ReleaseParticleIndex(impact_pfx)
    self:GetParent():EmitSound("Hero_EmberSpirit.SearingChains.Target")
end

function modifier_ember_spirit_searing_chains_lua:OnIntervalThink()
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self.damage,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self:GetAbility()
    })
end

function modifier_ember_spirit_searing_chains_lua:GetEffectName()
	return "particles/units/heroes/hero_ember_spirit/ember_spirit_searing_chains_debuff.vpcf"
end

function modifier_ember_spirit_searing_chains_lua:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_ember_spirit_searing_chains_lua:CheckState()
    return {
        [MODIFIER_STATE_ROOTED] = true
    }
end