
ember_spirit_fire_remnant_lua = class({})
ember_spirit_fire_remnant_lua.remenants = ember_spirit_fire_remnant_lua.remenants or {}

LinkLuaModifier("modifier_fire_remnant_intr", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_fire_remnant_state", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_fire_remnant_marker", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_fire_remnant_int12", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_fire_remnant_int12_aura_effect", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_npc_dota_hero_ember_spirit_str8", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)

LinkLuaModifier("modifier_ember_spirit_flame_guard_lua", "heroes/hero_ember_spirit/ember_spirit_flame_guard_lua.lua", LUA_MODIFIER_MOTION_NONE)

function ember_spirit_fire_remnant_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 150 + math.min(65000, self:GetCaster():GetIntellect(true) / 30)
end

function ember_spirit_fire_remnant_lua:GetIntrinsicModifierName()
    return "modifier_fire_remnant_intr"
end

function ember_spirit_fire_remnant_lua:OnUpgrade()
    local sub_ability = self:GetCaster():FindAbilityByName("ember_spirit_activate_fire_remnant_lua")
    if sub_ability then
        sub_ability:SetLevel(math.min(self:GetLevel(), 3))
    end
end

function ember_spirit_fire_remnant_lua:GetAssociatedSecondaryAbilities()
    return "ember_spirit_fire_remnant_lua"
end

function ember_spirit_fire_remnant_lua:OnAbilityPhaseStart()
    self:GetCaster().pet_locked = true
    if GridNav:CanFindPath(Vector(-1102, 6103, 0), self:GetCursorPosition()) == false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        if player then
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_cast_here" })
            -- self:EndCooldown()
            -- self:RefundManaCost()
            return false
        end
    end
end

function ember_spirit_fire_remnant_lua:OnSpellStart()
    Timers:CreateTimer(FrameTime(),function()
        self:GetCaster().pet_locked = false   
    end)
    local modifier_caster = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_fire_remnant_marker", {duration = self:GetSpecialValueFor("duration")})
    
    local duration = self:GetSpecialValueFor("duration")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int11") then
        duration = -1
        if #self.remenants > 3 then
            local rem = table.remove(self.remenants, 1)
            rem:RemoveModifierByName("modifier_fire_remnant_state")
        end
    end
    local remenant = CreateUnitByName("npc_dota_ember_spirit_remnant", self:GetCursorPosition(), true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int10") then
	    local ability = self:GetCaster():FindAbilityByName("ember_spirit_flame_guard_lua")
        remenant:AddNewModifier(self:GetCaster(), ability, "modifier_ember_spirit_flame_guard_lua", {duration = duration})
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int12") then
        remenant:AddNewModifier(self:GetCaster(), self, "modifier_fire_remnant_int12", {duration = duration})
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str8") then
        remenant:AddNewModifier(self:GetCaster(), self, "modifier_npc_dota_hero_ember_spirit_str8", {})
    end
    remenant.active = false
    local modifier_remenant = remenant:AddNewModifier(self:GetCaster(), self, "modifier_fire_remnant_state", {duration = duration})
    modifier_remenant.modifier_caster = modifier_caster
    remenant.modifier_remenant = modifier_remenant
    table.insert(self.remenants, remenant)

    local distance = (self:GetCaster():GetAbsOrigin() - self:GetCursorPosition()):Length2D()
    local direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()

    local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_fire_remnant_trail.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
    ParticleManager:SetParticleControlEnt(iParticleID, 0, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(iParticleID, 0, self:GetCaster():GetAbsOrigin())
    ParticleManager:SetParticleControl(iParticleID, 1, direction * self:GetCaster():GetMoveSpeedModifier(self:GetCaster():GetBaseMoveSpeed(), false) * self:GetSpecialValueFor("speed_multiplier") * 0.01)
    ParticleManager:SetParticleShouldCheckFoW(iParticleID, false)
    remenant.iParticleID = iParticleID
    self:GetCaster():EmitSound("Hero_EmberSpirit.FireRemnant.Cast")

    local info = 
	{
		Ability = self,
		EffectName = "",
		vSpawnOrigin = self:GetCaster():GetAbsOrigin(),
		fDistance = distance,
		fStartRadius = 0,
		fEndRadius = 0,
		Source = caster,
		bHasFrontalCone = false,
		bReplaceExisting = false,
		iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_NONE,
		iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_NONE,
		iUnitTargetType = DOTA_UNIT_TARGET_NONE,
		fExpireTime = GameRules:GetGameTime() + 30.0,
		bDeleteOnHit = true,
		vVelocity = direction * self:GetCaster():GetMoveSpeedModifier(self:GetCaster():GetBaseMoveSpeed(), false) * self:GetSpecialValueFor("speed_multiplier") * 0.01,
		bProvidesVision = false,
        ExtraData = {remenant = remenant:entindex(), iParticleID = iParticleID}
	}
	local projectile = ProjectileManager:CreateLinearProjectile(info)
    remenant.projectile_id = projectile
end

function ember_spirit_fire_remnant_lua:OnProjectileThink_ExtraData(vLocation, ExtraData)
    local remenant = EntIndexToHScript(ExtraData.remenant)
    remenant:SetAbsOrigin(vLocation)
end

function ember_spirit_fire_remnant_lua:OnProjectileHit_ExtraData(hTarget, vLocation, ExtraData)
    local radius = self:GetSpecialValueFor("radius")
    local Seq = math.random(8,10)
    local remenant = EntIndexToHScript(ExtraData.remenant)
    remenant.active = true
    local modifier_remenant = remenant.modifier_remenant
    vLocation = GetGroundPosition(vLocation, self:GetCaster())
    local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_fire_remnant.vpcf", PATTACH_CUSTOMORIGIN, remenant)
    ParticleManager:SetParticleControl(iParticleID, 0, vLocation)
    ParticleManager:SetParticleFoWProperties(iParticleID, 0, -1, radius)
    ParticleManager:SetParticleControlEnt(iParticleID, 1, self:GetCaster(), PATTACH_CUSTOMORIGIN_FOLLOW, nil, vLocation, true)
    ParticleManager:SetParticleControl(iParticleID, 2, Vector(Seq, 0, 0))
    remenant.iParticleID = iParticleID
    modifier_remenant:AddParticle(iParticleID, true, false, -1, false, false)

    ParticleManager:DestroyParticle(ExtraData.iParticleID, true)
    ParticleManager:ReleaseParticleIndex(ExtraData.iParticleID)
    remenant:EmitSound("Hero_EmberSpirit.FireRemnant.Create")
end

modifier_fire_remnant_state = class({})
--Classifications template
function modifier_fire_remnant_state:IsHidden()
    return true
end

function modifier_fire_remnant_state:IsDebuff()
    return false
end

function modifier_fire_remnant_state:IsPurgable()
    return false
end

function modifier_fire_remnant_state:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_fire_remnant_state:IsStunDebuff()
    return false
end

function modifier_fire_remnant_state:RemoveOnDeath()
    return true
end

function modifier_fire_remnant_state:DestroyOnExpire()
    return true
end

function modifier_fire_remnant_state:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetAbility().remenants = table.remove_item(self:GetAbility().remenants, self:GetParent())
    local iParticleID = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_hit.vpcf", PATTACH_WORLDORIGIN, nil)
    ParticleManager:SetParticleControl(iParticleID, 0, self:GetParent():GetAbsOrigin())
    ParticleManager:SetParticleFoWProperties(iParticleID, 0, -1, self:GetAbility():GetSpecialValueFor("radius"))
    ParticleManager:ReleaseParticleIndex(iParticleID)
    if self.modifier_caster then
        self.modifier_caster:Destroy()
    end
    UTIL_Remove(self:GetParent())
end

function modifier_fire_remnant_state:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_ROOTED] = true,
		[MODIFIER_STATE_UNSELECTABLE] = true,
		[MODIFIER_STATE_FORCED_FLYING_VISION] = true
    }
end

modifier_fire_remnant_marker = class({})
--Classifications template
function modifier_fire_remnant_marker:IsHidden()
    return false
end

function modifier_fire_remnant_marker:IsDebuff()
    return false
end

function modifier_fire_remnant_marker:IsPurgable()
    return false
end

function modifier_fire_remnant_marker:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_fire_remnant_marker:IsStunDebuff()
    return false
end

function modifier_fire_remnant_marker:RemoveOnDeath()
    return false
end

function modifier_fire_remnant_marker:DestroyOnExpire()
    return false
end

function modifier_fire_remnant_marker:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

ember_spirit_activate_fire_remnant_lua = class({})

function ember_spirit_activate_fire_remnant_lua:OnUnStolen()
    self:GetParent():RemoveModifierByName("modifier_fire_remnant_motion")
end

function ember_spirit_activate_fire_remnant_lua:GetAssociatedPrimaryAbilities()
    return "ember_spirit_fire_remnant_lua"
end

function ember_spirit_activate_fire_remnant_lua:OnAbilityPhaseStart()
    local main_ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
    if #main_ability.remenants > 0 then
        return true
    end
    local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
    CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", {message = "#dota_hud_error_ember_spirit_no_active_remnants"})
    return false
end

function ember_spirit_activate_fire_remnant_lua:OnSpellStart()
    local t = {}
    local main_ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
    if not main_ability then
        return
    end
    local mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_fire_remnant_motion", {})
    mod.remenants = main_ability.remenants
    mod:StartIntervalThink(0.01)
    self:SetActivated(false)
end

LinkLuaModifier("modifier_fire_remnant_motion", "heroes/hero_ember_spirit/ember_spirit_fire_remnant_lua", LUA_MODIFIER_MOTION_NONE)

modifier_fire_remnant_motion = class({})
--Classifications template
function modifier_fire_remnant_motion:IsHidden()
    return false
end

function modifier_fire_remnant_motion:IsDebuff()
    return false
end

function modifier_fire_remnant_motion:IsPurgable()
    return false
end

function modifier_fire_remnant_motion:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_fire_remnant_motion:IsStunDebuff()
    return false
end

function modifier_fire_remnant_motion:RemoveOnDeath()
    return false
end

function modifier_fire_remnant_motion:DestroyOnExpire()
    return false
end

function modifier_fire_remnant_motion:OnCreated()
    if not IsServer() then
        return
    end
    self.hitted_targets = {}
    self.target_position = self:GetAbility():GetCursorPosition()
    self.radius = self:GetAbility():GetSpecialValueFor("radius")
    self.speed = self:GetAbility():GetSpecialValueFor("speed")
    local abi = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
    self.damage_tbl = {
        -- victim = data.target,
        attacker = self:GetCaster(),
        damage = abi:GetSpecialValueFor("damage"),
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self:GetAbility()
    }
    -- self.special_bonus_unique_npc_dota_hero_ember_spirit_str11 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_str11")
    self:GetCaster():EmitSound("Hero_EmberSpirit.FireRemnant.Activate")
    local pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_ember_spirit/ember_spirit_remnant_dash.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent())
	ParticleManager:SetParticleControlEnt(pfx, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(pfx, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	self:AddParticle(pfx, false, false, 15, false, false)
end

function modifier_fire_remnant_motion:OnIntervalThink()
    if not self.movement_speed or not self.direction then
        self:CalculateRemenant()
    end
    if not self.remenants[1] or not self.movement_speed or not self.direction then
        self:StartIntervalThink(-1)
        self:Destroy()
        return
    end
    self.remenant_position = self.remenants[1]:GetAbsOrigin()
    self.direction = (self.remenants[1]:GetAbsOrigin() - self:GetParent():GetAbsOrigin()):Normalized()
    local new_pos = self:GetParent():GetAbsOrigin() + self.direction * self.movement_speed * 0.01
    local units = FindUnitsInLine(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), new_pos, nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE)
    for _,unit in pairs(units) do
        if not self.hitted_targets[unit] then
            self.damage_tbl.victim = unit
            ApplyDamageRDA(self.damage_tbl)
            self.hitted_targets[unit] = true
        end
    end
    new_pos = GetGroundPosition(new_pos, self:GetParent())
    self:GetParent():SetAbsOrigin(new_pos)
    GridNav:DestroyTreesAroundPoint(self:GetParent():GetOrigin(), 200, true)

    if (self.remenant_position - new_pos):Length2D() < 50 then
        if ProjectileManager:IsValidProjectile(self.remenants[1].projectile_id) then
            ProjectileManager:DestroyLinearProjectile(self.remenants[1].projectile_id)
        end
        GridNav:DestroyTreesAroundPoint(self:GetParent():GetOrigin(), self.radius, true)
        self.movement_speed = nil
        ParticleManager:DestroyParticle(self.remenants[1].iParticleID, true)
        ParticleManager:ReleaseParticleIndex(self.remenants[1].iParticleID)
        self.hitted_targets = {}
        EmitSoundOnLocationWithCaster(new_pos, "Hero_EmberSpirit.FireRemnant.Explode", self:GetCaster())
        if self.special_bonus_unique_npc_dota_hero_ember_spirit_str11 then
            self.current_remenant.modifier_remenant:SetDuration(8, true)
        else
            self.current_remenant.modifier_remenant:Destroy()
        end
        if not self.remenants[1] then
            self:StartIntervalThink(-1)
            self:Destroy()
        end
    end
end

function modifier_fire_remnant_motion:CalculateRemenant()
    if not self.remenants[1] then
        return
    end
    table.sort( self.remenants, function(a, b) 
        local len_cursor_a = (a:GetAbsOrigin() - self.target_position):Length2D()
        local len_cursor_b = (b:GetAbsOrigin() - self.target_position):Length2D()
        return len_cursor_a > len_cursor_b
    end)
    local d = self.remenants[1]:GetAbsOrigin() - self:GetParent():GetAbsOrigin()
    local direction = d:Normalized()
    local len = d:Length2D()
    if len > self.speed then
        self.movement_speed = len / 0.4
    else
        self.movement_speed = self.speed
    end
    self.direction = direction
    self.remenant_position = self.remenants[1]:GetAbsOrigin()
    self.current_remenant = self.remenants[1]
end

function modifier_fire_remnant_motion:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetCaster():StopSound("Hero_EmberSpirit.FireRemnant.Activate")
    self:GetCaster():EmitSound("Hero_EmberSpirit.FireRemnant.Stop")
    self.main_ability = self:GetCaster():FindAbilityByName("ember_spirit_fire_remnant_lua")
    self.main_ability.remenants = {}
    FindClearSpaceForUnit(self:GetParent(), self:GetParent():GetAbsOrigin(), true)
    self:GetAbility():SetActivated(true)
end

function modifier_fire_remnant_motion:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		-- [MODIFIER_STATE_ROOTED] = true,
		[MODIFIER_STATE_UNSELECTABLE] = true
	}
end

function modifier_fire_remnant_motion:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_IGNORE_CAST_ANGLE
	}
end

function modifier_fire_remnant_motion:GetModifierIgnoreCastAngle()
	return 1
end

modifier_fire_remnant_intr = class({})
function modifier_fire_remnant_intr:IsHidden() return true end
function modifier_fire_remnant_intr:IsDebuff() return false end
function modifier_fire_remnant_intr:IsPurgable() return false end
function modifier_fire_remnant_intr:IsPurgeException() return false end
function modifier_fire_remnant_intr:IsStunDebuff() return false end
function modifier_fire_remnant_intr:RemoveOnDeath() return false end
function modifier_fire_remnant_intr:DestroyOnExpire() return false end

function modifier_fire_remnant_intr:OnCreated( kv )
    if IsServer() then
        self:StartIntervalThink(1)
    end
end

function modifier_fire_remnant_intr:OnIntervalThink()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int12") and GridNav:CanFindPath(Vector(-1102, 6103, 0), self:GetParent():GetAbsOrigin()) == false then
        self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_fire_remnant_int12", {})
    else
        self:GetCaster():RemoveModifierByName("modifier_fire_remnant_int12")
    end
end

function modifier_fire_remnant_intr:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
    }
end

function modifier_fire_remnant_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_fire_remnant_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int8") then
                value = value + self:GetCaster():GetIntellect(true)
            end

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int12") then
                value = value + self:GetCaster():GetIntellect(true) * 0.5
            end

            return value
		end
	end
	return 0
end

function modifier_fire_remnant_intr:GetModifierSpellAmplify_Percentage()
    local bonus = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi11") then
        bonus = self:GetAbility():GetLevel() / 10 * 35 * math.min(100, self:GetCaster():GetLevel())
    end
    return bonus
end

modifier_fire_remnant_int12 = class({})
function modifier_fire_remnant_int12:IsHidden() return true end
function modifier_fire_remnant_int12:IsDebuff() return false end
function modifier_fire_remnant_int12:IsPurgable() return false end
function modifier_fire_remnant_int12:IsPurgeException() return false end
function modifier_fire_remnant_int12:IsStunDebuff() return false end
function modifier_fire_remnant_int12:RemoveOnDeath() return true end
function modifier_fire_remnant_int12:DestroyOnExpire() return true end

function modifier_fire_remnant_int12:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE
    }
end

function modifier_fire_remnant_int12:GetModifierSpellAmplify_Percentage()
    -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_agi11") then
    --     if self:GetAbility():GetLevel() == 10 then
    --         return 100 * self:GetCaster():GetLevel()
    --     end
    -- end
end

function modifier_fire_remnant_int12:IsAura() 
    return self:GetAbility():GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int13") == nil
end
function modifier_fire_remnant_int12:GetModifierAura() return "modifier_fire_remnant_int12_aura_effect" end
function modifier_fire_remnant_int12:GetAuraRadius() return 500 end
function modifier_fire_remnant_int12:GetAuraDuration() return 0.5 end
function modifier_fire_remnant_int12:GetAuraSearchTeam() return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_fire_remnant_int12:GetAuraSearchType() return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC end
function modifier_fire_remnant_int12:GetAuraSearchFlags() return 0 end

function modifier_fire_remnant_int12:OnCreated( kv )
    if not IsServer() then return end
    if self:GetAbility():GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ember_spirit_int13") then
        self:StartIntervalThink(0.5)
    end
end

function modifier_fire_remnant_int12:OnIntervalThink()
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 500,  DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _, unit in pairs(units) do
        ApplyDamageRDA({
            victim = unit,
            attacker = self:GetCaster(),
            damage = self:GetAbility():GetSpecialValueFor("damage") * 0.5,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = DOTA_DAMAGE_FLAG_NONE,
            ability = self:GetAbility()
        })
    end
end

modifier_fire_remnant_int12_aura_effect = class({})
function modifier_fire_remnant_int12_aura_effect:IsHidden() return false end
function modifier_fire_remnant_int12_aura_effect:IsDebuff() return true end
function modifier_fire_remnant_int12_aura_effect:IsPurgable() return false end
function modifier_fire_remnant_int12_aura_effect:IsPurgeException() return false end
function modifier_fire_remnant_int12_aura_effect:IsStunDebuff() return false end
function modifier_fire_remnant_int12_aura_effect:RemoveOnDeath() return true end
function modifier_fire_remnant_int12_aura_effect:DestroyOnExpire() return true end

function modifier_fire_remnant_int12_aura_effect:OnCreated()
    self.damage = self:GetAbility():GetSpecialValueFor("damage") * 0.5
    self.parent = self:GetParent()
    self.caster = self:GetCaster()
    self.abi = self:GetAbility()
    if not IsServer() then return end
    self:StartIntervalThink(0.5)
end

function modifier_fire_remnant_int12_aura_effect:OnIntervalThink()
    ApplyDamageRDA({
        victim = self.parent,
        attacker = self.caster,
        damage = self.damage,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = DOTA_DAMAGE_FLAG_NONE,
        ability = self.abi
    })
end

modifier_npc_dota_hero_ember_spirit_str8 = class({})

function modifier_npc_dota_hero_ember_spirit_str8:IsHidden() return false end

function modifier_npc_dota_hero_ember_spirit_str8:OnCreated( kv )
    self.thinker = kv.isProvidedByAura~=1
end

function modifier_npc_dota_hero_ember_spirit_str8:IsAura()
	return self.thinker
end

function modifier_npc_dota_hero_ember_spirit_str8:GetModifierAura()
	return "modifier_npc_dota_hero_ember_spirit_str8"
end

function modifier_npc_dota_hero_ember_spirit_str8:GetAuraRadius()
	return 900
end

function modifier_npc_dota_hero_ember_spirit_str8:GetAuraDuration()
	return 10
end

function modifier_npc_dota_hero_ember_spirit_str8:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_FRIENDLY
end

function modifier_npc_dota_hero_ember_spirit_str8:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_npc_dota_hero_ember_spirit_str8:GetAuraSearchFlags()
	return 0
end

function modifier_npc_dota_hero_ember_spirit_str8:DeclareFunctions()
    if self.thinker then return end
    return {
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
        MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
    }
end

function modifier_npc_dota_hero_ember_spirit_str8:GetModifierPhysicalArmorBonus()
    return math.pow(20 * self:GetAbility():GetLevel(), 1.12)
end

function modifier_npc_dota_hero_ember_spirit_str8:GetModifierMagicalResistanceBonus()
    return 20 + 1.5 * self:GetAbility():GetLevel()
end