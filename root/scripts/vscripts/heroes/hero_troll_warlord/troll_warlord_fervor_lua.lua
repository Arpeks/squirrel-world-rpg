troll_warlord_fervor_lua = class({})

function troll_warlord_fervor_lua:GetIntrinsicModifierName()
    return "modifier_troll_warlord_fervor_lua"
end

modifier_troll_warlord_fervor_lua = class({})

LinkLuaModifier("modifier_troll_warlord_fervor_lua", "heroes/hero_troll_warlord/troll_warlord_fervor_lua.lua", LUA_MODIFIER_MOTION_NONE)

--Classifications template
function modifier_troll_warlord_fervor_lua:IsHidden()
    return self:GetStackCount() == 0
end

function modifier_troll_warlord_fervor_lua:IsDebuff()
    return false
end

function modifier_troll_warlord_fervor_lua:IsPurgable()
    return false
end

function modifier_troll_warlord_fervor_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_troll_warlord_fervor_lua:IsStunDebuff()
    return false
end

function modifier_troll_warlord_fervor_lua:RemoveOnDeath()
    return false
end

function modifier_troll_warlord_fervor_lua:DestroyOnExpire()
    return false
end

function modifier_troll_warlord_fervor_lua:OnCreated()
    self.records = {}
end

function modifier_troll_warlord_fervor_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,

        -- MODIFIER_EVENT_ON_DEATH,
        
        MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
        -- MODIFIER_EVENT_ON_ATTACK,

        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
    }
end

function modifier_troll_warlord_fervor_lua:GetModifierPreAttack_BonusDamage()
    return self:GetAbility():GetSpecialValueFor("damage") * self:GetStackCount()
end

function modifier_troll_warlord_fervor_lua:GetModifierAttackSpeedBonus_Constant()
    return self:GetAbility():GetSpecialValueFor("attack_speed") * self:GetStackCount()
end

function modifier_troll_warlord_fervor_lua:CustomOnAttackLanded(data)
    if data.attacker == self:GetParent() then
        if not data.target:IsHero() then
            if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi12") then
                if self:GetParent():GetDisplayAttackSpeed() >= 1500 then
                    self:SetStackCount(self:GetStackCount() + 5)
                else
                    self:IncrementStackCount()
                end
            else
                self:IncrementStackCount()
            end
        end
        if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi13") then
            if RollPercentage(10) then
                self.records[data.record + 1] = true
                self:GetParent():PerformAttack(data.target, true, true, true, true, true, false, false)
            end
        end
    end
end

function modifier_troll_warlord_fervor_lua:CustomOnDeath(data)
    if data.attacker == self:GetParent() then
        if data.unit:IsBoss() then
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str6") then
                self:SetStackCount(self:GetStackCount() + 100)
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str9") then
                self:SetStackCount(self:GetStackCount() + 1000)
            end
        end

    end
end

function modifier_troll_warlord_fervor_lua:GetModifierDamageOutgoing_Percentage(data)
    if self.records[data.record] then
        self.records[data.record] = nil
        return 100 + self:GetStackCount() / 200
    end
end

function modifier_troll_warlord_fervor_lua:GetModifierBonusStats_Strength()
    local base = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int8") then
        base = base + self:GetCaster():GetIntellect(true)
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int11") then
        base = base + self:GetCaster():GetAgility()
    end
    return base
end