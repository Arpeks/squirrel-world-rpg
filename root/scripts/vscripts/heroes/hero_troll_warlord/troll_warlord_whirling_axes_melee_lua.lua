troll_warlord_whirling_axes_melee_lua = class({})

LinkLuaModifier("modifier_troll_warlord_whirling_axes_melee_lua_thinker", "heroes/hero_troll_warlord/troll_warlord_whirling_axes_melee_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_troll_warlord_whirling_axes_melee_lua", "heroes/hero_troll_warlord/troll_warlord_whirling_axes_melee_lua.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_troll_warlord_whirling_axes_melee_blind_lua", "heroes/hero_troll_warlord/troll_warlord_whirling_axes_melee_lua.lua", LUA_MODIFIER_MOTION_NONE)

-- function troll_warlord_whirling_axes_melee_lua:OnUpgrade()
-- 	local abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_ranged_lua")
-- 	if abi then
--         if abi:GetLevel() < self:GetLevel() then
-- 		    abi:SetLevel(self:GetLevel())
--         end
-- 	end
-- 	if self:GetCaster():IsRangedAttacker() then
--         self:SetActivated(false)
--     end
-- end

function troll_warlord_whirling_axes_melee_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int7") then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
end

function troll_warlord_whirling_axes_melee_lua:GetIntrinsicModifierName()
	return "modifier_troll_warlord_whirling_axes_melee_lua"
end

function troll_warlord_whirling_axes_melee_lua:OnUpgrade()
	local troll_warlord_switch_stance_lua = self:GetCaster():FindAbilityByName("troll_warlord_switch_stance_lua")
	if troll_warlord_switch_stance_lua:GetLevel() == 0 or not troll_warlord_switch_stance_lua:GetToggleState() then
		self:SetActivated(false)
	end
end

function troll_warlord_whirling_axes_melee_lua:OnSpellStart()
	local caster = self:GetCaster()
	local caster_location = caster:GetAbsOrigin()
	local particle = "particles/units/heroes/hero_troll_warlord/troll_warlord_whirling_axe_melee.vpcf"
	local angle_east = QAngle(0,90,0)
	local angle_west = QAngle(0,-90,0)

	local start_radius = 100

	local forward_vector = caster:GetForwardVector()
	local front_position = caster_location + forward_vector * start_radius
	local position_east = RotatePosition(caster_location, angle_east, front_position) 
	local position_west = RotatePosition(caster_location, angle_west, front_position)

	position_east = GetGroundPosition( position_east, self:GetCaster() )
	position_west = GetGroundPosition( position_west, self:GetCaster() )
	position_east.z = position_east.z + 75
	position_west.z = position_west.z + 75


	local index = DoUniqueString("index")
	self[index] = {}
	Timers:CreateTimer(10,function()
		self[index] = nil
	end)
	self.whirling_axes_east = CreateUnitByName("npc_dota_troll_warlord_axe", position_east, false, caster, caster, caster:GetTeam() )
	self.whirling_axes_east:SetAbsOrigin(position_east)
	self.whirling_axes_east:AddNewModifier(caster, self, "modifier_troll_warlord_whirling_axes_melee_lua_thinker", {index = index})

	self.whirling_axes_east.particle = ParticleManager:CreateParticle(particle, PATTACH_ABSORIGIN_FOLLOW, self.whirling_axes_east)
	ParticleManager:SetParticleControl(self.whirling_axes_east.particle, 0, self.whirling_axes_east:GetAbsOrigin())
	ParticleManager:SetParticleControl(self.whirling_axes_east.particle, 1, self.whirling_axes_east:GetAbsOrigin())
	ParticleManager:SetParticleControl(self.whirling_axes_east.particle, 4, Vector(3,0,0))
	self.whirling_axes_east.axe_radius = start_radius
	self.whirling_axes_east.start_time = GameRules:GetGameTime()
	self.whirling_axes_east.side = 0

	self.whirling_axes_west = CreateUnitByName("npc_dota_troll_warlord_axe", position_west, false, caster, caster, caster:GetTeam() )
	self.whirling_axes_west:SetAbsOrigin(position_west)
	self.whirling_axes_west:AddNewModifier(caster, self, "modifier_troll_warlord_whirling_axes_melee_lua_thinker", {index = index})
	self.whirling_axes_west.particle = ParticleManager:CreateParticle(particle, PATTACH_ABSORIGIN_FOLLOW, self.whirling_axes_west)
	ParticleManager:SetParticleControl(self.whirling_axes_west.particle, 0, self.whirling_axes_west:GetAbsOrigin())
	ParticleManager:SetParticleControl(self.whirling_axes_west.particle, 1, self.whirling_axes_west:GetAbsOrigin())
	ParticleManager:SetParticleControl(self.whirling_axes_west.particle, 4, Vector(3,0,0))
	self.whirling_axes_west.axe_radius = start_radius
	self.whirling_axes_west.start_time = GameRules:GetGameTime()
	self.whirling_axes_west.side = 1
end

modifier_troll_warlord_whirling_axes_melee_lua_thinker = class({})

function modifier_troll_warlord_whirling_axes_melee_lua_thinker:OnCreated(params)
	if not IsServer() then return end
	self.index = params.index
	self.more_damage = params.more_damage
	self:StartIntervalThink(FrameTime())
	self.i = params.i
end

function modifier_troll_warlord_whirling_axes_melee_lua_thinker:OnIntervalThink()
	if not IsServer() then return end
	if self:GetParent().start_time == nil then
		self:Destroy()
	end
	local elapsed_time = GameRules:GetGameTime() - self:GetParent().start_time
	if not self:GetCaster():IsAlive() then self:GetParent():RemoveSelf() return end
	local damage = self:GetAbility():GetSpecialValueFor("damage")
	local hit_radius = self:GetAbility():GetSpecialValueFor("hit_radius")
	local max_range = self:GetAbility():GetSpecialValueFor("max_range")
	local axe_movement_speed = self:GetAbility():GetSpecialValueFor("axe_movement_speed")
	local blind_duration = self:GetAbility():GetSpecialValueFor("blind_duration")
	local whirl_duration = self:GetAbility():GetSpecialValueFor("whirl_duration")
	local caster_location = self:GetCaster():GetAbsOrigin()
	local currentRadius	= self:GetParent().axe_radius 


	local deltaRadius = axe_movement_speed / whirl_duration/2 * FrameTime()
	if elapsed_time >= whirl_duration * 0.65 then
		currentRadius = currentRadius - deltaRadius
	else
		currentRadius = currentRadius + deltaRadius
	end
	currentRadius = math.min( currentRadius, (max_range - hit_radius))
	self:GetParent().axe_radius = currentRadius

	local rotation_angle

	if self.i == 1 then 

		if self:GetParent().side == 1 then
		rotation_angle = elapsed_time * 360
		else
			rotation_angle = elapsed_time * 360 + 180
		end

	else 

		if self:GetParent().side == 1 then
		rotation_angle = elapsed_time * 360 + 90
		else
			rotation_angle = elapsed_time * 360 - 90
		end

	end

	local relPos = Vector( 0, currentRadius, 0 )
	relPos = RotatePosition( Vector(0,0,0), QAngle( 0, -rotation_angle, 0 ), relPos )
	local absPos = GetGroundPosition( relPos + caster_location, self:GetParent() )
	absPos.z = absPos.z + 75
	self:GetParent():SetAbsOrigin( absPos )

	ParticleManager:SetParticleControl(self:GetParent().particle, 0, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl(self:GetParent().particle, 1, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl(self:GetParent().particle, 3, self:GetParent():GetAbsOrigin())
	if elapsed_time >= whirl_duration then
		self:Destroy()
		self:GetParent():RemoveSelf()
	end

	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, hit_radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO+DOTA_UNIT_TARGET_BASIC, 0, FIND_CLOSEST, false)
	for _, unit in pairs(units) do
		local was_hit = false
		if self:GetAbility() and self.index and self:GetAbility()[self.index] then
			for _, stored_target in ipairs(self:GetAbility()[self.index]) do
				if unit == stored_target then
					was_hit = true
				end
			end
		end
		if was_hit == false then
			table.insert(self:GetAbility()[self.index],unit)
			unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_troll_warlord_whirling_axes_melee_blind_lua", {duration = blind_duration})

			local current_damage = damage

			ApplyDamageRDA({victim = unit, attacker = self:GetCaster(), damage = current_damage, damage_type = DAMAGE_TYPE_MAGICAL, ability = self:GetAbility()})
			unit:EmitSound("Hero_TrollWarlord.WhirlingAxes.Target")

		end
	end
end

function modifier_troll_warlord_whirling_axes_melee_lua_thinker:CheckState()
    return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_INVULNERABLE] = true  
	}
end

function modifier_troll_warlord_whirling_axes_melee_lua_thinker:OnDestroy()
	if not IsServer() then return end
	--self:GetAbility()[self.index] = nil
	UTIL_Remove(self:GetParent())
end

modifier_troll_warlord_whirling_axes_melee_lua = class({})
--Classifications template
function modifier_troll_warlord_whirling_axes_melee_lua:IsHidden()
	return true
end

function modifier_troll_warlord_whirling_axes_melee_lua:IsDebuff()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_lua:IsPurgable()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_troll_warlord_whirling_axes_melee_lua:IsStunDebuff()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_lua:RemoveOnDeath()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_lua:DestroyOnExpire()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(3)
end

function modifier_troll_warlord_whirling_axes_melee_lua:OnIntervalThink()
	if self:GetAbility():GetAutoCastState() then
		if self:GetAbility():IsActivated() then
			self:GetAbility():OnSpellStart()
			self:GetAbility():UseResources(false, false, false, true)
		end
	end
end

function modifier_troll_warlord_whirling_axes_melee_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
	}
end

function modifier_troll_warlord_whirling_axes_melee_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_troll_warlord_whirling_axes_melee_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str8") then
                value = value + self:GetCaster():GetStrength()
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 3
            end
            return value
		end
	end
	return 0
end

modifier_troll_warlord_whirling_axes_melee_blind_lua = class({})
--Classifications template
function modifier_troll_warlord_whirling_axes_melee_blind_lua:IsHidden()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:IsDebuff()
	return true
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:IsPurgable()
	return true
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_troll_warlord_whirling_axes_melee_blind_lua:IsStunDebuff()
	return false
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:RemoveOnDeath()
	return true
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:DestroyOnExpire()
	return true
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:OnCreated()
	if not IsServer() then
		return
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int7") then
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int12") then
			self.delay = self:GetCaster():GetSecondsPerAttack(false)
			self:StartIntervalThink(self.delay)
		else
			self:StartIntervalThink(1)
		end
	end
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:OnIntervalThink()
	ApplyDamageRDA({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = self:GetAbility():GetSpecialValueFor("damage") * self.delay,
		damage_type = DAMAGE_TYPE_PHYSICAL,
		damage_flags = 0,
		ability = self:GetAbility()
	})
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MISS_PERCENTAGE,

		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:GetModifierMiss_Percentage()
	return self:GetAbility():GetSpecialValueFor("blind_pct")
end

function modifier_troll_warlord_whirling_axes_melee_blind_lua:GetModifierPhysicalArmorBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int10") then
		return self:GetAbility():GetLevel() * -10
	end
end