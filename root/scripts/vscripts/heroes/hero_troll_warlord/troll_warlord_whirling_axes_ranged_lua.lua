troll_warlord_whirling_axes_ranged_lua = class({})

LinkLuaModifier("modifier_troll_warlord_whirling_axes_ranged_lua", "heroes/hero_troll_warlord/troll_warlord_whirling_axes_ranged_lua", LUA_MODIFIER_MOTION_NONE)

-- function troll_warlord_whirling_axes_ranged_lua:OnUpgrade()
-- 	local abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_melee_lua")
-- 	if abi then
--         if abi:GetLevel() < self:GetLevel() then
-- 		    abi:SetLevel(self:GetLevel())
--         end
-- 	end
--     if not self:GetCaster():IsRangedAttacker() then
--         self:SetActivated(false)
--     end
-- end

function troll_warlord_whirling_axes_ranged_lua:OnUpgrade()
	local troll_warlord_switch_stance_lua = self:GetCaster():FindAbilityByName("troll_warlord_switch_stance_lua")
    if troll_warlord_switch_stance_lua:GetLevel() > 0 and troll_warlord_switch_stance_lua:GetToggleState() then
        self:SetActivated(false)
    end
end

function troll_warlord_whirling_axes_ranged_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int12") then
		return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING
end

function troll_warlord_whirling_axes_ranged_lua:GetIntrinsicModifierName()
    return "modifier_troll_warlord_whirling_axes_ranged_lua"
end

function troll_warlord_whirling_axes_ranged_lua:OnSpellStart()
    self:OnSpellStart_point(self:GetCursorPosition())
end

function troll_warlord_whirling_axes_ranged_lua:OnSpellStart_point(point)
    if not self.hitted_table then
        self.hitted_table = {}
    end
    local direction = (point - self:GetCaster():GetOrigin()):Normalized()
    if direction:Length2D() == 0 then
        direction = self:GetCaster():GetForwardVector()
    end
    direction.z = 0
    direction = RotatePosition(Vector(0,0,0), QAngle(0,-15,0), direction)
    local axe_speed = self:GetSpecialValueFor("axe_speed")

    local should_mult_damage = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi11") ~= nil
    local unic_string = DoUniqueString("index")
    self.hitted_table[unic_string] = {}

    Timers:CreateTimer(10,function()
        self.hitted_table[unic_string] = nil
    end)

    local projectile =
    {
        Ability				= self,
        EffectName			= "particles/units/heroes/hero_troll_warlord/troll_warlord_whirling_axe_ranged.vpcf",
        vSpawnOrigin		= self:GetCaster():GetOrigin(),
        fDistance			= self:GetSpecialValueFor("axe_range"),
        fStartRadius		= self:GetSpecialValueFor("axe_width"),
        fEndRadius			= self:GetSpecialValueFor("axe_width"),
        Source				= self:GetCaster(),
        bHasFrontalCone		= false,
        bReplaceExisting	= false,
        iUnitTargetTeam		= self:GetAbilityTargetTeam(),
        iUnitTargetFlags	= self:GetAbilityTargetFlags(),
        iUnitTargetType		= self:GetAbilityTargetType(),
        fExpireTime 		= GameRules:GetGameTime() + 10.0,
        bDeleteOnHit		= false,
        -- vVelocity			= direction * axe_speed,
        bProvidesVision		= false,
        ExtraData			= {unic_string = unic_string, should_mult_damage = should_mult_damage}
    }

    for i = 1, self:GetSpecialValueFor("axe_count") do
        direction = RotatePosition(Vector(0,0,0), QAngle(0,5,0), direction)
        projectile.vVelocity = direction * axe_speed
        ProjectileManager:CreateLinearProjectile(projectile)
    end
    self:GetCaster():EmitSound("Hero_TrollWarlord.WhirlingAxes.Ranged")
end

function troll_warlord_whirling_axes_ranged_lua:OnProjectileHit_ExtraData(target, location, ExtraData)
    if not target then
        return
    end
    if (self.hitted_table[ExtraData.unic_string][target] == nil) or ExtraData.should_mult_damage == 1 then
        self.hitted_table[ExtraData.unic_string][target] = true
        ApplyDamageRDA({
            victim = target,
            attacker = self:GetCaster(),
            damage = self:GetSpecialValueFor("axe_damage"),
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self
        })
    end
end

modifier_troll_warlord_whirling_axes_ranged_lua = class({})
--Classifications template
function modifier_troll_warlord_whirling_axes_ranged_lua:IsHidden()
	return true
end

function modifier_troll_warlord_whirling_axes_ranged_lua:IsDebuff()
	return false
end

function modifier_troll_warlord_whirling_axes_ranged_lua:IsPurgable()
	return false
end

function modifier_troll_warlord_whirling_axes_ranged_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_troll_warlord_whirling_axes_ranged_lua:IsStunDebuff()
	return false
end

function modifier_troll_warlord_whirling_axes_ranged_lua:RemoveOnDeath()
	return false
end

function modifier_troll_warlord_whirling_axes_ranged_lua:DestroyOnExpire()
	return false
end

function modifier_troll_warlord_whirling_axes_ranged_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(1)
end

function modifier_troll_warlord_whirling_axes_ranged_lua:OnIntervalThink()
	if self:GetAbility():GetAutoCastState() then
		if self:GetAbility():IsActivated() then
            self:GetAbility():OnSpellStart_point(self:GetParent():GetAbsOrigin() + self:GetCaster():GetForwardVector() * 100)
		    self:GetAbility():UseResources(false, false, false, true)
        end
	end
end

function modifier_troll_warlord_whirling_axes_ranged_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

        -- MODIFIER_EVENT_ON_ATTACK
	}
end

function modifier_troll_warlord_whirling_axes_ranged_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "axe_damage" then
			return 1
		end
	end
	return 0
end

function modifier_troll_warlord_whirling_axes_ranged_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "axe_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "axe_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int6") then
                value = value + self:GetCaster():GetIntellect(true)
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi8") then
                value = value + self:GetCaster():GetAgility()
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int13") then
                value = value + self:GetCaster():GetIntellect(true) * 3
            end
            return value
		end
	end
	return 0
end

function modifier_troll_warlord_whirling_axes_ranged_lua:CustomOnAttack(data)
    if data.attacker~=self:GetParent() then return end
	if not self:GetParent():IsRangedAttacker() then return end
	if data.no_attack_cooldown then return end
	if data.target:IsBuilding() then return end

    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_int9") then
        if RollPercentage(5) then
		    if self:GetAbility():IsActivated() then
                self:GetAbility():OnSpellStart_point(data.target:GetAbsOrigin())
            end
        end
    end
end