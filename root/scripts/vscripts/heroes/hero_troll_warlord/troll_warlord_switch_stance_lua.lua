troll_warlord_switch_stance_lua = class({})

function troll_warlord_switch_stance_lua:GetIntrinsicModifierName()
	return "modifier_troll_warlord_switch_stance_lua"
end

function troll_warlord_switch_stance_lua:OnOwnerDied()
	if self:GetToggleState() then
		if self:IsActivated() then 
			self:ToggleAbility()
		else
			self:OnToggle()
		end
	end
end

function troll_warlord_switch_stance_lua:ResetToggleOnRespawn()
	return false
end

function troll_warlord_switch_stance_lua:GetAbilityTextureName()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str12") then
		return "troll_warlord_berserkers_rage_active"
	end

	if self:GetToggleState() then
		return "troll_warlord_berserkers_rage_active"
	else
		return "troll_warlord_switch_stance"
	end
end

function troll_warlord_switch_stance_lua:OnToggle()
	if not self:IsActivated() then
		return
	end
	if self:GetCaster():GetAttackCapability() ~= DOTA_UNIT_CAP_MELEE_ATTACK then
		self:GetCaster():SetAttackCapability(DOTA_UNIT_CAP_MELEE_ATTACK)
		local abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_ranged_lua")
		if abi then
			abi:SetActivated(false)
		end
		abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_melee_lua")
		if abi then
			abi:SetActivated(true)
		end
	else
		self:GetCaster():SetAttackCapability(DOTA_UNIT_CAP_RANGED_ATTACK)
		local abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_ranged_lua")
		if abi then
			abi:SetActivated(true)
		end
		abi = self:GetCaster():FindAbilityByName("troll_warlord_whirling_axes_melee_lua")
		if abi then
			abi:SetActivated(false)
		end
	end
	self:GetCaster():EmitSound("Hero_TrollWarlord.BerserkersRage.Toggle")
	self:GetCaster():StartGesture(ACT_DOTA_CAST_ABILITY_1)
end

modifier_troll_warlord_switch_stance_lua = class({})

LinkLuaModifier("modifier_troll_warlord_switch_stance_lua", "heroes/hero_troll_warlord/troll_warlord_switch_stance_lua.lua", LUA_MODIFIER_MOTION_NONE)

--Classifications template
function modifier_troll_warlord_switch_stance_lua:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_troll_warlord_switch_stance_lua:IsDebuff()
	return false
end

function modifier_troll_warlord_switch_stance_lua:IsPurgable()
	return false
end

function modifier_troll_warlord_switch_stance_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_troll_warlord_switch_stance_lua:IsStunDebuff()
	return false
end

function modifier_troll_warlord_switch_stance_lua:RemoveOnDeath()
	return false
end

function modifier_troll_warlord_switch_stance_lua:DestroyOnExpire()
	return false
end

function modifier_troll_warlord_switch_stance_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(FrameTime())
end

function modifier_troll_warlord_switch_stance_lua:OnIntervalThink()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str12") then
		if self:GetCaster():GetAttackCapability() == DOTA_UNIT_CAP_RANGED_ATTACK then
			self:GetAbility():ToggleAbility()
		end
		self:GetAbility():SetActivated(false)
		return
	else
		self:GetAbility():SetActivated(true)
	end
end
function modifier_troll_warlord_switch_stance_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_BASE_ATTACK_TIME_CONSTANT,
		MODIFIER_PROPERTY_TRANSLATE_ATTACK_SOUND,
		MODIFIER_PROPERTY_HEALTH_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,

		-- MODIFIER_EVENT_ON_ATTACK,

		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS_PERCENTAGE,
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,

		-- MODIFIER_EVENT_ON_DEATH,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,

		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS
	}
end

function modifier_troll_warlord_switch_stance_lua:GetActivityTranslationModifiers()
	if not self:GetParent():IsRangedAttacker() then
		return "melee"
	end
end

function modifier_troll_warlord_switch_stance_lua:GetAttackSound()
	return "Hero_TrollWarlord.ProjectileImpact"
end

function modifier_troll_warlord_switch_stance_lua:CheckState()
	return {
		[MODIFIER_STATE_DEBUFF_IMMUNE] = self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str12") ~= nil
	}
end

function modifier_troll_warlord_switch_stance_lua:GetModifierAttackRangeBonus()
	if not self:GetParent():IsRangedAttacker() then
		return -self:GetAbility():GetSpecialValueFor("bonus_range")
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierBaseAttackTimeConstant()
	if not self:GetParent():IsRangedAttacker() then
		if self:GetParent():HasModifier("modifier_talent_base_attack_time") then
			return self:GetAbility():GetSpecialValueFor("base_attack_time") - 0.1
		end
		return self:GetAbility():GetSpecialValueFor("base_attack_time")
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierHealthBonus()
	if not self:GetParent():IsRangedAttacker() then
		return self:GetAbility():GetSpecialValueFor("bonus_hp")
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierPreAttack_BonusDamage()
	if not self:GetParent():IsRangedAttacker() then
		if not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str13") then
			return self:GetAbility():GetSpecialValueFor("damage")
		end
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierMoveSpeedBonus_Constant()
	if self:GetParent():IsRangedAttacker() then
		return self:GetAbility():GetSpecialValueFor("bonus_movespeed")
	end
end

function modifier_troll_warlord_switch_stance_lua:CustomOnAttack(data)
	if data.attacker~=self:GetParent() then return end
	if not self:GetParent():IsRangedAttacker() then return end
	if data.no_attack_cooldown then return end
	if data.target:IsBuilding() then return end

	local orbs_proc = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi10") ~= nil
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetCaster():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	if #units == 1 then
		if RollPercentage(50) then
			self:GetParent():PerformAttack(units[1], orbs_proc, true, true, true, true, false, false)
		end
	else
		local c = 0
		for _, unit in pairs(units) do
			if unit ~= data.target then
				self:GetParent():PerformAttack(unit, orbs_proc, true, true, true, true, false, false)
				c = c + 1
				if c == 2 then
					break
				end
			end
		end
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierBonusStats_Strength()
	if self.lock then
		return 0
	end
	self.lock = true
	local str = self:GetParent():GetStrength()
	self.lock = false
	
	return self:GetStackCount() + (self:GetAbility():GetSpecialValueFor("bonus_str") * (self:GetAbility():GetLevel() / 100) * str)
end

function modifier_troll_warlord_switch_stance_lua:CustomOnDeath(data)
	if data.attacker == self:GetParent() then
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str10") then
			self:IncrementStackCount()
        end
	end
end

function modifier_troll_warlord_switch_stance_lua:CustomOnAttackLanded(data)
	if data.attacker~=self:GetParent() then return end
	if data.no_attack_cooldown then return end
	if data.target:IsBuilding() then return end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str13") then
		ApplyDamageRDA({
			victim = data.target,
			attacker = data.attacker,
			damage = self:GetAbility():GetSpecialValueFor("damage"),
			damage_type = DAMAGE_TYPE_PURE,
			damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
			ability = self:GetAbility()
		})
		SendOverheadEventMessage(data.attacker, OVERHEAD_ALERT_BONUS_SPELL_DAMAGE, data.target, self:GetAbility():GetSpecialValueFor("damage"), nil)
	end
end

function modifier_troll_warlord_switch_stance_lua:GetModifierBaseAttack_BonusDamage()
	return self:GetAbility():GetSpecialValueFor("agi6") * self:GetParent():GetAgility()
end

function modifier_troll_warlord_switch_stance_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_troll_warlord_switch_stance_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_agi9") then
                value = value + self:GetCaster():GetDamageMax()
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_troll_warlord_str13") then
                value = value + self:GetCaster():GetStrength() * 5
            end
            return value
		end
	end
	return 0
end