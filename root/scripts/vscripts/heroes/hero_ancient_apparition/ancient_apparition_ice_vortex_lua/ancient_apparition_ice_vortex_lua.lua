LinkLuaModifier("modifier_ancient_apparition_ice_vortex_lua_aura", "heroes/hero_ancient_apparition/ancient_apparition_ice_vortex_lua/ancient_apparition_ice_vortex_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ancient_apparition_ice_vortex_lua_aura_effect", "heroes/hero_ancient_apparition/ancient_apparition_ice_vortex_lua/ancient_apparition_ice_vortex_lua", LUA_MODIFIER_MOTION_NONE)

ancient_apparition_ice_vortex_lua = class({})

function ancient_apparition_ice_vortex_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function ancient_apparition_ice_vortex_lua:OnSpellStart()
	if not IsServer() then return end
	
	local caster = self:GetCaster()
	local duration = self:GetSpecialValueFor("duration")
	
	self:GetCaster():EmitSound("Hero_Ancient_Apparition.IceVortexCast")

	caster:AddNewModifier(caster, self, "modifier_ancient_apparition_ice_vortex_lua_aura", {duration = duration})
end

function ancient_apparition_ice_vortex_lua:GetDamageIncrease()
	local multiplier = self:GetSpecialValueFor("damage") / 100
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_agi7") ~= nil then
		multiplier = multiplier * 2
	end
	return 1 + multiplier
end

function ancient_apparition_ice_vortex_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_int8") then
		return DOTA_ABILITY_BEHAVIOR_TOGGLE + DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_NO_TARGET 
end

function ancient_apparition_ice_vortex_lua:OnToggle()
	local caster = self:GetCaster()
	if self:GetToggleState() then
		self:GetCaster():EmitSound("Hero_Ancient_Apparition.IceVortexCast")
		caster:AddNewModifier(caster, self, "modifier_ancient_apparition_ice_vortex_lua_aura", {})
	else
		caster:RemoveModifierByName("modifier_ancient_apparition_ice_vortex_lua_aura")
	end
end

function ancient_apparition_ice_vortex_lua:OnOwnerDied()
	self.toggle_state = self:GetToggleState()
end

function ancient_apparition_ice_vortex_lua:OnRespawn()
	if self.toggle_state then
		self:ToggleAbility()
	end
end

function ancient_apparition_ice_vortex_lua:GetCooldown( level )
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_int8") then
		return 0.2
	end
	return self.BaseClass.GetCooldown( self, level )
end

---------------------------------------------------------------------------------------------------

modifier_ancient_apparition_ice_vortex_lua_aura = class({})

function modifier_ancient_apparition_ice_vortex_lua_aura:IsHidden()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura:OnCreated()
	local vortex_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_ancient_apparition/ancient_anti_abrasion.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
	ParticleManager:SetParticleControl(vortex_particle, 0, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl(vortex_particle, 5, Vector(700, 0, 0))
	self:AddParticle(vortex_particle, false, false, -1, false, false)
	
	self.effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_crystalmaiden/maiden_freezing_field_snow.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
	-- self.effect_cast = assert(loadfile("lua_abilities/rubick_spell_steal_lua/rubick_spell_steal_lua_arcana"))(self, particle_cast, PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
	ParticleManager:SetParticleControl( self.effect_cast, 1, Vector( 700, 700, 1 ) )
	self:AddParticle( self.effect_cast, false, false, -1, false, false )
end

function modifier_ancient_apparition_ice_vortex_lua_aura:IsDebuff()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura:IsPurgable()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura:IsAura()
	return (not self:GetCaster():PassivesDisabled())
end

function modifier_ancient_apparition_ice_vortex_lua_aura:GetModifierAura()
	return "modifier_ancient_apparition_ice_vortex_lua_aura_effect"
end

function modifier_ancient_apparition_ice_vortex_lua_aura:GetAuraRadius()
	return 700
end

function modifier_ancient_apparition_ice_vortex_lua_aura:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_ancient_apparition_ice_vortex_lua_aura:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_ancient_apparition_ice_vortex_lua_aura:GetAuraDuration()
    return 0.2
end

------------------------------------------------------------------------------------------------

modifier_ancient_apparition_ice_vortex_lua_aura_effect = class({})

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:IsHidden()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:IsDebuff()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:IsPurgable()
	return false
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:OnCreated( kv )
	self.slow = self:GetAbility():GetSpecialValueFor( "slow" ) * (-1)
	self.damage = self:GetAbility():GetSpecialValueFor( "damage" )
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_agi7") ~= nil then 
		self.slow = self.slow * 2
	end
	if IsServer() then
		self.interval = 0.2
		if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_int8") then
			self:StartIntervalThink(0.2)
		end
	end
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:OnIntervalThink()
	local damage = self:GetCaster():GetIntellect(true) * 0.08
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_ancient_apparition_int50") ~= nil then 
		damage = damage + self:GetCaster():GetIntellect(true) * 0.5
	end
	damage = damage * self:GetAbility():GetDamageIncrease()
	ApplyDamageRDA({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = damage * self.interval,
		damage_type = DAMAGE_TYPE_MAGICAL,
		damage_flags = 0,
		ability = self:GetAbility()
	})
end


function modifier_ancient_apparition_ice_vortex_lua_aura_effect:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		-- MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
		MODIFIER_PROPERTY_DISABLE_HEALING,
	}
	return funcs
end


function modifier_ancient_apparition_ice_vortex_lua_aura_effect:GetModifierMoveSpeedBonus_Percentage(keys)
	return self.slow
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:GetModifierMagicalResistanceBonus(keys)
	-- if keys.damage_category == DOTA_DAMAGE_CATEGORY_SPELL then
	-- 	if self.npc_dota_hero_ancient_apparition_int8 then
	-- 		return self.damage + 15
	-- 	end
	-- 	return self.damage
	-- elseif self.npc_dota_hero_ancient_apparition_int8 then
	-- 	return 115
	-- end
	-- return -self.damage
end

function modifier_ancient_apparition_ice_vortex_lua_aura_effect:GetDisableHealing()
	if self:GetCaster():FindAbilityByName("npc_dota_hero_ancient_apparition_str6") ~= nil then 
		return 1
	end
	return 0	
end