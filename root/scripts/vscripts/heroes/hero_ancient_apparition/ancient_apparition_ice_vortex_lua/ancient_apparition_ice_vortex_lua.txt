"DOTAAbilities"
{	
	"ancient_apparition_ice_vortex_lua"
	{
		"BaseClass"                     "ability_lua"
		"ScriptFile"                   	"heroes/hero_ancient_apparition/ancient_apparition_ice_vortex_lua/ancient_apparition_ice_vortex_lua.lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityTextureName"			"ancient_apparition_ice_vortex"
		"MaxLevel"						"15"
		"AbilityCastRange"				"700"
		"AbilityCooldown"				"16"
		
		"AbilityValues"
		{
			"radius"
			{
				"value"			"700"
				"affected_by_aoe_increase"	"1"
			}
			"slow"						"5 10 15 20 25 30 35 40 45 50 55 60 65 70 75"
			"duration"					"5"
			"damage"					"8 11 14 17 20 23 26 29 32 35 38 41 44 47 50"
		}
	}
}