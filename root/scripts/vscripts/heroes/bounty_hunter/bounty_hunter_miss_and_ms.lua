bounty_hunter_miss_and_ms = class({})

LinkLuaModifier( "modifier_bounty_hunter_miss_and_ms", "heroes/bounty_hunter/bounty_hunter_miss_and_ms.lua", LUA_MODIFIER_MOTION_NONE )

function bounty_hunter_miss_and_ms:GetIntrinsicModifierName()
    return "modifier_bounty_hunter_miss_and_ms"
end

modifier_bounty_hunter_miss_and_ms = class({})
--Classifications template
function modifier_bounty_hunter_miss_and_ms:IsHidden()
    return true
end

function modifier_bounty_hunter_miss_and_ms:IsDebuff()
    return false
end

function modifier_bounty_hunter_miss_and_ms:IsPurgable()
    return false
end

function modifier_bounty_hunter_miss_and_ms:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_bounty_hunter_miss_and_ms:IsStunDebuff()
    return false
end

function modifier_bounty_hunter_miss_and_ms:RemoveOnDeath()
    return false
end

function modifier_bounty_hunter_miss_and_ms:DestroyOnExpire()
    return false
end

function modifier_bounty_hunter_miss_and_ms:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_EVASION_CONSTANT,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
        -- MODIFIER_EVENT_ON_DEATH,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
    }
end

function modifier_bounty_hunter_miss_and_ms:CustomOnDeath(data)
    if data.attacker~=self:GetParent() then return end
    if data.unit:HasModifier("modifier_bounty_hunter_track_lua") and self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi10") then
        self:IncrementStackCount()
    end
end

function modifier_bounty_hunter_miss_and_ms:GetModifierBonusStats_Intellect()
    return self:GetAbility():GetSpecialValueFor("bonus_intellect") * self:GetStackCount()
end

function modifier_bounty_hunter_miss_and_ms:GetModifierEvasion_Constant()
    return self:GetAbility():GetSpecialValueFor("miss")
end

function modifier_bounty_hunter_miss_and_ms:GetModifierMoveSpeedBonus_Percentage()
    return self:GetAbility():GetSpecialValueFor("ms")
end

function modifier_bounty_hunter_miss_and_ms:CustomOnAttackLanded(data)
    if data.attacker~=self:GetParent() then return end
    if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int9") == nil then return end

    local splash = false

    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 1200,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for k, unit in pairs(units) do
        if unit:HasModifier("modifier_bounty_hunter_track_lua") then
            splash = true
            break
        end
    end

    if splash and ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
        if data.target ~= nil and data.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
            local direction = data.target:GetOrigin()-self:GetParent():GetOrigin()
            direction.z = 0
            direction = direction:Normalized()
            local range = self:GetParent():GetOrigin() + direction*650/2
    
            local facing_direction = data.attacker:GetAnglesAsVector().y
            local cleave_targets = FindUnitsInRadius(data.attacker:GetTeamNumber(),data.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
            for _,target in pairs(cleave_targets) do
                if target ~= data.target and not target:IsBoss() and not target:IsRealHero() then
                    local attacker_vector = (target:GetOrigin() - data.attacker:GetOrigin())
                    local attacker_direction = VectorToAngles( attacker_vector ).y
                    local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
                    if angle_diff < 45 then
                        ApplyDamageRDA({
                            victim = target,
                            attacker = data.attacker,
                            damage = data.damage,
                            damage_type = DAMAGE_TYPE_PHYSICAL,
                            damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
                            ability = self:GetAbility()
                        })
                    end
                end
            end
            local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
            ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
            ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
            ParticleManager:ReleaseParticleIndex( effect_cast )
    
        end
    end
end