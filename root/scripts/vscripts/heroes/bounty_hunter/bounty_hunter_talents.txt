"DOTAAbilities"
{
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int6"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int7"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int8"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi7"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi11"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
//==	
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str6"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str7"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str8"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str9"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str10"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str11"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}

	//last
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi12"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str12"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	//last
	"special_bonus_unique_npc_dota_hero_bounty_hunter_int13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	"special_bonus_unique_npc_dota_hero_bounty_hunter_agi13"
	{
		"BaseClass"						"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
	
	"special_bonus_unique_npc_dota_hero_bounty_hunter_str13"
	{
		"BaseClass"             		"special_bonus_base"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
	}
}