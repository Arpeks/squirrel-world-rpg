LinkLuaModifier( "modifier_bounty_hunter_jinada_lua", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_jinada_agi12", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_jinada_agi12_debuff", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_jinada_agi12_debuff_tracker", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_jinada_agi12_tracker", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_jinada_str9", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_int6", "heroes/bounty_hunter/bounty_hunter_jinada_lua.lua", LUA_MODIFIER_MOTION_NONE )

bounty_hunter_jinada_lua  = class({})

function bounty_hunter_jinada_lua:GetIntrinsicModifierName()
	return "modifier_bounty_hunter_jinada_lua"
end

function bounty_hunter_jinada_lua:OnOrbFire( params )

end

function bounty_hunter_jinada_lua:OnOrbImpact( params )
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi12") then 
		-- if params.target:IsBuilding() or params.target:GetUnitName() == "npc_dota_hero_target_dummy" then return end
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_bounty_hunter_jinada_agi12", {duration = self:GetSpecialValueFor("duration")})
		params.target:AddNewModifier(self:GetCaster(), self, "modifier_bounty_hunter_jinada_agi12_debuff", {duration = self:GetSpecialValueFor("duration")})
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str9") then 
		params.target:AddNewModifier(self:GetCaster(), self, "modifier_bounty_hunter_jinada_str9", {duration = 1})
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi7") then 
		local dmg = ApplyDamageRDA({
			victim = params.target,
			attacker = params.attacker,
			damage = self:GetCaster():GetIntellect(true) * 0.75,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flags = 0,
			ability = self
		})
		SendOverheadEventMessage( params.target, OVERHEAD_ALERT_BONUS_SPELL_DAMAGE, params.target, dmg, nil )
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int8") then return end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi8") then return end
	self:GetCaster():ModifyGoldFiltered(self:GetSpecialValueFor( "gold_steal" ), true, DOTA_ModifyGold_SharedGold)
	SendOverheadEventMessage( self:GetCaster(), OVERHEAD_ALERT_GOLD, self:GetCaster(), self:GetSpecialValueFor( "gold_steal" ), nil )

	local name = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_bounty_hunter/bounty_hunter_jinada.vpcf", self:GetCaster())
	money_particle = ParticleManager:CreateParticle(name, PATTACH_ABSORIGIN_FOLLOW, params.target)
	ParticleManager:SetParticleControlEnt(money_particle, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
	ParticleManager:ReleaseParticleIndex(money_particle)
end

modifier_bounty_hunter_jinada_lua = class({})

--------------------------------------------------------------------------------
-- Classifications
function modifier_bounty_hunter_jinada_lua:IsHidden()
	return true
end

function modifier_bounty_hunter_jinada_lua:IsDebuff()
	return false
end

function modifier_bounty_hunter_jinada_lua:IsPurgable()
	return false
end

function modifier_bounty_hunter_jinada_lua:GetAttributes()
	return MODIFIER_ATTRIBUTE_PERMANENT
end

--------------------------------------------------------------------------------
-- Initializations
function modifier_bounty_hunter_jinada_lua:OnCreated( kv )
	-- generate data
	self.str6_counter = 0
	self:SetHasCustomTransmitterData( true )
	self.cast = false
	self.records = {}
end

function modifier_bounty_hunter_jinada_lua:OnRefresh( kv )
end

function modifier_bounty_hunter_jinada_lua:OnIntervalThink()
	if self:GetAbility():GetAutoCastState() then
		self:SetStackCount(1)
	else
		self:SetStackCount(0)
	end
	self:StartIntervalThink(-1)
end

function modifier_bounty_hunter_jinada_lua:AddCustomTransmitterData()
	return {
	str6_counter = self.str6_counter,
	}
end

function modifier_bounty_hunter_jinada_lua:HandleCustomTransmitterData( data )
	self.str6_counter = data.str6_counter
end

--------------------------------------------------------------------------------
-- Modifier Effects
function modifier_bounty_hunter_jinada_lua:DeclareFunctions()
	local funcs = {
		-- MODIFIER_EVENT_ON_ATTACK_START,
		-- MODIFIER_EVENT_ON_ATTACK,
		-- MODIFIER_EVENT_ON_ATTACK_FAIL,
		MODIFIER_PROPERTY_PROCATTACK_FEEDBACK,
		-- MODIFIER_EVENT_ON_ATTACK_RECORD_DESTROY,

		-- MODIFIER_EVENT_ON_ORDER,

		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,

		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,

		-- MODIFIER_EVENT_ON_DEATH,

		MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT
	}

	return funcs
end

function modifier_bounty_hunter_jinada_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			return 1
		end
		if data.ability_special_value == "gold_steal" then
			return 1
		end
	end
	return 0
end

function modifier_bounty_hunter_jinada_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_damage", data.ability_special_level )
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi11") then
                value = value + self:GetParent():GetDamageMin() * 0.5
            end
            return value
		end
		if data.ability_special_value == "gold_steal" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "gold_steal", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi9") then
                value = value + 70 * self:GetAbility():GetLevel()
            end
            return value
		end
	end
	return 0
end

function modifier_bounty_hunter_jinada_lua:GetModifierAttackSpeedBonus_Constant()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi8") then
		return 50 * self:GetAbility():GetLevel()
	end
end

function modifier_bounty_hunter_jinada_lua:CustomOnDeath(data)
	if data.attacker == self:GetParent() then
		if self.records[data.record] then
			if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str6") then
				self.str6_counter = self.str6_counter + 1
				self:SendBuffRefreshToClients()
			end
		end
	end
end

function modifier_bounty_hunter_jinada_lua:GetModifierConstantHealthRegen()
	return self.str6_counter
end

function modifier_bounty_hunter_jinada_lua:GetModifierPhysicalArmorBonus()
	return self.str6_counter * 0.5
end

function modifier_bounty_hunter_jinada_lua:GetModifierMoveSpeedBonus_Constant()
	return self.str6_counter * 0.5
end

function modifier_bounty_hunter_jinada_lua:CustomOnAttackStart(data)
	if data.attacker~=self:GetParent() then return end
	if data.target:HasModifier("modifier_bounty_hunter_track_lua") then
		data.attacker:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_bounty_hunter_int6", {duration = 1})
	end
end

function modifier_bounty_hunter_jinada_lua:CustomOnAttack( params )
	-- if not IsServer() then return end
	if params.attacker~=self:GetParent() then return end
	if self:GetCaster().ActiveJinada then
		self.records[params.record] = true

		-- run OrbFire script if available
		if self:GetAbility().OnOrbFire then self:GetAbility():OnOrbFire( params ) end
		return
	end

	-- register attack if being cast and fully castable
	if self:ShouldLaunch( params.target ) then
		-- use mana and cd
		self:GetAbility():UseResources( true, false,false, true)

		-- record the attack
		self.records[params.record] = true

		-- run OrbFire script if available
		if self:GetAbility().OnOrbFire then self:GetAbility():OnOrbFire( params ) end
	end

	self.cast = false
end
function modifier_bounty_hunter_jinada_lua:GetModifierProcAttack_Feedback( params )
	if self.records[params.record] then
		-- apply the effect
		if self:GetAbility().OnOrbImpact then 
			self:GetAbility():OnOrbImpact( params )
			self:SetStackCount(0)
			self:StartIntervalThink(self:GetAbility():GetCooldown(self:GetAbility():GetLevel()))
		end
	end
end
function modifier_bounty_hunter_jinada_lua:CustomOnAttackFail( params )
	if self.records[params.record] then
		-- apply the fail effect
		if self:GetAbility().OnOrbFail then self:GetAbility():OnOrbFail( params ) end
	end
end
function modifier_bounty_hunter_jinada_lua:CustomOnAttackRecordDestroy( params )
	self.records[params.record] = nil
end

function modifier_bounty_hunter_jinada_lua:CustomOnOrder( params )
	if params.unit~=self:GetParent() then return end

	if params.ability then
		if params.order_type == DOTA_UNIT_ORDER_CAST_TOGGLE_AUTO then
			if not self:GetAbility():GetAutoCastState() then
				self:SetStackCount(1)
			else
				self:SetStackCount(0)
			end
		end
		-- if this ability, cast
		if params.ability==self:GetAbility() then
			self.cast = true
			return
		end

		-- if casting other ability that cancel channel while casting this ability, turn off
		local pass = false
		local behavior = params.ability:GetBehavior()
	--[[	if self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_CHANNEL ) or 
			self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_MOVEMENT ) or
			self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_IGNORE_CHANNEL )
		then
			local pass = true -- do nothing
		end ]]--

		if self.cast and (not pass) then
			self.cast = false
		end
	else
		-- if ordering something which cancel channel, turn off
		if self.cast then
			if self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_POSITION ) or
				self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_TARGET )	or
				self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_MOVE ) or
				self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_TARGET ) or
				self:FlagExist( params.order_type, DOTA_UNIT_ORDER_STOP ) or
				self:FlagExist( params.order_type, DOTA_UNIT_ORDER_HOLD_POSITION )
			then
				self.cast = false
			end
		end
	end
end

--------------------------------------------------------------------------------
-- Helper
function modifier_bounty_hunter_jinada_lua:ShouldLaunch( target )
	if self:GetCaster().ActiveJinada then
		self:SetStackCount(1)
		return true
	end
	-- check autocast
	if self:GetAbility():GetAutoCastState() then
		-- filter whether target is valid
		if self:GetAbility().CastFilterResultTarget~=CDOTA_Ability_Lua.CastFilterResultTarget then
			-- check if ability has custom target cast filter
			if self:GetAbility():CastFilterResultTarget( target )==UF_SUCCESS then
				self.cast = true
			end
		else
			local nResult = UnitFilter(
				target,
				self:GetAbility():GetAbilityTargetTeam(),
				self:GetAbility():GetAbilityTargetType(),
				self:GetAbility():GetAbilityTargetFlags(),
				self:GetCaster():GetTeamNumber()
			)
			if nResult == UF_SUCCESS then
				self.cast = true
			end
		end
	end

	if self.cast and self:GetAbility():IsFullyCastable() and (not self:GetParent():IsSilenced()) then
		self:SetStackCount(1)
		return true
	end

	return false
end

function modifier_bounty_hunter_jinada_lua:GetModifierPreAttack_BonusDamage(params)
	if IsServer() then
		if params.target and self:ShouldLaunch( params.target ) then
			if self:GetStackCount() == 1 then
				return self:GetAbility():GetSpecialValueFor("bonus_damage") + self.str6_counter * 15
			end
		end
		return self.str6_counter * 15
	end

	if self:GetStackCount() == 1 then
		return self:GetAbility():GetSpecialValueFor("bonus_damage") + self.str6_counter * 15
	end
	return self.str6_counter * 15
end

function modifier_bounty_hunter_jinada_lua:GetModifierDamageOutgoing_Percentage(data)
	return self:GetAbility():GetSpecialValueFor("outgoing_damage")
end

--------------------------------------------------------------------------------
-- Helper: Flags
function modifier_bounty_hunter_jinada_lua:FlagExist(a,b)--Bitwise Exist
	local p,c,d=1,0,b
	while a>0 and b>0 do
		local ra,rb=a%2,b%2
		if ra+rb>1 then c=c+p end
		a,b,p=(a-ra)/2,(b-rb)/2,p*2
	end
	return c==d
end


modifier_bounty_hunter_jinada_agi12 = class({})
--Classifications template
function modifier_bounty_hunter_jinada_agi12:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_bounty_hunter_jinada_agi12:IsDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12:IsPurgable()
	return false
end

function modifier_bounty_hunter_jinada_agi12:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_jinada_agi12:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12:RemoveOnDeath()
	return false
end

function modifier_bounty_hunter_jinada_agi12:DestroyOnExpire()
	return false
end

function modifier_bounty_hunter_jinada_agi12:OnCreated()
	if not IsServer() then
		return
	end
	print("2")
	self.total_stealled_damage = 0
	self.stack_table = {}
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
	self:SetStackCount(20 * self:GetAbility():GetLevel())
	Timers:CreateTimer(0.1,function()
		while self.stack_table[1] and (self.stack_table[1] + 60) <= GameRules:GetDOTATime(false, false) do
			table.remove(self.stack_table, 1)
			self:SetStackCount(self:GetStackCount() - 20 * self:GetAbility():GetLevel())
		end
		return 0.1
	end)
	-- self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_bounty_hunter_jinada_agi12_tracker", {duration = 60})
	self:SetHasCustomTransmitterData( true )
end

function modifier_bounty_hunter_jinada_agi12:OnRefresh()
	if not IsServer() then
		return
	end
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_agi13") then
		self.total_stealled_damage = self.total_stealled_damage + 0.2
	end
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
	self:SetStackCount(20 * self:GetAbility():GetLevel() + self:GetStackCount())
	-- self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_bounty_hunter_jinada_agi12_tracker", {duration = 60})
	self:SendBuffRefreshToClients()
end

function modifier_bounty_hunter_jinada_agi12:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE
	}
end

function modifier_bounty_hunter_jinada_agi12:GetModifierPreAttack_BonusDamage()
	return math.floor(self.total_stealled_damage) * 140 + self:GetStackCount()
end

function modifier_bounty_hunter_jinada_agi12:AddCustomTransmitterData()
	return {
		total_stealled_damage = self.total_stealled_damage,
	}
end

function modifier_bounty_hunter_jinada_agi12:HandleCustomTransmitterData( data )
	self.total_stealled_damage = data.total_stealled_damage
end

modifier_bounty_hunter_jinada_agi12_tracker = class({})
--Classifications template
function modifier_bounty_hunter_jinada_agi12_tracker:IsHidden()
	return true
end

function modifier_bounty_hunter_jinada_agi12_tracker:IsDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12_tracker:IsPurgable()
	return false
end

function modifier_bounty_hunter_jinada_agi12_tracker:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_jinada_agi12_tracker:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12_tracker:RemoveOnDeath()
	return true
end

function modifier_bounty_hunter_jinada_agi12_tracker:DestroyOnExpire()
	return true
end

function modifier_bounty_hunter_jinada_agi12_tracker:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_bounty_hunter_jinada_agi12_tracker:OnCreated()
	if not IsServer() then
		return
	end
	self.damage_bonus = 20 * self:GetAbility():GetLevel()
	self.mod = self:GetParent():FindModifierByName("modifier_bounty_hunter_jinada_agi12")
	self.mod:SetStackCount(self.mod:GetStackCount() + self.damage_bonus)
end

function modifier_bounty_hunter_jinada_agi12_tracker:OnDestroy()
	if not IsServer() then
		return
	end
	self.mod:SetStackCount(self.mod:GetStackCount() - self.damage_bonus)
end

modifier_bounty_hunter_jinada_str9 = class({})
--Classifications template
function modifier_bounty_hunter_jinada_str9:IsHidden()
	return false
end

function modifier_bounty_hunter_jinada_str9:IsDebuff()
	return true
end

function modifier_bounty_hunter_jinada_str9:IsPurgable()
	return true
end

function modifier_bounty_hunter_jinada_str9:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_jinada_str9:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_jinada_str9:RemoveOnDeath()
	return true
end

function modifier_bounty_hunter_jinada_str9:DestroyOnExpire()
	return true
end

function modifier_bounty_hunter_jinada_str9:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_bounty_hunter_jinada_str9:GetModifierPhysicalArmorBonus()
	return -8 * self:GetAbility():GetLevel()
end


modifier_bounty_hunter_int6 = class({})
--Classifications template
function modifier_bounty_hunter_int6:IsHidden()
	return self:GetAbility():GetSpecialValueFor("bonus_attack_speed") ~= 0
end

function modifier_bounty_hunter_int6:IsDebuff()
	return false
end

function modifier_bounty_hunter_int6:IsPurgable()
	return false
end

function modifier_bounty_hunter_int6:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_int6:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_int6:RemoveOnDeath()
	return true
end

function modifier_bounty_hunter_int6:DestroyOnExpire()
	return true
end

function modifier_bounty_hunter_int6:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ATTACK_START,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_bounty_hunter_int6:CustomOnAttackStart(data)
	if data.attacker~=self:GetParent() then return end
	if not data.target:HasModifier("modifier_bounty_hunter_track_lua") then
		self:Destroy()
	end
end

function modifier_bounty_hunter_int6:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_attack_speed") * self:GetAbility():GetLevel()
end

modifier_bounty_hunter_jinada_agi12_debuff = class({})
--Classifications template
function modifier_bounty_hunter_jinada_agi12_debuff:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_bounty_hunter_jinada_agi12_debuff:IsDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff:IsPurgable()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_jinada_agi12_debuff:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff:RemoveOnDeath()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff:DestroyOnExpire()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff:OnCreated()
	if not IsServer() then
		return
	end
	self.total_stealled_damage = 0
	self.stack_table = {}
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
	self:SetStackCount(20 * self:GetAbility():GetLevel())
	Timers:CreateTimer(0.1,function()
		while self.stack_table[1] and (self.stack_table[1] + 60) <= GameRules:GetDOTATime(false, false) do
			table.remove(self.stack_table, 1)
			self:SetStackCount(self:GetStackCount() - 20 * self:GetAbility():GetLevel())
		end
		return 0.1
	end)
	self:SetHasCustomTransmitterData( true )
end

function modifier_bounty_hunter_jinada_agi12_debuff:OnRefresh()
	if not IsServer() then
		return
	end
	table.insert(self.stack_table, GameRules:GetDOTATime(false, false))
	self:SetStackCount(20 * self:GetAbility():GetLevel() + self:GetStackCount())
	self:SendBuffRefreshToClients()
end

function modifier_bounty_hunter_jinada_agi12_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE
	}
end

function modifier_bounty_hunter_jinada_agi12_debuff:GetModifierPreAttack_BonusDamage()
	return self:GetStackCount() * -1
end

modifier_bounty_hunter_jinada_agi12_debuff_tracker = class({})
--Classifications template
function modifier_bounty_hunter_jinada_agi12_debuff_tracker:IsHidden()
	return true
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:IsDebuff()
	return true
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:IsPurgable()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_jinada_agi12_debuff_tracker:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:RemoveOnDeath()
	return true
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:DestroyOnExpire()
	return true
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:OnCreated()
	if not IsServer() then
		return
	end
	self.mod = self:GetParent():FindModifierByName("modifier_bounty_hunter_jinada_agi12_debuff")
	self.mod:SetStackCount(self.mod:GetStackCount() + 200)
end

function modifier_bounty_hunter_jinada_agi12_debuff_tracker:OnDestroy()
	if not IsServer() then
		return
	end
	self.mod:SetStackCount(self.mod:GetStackCount() - 200)
end