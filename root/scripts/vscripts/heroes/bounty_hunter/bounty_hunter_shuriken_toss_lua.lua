LinkLuaModifier( "modifier_bounty_hunter_shuriken_toss_lua", "heroes/bounty_hunter/bounty_hunter_shuriken_toss_lua.lua", LUA_MODIFIER_MOTION_NONE )

bounty_hunter_shuriken_toss_lua = class({})

function bounty_hunter_shuriken_toss_lua:GetBehavior()
	local beh = DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int8") then
		beh = DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int12") then
		beh = beh + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return beh
end

function bounty_hunter_shuriken_toss_lua:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function bounty_hunter_shuriken_toss_lua:GetIntrinsicModifierName()
	return "modifier_bounty_hunter_shuriken_toss_lua"
end

function bounty_hunter_shuriken_toss_lua:OnSpellStart_target(target, pos)
	local info = {
		Target = target,
		Source = self:GetCaster(),
		Ability = self,	
		
		EffectName = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_bounty_hunter/bounty_hunter_suriken_toss.vpcf", self:GetCaster()),
		iMoveSpeed = 1000,
		
		bDrawsOnMinimap = false,
		bVisibleToEnemies = true,
	}
	local str7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str7")
	local str10 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str10")

	if self:GetSpecialValueFor("radius") > 0 then
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), pos or target:GetAbsOrigin(), nil, self:GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for k, unit in pairs(units) do
			info.Target = unit
			if str7 and not str10 then
				ProjectileManager:CreateTrackingProjectile(info)
				if RollPercentage(50) then
					ProjectileManager:CreateTrackingProjectile(info)
				end
			elseif str7 and str10 then
				ProjectileManager:CreateTrackingProjectile(info)
				while RollPercentage(50) do
					ProjectileManager:CreateTrackingProjectile(info)
				end
			else
				ProjectileManager:CreateTrackingProjectile(info)
			end
		end
	else
		if str7 and not str10 then
			ProjectileManager:CreateTrackingProjectile(info)
			if RollPercentage(50) then
				ProjectileManager:CreateTrackingProjectile(info)
			end
		elseif str7 and str10 then
			ProjectileManager:CreateTrackingProjectile(info)
			while RollPercentage(50) do
				ProjectileManager:CreateTrackingProjectile(info)
			end
		else
			ProjectileManager:CreateTrackingProjectile(info)
		end
	end
end

function bounty_hunter_shuriken_toss_lua:OnSpellStart()
	self:OnSpellStart_target(self:GetCursorTarget(), self:GetCursorPosition())
end

function bounty_hunter_shuriken_toss_lua:OnProjectileHit_ExtraData(hTarget, vLocation, ExtraData)
    if not hTarget then return end
    ApplyDamageRDA({
        victim = hTarget,
        attacker = self:GetCaster(),
        damage = self:GetSpecialValueFor("bonus_damage"),
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self
    })
    hTarget:AddNewModifier(self:GetCaster(), self, "modifier_bounty_hunter_wind_walk_slow", {duration = self:GetSpecialValueFor("slow_duration")})
end

modifier_bounty_hunter_shuriken_toss_lua = class({})
--Classifications template
function modifier_bounty_hunter_shuriken_toss_lua:IsHidden()
	return true
end

function modifier_bounty_hunter_shuriken_toss_lua:IsDebuff()
	return false
end

function modifier_bounty_hunter_shuriken_toss_lua:IsPurgable()
	return false
end

function modifier_bounty_hunter_shuriken_toss_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_shuriken_toss_lua:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_shuriken_toss_lua:RemoveOnDeath()
	return false
end

function modifier_bounty_hunter_shuriken_toss_lua:DestroyOnExpire()
	return false
end

function modifier_bounty_hunter_shuriken_toss_lua:OnCreated()
	self.attack_speed = 100
	if not IsServer() then
		return
	end
	self:SetHasCustomTransmitterData( true )
	self:StartIntervalThink(0.1)
end

function modifier_bounty_hunter_shuriken_toss_lua:OnIntervalThink()
	self.attack_speed = self:GetParent():GetDisplayAttackSpeed()
	self:SendBuffRefreshToClients()
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int12") then
		if self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() then
			local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("cast_range"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
			if #units > 0 then
				self:GetAbility():UseResources(true, false, false, true)
				self:GetAbility():OnSpellStart_target(units[1])
			end
		end
	end
end

function modifier_bounty_hunter_shuriken_toss_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

		-- MODIFIER_EVENT_ON_DEATH
	}
end

function modifier_bounty_hunter_shuriken_toss_lua:AddCustomTransmitterData()
	return {
		attack_speed = self.attack_speed,
	}
end

function modifier_bounty_hunter_shuriken_toss_lua:HandleCustomTransmitterData( data )
	self.attack_speed = data.attack_speed
end

function modifier_bounty_hunter_shuriken_toss_lua:GetModifierPreAttack_BonusDamage()
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int7") then
		return self.attack_speed * self:GetAbility():GetLevel()
	end
end

function modifier_bounty_hunter_shuriken_toss_lua:CustomOnDeath(data)
	if data.attacker~=self:GetParent() then return end
	if data.unit:IsBoss() then
		self:IncrementStackCount()
	end
end

function modifier_bounty_hunter_shuriken_toss_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			return 1
		end
	end
	return 0
end

function modifier_bounty_hunter_shuriken_toss_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_damage", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int11") then
                value = value + self:GetCaster():GetIntellect(true) * 0.5
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_int13") then
                value = value + self:GetCaster():GetIntellect(true) * math.max(self:GetStackCount(), 1) / 100
            end
            return value
		end
	end
	return 0
end