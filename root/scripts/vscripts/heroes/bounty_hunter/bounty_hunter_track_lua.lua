LinkLuaModifier( "modifier_bounty_hunter_track_lua_aura", "heroes/bounty_hunter/bounty_hunter_track_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bounty_hunter_track_lua", "heroes/bounty_hunter/bounty_hunter_track_lua.lua", LUA_MODIFIER_MOTION_NONE )

bounty_hunter_track_lua = class({})

function bounty_hunter_track_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str12") then
		return DOTA_ABILITY_BEHAVIOR_PASSIVE
	end
	return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
end

function bounty_hunter_track_lua:OnSpellStart()
	self:GetCursorTarget():AddNewModifier(self:GetCaster(), self, "modifier_bounty_hunter_track_lua", {duration = self:GetSpecialValueFor("duration")})
end

function bounty_hunter_track_lua:GetIntrinsicModifierName()
	return "modifier_bounty_hunter_track_lua_aura"
end

modifier_bounty_hunter_track_lua_aura = class({})
--Classifications template
function modifier_bounty_hunter_track_lua_aura:IsHidden()
	return true
end

function modifier_bounty_hunter_track_lua_aura:IsDebuff()
	return false
end

function modifier_bounty_hunter_track_lua_aura:IsPurgable()
	return false
end

function modifier_bounty_hunter_track_lua_aura:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_track_lua_aura:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_track_lua_aura:RemoveOnDeath()
	return false
end

function modifier_bounty_hunter_track_lua_aura:DestroyOnExpire()
	return false
end

--------------------------------------------------------------------------------
-- Aura Effects
function modifier_bounty_hunter_track_lua_aura:IsAura()
	return true
end

function modifier_bounty_hunter_track_lua_aura:GetModifierAura()
	return "modifier_bounty_hunter_track_lua"
end

function modifier_bounty_hunter_track_lua_aura:GetAuraRadius()
	return self:GetAbility():GetSpecialValueFor("radius_passive")
end

function modifier_bounty_hunter_track_lua_aura:GetAuraDuration()
	return 5
end

function modifier_bounty_hunter_track_lua_aura:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_bounty_hunter_track_lua_aura:GetAuraSearchType()
	return DOTA_UNIT_TARGET_BASIC
end

function modifier_bounty_hunter_track_lua_aura:GetAuraSearchFlags()
	return 0
end

modifier_bounty_hunter_track_lua = class({})
--Classifications template
function modifier_bounty_hunter_track_lua:IsHidden()
	return false
end

function modifier_bounty_hunter_track_lua:IsDebuff()
	return true
end

function modifier_bounty_hunter_track_lua:IsPurgable()
	return true
end

function modifier_bounty_hunter_track_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_bounty_hunter_track_lua:IsStunDebuff()
	return false
end

function modifier_bounty_hunter_track_lua:RemoveOnDeath()
	return true
end

function modifier_bounty_hunter_track_lua:DestroyOnExpire()
	return true
end

function modifier_bounty_hunter_track_lua:OnCreated()
	self.str11 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str11")
	if not IsServer() then
		return
	end
	local name = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_cast.vpcf", self:GetCaster())
	local nfx = ParticleManager:CreateParticle(name, PATTACH_POINT, self:GetCaster())
	ParticleManager:SetParticleControlEnt(nfx, 0, self:GetCaster(), PATTACH_POINT, "attach_attack2", self:GetCaster():GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(nfx, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	ParticleManager:ReleaseParticleIndex(nfx)

	local name = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_trail.vpcf", self:GetCaster())
	local nfx = ParticleManager:CreateParticle(name, PATTACH_POINT_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControlEnt(nfx, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(nfx, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	self:AddParticle(nfx, false, false, -1, false, false)

	local name = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_shield.vpcf", self:GetCaster())
	local nfx2 = ParticleManager:CreateParticle(name, PATTACH_OVERHEAD_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControlEnt(nfx2, 0, self:GetParent(), PATTACH_OVERHEAD_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	self:AddParticle(nfx2, false, false, -1, false, false)

	self.mod = self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_true_sight", {})

	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str8") then
		self:GetCaster().ActiveJinada = true
		self:GetCaster().pet_locked = true
		self:GetCaster():PerformAttack(self:GetParent(), true, false, true, true, false, false, true)
		self:GetCaster().ActiveJinada = false
		self:GetCaster().pet_locked = false
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str13") then
		self:StartIntervalThink(self:GetCaster():GetSecondsPerAttack(true))
	end
end

function modifier_bounty_hunter_track_lua:OnRefresh()
	if not IsServer() then
		return
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_bounty_hunter_str8") then
		self:GetCaster().ActiveJinada = true
		self:GetCaster().pet_locked = true
		self:GetCaster():PerformAttack(self:GetParent(), true, true, true, true, false, false, true)
		self:GetCaster().ActiveJinada = false
		self:GetCaster().pet_locked = false
	end
end

function modifier_bounty_hunter_track_lua:OnIntervalThink()
	self:GetCaster().ActiveJinada = true
	self:GetCaster().pet_locked = true
	self:GetCaster():PerformAttack(self:GetParent(), true, true, true, true, false, false, true)
	self:GetCaster().ActiveJinada = false
	self:GetCaster().pet_locked = false
end

function modifier_bounty_hunter_track_lua:OnDestroy()
	if not IsServer() then
		return
	end

	if self.mod and not self.mod:IsNull() then
		self.mod:Destroy()
	end
	local bonus_gold = self:GetAbility():GetSpecialValueFor( "bonus_gold" )
	local bonus_gold_self = self:GetAbility():GetSpecialValueFor( "bonus_gold_self" )
	local bonus_gold_radius = self:GetAbility():GetSpecialValueFor( "bonus_gold_radius" )
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, bonus_gold_radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

	for _,unit in pairs(units) do
		if unit ~= self:GetCaster() then
			unit:ModifyGoldFiltered(bonus_gold, true, DOTA_ModifyGold_SharedGold)
			SendOverheadEventMessage( unit, OVERHEAD_ALERT_GOLD, unit, bonus_gold, nil )
		end
	end
	self:GetCaster():ModifyGoldFiltered(bonus_gold_self, true, DOTA_ModifyGold_SharedGold)
	SendOverheadEventMessage( self:GetCaster(), OVERHEAD_ALERT_GOLD, self:GetCaster(), bonus_gold_self, nil )
end

function modifier_bounty_hunter_track_lua:DeclareFunctions()
    return { 
		MODIFIER_PROPERTY_PROVIDES_FOW_POSITION,
		MODIFIER_PROPERTY_PREATTACK_TARGET_CRITICALSTRIKE
	}
end

function modifier_bounty_hunter_track_lua:GetModifierProvidesFOWVision()
	return 1
end

function modifier_bounty_hunter_track_lua:CheckState()
	return {
		[MODIFIER_STATE_PROVIDES_VISION] = true
	}
end

function modifier_bounty_hunter_track_lua:GetModifierPreAttack_Target_CriticalStrike(data)
	return self:GetAbility():GetSpecialValueFor( "crit" )
end