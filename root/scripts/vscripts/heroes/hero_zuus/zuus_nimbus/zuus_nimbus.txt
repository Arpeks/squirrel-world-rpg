"DOTAAbilities"
{
	"zuus_nimbus"
	{
		"BaseClass"                     "ability_lua"
		"ScriptFile"                    "heroes/hero_zuus/zuus_nimbus/zuus_nimbus.lua"
		"AbilityBehavior"               "DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
		"AbilityUnitDamageType"         "DAMAGE_TYPE_MAGICAL"   
		"SpellImmunityType"             "SPELL_IMMUNITY_ENEMIES_NO"
		"MaxLevel"                      "1"
		"AbilityTextureName"            "zuus_cloud"

		"AbilityCastRange"              "3000"
		"AbilityCastPoint"              "0.2"
		"AbilityCastAnimation"          "ACT_DOTA_CAST_ABILITY_4"
		"AbilityCooldown"               "20"

		"AbilityValues"
		{
			"cloud_duration"
			{
				"value"                   "30"
			}
			"cloud_interval"
			{
				"value"                   "2"
			}
			"cloud_radius"
			{
				"value"                   "400"
				"affected_by_aoe_increase" "1"
			}
			"cast_range"
			{
				"value"                   "1000"
			}
		}
	}
}
