LinkLuaModifier( "modifier_zuus_nimbus", "heroes/hero_zuus/zuus_nimbus/zuus_nimbus.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if zuus_nimbus == nil then
	zuus_nimbus = class({})
end
function zuus_nimbus:GetIntrinsicModifierName()
	return "modifier_zuus_nimbus"
end
---------------------------------------------------------------------
--Modifiers
if modifier_zuus_nimbus == nil then
	modifier_zuus_nimbus = class({})
end
function modifier_zuus_nimbus:OnCreated(params)
	if IsServer() then
	end
end
function modifier_zuus_nimbus:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_zuus_nimbus:OnDestroy()
	if IsServer() then
	end
end
function modifier_zuus_nimbus:DeclareFunctions()
	return {
	}
end