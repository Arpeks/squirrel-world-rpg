templar_assassin_psionic_trap_lua = class({})
--мэн модифар, в нем почти вся логика
LinkLuaModifier("modifier_templar_assassin_psionic_trap_lua_controller", "heroes/hero_templar_assassin/templar_assassin_psionic_trap_lua", LUA_MODIFIER_MOTION_NONE)
--чтобы менять иконку абилки
LinkLuaModifier("modifier_templar_assassin_trap_teleport_lua", "heroes/hero_templar_assassin/templar_assassin_psionic_trap_lua", LUA_MODIFIER_MOTION_NONE)
--бесполезный
LinkLuaModifier("modifier_templar_assassin_psionic_trap_lua", "heroes/hero_templar_assassin/templar_assassin_psionic_trap_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_psionic_trap_lua_damage", "heroes/hero_templar_assassin/templar_assassin_psionic_trap_lua", LUA_MODIFIER_MOTION_NONE)

function templar_assassin_psionic_trap_lua:OnUpgrade()
    self:GetCaster():FindAbilityByName("templar_assassin_trap_teleport_lua"):SetLevel(1)
end

function templar_assassin_psionic_trap_lua:OnSpellStart()
    if not self.traps then
        self.traps = {}
    end
    if GridNav:CanFindPath(self:GetCaster():GetOrigin(), self:GetCursorPosition()) == false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        if player then
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", { message = "dota_hud_error_cant_cast_here" })
            self:EndCooldown()
            self:RefundManaCost()
            return
        end
    end
    local mod = self:GetCaster():FindModifierByName("modifier_templar_assassin_psionic_trap_lua")
    if mod then
        if mod:GetStackCount() >= self:GetSpecialValueFor("max_traps") then
            mod:DecrementStackCount()
            local trap = table.remove(self.traps, 1)
            UTIL_Remove(trap)
        end
        local trap = CreateUnitByName("npc_dota_templar_assassin_psionic_trap", self:GetCursorPosition(), false, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
        trap:FindAbilityByName("templar_assassin_self_trap_lua"):SetLevel(1)
        trap:AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_psionic_trap_lua_controller", {})
        -- trap:AddNewModifier(self:GetCaster(), self, "modifier_pips", {pips_count = 5})
        trap:SetControllableByPlayer(self:GetCaster():GetPlayerOwnerID(), true)
        trap:SetOwner(self:GetCaster())
        mod:IncrementStackCount()
        table.insert(self.traps, trap)
        EmitSoundOn("Hero_TemplarAssassin.Trap.Cast", trap)
    end
end

function templar_assassin_psionic_trap_lua:GetIntrinsicModifierName()
    return "modifier_templar_assassin_psionic_trap_lua"
end

templar_assassin_self_trap_lua = class({})

function templar_assassin_self_trap_lua:OnSpellStart()
    local mod = self:GetCaster():FindModifierByName("modifier_templar_assassin_psionic_trap_lua_controller")
    local main_ability = self:GetCaster():FindAbilityByName("templar_assassin_psionic_trap_lua")
    if main_ability then
        for i,trap in pairs(main_ability.traps) do
            if trap == self:GetCaster() then
                table.remove(main_ability.traps, i)
                break
            end
        end
    end
    if mod then
        mod:Destroy()
    end
end

templar_assassin_trap_teleport_lua = class({})

function templar_assassin_trap_teleport_lua:GetAbilityTextureName()
    if self:GetCaster():GetModifierStackCount("modifier_templar_assassin_trap_teleport_lua", self:GetCaster()) == 1 then
        return "templar_assassin_trap_teleport"
    end
    return "templar_assassin_trap"
end

function templar_assassin_trap_teleport_lua:GetIntrinsicModifierName()
    return "modifier_templar_assassin_trap_teleport_lua"
end

function templar_assassin_trap_teleport_lua:GetChannelTime()
    if self:GetCaster():GetModifierStackCount("modifier_templar_assassin_trap_teleport_lua", self:GetCaster()) == 1 then
        return 3
    end
    return 0
end

function templar_assassin_trap_teleport_lua:OnSpellStart()
    if self:GetAutoCastState() then
        return
    end
    local main_ability = self:GetCaster():FindAbilityByName("templar_assassin_psionic_trap_lua")
    if main_ability then
        local position = self:GetCursorPosition()
        local len = 999999
        local to_explode = nil
        local number = 0
        for i,trap in pairs(main_ability.traps) do
            local range = (trap:GetAbsOrigin() - position):Length2D()
            if range < len then
                to_explode = trap
                len = range
                number = i
            end
        end
        if to_explode then
            table.remove(main_ability.traps, number)
            self:GetCaster():FindModifierByName("modifier_templar_assassin_psionic_trap_lua"):DecrementStackCount()
            to_explode:FindModifierByName("modifier_templar_assassin_psionic_trap_lua_controller"):Destroy()
        end
    end
end

function templar_assassin_trap_teleport_lua:OnChannelFinish(bInterrupted)
    if not bInterrupted then
        local main_ability = self:GetCaster():FindAbilityByName("templar_assassin_psionic_trap_lua")
        if main_ability then
            local position = self:GetCursorPosition()
            local len = 999999
            local to_explode = nil
            local number = 0
            for i,trap in pairs(main_ability.traps) do
                local range = (trap:GetAbsOrigin() - position):Length2D()
                if range < len then
                    to_explode = trap
                    len = range
                    number = i
                end
            end
            if to_explode then
                table.remove(main_ability.traps, number)
                if self:GetAutoCastState() then
                    FindClearSpaceForUnit(self:GetCaster(), to_explode:GetAbsOrigin(), true)
                end
                self:GetCaster():FindModifierByName("modifier_templar_assassin_psionic_trap_lua"):DecrementStackCount()
                to_explode:FindModifierByName("modifier_templar_assassin_psionic_trap_lua_controller"):Destroy()
            end
        end
    end
end

modifier_templar_assassin_trap_teleport_lua = class({})
--Classifications template
function modifier_templar_assassin_trap_teleport_lua:IsHidden()
    return true
end

function modifier_templar_assassin_trap_teleport_lua:IsDebuff()
    return false
end

function modifier_templar_assassin_trap_teleport_lua:IsPurgable()
    return false
end

function modifier_templar_assassin_trap_teleport_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_trap_teleport_lua:IsStunDebuff()
    return false
end

function modifier_templar_assassin_trap_teleport_lua:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_trap_teleport_lua:DestroyOnExpire()
    return false
end

function modifier_templar_assassin_trap_teleport_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:SetStackCount(0)
    self:StartIntervalThink(FrameTime())
end

function modifier_templar_assassin_trap_teleport_lua:OnIntervalThink()
    if self:GetAbility():GetAutoCastState() then
        self:SetStackCount(1)
    else
        self:SetStackCount(0)
    end
end

modifier_templar_assassin_psionic_trap_lua = class({})
--Classifications template
function modifier_templar_assassin_psionic_trap_lua:IsHidden()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:IsDebuff()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:IsPurgable()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_psionic_trap_lua:IsStunDebuff()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:DestroyOnExpire()
    return false
end

function modifier_templar_assassin_psionic_trap_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:SetStackCount(0)
end

modifier_templar_assassin_psionic_trap_lua_controller = class({})
--Classifications template
function modifier_templar_assassin_psionic_trap_lua_controller:IsHidden()
    return true
end

function modifier_templar_assassin_psionic_trap_lua_controller:IsDebuff()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_controller:IsPurgable()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_controller:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_psionic_trap_lua_controller:IsStunDebuff()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_controller:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_controller:DestroyOnExpire()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_controller:OnCreated()
    if not IsServer() then
        return
    end
    local pcf = ParticleManager:CreateParticle("particles/econ/items/lanaya/lanaya_epit_trap/templar_assassin_epit_trap.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    self:AddParticle(pcf, false, false, -1, true, false)
end

function modifier_templar_assassin_psionic_trap_lua_controller:OnDestroy()
    if not IsServer() then
        return
    end
    EmitSoundOn("Hero_TemplarAssassin.Trap.Explode", trap)
    local pcf = ParticleManager:CreateParticle("particles/econ/items/lanaya/lanaya_epit_trap/templar_assassin_epit_trap_explode.vpcf", PATTACH_WORLDORIGIN, nil)
    ParticleManager:SetParticleControl(pcf, 0, self:GetParent():GetAbsOrigin())

    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for i,unit in pairs(units) do
        unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_psionic_trap_lua_damage", {duration = self:GetAbility():GetSpecialValueFor("duration")})
    end
    UTIL_Remove(self:GetParent())
end

function modifier_templar_assassin_psionic_trap_lua_controller:CheckState()
    return {
        [MODIFIER_STATE_ROOTED] = true,
        [MODIFIER_STATE_INVISIBLE] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
    }
end

modifier_templar_assassin_psionic_trap_lua_damage = class({})
--Classifications template
function modifier_templar_assassin_psionic_trap_lua_damage:IsHidden()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_damage:IsDebuff()
    return true
end

function modifier_templar_assassin_psionic_trap_lua_damage:IsPurgable()
    return true
end

function modifier_templar_assassin_psionic_trap_lua_damage:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_psionic_trap_lua_damage:IsStunDebuff()
    return false
end

function modifier_templar_assassin_psionic_trap_lua_damage:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_psionic_trap_lua_damage:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_psionic_trap_lua_damage:OnCreated()
    if not IsServer() then
        return
    end
    self.damage_tick_rate = self:GetAbility():GetSpecialValueFor("damage_tick_rate")
    self.trap_bonus_damage = self:GetAbility():GetSpecialValueFor("trap_bonus_damage")
    self:OnIntervalThink()
    self:StartIntervalThink(self.damage_tick_rate)
end

function modifier_templar_assassin_psionic_trap_lua_damage:OnIntervalThink()
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self.trap_bonus_damage * self.damage_tick_rate,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self:GetAbility(),
    })
end