LinkLuaModifier("modifier_templar_assassin_refraction_lua_attack", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_refraction_lua_deffense", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_refraction_lua_permanent", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_bkb", "modifiers/modifier_bkb", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_str10_cooldown", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_refraction_lua_str8", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)

LinkLuaModifier("modifier_templar_assassin_refraction_lua_str6", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)


LinkLuaModifier("modifier_templar_assassin_refraction_lua_str_7_stack_remove", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_refraction_untouchable_debuff", "heroes/hero_templar_assassin/templar_assassin_refraction_lua", LUA_MODIFIER_MOTION_NONE)

templar_assassin_refraction_lua = class({})

function templar_assassin_refraction_lua:GetManaCost(iLevel)
	return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function templar_assassin_refraction_lua:GetIntrinsicModifierName()
    return "modifier_templar_assassin_refraction_lua_permanent"
end

function templar_assassin_refraction_lua:CanMulticast()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str12") then
        return false
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str13") then
        return false
    end
    return true
end

function templar_assassin_refraction_lua:OnSpellStart()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str13") then
        local modifier = self:GetCaster():FindModifierByName("modifier_templar_assassin_refraction_lua_permanent")
        modifier:SetStackCount(modifier:GetStackCount()+10)
    end
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_refraction_lua_attack", {duration = self:GetSpecialValueFor("duration")})
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_refraction_lua_deffense", {duration = self:GetSpecialValueFor("duration")})
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str8") then
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_refraction_lua_str8", {duration = 8})
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str12") then
        self:StartCooldown(self:GetCooldown(self:GetLevel()))
    end
end

function templar_assassin_refraction_lua:GetBehavior()
    if self:GetCaster():HasModifier("modifier_hero_templar_assassin_buff_1") then
        return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE + DOTA_ABILITY_BEHAVIOR_AUTOCAST
    end
    return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
end

modifier_templar_assassin_refraction_lua_attack = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_attack:GetTexture()
    return "templar_assassin_refraction_damage"
end

function modifier_templar_assassin_refraction_lua_attack:IsHidden()
    return false
end

function modifier_templar_assassin_refraction_lua_attack:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_attack:IsPurgable()
    return true
end

function modifier_templar_assassin_refraction_lua_attack:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_attack:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_attack:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_refraction_lua_attack:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_refraction_lua_attack:OnStackCountChanged(last_stack_count)
    if not IsServer() then
        return
    end
    if last_stack_count > self:GetStackCount() then
        if self:GetStackCount() == 0 then
            self:Destroy()
        end
    end
end

function modifier_templar_assassin_refraction_lua_attack:OnCreated()
    self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
    if not IsServer() then
        return
    end
    local count = self:GetAbility():GetSpecialValueFor("instances")
    self:SetStackCount(count)

    self.permanent_damage_modifer = self:GetParent():FindModifierByName("modifier_templar_assassin_refraction_lua_permanent")
end

function modifier_templar_assassin_refraction_lua_attack:OnRefresh()
	self:OnCreated()
end

function modifier_templar_assassin_refraction_lua_attack:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED
	}
end

function modifier_templar_assassin_refraction_lua_attack:GetModifierPreAttack_BonusDamage()
    if self:GetStackCount() > 0 then
	    return self.bonus_damage
    end
end

function modifier_templar_assassin_refraction_lua_attack:CustomOnAttackLanded(data)
	if data.attacker == self:GetParent() and data.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() and self:GetStackCount() > 0 then
		data.target:EmitSound("Hero_TemplarAssassin.Refraction.Damage")
		self:DecrementStackCount()
	end
end

modifier_templar_assassin_refraction_lua_deffense = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_deffense:GetTexture()
    return "templar_assassin_refraction"
end

function modifier_templar_assassin_refraction_lua_deffense:IsHidden()
    return false
end

function modifier_templar_assassin_refraction_lua_deffense:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_deffense:IsPurgable()
    return true
end

function modifier_templar_assassin_refraction_lua_deffense:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_deffense:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_deffense:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_refraction_lua_deffense:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_refraction_lua_deffense:OnStackCountChanged(last_stack_count)
    if not IsServer() then
        return
    end
    if last_stack_count > self:GetStackCount() then
        if self:GetStackCount() == 0 then
            self:Destroy()
        end
    end
end

function modifier_templar_assassin_refraction_lua_deffense:OnCreated()
    if not IsServer() then
        return
    end
    local count = self:GetAbility():GetSpecialValueFor("instances")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str7") then
        count = math.min(count, 12)
    end
    self:SetStackCount(count)
    if self.refraction_particle then
        ParticleManager:DestroyParticle(self.refraction_particle, false)
        ParticleManager:ReleaseParticleIndex(self.refraction_particle)
    end
    self.special_bonus_unique_npc_dota_hero_templar_assassin_str7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str7")
    self.damage_modifier = self:GetParent():FindModifierByName("modifier_templar_assassin_refraction_lua_attack")
    self.refraction_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_templar_assassin/templar_assassin_refraction.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
	ParticleManager:SetParticleControlEnt(self.refraction_particle, 1, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	self:AddParticle(self.refraction_particle, false, false, -1, true, false)
end

function modifier_templar_assassin_refraction_lua_deffense:OnRefresh()
	self:OnCreated()
end

function modifier_templar_assassin_refraction_lua_deffense:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE
	}
end

function modifier_templar_assassin_refraction_lua_deffense:GetAbsoluteNoDamagePhysical()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str9") then
        return 0
    end
    if self:GetStackCount() > 0 then
	    return 1
    end
end

function modifier_templar_assassin_refraction_lua_deffense:GetAbsoluteNoDamageMagical()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str9") then
        return 0
    end
    if self:GetStackCount() > 0 then
	    return 1
    end
end

function modifier_templar_assassin_refraction_lua_deffense:GetAbsoluteNoDamagePure(data)
    if self:GetStackCount() > 0 then
		self:GetParent():EmitSound("Hero_TemplarAssassin.Refraction.Absorb")
		
		local warp_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_templar_assassin/templar_assassin_refract_plasma_contact_warp.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:ReleaseParticleIndex(warp_particle)
		
		local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_templar_assassin/templar_assassin_refract_hit.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControlEnt(hit_particle, 2, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
		ParticleManager:ReleaseParticleIndex(hit_particle)
		
        if self.special_bonus_unique_npc_dota_hero_templar_assassin_str7 then
            if not self:GetCaster():HasModifier("modifier_templar_assassin_refraction_lua_str_7_stack_remove") then
                local mod = self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_refraction_lua_str_7_stack_remove", {duration = 0.3})
                mod.main_modifier = self
            end
        else
		    self:DecrementStackCount()
        end
        -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str12") then
        --     ApplyDamageRDA({
        --         victim = data.attacker,
        --         attacker = self:GetCaster(),
        --         damage = self:GetCaster():GetStrength() * (self:GetAbility().strDamagePerc / 100),
        --         damage_type = DAMAGE_TYPE_PURE,
        --         ability = self:GetAbility(),
        --         damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        --         damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.10
        --     })
        -- end
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str9") then
            return 0
        end
		return 1
	end
end

modifier_templar_assassin_refraction_lua_permanent = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_permanent:IsHidden()
    return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str13") == nil
end

function modifier_templar_assassin_refraction_lua_permanent:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_permanent:IsPurgable()
    return false
end

function modifier_templar_assassin_refraction_lua_permanent:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_permanent:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_permanent:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_refraction_lua_permanent:DestroyOnExpire()
    return false
end

function modifier_templar_assassin_refraction_lua_permanent:OnCreated( kv )
    if not IsServer() then return end
    self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_refraction_lua_str6", {})
    self:SetStackCount(250)
    self:StartIntervalThink(0.2)
end

function modifier_templar_assassin_refraction_lua_permanent:OnIntervalThink()
    if self:GetCaster():HasModifier("modifier_hero_templar_assassin_buff_1") and self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() and self:GetCaster():IsAlive() and not self:GetCaster():IsSilenced() and not  self:GetCaster():IsStunned() then
        self:GetAbility():UseResources(false, false, false, true)
        self:GetAbility():OnSpellStart()
    end
end

function modifier_templar_assassin_refraction_lua_permanent:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        -- MODIFIER_EVENT_ON_TAKEDAMAGE,
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
        -- MODIFIER_EVENT_ON_ATTACK_START,
    }
end

function modifier_templar_assassin_refraction_lua_permanent:CustomOnAttackStart( params )
	if IsServer() then
		if params.target~=self:GetParent() then return end
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str13") then
            params.attacker:AddNewModifier(
                self:GetParent(),
                self:GetAbility(),
                "modifier_templar_assassin_refraction_untouchable_debuff",
                nil
            )
        end
	end
end

function modifier_templar_assassin_refraction_lua_permanent:GetModifierIncomingDamage_Percentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str9") then
	    return -35
    end
    return 0
end

function modifier_templar_assassin_refraction_lua_permanent:GetModifierPreAttack_BonusDamage()
    return 0
end

function modifier_templar_assassin_refraction_lua_permanent:CustomOnTakeDamage(data)
    if data.unit == self:GetParent() then
        if self:GetCaster():GetHealthPercent() < 25 and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str10") and not self:GetParent():FindModifierByName("modifier_templar_assassin_str10_cooldown") then
            self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_refraction_lua_deffense", {duration = self:GetAbility():GetSpecialValueFor("duration")})
            self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_bkb", {duration=3})
            self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_str10_cooldown", {duration=160})
        end
        if data.damage_category == DOTA_DAMAGE_CATEGORY_ATTACK and data.damage_type == DAMAGE_TYPE_PHYSICAL then
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str12") then
                ApplyDamageRDA({
                    victim = data.attacker,
                    attacker = self:GetCaster(),
                    damage = self:GetCaster():GetStrength() * (self:GetStackCount() / 100),
                    damage_type = DAMAGE_TYPE_PURE,
                    ability = self:GetAbility(),
                    damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
                })
            end
        end
    end
end

modifier_templar_assassin_refraction_lua_str8 = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_str8:IsHidden()
    return false
end

function modifier_templar_assassin_refraction_lua_str8:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str8:IsPurgable()
    return false
end

function modifier_templar_assassin_refraction_lua_str8:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_str8:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str8:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_refraction_lua_str8:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_refraction_lua_str8:OnCreated()
    self.agility = self:GetParent():GetAgility()
    self.strength = self:GetParent():GetStrength()
    self.intellect = self:GetParent():GetIntellect(true)
    if not IsServer() then
        return
    end
    local count = self:GetAbility():GetSpecialValueFor("instances")
    self:SetStackCount(count)
end

function modifier_templar_assassin_refraction_lua_str8:OnRefresh()
	self:OnCreated()
end


function modifier_templar_assassin_refraction_lua_str8:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
    }
end

function modifier_templar_assassin_refraction_lua_str8:GetModifierBonusStats_Strength()
    if self.strength then
        return self.strength * self:GetStackCount() * 0.025
    end
end

function modifier_templar_assassin_refraction_lua_str8:GetModifierBonusStats_Agility()
    if self.agility then
        return self.agility * self:GetStackCount() * 0.025
    end
end

function modifier_templar_assassin_refraction_lua_str8:GetModifierBonusStats_Intellect()
    if self.intellect then
        return self.intellect * self:GetStackCount() * 0.025
    end
end

function modifier_templar_assassin_refraction_lua_str8:GetModifierIncomingDamage_Percentage()
	if self:GetCaster():FindModifierByName("special_bonus_unique_npc_dota_hero_templar_assassin_str11") then
        return -25
    end
	return 0
end

modifier_templar_assassin_str10_cooldown = class({})
--Classifications template
function modifier_templar_assassin_str10_cooldown:IsHidden()
    return false
end

function modifier_templar_assassin_str10_cooldown:IsDebuff()
    return false
end

function modifier_templar_assassin_str10_cooldown:IsPurgable()
    return false
end

function modifier_templar_assassin_str10_cooldown:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_str10_cooldown:IsStunDebuff()
    return false
end

function modifier_templar_assassin_str10_cooldown:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_str10_cooldown:DestroyOnExpire()
    return true
end

modifier_templar_assassin_refraction_lua_str_7_stack_remove = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_str_7_stack_remove:IsHidden()
    return true
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:IsPurgable()
    return false
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_str_7_stack_remove:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_refraction_lua_str_7_stack_remove:OnDestroy()
    if not IsServer() then
        return
    end
    if self.main_modifier then
        self.main_modifier:DecrementStackCount()
    end
end

modifier_templar_assassin_refraction_lua_str6 = class({})
--Classifications template
function modifier_templar_assassin_refraction_lua_str6:IsHidden()
    return self:GetStackCount() == 0
end

function modifier_templar_assassin_refraction_lua_str6:IsDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str6:IsPurgable()
    return false
end

function modifier_templar_assassin_refraction_lua_str6:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_refraction_lua_str6:IsStunDebuff()
    return false
end

function modifier_templar_assassin_refraction_lua_str6:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_refraction_lua_str6:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_refraction_lua_str6:OnCreated()
    self.parent = self:GetParent()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(0.2)
end

function modifier_templar_assassin_refraction_lua_str6:OnIntervalThink()
    if self.parent:FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_str6") then
        local bonus = (self.parent:GetStrength() - self:GetStackCount()) * 0.15
        self:SetStackCount(bonus)
    else
        self:SetStackCount(0)
    end
end

function modifier_templar_assassin_refraction_lua_str6:OnRefresh()
	self:OnCreated()
end


function modifier_templar_assassin_refraction_lua_str6:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
    }
end

function modifier_templar_assassin_refraction_lua_str6:GetModifierBonusStats_Strength()
    return self:GetStackCount()
end

modifier_templar_assassin_refraction_untouchable_debuff = {}

function modifier_templar_assassin_refraction_untouchable_debuff:IsHidden()
	return true
end

function modifier_templar_assassin_refraction_untouchable_debuff:IsDebuff()
	return true
end

function modifier_templar_assassin_refraction_untouchable_debuff:IsStunDebuff()
	return false
end

function modifier_templar_assassin_refraction_untouchable_debuff:IsPurgable()
	return false
end

function modifier_templar_assassin_refraction_untouchable_debuff:OnCreated( kv )
	self.slow = 80 * self:GetAbility():GetLevel()
	self.duration = 5
end

function modifier_templar_assassin_refraction_untouchable_debuff:OnRefresh( kv )
	self.slow = 80 * self:GetAbility():GetLevel()
	self.duration = 5
end

function modifier_templar_assassin_refraction_untouchable_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PRE_ATTACK,
		-- MODIFIER_EVENT_ON_ATTACK,
		-- MODIFIER_EVENT_ON_ATTACK_FINISHED,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_templar_assassin_refraction_untouchable_debuff:GetModifierPreAttack( params )
	if IsServer() then
		if not self.HasAttacked then
			self.record = params.record
		end

		if params.target~=self:GetCaster() then
			self.attackOther = true
		end
	end
end

function modifier_templar_assassin_refraction_untouchable_debuff:CustomOnAttack( params )
	if IsServer() then
		if params.record~=self.record then return end

		self:SetDuration(self.duration, true)
		self.HasAttacked = true
	end
end

function modifier_templar_assassin_refraction_untouchable_debuff:CustomOnAttackFinished( params )
	if IsServer() then
		if params.attacker~=self:GetParent() then return end
		
		if not self.HasAttacked then
			self:Destroy()
		end

		if self.attackOther then
			self:Destroy()
		end
	end
end

function modifier_templar_assassin_refraction_untouchable_debuff:GetModifierAttackSpeedBonus_Constant()
	if IsServer() then
		if self:GetParent():GetAggroTarget()==self:GetCaster() then
			return self.slow
		else
			return 0
		end
	end

	return self.slow
end