LinkLuaModifier("modifier_templar_assassin_meld_lua", "heroes/hero_templar_assassin/templar_assassin_meld_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_meld_lua_orb", "heroes/hero_templar_assassin/templar_assassin_meld_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_meld_lua_debuff", "heroes/hero_templar_assassin/templar_assassin_meld_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_meld_lua_burn", "heroes/hero_templar_assassin/templar_assassin_meld_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_meld_lua_instic", "heroes/hero_templar_assassin/templar_assassin_meld_lua", LUA_MODIFIER_MOTION_NONE)

templar_assassin_meld_lua = class({})

function templar_assassin_meld_lua:GetManaCost(iLevel)
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function templar_assassin_meld_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int9") then
        return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_AUTOCAST + DOTA_ABILITY_BEHAVIOR_ATTACK
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int7") then
        return DOTA_ABILITY_BEHAVIOR_TOGGLE + DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
    end
    return DOTA_ABILITY_BEHAVIOR_NO_TARGET
end

function templar_assassin_meld_lua:OnAbilityUpgrade(hUpgradeAbility)
    if hUpgradeAbility:GetAbilityName() == "special_bonus_unique_npc_dota_hero_templar_assassin_int9" then
        if self:GetAutoCastState() then
            self:ToggleAutoCast()
        end
        if self:GetToggleState() then
            self:ToggleAbility()
        end
        self:RefreshIntrinsicModifier()
    end
    if hUpgradeAbility:GetAbilityName() == "special_bonus_unique_npc_dota_hero_templar_assassin_int7" then
        if self:GetAutoCastState() then
            self:ToggleAutoCast()
        end
        if self:GetToggleState() then
            self:ToggleAbility()
        end
        self:RefreshIntrinsicModifier()
    end
    if hUpgradeAbility:GetAbilityName() == "special_bonus_unique_npc_dota_hero_templar_assassin_int12" then
        if self:GetAutoCastState() then
            self:ToggleAutoCast()
        end
        if self:GetToggleState() then
            self:ToggleAbility()
        end
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_meld_lua_int12", {})
    end
end

function templar_assassin_meld_lua:GetIntrinsicModifierName()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int9") then
        return "modifier_templar_assassin_meld_lua_orb"
    end
    return
end

function templar_assassin_meld_lua:GetCastRange(vLocation, hTarget)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int9") then
        return self:GetCaster():Script_GetAttackRange() + 50
    end
    return 0
end

function templar_assassin_meld_lua:OnOrbFire( params )
	-- EmitSoundOn( "Hero_TemplarAssassin.Meld.Attack", self:GetCaster() )
    -- self:UseResources(true, false, false, true)
end

function templar_assassin_meld_lua:OnOrbImpact( data )
    EmitSoundOn( "Hero_TemplarAssassin.Meld.Attack", data.target )
    -- local damage = data.attacker:GetAverageTrueAttackDamage(data.target) + self:GetSpecialValueFor("bonus_damage")
    -- damage = damage + damage * ((self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility() + self:GetCaster():GetStrength()) * 0.1 * 0.01)
    -- ApplyDamage({
    --     victim = data.target,
    --     attacker = data.attacker,
    --     damage = damage,
    --     damage_type = DAMAGE_TYPE_PHYSICAL,
    --     damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
    --     ability = self
    -- })
    data.target:AddNewModifier(data.attacker, self, "modifier_templar_assassin_meld_lua_debuff", {duration = self:GetSpecialValueFor("duration")})
end

function templar_assassin_meld_lua:OnSpellStart()
    self:GetCaster():Hold()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_meld_lua", {})
end

function templar_assassin_meld_lua:OnToggle()
    -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int11") then
    --     self:GetCaster():Purge(false, true, false, true, true)
    --     self:StartCooldown(10)
    -- end
    if self:GetToggleState() then
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_meld_lua_instic", {})
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int12") then
            self:EndCooldown()
        end
    else
        self:GetCaster():RemoveModifierByName("modifier_templar_assassin_meld_lua_instic")
    end
end

modifier_templar_assassin_meld_lua = class({})
--Classifications template
function modifier_templar_assassin_meld_lua:IsHidden()
    return false
end

function modifier_templar_assassin_meld_lua:IsDebuff()
    return false
end

function modifier_templar_assassin_meld_lua:IsPurgable()
    return false
end

function modifier_templar_assassin_meld_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_meld_lua:IsStunDebuff()
    return false
end

function modifier_templar_assassin_meld_lua:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_meld_lua:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_meld_lua:OnCreated()
    self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
    self.duration = self:GetAbility():GetSpecialValueFor("duration")
    if not IsServer() then
        return
    end
    self:StartIntervalThink(0.1)
end

function modifier_templar_assassin_meld_lua:OnIntervalThink()
    if self:GetParent():IsMoving() then
        self:Destroy()
    end
end

function modifier_templar_assassin_meld_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
        -- MODIFIER_EVENT_ON_ATTACK,
        MODIFIER_PROPERTY_PROJECTILE_NAME,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
    }
end

function modifier_templar_assassin_meld_lua:GetModifierPreAttack_BonusDamage()
    return self.bonus_damage
end

function modifier_templar_assassin_meld_lua:CheckState()
    return {
        [MODIFIER_STATE_INVISIBLE] = true
    }
end

function modifier_templar_assassin_meld_lua:GetActivityTranslationModifiers()
    return "meld"
end

function modifier_templar_assassin_meld_lua:CustomOnAttack(data)
	if data.attacker == self:GetParent() and not data.no_attack_cooldown and not self.record then
        self.record = data.record
	end
end

function modifier_templar_assassin_meld_lua:CustomOnAttackLanded(data)
	if data.attacker == self:GetParent() and ((self.record == data.record) or self.special_bonus_unique_npc_dota_hero_templar_assassin_int10) then
        data.target:EmitSound("Hero_TemplarAssassin.Meld.Attack")
        data.target:AddNewModifier(data.attacker, self:GetAbility(), "modifier_templar_assassin_meld_lua_debuff", {duration = self:GetAbility():GetSpecialValueFor("duration")})
        if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int8") then
            if self:GetDuration() == -1 then
                self:SetDuration(2, true)
            end
        else
            self:Destroy()
        end
	end
end

function modifier_templar_assassin_meld_lua:GetModifierProjectileName()
    if (not self.record) or self.special_bonus_unique_npc_dota_hero_templar_assassin_int10 then
	    return "particles/units/heroes/hero_templar_assassin/templar_assassin_meld_attack.vpcf"
    end
end

function modifier_templar_assassin_meld_lua:CustomOnAttack(data)
	if data.attacker == self:GetParent() and not data.no_attack_cooldown and not self.record then
        self.record = data.record
	end
end

function modifier_templar_assassin_meld_lua:GetModifierInvisibilityLevel()
	return 1
end

modifier_templar_assassin_meld_lua_debuff = class({})
--Classifications template
function modifier_templar_assassin_meld_lua_debuff:IsHidden()
    return false
end

function modifier_templar_assassin_meld_lua_debuff:IsDebuff()
    return true
end

function modifier_templar_assassin_meld_lua_debuff:IsPurgable()
    return true
end

function modifier_templar_assassin_meld_lua_debuff:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_meld_lua_debuff:IsStunDebuff()
    return false
end

function modifier_templar_assassin_meld_lua_debuff:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_meld_lua_debuff:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_meld_lua_debuff:OnCreated()
    self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
    self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
    if not IsServer() then
        return
    end
end

function modifier_templar_assassin_meld_lua_debuff:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
    }
end

function modifier_templar_assassin_meld_lua_debuff:GetModifierPhysicalArmorBonus(data)
    local bonus_armor = self.bonus_armor
    if data.attacker == self:GetCaster() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int11") then
        bonus_armor = bonus_armor + (self:GetParent():GetPhysicalArmorValue(false) * 0.2)
    end
    return bonus_armor
end


modifier_templar_assassin_meld_lua_burn = class({})
--Classifications template
function modifier_templar_assassin_meld_lua_burn:IsHidden()
    return false
end

function modifier_templar_assassin_meld_lua_burn:IsDebuff()
    return true
end

function modifier_templar_assassin_meld_lua_burn:IsPurgable()
    return true
end

function modifier_templar_assassin_meld_lua_burn:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_meld_lua_burn:IsStunDebuff()
    return false
end

function modifier_templar_assassin_meld_lua_burn:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_meld_lua_burn:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_meld_lua_burn:OnCreated(kv)
    self.damage = kv.damage
    if not IsServer() then
        return
    end
    if self:GetParent():IsBuilding() then return end
    self:StartIntervalThink(0.5)
end

function modifier_templar_assassin_meld_lua_burn:OnRefresh(kv)
    self.damage = kv.damage
end

function modifier_templar_assassin_meld_lua_burn:OnIntervalThink()
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self.damage * 0.5,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        ability = self:GetAbility(),
        damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.5
    })
    self:StartIntervalThink(0.5)
end

modifier_templar_assassin_meld_lua_orb = class({})

function modifier_templar_assassin_meld_lua_orb:IsHidden()
    return true
end

function modifier_templar_assassin_meld_lua_orb:IsDebuff()
    return false
end

function modifier_templar_assassin_meld_lua_orb:IsPurgable()
    return false
end

function modifier_templar_assassin_meld_lua_orb:GetAttributes()
    return MODIFIER_ATTRIBUTE_PERMANENT
end

function modifier_templar_assassin_meld_lua_orb:OnCreated( kv )
    self.ability = self:GetAbility()
    self.cast = false
    self.records = {}
end

function modifier_templar_assassin_meld_lua_orb:DeclareFunctions()
    local funcs = {
        -- MODIFIER_EVENT_ON_ATTACK_FAIL,
        MODIFIER_PROPERTY_PROCATTACK_FEEDBACK,
        -- MODIFIER_EVENT_ON_ATTACK_RECORD_DESTROY,
        -- MODIFIER_EVENT_ON_ORDER,
        MODIFIER_PROPERTY_PROJECTILE_NAME,
        -- MODIFIER_EVENT_ON_ATTACK,
        -- MODIFIER_EVENT_ON_ATTACKED,
        MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
    }

    return funcs
end

function modifier_templar_assassin_meld_lua_orb:GetModifierPreAttack_BonusDamage()
    if not IsServer() then return end
    if self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() then
        return self:GetAbility():GetSpecialValueFor("bonus_damage")
    end
end

function modifier_templar_assassin_meld_lua_orb:GetModifierPreAttack_CriticalStrike(data)
    if self:ShouldLaunch( data.target ) and self:GetAbility():IsFullyCastable() then
        return 100 + (self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility() + self:GetCaster():GetStrength()) * 0.1
    end
end

function modifier_templar_assassin_meld_lua_orb:CustomOnAttack( params )
    if params.attacker~=self:GetParent() then return end
    if self:ShouldLaunch( params.target ) and self:GetAbility():IsFullyCastable() then
        self.ability:UseResources( true, false, false, true )
        self.records[params.record] = true
        if self.ability.OnOrbFire then self.ability:OnOrbFire( params ) end
    end

    self.cast = false
end

function modifier_templar_assassin_meld_lua_orb:CustomOnAttacked( params )
    
end

function modifier_templar_assassin_meld_lua_orb:GetModifierProcAttack_Feedback( params )
    if self.records[params.record] then
        if self.ability.OnOrbImpact then self.ability:OnOrbImpact( params ) end
    end
end
function modifier_templar_assassin_meld_lua_orb:CustomOnAttackFail( params )
    if self.records[params.record] then
        if self.ability.OnOrbFail then self.ability:OnOrbFail( params ) end
    end
end
function modifier_templar_assassin_meld_lua_orb:CustomOnAttackRecordDestroy( params )
    self.records[params.record] = nil
end

function modifier_templar_assassin_meld_lua_orb:CustomOnOrder( params )
    if params.unit~=self:GetParent() then return end

    if params.ability then
        if params.ability==self:GetAbility() then
            self.cast = true
            return
        end
        local pass = false
        local behavior = params.ability:GetBehaviorInt()
        if self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_CHANNEL ) or 
            self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_MOVEMENT ) or
            self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_IGNORE_CHANNEL )
        then
            local pass = true -- do nothing
        end

        if self.cast and (not pass) then
            self.cast = false
        end
    else
        if self.cast then
            if self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_POSITION ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_TARGET ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_MOVE ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_TARGET ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_STOP ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_HOLD_POSITION )
            then
                self.cast = false
            end
        end
    end
end

function modifier_templar_assassin_meld_lua_orb:GetModifierProjectileName()
    if self:ShouldLaunch( self:GetCaster():GetAggroTarget() ) and self:GetAbility():IsFullyCastable() then
        return "particles/units/heroes/hero_templar_assassin/templar_assassin_meld_attack.vpcf"
    end
end

function modifier_templar_assassin_meld_lua_orb:ShouldLaunch( target )
    if self.ability:GetAutoCastState() then
        if self.ability.CastFilterResultTarget~=CDOTA_Ability_Lua.CastFilterResultTarget then
            if self.ability:CastFilterResultTarget( target )==UF_SUCCESS then
                self.cast = true
            end
        else
            local nResult = UnitFilter(
                target,
                self.ability:GetAbilityTargetTeam(),
                self.ability:GetAbilityTargetType(),
                self.ability:GetAbilityTargetFlags(),
                self:GetCaster():GetTeamNumber()
            )
            if nResult == UF_SUCCESS then
                self.cast = true
            end
        end
    end

    if self.cast then
        return true
    end

    return false
end

function modifier_templar_assassin_meld_lua_orb:FlagExist(a,b)
    local p,c,d=1,0,b
    while a>0 and b>0 do
        local ra,rb=a%2,b%2
        if ra+rb>1 then c=c+p end
        a,b,p=(a-ra)/2,(b-rb)/2,p*2
    end
    return c==d
end

modifier_templar_assassin_meld_lua_instic = class({})
--Classifications template
function modifier_templar_assassin_meld_lua_instic:IsHidden()
    return true
end

function modifier_templar_assassin_meld_lua_instic:IsDebuff()
    return false
end

function modifier_templar_assassin_meld_lua_instic:IsPurgable()
    return false
end

function modifier_templar_assassin_meld_lua_instic:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_meld_lua_instic:IsStunDebuff()
    return false
end

function modifier_templar_assassin_meld_lua_instic:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_meld_lua_instic:DestroyOnExpire()
    return false
end


function modifier_templar_assassin_meld_lua_instic:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_PROJECTILE_NAME,
        MODIFIER_PROPERTY_OVERRIDE_ATTACK_MAGICAL,
        MODIFIER_PROPERTY_OVERRIDE_ATTACK_DAMAGE,
        MODIFIER_PROPERTY_PROCATTACK_BONUS_DAMAGE_MAGICAL,
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE,
        MODIFIER_PROPERTY_SUPPRESS_CRIT,
    }
    return funcs
end

function modifier_templar_assassin_meld_lua_instic:OnCreated( kv )
    if not IsServer() then return end
    self:OnIntervalThink()
    self:StartIntervalThink(1)
end

function modifier_templar_assassin_meld_lua_instic:OnIntervalThink()
    local manacost = 10 + math.min(65000, self:GetCaster():GetIntellect(true) / 300)
    if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int12") then
        manacost = 0
    end
    if self:GetCaster():GetMana() >= manacost then
        self:GetCaster():SpendMana(manacost, self:GetAbility())
    else
        self:GetAbility():ToggleAbility()
        self:Destroy()
    end
end

function modifier_templar_assassin_meld_lua_instic:GetModifierOverrideAttackDamage()
	return 0
end

function modifier_templar_assassin_meld_lua_instic:GetSuppressCrit()
	return 1
end

function modifier_templar_assassin_meld_lua_instic:CustomOnTakeDamage( params )
	if IsServer() then
        if params.attacker ~= self:GetParent() then return end
		if self:GetParent():GetTeamNumber() == params.unit:GetTeamNumber() then return end

        if params.damage_category == DOTA_DAMAGE_CATEGORY_ATTACK then
			if params.damage_flags == DOTA_DAMAGE_FLAG_REFLECTION + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION then return end
            if params.no_attack_cooldown or not self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int12") then return end
            if params.unit:IsBuilding() then return end
            local damageTable = {
                victim = params.unit,
				attacker = params.attacker,
				damage =  self.damage,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = DOTA_DAMAGE_FLAG_REFLECTION + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
            }
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int13") then
                damageTable.damage_amplification = 1 + self:GetCaster():GetSpellAmplification(false) * 0.15
            end
			ApplyDamageRDA(damageTable)
			EmitSoundOn( "Hero_Muerta.PierceTheVeil.ProjectileImpact", params.unit )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_int10") then
                params.unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_templar_assassin_meld_lua_burn", {duration = 3, damage = self.damage * 2})
            end
		end
	end
	return 0
end

function modifier_templar_assassin_meld_lua_instic:GetModifierTotalDamageOutgoing_Percentage( params )
	if params.inflictor then return 0 end
	if params.damage_category~=DOTA_DAMAGE_CATEGORY_ATTACK then return 0 end
	if params.damage_type~=DAMAGE_TYPE_PHYSICAL then return 0 end
	if not params.target:IsMagicImmune() then
		self.damage = 0
		self.damage = params.original_damage
	else
		EmitSoundOn( "Hero_Muerta.PierceTheVeil.ProjectileImpact.MagicImmune", params.target )
	end
	return -200
end

function modifier_templar_assassin_meld_lua_instic:GetOverrideAttackMagical()
    return 1
end

function modifier_templar_assassin_meld_lua_instic:GetModifierProjectileName()
    return "particles/units/heroes/hero_templar_assassin/templar_assassin_meld_attack.vpcf"
end

function modifier_templar_assassin_meld_lua_instic:GetEffectName()
    return "particles/items5_fx/revenant_brooch.vpcf"
end

function modifier_templar_assassin_meld_lua_instic:CheckState()
    return {
        [MODIFIER_STATE_CANNOT_TARGET_BUILDINGS] = true
    }
end
