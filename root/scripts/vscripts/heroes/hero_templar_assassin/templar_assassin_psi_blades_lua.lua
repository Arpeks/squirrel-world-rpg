LinkLuaModifier("modifier_templar_assassin_psi_blades_lua", "heroes/hero_templar_assassin/templar_assassin_psi_blades_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_templar_assassin_psi_blades_lua_agi12", "heroes/hero_templar_assassin/templar_assassin_psi_blades_lua", LUA_MODIFIER_MOTION_NONE)

templar_assassin_psi_blades_lua = class({})

function templar_assassin_psi_blades_lua:GetIntrinsicModifierName()
    return "modifier_templar_assassin_psi_blades_lua"
end

function templar_assassin_psi_blades_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi12") then
        return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_TOGGLE + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
    end
    return DOTA_ABILITY_BEHAVIOR_PASSIVE
end

function templar_assassin_psi_blades_lua:OnToggle()
    if self:GetToggleState() then
        self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_templar_assassin_psi_blades_lua_agi12", {})
    else
        self:GetCaster():RemoveModifierByName("modifier_templar_assassin_psi_blades_lua_agi12")
    end
end

modifier_templar_assassin_psi_blades_lua = class({})
--Classifications template
function modifier_templar_assassin_psi_blades_lua:IsHidden()
    return true
end

function modifier_templar_assassin_psi_blades_lua:IsDebuff()
    return false
end

function modifier_templar_assassin_psi_blades_lua:IsPurgable()
    return false
end

function modifier_templar_assassin_psi_blades_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_psi_blades_lua:IsStunDebuff()
    return false
end

function modifier_templar_assassin_psi_blades_lua:RemoveOnDeath()
    return false
end

function modifier_templar_assassin_psi_blades_lua:DestroyOnExpire()
    return false
end

function modifier_templar_assassin_psi_blades_lua:OnCreated()
    self.attack_spill_range = self:GetAbility():GetSpecialValueFor("attack_spill_range")
    self.attack_spill_width = self:GetAbility():GetSpecialValueFor("attack_spill_width")
    self.attack_spill_pct = self:GetAbility():GetSpecialValueFor("attack_spill_pct") * 0.01
    self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range")
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi10") then
        self.attack_spill_width = self:GetAbility():GetSpecialValueFor("attack_spill_width") + 100
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi7") then
        self.attack_spill_pct = 100 * 0.01
    end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi8") then
        self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range") + self:GetAbility():GetLevel() * 10
    end
end

function modifier_templar_assassin_psi_blades_lua:OnRefresh()
    self:OnCreated()
end



function modifier_templar_assassin_psi_blades_lua:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
    }
end

function modifier_templar_assassin_psi_blades_lua:GetModifierDamageOutgoing_Percentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi11") then
        return self:GetCaster():Script_GetAttackRange() * 0.04
    end
end

function modifier_templar_assassin_psi_blades_lua:CustomOnAttackLanded(data)
    if data.attacker == self:GetParent() and not data.no_attack_cooldown then
        local limit = 999
        local end_point = data.target:GetAbsOrigin() + (data.target:GetAbsOrigin() - self:GetParent():GetAbsOrigin()):Normalized() * self.attack_spill_range
        local units = FindUnitsInLine(self:GetCaster():GetTeamNumber(), data.target:GetAbsOrigin(), end_point, nil, self.attack_spill_width, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0)
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi9") then
            units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), data.target:GetAbsOrigin(), nil, 400, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false)
            limit = 2
        end
        for _, enemy in pairs(units) do
			if enemy ~= data.target and limit > 0 then
                limit = limit - 1
				enemy:EmitSound("Hero_TemplarAssassin.PsiBlade")
				self.psi_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_templar_assassin/templar_assassin_psi_blade.vpcf", PATTACH_ABSORIGIN_FOLLOW, data.target)
				ParticleManager:SetParticleControlEnt(self.psi_particle, 0, data.unit, PATTACH_POINT_FOLLOW, "attach_hitloc", data.target:GetAbsOrigin(), true)
				ParticleManager:SetParticleControlEnt(self.psi_particle, 1, enemy, PATTACH_POINT_FOLLOW, "attach_hitloc", enemy:GetAbsOrigin(), true)
				ParticleManager:ReleaseParticleIndex(self.psi_particle)
                ApplyDamageRDA({
                    attacker 		= self:GetParent(),
                    victim 			= enemy,
                    damage 			= data.damage * self.attack_spill_pct,
                    damage_type		= self:GetAbility():GetAbilityDamageType(),
                    damage_flags 	= DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR,
                    ability 		= self:GetAbility()
                })
            end
		end
    end
end

function modifier_templar_assassin_psi_blades_lua:GetModifierAttackRangeBonus()
    return self.bonus_attack_range
end

function modifier_templar_assassin_psi_blades_lua:GetModifierAttackSpeedBonus_Constant()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi6") then
        return self.bonus_attack_range * 2
    end
end

modifier_templar_assassin_psi_blades_lua_agi12 = class({})
--Classifications template
function modifier_templar_assassin_psi_blades_lua_agi12:IsHidden()
    return false
end

function modifier_templar_assassin_psi_blades_lua_agi12:IsDebuff()
    return false
end

function modifier_templar_assassin_psi_blades_lua_agi12:IsPurgable()
    return false
end

function modifier_templar_assassin_psi_blades_lua_agi12:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_templar_assassin_psi_blades_lua_agi12:IsStunDebuff()
    return false
end

function modifier_templar_assassin_psi_blades_lua_agi12:RemoveOnDeath()
    return true
end

function modifier_templar_assassin_psi_blades_lua_agi12:DestroyOnExpire()
    return true
end

function modifier_templar_assassin_psi_blades_lua_agi12:OnCreated()
    self.time = 0
    self.bonus_damage = 20
    self.maxHealth = self:GetCaster():GetMaxHealth()
    self.interval = 0.0667
    self.self_damage = self.maxHealth * 0.02 * self.interval
    self.addition_damage = 0.01
    self.regen_amplify = -100
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi13") then
        self.regen_amplify = -50
        -- self.bonus_damage = 40
    end
    self:StartIntervalThink(self.interval)
end

function modifier_templar_assassin_psi_blades_lua_agi12:OnRefresh()
    -- self:OnCreated()
end

function modifier_templar_assassin_psi_blades_lua_agi12:OnIntervalThink()
    if self:GetStackCount() < 13 then
        self.time = self.time + self.interval
        -- self.self_damage = self.self_damage + (self.maxHealth * self.addition_damage * self.interval)
        local maxHealth = self:GetCaster():GetMaxHealth()
        -- self.self_damage = maxHealth * 0.02 * self.interval + maxHealth * (self.time * self.addition_damage * self.interval)
        self.self_damage = maxHealth * (0.02 + self.time * self.addition_damage) * self.interval

        self:SetStackCount(self.time)
    end
    if IsServer() then
        ApplyDamageRDA({
            attacker 		= self:GetParent(),
            victim 			= self:GetParent(),
            damage 			= self.self_damage,
            damage_type		= DAMAGE_TYPE_PURE,
            damage_flags 	= DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NON_LETHAL,
            ability 		= self:GetAbility(),
        })
    end
end

function modifier_templar_assassin_psi_blades_lua_agi12:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
        MODIFIER_PROPERTY_HP_REGEN_AMPLIFY_PERCENTAGE,
        MODIFIER_PROPERTY_SPELL_LIFESTEAL_AMPLIFY_PERCENTAGE,
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
    }
end

function modifier_templar_assassin_psi_blades_lua_agi12:GetModifierDamageOutgoing_Percentage()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_templar_assassin_agi13") then
        return self.time * 40
    else
        return self.time * 20
    end
    
end

function modifier_templar_assassin_psi_blades_lua_agi12:GetModifierSpellAmplify_Percentage()
	return self.regen_amplify
end

function modifier_templar_assassin_psi_blades_lua_agi12:GetModifierHPRegenAmplify_Percentage()
	return self.regen_amplify
end

function modifier_templar_assassin_psi_blades_lua_agi12:GetModifierSpellLifestealRegenAmplify_Percentage()
	return self.regen_amplify
end
