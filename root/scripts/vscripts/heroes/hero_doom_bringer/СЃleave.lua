npc_dota_hero_doom_bringer_agi9 = class({})
LinkLuaModifier( "modifier_npc_dota_hero_doom_bringer_agi9", "heroes/hero_doom_bringer/сleave", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function npc_dota_hero_doom_bringer_agi9:GetIntrinsicModifierName()
	return "modifier_npc_dota_hero_doom_bringer_agi9"
end

--------------------------------------------------------------------------------

modifier_npc_dota_hero_doom_bringer_agi9 = class({})

function modifier_npc_dota_hero_doom_bringer_agi9:IsHidden()
end

function modifier_npc_dota_hero_doom_bringer_agi9:IsDebuff( kv )
	return false
end

function modifier_npc_dota_hero_doom_bringer_agi9:IsPurgable( kv )
	return false
end


function modifier_npc_dota_hero_doom_bringer_agi9:DeclareFunctions()
	local funcs = {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_npc_dota_hero_doom_bringer_agi9:CustomOnAttackLanded( params )
	if IsServer() then
		if params.attacker == self:GetParent() and ( not self:GetParent():IsIllusion() ) then
			local target = params.target
			if target ~= nil and target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
			
				local abil = self:GetCaster():FindAbilityByName("npc_dota_hero_sven_int7")
				if abil ~= nil then 
				params.target:AddNewModifier(self:GetCaster(), ability, "modifier_magic_debuff", {duration = 2})
				end
				
				if params.attacker == self:GetParent() then
					if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
						if params.target ~= nil and params.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
							local direction = params.target:GetOrigin()-self:GetParent():GetOrigin()
							direction.z = 0
							direction = direction:Normalized()
							local range = self:GetParent():GetOrigin() + direction*650/2
			
							local facing_direction = params.attacker:GetAnglesAsVector().y
							local cleave_targets = FindUnitsInRadius(params.attacker:GetTeamNumber(),params.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
							for _,target in pairs(cleave_targets) do
								if target ~= params.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
									local attacker_vector = (target:GetOrigin() - params.attacker:GetOrigin())
									local attacker_direction = VectorToAngles( attacker_vector ).y
									local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
									if angle_diff < 45 then
										ApplyDamageRDA({
											victim = target,
											attacker = params.attacker,
											damage = params.damage * (100/100),
											damage_type = DAMAGE_TYPE_PHYSICAL,
											damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
											ability = self:GetAbility()
										})
									end
								end
							end
							local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
							ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
							ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
							ParticleManager:ReleaseParticleIndex( effect_cast )
			
						end
					end
				end
			end
		end
	end
	return 0
end