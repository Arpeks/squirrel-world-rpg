"DOTAAbilities"
{
		//=================================================================================================================
	// Razor: Unstable Current			不稳定电流
	//=================================================================================================================
	"razor_unstable_current_bh"
	{
		"BaseClass"						"ability_lua"	// "ability_datadriven"
		"ScriptFile"					"heroes/hero_razor/razor_storm_surge_lua/razor_unstable_current_bh.lua"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE" // "DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"AbilityTextureName"			"razor_unstable_current"	// "mjz_razor_unstable_current"

		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_3"

		"MaxLevel"						"15"
		"AbilityCooldown"				"3"
		"AbilityManaCost"				"0"	
		"precache"
		{
			"soundfile"			"soundevents/game_sounds_heroes/game_sounds_razor.vsndevts"
			"particle"			"particles/units/heroes/hero_razor/razor_unstable_current.vpcf"
		}

        "AbilityValues"
        {
            "movement_speed_pct"
            {
                "value"                  "8 11 14 17 20 23 26 29 32 35 38 41 44 47 50"
            }
            "chance"
            {
                "value"                  "18"
            }
            "passive_area_damage"
            {
                "value"                  "50 100 150 200 250 300 350 400 450 500 550 600 650 700 750"
            }
            "passive_area_interval"
            {
                "value"                  "3"
            }
            "radius"
            {
                "value"                  "500"
				"affected_by_aoe_increase"	"1"
            }
            "max_targets"
            {
                "value"                  "1 1 2 2 3 3 3 4 4 4 5 5 5 6 6"
            }
        }
	}
}