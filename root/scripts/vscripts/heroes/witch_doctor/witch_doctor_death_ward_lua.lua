witch_doctor_death_ward_lua = class({})

function witch_doctor_death_ward_lua:GetChannelTime()
	return self:GetSpecialValueFor("AbilityChannelTime")
end

function witch_doctor_death_ward_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int12") then
		return DOTA_ABILITY_BEHAVIOR_PASSIVE
	end
	return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_CHANNELLED
end

function witch_doctor_death_ward_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int12") then
		return 0
	end
	return self.BaseClass.GetManaCost(self, iLevel)
end

function witch_doctor_death_ward_lua:OnSpellStart()
	self.ward = self:CreateWard(self:GetCursorPosition())
	self:EndCooldown()
	EmitSoundOn("Hero_WitchDoctor.Death_WardBuild", self.ward)
end

function witch_doctor_death_ward_lua:OnChannelFinish(bInterrupted)
	self.ward.mod:OnIntervalThink()
end

function witch_doctor_death_ward_lua:CreateWard(position, reduction)
	local unit = CreateUnitByName("npc_dota_witch_doctor_death_ward", position, true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
	unit:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
	unit:SetOwner(self:GetCaster())
	unit:SetBaseAttackTime( 0.22 )
	unit:SetBaseDamageMin(self:GetSpecialValueFor("damage"))
	unit:SetBaseDamageMax(self:GetSpecialValueFor("damage"))

	unit.mod = unit:AddNewModifier(self:GetCaster(), self, "modifier_witch_doctor_death_ward_lua", {duration = self:GetSpecialValueFor("AbilityChannelTime"), reduction = reduction})
	return unit
end

function witch_doctor_death_ward_lua:OnProjectileHit_ExtraData(hTarget, vLocation, ExtraData)
	if not hTarget then
		return 
	end
	-- if RollPercentage(hTarget:GetEvasion() * 100) and RollPercentage(self:GetSpecialValueFor("bonus_accuracy")) then
	-- 	return end
	-- end
	local secondary_attack_damage_pct = self:GetSpecialValueFor("secondary_attack_damage_pct")
	if secondary_attack_damage_pct == 0 then
		secondary_attack_damage_pct = 100
	end
	secondary_attack_damage_pct = secondary_attack_damage_pct / 100
	ApplyDamageRDA({
		victim = hTarget,
		attacker = self:GetCaster(),
		damage = self:GetSpecialValueFor("damage") * secondary_attack_damage_pct,
		damage_type = ExtraData.damage_type,
		damage_flags = ExtraData.damage_type == DAMAGE_TYPE_PURE and DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
		ability = self
	})
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), vLocation, nil, self:GetSpecialValueFor("bounce_radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,unit in pairs(units) do
		if unit ~= hTarget and not ExtraData[tostring(unit:entindex())] then
			ExtraData[tostring(unit:entindex())] = true
			ProjectileManager:CreateTrackingProjectile({
				Target = unit,
				-- Source = self:GetCaster(),
				Ability = self,	
				vSourceLoc = hTarget:GetAbsOrigin(),
				EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_ward_attack.vpcf",
				iMoveSpeed = 1000,
				bDodgeable = false,
				ExtraData = ExtraData
			})
			break
		end
	end
end

function witch_doctor_death_ward_lua:GetIntrinsicModifierName()
	return "modifier_witch_doctor_death_ward_lua_intr"
end

LinkLuaModifier( "modifier_witch_doctor_death_ward_lua", "heroes/witch_doctor/witch_doctor_death_ward_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_witch_doctor_death_ward_lua_intr", "heroes/witch_doctor/witch_doctor_death_ward_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_witch_doctor_death_ward_lua_passive", "heroes/witch_doctor/witch_doctor_death_ward_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_witch_doctor_death_ward_lua = class({})
--Classifications template
function modifier_witch_doctor_death_ward_lua:IsHidden()
	return true
end

function modifier_witch_doctor_death_ward_lua:IsDebuff()
	return false
end

function modifier_witch_doctor_death_ward_lua:IsPurgable()
	return false
end

function modifier_witch_doctor_death_ward_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_witch_doctor_death_ward_lua:IsStunDebuff()
	return false
end

function modifier_witch_doctor_death_ward_lua:RemoveOnDeath()
	return true
end

function modifier_witch_doctor_death_ward_lua:DestroyOnExpire()
	return false
end

function modifier_witch_doctor_death_ward_lua:OnCreated(data)
	if not IsServer() then
		return
	end
	self.damage_type = DAMAGE_TYPE_PURE
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int6") then
		self.damage_type = DAMAGE_TYPE_MAGICAL
	end

	self.reduction = data.reduction
	self.hit_tbl = {}
	self.secondary_attack_damage_pct = self:GetAbility():GetSpecialValueFor("secondary_attack_damage_pct")
	if self.secondary_attack_damage_pct == 0 then
		self.secondary_attack_damage_pct = 100
	end
	self.secondary_attack_damage_pct = self.secondary_attack_damage_pct / 100
	self.initial_target_count = self:GetAbility():GetSpecialValueFor("initial_target_count")
	self.wardParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_witchdoctor/witchdoctor_ward_skull.vpcf", PATTACH_POINT_FOLLOW, self:GetParent()) 
	ParticleManager:SetParticleControlEnt(self.wardParticle, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_attack1", self:GetParent():GetAbsOrigin(), true)
	ParticleManager:SetParticleControl(self.wardParticle, 2, self:GetParent():GetAbsOrigin())
	self:StartIntervalThink(self:GetDuration())
end

function modifier_witch_doctor_death_ward_lua:OnIntervalThink()
	if not IsServer() then
		return
	end
	self:GetAbility().ward = nil
	self.stun = true
	StopSoundOn("Hero_WitchDoctor.Death_WardBuild", self:GetParent())
	self:GetAbility():SetActivated(true)
	self:GetAbility():UseResources(false, false, false, true)
	self:GetParent():AddNoDraw()
	local parent = self:GetParent()
	Timers:CreateTimer(5,function()
		UTIL_Remove(parent)
	end)
	self:StartIntervalThink(-1)
end

function modifier_witch_doctor_death_ward_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_witch_doctor_death_ward_lua:GetModifierDamageOutgoing_Percentage()
	if not IsServer() then
		return
	end
	return -200
end

function modifier_witch_doctor_death_ward_lua:GetModifierAttackRangeBonus()
	return self:GetAbility():GetSpecialValueFor("bonus_attack_range")
end

function modifier_witch_doctor_death_ward_lua:GetModifierAttackSpeedBonus_Constant()
	return self.reduction
end

function modifier_witch_doctor_death_ward_lua:CustomOnAttack(data)
	if data.attacker == self:GetParent() and not data.no_attack_cooldown then
		if self.initial_target_count == 2 then
			local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange(), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
			for _,unit in pairs(units) do
				if unit ~= data.target then
					self:GetParent():PerformAttack(unit, true, true, true, false, true, false, false)
					break
				end
			end
		end
	end
end

function modifier_witch_doctor_death_ward_lua:CustomOnAttackLanded(data)
	if data.attacker == self:GetParent() then
		ApplyDamageRDA({
			victim = data.target,
			attacker = data.attacker,
			damage = self:GetSpecialValueFor("damage") * self.secondary_attack_damage_pct,
			damage_type = self.damage_type,
			damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
			ability = self:GetAbility()
		})
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int9") then
			local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), data.target:GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("bounce_radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
			for i,unit in pairs(units) do
				if unit ~= data.target then
					ProjectileManager:CreateTrackingProjectile({
						Target = unit,
						-- Source = data.target,
						Ability = self:GetAbility(),	
						vSourceLoc = data.target:GetAttachmentOrigin(data.target:ScriptLookupAttachment("attach_hitloc")),
						EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_ward_attack.vpcf",
						iMoveSpeed = 1000,
						bDodgeable = false,
						ExtraData = {
							[tostring(data.target:entindex())] = true,
							damage_type = self.damage_type,
						}
					})
					if i >= 3 then
						break
					end
				end
			end
		end
	end
end

function modifier_witch_doctor_death_ward_lua:CheckState()
	return {
		[MODIFIER_STATE_STUNNED] = self.stun,
		[MODIFIER_STATE_UNSELECTABLE] = self.stun,
		[MODIFIER_STATE_CANNOT_MISS] = RandomInt(1, 2) == 1,
		[MODIFIER_STATE_ATTACK_IMMUNE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_INVULNERABLE] = true
	}
end

modifier_witch_doctor_death_ward_lua_passive = class({})
function modifier_witch_doctor_death_ward_lua_passive:IsHidden() return true end
function modifier_witch_doctor_death_ward_lua_passive:IsDebuff() return false end
function modifier_witch_doctor_death_ward_lua_passive:IsPurgable() return false end
function modifier_witch_doctor_death_ward_lua_passive:IsPurgeException() return false end
function modifier_witch_doctor_death_ward_lua_passive:IsStunDebuff() return false end
function modifier_witch_doctor_death_ward_lua_passive:RemoveOnDeath() return false end
function modifier_witch_doctor_death_ward_lua_passive:DestroyOnExpire() return false end

function modifier_witch_doctor_death_ward_lua_passive:OnCreated(data)
	self.damage_type = DAMAGE_TYPE_PURE
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int6") then
		self.damage_type = DAMAGE_TYPE_MAGICAL
	end

	self.hit_tbl = {}
	self.secondary_attack_damage_pct = self:GetAbility():GetSpecialValueFor("secondary_attack_damage_pct")
	if self.secondary_attack_damage_pct == 0 then
		self.secondary_attack_damage_pct = 100
	end

	self.secondary_attack_damage_pct = self.secondary_attack_damage_pct / 100
	self.initial_target_count = self:GetAbility():GetSpecialValueFor("initial_target_count")
	self.wardParticle = ParticleManager:CreateParticle("particles/heroes/hero_witchdoctor/witchdoctor_ward_overheadward_skull.vpcf", PATTACH_POINT_FOLLOW, self:GetParent()) 
	ParticleManager:SetParticleControlEnt(self.wardParticle, 0, self:GetParent(), PATTACH_OVERHEAD_FOLLOW, "", self:GetParent():GetAbsOrigin() + Vector(0,0,300), true)
	ParticleManager:SetParticleControlForward(self.wardParticle, 1, Vector(0,-1,0))
	ParticleManager:SetParticleControl(self.wardParticle, 2, self:GetParent():GetAbsOrigin() + Vector(0,0,300))
	self:AddParticle(self.wardParticle, false, false, -1, false, false)

	if not IsServer() then return end

	self.last_sec = self:GetCaster():GetSecondsPerAttack(true)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int13") then
		self:StartIntervalThink(self.last_sec)
	else
		self:StartIntervalThink(0.22)
	end
end

function modifier_witch_doctor_death_ward_lua_passive:OnRefresh()
	self.damage_type = DAMAGE_TYPE_PURE
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int6") then
		self.damage_type = DAMAGE_TYPE_MAGICAL
	end

	self.hit_tbl = {}
	self.secondary_attack_damage_pct = self:GetAbility():GetSpecialValueFor("secondary_attack_damage_pct")
	if self.secondary_attack_damage_pct == 0 then
		self.secondary_attack_damage_pct = 100
	end

	if not IsServer() then return end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int13") then
		if self.last_sec ~= self:GetCaster():GetSecondsPerAttack(true) then
			self.last_sec = self:GetCaster():GetSecondsPerAttack(true)
			self:StartIntervalThink(self:GetCaster():GetSecondsPerAttack(true))
		end
	end
end

function modifier_witch_doctor_death_ward_lua_passive:OnIntervalThink()
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("bounce_radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	if #units > 0 then
		ParticleManager:SetParticleControlForward(self.wardParticle, 1, (units[1]:GetAbsOrigin() - self:GetParent():GetAbsOrigin()):Normalized())
		ProjectileManager:CreateTrackingProjectile({
			Target = units[1],
			-- Source = data.target,
			Ability = self:GetAbility(),	
			vSourceLoc = self:GetParent():GetAbsOrigin() + Vector(0,0,300),
			EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_ward_attack.vpcf",
			iMoveSpeed = 1000,
			bDodgeable = false,
			ExtraData = {
				[tostring(units[1]:entindex())] = true,
				damage_type = self.damage_type,
			}
		})
	end
end

modifier_witch_doctor_death_ward_lua_intr = class({})
function modifier_witch_doctor_death_ward_lua_intr:IsHidden() return true end
function modifier_witch_doctor_death_ward_lua_intr:IsDebuff() return false end
function modifier_witch_doctor_death_ward_lua_intr:IsPurgable() return false end
function modifier_witch_doctor_death_ward_lua_intr:IsPurgeException() return false end
function modifier_witch_doctor_death_ward_lua_intr:IsStunDebuff() return false end
function modifier_witch_doctor_death_ward_lua_intr:RemoveOnDeath() return false end
function modifier_witch_doctor_death_ward_lua_intr:DestroyOnExpire() return false end

function modifier_witch_doctor_death_ward_lua_intr:OnCreated()
	if not IsServer() then return end
	self.agi9_last_cast = -240
	self.is_channeling = self:GetCaster():IsChanneling()
	self:SetHasCustomTransmitterData( true )
	self:StartIntervalThink(FrameTime())
end

function modifier_witch_doctor_death_ward_lua_intr:OnIntervalThink()
	self:SetStackCount(self:GetParent():GetBaseDamageMin() * 0.5)
	self.is_channeling = self:GetCaster():IsChanneling()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int12") then
		self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_witch_doctor_death_ward_lua_passive", {})
	else
		self:GetCaster():RemoveModifierByName("modifier_witch_doctor_death_ward_lua_passive")
	end
	self:SendBuffRefreshToClients()
end

function modifier_witch_doctor_death_ward_lua_intr:AddCustomTransmitterData()
	return {
		is_channeling = self.is_channeling,
	}
end

function modifier_witch_doctor_death_ward_lua_intr:HandleCustomTransmitterData( data )
	self:GetAbility().IsChanneling = function() return data.is_channeling == 1 or false end
end

function modifier_witch_doctor_death_ward_lua_intr:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

		MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_MIN_HEALTH,
	}
end

function modifier_witch_doctor_death_ward_lua_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_witch_doctor_death_ward_lua_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int6") then
				value = value + self:GetCaster():GetAttributesValue()
			end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi6") then
				value = value + self:GetStackCount()
			end
            return value
		end
	end
	return 0
end

function modifier_witch_doctor_death_ward_lua_intr:GetModifierHealthRegenPercentage()
	local active = false
	local mult = 1
	if self:GetCaster():HasModifier("modifier_witch_doctor_death_ward_lua_passive") then
		active = true
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str6") and self:GetAbility():IsChanneling() then
		active = true
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int13") then
		mult = 2
	end
	if active then
		return 3 * mult
	end
end

function modifier_witch_doctor_death_ward_lua_intr:GetModifierAttackSpeedBonus_Constant()
	local active = false
	local mult = 1
	if self:GetCaster():HasModifier("modifier_witch_doctor_death_ward_lua_passive") then
		active = true
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str9") and self:GetAbility():IsChanneling() then
		active = true
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int13") then
		mult = 2
	end
	if active then
		return 25 * self:GetAbility():GetLevel() * mult
	end
end

function modifier_witch_doctor_death_ward_lua_intr:GetModifierAttackRangeBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str11") then
		return 300
	end
end

function modifier_witch_doctor_death_ward_lua_intr:GetMinHealth()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi9") and (self.agi9_last_cast + 240) < GameRules:GetGameTime() then
		return 1
	end
	return 0
end

function modifier_witch_doctor_death_ward_lua_intr:CustomOnTakeDamage(data)
	if data.unit == self:GetParent() and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi9") then
		if (self.agi9_last_cast + 240) < GameRules:GetGameTime() and self:GetParent():GetHealth() <= 5 then
			self.agi9_last_cast = GameRules:GetGameTime()
			self:GetParent():SetHealth(self:GetParent():GetMaxHealth() * 0.6)
			local abi = self:GetParent():FindAbilityByName("witch_doctor_voodoo_switcheroo_lua")
			abi:SetActivated(true)
			abi:SetHidden(false)
			abi:OnSpellStart(true)
			abi:StartCooldown(240)
		end
	end
end