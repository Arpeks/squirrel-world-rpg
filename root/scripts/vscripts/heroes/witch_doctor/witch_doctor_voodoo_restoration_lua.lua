LinkLuaModifier( "modifier_witch_doctor_voodoo_restoration_lua", "heroes/witch_doctor/witch_doctor_voodoo_restoration_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_witch_doctor_voodoo_restoration_lua_intr", "heroes/witch_doctor/witch_doctor_voodoo_restoration_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_witch_doctor_voodoo_restoration_lua_str10", "heroes/witch_doctor/witch_doctor_voodoo_restoration_lua.lua", LUA_MODIFIER_MOTION_NONE )

witch_doctor_voodoo_restoration_lua = class({})

function witch_doctor_voodoo_restoration_lua:ResetToggleOnRespawn()
	return true
end

function witch_doctor_voodoo_restoration_lua:OnToggle()
	if self:GetToggleState() then
		EmitSoundOn("Hero_WitchDoctor.Voodoo_Restoration", self:GetCaster())
		EmitSoundOn("Hero_WitchDoctor.Voodoo_Restoration.Loop", self:GetCaster())
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_witch_doctor_voodoo_restoration_lua", {})
	else
		EmitSoundOn("Hero_WitchDoctor.Voodoo_Restoration.Off", self:GetCaster())
		StopSoundOn("Hero_WitchDoctor.Voodoo_Restoration.Loop", self:GetCaster())
		self:GetCaster():RemoveModifierByName("modifier_witch_doctor_voodoo_restoration_lua")
	end
end

function witch_doctor_voodoo_restoration_lua:GetIntrinsicModifierName()
	return "modifier_witch_doctor_voodoo_restoration_lua_intr"
end

modifier_witch_doctor_voodoo_restoration_lua = class({})
--Classifications template
function modifier_witch_doctor_voodoo_restoration_lua:IsHidden()
	return false
end

function modifier_witch_doctor_voodoo_restoration_lua:IsDebuff()
	return false
end

function modifier_witch_doctor_voodoo_restoration_lua:IsPurgable()
	return false
end

function modifier_witch_doctor_voodoo_restoration_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_witch_doctor_voodoo_restoration_lua:IsStunDebuff()
	return false
end

function modifier_witch_doctor_voodoo_restoration_lua:RemoveOnDeath()
	return true
end

function modifier_witch_doctor_voodoo_restoration_lua:DestroyOnExpire()
	return true
end

function modifier_witch_doctor_voodoo_restoration_lua:OnCreated()
	if not IsServer() then
		return
	end
	local heal_interval = self:GetAbility():GetSpecialValueFor("heal_interval")
	self.does_heal_all_allies = self:GetAbility():GetSpecialValueFor("does_heal_all_allies") == 1

	self.heal = self:GetAbility():GetSpecialValueFor("heal") * heal_interval
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str7") then
		self.str7 = true
	end
	self.radius = self:GetAbility():GetSpecialValueFor("radius")
	self.mana_per_second = self:GetAbility():GetSpecialValueFor("mana_per_second") * heal_interval
	self.self_only_heal_percentage = self:GetAbility():GetSpecialValueFor("self_only_heal_percentage")

	local mainParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_witchdoctor/witchdoctor_voodoo_restoration.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
	ParticleManager:SetParticleControlEnt(mainParticle, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
	ParticleManager:SetParticleControl(mainParticle, 1, Vector( self.radius, self.radius, self.radius ) )
	ParticleManager:SetParticleControlEnt(mainParticle, 2, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_staff", self:GetParent():GetAbsOrigin(), true)
	self:AddParticle(mainParticle, false, false, -1, false, false)

	self:StartIntervalThink(heal_interval)
end

function modifier_witch_doctor_voodoo_restoration_lua:OnRefresh()
	local heal_interval = self:GetAbility():GetSpecialValueFor("heal_interval")
	self.does_heal_all_allies = self:GetAbility():GetSpecialValueFor("does_heal_all_allies") == 1

	self.heal = self:GetAbility():GetSpecialValueFor("heal") * heal_interval
	self.radius = self:GetAbility():GetSpecialValueFor("radius")
	self.mana_per_second = self:GetAbility():GetSpecialValueFor("mana_per_second") * heal_interval
	self.self_only_heal_percentage = self:GetAbility():GetSpecialValueFor("self_only_heal_percentage")

end

function modifier_witch_doctor_voodoo_restoration_lua:OnIntervalThink()
	if self.does_heal_all_allies then
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for _,unit in pairs(units) do
			if self.str7 then
				unit:Heal(min(self.heal * (1 + self:GetCaster():GetSpellAmplification(false) * 0.3), 2^25), self)
			else
				unit:Heal(self.heal, self)
			end
			SendOverheadEventMessage(nil, OVERHEAD_ALERT_HEAL, unit, self.heal, nil)
		end
	else
		local damage = self.heal + self:GetParent():GetHealthRegen() * 0.045
		if self.str7 then
			damage = damage * (1 + self:GetCaster():GetSpellAmplification(false) * 0.3)
		end
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for _,unit in pairs(units) do
			self:GetCaster():PerformAttack(
				unit, -- hTarget
				true, -- bUseCastAttackOrb
				true, -- bProcessProcs
				true, -- bSkipCooldown
				true, -- bIgnoreInvis
				false, -- bUseProjectile
				true, -- bFakeAttack
				true -- bNeverMiss
			)
			ApplyDamageRDA({
				victim = unit,
				attacker = self:GetCaster(),
				damage = damage,
				damage_type = DAMAGE_TYPE_PHYSICAL,
				damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
				ability = self:GetAbility()
			})
		end
		-- self:GetParent():Heal(self.heal * self.self_only_heal_percentage / 100, self)
		-- SendOverheadEventMessage(nil, OVERHEAD_ALERT_HEAL, self:GetParent(), self.heal * self.self_only_heal_percentage / 100, nil)
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str10") then
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for _,unit in pairs(units) do
			unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_witch_doctor_voodoo_restoration_lua_str10", {duration = 0.5})
		end
	end
	self:GetCaster():SpendMana(self.mana_per_second, self:GetAbility())
	if self:GetCaster():GetMana() < self.mana_per_second then
		self:GetAbility():ToggleAbility()
	end
end

function modifier_witch_doctor_voodoo_restoration_lua:DeclareFunctions()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi10") then
		return {
			MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
			MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS
		}
	end
end

function modifier_witch_doctor_voodoo_restoration_lua:GetModifierPhysicalArmorBonus()
	return self:GetAbility():GetLevel() * 100
end

function modifier_witch_doctor_voodoo_restoration_lua:GetModifierMagicalResistanceBonus()
	return self:GetAbility():GetLevel() * 4
end

modifier_witch_doctor_voodoo_restoration_lua_str10 = class({})
function modifier_witch_doctor_voodoo_restoration_lua_str10:IsHidden() return false end
function modifier_witch_doctor_voodoo_restoration_lua_str10:IsDebuff() return true end
function modifier_witch_doctor_voodoo_restoration_lua_str10:IsPurgable() return true end
function modifier_witch_doctor_voodoo_restoration_lua_str10:IsPurgeException() return false end
function modifier_witch_doctor_voodoo_restoration_lua_str10:IsStunDebuff() return false end
function modifier_witch_doctor_voodoo_restoration_lua_str10:RemoveOnDeath() return true end
function modifier_witch_doctor_voodoo_restoration_lua_str10:DestroyOnExpire() return true end

function modifier_witch_doctor_voodoo_restoration_lua_str10:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
end

function modifier_witch_doctor_voodoo_restoration_lua_str10:GetModifierPhysicalArmorBonus()
	return -10 * self:GetAbility():GetLevel()
end

modifier_witch_doctor_voodoo_restoration_lua_intr = class({})
function modifier_witch_doctor_voodoo_restoration_lua_intr:IsHidden() return true end
function modifier_witch_doctor_voodoo_restoration_lua_intr:IsDebuff() return false end
function modifier_witch_doctor_voodoo_restoration_lua_intr:IsPurgable() return false end
function modifier_witch_doctor_voodoo_restoration_lua_intr:IsPurgeException() return false end
function modifier_witch_doctor_voodoo_restoration_lua_intr:IsStunDebuff() return false end
function modifier_witch_doctor_voodoo_restoration_lua_intr:RemoveOnDeath() return false end
function modifier_witch_doctor_voodoo_restoration_lua_intr:DestroyOnExpire() return false end

function modifier_witch_doctor_voodoo_restoration_lua_intr:OnCreated()
	if not IsServer() then return end
	self:GetParent():RemoveModifierByName("modifier_witch_doctor_voodoo_switcheroo_lua")
	self:StartIntervalThink(FrameTime())
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:OnIntervalThink()
	self:SetStackCount(self:GetParent():GetBaseDamageMin() * 0.5)
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE
	}
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "heal" then
			return 1
		end
	end
	return 0
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "heal" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "heal", data.ability_special_level )
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi7") then
				value = value + self:GetStackCount()
			end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str13") then
				value = value + self:GetCaster():GetMaxHealth() * 0.02
			end
            return value
		end
	end
	return 0
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:GetModifierSpellAmplify_Percentage()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str13") then
		return self:GetCaster():GetStrength() * 0.55
	end
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:CustomOnAttack(keys)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi8") then
		if keys.attacker == self:GetParent() and keys.target and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() and not keys.no_attack_cooldown and not self:GetParent():PassivesDisabled() then	
			local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE, FIND_ANY_ORDER, false)
			local target_number = 3
			for _, enemy in pairs(enemies) do
				if enemy ~= keys.target then
		
					self:GetParent():PerformAttack(enemy, false, true, true, true, true, false, false)

					target_number = target_number - 1
					
					if target_number <= 0 then
						break
					end
				end
			end
		end
	end
end

function modifier_witch_doctor_voodoo_restoration_lua_intr:GetModifierHealthRegenPercentage()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int7") then
		return 4
	end
end