witch_doctor_maledict_lua = class({})

function witch_doctor_maledict_lua:GetAOERadius()
    return self:GetSpecialValueFor("radius")
end

function witch_doctor_maledict_lua:OnSpellStart()
    local duration = self:GetDuration()
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCursorPosition(), nil, self:GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        unit:AddNewModifier(self:GetCaster(), self, "modifier_witch_doctor_maledict_lua", {duration = duration})
    end

    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_witchdoctor/witchdoctor_maledict_aoe.vpcf", PATTACH_POINT, self:GetCaster())
    ParticleManager:SetParticleControl(pcf, 0, self:GetCursorPosition())
    ParticleManager:SetParticleControl(pcf, 1, Vector(self:GetSpecialValueFor("radius"),0,0))
    ParticleManager:ReleaseParticleIndex(pcf)
    EmitSoundOnLocationWithCaster(self:GetCaster():GetAbsOrigin(), "Hero_WitchDoctor.Maledict_Cast", self:GetCaster())
end

function witch_doctor_maledict_lua:GetIntrinsicModifierName()
    return "modifier_witch_doctor_maledict_lua_inst"
end

LinkLuaModifier("modifier_witch_doctor_maledict_lua_inst", "heroes/witch_doctor/witch_doctor_maledict_lua", LUA_MODIFIER_MOTION_NONE)

modifier_witch_doctor_maledict_lua_inst = class({})
--Classifications template
function modifier_witch_doctor_maledict_lua_inst:IsHidden()
    return true
end

function modifier_witch_doctor_maledict_lua_inst:IsDebuff()
    return false
end

function modifier_witch_doctor_maledict_lua_inst:IsPurgable()
    return false
end

function modifier_witch_doctor_maledict_lua_inst:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_witch_doctor_maledict_lua_inst:IsStunDebuff()
    return false
end

function modifier_witch_doctor_maledict_lua_inst:RemoveOnDeath()
    return false
end

function modifier_witch_doctor_maledict_lua_inst:DestroyOnExpire()
    return false
end

function modifier_witch_doctor_maledict_lua_inst:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS
    }
end

function modifier_witch_doctor_maledict_lua_inst:CustomOnAttackLanded(data)
    if data.attacker == self:GetParent() and not data.target:IsBuilding() and not data.target:IsOther() then
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int8") then
            local cd = self:GetAbility():GetCooldownTimeRemaining()
            self:GetAbility():EndCooldown()
            self:GetAbility():StartCooldown(cd - 0.1)
        end
    end
end

function modifier_witch_doctor_maledict_lua_inst:GetModifierProjectileSpeedBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_str8") then
		return 60 * self:GetAbility():GetLevel()
	end
	return 0
end

LinkLuaModifier("modifier_witch_doctor_maledict_lua", "heroes/witch_doctor/witch_doctor_maledict_lua", LUA_MODIFIER_MOTION_NONE)

modifier_witch_doctor_maledict_lua = class({})
--Classifications template
function modifier_witch_doctor_maledict_lua:IsHidden()
    return false
end

function modifier_witch_doctor_maledict_lua:IsDebuff()
    return true
end

function modifier_witch_doctor_maledict_lua:IsPurgable()
    return false
end

function modifier_witch_doctor_maledict_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_witch_doctor_maledict_lua:IsStunDebuff()
    return false
end

function modifier_witch_doctor_maledict_lua:RemoveOnDeath()
    return true
end

function modifier_witch_doctor_maledict_lua:DestroyOnExpire()
    return true
end

function modifier_witch_doctor_maledict_lua:OnCreated()
	if IsServer() then
	    self.damage = self:GetAbility():GetAbilityDamage()
        self.damage_type = DAMAGE_TYPE_MAGICAL
        self.damage_flags = DOTA_DAMAGE_FLAG_NONE
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_int11") then
            self.damage = self.damage + self:GetCaster():GetPrimaryStatValue()
            self.damage_type = DAMAGE_TYPE_PURE
            self.damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
        end
	    self.burst = self:GetAbility():GetSpecialValueFor("bonus_damage") / 100
		self.burstTimer = 4
        
		self.currentTime = GameRules:GetGameTime()
		self.hp = self:GetParent():GetHealth()
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi11") then
		    self.burstTimer = 2
            self:StartIntervalThink( 0.25 )
        else
            self:StartIntervalThink( 0.5 )
        end
		self:GetParent():EmitSound("Hero_WitchDoctor.Maledict_Loop")
		local maledictFX = ParticleManager:CreateParticle("particles/units/heroes/hero_witchdoctor/witchdoctor_maledict.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl( maledictFX, 1, Vector(self.burstTimer,0,0) )
        self:AddParticle(maledictFX, false, false, -1, false, false)
	end
end

function modifier_witch_doctor_maledict_lua:OnRefresh()
    if not IsServer() then
        return
    end
	self.damage = self:GetAbility():GetAbilityDamage()
	self.burst = self:GetAbility():GetSpecialValueFor("bonus_damage") / 100
    local hp = self:GetParent():GetHealth()
    if hself.p < hp then
        self.ph = hp
    end
end

function modifier_witch_doctor_maledict_lua:OnIntervalThink()
	local ability = self:GetAbility()
	local parent = self:GetParent()
	local caster = self:GetCaster()
	
	if self.currentTime + self.burstTimer <= GameRules:GetGameTime() then
		self.currentTime = GameRules:GetGameTime()
        -- ApplyDamageRDA({
        --     victim = parent,
        --     attacker = caster,
        --     damage = math.max( 0, self.hp - parent:GetHealth() ) * self.burst,
        --     damage_type = DAMAGE_TYPE_MAGICAL,
        --     damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        --     ability = self:GetAbility()
        -- })
		self:GetParent():EmitSound("Hero_WitchDoctor.Maledict_Tick")
	end
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi12") then
        local phys_damage =  self:GetCaster():GetAverageTrueAttackDamage(nil) * 3
        ApplyDamageRDA({
            victim = parent,
            attacker = caster,
            damage = phys_damage,
            damage_type = DAMAGE_TYPE_PHYSICAL,
            damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
            ability = self:GetAbility()
        })
        SendOverheadEventMessage( nil, OVERHEAD_ALERT_CRITICAL, parent, phys_damage, nil )
    end
    ApplyDamageRDA({
        victim = parent,
        attacker = caster,
        damage = self.damage * 0.5,
        damage_type = self.damage_type,
        damage_flags = self.damage_flags,
        ability = self:GetAbility()
    })
end

function modifier_witch_doctor_maledict_lua:OnDestroy()
	if IsServer() then
		local ability = self:GetAbility()
		local parent = self:GetParent()
		local caster = self:GetCaster()
		self:GetParent():StopSound("Hero_WitchDoctor.Maledict_Loop")
		self:GetParent():EmitSound("Hero_WitchDoctor.Maledict_Tick")
        ApplyDamageRDA({
            victim = parent,
            attacker = caster,
            damage = math.max( 0, self.hp - parent:GetHealth() ) * self.burst,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
            ability = self:GetAbility()
        })
	end
end
