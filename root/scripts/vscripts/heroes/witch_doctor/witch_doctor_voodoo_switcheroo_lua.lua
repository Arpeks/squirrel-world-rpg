witch_doctor_voodoo_switcheroo_lua = class({})

function witch_doctor_voodoo_switcheroo_lua:Spawn()
	if not IsServer() then return end
	self:SetLevel(1)
end

function witch_doctor_voodoo_switcheroo_lua:IsRefreshable()
	return false
end

function witch_doctor_voodoo_switcheroo_lua:OnSpellStart(self_cast)
	if not self_cast then return end

	local abi = self:GetCaster():FindAbilityByName("witch_doctor_death_ward_lua")
	if abi and abi:GetLevel() > 0 then
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_witch_doctor_voodoo_switcheroo_lua", {duration = self:GetSpecialValueFor("duration")})
		self.ward = abi:CreateWard(self:GetCaster():GetAbsOrigin(), self:GetSpecialValueFor("attack_speed_reduction") * -1)
		PlayerResource:SetOverrideSelectionEntity(self:GetCaster():GetPlayerOwnerID(), self.ward)
	end
end

LinkLuaModifier( "modifier_witch_doctor_voodoo_switcheroo_lua", "heroes/witch_doctor/witch_doctor_voodoo_switcheroo_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_witch_doctor_voodoo_switcheroo_lua = class({})
--Classifications template
function modifier_witch_doctor_voodoo_switcheroo_lua:IsHidden()
	return true
end

function modifier_witch_doctor_voodoo_switcheroo_lua:IsDebuff()
	return false
end

function modifier_witch_doctor_voodoo_switcheroo_lua:IsPurgable()
	return false
end

function modifier_witch_doctor_voodoo_switcheroo_lua:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_witch_doctor_voodoo_switcheroo_lua:IsStunDebuff()
	return false
end

function modifier_witch_doctor_voodoo_switcheroo_lua:RemoveOnDeath()
	return false
end

function modifier_witch_doctor_voodoo_switcheroo_lua:DestroyOnExpire()
	return true
end

function modifier_witch_doctor_voodoo_switcheroo_lua:OnCreated()
	if not IsServer() then
		return
	end
	self:GetParent():AddNoDraw()
end

function modifier_witch_doctor_voodoo_switcheroo_lua:OnDestroy()
	if not IsServer() then
		return
	end
	self:GetParent():RemoveNoDraw()
	PlayerResource:SetOverrideSelectionEntity(self:GetParent():GetPlayerOwnerID(), self:GetParent())
	PlayerResource:SetOverrideSelectionEntity(self:GetParent():GetPlayerOwnerID(), nil)
	self:GetAbility().ward.mod:OnIntervalThink()
end

function modifier_witch_doctor_voodoo_switcheroo_lua:CheckState()
	return {
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		[MODIFIER_STATE_UNSELECTABLE] = true,
		[MODIFIER_STATE_COMMAND_RESTRICTED] = true,
		[MODIFIER_STATE_ATTACK_IMMUNE] = true,
		[MODIFIER_STATE_INVISIBLE] = true,
	}
end
