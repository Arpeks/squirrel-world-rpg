witch_doctor_paralyzing_cask_lua = class({})

function witch_doctor_paralyzing_cask_lua:GetIntrinsicModifierName()
    return "modifier_witch_doctor_paralyzing_cask_lua_inst"
end

function witch_doctor_paralyzing_cask_lua:OnSpellStart()
	local speed = self:GetSpecialValueFor("speed")

	EmitSoundOn("Hero_WitchDoctor.Paralyzing_Cask_Cast", self:GetCaster())

	ProjectileManager:CreateTrackingProjectile({
		Target = self:GetCursorTarget(),
		Source = self:GetCaster(),
		Ability = self,	
		
		EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_cask.vpcf",
		iMoveSpeed = speed,
		bDodgeable = false,
		ExtraData = {
			bounces = 0
		}
	})
end

function witch_doctor_paralyzing_cask_lua:OnProjectileHit_ExtraData(hTarget, vLocation, ExtraData)
	local delay = self:GetSpecialValueFor("bounce_delay")
	local bounce_range = self:GetSpecialValueFor("bounce_range")
	local hero_duration = self:GetSpecialValueFor("hero_duration")
	local bounces = self:GetSpecialValueFor("bounces")
	local speed = self:GetSpecialValueFor("speed")
	local base_damage = self:GetSpecialValueFor("base_damage")

	local agi13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi13")
	if hTarget ~= self:GetCaster() then

		if agi13 then
			local bounce_bonus_damage = self:GetCaster():GetAverageTrueAttackDamage(nil)
			ApplyDamageRDA({
				victim = hTarget,
				attacker = self:GetCaster(),
				damage = bounce_bonus_damage * ExtraData.bounces,
				damage_type = DAMAGE_TYPE_PHYSICAL,
				damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR,
				ability = self
			})
		end

		ApplyDamageRDA({
			victim = hTarget,
			attacker = self:GetCaster(),
			damage = base_damage,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flags = DOTA_DAMAGE_FLAG_NONE,
			ability = self
		})
		hTarget:AddNewModifier(self:GetCaster(), self, "modifier_stunned", {duration = hero_duration * (1 - hTarget:GetStatusResistance())})
	end

	EmitSoundOn("Hero_WitchDoctor.Paralyzing_Cask_Bounce", hTarget)
	print(ExtraData.bounces, bounces, delay)
	if ExtraData.bounces >= bounces then
		return
	end

	Timers:CreateTimer(delay,function()
		local target_found = false
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), vLocation, nil, bounce_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)
		for _,unit in pairs(units) do
			if unit ~= hTarget then
				target_found = true

				ProjectileManager:CreateTrackingProjectile({
					Target = unit,
					-- Source = self:GetCaster(),
					Ability = self,	
					vSourceLoc = hTarget:GetAttachmentOrigin(hTarget:ScriptLookupAttachment("attach_hitloc")),
					EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_cask.vpcf",
					iMoveSpeed = speed,
					bDodgeable = false,
					ExtraData = {
						bounces = ExtraData.bounces + 1
					}
				})

				break
			end
		end
		if not target_found and agi13 then
			if hTarget ~= self:GetCaster() then
				if (self:GetCaster():GetAbsOrigin() - vLocation):Length2D() < bounce_range then
					ProjectileManager:CreateTrackingProjectile({
						Target = self:GetCaster(),
						-- Source = self:GetCaster(),
						Ability = self,	
						vSourceLoc = hTarget:GetAttachmentOrigin(hTarget:ScriptLookupAttachment("attach_hitloc")),
						EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_cask.vpcf",
						iMoveSpeed = speed,
						bDodgeable = false,
						ExtraData = {
							bounces = ExtraData.bounces + 1
						}
					})
				end
			else
				self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_witch_doctor_paralyzing_cask_lua", {duration = 5, bounces = ExtraData.bounces})
			end
		end
	end)
end

LinkLuaModifier( "modifier_witch_doctor_paralyzing_cask_lua", "heroes/witch_doctor/witch_doctor_paralyzing_cask_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_witch_doctor_paralyzing_cask_lua = class({})
function modifier_witch_doctor_paralyzing_cask_lua:IsHidden() return false end
function modifier_witch_doctor_paralyzing_cask_lua:IsDebuff() return false end
function modifier_witch_doctor_paralyzing_cask_lua:IsPurgable() return false end
function modifier_witch_doctor_paralyzing_cask_lua:IsPurgeException() return false end
function modifier_witch_doctor_paralyzing_cask_lua:IsStunDebuff() return false end
function modifier_witch_doctor_paralyzing_cask_lua:RemoveOnDeath() return true end
function modifier_witch_doctor_paralyzing_cask_lua:DestroyOnExpire() return true end

function modifier_witch_doctor_paralyzing_cask_lua:OnCreated(data)
	if not IsServer() then return end
	self.bounces = data.bounces
	self.bounce_range = self:GetAbility():GetSpecialValueFor("bounce_range")
	local particle = ParticleManager:CreateParticle("particles/heroes/hero_witchdoctor/cask_rotate_around/cask_rotate_around.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(particle, 0, self:GetParent():GetAbsOrigin())
    self:AddParticle(particle, false, false, -1, false, true)

	self:StartIntervalThink(0.2)
end

function modifier_witch_doctor_paralyzing_cask_lua:OnIntervalThink()
	local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.bounce_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)
	if units[1] ~= nil then
		ProjectileManager:CreateTrackingProjectile({
			Target = units[1],
			-- Source = self:GetCaster(),
			Ability = self:GetAbility(),	
			vSourceLoc = self:GetParent():GetAttachmentOrigin(self:GetParent():ScriptLookupAttachment("attach_hitloc")),
			EffectName = "particles/units/heroes/hero_witchdoctor/witchdoctor_cask.vpcf",
			iMoveSpeed = self:GetAbility():GetSpecialValueFor("speed"),
			bDodgeable = false,
			ExtraData = {
				bounces = self.bounces + 1
			}
		})
		self:StartIntervalThink(-1)
		self:Destroy()
	end
end

LinkLuaModifier( "modifier_witch_doctor_paralyzing_cask_lua_inst", "heroes/witch_doctor/witch_doctor_paralyzing_cask_lua.lua", LUA_MODIFIER_MOTION_NONE )

modifier_witch_doctor_paralyzing_cask_lua_inst = class({})

function modifier_witch_doctor_paralyzing_cask_lua_inst:IsHidden() return true end

function modifier_witch_doctor_paralyzing_cask_lua_inst:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	}
end

function modifier_witch_doctor_paralyzing_cask_lua_inst:GetModifierPreAttack_BonusDamage()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_witch_doctor_agi13") then
		if IsServer() then
			self:SetStackCount(self:GetCaster():GetLastHits() * 10)
		end
		return self:GetStackCount()
	end
	return 0
end