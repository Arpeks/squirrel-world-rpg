obsidian_destroyer_equilibrium_lua = class({})

LinkLuaModifier( "modifier_obsidian_destroyer_equilibrium_effect_lua", "heroes/hero_obsidian_destroyer/obsidian_destroyer_equilibrium_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_equilibrium_lua", "heroes/hero_obsidian_destroyer/obsidian_destroyer_equilibrium_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_equilibrium_bonus_health", "heroes/hero_obsidian_destroyer/obsidian_destroyer_equilibrium_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_equilibrium_bonus_mana2", "heroes/hero_obsidian_destroyer/obsidian_destroyer_equilibrium_lua", LUA_MODIFIER_MOTION_NONE )

function obsidian_destroyer_equilibrium_lua:GetIntrinsicModifierName()
    return "modifier_obsidian_destroyer_equilibrium_lua"
end

function obsidian_destroyer_equilibrium_lua:GetBehavior()
    return DOTA_ABILITY_BEHAVIOR_PASSIVE
end

modifier_obsidian_destroyer_equilibrium_effect_lua = class({})
--Classifications template
function modifier_obsidian_destroyer_equilibrium_effect_lua:IsHidden()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:IsDebuff()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:IsPurgable()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_equilibrium_effect_lua:IsStunDebuff()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:RemoveOnDeath()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:DestroyOnExpire()
    return false
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:OnCreated(kv)
    self.mana_back_chance = self:GetAbility():GetSpecialValueFor("chance")
    self.refresh_persent = self:GetAbility():GetSpecialValueFor("refresh_persent") / 2
    if not IsServer() then
        return
    end
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:OnRefresh(kv)
    self.mana_back_chance = self:GetAbility():GetSpecialValueFor("chance")
    self.refresh_persent = self:GetAbility():GetSpecialValueFor("refresh_persent") / 2
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:DeclareFunctions()
    return {
        MODIFIER_EVENT_ON_SPENT_MANA,
        MODIFIER_PROPERTY_TOOLTIP,
        MODIFIER_PROPERTY_TOOLTIP2,
    }
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:OnTooltip(keys)
	return self.mana_back_chance
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:OnTooltip2(keys)
	return self.refresh_persent
end

function modifier_obsidian_destroyer_equilibrium_effect_lua:OnSpentMana(keys)
	if keys.unit == self:GetParent() and not keys.ability:IsToggle() and not keys.ability:IsItem() and self:GetParent().GetMaxMana then
		if RollPercentage(self.mana_back_chance ) then
			self.proc_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_essence_effect.vpcf", PATTACH_ABSORIGIN, self:GetParent())
			ParticleManager:SetParticleControlEnt(self.proc_particle, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
			ParticleManager:ReleaseParticleIndex(self.proc_particle)
            self:GetParent():GiveMana(self:GetParent():GetMaxMana() * self.refresh_persent / 100)
        end
	end
end


modifier_obsidian_destroyer_equilibrium_lua = class({})
--Classifications template
function modifier_obsidian_destroyer_equilibrium_lua:IsHidden()
    return true
end

function modifier_obsidian_destroyer_equilibrium_lua:IsDebuff()
    return false
end

function modifier_obsidian_destroyer_equilibrium_lua:IsPurgable()
    return false
end

function modifier_obsidian_destroyer_equilibrium_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_equilibrium_lua:IsStunDebuff()
    return false
end

function modifier_obsidian_destroyer_equilibrium_lua:RemoveOnDeath()
    return false
end

function modifier_obsidian_destroyer_equilibrium_lua:DestroyOnExpire()
    return false
end

function modifier_obsidian_destroyer_equilibrium_lua:OnCreated()
    if not IsServer() then
        return
    end
    self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_obsidian_destroyer_equilibrium_bonus_health", {})
    self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_obsidian_destroyer_equilibrium_bonus_mana2", {})
    self.mana_back_chance = self:GetAbility():GetSpecialValueFor("chance")
    self.refresh_persent = self:GetAbility():GetSpecialValueFor("refresh_persent")
    self:StartIntervalThink(1)
end

function modifier_obsidian_destroyer_equilibrium_lua:OnRefresh()
    if not IsServer() then
        return
    end
    self.mana_back_chance = self:GetAbility():GetSpecialValueFor("chance")
    self.refresh_persent = self:GetAbility():GetSpecialValueFor("refresh_persent")
end

function modifier_obsidian_destroyer_equilibrium_lua:OnIntervalThink()

    self:SetStackCount(self:GetAbility():GetSpecialValueFor("bonus_mana_per_int") * self:GetCaster():GetIntellect(true) + self:GetCaster():GetIntellect(true) * 0.1 + 500)
end

function modifier_obsidian_destroyer_equilibrium_lua:DeclareFunctions()
    return {
        MODIFIER_EVENT_ON_SPENT_MANA,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        -- MODIFIER_EVENT_ON_TAKEDAMAGE,
        MODIFIER_PROPERTY_MANA_BONUS
    }
end

function modifier_obsidian_destroyer_equilibrium_lua:OnSpentMana(keys)
	if keys.unit == self:GetParent() and not keys.ability:IsToggle() and not keys.ability:IsItem() and self:GetParent().GetMaxMana then
		if RollPseudoRandomPercentage(self.mana_back_chance, self:GetCaster():entindex(), self:GetCaster()) then
			self.proc_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_essence_effect.vpcf", PATTACH_ABSORIGIN, self:GetParent())
			ParticleManager:SetParticleControlEnt(self.proc_particle, 0, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
			ParticleManager:ReleaseParticleIndex(self.proc_particle)
            self:GetParent():GiveMana(self:GetParent():GetMaxMana() * self.refresh_persent / 100)

            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi8") then
                local bonus_mana_modifier = self:GetCaster():FindModifierByName("modifier_obsidian_destroyer_equilibrium_bonus_mana2")
                if bonus_mana_modifier then
                    bonus_mana_modifier:IncrementStackCount()
                end
            end
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str9") then
                local bonus_health_modifier = self:GetCaster():FindModifierByName("modifier_obsidian_destroyer_equilibrium_bonus_health")
                if bonus_health_modifier then
                    bonus_health_modifier:IncrementStackCount()
                end
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str11") then
                if RollPseudoRandomPercentage(70, self:GetCaster():entindex(), self:GetCaster()) then
                    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 500,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
                    for _,unit in pairs(units) do
                        ApplyDamageRDA({
                            victim = unit,
                            attacker = self:GetCaster(),
                            damage = self:GetCaster():GetStrength(),
                            damage_type = DAMAGE_TYPE_MAGICAL,
                            damage_flags = 0,
                            ability = self:GetAbility()
                        })
                    end
                    local pcf = ParticleManager:CreateParticle("particles/items3_fx/blink_overwhelming_burst.vpcf", PATTACH_POINT, self:GetCaster())
                    ParticleManager:SetParticleControl(pcf, 0, self:GetCaster():GetAbsOrigin())
                    ParticleManager:SetParticleControl(pcf, 1, Vector(500, 500, 500))
                    ParticleManager:ReleaseParticleIndex(pcf)
                end
            end
        end
	end
end

function modifier_obsidian_destroyer_equilibrium_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "LevelsBetweenUpgrades" then
			return 1
		end
        if data.ability_special_value == "bonus_mana_per_int" then
            return 1
        end
	end
	return 0
end

function modifier_obsidian_destroyer_equilibrium_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "LevelsBetweenUpgrades" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "LevelsBetweenUpgrades", data.ability_special_level )
            return value
		end
        if data.ability_special_value == "bonus_mana_per_int" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "bonus_mana_per_int", data.ability_special_level )
            if self:GetCaster():HasModifier("modifier_npc_dota_hero_obsidian_destroyer_buff_1") then
                value = value + 0.5
            end
            return value
		end
	end
	return 0
end

function modifier_obsidian_destroyer_equilibrium_lua:CustomOnTakeDamage(keys)
	if keys.attacker == self:GetParent() and not keys.unit:IsBuilding() and not keys.unit:IsOther() and keys.inflictor and not keys.inflictor:IsItem() then	
        if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi11") then
            if keys.damage_category == 0 and keys.inflictor and bit.band(keys.damage_flags, DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL) ~= DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL then

                self.lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker)
                ParticleManager:SetParticleControl(self.lifesteal_pfx, 0, keys.attacker:GetAbsOrigin())
                ParticleManager:ReleaseParticleIndex(self.lifesteal_pfx)
                heal = keys.damage * (25 / 100)
                keys.attacker:HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), true)
            end
        end
	end
end

function modifier_obsidian_destroyer_equilibrium_lua:GetModifierManaBonus()
    return self:GetCaster():GetIntellect(true) * self:GetAbility():GetSpecialValueFor("bonus_mana_per_int")
end

function modifier_obsidian_destroyer_equilibrium_lua:IsAura()
	return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi9") ~= nil
end

function modifier_obsidian_destroyer_equilibrium_lua:GetModifierAura()
    return "modifier_obsidian_destroyer_equilibrium_effect_lua"
end

function modifier_obsidian_destroyer_equilibrium_lua:GetAuraRadius()
	return FIND_UNITS_EVERYWHERE
end

function modifier_obsidian_destroyer_equilibrium_lua:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_FRIENDLY
end

function modifier_obsidian_destroyer_equilibrium_lua:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO
end

function modifier_obsidian_destroyer_equilibrium_lua:GetAuraEntityReject(ent) 
    if ent:GetTeamNumber() == self:GetCaster():GetTeamNumber() and self:GetCaster() == ent then
        return true
    end
    return false
end

modifier_obsidian_destroyer_equilibrium_bonus_mana2 = class({})
--Classifications template
function modifier_obsidian_destroyer_equilibrium_bonus_mana2:IsHidden()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:IsDebuff()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_equilibrium_bonus_mana2:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:DestroyOnExpire()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:OnCreated(kv)
    print('OnCreated')
	self.duration = 15
	self.stack_table = {}
	if not IsServer() then return end
	self:StartIntervalThink(0.2)
end
function modifier_obsidian_destroyer_equilibrium_bonus_mana2:OnIntervalThink()
	local repeat_needed = true
	while repeat_needed do
		if self.stack_table[1] ~= nil and GameRules:GetGameTime() - self.stack_table[1] >= self.duration then
			table.remove(self.stack_table, 1)
			self:DecrementStackCount()
		else
			repeat_needed = false
		end
	end
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:OnStackCountChanged(prev_stacks)
	if not IsServer() then return end
    self:GetCaster():CalculateStatBonus(true)
	if self:GetStackCount() > prev_stacks then
		table.insert(self.stack_table, GameRules:GetGameTime())
		self:GetCaster():GiveMana(self:GetCaster():GetIntellect(true) * 0.02)
	end
end
function modifier_obsidian_destroyer_equilibrium_bonus_mana2:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MANA_BONUS
	}
end

function modifier_obsidian_destroyer_equilibrium_bonus_mana2:GetModifierManaBonus()
	return self:GetCaster():GetIntellect(true) * 0.02 * self:GetStackCount()
end
-----------------
modifier_obsidian_destroyer_equilibrium_bonus_health = class({})
--Classifications template
function modifier_obsidian_destroyer_equilibrium_bonus_health:IsHidden()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:IsDebuff()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_equilibrium_bonus_health:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:DestroyOnExpire()
	return true
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:OnCreated(kv)
	self.duration = 15
	self.stack_table = {}
	if not IsServer() then return end
	self:StartIntervalThink(0.2)
end
function modifier_obsidian_destroyer_equilibrium_bonus_health:OnIntervalThink()
	local repeat_needed = true
	while repeat_needed do
		if self.stack_table[1] ~= nil and GameRules:GetGameTime() - self.stack_table[1] >= self.duration then
			table.remove(self.stack_table, 1)
			self:DecrementStackCount()
		else
			repeat_needed = false
		end
	end
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:OnStackCountChanged(prev_stacks)
	if not IsServer() then return end
    self:GetCaster():CalculateStatBonus(true)
	if self:GetStackCount() > prev_stacks then
		table.insert(self.stack_table, GameRules:GetGameTime())
		self:GetCaster():HealWithParams(self:GetCaster():GetStrength() * 0.4 * 1, self:GetAbility(), false, false, self:GetCaster(), false)
	end
end
function modifier_obsidian_destroyer_equilibrium_bonus_health:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_EXTRA_HEALTH_BONUS
	}
end

function modifier_obsidian_destroyer_equilibrium_bonus_health:GetModifierExtraHealthBonus()
	return self:GetCaster():GetStrength() * 0.4 * 1  * self:GetStackCount()
end