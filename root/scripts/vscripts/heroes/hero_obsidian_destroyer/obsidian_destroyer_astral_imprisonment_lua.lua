obsidian_destroyer_astral_imprisonment_lua = class({})

LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_lua", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_agi12", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_lua_instic", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_bonus_mana", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_bonus_health", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_obsidian_destroyer_astral_imprisonment_burn", "heroes/hero_obsidian_destroyer/obsidian_destroyer_astral_imprisonment_lua", LUA_MODIFIER_MOTION_NONE )

function obsidian_destroyer_astral_imprisonment_lua:GetIntrinsicModifierName()
	return "modifier_obsidian_destroyer_astral_imprisonment_lua_instic"
end

function obsidian_destroyer_astral_imprisonment_lua:GetManaCost(iLevel)
	return math.min(65000, (10 + self:GetCaster():GetIntellect(true) / 30) * 10)
end

function obsidian_destroyer_astral_imprisonment_lua:OnSpellStart()
	local caster = self:GetCaster()
	local target = self:GetCursorTarget()

	local duration = self:GetSpecialValueFor( "prison_duration" )
	if target:IsRealHero() then
		duration = self:GetSpecialValueFor( "prison_duration_ally" )
	end
	target:AddNewModifier(caster,self,"modifier_obsidian_destroyer_astral_imprisonment_lua",{ duration = duration })
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int7") then
		CreateModifierThinker( caster, self, "modifier_obsidian_destroyer_astral_imprisonment_burn", { duration = 5 }, target:GetAbsOrigin(), caster:GetTeamNumber(), false )
	end
	EmitSoundOn( "Hero_ObsidianDestroyer.AstralImprisonment.Cast", caster )
end

function obsidian_destroyer_astral_imprisonment_lua:IsRefreshable()
	return true
end

function obsidian_destroyer_astral_imprisonment_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi10") then
		return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_DONT_RESUME_ATTACK + DOTA_ABILITY_BEHAVIOR_AUTOCAST
	end
	return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_DONT_RESUME_ATTACK
end

function obsidian_destroyer_astral_imprisonment_lua:GetCastRange(vLocation, hTarget)
	return self:GetSpecialValueFor("cast_range")
end

modifier_obsidian_destroyer_astral_imprisonment_lua = class({})

function modifier_obsidian_destroyer_astral_imprisonment_lua:IsHidden()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:IsDebuff()
	return self:GetCaster():GetTeamNumber()~=self:GetParent():GetTeamNumber()
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:IsStunDebuff()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:IsPurgable()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:RemoveOnDeath()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:OnCreated( kv )
	local damage = self:GetAbility():GetSpecialValueFor( "damage" )
	self.parent = self:GetParent()
	self.heal = kv.heal
	self.isAlly = self:GetParent():GetTeamNumber() == self:GetCaster():GetTeamNumber()
	if not IsServer() then return end
	self.damageTable = {
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = damage,
		damage_type = self:GetAbility():GetAbilityDamageType(),
		ability = self:GetAbility(), --Optional.
	}
	if self.isAlly then
		self:GetParent():AddNoDraw()
	end
	local effect_cast1 = ParticleManager:CreateParticle( "particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_prison.vpcf", PATTACH_WORLDORIGIN, nil )
	ParticleManager:SetParticleControl( effect_cast1, 0, self:GetParent():GetOrigin() )
	local effect_cast2 = ParticleManager:CreateParticleForTeam( "particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_prison_ring.vpcf", PATTACH_WORLDORIGIN, nil, self:GetCaster():GetTeamNumber() )
	ParticleManager:SetParticleControl( effect_cast2, 0, self:GetParent():GetOrigin() )
	self:AddParticle(effect_cast1,false,false,-1,false,false)
	self:AddParticle(effect_cast2,false,false,-1,false,false)
	EmitSoundOnLocationWithCaster( self:GetParent():GetOrigin(), "Hero_ObsidianDestroyer.AstralImprisonment", self:GetCaster() )
	if self:GetParent() == self:GetCaster() then
		return
	end
	self:StartIntervalThink(1)
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:OnIntervalThink()
	-- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int10") then
	-- 	local damageEnd = self:GetAbility():GetSpecialValueFor( "damage" ) + (self:GetCaster():GetIntellect(true) * 2) * (self:GetCaster():GetSpellAmplification(false) + 1)
	-- 	local magicResistence = self:GetParent():Script_GetMagicalArmorValue(false, self:GetAbility())
	-- 	if self:GetParent():GetHealth() < damageEnd * (1 - magicResistence) then
	-- 		self:OnDestroy()
	-- 		self:SetDuration(0, true)
	-- 	end
	-- end
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:OnRefresh( kv )
	local damage = self:GetAbility():GetSpecialValueFor( "damage" )

	if not IsServer() then return end
	self.damageTable.damage = damage
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:OnDestroy()
	local damage = self:GetAbility():GetSpecialValueFor( "damage" )
	if not IsServer() then return end
	self.damageTable.damage = damage
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int10") then
		local damageEnd = self:GetAbility():GetSpecialValueFor( "damage" ) + (self:GetCaster():GetIntellect(true) * 2) * (self:GetCaster():GetSpellAmplification(false) + 1)
		local magicResistence = self:GetParent():Script_GetMagicalArmorValue(false, self:GetAbility())
		if self:GetParent():GetHealth() < damageEnd * (1 - magicResistence) then
			self.damageTable.damage = damage + (self:GetCaster():GetIntellect(true) * 2)
		end
	end
	if self.isAlly then
		self:GetParent():RemoveNoDraw()
	end
	StopSoundOn( "Hero_ObsidianDestroyer.AstralImprisonment", self:GetCaster() )
	EmitSoundOnLocationWithCaster( self:GetParent():GetOrigin(), "Hero_ObsidianDestroyer.AstralImprisonment.End", self:GetCaster() )

	if self:GetParent():GetTeamNumber() == self:GetCaster():GetTeamNumber() then
		if self.heal then
			self:GetParent():HealWithParams(self:GetParent():GetMaxHealth(), self:GetAbility(), false, true, self:GetCaster(), false)
		end
		return
	end
    self.damageTable.victim = self:GetParent()
	ApplyDamageRDA( self.damageTable )
	SendOverheadEventMessage(nil,OVERHEAD_ALERT_BONUS_SPELL_DAMAGE,self:GetParent(),self.damageTable.damage,nil)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi12") then
		self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_obsidian_destroyer_astral_imprisonment_agi12", {duration = 4})
	end

	local manaStealPercent = self:GetAbility():GetSpecialValueFor("mana_capacity_steal")
	local manaStealDuration = self:GetAbility():GetSpecialValueFor("mana_capacity_duration")
	local bonus_mana_modifier = self:GetCaster():FindModifierByName("modifier_obsidian_destroyer_astral_imprisonment_bonus_mana")
	if bonus_mana_modifier then
		bonus_mana_modifier:IncrementStackCount()
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str10") then
		local bonus_health_modifier = self:GetCaster():FindModifierByName("modifier_obsidian_destroyer_astral_imprisonment_bonus_health")
		if bonus_health_modifier then
			bonus_health_modifier:IncrementStackCount()
		end
	end
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:CheckState()
	local state = {
		[MODIFIER_STATE_OUT_OF_GAME] = self.isAlly,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_STUNNED] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_UNSELECTABLE] = self.isAlly,
	}

	return state
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_DEATH
    }
end

function modifier_obsidian_destroyer_astral_imprisonment_lua:CustomOnDeath(data)
    if data.unit == self.parent then
        self:OnDestroy()
    end
end

modifier_obsidian_destroyer_astral_imprisonment_lua_instic = class({})
--Classifications template
function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:IsHidden()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:IsDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:RemoveOnDeath()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:DestroyOnExpire()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:OnCreated( kv )
	if not IsServer() then return end
	self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(), "modifier_obsidian_destroyer_astral_imprisonment_bonus_mana", {})
	self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(), "modifier_obsidian_destroyer_astral_imprisonment_bonus_health", {})
	self:StartIntervalThink(0.2)
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:OnIntervalThink()
	if self:GetParent():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi10") then
		if self:GetAbility():GetAutoCastState() and self:GetAbility():IsFullyCastable() then
			local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetOrigin(), nil, self:GetAbility():GetCastRange(), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false)
			if #enemies > 0 then
				local target = enemies[RandomInt(1, #enemies)]
				self:GetParent():SetCursorCastTarget(target)
				self:GetAbility():OnSpellStart()
				self:GetAbility():UseResources(false, false, false, true)
			end
		end
	end
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		MODIFIER_PROPERTY_MIN_HEALTH,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE
    }
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int10") then
				value = value + self:GetCaster():GetIntellect(true)
			end
            return value
		end
	end
	return 0
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:GetMinHealth()
	if not self:GetParent():HasModifier("modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd") and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str6") then
		return 1
	end
	return 0
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_instic:CustomOnTakeDamage(data)
	if data.unit == self:GetParent() and not self:GetCaster():HasModifier("modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd") and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str6") then
		if data.damage >= self:GetParent():GetHealth() then
			self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_obsidian_destroyer_astral_imprisonment_lua", {duration = self:GetAbility():GetSpecialValueFor("prison_duration"), heal = true})
			self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd", {duration = 200 * self:GetCaster():GetCooldownReduction()})
		end
	end
end

modifier_obsidian_destroyer_astral_imprisonment_agi12 = class({})
--Classifications template
function modifier_obsidian_destroyer_astral_imprisonment_agi12:IsHidden()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:IsDebuff()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_astral_imprisonment_agi12:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:DestroyOnExpire()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
	}
end

function modifier_obsidian_destroyer_astral_imprisonment_agi12:GetModifierIncomingDamage_Percentage(data)
	if data.attacker == self:GetCaster() then
		return 100
	end
	return 0
end

modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd = class({})
--Classifications template
function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:IsHidden()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:IsDebuff()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_lua_save_cd:DestroyOnExpire()
	return true
end

modifier_obsidian_destroyer_astral_imprisonment_bonus_mana = class({})
--Classifications template
function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:IsHidden()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:IsDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:DestroyOnExpire()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:OnCreated(kv)
	self.duration = self:GetAbility():GetSpecialValueFor("mana_capacity_duration")
	self.stack_table = {}
	if not IsServer() then return end
	self:StartIntervalThink(0.2)
end
function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:OnIntervalThink()
	local repeat_needed = true
	while repeat_needed do
		if self.stack_table[1] ~= nil and GameRules:GetGameTime() - self.stack_table[1] >= self.duration then
			table.remove(self.stack_table, 1)
			self:DecrementStackCount()
		else
			repeat_needed = false
		end
	end
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:OnStackCountChanged(prev_stacks)
	if not IsServer() then return end
	self:GetCaster():CalculateStatBonus(true)
	if self:GetStackCount() > prev_stacks then
		table.insert(self.stack_table, GameRules:GetGameTime())
		self:GetCaster():GiveMana(self:GetCaster():GetIntellect(true) / 100 * self:GetAbility():GetSpecialValueFor("mana_capacity_steal"))
	end
	
end
function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MANA_BONUS
	}
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_mana:GetModifierManaBonus()
	return (self:GetCaster():GetIntellect(true) / 100 * self:GetAbility():GetSpecialValueFor("mana_capacity_steal")) * self:GetStackCount()
end

modifier_obsidian_destroyer_astral_imprisonment_burn = class({})

function modifier_obsidian_destroyer_astral_imprisonment_burn:IsHidden()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:IsDebuff()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:OnCreated( kv )

	self.thinker = kv.isProvidedByAura~=1

	if not IsServer() then return end
	if self.thinker then return end
	self:StartIntervalThink( 1 )
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:OnRefresh( kv )
	
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:OnRemoved()
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:OnDestroy()
	if not IsServer() then return end
	if not self.thinker then return end

	UTIL_Remove( self:GetParent() )
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:OnIntervalThink()
	if not IsServer() then return end
	local damageTable = {
		attacker = self:GetCaster(),
		damage_type = DAMAGE_TYPE_MAGICAL,
		ability = self:GetAbility(),
		victim = self:GetParent(),
		damage = math.max(self:GetCaster():GetIntellect(true), self:GetCaster():GetMaxMana()) * 0.8,
	}
	ApplyDamageRDA( damageTable )
	SendOverheadEventMessage(nil,OVERHEAD_ALERT_BONUS_SPELL_DAMAGE,self:GetParent(),damageTable.damage,nil)
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:IsAura()
	return self.thinker
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetModifierAura()
	return "modifier_obsidian_destroyer_astral_imprisonment_burn"
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetAuraRadius()
	return 600
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetAuraDuration()
	return 0.5
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetAuraSearchType()
	return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetAuraSearchFlags()
	return 0
end

function modifier_obsidian_destroyer_astral_imprisonment_burn:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

modifier_obsidian_destroyer_astral_imprisonment_bonus_health = class({})
--Classifications template
function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:IsHidden()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:IsDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:IsPurgable()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:IsStunDebuff()
	return false
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:RemoveOnDeath()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:DestroyOnExpire()
	return true
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:OnCreated(kv)
	self.duration = self:GetAbility():GetSpecialValueFor("mana_capacity_duration")
	self.stack_table = {}
	if not IsServer() then return end
	self:StartIntervalThink(0.2)
end
function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:OnIntervalThink()
	local repeat_needed = true
	while repeat_needed do
		if self.stack_table[1] ~= nil and GameRules:GetGameTime() - self.stack_table[1] >= self.duration then
			table.remove(self.stack_table, 1)
			self:DecrementStackCount()
		else
			repeat_needed = false
		end
	end
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:OnStackCountChanged(prev_stacks)
	if not IsServer() then return end
	self:GetCaster():CalculateStatBonus(true)
	if self:GetStackCount() > prev_stacks then
		table.insert(self.stack_table, GameRules:GetGameTime())
		self:GetCaster():HealWithParams(self:GetCaster():GetStrength() * 3.5, self:GetAbility(), false, false, self:GetCaster(), false)
	end
end
function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_EXTRA_HEALTH_BONUS
	}
end

function modifier_obsidian_destroyer_astral_imprisonment_bonus_health:GetModifierExtraHealthBonus()
	return self:GetCaster():GetStrength() * 3.5  * self:GetStackCount()
end