obsidian_destroyer_sanity_eclipse_lua = class({})

LinkLuaModifier( "modifier_obsidian_destroyer_sanity_eclipse_lua", "heroes/hero_obsidian_destroyer/obsidian_destroyer_sanity_eclipse_lua", LUA_MODIFIER_MOTION_NONE )

function obsidian_destroyer_sanity_eclipse_lua:GetAbilityTextureName()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6") then
        return "obsidian_destroyer/ti8_immortal_wings/obsidian_destroyer_sanity_eclipse_immortal"
    end
    return "obsidian_destroyer_sanity_eclipse"
end

function obsidian_destroyer_sanity_eclipse_lua:GetIntrinsicModifierName()
    return "modifier_obsidian_destroyer_sanity_eclipse_lua"
end

function obsidian_destroyer_sanity_eclipse_lua:GetBehavior()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6") then
        return DOTA_ABILITY_BEHAVIOR_PASSIVE
    end
    return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
end

function obsidian_destroyer_sanity_eclipse_lua:GetManaCost(iLevel)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6") then
        return 0
    end
    return math.min(65000, (20 + self:GetCaster():GetIntellect(true) / 20) * 10)
end

function obsidian_destroyer_sanity_eclipse_lua:GetAOERadius()
    return self:GetSpecialValueFor("radius")
end

function obsidian_destroyer_sanity_eclipse_lua:OnSpellStart()
    local damage = 0
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int11") then
        damage = damage + math.max(self:GetCaster():GetMaxMana(), self:GetCaster():GetIntellect(true)) * self:GetSpecialValueFor("damage_multiplier") / 100
    else
        damage = damage + self:GetCaster():GetMaxMana() * self:GetSpecialValueFor("damage_multiplier") / 100
    end
    damage = damage + self:GetCaster():GetIntellect(true) * self:GetSpecialValueFor("int_multiplier") / 100
    local point = self:GetCursorPosition()
    local radius = self:GetSpecialValueFor("radius")
    local targets = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), point, nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false)

    for _, target in pairs(targets) do
        ApplyDamageRDA({
            victim = target,
            attacker = self:GetCaster(),
            damage = damage,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self
        })
    end
	local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_sanity_eclipse_area.vpcf", PATTACH_WORLDORIGIN, nil )
	ParticleManager:SetParticleControl( effect_cast, 0, point )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( radius, radius, 0 ) )
	ParticleManager:SetParticleControl( effect_cast, 2, Vector( radius, radius, radius ) )
	ParticleManager:ReleaseParticleIndex( effect_cast )

	EmitSoundOn( "Hero_ObsidianDestroyer.Sanityeclipse.Cast", self:GetCaster() )
	EmitSoundOnLocationWithCaster( point, "Hero_ObsidianDestroyer.Sanityeclipse", self:GetCaster() )
end

modifier_obsidian_destroyer_sanity_eclipse_lua = class({})
--Classifications template
function modifier_obsidian_destroyer_sanity_eclipse_lua:IsHidden()
    return true
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:IsDebuff()
    return false
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:IsPurgable()
    return false
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_obsidian_destroyer_sanity_eclipse_lua:IsStunDebuff()
    return false
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:RemoveOnDeath()
    return false
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:DestroyOnExpire()
    return false
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS,
    }
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:GetModifierAttackSpeedBonus_Constant()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6") then
        return math.min(50 + self:GetCaster():GetLevel() * 10, 400)
    end
end

function modifier_obsidian_destroyer_sanity_eclipse_lua:GetModifierProjectileSpeedBonus()
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6") then
        return 300
    end
end