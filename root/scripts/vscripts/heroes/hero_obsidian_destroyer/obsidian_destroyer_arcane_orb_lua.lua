obsidian_destroyer_arcane_orb_lua = class({})

LinkLuaModifier( "modifier_obsidian_destroyer_arcane_orb_orb", "heroes/hero_obsidian_destroyer/obsidian_destroyer_arcane_orb_lua", LUA_MODIFIER_MOTION_NONE )

function obsidian_destroyer_arcane_orb_lua:GetAbilityTextureName()
    -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str12") then
    --     return "obsidian_destroyer/lucent_gate_icons/obsidian_destroyer_arcane_orb"
    -- end
    -- if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_str13") then
    --     return "obsidian_destroyer/lucent_gate_icons/obsidian_destroyer_arcane_orb"
    -- end
    return "obsidian_destroyer_arcane_orb"
end

function obsidian_destroyer_arcane_orb_lua:GetManaCost(iLevel)
    if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int8") then
        return self:GetCaster():GetMana() * 0.2
    end
	return math.min(65000, (10 + self:GetCaster():GetIntellect(true) / 60) * 10)
end

function obsidian_destroyer_arcane_orb_lua:GetIntrinsicModifierName()
    return "modifier_obsidian_destroyer_arcane_orb_orb"
end

function obsidian_destroyer_arcane_orb_lua:GetCastRange(vLocation, hTarget)
    return self:GetCaster():Script_GetAttackRange() + 50
end

function obsidian_destroyer_arcane_orb_lua:OnOrbFire( params )
	EmitSoundOn( "Hero_ObsidianDestroyer.ArcaneOrb", self:GetCaster() )
end

function obsidian_destroyer_arcane_orb_lua:OnOrbImpact( params )
	local caster = self:GetCaster()
    if self:GetManaCost() > caster:GetMana() then
        return
    end
	local damage = self:GetSpecialValueFor("damage")
    local mod = self:GetCaster():FindModifierByName("modifier_obsidian_destroyer_arcane_orb_orb")
    if mod then
        local stack_count = mod:GetStackCount()
        local stack_add =  damage / 100 * self:GetSpecialValueFor("shield")
        if stack_count + stack_add > caster:GetStrength() * 20 then
            mod:SetStackCount(caster:GetStrength() * 20)
        else
            mod:SetStackCount(stack_count + stack_add)
        end
        
    end
	local damageTable = {
		victim = params.target,
		attacker = caster,
		damage = damage,
		damage_type = self:GetAbilityDamageType(),
        damage_flags = DOTA_DAMAGE_FLAG_NONE,
		ability = self, --Optional.
	}
    if not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int12") then
        ApplyDamageRDA( damageTable )
        SendOverheadEventMessage(nil,OVERHEAD_ALERT_BONUS_SPELL_DAMAGE,params.target,damageTable.damage,nil)
    else
        local enemies = FindUnitsInRadius(caster:GetTeamNumber(),params.target:GetOrigin(),nil,150,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,0,0,false)
        for _,enemy in pairs(enemies) do
            damageTable.victim = enemy
            ApplyDamageRDA( damageTable )
            SendOverheadEventMessage(nil,OVERHEAD_ALERT_BONUS_SPELL_DAMAGE,enemy,damageTable.damage,nil)
        end
    end

	EmitSoundOn( "Hero_ObsidianDestroyer.ArcaneOrb.Impact", params.target )
end


modifier_obsidian_destroyer_arcane_orb_orb = class({})

function modifier_obsidian_destroyer_arcane_orb_orb:IsHidden()
    return true
end

function modifier_obsidian_destroyer_arcane_orb_orb:IsDebuff()
    return false
end

function modifier_obsidian_destroyer_arcane_orb_orb:IsPurgable()
    return false
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetAttributes()
    return MODIFIER_ATTRIBUTE_PERMANENT
end

function modifier_obsidian_destroyer_arcane_orb_orb:OnCreated( kv )
    self.ability = self:GetAbility()
    self.cast = false
    self.records = {}
end

function modifier_obsidian_destroyer_arcane_orb_orb:DeclareFunctions()
    local funcs = {
        -- MODIFIER_EVENT_ON_ATTACK_FAIL,
        MODIFIER_PROPERTY_PROCATTACK_FEEDBACK,
        -- MODIFIER_EVENT_ON_ATTACK_RECORD_DESTROY,
        -- MODIFIER_EVENT_ON_ORDER,
        MODIFIER_PROPERTY_PROJECTILE_NAME,
        -- MODIFIER_EVENT_ON_ATTACK,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
        MODIFIER_PROPERTY_INCOMING_SPELL_DAMAGE_CONSTANT,
        MODIFIER_PROPERTY_HEALTH_BONUS,
    }

    return funcs
end

function modifier_obsidian_destroyer_arcane_orb_orb:CustomOnAttack( params )
    if params.attacker~=self:GetParent() then return end
    if params.no_attack_cooldown then return end
    if self:ShouldLaunch( params.target ) then
        self.ability:UseResources( true, false, false, true )
        self.records[params.record] = true
        if self.ability.OnOrbFire then self.ability:OnOrbFire( params ) end
    end

    self.cast = false
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierProcAttack_Feedback( params )
    if self.records[params.record] then
        if self.ability.OnOrbImpact then self.ability:OnOrbImpact( params ) end
    end
end
function modifier_obsidian_destroyer_arcane_orb_orb:CustomOnAttackFail( params )
    if self.records[params.record] then
        if self.ability.OnOrbFail then self.ability:OnOrbFail( params ) end
    end
end
function modifier_obsidian_destroyer_arcane_orb_orb:CustomOnAttackRecordDestroy( params )
    self.records[params.record] = nil
end

function modifier_obsidian_destroyer_arcane_orb_orb:CustomOnOrder( params )
    if params.unit~=self:GetParent() then return end

    if params.ability then
        if params.ability==self:GetAbility() then
            self.cast = true
            return
        end
        local pass = false
        local behavior = params.ability:GetBehaviorInt()
        if self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_CHANNEL ) or 
            self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_MOVEMENT ) or
            self:FlagExist( behavior, DOTA_ABILITY_BEHAVIOR_IGNORE_CHANNEL )
        then
            local pass = true -- do nothing
        end

        if self.cast and (not pass) then
            self.cast = false
        end
    else
        if self.cast then
            if self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_POSITION ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_MOVE_TO_TARGET ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_MOVE ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_ATTACK_TARGET ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_STOP ) or
                self:FlagExist( params.order_type, DOTA_UNIT_ORDER_HOLD_POSITION )
            then
                self.cast = false
            end
        end
    end
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierProjectileName()
    if self:ShouldLaunch( self:GetCaster():GetAggroTarget() ) and self:GetAbility():IsFullyCastable() then
        return "particles/units/heroes/hero_obsidian_destroyer/obsidian_destroyer_arcane_orb.vpcf"
    end
end

function modifier_obsidian_destroyer_arcane_orb_orb:ShouldLaunch( target )
    if self.ability:GetAutoCastState() then
        if self.ability.CastFilterResultTarget~=CDOTA_Ability_Lua.CastFilterResultTarget then
            if self.ability:CastFilterResultTarget( target )==UF_SUCCESS then
                self.cast = true
            end
        else
            local nResult = UnitFilter(
                target,
                self.ability:GetAbilityTargetTeam(),
                self.ability:GetAbilityTargetType(),
                self.ability:GetAbilityTargetFlags(),
                self:GetCaster():GetTeamNumber()
            )
            if nResult == UF_SUCCESS then
                self.cast = true
            end
        end
    end

    if self.cast then
        return true
    end

    return false
end

function modifier_obsidian_destroyer_arcane_orb_orb:FlagExist(a,b)
    local p,c,d=1,0,b
    while a>0 and b>0 do
        local ra,rb=a%2,b%2
        if ra+rb>1 then c=c+p end
        a,b,p=(a-ra)/2,(b-rb)/2,p*2
    end
    return c==d
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
			return 1
		end
	end
	return 0
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage" then
            local ability = self:GetAbility()
            local caster = self:GetCaster()

			local value = ability:GetLevelSpecialValueNoOverride( "damage", data.ability_special_level )

            local mod = caster:FindAbilityByName("modifier_obsidian_destroyer_equilibrium_lua")
            if mod and mod:IsHidden() == false then
                value = value + mod:GetStackCount() * ability:GetSpecialValueFor("mana_damage_pct")/100
            else
                value = value + caster:GetMaxMana() * ability:GetSpecialValueFor("mana_damage_pct")/100
            end
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_obsidian_destroyer_int11") then
                value = value + math.max(self:GetCaster():GetMaxMana(), self:GetCaster():GetIntellect(true)) * ability:GetSpecialValueFor("mana_damage_pct")/100
            else
                value = value + self:GetCaster():GetMaxMana() * ability:GetSpecialValueFor("mana_damage_pct")/100
            end
            value = value + ability:GetSpecialValueFor("str_damage") /100 * caster:GetStrength()
            if ability:GetSpecialValueFor("health_damage") > 0 then
                local maxHealth = caster:GetMaxHealth()
                if maxHealth > caster:GetStrength() * 100 then
                    maxHealth = caster:GetStrength() * 100
                end
                value = value + maxHealth / 100 * ability:GetSpecialValueFor("health_damage")
            end
            value = value + ability:GetSpecialValueFor("int_damage") / 100 * caster:GetIntellect(true)

            return value
		end
	end
	return 0
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierIncomingSpellDamageConstant(keys)
	if IsClient() then
		return self:GetStackCount()
	end
	if keys.damage_type == DAMAGE_TYPE_MAGICAL then
		if keys.original_damage >= self:GetStackCount() then
			return self:GetStackCount() * (-1)
		else
			self:SetStackCount(self:GetStackCount() - keys.original_damage)
			return keys.original_damage * (-1)
		end
	end
end

function modifier_obsidian_destroyer_arcane_orb_orb:GetModifierHealthBonus( params )
    -- if self:GetAbility():GetSpecialValueFor("health_damage") > 0 then
    --     return self:GetCaster():GetStrength() * 20 * -0.30
    -- end
    return 0
end