"DOTAAbilities"
{
	"drow_cross_lua"
	{
		"BaseClass"						"ability_lua"
		"AbilityTextureName"			"POWERSHOT"
		"ScriptFile"					"heroes/hero_drow_ranger/drow_ranger_powershot_lua/drow_cross_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"MaxLevel"						"15"
		"AbilityCooldown"				"10"
		"AbilityManaCost"				"100"
		"AbilityCastRange"				"700"
		"AbilityValues"
		{
			"speed"
			{
				"value"					"1500"
			}
			"distance"
			{
				"value"					"800"
			}
			"reduction"
			{
				"value"					"-100"
			}
			"damage"
			{
				"value"					"50 100 150 200 250 300 350 400 450 500 550 600 650 700 750"
			}
			"count"
			{
				"value"					"2"
			}
		}
	}
	
}