drow_ranger_frost_arrows_lua = class({})
LinkLuaModifier( "modifier_drow_ranger_frost_arrows_lua", "heroes/hero_drow_ranger/drow_ranger_frost_arrows_lua/modifier_drow_ranger_frost_arrows_lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_generic_orb_effect_lua", "heroes/generic/modifier_generic_orb_effect_lua", LUA_MODIFIER_MOTION_NONE )


function drow_ranger_frost_arrows_lua:GetIntrinsicModifierName()
	return "modifier_generic_orb_effect_lua"
end

function drow_ranger_frost_arrows_lua:GetManaCost(iLevel)
	if self:GetCaster():HasModifier("modifier_hero_drow_ranger_buff_1") then
		return 0
	end
    return 1 + math.min(65000, self:GetCaster():GetIntellect(true) / 250)
end

function drow_ranger_frost_arrows_lua:OnSpellStart()
end

--------------------------------------------------------------------------------
-- Orb Effects
function drow_ranger_frost_arrows_lua:GetProjectileName()
	return "particles/units/heroes/hero_drow/drow_frost_arrow.vpcf"
end

function drow_ranger_frost_arrows_lua:OnOrbFire( params )
	-- play effects
	local sound_cast = "Hero_DrowRanger.FrostArrows"
	EmitSoundOn( sound_cast, self:GetCaster() )
end

function drow_ranger_frost_arrows_lua:OnOrbImpact( params )
	local duration = self:GetSpecialValueFor("frost_arrows_duration_tooltip")

	params.target:AddNewModifier(
		self:GetCaster(), -- player source
		self, -- ability source
		"modifier_drow_ranger_frost_arrows_lua", -- modifier name
		{ duration = duration } -- kv
	)
	if self:GetCaster():HasModifier("modifier_hero_drow_ranger_buff_1") then
		ApplyDamageRDA({
			victim = params.target,
			attacker = self:GetCaster(),
			ability = self,
			damage = self:GetCaster():GetDamageMax() * 0.4,
			damage_type = DAMAGE_TYPE_PHYSICAL,
			damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL
		})
	end
end