LinkLuaModifier( "modifier_clinkz_strafe_lua", "heroes/hero_clinkz/clinkz_strafe_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_strafe_lua_intr", "heroes/hero_clinkz/clinkz_strafe_lua.lua", LUA_MODIFIER_MOTION_NONE )

clinkz_strafe_lua = class({})

function clinkz_strafe_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function clinkz_strafe_lua:GetCooldown(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi8") then
		return 0
	end
	return self.BaseClass.GetCooldown(self, iLevel)
end

function clinkz_strafe_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi8") then
		return DOTA_ABILITY_BEHAVIOR_TOGGLE
	end
	return self.BaseClass.GetBehavior(self)
end

function clinkz_strafe_lua:OnSpellStart()
	self:GetCaster():ApplyArchers(self:GetCaster(), self, "modifier_clinkz_strafe_lua", {duration = self:GetSpecialValueFor("duration")})

	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_strafe_lua", {duration = self:GetSpecialValueFor("duration")})
end

function clinkz_strafe_lua:OnToggle()
	if self:GetToggleState() then
		self:GetCaster():ApplyArchers(self:GetCaster(), self, "modifier_clinkz_strafe_lua", {duration = 1})

		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_strafe_lua", {})
	else
		self:GetCaster():RemoveModifierByName("modifier_clinkz_strafe_lua")
	end
end

function clinkz_strafe_lua:GetIntrinsicModifierName()
	return "modifier_clinkz_strafe_lua_intr"
end

modifier_clinkz_strafe_lua = class({})
function modifier_clinkz_strafe_lua:IsHidden() return false end
function modifier_clinkz_strafe_lua:IsDebuff() return false end
function modifier_clinkz_strafe_lua:IsPurgable() return false end
function modifier_clinkz_strafe_lua:IsPurgeException() return false end
function modifier_clinkz_strafe_lua:IsStunDebuff() return false end
function modifier_clinkz_strafe_lua:RemoveOnDeath() return true end
function modifier_clinkz_strafe_lua:DestroyOnExpire() return true end

function modifier_clinkz_strafe_lua:OnCreated()
	self.special_bonus_unique_npc_dota_hero_clinkz_agi13 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi13")
	if self:GetParent() ~= self:GetCaster() then
		local archer_attack_speed_pct = self:GetAbility():GetSpecialValueFor("archer_attack_speed_pct") / 100
		self.attack_speed_bonus = self:GetAbility():GetSpecialValueFor("attack_speed_bonus") * archer_attack_speed_pct
	else
		self.attack_speed_bonus = self:GetAbility():GetSpecialValueFor("attack_speed_bonus")
	end
	self.attack_range_bonus = self:GetAbility():GetSpecialValueFor("attack_range_bonus")
	if not IsServer() then return end
	EmitSoundOn("Hero_Clinkz.Strafe", self:GetParent())
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_strafe.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
	self:AddParticle(pcf, false, false, -1, false, false)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi8") then
		self:StartIntervalThink(0.5)
	end
end

function modifier_clinkz_strafe_lua:OnIntervalThink()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi8") then
		self:GetCaster():ApplyArchers(self:GetCaster(), self:GetAbility(), "modifier_clinkz_strafe_lua", {duration = 1})
	else
		self:Destroy()
	end
end

function modifier_clinkz_strafe_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,

		MODIFIER_PROPERTY_IGNORE_ATTACKSPEED_LIMIT,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
	}
end

function modifier_clinkz_strafe_lua:GetModifierAttackSpeedBonus_Constant()
	return self.attack_speed_bonus
end

function modifier_clinkz_strafe_lua:GetModifierAttackRangeBonus()
	return self.attack_range_bonus
end

function modifier_clinkz_strafe_lua:GetModifierAttackSpeed_Limit()
	if self.special_bonus_unique_npc_dota_hero_clinkz_agi13 then
		return 1
	end
end

function modifier_clinkz_strafe_lua:GetModifierPreAttack_CriticalStrike(keys)
	if (self.special_bonus_unique_npc_dota_hero_clinkz_agi13 and keys.target and not keys.target:IsOther() and not keys.target:IsBuilding() and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber()) and RandomInt(1,100) < 18 then
		return math.max(100, math.min(2500, self:GetParent():GetDisplayAttackSpeed()))
	end
end

modifier_clinkz_strafe_lua_intr = class({})
function modifier_clinkz_strafe_lua_intr:IsHidden() return true end
function modifier_clinkz_strafe_lua_intr:IsDebuff() return false end
function modifier_clinkz_strafe_lua_intr:IsPurgable() return false end
function modifier_clinkz_strafe_lua_intr:IsPurgeException() return false end
function modifier_clinkz_strafe_lua_intr:IsStunDebuff() return false end
function modifier_clinkz_strafe_lua_intr:RemoveOnDeath() return false end
function modifier_clinkz_strafe_lua_intr:DestroyOnExpire() return false end

function modifier_clinkz_strafe_lua_intr:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
	}
end

function modifier_clinkz_strafe_lua_intr:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "attack_speed_bonus" then
			return 1
		end
	end
	return 0
end

function modifier_clinkz_strafe_lua_intr:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "attack_speed_bonus" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "attack_speed_bonus", data.ability_special_level )
            if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str8") then
                value = value + self:GetAbility():GetLevel() * 50
            end
            return value
		end
	end
	return 0
end