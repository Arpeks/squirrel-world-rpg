LinkLuaModifier( "modifier_clinkz_tar_bomb_lua", "heroes/hero_clinkz/clinkz_tar_bomb_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_tar_bomb_lua_aura", "heroes/hero_clinkz/clinkz_tar_bomb_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_tar_bomb_lua_aura_effect", "heroes/hero_clinkz/clinkz_tar_bomb_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_tar_bomb_lua_agi12", "heroes/hero_clinkz/clinkz_tar_bomb_lua.lua", LUA_MODIFIER_MOTION_NONE )

clinkz_tar_bomb_lua = class({})

function clinkz_tar_bomb_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function clinkz_tar_bomb_lua:GetIntrinsicModifierName()
	return "modifier_clinkz_tar_bomb_lua"
end

function clinkz_tar_bomb_lua:OnSpellStart()
	ProjectileManager:CreateTrackingProjectile({
		Target = self:GetCursorTarget(),
		Source = self:GetCaster(),
		Ability = self,	
		
		EffectName = ParticleManager:GetParticleReplacement("particles/units/heroes/hero_clinkz/clinkz_tar_bomb_projectile.vpcf", self:GetCaster()),
		iMoveSpeed = self:GetSpecialValueFor("projectile_speed"),
		iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_2,

		bDrawsOnMinimap = false,
		bVisibleToEnemies = true,
	})
	EmitSoundOn("Hero_Clinkz.TarBomb.Cast", self:GetCaster())
end

function clinkz_tar_bomb_lua:OnProjectileHit(hTarget, vLocation)
	if not hTarget then return end

	ApplyDamageRDA({
		victim = hTarget,
		attacker = self:GetCaster(),
		damage = self:GetSpecialValueFor("impact_damage"),
		damage_type = DAMAGE_TYPE_MAGICAL,
		damage_flags = DOTA_DAMAGE_FLAG_NONE,
		ability = self
	})
	self:GetCaster():ArchersAttack(hTarget)
	local duration = self:GetSpecialValueFor("ground_duration")
	EmitSoundOn("Hero_Clinkz.TarBomb.Target", self:GetCaster())
	
	CreateModifierThinker(self:GetCaster(), self, "modifier_clinkz_tar_bomb_lua_aura", {duration = duration}, vLocation, self:GetCaster():GetTeamNumber(), false)
end

modifier_clinkz_tar_bomb_lua_aura = class({})
function modifier_clinkz_tar_bomb_lua_aura:IsHidden() return true end
function modifier_clinkz_tar_bomb_lua_aura:IsDebuff() return false end
function modifier_clinkz_tar_bomb_lua_aura:IsPurgable() return false end
function modifier_clinkz_tar_bomb_lua_aura:IsPurgeException() return false end
function modifier_clinkz_tar_bomb_lua_aura:IsStunDebuff() return false end
function modifier_clinkz_tar_bomb_lua_aura:RemoveOnDeath() return false end
function modifier_clinkz_tar_bomb_lua_aura:DestroyOnExpire() return true end

function modifier_clinkz_tar_bomb_lua_aura:OnCreated()
	self.radius = self:GetAbility():GetSpecialValueFor("radius")

	if not IsServer() then return end

	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_tar_bomb_thinker.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
	ParticleManager:SetParticleControl(pcf, 0, self:GetParent():GetAbsOrigin())
	ParticleManager:SetParticleControl(pcf, 5, Vector(self.radius, self.radius, self.radius))
end

function modifier_clinkz_tar_bomb_lua_aura:OnDestroy()
	if not IsServer() then return end
	
	UTIL_Remove(self:GetParent())
end

function modifier_clinkz_tar_bomb_lua_aura:IsAura() return true end
function modifier_clinkz_tar_bomb_lua_aura:GetModifierAura() return "modifier_clinkz_tar_bomb_lua_aura_effect" end
function modifier_clinkz_tar_bomb_lua_aura:GetAuraRadius() return self.radius end
function modifier_clinkz_tar_bomb_lua_aura:GetAuraDuration() return 0.5 end
function modifier_clinkz_tar_bomb_lua_aura:GetAuraSearchTeam() return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_clinkz_tar_bomb_lua_aura:GetAuraSearchType() return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC end
function modifier_clinkz_tar_bomb_lua_aura:GetAuraSearchFlags() return 0 end

modifier_clinkz_tar_bomb_lua_aura_effect = class({})
function modifier_clinkz_tar_bomb_lua_aura_effect:IsHidden() return false end
function modifier_clinkz_tar_bomb_lua_aura_effect:IsDebuff() return true end
function modifier_clinkz_tar_bomb_lua_aura_effect:IsPurgable() return false end
function modifier_clinkz_tar_bomb_lua_aura_effect:IsPurgeException() return false end
function modifier_clinkz_tar_bomb_lua_aura_effect:IsStunDebuff() return false end
function modifier_clinkz_tar_bomb_lua_aura_effect:RemoveOnDeath() return true end
function modifier_clinkz_tar_bomb_lua_aura_effect:DestroyOnExpire() return true end

function modifier_clinkz_tar_bomb_lua_aura_effect:GetEffectName()
	return "particles/units/heroes/hero_clinkz/clinkz_tar_bomb_debuff.vpcf"
end

function modifier_clinkz_tar_bomb_lua_aura_effect:GetEffectAttachType()
	return PATTACH_POINT_FOLLOW
end

modifier_clinkz_tar_bomb_lua = class({})
function modifier_clinkz_tar_bomb_lua:IsHidden() return true end
function modifier_clinkz_tar_bomb_lua:IsDebuff() return false end
function modifier_clinkz_tar_bomb_lua:IsPurgable() return false end
function modifier_clinkz_tar_bomb_lua:IsPurgeException() return false end
function modifier_clinkz_tar_bomb_lua:IsStunDebuff() return false end
function modifier_clinkz_tar_bomb_lua:RemoveOnDeath() return false end
function modifier_clinkz_tar_bomb_lua:DestroyOnExpire() return false end

function modifier_clinkz_tar_bomb_lua:OnCreated()
	self.damage_bonus = self:GetAbility():GetSpecialValueFor("damage_bonus")
	if not IsServer() then return end
	self.proj_name = self:GetParent():GetRangedProjectileName()
	self.special_bonus_unique_npc_dota_hero_clinkz_agi12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi12")
end

function modifier_clinkz_tar_bomb_lua:OnRefresh()
	self.damage_bonus = self:GetAbility():GetSpecialValueFor("damage_bonus")
	self.special_bonus_unique_npc_dota_hero_clinkz_agi12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi12")
end

function modifier_clinkz_tar_bomb_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PROCATTACK_BONUS_DAMAGE_PHYSICAL,
		MODIFIER_PROPERTY_PROCATTACK_BONUS_DAMAGE_MAGICAL,
		MODIFIER_PROPERTY_PROJECTILE_NAME,

		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
	}
end

function modifier_clinkz_tar_bomb_lua:GetModifierProcAttack_BonusDamage_Physical(data)
	if data and data.target and data.target:HasModifier("modifier_clinkz_tar_bomb_lua_aura_effect") then
		return self.damage_bonus
	end
end

function modifier_clinkz_tar_bomb_lua:GetModifierProcAttack_BonusDamage_Magical(data)
	if data and data.target and data.target:HasModifier("modifier_clinkz_tar_bomb_lua_aura_effect") then
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int6") then
			ApplyDamageRDA({
				victim = data.target,
				attacker = self:GetCaster(),
				damage = self:GetCaster():GetIntellect(true) * 0.4,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = DOTA_DAMAGE_FLAG_NONE,
				ability = self:GetAbility()
			})
		end
	end
end

function modifier_clinkz_tar_bomb_lua:GetModifierProjectileName()
	if self.change_proj then
		return "particles/units/heroes/hero_clinkz/clinkz_searing_arrow.vpcf"
	else
		return self.proj_name
	end
	return self.proj_name
end

function modifier_clinkz_tar_bomb_lua:CustomOnAttackStart(data)
	if data.attacker ~= self:GetParent() then return end
	if data and data.target and data.target:HasModifier("modifier_clinkz_tar_bomb_lua_aura_effect") then
		self.change_proj = true
	else
		self.change_proj = false
	end
end

function modifier_clinkz_tar_bomb_lua:CustomOnAttackLanded(data)
	if data.attacker ~= self:GetParent() then return end
	if data and data.target and data.target:HasModifier("modifier_clinkz_tar_bomb_lua_aura_effect") then
		if self.special_bonus_unique_npc_dota_hero_clinkz_agi12 then
			data.target:AddNewModifier(data.attacker, self:GetAbility(), "modifier_clinkz_tar_bomb_lua_agi12", {duration = 3, damage = data.original_damage * 0.25})
		end
	end
end

function modifier_clinkz_tar_bomb_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_bonus" then
			return 1
		end
	end
	return 0
end

function modifier_clinkz_tar_bomb_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "damage_bonus" then
			local value = self:GetAbility():GetLevelSpecialValueNoOverride( "damage_bonus", data.ability_special_level )
			if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi11") then
				value = value * math.min(self:GetCaster():GetLevel(), 200)
			end
            return value
		end
	end
	return 0
end

function modifier_clinkz_tar_bomb_lua:IsAura() return self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi11") and self:GetCaster():HasModifier("modifier_clinkz_strafe_lua") or false end
function modifier_clinkz_tar_bomb_lua:GetModifierAura() return "modifier_clinkz_tar_bomb_lua_aura_effect" end
function modifier_clinkz_tar_bomb_lua:GetAuraRadius() return 2000 end
function modifier_clinkz_tar_bomb_lua:GetAuraDuration() return 0.5 end
function modifier_clinkz_tar_bomb_lua:GetAuraSearchTeam() return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_clinkz_tar_bomb_lua:GetAuraSearchType() return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC end
function modifier_clinkz_tar_bomb_lua:GetAuraSearchFlags() return 0 end


modifier_clinkz_tar_bomb_lua_agi12 = class({})
function modifier_clinkz_tar_bomb_lua_agi12:IsHidden() return false end
function modifier_clinkz_tar_bomb_lua_agi12:IsDebuff() return true end
function modifier_clinkz_tar_bomb_lua_agi12:IsPurgable() return true end
function modifier_clinkz_tar_bomb_lua_agi12:IsPurgeException() return false end
function modifier_clinkz_tar_bomb_lua_agi12:IsStunDebuff() return false end
function modifier_clinkz_tar_bomb_lua_agi12:RemoveOnDeath() return true end
function modifier_clinkz_tar_bomb_lua_agi12:DestroyOnExpire() return true end

function modifier_clinkz_tar_bomb_lua_agi12:OnCreated(data)
	if not IsServer() then return end
	
	self.stack_duration = self:GetDuration()
	self.created_time_table = {}
    self.added_damage_table = {}

	if data.damage then
		local damage = math.floor(data.damage)
		self:SetStackCount(damage)
		table.insert(self.created_time_table, GameRules:GetDOTATime(false, false))
		table.insert(self.added_damage_table, damage)
	end
	Timers:CreateTimer(0.1,function()
        local duration = self:GetAbility():GetSpecialValueFor("decay_duration")
		while self.created_time_table[1] and (self.created_time_table[1] + self.stack_duration) <= GameRules:GetDOTATime(false, false) do
            self:SetStackCount(self:GetStackCount() - self.added_damage_table[1])
			table.remove(self.created_time_table, 1)
			table.remove(self.added_damage_table, 1)
		end
		return 0.1
	end)

	self:OnIntervalThink()
	self:StartIntervalThink(1)
end

function modifier_clinkz_tar_bomb_lua_agi12:OnRefresh(data)
	if not IsServer() then return end
	if data.damage then
		local damage = math.floor(data.damage)
		self:SetStackCount(self:GetStackCount() + damage)
		table.insert(self.created_time_table, GameRules:GetDOTATime(false, false))
		table.insert(self.added_damage_table, damage)
	end
end

function modifier_clinkz_tar_bomb_lua_agi12:OnIntervalThink()
	ApplyDamageRDA({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = self:GetStackCount(),
		damage_type = DAMAGE_TYPE_PHYSICAL,
		damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
		ability = self:GetAbility()
	})
end