LinkLuaModifier( "modifier_clinkz_wind_walk_lua", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_wind_walk_lua_archer", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_wind_walk_lua_int9", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_wind_walk_lua_int12_controll", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_wind_walk_lua_int12_attack", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_wind_walk_lua_splitshot", "heroes/hero_clinkz/clinkz_wind_walk_lua.lua", LUA_MODIFIER_MOTION_NONE )


clinkz_wind_walk_lua = class({})
clinkz_wind_walk_lua.archers = clinkz_wind_walk_lua.archers or {}

function clinkz_wind_walk_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 150 + math.min(65000, self:GetCaster():GetIntellect(true) / 30)
end

function clinkz_wind_walk_lua:Spawn()
	if not IsServer() then return end
	local abi = self
	self:GetCaster().ApplyArchers = function(self, caster, ability, modifier_name, params_table)
		local radius = ability:GetSpecialValueFor("strafe_skeleton_radius")
		local pos = self:GetAbsOrigin()
		for _,archer in pairs(abi.archers) do
			if (archer:GetAbsOrigin() - pos):Length2D() < radius then
				archer:AddNewModifier(caster, ability, modifier_name, params_table)
			end
		end
	end

	self:GetCaster().CreateArcher = function(self, position)
		local archer = CreateUnitByName("npc_dota_clinkz_skeleton_archer_custom", position, false, self, self, self:GetTeamNumber())
		archer:SetNeverMoveToClearSpace(true)
		local caster_min = self:GetBaseDamageMin() * (abi:GetSpecialValueFor("skeleton_damage") / 100)
		local caster_max = self:GetBaseDamageMax() * (abi:GetSpecialValueFor("skeleton_damage") / 100)
		if self:FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi9") then
			caster_min = caster_min * 5
			caster_max = caster_max * 5
		end
		if self:FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int12") then
			archer:AddNewModifier(self, abi, "modifier_clinkz_wind_walk_lua_int12_attack", {})
		end
		
		archer:SetBaseDamageMin(caster_min)
		archer:SetBaseDamageMax(caster_max)
		archer.archer_damage = caster_min
		archer:AddAbility("clinkz_tar_bomb_lua"):SetLevel(self:FindAbilityByName("clinkz_tar_bomb_lua"):GetLevel())
		archer:AddNewModifier(self, abi, "modifier_pips", {pips_count = 2})
		archer:AddNewModifier(self, abi, "modifier_clinkz_wind_walk_lua_archer", {
			duration = abi:GetSpecialValueFor("duration"),
			range = self:Script_GetAttackRange() - self:GetBaseAttackRange(),
			cast_count = self.death_cast_caunt or 0
		})
		table.insert(abi.archers, archer)

		if self:HasModifier("modifier_npc_dota_hero_clinkz_buff_1") then
			archer:AddNewModifier(self, abi, "modifier_clinkz_wind_walk_lua_splitshot", {})
		end
		return archer
	end

	self:GetCaster().ArchersAttack = function(self, target)
		for _,archer in pairs(abi.archers) do
			archer:SetForceAttackTarget(target)
		end
	end

	self:GetCaster().GetArchers = function(self, func)
		return abi.archers
	end
end

function clinkz_wind_walk_lua:GetManaCost(iLevel)
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int12") then
		return 0
	end
	return self.BaseClass.GetManaCost(self, iLevel)
end

function clinkz_wind_walk_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int12") then
		return DOTA_ABILITY_BEHAVIOR_TOGGLE
	end
	return self.BaseClass.GetBehavior(self)
end

function clinkz_wind_walk_lua:GetIntrinsicModifierName()
	return "modifier_clinkz_wind_walk_lua_int9"
end

function clinkz_wind_walk_lua:OnSpellStart()
	EmitSoundOn("Hero_Clinkz.WindWalk", self:GetCaster())
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_wind_walk_lua", {duration = self:GetSpecialValueFor("duration")})
end

function clinkz_wind_walk_lua:OnToggle()
	-- if self.int12_archers then
	-- 	UTIL_Remove(self.int12_archers[1])
	-- 	UTIL_Remove(self.int12_archers[2])
	-- 	self.int12_archers = nil
	-- end
	if not self.int12_archers then
		self.int12_archers = {}
		local pos = self:GetCaster():GetAbsOrigin() + self:GetCaster():GetLeftVector() * 100
		local archer = self:GetCaster():CreateArcher(pos)
		archer:AddNewModifier(self:GetCaster(), self, "modifier_clinkz_wind_walk_lua_int12_controll", {side = 1})
		table.insert(self.int12_archers, archer)

		local pos = self:GetCaster():GetAbsOrigin() + self:GetCaster():GetLeftVector() * 100 * -1
		local archer = self:GetCaster():CreateArcher(pos)
		archer:AddNewModifier(self:GetCaster(), self, "modifier_clinkz_wind_walk_lua_int12_controll", {side = 2})
		table.insert(self.int12_archers, archer)
	end
end

modifier_clinkz_wind_walk_lua = class({})
function modifier_clinkz_wind_walk_lua:IsHidden() return false end
function modifier_clinkz_wind_walk_lua:IsDebuff() return false end
function modifier_clinkz_wind_walk_lua:IsPurgable() return false end
function modifier_clinkz_wind_walk_lua:IsPurgeException() return false end
function modifier_clinkz_wind_walk_lua:IsStunDebuff() return false end
function modifier_clinkz_wind_walk_lua:RemoveOnDeath() return true end
function modifier_clinkz_wind_walk_lua:DestroyOnExpire() return true end

function modifier_clinkz_wind_walk_lua:OnCreated()
	self.move_speed_bonus_pct = self:GetAbility():GetSpecialValueFor("move_speed_bonus_pct")
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_burning_army_start.vpcf", PATTACH_POINT, self:GetParent())
	self:AddParticle(pcf, false, false, -1, false, false)
end

function modifier_clinkz_wind_walk_lua:OnDestroy()
	if not IsServer() then return end
	local pos = self:GetCaster():GetAbsOrigin()
	local dir = self:GetCaster():GetLeftVector() * -1 * 50
	local skeleton_count = self:GetSpecialValueFor("skeleton_count")
	for i=1,skeleton_count do
		self:GetCaster():CreateArcher(pos + dir)
		dir = RotatePosition(Vector(0,0,0), QAngle(0, 360/skeleton_count, 0), dir)
	end
end

function modifier_clinkz_wind_walk_lua:CheckState()
	return {
		[MODIFIER_STATE_NOT_ON_MINIMAP_FOR_ENEMIES] = true,
		[MODIFIER_STATE_INVISIBLE] = (self:GetElapsedTime() > 0.6) or nil,
	}
end

function modifier_clinkz_wind_walk_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE
	}
end

function modifier_clinkz_wind_walk_lua:GetModifierInvisibilityLevel()
	return min(self:GetElapsedTime(), 1)
end

function modifier_clinkz_wind_walk_lua:GetModifierMoveSpeedBonus_Percentage()
	return self.move_speed_bonus_pct
end

function modifier_clinkz_wind_walk_lua:CustomOnAbilityFullyCast(data)
	if data.unit ~= self:GetParent() then return end
	if self:GetElapsedTime() < 0.1 then return end
	if data.ability:GetAbilityName() ~= "clinkz_strafe_lua" then
		self:Destroy()
	end
end

function modifier_clinkz_wind_walk_lua:CustomOnAttack(data)
	if data.attacker == self:GetParent() then
		self:Destroy()
	end
end

modifier_clinkz_wind_walk_lua_archer = class({})
function modifier_clinkz_wind_walk_lua_archer:IsHidden() return true end
function modifier_clinkz_wind_walk_lua_archer:IsDebuff() return false end
function modifier_clinkz_wind_walk_lua_archer:IsPurgable() return false end
function modifier_clinkz_wind_walk_lua_archer:IsPurgeException() return false end
function modifier_clinkz_wind_walk_lua_archer:IsStunDebuff() return false end
function modifier_clinkz_wind_walk_lua_archer:RemoveOnDeath() return true end
function modifier_clinkz_wind_walk_lua_archer:DestroyOnExpire() return true end
function modifier_clinkz_wind_walk_lua_archer:CanParentBeAutoAttacked() return true end

function modifier_clinkz_wind_walk_lua_archer:OnCreated(data)
	if not IsServer() then return end
	self.attack_range = data.range
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str9") then
		self:SetStackCount(data.cast_count)
	end
	self:SetHasCustomTransmitterData( true )
	self:SendBuffRefreshToClients()
	EmitSoundOn("Hero_Clinkz.Skeleton_Archer.Spawn", self:GetParent())
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_burning_army.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
	self:AddParticle(pcf, false, false, -1, false, false)
	local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_burning_army_start.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
	self:AddParticle(pcf, false, false, -1, false, false)
end

function modifier_clinkz_wind_walk_lua_archer:OnDestroy()
	if not IsServer() then return end
	EmitSoundOn("Hero_Clinkz.Skeleton_Archer.Destroy", self:GetParent())
	self:GetAbility().archers = table.remove_item(self:GetAbility().archers, self:GetParent())
	UTIL_Remove(self:GetParent())
end

function modifier_clinkz_wind_walk_lua_archer:AddCustomTransmitterData()
	return {
		attack_range = self.attack_range,
	}
end

function modifier_clinkz_wind_walk_lua_archer:HandleCustomTransmitterData( data )
	self.attack_range = data.attack_range
end

function modifier_clinkz_wind_walk_lua_archer:CheckState()
	return {
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true
	}
end

function modifier_clinkz_wind_walk_lua_archer:DeclareFunctions()
	if self:GetDuration() == -1 then
		return {
			MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
			MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE
		}
	end		
	return {
		MODIFIER_PROPERTY_LIFETIME_FRACTION,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE
	}
end

function modifier_clinkz_wind_walk_lua_archer:GetUnitLifetimeFraction()
	return ( self:GetDuration() - self:GetElapsedTime()) / self:GetDuration() 
end

function modifier_clinkz_wind_walk_lua_archer:GetModifierAttackRangeBonus()
	return self.attack_range
end

function modifier_clinkz_wind_walk_lua_archer:GetModifierPreAttack_BonusDamage()
	return self:GetStackCount() * 250
end

function modifier_clinkz_wind_walk_lua_archer:GetEffectName()
	return "particles/units/heroes/hero_clinkz/clinkz_burning_army_ambient.vpcf"
end

function modifier_clinkz_wind_walk_lua_archer:GetEffectAttachType()
	return PATTACH_POINT_FOLLOW
end

modifier_clinkz_wind_walk_lua_int9 = class({})
function modifier_clinkz_wind_walk_lua_int9:IsHidden() return true end
function modifier_clinkz_wind_walk_lua_int9:IsDebuff() return false end
function modifier_clinkz_wind_walk_lua_int9:IsPurgable() return false end
function modifier_clinkz_wind_walk_lua_int9:IsPurgeException() return false end
function modifier_clinkz_wind_walk_lua_int9:IsStunDebuff() return false end
function modifier_clinkz_wind_walk_lua_int9:RemoveOnDeath() return false end
function modifier_clinkz_wind_walk_lua_int9:DestroyOnExpire() return false end

function modifier_clinkz_wind_walk_lua_int9:OnCreated()
	if not IsServer() then return end
	self:StartIntervalThink(1)
end

function modifier_clinkz_wind_walk_lua_int9:OnIntervalThink()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int9") == nil then
		self:Destroy()
		return
	end
	if self:GetAbility():GetLevel() < 5 then
		self:SetStackCount(0)
		return
	end
	local archers = self:GetCaster():GetArchers()
	local c = 0
	local pos = self:GetParent():GetAbsOrigin()
	for _,archer in pairs(archers) do
		if (archer:GetAbsOrigin() - pos):Length2D() < 900 then
			c = c + 1
		end
	end
	self:SetStackCount(c)
end

function modifier_clinkz_wind_walk_lua_int9:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_clinkz_wind_walk_lua_int9:GetModifierAttackSpeedBonus_Constant()
	return 200 * math.min(self:GetStackCount(), 2)
end

modifier_clinkz_wind_walk_lua_int12_controll = class({})
function modifier_clinkz_wind_walk_lua_int12_controll:IsHidden() return true end
function modifier_clinkz_wind_walk_lua_int12_controll:IsDebuff() return false end
function modifier_clinkz_wind_walk_lua_int12_controll:IsPurgable() return false end
function modifier_clinkz_wind_walk_lua_int12_controll:IsPurgeException() return false end
function modifier_clinkz_wind_walk_lua_int12_controll:IsStunDebuff() return false end
function modifier_clinkz_wind_walk_lua_int12_controll:RemoveOnDeath() return false end
function modifier_clinkz_wind_walk_lua_int12_controll:DestroyOnExpire() return false end

function modifier_clinkz_wind_walk_lua_int12_controll:OnCreated(data)
	if not IsServer() then return end
	self.side = data.side
	self.parent = self:GetParent()
	self.caster = self:GetCaster()
	self.parent:SetBaseAttackTime(self.caster:GetBaseAttackTime())
	self.attack_speed = self.caster:GetDisplayAttackSpeed() * 0.25
	self.tick = 0
	self:StartIntervalThink(0.01)
	self.parent:FindModifierByName("modifier_clinkz_wind_walk_lua_archer"):SetDuration(-1, true)

	self:SetHasCustomTransmitterData( true )
	self:SendBuffRefreshToClients()
end

function modifier_clinkz_wind_walk_lua_int12_controll:AddCustomTransmitterData()
	return {
		attack_speed = self.attack_speed,
	}
end

function modifier_clinkz_wind_walk_lua_int12_controll:HandleCustomTransmitterData( data )
	self.attack_speed = data.attack_speed
end

function modifier_clinkz_wind_walk_lua_int12_controll:OnIntervalThink()
	if self:GetAbility():GetToggleState() then
		if self.caster:IsAttacking() and self.parent:AttackReady() then
			self.parent:SetForceAttackTarget(self.caster:GetAttackTarget())
		else
			self.parent:SetForceAttackTarget(nil)
		end

		self.parent:SetForwardVector(self.caster:GetForwardVector())

		if self.caster:IsMoving() then
			self.parent:StartGesture(ACT_DOTA_RUN)
		else
			self.parent:FadeGesture(ACT_DOTA_RUN)
		end
	end

	if self.side == 1 then
		self.parent:SetAbsOrigin(self.caster:GetAbsOrigin() + self.caster:GetLeftVector() * 100)
	else
		self.parent:SetAbsOrigin(self.caster:GetAbsOrigin() + self.caster:GetLeftVector() * 100 * -1)
	end
	if not self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int12") then
		self:GetAbility().int12_archers = nil
		UTIL_Remove(self:GetParent())
	end

	self.tick = self.tick + 1
	if self.tick % 100 == 0 then
		self.attack_speed = self.caster:GetDisplayAttackSpeed() * 0.25
		self:SendBuffRefreshToClients()
		local caster_min = self:GetCaster():GetBaseDamageMin()
		local caster_max = self:GetCaster():GetBaseDamageMax()
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi9") then
			caster_min = caster_min * 5
			caster_max = caster_max * 5
		end
		if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str9") then
			local death_cast_caunt = self:GetCaster().death_cast_caunt or 0
			caster_min = caster_min + 250 * death_cast_caunt
			caster_max = caster_max + 250 * death_cast_caunt
		end
		self:GetParent():SetBaseDamageMin(caster_min)
		self:GetParent():SetBaseDamageMax(caster_max)
		self:GetParent().archer_damage = caster_min
	end
end

function modifier_clinkz_wind_walk_lua_int12_controll:CheckState()
	return {
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNSELECTABLE] = false,
	}
end

function modifier_clinkz_wind_walk_lua_int12_controll:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DISABLE_AUTOATTACK,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_clinkz_wind_walk_lua_int12_controll:GetDisableAutoAttack()
	return self:GetAbility():GetToggleState() and 1 or 0
end

function modifier_clinkz_wind_walk_lua_int12_controll:GetModifierAttackSpeedBonus_Constant()
	return self.attack_speed
end

modifier_clinkz_wind_walk_lua_int12_attack = class({})

function modifier_clinkz_wind_walk_lua_int12_attack:IsHidden() return true end

function modifier_clinkz_wind_walk_lua_int12_attack:IsPurgable() return false end

function modifier_clinkz_wind_walk_lua_int12_attack:OnCreated(data)
	if not IsServer() then return end
	self.damage = 0
	self:StartIntervalThink(1)
end

function modifier_clinkz_wind_walk_lua_int12_attack:OnIntervalThink()
    self.damage = self:GetParent().archer_damage
	
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int13") then
		self.damage = self.damage * (self:GetCaster():GetSpellAmplification(false) + 1) * 0.4
	end
end

function modifier_clinkz_wind_walk_lua_int12_attack:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE
	}
end

function modifier_clinkz_wind_walk_lua_int12_attack:GetModifierDamageOutgoing_Percentage()
	return -100
end

function modifier_clinkz_wind_walk_lua_int12_attack:CustomOnAttackLanded(data)
	if data.attacker ~= self:GetParent() then return end
	if data and data.target then
		ApplyDamageRDA({
			victim = data.target,
			attacker = self:GetCaster(),
			damage = self.damage,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
			ability = self:GetAbility()
		})
	end
end

modifier_clinkz_wind_walk_lua_splitshot = class({})

function modifier_clinkz_wind_walk_lua_splitshot:IsPurgable()
	return false
end

function modifier_clinkz_wind_walk_lua_splitshot:CustomOnAttack(keys)
    if keys.no_attack_cooldown then
        return
    end
    if keys.attacker == self:GetParent() then	
	
        local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), keys.target:GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NOT_MAGIC_IMMUNE_ALLIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false)		
        local nTargetNumber = 0		
        for _, hEnemy in pairs(enemies) do
            if hEnemy ~= keys.target then

                self:GetParent():PerformAttack(hEnemy, true, true, true, true, true, false, false)		
                nTargetNumber = nTargetNumber + 1
                
                if nTargetNumber >= 3 then
                    break
                end
            end
        end
    end 
end