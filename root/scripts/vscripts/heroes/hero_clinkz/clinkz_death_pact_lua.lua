LinkLuaModifier( "modifier_clinkz_death_pact_lua", "heroes/hero_clinkz/clinkz_death_pact_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_death_pact_lua_isnt", "heroes/hero_clinkz/clinkz_death_pact_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_death_pact_str12_damage_aura", "heroes/hero_clinkz/clinkz_death_pact_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_clinkz_death_pact_count_lua", "heroes/hero_clinkz/clinkz_death_pact_lua.lua", LUA_MODIFIER_MOTION_NONE )

clinkz_death_pact_lua = class({})

function clinkz_death_pact_lua:GetManaCost(iLevel)
	if not self:GetCaster():IsRealHero() then return 0 end
    return 100 + math.min(65000, self:GetCaster():GetIntellect(true) / 100)
end

function clinkz_death_pact_lua:GetAOERadius()
	return 300
end

function clinkz_death_pact_lua:GetBehavior()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str13") then
		return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
	end
	return self.BaseClass.GetBehavior(self)
end

function clinkz_death_pact_lua:CastFilterResultTarget(hTarget)
	if hTarget:GetClassname() == "npc_dota_clinkz_skeleton_archer" then
		return UF_SUCCESS
	end
	return UnitFilter(hTarget, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_CAN_BE_SEEN, self:GetCaster():GetTeamNumber())
end

function clinkz_death_pact_lua:OnSpellStart()
	self:GetCaster().death_cast_caunt = self:GetCaster().death_cast_caunt and self:GetCaster().death_cast_caunt + 1 or 0
	local special_bonus_unique_npc_dota_hero_clinkz_int7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int7")
	if self:GetCursorTarget() then
		-- self:GetCaster():CreateArcher(self:GetCursorPosition(), self)
		self:GetCursorTarget():Kill(self, self:GetCaster())
		local part = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_death_pact.vpcf", PATTACH_ABSORIGIN, self:GetCursorTarget())
		ParticleManager:SetParticleControlEnt(part, 1, self:GetCaster(), PATTACH_ABSORIGIN, "", self:GetCaster():GetAbsOrigin(), true)
		ParticleManager:ReleaseParticleIndex(part)

		self:GetCaster():EmitSound("Hero_Clinkz.DeathPact.Cast")
		self:GetCursorTarget():EmitSound("Hero_Clinkz.DeathPact")

		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_death_pact_lua", {duration = self:GetSpecialValueFor("duration")})
		if special_bonus_unique_npc_dota_hero_clinkz_int7 then
			local mod = self:GetCaster():FindModifierByName("modifier_clinkz_death_pact_lua_isnt")
			if mod then
				mod:IncrementStackCount()
			end
		end
	else
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCursorPosition(), nil, 300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for i = 1, math.min(#units, 3) do
			local unit  = units[i]
			if not unit:IsAncient() and not unit:IsBoss() then
				local part = ParticleManager:CreateParticle("particles/units/heroes/hero_clinkz/clinkz_death_pact.vpcf", PATTACH_ABSORIGIN, unit)
				ParticleManager:SetParticleControlEnt(part, 1, self:GetCaster(), PATTACH_ABSORIGIN, "", self:GetCaster():GetAbsOrigin(), true)
				ParticleManager:ReleaseParticleIndex(part)
				
				self:GetCaster():EmitSound("Hero_Clinkz.DeathPact.Cast")
				unit:EmitSound("Hero_Clinkz.DeathPact")
				
				self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_death_pact_lua", {duration = self:GetSpecialValueFor("duration"), addition_hp = unit:GetMaxHealth() * 0.03})
				if special_bonus_unique_npc_dota_hero_clinkz_int7 then
					local mod = self:GetCaster():FindModifierByName("modifier_clinkz_death_pact_lua_isnt")
					if mod then
						mod:IncrementStackCount()
					end
				end
				unit:Kill(self, self:GetCaster())
			end
		end
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str6") then
		self:GetCaster():ModifyIntellect(30)
	end
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str12") then
		local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCursorPosition(), nil, 600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
		for _,unit in pairs(units) do
			ApplyDamageRDA({
				victim = unit,
				attacker = self:GetCaster(),
				damage = self:GetCaster().death_cast_caunt * 100,
				damage_type = DAMAGE_TYPE_MAGICAL,
				damage_flags = DOTA_DAMAGE_FLAG_NONE,
				ability = self
			})
		end
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_clinkz_death_pact_str12_damage_aura", {duration = self:GetSpecialValueFor("duration")})
	end
end

function clinkz_death_pact_lua:GetIntrinsicModifierName()
	return "modifier_clinkz_death_pact_lua_isnt"
end

modifier_clinkz_death_pact_lua = class({})
function modifier_clinkz_death_pact_lua:IsHidden() return false end
function modifier_clinkz_death_pact_lua:IsDebuff() return false end
function modifier_clinkz_death_pact_lua:IsPurgable() return false end
function modifier_clinkz_death_pact_lua:IsPurgeException() return false end
function modifier_clinkz_death_pact_lua:IsStunDebuff() return false end
function modifier_clinkz_death_pact_lua:RemoveOnDeath() return true end
function modifier_clinkz_death_pact_lua:DestroyOnExpire() return true end
function modifier_clinkz_death_pact_lua:GetAttributes() return self.special_bonus_unique_npc_dota_hero_clinkz_str10 and MODIFIER_ATTRIBUTE_MULTIPLE or MODIFIER_ATTRIBUTE_NONE end

function modifier_clinkz_death_pact_lua:OnCreated(data)
	self.health_gain = self:GetAbility():GetSpecialValueFor("health_gain")
	self.special_bonus_unique_npc_dota_hero_clinkz_int10 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int10")
	self.special_bonus_unique_npc_dota_hero_clinkz_str7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str7")
	self.special_bonus_unique_npc_dota_hero_clinkz_str10 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str10")
	self.special_bonus_unique_npc_dota_hero_clinkz_str11 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str11")
	self.special_bonus_unique_npc_dota_hero_clinkz_agi7 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi7")
	self.special_bonus_unique_npc_dota_hero_clinkz_agi10 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi10")
	self.special_bonus_unique_npc_dota_hero_clinkz_str12 = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str12")
	if not IsServer() then return end
	self.addition_hp = data.addition_hp or 0
	self.spell_amp = self:GetCaster():GetSpellAmplification(false) * 0.1 + 1
	self:SetHasCustomTransmitterData( true )
	self:SendBuffRefreshToClients()

	self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_clinkz_death_pact_count_lua", {})
end

function modifier_clinkz_death_pact_lua:AddCustomTransmitterData()
	return {
		spell_amp = self.spell_amp,
		addition_hp = self.addition_hp,
	}
end

function modifier_clinkz_death_pact_lua:HandleCustomTransmitterData( data )
	self.spell_amp = data.spell_amp
	self.addition_hp = data.addition_hp
end

function modifier_clinkz_death_pact_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_HEALTH_BONUS,

		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
		MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
	}
end

function modifier_clinkz_death_pact_lua:GetModifierHealthBonus()
	return self.health_gain + self.addition_hp
end

function modifier_clinkz_death_pact_lua:GetModifierConstantHealthRegen()
	if self.special_bonus_unique_npc_dota_hero_clinkz_str7 then
		return self.health_gain * 0.07
	end
end

function modifier_clinkz_death_pact_lua:GetModifierMagicalResistanceBonus()
	if self.special_bonus_unique_npc_dota_hero_clinkz_str11 then
		return 65
	end
end

function modifier_clinkz_death_pact_lua:GetEffectName()
	return "particles/units/heroes/hero_clinkz/clinkz_death_pact_buff.vpcf"
end

function modifier_clinkz_death_pact_lua:GetEffectAttachType()
	return PATTACH_POINT_FOLLOW
end

modifier_clinkz_death_pact_lua_isnt = class({})
function modifier_clinkz_death_pact_lua_isnt:IsHidden() return self:GetStackCount() == 0 end
function modifier_clinkz_death_pact_lua_isnt:IsDebuff() return false end
function modifier_clinkz_death_pact_lua_isnt:IsPurgable() return false end
function modifier_clinkz_death_pact_lua_isnt:IsPurgeException() return false end
function modifier_clinkz_death_pact_lua_isnt:IsStunDebuff() return false end
function modifier_clinkz_death_pact_lua_isnt:RemoveOnDeath() return false end
function modifier_clinkz_death_pact_lua_isnt:DestroyOnExpire() return false end

function modifier_clinkz_death_pact_lua_isnt:OnCreated( kv )
	if not IsServer() then return end
	self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_clinkz_death_pact_count_lua", {})
end

function modifier_clinkz_death_pact_lua_isnt:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE
	}
end

function modifier_clinkz_death_pact_lua_isnt:GetModifierBonusStats_Intellect()
	if self.lock then
		return 0
	end
	self.lock = true
	local int = self:GetCaster():GetIntellect(true)
	self.lock = false
	return int * self:GetStackCount() * 0.005
end

function modifier_clinkz_death_pact_lua_isnt:GetModifierAttackRangeBonus()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int8") then
		return 200
	end
end

function modifier_clinkz_death_pact_lua_isnt:GetModifierBaseAttack_BonusDamage()
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int11") then
		local PrimaryAttribute = self:GetCaster():GetPrimaryAttribute()
		if PrimaryAttribute == DOTA_ATTRIBUTE_STRENGTH then
			return self:GetCaster():GetStrength() * 0.5
		elseif PrimaryAttribute == DOTA_ATTRIBUTE_AGILITY then
			return self:GetCaster():GetAgility() * 0.5
		elseif PrimaryAttribute == DOTA_ATTRIBUTE_INTELLECT then
			return self:GetCaster():GetIntellect(true) * 0.5
		end
	end
end

modifier_clinkz_death_pact_str12_damage_aura = class({})

function modifier_clinkz_death_pact_str12_damage_aura:IsHidden() return false end

function modifier_clinkz_death_pact_str12_damage_aura:OnCreated()
	if IsServer() then
		self:StartIntervalThink(1)
	end
end

function modifier_clinkz_death_pact_str12_damage_aura:OnIntervalThink()
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,unit in pairs(units) do
		ApplyDamageRDA({
			victim = unit,
			attacker = self:GetCaster(),
			damage = self:GetCaster().death_cast_caunt * 200,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flags = DOTA_DAMAGE_FLAG_NONE,
			ability = self:GetAbility()
		})
	end
end

modifier_clinkz_death_pact_count_lua = class({})

function modifier_clinkz_death_pact_count_lua:IsHidden() return false end

function modifier_clinkz_death_pact_count_lua:IsPurgable() return false end

function modifier_clinkz_death_pact_count_lua:RemoveOnDeath() return false end

function modifier_clinkz_death_pact_count_lua:OnCreated( kv )
	
end

function modifier_clinkz_death_pact_count_lua:OnRefresh( kv )
	self:SetStackCount( self:GetStackCount() + 1)
end

function modifier_clinkz_death_pact_count_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
	}
end

function modifier_clinkz_death_pact_count_lua:GetModifierBaseAttack_BonusDamage()
	local bonus = 0
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi7") then
		local count = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str12") and self:GetStackCount() or 1
		local health_gain = self:GetAbility():GetSpecialValueFor("health_gain")
		bonus = count * (health_gain * 0.8) * (1 + self:GetCaster():GetSpellAmplification(false) * 0.07)
	end
	return bonus
end

function modifier_clinkz_death_pact_count_lua:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_MAGICAL and self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_agi7") then return -999 end
end

function modifier_clinkz_death_pact_count_lua:GetModifierSpellAmplify_Percentage()
	local bonus = 0
	if self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_int10") then
		local count = self:GetCaster():FindAbilityByName("special_bonus_unique_npc_dota_hero_clinkz_str12") and self:GetStackCount() or 1
		bonus = count * self:GetCaster():GetIntellect(true) * 0.01
	end
	return bonus
end