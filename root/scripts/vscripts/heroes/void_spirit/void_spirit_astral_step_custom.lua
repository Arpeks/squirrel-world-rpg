LinkLuaModifier( "modifier_void_spirit_astral_step_custom", "heroes/void_spirit/void_spirit_astral_step_custom.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if void_spirit_astral_step_custom == nil then
	void_spirit_astral_step_custom = class({})
end
function void_spirit_astral_step_custom:GetIntrinsicModifierName()
	return "modifier_void_spirit_astral_step_custom"
end
---------------------------------------------------------------------
--Modifiers
if modifier_void_spirit_astral_step_custom == nil then
	modifier_void_spirit_astral_step_custom = class({})
end
function modifier_void_spirit_astral_step_custom:OnCreated(params)
	if IsServer() then
	end
end
function modifier_void_spirit_astral_step_custom:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_void_spirit_astral_step_custom:OnDestroy()
	if IsServer() then
	end
end
function modifier_void_spirit_astral_step_custom:DeclareFunctions()
	return {
	}
end