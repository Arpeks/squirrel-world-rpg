"DOTAAbilities"
{
	"sand_caustic"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_sand/sand_caustic/sand_caustic"
		"AbilityTextureName"			"sandking_caustic_finale"
		"MaxLevel"						"15"

		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"

        "AbilityValues"
        {
            "caustic_finale_radius"
            {
                "value"                 "400"
				"affected_by_aoe_increase"	"1"
            }
            "caustic_finale_damage"
            {
                "value"                 "30 60 90 120 150 180 210 240 270 300 330 360 390 420 450"
            }
            "caustic_finale_duration"
            {
                "value"                 "5"
            }
        }
	}
}