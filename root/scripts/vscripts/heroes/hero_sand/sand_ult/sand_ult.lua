sandking_custom_trembling_waves = class({})

function sandking_custom_trembling_waves:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function sandking_custom_trembling_waves:GetIntrinsicModifierName()
	return "modifier_sand_ult"
end

modifier_sand_ult = class({})

LinkLuaModifier("modifier_sand_ult", "heroes/hero_sand/sand_ult/sand_ult.lua", LUA_MODIFIER_MOTION_NONE)

--Classifications template
function modifier_sand_ult:IsHidden()
	return true
end

function modifier_sand_ult:IsDebuff()
	return false
end

function modifier_sand_ult:IsPurgable()
	return false
end

function modifier_sand_ult:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_sand_ult:IsStunDebuff()
	return false
end

function modifier_sand_ult:RemoveOnDeath()
	return false
end

function modifier_sand_ult:DestroyOnExpire()
	return false
end

function modifier_sand_ult:OnCreated()
	if not IsServer() then
		return
	end
	self:StartIntervalThink(0.333)
end

function modifier_sand_ult:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_TAKEDAMAGE
	}
end

function modifier_sand_ult:CustomOnTakeDamage(params)
	if not IsServer() then
		return
	end
	if not FlagExist(params.damage_flags, DOTA_DAMAGE_FLAG_REFLECTION) then
		if params.unit == self:GetParent() and params.attacker ~= self:GetParent() then
			self:sandking_waves()
		end
	end
end

function FlagExist(a,b)
	local p,c,d=1,0,b
	while a>0 and b>0 do
		local ra,rb=a%2,b%2
		if ra+rb>1 then c=c+p end
		a,b,p=(a-ra)/2,(b-rb)/2,p*2
	end
	return c==d
end

function modifier_sand_ult:OnIntervalThink()
	if self:GetCaster():HasModifier("modifier_npc_dota_hero_sand_king_buff_1") then
		self:MakeDamage()
	end
	if self:GetCaster():FindAbilityByName("npc_dota_hero_sand_king_str8") ~= nil then
		self:IncrementStackCount()
		if self:GetStackCount() >= 3 then
			self:SetStackCount(0)
			self:MakeDamage()
		end
	end
end

LinkLuaModifier( "modifier_sand_caustic_debuff", "heroes/hero_sand/sand_caustic/modifier_sand_caustic_debuff", LUA_MODIFIER_MOTION_NONE )

function modifier_sand_ult:sandking_waves()
	local caster = self:GetCaster()
	local ability = self:GetAbility()
	local chance = ability:GetSpecialValueFor("chance")

	if caster:FindAbilityByName("npc_dota_hero_sand_king_int9") ~= nil then 
        chance = 15
    end
	
	if caster:FindAbilityByName("npc_dota_hero_sand_king_agi_last") ~= nil then 
        chance = 101
    end
	
	if RandomInt(1,100) < chance then
		self:MakeDamage()
	end
end

function modifier_sand_ult:GetDamageValue()

end

function modifier_sand_ult:MakeDamage()
	local caster = self:GetCaster()
	local ability = self:GetAbility()

	local sand_ult_damage = ability:GetSpecialValueFor("damage")

	if caster:FindAbilityByName("npc_dota_hero_sand_king_str11") ~= nil then 
		sand_ult_damage = ability:GetSpecialValueFor("damage") + caster:GetStrength() * 0.75
	end
	
	if caster:FindAbilityByName("npc_dota_hero_sand_king_str_last") ~= nil then 
		sand_ult_damage = sand_ult_damage + caster:GetStrength() * 2
	end
	
	if caster:FindAbilityByName("npc_dota_hero_sand_king_agi9") ~= nil then 
		local agi = caster:GetAgility()
		local int = caster:GetIntellect(true)
		local str = caster:GetStrength()
		local high = (agi > int and agi > str and agi) or (int > agi and int > str and int) or str
		sand_ult_damage = sand_ult_damage + high
	end

	local radius = ability:GetSpecialValueFor("radius")

	if caster:FindAbilityByName("special_bonus_unique_npc_dota_hero_sand_king_agi50") ~= nil then 
		radius = radius + 300
	end

	local damage_table = {}
		damage_table.attacker = caster
		damage_table.damage = sand_ult_damage
		damage_table.ability = ability
		damage_table.damage_type = DAMAGE_TYPE_MAGICAL
		local units = FindUnitsInRadius(caster:GetTeam(), caster:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, 0, 0, false)
		for _, unit in ipairs(units) do
			if unit:IsAlive() then
			
				if caster:FindAbilityByName("npc_dota_hero_sand_king_agi8") ~= nil then 
					ability2 = caster:FindAbilityByName("sand_caustic") 
					if ability2 ~= nil and ability2:IsTrained() then 
						unit:AddNewModifier( caster, ability, "modifier_sand_caustic_debuff", { duration = 5 } )
					end
				end
				
				damage_table.victim = unit
				ApplyDamageRDA(damage_table)
			end
		end
		-- StartSoundEvent( "Ability.SandKing_Epicenter.spell", caster )
		-- Timers:CreateTimer(0.1, function() 
		-- StopSoundEvent( "Ability.SandKing_Epicenter.spell", caster )
		-- end)
		
		Timers:CreateTimer(0.01, function() 
			caster.ShieldParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_sandking/sandking_epicenter.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster)
			ParticleManager:SetParticleControl(caster.ShieldParticle, 1, Vector(radius,0,radius))


			-- Proper Particle attachment courtesy of BMD. Only PATTACH_POINT_FOLLOW will give the proper shield position
			ParticleManager:SetParticleControlEnt(caster.ShieldParticle, 0, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
		end)
end


function talent(keys)
	local caster = keys.caster
	local abil = caster:FindAbilityByName("npc_dota_hero_sand_king_str8")
	if abil ~= nil then 
		local ability = keys.ability
		local radius = ability:GetSpecialValueFor("radius")
		sand_ult_damage = ability:GetSpecialValueFor("damage")
		
		if caster:FindAbilityByName("npc_dota_hero_sand_king_str11") ~= nil then 
		sand_ult_damage = ability:GetSpecialValueFor("damage") + caster:GetStrength() * 0.5
		end

		if caster:FindAbilityByName("npc_dota_hero_sand_king_str_last") ~= nil then 
		sand_ult_damage = sand_ult_damage + caster:GetStrength() * 2
		end
		
		if caster:FindAbilityByName("npc_dota_hero_sand_king_agi9") ~= nil then
			if caster:GetIntellect(true) >= caster:GetAgility() and caster:GetIntellect(true) > caster:GetStrength() then
				sand_ult_damage = sand_ult_damage + caster:GetIntellect(true)
			elseif caster:GetAgility() >= caster:GetIntellect(true) and caster:GetAgility() > caster:GetStrength() then
				sand_ult_damage = sand_ult_damage + caster:GetAgility()
			elseif caster:GetStrength() >= caster:GetIntellect(true) and caster:GetStrength() > caster:GetAgility() then
				sand_ult_damage = sand_ult_damage + caster:GetStrength()
			end
		end

		local damage_table = {}
		damage_table.attacker = caster
		damage_table.damage = sand_ult_damage
		damage_table.ability = ability
		damage_table.damage_type = DAMAGE_TYPE_MAGICAL
		local units = FindUnitsInRadius(caster:GetTeam(), caster:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, 0, 0, false)
		for _, unit in ipairs(units) do
		if unit:IsAlive() then
			damage_table.victim = unit
			ApplyDamageRDA(damage_table)
		end
		end
		-- StartSoundEvent( "Ability.SandKing_Epicenter.spell", caster )
		-- Timers:CreateTimer(0.1, function() 
		-- StopSoundEvent( "Ability.SandKing_Epicenter.spell", caster )
		-- end)
			Timers:CreateTimer(0.01, function() 
				caster.ShieldParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_sandking/sandking_epicenter.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster)
				ParticleManager:SetParticleControl(caster.ShieldParticle, 1, Vector(radius,0,radius))
				ParticleManager:SetParticleControlEnt(caster.ShieldParticle, 0, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
			end)
		end
end