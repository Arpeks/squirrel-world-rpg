modifier_npc_dota_hero_medusa_buff_1 = class({})

function modifier_npc_dota_hero_medusa_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_medusa_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_medusa_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_medusa_buff_1:DeclareFunctions()
    return
    {
        MODIFIER_PROPERTY_MANA_BONUS,
    }
end

function modifier_npc_dota_hero_medusa_buff_1:GetModifierManaBonus()
    local ability = self:GetParent():FindAbilityByName("medusa_mana_shield_lua")
    local bonus = self:GetParent():GetIntellect(true) * 4 + ability:GetLevel() * 100
    return bonus
end

function modifier_npc_dota_hero_medusa_buff_1:CustomOnAttack(params)
	if IsServer() then
		if params.attacker == self:GetParent() then
			self:GetParent():GiveMana( self:GetParent():GetMaxMana() * 0.1 )
		end
	end
end