modifier_hero_drow_ranger_buff_1 = class({})

function modifier_hero_drow_ranger_buff_1:IsHidden()
    return true
end

function modifier_hero_drow_ranger_buff_1:IsPurgable()
    return false
end

function modifier_hero_drow_ranger_buff_1:RemoveOnDeath()
    return false
end