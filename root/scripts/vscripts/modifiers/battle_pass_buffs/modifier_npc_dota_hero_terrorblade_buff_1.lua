modifier_npc_dota_hero_terrorblade_buff_1 = class({})

function modifier_npc_dota_hero_terrorblade_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_terrorblade_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_terrorblade_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_terrorblade_buff_1:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE,
    }
end

function modifier_npc_dota_hero_terrorblade_buff_1:GetModifierPercentageCooldown()
    return 50
end