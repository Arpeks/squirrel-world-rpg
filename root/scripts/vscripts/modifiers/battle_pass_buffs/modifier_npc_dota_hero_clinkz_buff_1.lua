modifier_npc_dota_hero_clinkz_buff_1 = class({})

function modifier_npc_dota_hero_clinkz_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_clinkz_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_clinkz_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_clinkz_buff_1:CustomOnAttack(keys)
    if keys.no_attack_cooldown then
        return
    end
    if keys.attacker == self:GetParent() then	
	
        local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), keys.target:GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NOT_MAGIC_IMMUNE_ALLIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false)		
        local nTargetNumber = 0		
        for _, hEnemy in pairs(enemies) do
            if hEnemy ~= keys.target then

                self:GetParent():PerformAttack(hEnemy, true, true, true, true, true, false, false)		
                nTargetNumber = nTargetNumber + 1
                
                if nTargetNumber >= 3 then
                    break
                end
            end
        end
    end 
end