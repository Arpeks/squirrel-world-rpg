modifier_hero_sniper_buff_1 = class({})

function modifier_hero_sniper_buff_1:IsHidden()
    return true
end

function modifier_hero_sniper_buff_1:IsPurgable()
    return false
end

function modifier_hero_sniper_buff_1:RemoveOnDeath()
    return false
end

function modifier_hero_sniper_buff_1:OnCreated( kv )

end

function modifier_hero_sniper_buff_1:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
        -- MODIFIER_EVENT_ON_ATTACK_ALLIED,
        -- MODIFIER_EVENT_ON_ORDER,
        MODIFIER_PROPERTY_ALWAYS_ALLOW_ATTACK,
        -- MODIFIER_EVENT_ON_ATTACK,
        -- MODIFIER_EVENT_ON_ATTACKED,
	}
	return funcs
end

function modifier_hero_sniper_buff_1:GetModifierAttackRangeBonus()
    if self.attack and self:GetParent():AttackReady() then
        return 999999
    end
	return 0
end

function modifier_hero_sniper_buff_1:CustomOnAttackAllied(keys)
    print("attack1")
    -- DeepPrintTable(keys)
end

function modifier_hero_sniper_buff_1:CustomOnAttack(keys)
    -- if keys.no_attack_cooldown or self:GetParent():AttackReady() == false or  then
    --     self.attack = false
    -- end
    -- if self:GetParent():GetAttackTarget() ~= keys.target then
    --     self.attack = false
    -- end
end

function modifier_hero_sniper_buff_1:CustomOnAttacked(keys)
    self.attack = false
end

function modifier_hero_sniper_buff_1:GetAlwaysAllowAttack(keys)
    local target = self:GetParent():GetAggroTarget()
    local allies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), target:GetAbsOrigin(), nil, 400, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    if #allies > 0 and (self:GetParent():GetAbsOrigin()-target:GetAbsOrigin()):Length2D() > self:GetParent():Script_GetAttackRange() and self:GetParent():AttackReady() then
        self.attack = true
    else
        self.attack = false
    end
end

function modifier_hero_sniper_buff_1:CustomOnOrder(data)
    if data.unit~=self:GetParent() then return end
	-- DeepPrintTable(data)
end