modifier_npc_dota_hero_nevermore_buff_1 = class({})

function modifier_npc_dota_hero_nevermore_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_nevermore_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_nevermore_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_nevermore_buff_1:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS,
    }
end

function modifier_npc_dota_hero_nevermore_buff_1:GetModifierProjectileSpeedBonus()
    return 1000
end