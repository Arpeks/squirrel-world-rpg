modifier_hero_sky_mage_buff_1 = class({})

function modifier_hero_sky_mage_buff_1:IsHidden()
    return true
end

function modifier_hero_sky_mage_buff_1:IsPurgable()
    return false
end

function modifier_hero_sky_mage_buff_1:RemoveOnDeath()
    return false
end