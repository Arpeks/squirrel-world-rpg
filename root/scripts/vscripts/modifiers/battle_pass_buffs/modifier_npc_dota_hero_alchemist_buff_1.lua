modifier_npc_dota_hero_alchemist_buff_1 = class({})

function modifier_npc_dota_hero_alchemist_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_alchemist_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_alchemist_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_alchemist_buff_1:OnCreated( kv )
    if IsServer() then
        -- self:StartIntervalThink(30)
    end
end

function modifier_npc_dota_hero_alchemist_buff_1:OnIntervalThink()
    local caster = self:GetCaster()
    local r = {0,1,3,4,6}
    CreateRune(Vector(caster:GetAbsOrigin().x + RandomInt(-100, 100), caster:GetAbsOrigin().y + RandomInt(-100, 100), caster:GetAbsOrigin().z), r[RandomInt(1, #r)])
end