modifier_npc_dota_hero_gyrocopter_buff_1 = class({})

function modifier_npc_dota_hero_gyrocopter_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_gyrocopter_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_gyrocopter_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_gyrocopter_buff_1:OnCreated( kv )
    self.parent = self:GetParent()
end

function modifier_npc_dota_hero_gyrocopter_buff_1:DeclareFunctions()
    local funcs = {
        -- MODIFIER_EVENT_ON_ATTACK
    }

    return funcs
end

function modifier_npc_dota_hero_gyrocopter_buff_1:CustomOnAttack( params )
    if IsServer() then
        if self.parent == params.attacker then
            self:IncrementStackCount()
            if self:GetStackCount() >= 3 then
                self:SetStackCount(0)
                local ability = self.parent:FindAbilityByName("gyrocopter_flak_cannon_lua")
                if ability and ability:GetLevel() > 0 then
                    local mod = self.parent:FindModifierByName("modifier_gyrocopter_flak_cannon_lua")
                    if not mod then
                        mod = self.parent:AddNewModifier(self.parent, ability, "modifier_gyrocopter_flak_cannon_lua", {})
                    end
                    mod:IncrementStackCount()
                end
            end
        end
    end
end