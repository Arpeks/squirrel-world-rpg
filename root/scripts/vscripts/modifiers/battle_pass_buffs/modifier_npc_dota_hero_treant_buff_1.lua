modifier_npc_dota_hero_treant_buff_1 = class({})

function modifier_npc_dota_hero_treant_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_treant_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_treant_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_treant_buff_1:OnCreated( kv )
    if IsServer() then
        GameRules:SetTreeRegrowTime(5)
    end
end

function modifier_npc_dota_hero_treant_buff_1:OnDestroy( kv )
    if IsServer() then
        GameRules:SetTreeRegrowTime(60*3)
    end
end