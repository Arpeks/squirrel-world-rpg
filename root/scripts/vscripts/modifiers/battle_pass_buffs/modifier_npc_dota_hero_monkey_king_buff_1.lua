modifier_npc_dota_hero_monkey_king_buff_1 = class({})

function modifier_npc_dota_hero_monkey_king_buff_1:IsHidden()
    return true
end

function modifier_npc_dota_hero_monkey_king_buff_1:IsPurgable()
    return false
end

function modifier_npc_dota_hero_monkey_king_buff_1:RemoveOnDeath()
    return false
end

function modifier_npc_dota_hero_monkey_king_buff_1:OnCreated( kv )
    self.total_level = 0
    self:StartIntervalThink(1)
end

function modifier_npc_dota_hero_monkey_king_buff_1:OnIntervalThink()
    local total_level = 0
    for _, ability_name in ipairs({
        "monkey_king_boundless_strike_lua", 
        "monkey_king_banana_attack", 
        "monkey_king_jingu_mastery_lua", 
        "monkey_king_wukongs_command_lua"
    }) do
        local ability = self:GetParent():FindAbilityByName(ability_name)
        total_level = total_level + ability:GetLevel()
    end
    self.total_level = total_level
    if IsServer() then
        self:GetParent():CalculateStatBonus(true)
    end
end

function modifier_npc_dota_hero_monkey_king_buff_1:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_HEALTH_BONUS,
    }
end

function modifier_npc_dota_hero_monkey_king_buff_1:GetModifierHealthBonus()
    return self.total_level * 1000
end

function modifier_npc_dota_hero_monkey_king_buff_1:CustomOnTakeDamage(params)
	if params.attacker == self:GetParent() then
        local heal = params.damage * 0.2
		self:GetParent():Heal( math.min(math.abs(heal), 2^30), nil )
		local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
        local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, params.attacker )
        ParticleManager:SetParticleControl( effect_cast, 1, params.attacker:GetOrigin() )
        ParticleManager:ReleaseParticleIndex( effect_cast )
	end
end