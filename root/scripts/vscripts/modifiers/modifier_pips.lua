modifier_pips = class({})
--Classifications template
function modifier_pips:IsHidden()
    return true
end

function modifier_pips:IsDebuff()
    return false
end

function modifier_pips:IsPurgable()
    return false
end

function modifier_pips:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_pips:IsStunDebuff()
    return false
end

function modifier_pips:RemoveOnDeath()
    return true
end

function modifier_pips:DestroyOnExpire()
    return false
end

function modifier_pips:OnCreated(data)
    self.parent = self:GetParent()
    if not IsServer() then
        return
    end
    self:SetStackCount(data.pips_count or 1)
    self.parent:SetMaxHealth(data.pips_count * 4)
    self.parent:SetBaseMaxHealth(data.pips_count * 4)
    self.parent:SetHealth(data.pips_count * 4)
end

function modifier_pips:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_HEALTHBAR_PIPS,
        --MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
    }
end

function modifier_pips:GetModifierHealthBarPips()
    return math.min( self:GetStackCount(), 20 )
end

function modifier_pips:CustomOnAttackLanded(data)
    if data.target == self.parent then
        local chealth = self.parent:GetHealth()
        local damage = data.attacker:IsHero() and 4 or 2
        if (chealth - damage) <= 0 then
            self.parent:Kill(nil, data.attacker)
            Timers:CreateTimer(3,function()
                UTIL_Remove(self.parent)
            end)
        else
            self.parent:SetHealth(chealth - damage)
        end
    end
end

function modifier_pips:GetModifierIncomingDamage_Percentage()
    return -100
end