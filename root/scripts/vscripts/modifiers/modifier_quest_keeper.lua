LinkLuaModifier("modifier_quest_keeper_player_aura", "modifiers/modifier_quest_keeper.lua", LUA_MODIFIER_MOTION_NONE)
if modifier_quest_keeper_player_aura == nil then
	modifier_quest_keeper_player_aura = class({})
end

function modifier_quest_keeper_player_aura:IsHidden()
  	return true
end

function modifier_quest_keeper_player_aura:OnCreated(t)
	if IsServer() then
		CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(self:GetParent():GetPlayerOwnerID()),"activate_quest_aura",{
			["name"] = self:GetCaster():GetUnitName(), 
			["index"] = self:GetCaster():entindex()
		})
	end
end

function modifier_quest_keeper_player_aura:OnDestroy(t)
	if IsServer() then
		CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(self:GetParent():GetPlayerOwnerID()),"deactivate_quest_aura",{})
	end
end

if modifier_quest_keeper == nil then
	modifier_quest_keeper = class({})
end

function modifier_quest_keeper:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_DISABLE_AUTOATTACK,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
        MODIFIER_PROPERTY_MIN_HEALTH,
    }

    return funcs
end

function modifier_quest_keeper:CheckState()
	local state = {
		-- [MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = false,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNTARGETABLE] = true,
		[MODIFIER_STATE_UNTARGETABLE_ENEMY] = true,
		[MODIFIER_STATE_INVISIBLE] = true,
	}

  	return state
end

function modifier_quest_keeper:GetAbsoluteNoDamageMagical()
  	return 1
end

function modifier_quest_keeper:GetAbsoluteNoDamagePhysical()
  	return 1
end

function modifier_quest_keeper:GetAbsoluteNoDamagePure()
  	return 1
end

function modifier_quest_keeper:GetMinHealth()
  	return 1
end

function modifier_quest_keeper:IsHidden()
    return true
end

function modifier_quest_keeper:IsAura()
  	return true
end

function modifier_quest_keeper:GetModifierAura()
  	return "modifier_quest_keeper_player_aura"
end

function modifier_quest_keeper:GetAuraRadius()
  	return RDA_QUEST_NPC_AURA_RADIUS
end

function modifier_quest_keeper:GetAuraSearchType()
  	return DOTA_UNIT_TARGET_HERO
end

function modifier_quest_keeper:GetAuraSearchTeam()
  	return DOTA_UNIT_TARGET_TEAM_FRIENDLY
end

function modifier_quest_keeper:GetAuraDuration()
  	return 0.1
end