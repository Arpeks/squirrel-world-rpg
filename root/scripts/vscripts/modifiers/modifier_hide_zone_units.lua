LinkLuaModifier( "modifier_creep_antilag_aura", "modifiers/modifier_hide_zone_units", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_creep_antilag_phased", "modifiers/modifier_hide_zone_units", LUA_MODIFIER_MOTION_NONE )

modifier_creep_antilag = class({})
--Classifications template
function modifier_creep_antilag:IsHidden()
    return true
end

function modifier_creep_antilag:IsDebuff()
    return false
end

function modifier_creep_antilag:IsPurgable()
    return false
end

function modifier_creep_antilag:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_creep_antilag:IsStunDebuff()
    return false
end

function modifier_creep_antilag:RemoveOnDeath()
    return true
end

function modifier_creep_antilag:DestroyOnExpire()
    return true
end

if IsServer() then
    function modifier_creep_antilag:OnCreated()
        self:GetParent().hide_aura_modifier = self
        self:SetStackCount(0)
    end

    function modifier_creep_antilag:OnStackCountChanged()
        if self:GetStackCount() == 0 then
            self.main_mod = self:GetParent():AddNewModifier(self:GetParent(), nil, "modifier_creep_antilag_phased", {})
        elseif self.main_mod and not self.main_mod:IsNull() then
            self.main_mod:Destroy()
            self.main_mod = nil -- Очистка переменной после уничтожения
        end
    end
end
--------------------------------------------------------------------------------
-- Aura Effects
function modifier_creep_antilag:IsAura()
    return self:GetStackCount() == 0
end

function modifier_creep_antilag:GetModifierAura()
    return "modifier_creep_antilag_aura"
end

function modifier_creep_antilag:GetAuraRadius()
    return 2100
end

function modifier_creep_antilag:GetAuraDuration()
    return 4
end

function modifier_creep_antilag:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_creep_antilag:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_creep_antilag:GetAuraSearchFlags()
    return 0
end

modifier_creep_antilag_aura = class({})
--Classifications template
function modifier_creep_antilag_aura:IsHidden()
    return true
end

function modifier_creep_antilag_aura:IsDebuff()
    return false
end

function modifier_creep_antilag_aura:IsPurgable()
    return false
end

function modifier_creep_antilag_aura:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_creep_antilag_aura:IsStunDebuff()
    return false
end

function modifier_creep_antilag_aura:RemoveOnDeath()
    return true
end

function modifier_creep_antilag_aura:DestroyOnExpire()
    return true
end

function modifier_creep_antilag_aura:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

if IsServer() then
    function modifier_creep_antilag_aura:OnCreated()
        self:GetAuraOwner().hide_aura_modifier:IncrementStackCount()
    end
    function modifier_creep_antilag_aura:OnDestroy()
        if self:GetAuraOwner() and self:GetAuraOwner().hide_aura_modifier and not self:GetAuraOwner().hide_aura_modifier:IsNull() then
            self:GetAuraOwner().hide_aura_modifier:DecrementStackCount()
        end
    end
end
































modifier_creep_antilag_phased = class({})
--Classifications template
function modifier_creep_antilag_phased:IsHidden()
    return true
end

function modifier_creep_antilag_phased:IsDebuff()
    return false
end

function modifier_creep_antilag_phased:IsPurgable()
    return false
end

function modifier_creep_antilag_phased:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_creep_antilag_phased:IsStunDebuff()
    return false
end

function modifier_creep_antilag_phased:RemoveOnDeath()
    return true
end

function modifier_creep_antilag_phased:DestroyOnExpire()
    return true
end

function modifier_creep_antilag_phased:OnCreated()
    if not IsServer() then return end

    local parent = self:GetParent()

    parent:AddNoDraw()
end

function modifier_creep_antilag_phased:OnDestroy()
    if not IsServer() then return end

    local parent = self:GetParent()

    parent:RemoveNoDraw()
end

function modifier_creep_antilag_phased:CheckState()
    local state = {
        [MODIFIER_STATE_UNTARGETABLE] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP_FOR_ENEMIES] = true,
        [MODIFIER_STATE_CANNOT_BE_MOTION_CONTROLLED] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_INVISIBLE] = true,
        [MODIFIER_STATE_ATTACK_IMMUNE] = true,
        [MODIFIER_STATE_STUNNED] = true,
        [MODIFIER_STATE_UNSELECTABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_OUT_OF_GAME] = true
    }   

    return state
end

