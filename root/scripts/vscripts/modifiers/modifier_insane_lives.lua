modifier_insane_lives = class({})

function modifier_insane_lives:IsHidden()
	return false
end

function modifier_insane_lives:IsPurgable()
	return false
end

function modifier_insane_lives:RemoveOnDeath()
	return false
end

function modifier_insane_lives:GetTexture()
    return "item_aegis"
end

function modifier_insane_lives:OnStackCountChanged()
	if not IsServer() then
		return
	end
    if self:GetStackCount() == 0 and not GameRules:IsCheatMode() and EventHandler:IsInvokerKilled() == false then
		self:GetParent():SetRespawnsDisabled(true)
		self:GetParent():SetTimeUntilRespawn(-1)
	end
end

function modifier_insane_lives:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_TOOLTIP
	}
end

function modifier_insane_lives:OnTooltip()
	return self:GetStackCount()
end
