modifier_unreal_lives = class({})

function modifier_unreal_lives:IsHidden()
	return false
end

function modifier_unreal_lives:IsPurgable()
	return false
end

function modifier_unreal_lives:RemoveOnDeath()
	return false
end

function modifier_unreal_lives:GetTexture()
    return "item_aegis"
end

function modifier_unreal_lives:OnCreated()
	self.calc_agi = false
	self.calc_int = false
	self.calc_str = false
	self.agi = 0
	self.int = 0
	self.str = 0
end

function modifier_unreal_lives:OnStackCountChanged()
	if not IsServer() then
		return
	end
    if self:GetStackCount() == 0 and not GameRules:IsCheatMode() and EventHandler:IsInvokerKilled() == false then
		self:GetParent():SetRespawnsDisabled(true)
		self:GetParent():SetTimeUntilRespawn(-1)
	end
end

function modifier_unreal_lives:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_TOOLTIP,
		MODIFIER_PROPERTY_PRIMARY_STAT_DAMAGE_MULTIPLIER,
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS
	}
end

function modifier_unreal_lives:OnTooltip()
	return self:GetStackCount()
end

function modifier_unreal_lives:GetPrimaryStatDamageMultiplier()
	return -60
end

function modifier_unreal_lives:GetModifierBonusStats_Strength()
	if self:GetParent().calc_str then return 0 end
	self:GetParent().calc_str = true
	self.str = self:GetParent():GetStrength() * -0.75
	self:GetParent().calc_str = false
	return self.str
end

function modifier_unreal_lives:GetModifierBonusStats_Intellect()
	if self:GetParent().calc_int then return 0 end
	self:GetParent().calc_int = true
	self.int = self:GetParent():GetIntellect(true) * -0.75
	self:GetParent().calc_int = false
	return self.int
end

function modifier_unreal_lives:GetModifierBonusStats_Agility()
	if self:GetParent().calc_agi then return 0 end
	self:GetParent().calc_agi = true
	self.agi = self:GetParent():GetAgility() * -0.75
	self:GetParent().calc_agi = false
	return self.agi
end