modifier_event_listener = class({})
function modifier_event_listener:IsHidden() return false end
function modifier_event_listener:IsPurgable() return false end
function modifier_event_listener:OnCreated() 
    print("modifier_event_listener")
end

function modifier_event_listener:CheckState()
    return 
    {
        [MODIFIER_STATE_UNSELECTABLE] = true,
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_OUT_OF_GAME] = true,
        [MODIFIER_STATE_INVULNERABLE] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_NOT_ON_MINIMAP_FOR_ENEMIES] = true,
    }
end

function modifier_event_listener:DeclareFunctions()
  return
  {
    MODIFIER_EVENT_ON_ATTACK_LANDED,
    MODIFIER_EVENT_ON_ATTACK,
    MODIFIER_EVENT_ON_TAKEDAMAGE,
    MODIFIER_EVENT_ON_DEATH,
    MODIFIER_EVENT_ON_ABILITY_START,
    MODIFIER_EVENT_ON_ABILITY_EXECUTED,
    MODIFIER_EVENT_ON_ATTACK_START,
    MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
    MODIFIER_EVENT_ON_ORDER,
  }
end

function modifier_event_listener:OnAbilityFullyCast( params )
    local unit = params.unit
    if unit ~= nil then
        -- if not unit:IsRealHero() then return end
        for _, modifier in pairs(unit:FindAllModifiers()) do
            if modifier and modifier.CustomOnAbilityFullyCast then
                modifier:CustomOnAbilityFullyCast(params)
            end
        end
    end
    local target = params.target
    if target ~= nil then
        -- if not target:IsRealHero() then return end
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnAbilityFullyCast then
                modifier:CustomOnAbilityFullyCast(params)
            end
        end
    end
end

function modifier_event_listener:OnAbilityStart( params )
    local unit = params.unit
    if unit ~= nil then
        -- if not unit:IsRealHero() then return end
        for _, modifier in pairs(unit:FindAllModifiers()) do
            if modifier and modifier.CustomOnAbilityStart then
                modifier:CustomOnAbilityStart(params)
            end
        end
    end
    local target = params.target
    if target ~= nil then
        -- if not target:IsRealHero() then return end
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnAbilityStart then
                modifier:CustomOnAbilityStart(params)
            end
        end
    end
end

function modifier_event_listener:OnOrder( params )
    local unit = params.unit
    if unit ~= nil then
        -- if not unit:IsRealHero() then return end
        for _, modifier in pairs(unit:FindAllModifiers()) do
            if modifier and modifier.CustomOnOrder then
                modifier:CustomOnOrder(params)
            end
        end
    end
    local target = params.target
    if target ~= nil then
        -- if not target:IsRealHero() then return end
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnOrder then
                modifier:CustomOnOrder(params)
            end
        end
    end
end

function modifier_event_listener:OnAbilityExecuted( params )
    if not params.ability then return end
    local unit = params.unit
    if unit == nil then return end
    -- if not unit:IsRealHero() then return end
    for _, modifier in pairs(unit:FindAllModifiers()) do
        if modifier and modifier.CustomOnAbilityExecuted then
            modifier:CustomOnAbilityExecuted(params)
        end
    end
end

function modifier_event_listener:OnTakeDamage(params)
    local attacker = params.attacker
    local unit = params.unit

    if attacker == nil or unit == nil then return end

    if attacker then
        for _, modifier in pairs(attacker:FindAllModifiers()) do
            if modifier and modifier.CustomOnTakeDamage then
                modifier:CustomOnTakeDamage(params)
            end
        end
    end

    if unit then
        for _, modifier in pairs(unit:FindAllModifiers()) do
            if modifier and modifier.CustomOnTakeDamage then
                modifier:CustomOnTakeDamage(params)
            end
        end
    end
end

function modifier_event_listener:OnAttackLanded(params)
    local attacker = params.attacker
    local target = params.target
    if attacker == nil or target == nil then return end

    if attacker then
        for _, modifier in pairs(attacker:FindAllModifiers()) do
            if modifier and modifier.CustomOnAttackLanded then
                modifier:CustomOnAttackLanded(params)
            end
        end
    end

    if target then
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnAttackLanded then
                modifier:CustomOnAttackLanded(params)
            end
        end
    end
end

function modifier_event_listener:OnAttackStart(params)
    local attacker = params.attacker
    local target = params.target
    if attacker == nil or target == nil then return end
    
    if attacker then
        for _, modifier in pairs(attacker:FindAllModifiers()) do
            if modifier and modifier.CustomOnAttackStart then
                modifier:CustomOnAttackStart(params)
            end
        end
    end
    if target then
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnAttackStart then
                modifier:CustomOnAttackStart(params)
            end
        end
    end
end

function modifier_event_listener:OnAttack(params)
    local attacker = params.attacker
    local target = params.target
    if attacker == nil or target == nil then return end
    if attacker then
        for _, modifier in pairs(attacker:FindAllModifiers()) do
            if modifier and modifier.CustomOnAttack then
                modifier:CustomOnAttack(params)
            end
        end
    end
end

function modifier_event_listener:OnDeath(params)
    local attacker = params.attacker
    local target = params.unit

    if attacker then
        for _, modifier in pairs(attacker:FindAllModifiers()) do
            if modifier and modifier.CustomOnDeath then
                modifier:CustomOnDeath(params)
            end
        end
    end

    if target then
        for _, modifier in pairs(target:FindAllModifiers()) do
            if modifier and modifier.CustomOnDeath then
                modifier:CustomOnDeath(params)
            end
        end
    end
end