modifier_all_buff_scrolls = class({})

function modifier_all_buff_scrolls:IsHidden()
    return self:GetStackCount() == 0
end

function modifier_all_buff_scrolls:GetTexture()
	return "scroll_9"
end

function modifier_all_buff_scrolls:IsDebuff()
    return false
end

function modifier_all_buff_scrolls:IsPurgable()
    return false
end

function modifier_all_buff_scrolls:IsPurgeException()
    return false
end

function modifier_all_buff_scrolls:IsStunDebuff()
    return false
end

function modifier_all_buff_scrolls:RemoveOnDeath()
    return false
end

function modifier_all_buff_scrolls:DestroyOnExpire()
    return false
end

function modifier_all_buff_scrolls:OnCreated( kv )
    if not IsServer() then return end
    self:IncrementStackCount()
end

function modifier_all_buff_scrolls:OnRefresh( kv )
    self:OnCreated( kv )
end

function modifier_all_buff_scrolls:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_TOOLTIP
    }
    return funcs
end

function modifier_all_buff_scrolls:OnTooltip()
    return self:GetRemainingTime()
end