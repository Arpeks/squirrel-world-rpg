LinkLuaModifier("modifier_quest_shrine_aura", "modifiers/modifier_quests", LUA_MODIFIER_MOTION_NONE)

modifier_quest_shrine_aura = class({})

function modifier_quest_shrine_aura:IsHidden()
    return true
end

function modifier_quest_shrine_aura:IsPurgable()
    return false
end

function modifier_quest_shrine_aura:OnCreated( kv )
    if not IsServer() then 
        return
    end
    EventHandler:OnGetCloseToTarget(self:GetParent():GetPlayerOwnerID(), self:GetCaster():GetUnitName(), self:GetCaster():GetName())
end


if modifier_quest_shrine == nil then
	modifier_quest_shrine = class({})
end

function modifier_quest_shrine:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_DISABLE_AUTOATTACK,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
        MODIFIER_PROPERTY_MIN_HEALTH,
    }

    return funcs
end

function modifier_quest_shrine:CheckState()
	local state = {
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_ATTACK_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = false,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNTARGETABLE] = true,
		[MODIFIER_STATE_UNSELECTABLE] = false,
	}

  	return state
end

function modifier_quest_shrine:GetAbsoluteNoDamageMagical()
  	return 1
end

function modifier_quest_shrine:GetAbsoluteNoDamagePhysical()
  	return 1
end

function modifier_quest_shrine:GetAbsoluteNoDamagePure()
  	return 1
end

function modifier_quest_shrine:GetMinHealth()
  	return 1
end

function modifier_quest_shrine:IsHidden()
    return true
end

function modifier_quest_shrine:IsAura()
    return true
end

function modifier_quest_shrine:GetModifierAura()
    return "modifier_quest_shrine_aura"
end

function modifier_quest_shrine:GetAuraRadius()
    return RDA_QUEST_NPC_AURA_RADIUS
end

function modifier_quest_shrine:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO
end

function modifier_quest_shrine:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_BOTH
end

function modifier_quest_shrine:GetAuraSearchFlags()
    return DOTA_UNIT_TARGET_FLAG_DEAD + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE
end

function modifier_quest_shrine:GetAuraDuration()
    return 0.1
end

if modifier_quest_npc == nil then
	modifier_quest_npc = class({})
end

function modifier_quest_npc:CheckState()
	local state = {
		-- [MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNSELECTABLE] = false,
		[MODIFIER_STATE_UNTARGETABLE_ENEMY] = true,
		[MODIFIER_STATE_INVISIBLE] = true,
	}
  	return state
end

function modifier_quest_npc:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
        MODIFIER_PROPERTY_MIN_HEALTH,
    }

    return funcs
end

function modifier_quest_npc:GetAbsoluteNoDamageMagical()
	return 1
end

function modifier_quest_npc:GetAbsoluteNoDamagePhysical()
	return 1
end

function modifier_quest_npc:GetAbsoluteNoDamagePure()
	return 1
end

function modifier_quest_npc:GetMinHealth()
	return self:GetParent():GetMaxHealth()
end


modifier_quest_out_of_game = class({})

function modifier_quest_out_of_game:OnCreated( kv )
	if IsServer() then
		self:GetParent():AddNoDraw()
	end
end

function modifier_quest_out_of_game:OnDestroy()
	if IsServer() then
		self:GetParent():RemoveNoDraw()
	end
end

function modifier_quest_out_of_game:CheckState()
	if IsServer() then
		local state = {	
			[MODIFIER_STATE_INVULNERABLE] = true,
			[MODIFIER_STATE_UNSELECTABLE] = true,
			[MODIFIER_STATE_OUT_OF_GAME] = true,
			[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
			[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		}
		return state
	end
end

modifier_mines_ore = class({})

function modifier_mines_ore:IsHidden()
	return false
end

function modifier_mines_ore:IsPurgable()
	return false
end

function modifier_mines_ore:OnCreated(data)
	self.hitsLanded = 0
	self.pips_count = data.pips_count
	self.unitHealt = data.pips_count
	self.minPips = data.minPips
	self.maxPips = data.maxPips
	self.minScale = data.minScale
	self.maxScale = data.maxScale
	self.rarePips = data.rarePips
	self.rareScale = data.rareScale
    if not IsServer() then
        return
    end
	self:SetStackCount(self.unitHealt)
    self:GetParent():SetMaxHealth(self.unitHealt)
    self:GetParent():SetHealth(self.unitHealt)
    self:SetHasCustomTransmitterData( true )
end

function modifier_mines_ore:CheckState()
	local state = {
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_IGNORING_MOVE_AND_ATTACK_ORDERS] = true,
	}
  	return state
end

function modifier_mines_ore:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
    }
end
-- item_smithy_pickaxe
function modifier_mines_ore:CustomOnAttackLanded(data)
    if data.target == self:GetParent() then
		local damage = 1
		for ITEM_SLOT = DOTA_ITEM_SLOT_1, DOTA_STASH_SLOT_6 do --DOTA_ITEM_NEUTRAL_SLOT
			local item = data.attacker:GetItemInSlot(ITEM_SLOT)
			if item then
				if item:GetName() == "item_smithy_pickaxe" and item:GetPurchaser() == data.attacker then
					damage = damage + 1
				end
			end
		end
		self.hitsLanded = self.hitsLanded + damage
		self.unitHealt = self.pips_count - self.hitsLanded
		self:SendBuffRefreshToClients()
        if self.unitHealt <= 0 then
			self:GetParent():Kill(nil, data.attacker)
            UTIL_Remove(self:GetParent())
        else
			self:SetStackCount(self.unitHealt)
			self:GetParent():SetMaxHealth(self.pips_count)
            self:GetParent():SetHealth(self.unitHealt)
			if self.pips_count <= self.maxPips then
				local modelScale = self.minScale + (self.maxScale - self.minScale) * (self.unitHealt - self.minPips) / (self.maxPips - self.minPips)
				self:GetParent():SetModelScale(modelScale)
			else
				local modelScale = self.minScale + (self.rareScale - self.minScale) * (self.unitHealt - self.minPips) / (self.rarePips - self.minPips)
				self:GetParent():SetModelScale(modelScale)
			end
        end
    end
end

function modifier_mines_ore:GetAbsoluteNoDamageMagical()
	return 1
end

function modifier_mines_ore:GetAbsoluteNoDamagePhysical()
	return 1
end

function modifier_mines_ore:GetAbsoluteNoDamagePure()
	return 1
end

function modifier_mines_ore:AddCustomTransmitterData()
    return {
        pips_count = self.pips_count,
		minPips = self.minPips,
		maxPips = self.maxPips,
		minScale = self.minScale,
		maxScale = self.maxScale,
		rarePips = self.rarePips,
		rareScale = self.rareScale,
		unitHealt = self.unitHealt,
    }
end

function modifier_mines_ore:HandleCustomTransmitterData( data )
    self.pips_count = data.pips_count
	self.minPips = data.minPips
	self.maxPips = data.maxPips
	self.minScale = data.minScale
	self.maxScale = data.maxScale
	self.rarePips = data.rarePips
	self.rareScale = data.rareScale
	self.unitHealt = data.unitHealt
end

modifier_breakingcrate = class({})

function modifier_breakingcrate:IsHidden()
	return false
end

function modifier_breakingcrate:IsPurgable()
	return false
end

function modifier_breakingcrate:OnCreated(data)
	self.hitsLanded = 0
	self.pips_count = data.pips_count
	self.unitHealt = data.pips_count
    if not IsServer() then
        return
    end
	self:SetStackCount(self.unitHealt)
    self:GetParent():SetMaxHealth(self.unitHealt)
    self:GetParent():SetHealth(self.unitHealt)
    self:SetHasCustomTransmitterData( true )
end

function modifier_breakingcrate:CheckState()
	local state = {
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
  	return state
end

function modifier_breakingcrate:DeclareFunctions()
    return {
        -- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,
    }
end

function modifier_breakingcrate:CustomOnAttackLanded(data)
    if data.target == self:GetParent() then
		self.hitsLanded = self.hitsLanded + 1
		self.unitHealt = self.pips_count - self.hitsLanded
		self:SendBuffRefreshToClients()
        if self.unitHealt <= 0 then
			self:GetParent():Kill(nil, data.attacker)
        else
			self:SetStackCount(self.unitHealt)
			self:GetParent():SetMaxHealth(self.pips_count)
            self:GetParent():SetHealth(self.unitHealt)
        end
    end
end

function modifier_breakingcrate:GetAbsoluteNoDamageMagical()
	return 1
end

function modifier_breakingcrate:GetAbsoluteNoDamagePhysical()
	return 1
end

function modifier_breakingcrate:GetAbsoluteNoDamagePure()
	return 1
end

function modifier_breakingcrate:AddCustomTransmitterData()
    return {
        pips_count = self.pips_count,
		unitHealt = self.unitHealt,
    }
end

function modifier_breakingcrate:HandleCustomTransmitterData( data )
    self.pips_count = data.pips_count
	self.unitHealt = data.unitHealt
end