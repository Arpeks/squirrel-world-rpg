modifier_teleport_state = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
    RemoveOnDeath             = function(self) return false end,
    IsPermanent               = function(self) return true end,
})

function modifier_teleport_state:CheckState()
	local state = {
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_NOT_ON_MINIMAP] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_LOW_ATTACK_PRIORITY] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNTARGETABLE] = true,
		[MODIFIER_STATE_UNTARGETABLE_ENEMY] = true,
		[MODIFIER_STATE_INVISIBLE] = true,
        [MODIFIER_STATE_UNSELECTABLE]   = true,
        [MODIFIER_STATE_ATTACK_IMMUNE]  = true,
	}

  	return state
end

modifier_teleport_vipzone = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
    RemoveOnDeath             = function(self) return false end,
    IsPermanent               = function(self) return true end,
	IsAura                    = function(self) return true end,
    GetAuraRadius             = function(self) return 200 end,
    GetAuraSearchType         = function(self) return DOTA_UNIT_TARGET_HERO end,
    GetAuraSearchTeam         = function(self) return DOTA_UNIT_TARGET_TEAM_BOTH end,
    GetAuraSearchFlags        = function(self) return DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS end,
    GetAuraDuration           = function(self) return 0.1 end,
})

function modifier_teleport_vipzone:GetModifierAura()
    return "modifier_teleport_vipzone_aura"
end

function modifier_teleport_vipzone:OnCreated( kv )
    self.particle_disruption = "particles/chc/creature/mana_burn_aura_sigil.vpcf"
	self.particle_disruption_fx = ParticleManager:CreateParticle(self.particle_disruption, PATTACH_WORLDORIGIN, nil)
    local point = GetGroundPosition(self:GetParent():GetAbsOrigin(), self:GetParent())
    ParticleManager:SetParticleControl( self.particle_disruption_fx, 0, point )
end

function modifier_teleport_vipzone:OnDestroy()
    ParticleManager:DestroyParticle(self.particle_disruption_fx, false)
    ParticleManager:ReleaseParticleIndex(self.particle_disruption_fx)
end

LinkLuaModifier("modifier_teleport_vipzone_aura", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)

modifier_teleport_vipzone_aura = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
})

function modifier_teleport_vipzone_aura:OnCreated( kv )
    if IsServer() then
        Portals:TeleportToVipzone(self:GetParent())
    end
end

LinkLuaModifier("modifier_teleport_vipzone_out", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)

modifier_teleport_vipzone_out = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
    RemoveOnDeath             = function(self) return false end,
    IsPermanent               = function(self) return true end,
	IsAura                    = function(self) return true end,
    GetAuraRadius             = function(self) return 150 end,
    GetAuraSearchType         = function(self) return DOTA_UNIT_TARGET_HERO end,
    GetAuraSearchTeam         = function(self) return DOTA_UNIT_TARGET_TEAM_BOTH end,
    GetAuraSearchFlags        = function(self) return DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS end,
    GetAuraDuration           = function(self) return 0.2 end,
})

function modifier_teleport_vipzone_out:GetModifierAura()
    return "modifier_teleport_vipzone_out_aura"
end

function modifier_teleport_vipzone_out:OnCreated( kv )
    self.particle_disruption = "particles/chc/creature/mana_burn_aura_sigil.vpcf"
	self.particle_disruption_fx = ParticleManager:CreateParticle(self.particle_disruption, PATTACH_WORLDORIGIN, nil)
    local point = GetGroundPosition(self:GetParent():GetAbsOrigin(), self:GetParent())
    ParticleManager:SetParticleControl( self.particle_disruption_fx, 0, point )
end

function modifier_teleport_vipzone_out:OnDestroy()
    ParticleManager:DestroyParticle(self.particle_disruption_fx, false)
    ParticleManager:ReleaseParticleIndex(self.particle_disruption_fx)
end

LinkLuaModifier("modifier_teleport_vipzone_out_aura", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)

modifier_teleport_vipzone_out_aura = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
})

function modifier_teleport_vipzone_out_aura:OnCreated( kv )
    if IsServer() then
        if self:GetParent():HasModifier("modifier_teleport_cannot") then 
            self:Destroy()
        else
            Portals:TeleportHome(self:GetParent())
        end
    end
end

modifier_teleport_vipzone_out_hidden = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
    RemoveOnDeath             = function(self) return false end,
    IsPermanent               = function(self) return true end,
	IsAura                    = function(self) return true end,
    GetAuraRadius             = function(self) return 375 end,
    GetAuraSearchType         = function(self) return DOTA_UNIT_TARGET_HERO end,
    GetAuraSearchTeam         = function(self) return DOTA_UNIT_TARGET_TEAM_BOTH end,
    GetAuraSearchFlags        = function(self) return DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS end,
    GetAuraDuration           = function(self) return 0.1 end,
})

function modifier_teleport_vipzone_out_hidden:GetModifierAura()
    return "modifier_teleport_vipzone_out_hidden_aura"
end

LinkLuaModifier("modifier_teleport_vipzone_out_hidden_aura", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)

modifier_teleport_vipzone_out_hidden_aura = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
})

function modifier_teleport_vipzone_out_hidden_aura:OnCreated( kv )
    if IsServer() then
        local modifier_teleport_vipzone_out_hidden = self:GetAuraOwner():FindModifierByName("modifier_teleport_vipzone_out_hidden")
        if modifier_teleport_vipzone_out_hidden.activators == nil then modifier_teleport_vipzone_out_hidden.activators = {} end
        if not table.has_value(modifier_teleport_vipzone_out_hidden.activators, self:GetParent()) then 
            table.insert(modifier_teleport_vipzone_out_hidden.activators, self:GetParent())
        end
    end
end

function modifier_teleport_vipzone_out_hidden_aura:OnDestroy()
    if IsServer() then
        if self:GetParent():HasModifier("modifier_teleport_cannot") then
            self:GetParent():RemoveModifierByName("modifier_teleport_cannot")
        end

        local modifier_teleport_vipzone_out_hidden = self:GetAuraOwner():FindModifierByName("modifier_teleport_vipzone_out_hidden")
        modifier_teleport_vipzone_out_hidden.activators = table.remove_item(modifier_teleport_vipzone_out_hidden.activators,self:GetParent())
        if #modifier_teleport_vipzone_out_hidden.activators == 0 then
            self:GetCaster():RemoveModifierByName("modifier_teleport_vipzone_out_hidden")
            self:GetCaster():AddNewModifier(nil, nil, "modifier_teleport_vipzone_out", {})
        end
    end
end

modifier_teleport_roshan = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
    RemoveOnDeath             = function(self) return false end,
    IsPermanent               = function(self) return true end,
	IsAura                    = function(self) return true end,
    GetAuraRadius             = function(self) return 200 end,
    GetAuraSearchType         = function(self) return DOTA_UNIT_TARGET_HERO end,
    GetAuraSearchTeam         = function(self) return DOTA_UNIT_TARGET_TEAM_BOTH end,
    GetAuraSearchFlags        = function(self) return DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS end,
    GetAuraDuration           = function(self) return 0.1 end,
})

function modifier_teleport_roshan:GetModifierAura()
    return "modifier_teleport_roshan_aura"
end

function modifier_teleport_roshan:OnCreated( kv )
    self.particle_disruption = "particles/chc/creature/mana_burn_aura_sigil.vpcf"
	self.particle_disruption_fx = ParticleManager:CreateParticle(self.particle_disruption, PATTACH_WORLDORIGIN, nil)
    local point = GetGroundPosition(self:GetParent():GetAbsOrigin(), self:GetParent())
    ParticleManager:SetParticleControl( self.particle_disruption_fx, 0, point )
end

function modifier_teleport_roshan:OnDestroy()
    ParticleManager:DestroyParticle(self.particle_disruption_fx, false)
    ParticleManager:ReleaseParticleIndex(self.particle_disruption_fx)
end

LinkLuaModifier("modifier_teleport_roshan_aura", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)

modifier_teleport_roshan_aura = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
})

function modifier_teleport_roshan_aura:OnCreated( kv )
    if IsServer() then
        Portals:ActivateTeleportToRoshan(self:GetParent())
    end
end

modifier_teleport_cannot = class({
    IsHidden                  = function(self) return true end,
    IsPurgable                = function(self) return false end,
})