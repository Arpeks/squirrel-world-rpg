modifier_npc_boss_plague_squirrel_terminatorV2 = class({})
function modifier_npc_boss_plague_squirrel_terminatorV2:IsHidden() return true end
function modifier_npc_boss_plague_squirrel_terminatorV2:IsDebuff() return false end
function modifier_npc_boss_plague_squirrel_terminatorV2:IsPurgable() return false end
function modifier_npc_boss_plague_squirrel_terminatorV2:IsPurgeException() return false end
function modifier_npc_boss_plague_squirrel_terminatorV2:IsStunDebuff() return false end
function modifier_npc_boss_plague_squirrel_terminatorV2:RemoveOnDeath() return false end
function modifier_npc_boss_plague_squirrel_terminatorV2:DestroyOnExpire() return false end

function modifier_npc_boss_plague_squirrel_terminatorV2:OnCreated()
    if not IsServer() then return end
    self.real_hp_float = 2.0
    self.real_hp_exponent = 12 -- макс 15
    self.min_damage = math.pow(10, self.real_hp_exponent * -1)

    local parent = self:GetParent()

    parent:SetMaxHealth(500000)
    parent:SetBaseMaxHealth(500000)
    parent:SetHealth(500000)


    UnitHealthBar:AddUnit( parent )
    UnitHealthBar:UpdateCustonHealth( parent, self.real_hp_float, self.real_hp_exponent )
    
    self:StartIntervalThink(0.5)

    self:GetParent():SetCustomHealthLabel("BOSS", 55, 24, 66)
end

function modifier_npc_boss_plague_squirrel_terminatorV2:OnIntervalThink()
    UnitHealthBar:UpdateCustonHealth( self:GetParent(), self.real_hp_float, self.real_hp_exponent )	
end

function modifier_npc_boss_plague_squirrel_terminatorV2:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
    }
end

function modifier_npc_boss_plague_squirrel_terminatorV2:GetModifierIncomingDamage_Percentage(data)
    if not IsServer() then return -200 end
    local damage = data.damage
    for i=1,self.real_hp_exponent do
        damage = damage / 10
    end
    damage = max(damage, self.min_damage)
    self.real_hp_float = self.real_hp_float - damage
    if self.real_hp_float < 1 then
        self.real_hp_exponent = self.real_hp_exponent - 1
    end
    if self.real_hp_exponent <= 0 and self.real_hp_float <= 0 then
        -- full_win()
		-- Timers:CreateTimer(3,function() 
		-- 	GameRules:SetGameWinner(DOTA_TEAM_GOODGUYS)
		-- end)
    end
    return -200
end