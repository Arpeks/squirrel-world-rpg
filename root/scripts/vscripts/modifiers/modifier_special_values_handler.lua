modifier_special_values_handler = class({})

function modifier_special_values_handler:IsHidden() return true end
function modifier_special_values_handler:IsPermanent() return true end
function modifier_special_values_handler:IsPurgable() return false end

function modifier_special_values_handler:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
	}
end

function modifier_special_values_handler:GetModifierOverrideAbilitySpecial(params)
	local parent = self:GetParent()
	local modifiers = parent.special_values_modifiers

	if not modifiers then return end

	for i = #modifiers, 1, -1 do
		local modifier = modifiers[i]
		
		if modifier:IsNull() then
			table.remove(modifiers, i)
		else
			if modifier.GetAbilitySpecialValueMultiplier then
				local mult = modifier:GetAbilitySpecialValueMultiplier(params)

				if mult ~= nil and mult > 0 then
					return 1
				end
			end

			if modifier.GetAbilitySpecialValueBonus then
				local bonus = modifier:GetAbilitySpecialValueBonus(params)

				if bonus ~= nil and bonus ~= 0 then
					return 1
				end
			end
		end
	end

	if IsServer() and #modifiers == 0 then
		self:Destroy()
	end
end

function modifier_special_values_handler:GetModifierOverrideAbilitySpecialValue(params)
	local parent = self:GetParent()
	local modifiers = parent.special_values_modifiers
	local bonus = 0
	local mult = 1

	if not modifiers then return end

	for _, modifier in pairs(modifiers) do
		if modifier.GetAbilitySpecialValueMultiplier then
			mult = mult + (modifier:GetAbilitySpecialValueMultiplier(params) or 0)
		end

		if modifier.GetAbilitySpecialValueBonus then
			bonus = bonus + (modifier:GetAbilitySpecialValueBonus(params) or 0)
		end
	end

	if mult ~= 1 or bonus ~= 0 then
		local value = params.ability:GetLevelSpecialValueNoOverride(params.ability_special_value, params.ability_special_level)
		local final_value = (value + bonus) * mult
		-- print(string.format("[SVH] calculated %s as (%.2f + %.2f) * %.2f = %.2f", params.ability_special_value, value, bonus, mult, final_value))
		return final_value
	end
end
