if LocationSpawnControl == nil then
    _G.LocationSpawnControl = class({})
end

function LocationSpawnControl:Init()
    CustomGameEventManager:RegisterListener("location_toggle_button_presed",function(_, keys)
        self:ToggleButton(keys)
    end)
    self.lockedLocations = {}
    self.lockedLocations[1] = false
    self.lockedLocations[2] = false
    self.lockedLocations[3] = false
    self.lockedLocations[4] = false
    self.lockedLocations[5] = false
    self.lockedLocations[6] = false
    self.lockedLocations[7] = false
    self.lockedLocations[8] = false
    self.lockedLocations[9] = false

    self.displayData = {}
    self.displayData[1] = false
    self.displayData[2] = false
    self.displayData[3] = false
    self.displayData[4] = false
    self.displayData[5] = false
    self.displayData[6] = false
    self.displayData[7] = false
    self.displayData[8] = false
    self.displayData[9] = false
end

function LocationSpawnControl:ToggleButton(keys)
    local pid = keys.PlayerID
    local button_number = tonumber(keys.button_number)
    local checked = (keys.value == 1)

    self.lockedLocations[button_number] = not checked
    self:UpdateTable()
end

function LocationSpawnControl:Get()
    return self.lockedLocations
end

function LocationSpawnControl:AddBotton(button_number)
    self.displayData[button_number] = true
    self:UpdateTable()
end

function LocationSpawnControl:UpdateTable()
    local data = {}
    for i = 1, 9 do
        data[i] = {
            ["visible"] = self.displayData[i],
            ["selected"] = not self.lockedLocations[i],
        }
    end
    CustomNetTables:SetTableValue("GameInfo", "location_spawn", data)
end

LocationSpawnControl:Init()