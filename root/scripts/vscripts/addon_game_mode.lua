require('diff_wave')
require('libraries/timers')
require('libraries/notifications')
require("libraries/animations")
require("libraries/modifiers/modifier_summon_handler")
require("libraries/custom_indicator/custom_indicator" )
require("libraries/vector_target/vector_target" )
require("libraries/table")
require('libraries/util')
require('libraries/keyvalues')
require('libraries/utils/helpers')
-- require('libraries/ability_kv')
require('libraries/GameEventHandler')
require('EventHandler')
require('Portals')
require('LocationSpawnControl')
require("creep_spawner")
require("drop")
require("spawner")
require("rules")
require('towershop')
require('data/data')
require("data/heroesTalents")
require("data/battlePassStatic")
require("data/talentsData")
require("data/questsData")
-- require("damage")
require("dummy")
require("effects")
require("UnitHealthBar")

_G.key = GetDedicatedServerKeyV3("GJDNSTEDNBVDTJK")
_G.host = "https://random-defence-adventure.ru"
_G.DEV_MODE = false -- загрузка как в лобби без читов
_G.IS_SERVER_LOADING = true -- загрузка скриптов
_G.IS_SPAWN_MID_WAVE = true -- спавн крипов
_G.IS_TEST_MAP = false

if IsInToolsMode() then
	_G.key = ""
	_G.DEV_MODE = true -- загрузка как в лобби без читов
	_G.IS_SERVER_LOADING = false -- загрузка скриптов
	_G.IS_SPAWN_MID_WAVE = false -- спавн крипов
end

if CAddonAdvExGameMode == nil then
	CAddonAdvExGameMode = class({})
end

Precache = require("precache")

_G.connectionError = 0

function Activate() 
	GameRules.AddonAdventure = CAddonAdvExGameMode()
	GameRules.AddonAdventure:InitGameMode()
	ListenToGameEvent("dota_player_gained_level", LevelUp, nil)
	require("projectilemanager")
end

------------------------------------------------------------------------------
function CAddonAdvExGameMode:InitGameMode()
	GameRules:GetGameModeEntity():SetDaynightCycleDisabled(true)
	local GameModeEntity = GameRules:GetGameModeEntity()
	GameRules:GetGameModeEntity():SetLoseGoldOnDeath(false)
	local delay = 120
	GameRules:SetCustomGameSetupAutoLaunchDelay(delay)
	GameRules:GetGameModeEntity():SetHudCombatEventsDisabled( true )
	GameRules:SetPreGameTime(2)
	GameRules:SetShowcaseTime(1)
	GameRules:SetStrategyTime(10)
	GameRules:SetPostGameTime(60)
	-- GameRules:SetHeroSelectPenaltyTime(0)
	GameRules:SetUseUniversalShopMode(true)
	GameModeEntity:SetInnateMeleeDamageBlockAmount(0)
	GameModeEntity:SetInnateMeleeDamageBlockPercent(0)
	GameModeEntity:SetInnateMeleeDamageBlockPerLevelAmount(0)
	GameModeEntity:SetCustomBackpackSwapCooldown(3)
	GameRules:LockCustomGameSetupTeamAssignment(true)
	GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_GOODGUYS, 5 )
    GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_BADGUYS, 0 )
	GameRules:GetGameModeEntity():SetUnseenFogOfWarEnabled( not IsInToolsMode() )
	GameModeEntity:SetSelectionGoldPenaltyEnabled(false)
	GameRules:SetHideBlacklistedHeroes(true)
    GameRules:GetGameModeEntity():SetPlayerHeroAvailabilityFiltered( true )
	GameRules:SetUseBaseGoldBountyOnHeroes(true)
	GameRules:GetGameModeEntity():SetGoldSoundDisabled( true )
	GameRules:GetGameModeEntity():SetPauseEnabled( false )
	GameRules:GetGameModeEntity():SetMaximumAttackSpeed( 1500 ) 
	GameRules:GetGameModeEntity():SetMinimumAttackSpeed( 0 )
	GameRules:GetGameModeEntity():SetCustomXPRequiredToReachNextLevel( XP_PER_LEVEL_TABLE )
	GameRules:GetGameModeEntity():SetCustomHeroMaxLevel( HERO_MAX_LEVEL )
	GameRules:GetGameModeEntity():SetUseCustomHeroLevels( true )
	--------------------------------------------------------------------------------------------
	-- GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_INTELLIGENCE_MAGIC_RESIST, 0.0)
	GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_STRENGTH_HP_REGEN, 0.001)
	-- GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_INTELLIGENCE_MANA_REGEN, 0.0005)
	GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_AGILITY_ATTACK_SPEED, 0.05)
	GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_INTELLIGENCE_MANA, 0.1)
	GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_STRENGTH_HP, 20)
	GameModeEntity:SetCustomAttributeDerivedStatValue(DOTA_ATTRIBUTE_ALL_DAMAGE, 0.3)
	
	-------------------------------------------------------------------------------------------
	CustomGameEventManager:RegisterListener( "EndMiniGame", function(...) return OnEndMiniGame( ... ) end )
	CustomGameEventManager:RegisterListener( "item_boss_summon_choice", function(...) return ItemBossSummonChoice( ... ) end )
	

	ListenToGameEvent("game_rules_state_change", Dynamic_Wrap( CAddonAdvExGameMode, 'OnGameStateChanged' ), self )
	ListenToGameEvent("entity_killed", Dynamic_Wrap( CAddonAdvExGameMode, 'OnEntityKilled' ), self )
	ListenToGameEvent('npc_spawned', Dynamic_Wrap(CAddonAdvExGameMode, 'OnNPCSpawned'), self)	
	ListenToGameEvent("player_reconnected", Dynamic_Wrap(CAddonAdvExGameMode, 'OnPlayerReconnected'), self)	
	GameRules:GetGameModeEntity():SetExecuteOrderFilter(Dynamic_Wrap(CAddonAdvExGameMode, "OrderFilter"), self)
	GameRules:GetGameModeEntity():SetDamageFilter(Dynamic_Wrap(CAddonAdvExGameMode, "DamageFilter"), self)
	ListenToGameEvent("dota_item_picked_up", Dynamic_Wrap(CAddonAdvExGameMode, 'On_dota_item_picked_up'), self)
	-- CustomGameEventManager:RegisterListener("tp_check_lua", Dynamic_Wrap( tp, 'tp_check_lua' ))	
	CustomGameEventManager:RegisterListener("EndScreenExit", Dynamic_Wrap( CAddonAdvExGameMode, 'EndScreenExit' ))
	GameRules:GetGameModeEntity():SetBountyRunePickupFilter(Dynamic_Wrap(CAddonAdvExGameMode, "BountyRunePickupFilter"), self)
	ListenToGameEvent("dota_rune_activated_server", Dynamic_Wrap(CAddonAdvExGameMode, 'OnRunePickup'), self)
	EventHandler:init()
	diff_wave:InitGameMode()
	towershop:FillingNetTables()
	-- damage:Init()
	effects:init()
	_G.Activate_belka = false
	ListenToGameEvent("player_chat", Dynamic_Wrap( CAddonAdvExGameMode, "OnChat" ), self )
	GameRules:SetFilterMoreGold(true)
	GameRules:GetGameModeEntity():SetModifyGoldFilter(Dynamic_Wrap(CAddonAdvExGameMode, "GoldFilter"), self)
end
------------------------------ GoldFilter FUNCTIONS -----------------------------------------------
function WispTalent(hero, data)
	if hero:HasModifier("modifier_lifestealer_infest_bh_ally") then
		local modifier = hero:FindModifierByName("modifier_lifestealer_infest_bh_ally")
		modifier:GetCaster():ModifyGoldFiltered( data.gold * 0.3, true, DOTA_ModifyGold_Unspecified )
		return false
	end
	if hero:HasModifier("modifier _lifestealer_infest_bh") and data.reason_const == DOTA_ModifyGold_CreepKill then
		local modifier = hero:FindModifierByName("modifier_lifestealer_infest_bh")
		modifier.target:ModifyGoldFiltered( data.gold, true, DOTA_ModifyGold_CreepKill )
		return true
	end
	return false
end
function ScrollIncreaseGold(hero, data)
	if hero:HasModifier("modifier_book_gold") and data.reason_const == DOTA_ModifyGold_CreepKill then
		return data.gold * 1.20
	end
	return data.gold
end
function IncreaseNewPlayers(hero, data)
	local game_count = RATING.rating[data.player_id_const].games
	if game_count < 10 and data.reason_const ~= DOTA_ModifyGold_SellItem then
		local increase = {
			[0]=2.0, [1]=1.9, [2]=1.8, [3]=1.7, [4]=1.6, [5]=1.5, [6]=1.4, [7]=1.3, [8]=1.2, [9]=1.1
		}
		return data.gold * increase[game_count]
	end
	return data.gold
end
function CAddonAdvExGameMode:GoldFilter(data)
	if data.reason_const == DOTA_ModifyGold_AbandonedRedistribute then return false end
	local hero = PlayerResource:GetSelectedHeroEntity( data.player_id_const )
	if data.gold > 0 then
		data.gold = ScrollIncreaseGold(hero, data)
		data.gold = IncreaseNewPlayers(hero, data)
		if WispTalent(hero, data) then return false end
	end
	if diff_wave:GetGameDifficulty() == "Unreal" and data.gold > 0 then
		data.gold = data.gold * 0.8
	end
	EventHandler:GoldFilter(data)
	local mod = hero:FindModifierByName("modifier_gold_bank")
	local gold = hero:GetTotalGold()
	new_gold = gold + data.gold
	if new_gold > 99999 then
		hero:SetGold( 99999, false )
		if mod then
			mod:SetStackCount(new_gold - 99999)
		end
	else
		if mod then
			mod:SetStackCount(0)
		end
		hero:SetGold( new_gold, false )
	end
	if data.gold < 0 then
		hero.gold_spent = math.min(2000000000, (hero.gold_spent or 0) + math.abs(data.gold)) 
	elseif data.reason_const == DOTA_ModifyGold_SellItem then
		hero.gold_spent = math.min(0, (hero.gold_spent or 0) - math.abs(data.gold))
	end
	return false
end

function CAddonAdvExGameMode:InventoryFilter(event)
	-- DeepPrintTable(event)
end

function CAddonAdvExGameMode:OrderFilter(event)
	VectorTarget:OrderFilter(event)
    -- if event.order_type == DOTA_UNIT_ORDER_PATROL then
    --     return false
    -- end
	-- if event.order_type == DOTA_UNIT_ORDER_MOVE_TO_POSITION or event.order_type == DOTA_UNIT_ORDER_MOVE_TO_TARGET or event.order_type == DOTA_UNIT_ORDER_ATTACK_MOVE or event.order_type == DOTA_UNIT_ORDER_MOVE_TO_DIRECTION or event.order_type == DOTA_UNIT_ORDER_MOVE_RELATIVE then
	-- 	local unit = EntIndexToHScript(event.units["0"])
	-- 	if unit then
	-- 		if not unit:HasMovementCapability() then
	-- 			print(unit:GetUnitName())
	-- 		end
	-- 	end
	-- end
	if event.order_type == DOTA_UNIT_ORDER_CAST_TARGET and event.entindex_ability and event.entindex_target then
		local abi = EntIndexToHScript(event.entindex_ability)
		local target = EntIndexToHScript(event.entindex_target)
		if target and target:GetUnitName() == "npc_dota_hero_target_dummy" and abi and (abi:GetAbilityName() == "item_cyclone" or abi:GetAbilityName() == "item_wind_waker") then
			return false
		end
	end
	if QuestsRPG and QuestsRPG.OrderFilter then
		QuestsRPG:OrderFilter(event)
	end
	if event.order_type == DOTA_UNIT_ORDER_MOVE_TO_DIRECTION then
        return false
    end
    return true
end

function CAddonAdvExGameMode:DamageFilter(data)
	data.damage = math.min( math.abs( data.damage ), 2^30 )
    return true
end
------------------------------------------------------------------------------

function CAddonAdvExGameMode:OnChat( event )
    local text = event.text 
	local pid = event.playerid
	steamID = PlayerResource:GetSteamAccountID(pid)
	
	if text == "11" then
		local hRelay = Entities:FindByName( nil, "donate_forest" )
		hRelay:Trigger(nil,nil)
	end

	if text == "timer" then
		local c = 0
		for k,v in pairs(Timers.timers) do
			c = c + 1
		end
		GameRules:SendCustomMessage("Timers: " .. tostring(c),0,0)
	end

	if text == "dump" then
		-- _G.DumpSent = true
		local t = {}
		local count = 0
		local all_ent = Entities:FindAllInSphere(Vector(0,0,0), 99999)
		for _,ent in pairs(all_ent) do
			local class = ent:GetClassname()
			if class == "npc_dota_thinker" then
				local owner = ent:GetOwner():GetUnitName()
				if t[owner] then
					t[owner] = t[owner] + 1
				else
					t[owner] = 1
				end
				count = count + 1
			end
		end
		local max = 0
		local max_owner = nil
		for k,v in pairs(t) do
			if v > max then
				max = v
				max_owner = k
			end
		end
		GameRules:SendCustomMessage("Total count: " .. tostring(count),0,0)
		GameRules:SendCustomMessage("Found max: " .. tostring(max),0,0)
		GameRules:SendCustomMessage("By owner: " .. tostring(max_owner),0,0)

		DataBase:Send(DataBase.link.SendEntityDump, "GET", {
			name = PlayerResource:GetPlayerName(pid),
			match_id = tostring(GameRules:Script_GetMatchID()),
			mode = diff_wave.wavedef,
			time = math.ceil(GameRules:GetGameTime()/60),
			arr = t,
		}, nil, true, function()
			GameRules:SendCustomMessage("<font color='green'>Thanks for repost</font>",0,0)
		end)
	end

	if text == "-1" and steamID == 393187346 then
		PlayerResource:GetSelectedHeroEntity(0):ForceKill(false)
	end
	
	if text == "-2" and steamID == 393187346 then
		PlayerResource:GetSelectedHeroEntity(1):ForceKill(false)
	end
	
	if text == "-3" and steamID == 393187346 then
		PlayerResource:GetSelectedHeroEntity(2):ForceKill(false)
	end
	
	if text == "-4" and steamID == 393187346 then
		PlayerResource:GetSelectedHeroEntity(3):ForceKill(false)
	end
	
	if text == "-5" and steamID == 393187346 then
		PlayerResource:GetSelectedHeroEntity(4):ForceKill(false)
	end
	
	if text == "reset_time" then
		if PlayerResource:HasSelectedHero( pid ) then
			local hero = PlayerResource:GetSelectedHeroEntity( pid )
			if hero:GetTimeUntilRespawn() > 11 then
				hero:SetTimeUntilRespawn(10)
			end
		end    
	end 
end

function CAddonAdvExGameMode:On_dota_item_picked_up(keys)
	if keys.itemname == "item_key" then
		local hRelay = Entities:FindByName( nil, "donate_cementry" )
		hRelay:Trigger(nil,nil)
	end
end


function prt(t)
	GameRules:SendCustomMessage(''..t,0,0)
end

_G.PlayerConection = {}

function item_destroy()
		Timers:CreateTimer(10, function()
			for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
				if PlayerResource:GetTeam( nPlayerID ) == DOTA_TEAM_GOODGUYS then
				if PlayerResource:HasSelectedHero( nPlayerID ) then
				local hero = PlayerResource:GetSelectedHeroEntity( nPlayerID )
				local point = hero:GetOrigin()	
				local items_on_the_ground = Entities:FindAllByClassnameWithin("dota_item_drop",point,39900)
					for _,item in pairs(items_on_the_ground) do
					if item and not item:IsNull() then
						local containedItem = item:GetContainedItem()	
							if containedItem and not containedItem:IsNull() then
							local item_name = containedItem:GetAbilityName()
							local owner = containedItem:GetOwnerEntity()
							if owner and hero and hero == owner and not owner:IsNull() then
								Timers:CreateTimer(180, function()	
											if item_name == "item_key" then return end
											if item and not item:IsNull() then
											new_point = item:GetOrigin()
			
											local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/veil_of_discord.vpcf", PATTACH_CUSTOMORIGIN, item )
											ParticleManager:SetParticleControl( nFXIndex, 0, new_point )
											ParticleManager:SetParticleControl( nFXIndex, 1, Vector( 35, 35, 25 ) )
											ParticleManager:ReleaseParticleIndex( nFXIndex )
											UTIL_RemoveImmediate(item)	
											end
										end)
									end
								end
							end
						end
					end
				end
			end	
		return 181
    end)
end

function leave_game()
 Timers:CreateTimer(1, function()
 	rating_wave = ((math.floor(rat / 5)) * 2)
	for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
		if PlayerResource:GetTeam( nPlayerID ) == DOTA_TEAM_GOODGUYS then
			if PlayerResource:HasSelectedHero( nPlayerID ) then
				if PlayerResource:IsValidPlayerID(nPlayerID) then
					local connection = PlayerResource:GetConnectionState(nPlayerID)
						if connection ~= PlayerConection[nPlayerID] then
							PlayerConection[nPlayerID] = connection
								if not bot(nPlayerID) and connection == DOTA_CONNECTION_STATE_ABANDONED then 
									if GameRules:GetDOTATime(false, false) >= 60 * 5 and not DataBase:IsCheatMode() and EventHandler:IsInvokerKilled() == false then
										-- DataBase:PointsChange(nPlayerID, -25 * diff_wave.rating_scale, true)
										DataBase:EndGameSession(nPlayerID, -25 * diff_wave.rating_scale)
									end
								end
							end
						end
					end
				end
			end
		return 15
    end)
end

-- function Add_bsa_hero()    
--     if GetMapName() == "normal" and not GameRules:IsCheatMode() then
--         arr = {}
--         players = {}
--         for i = 0, PlayerResource:GetPlayerCount() - 1 do
--             if PlayerResource:IsValidPlayer(i) then 
--                 players[i] = {sid = tostring(PlayerResource:GetSteamID(i))}
--             end
--         end
--         arr['players'] = players
--         arr = json.encode(arr)
--         local req = CreateHTTPRequestScriptVM( "POST", "http://91.240.87.224/api_add_hero/?key=".._G.key )
--         req:SetHTTPRequestGetOrPostParameter('arr',arr)
--         req:SetHTTPRequestAbsoluteTimeoutMS(100000)
--         req:Send(function(res)
--             if res.StatusCode == 200 and res.Body ~= nil then
--                 print("DONE BSA HERO")
--                 print(res.StatusCode)
--                 print("DONE BSA HERO")
--             else
--                 print("ERROR BSA HERO")
--                 print(res.StatusCode)
--                 print("ERROR BSA HERO")
--             end
--         end)
--     end
-- end

_G.destroyed_barracks = false


function CAddonAdvExGameMode:EndScreenExit(keys)
	print(keys.PlayerID)
	DisconnectClient(keys.PlayerID, false)
end

function CAddonAdvExGameMode:OnPlayerReconnected(keys)
	local state = GameRules:State_Get()
	if state == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
			Timers:CreateTimer(2, function()	
				mmr = ((math.floor(rat / 5)) * 2 )
				doom = mega_boss_bonus
				doom = mega_boss_bonus * diff_wave.rating_scale
				mmr = mmr * diff_wave.rating_scale
			CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer(keys.PlayerID), "updateRatingCouter", {a = mmr,b = doom} )
		end)
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------

function LevelUp (eventInfo)
	local player = EntIndexToHScript( eventInfo.player )
	local player_id = player:GetPlayerID()
	local hero = PlayerResource:GetSelectedHeroEntity( player_id )
	if not hero then
		return 0.1
	end	
	-- for _,mod in pairs(hero:FindAllModifiers()) do
	-- 	mod:ForceRefresh()
	-- end
	local namePlayer = PlayerResource:GetPlayerName( player_id )
	local level = hero:GetLevel()
	
	if level == 17 or level == 19 or (level > 20 and level < 25) or level == 26 then
		hero:SetAbilityPoints(hero:GetAbilityPoints() + 1)
	end
end

HERO_MAX_LEVEL = 300

XP_PER_LEVEL_TABLE = {}
XP_PER_LEVEL_TABLE[0] = 0
XP_PER_LEVEL_TABLE[1] = 180
for i=2,25 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 180  
end

for i=26,50 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 190 
end

for i=51,75 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 200 
end

for i=76,100 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 210 
end

for i=101,150 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 220
end

for i=151,200 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 230 
end

for i=201,250 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 250 
end

for i=251,299 do
	XP_PER_LEVEL_TABLE[i] = XP_PER_LEVEL_TABLE[i-1]+i * 270 
end


--------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------

function CAddonAdvExGameMode:OnGameStateChanged( keys )
    local state = GameRules:State_Get()
	if state == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
		loadscript()
		local AllHeroPull = LoadKeyValues("scripts/npc/all_hero_pull.txt")
		for iPlayerID=0, PlayerResource:GetPlayerCount()-1 do
			for k,v in pairs(AllHeroPull) do
				GameRules:AddHeroToPlayerAvailability(iPlayerID, DOTAGameManager:GetHeroIDByName( k ) )
			end
		end
	elseif state == DOTA_GAMERULES_STATE_HERO_SELECTION then
		-- GameRules:BotPopulate()
		
	elseif state == DOTA_GAMERULES_STATE_STRATEGY_TIME then
		Portals:Init()
	for i=0, DOTA_MAX_TEAM_PLAYERS do
		if PlayerResource:HasSelectedHero(i) == false then
			local player = PlayerResource:GetPlayer(i)
			if player  then
				player:MakeRandomHeroSelection()
			end
		end
	end	 
    elseif state == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		CAddonAdvExGameMode:RDATestDisconnect()
		Timers:CreateTimer(30 * 60,function()
			collectgarbage("count")
			collectgarbage("collect")
			collectgarbage("count")
			return 30 * 60
		end)
		Timers:CreateTimer(function()
				if GameRules:IsDaytime() then
					GameRules:SetTimeOfDay(0.25)
				else
					GameRules:SetTimeOfDay(0.75)
				end
			return 300
		end)
		for iPlayerID = 0, PlayerResource:GetPlayerCount() - 1 do
			if PlayerResource:IsValidPlayer(iPlayerID) then
				Timers:CreateTimer(0.03,function()
					local npc = PlayerResource:GetSelectedHeroEntity(iPlayerID)
					if not npc then
						return 0.03
					end
					local playerID = iPlayerID
					npc:AddAbility("spell_item_pet"):SetLevel(1)
					npc:AddItemByName("item_tpscroll")
					npc:AddAbility("ability_quest"):SetLevel(1)
					-- npc:AddNewModifier(npc, nil, "modifier_all_buff_scrolls", {duration = 0.1})
					-- npc:AddAbility("ability_quest")

					for i=0,npc:GetAbilityCount() do 
						local ability = npc:GetAbilityByIndex(i)
						if ability ~= nil then
							local name = ability:GetAbilityName()
							local kv = GetAbilityKeyValuesByName(name)
							local class_name = ability:GetClassname()
							if class_name ~= "ability_lua" and class_name ~= "special_bonus_base" and class_name ~= "special_bonus_undefined" and class_name ~= "ability_datadriven" then
								npc:RemoveAbility(name)
							end
						end
					end

					if diff_wave.wavedef == "Insane" then
						npc:AddNewModifier(npc, nil, "modifier_insane_lives", {}):SetStackCount(5)
					end	
					if diff_wave.wavedef == "Impossible" then
						npc:AddNewModifier(npc, nil, "modifier_insane_lives", {}):SetStackCount(3)
					end
					if diff_wave.wavedef == "Unreal" then
						npc:AddNewModifier(npc, nil, "modifier_unreal_lives", {}):SetStackCount(2)
					end
					npc:AddNewModifier(npc, nil, "modifier_cheack_afk", nil)
					npc:AddNewModifier(npc, nil, "modifier_gold_bank", nil)
					
					CustomNetTables:SetTableValue("player_pets", tostring(playerID), {pet = "spell_item_pet"})	
					print("ban", Shop.pShop[playerID].ban)
					if Shop.pShop[playerID].ban and Shop.pShop[playerID].ban == 1 then 
						npc:AddNewModifier( npc, nil, "modifier_ban", {} )
						CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer(playerID), "ban", ban )
					end
					
					SendToServerConsole("dota_max_physical_items_purchase_limit " .. 500)
					
					steamID = PlayerResource:GetSteamAccountID(playerID)
					-- id_check(steamID) -----------------------------------------------
					
					for categoryKey, categoryValue in ipairs(Shop.pShop[playerID]) do
						for itemKey, itemValue in ipairs(categoryValue) do
							for _, itemname in pairs({"item_str", "item_agi", "item_int", "item_tree_gold"}) do
								if itemValue.itemname and itemValue.itemname == itemname and itemValue.now > 0 then
									npc:AddItemByName(itemname)
									itemValue.now = 0
									itemValue.status = "issued"
									break
								end
							end
						end
					end
				end)
			end
		end
		-- Timers:CreateTimer(3000, function()
			-- creep_spawner:spawn_2023()
		-- end)
		GameRules:SetTimeOfDay(0.25)
		GameRules:GetGameModeEntity():SetPauseEnabled( true )
		Spawner:Init()
		creep_spawner:spawn_creeps_forest()	
		Rules:spawn_creeps_don()
		-- Rules:spawn_lina()
		Dummy:init()
		leave_game()
		item_destroy()
		create_runes()
		local units = FindUnitsInRadius(DOTA_TEAM_BADGUYS, Vector(0,0,0), nil, 99999, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false)
		for _,u in pairs(units) do
			if u:IsBoss() then
				local mod = u:FindModifierByName("modifier_unreal")
				if mod then
					mod:StartIntervalThink(5)
				end
			end
		end
	end
end


function loadscript()
	if IS_SERVER_LOADING == false then
		print("local load")
		require("www/loader")
	else
		print("server load")
		local url = "https://cdn.random-defence-adventure.ru/backend/api/lua?key=" .. _G.key
		local req = CreateHTTPRequestScriptVM( "GET", url )
		req:SetHTTPRequestAbsoluteTimeoutMS(100000)
		req:Send(function(res)
			print(res.StatusCode)
			if res.StatusCode == 200 then
				CustomNetTables:SetTableValue("GameInfo", "GameSetupLoadingStage", {stage = 1})
				load = loadstring(res.Body)
				load()
				DataBase:GameSetup()
				for i = 0 , PlayerResource:GetPlayerCount()-1 do
					if PlayerResource:IsValidPlayer(i) then
						DataBase:PlayerSetup(i)
					end
				end
				CustomNetTables:SetTableValue("talants", "talents_experience", Talents.calculated_levels)
        		CustomNetTables:SetTableValue("talants", "second_branch", Talents.second_branch)
				CustomNetTables:SetTableValue('Pets', "list", Pets.list)
				CustomNetTables:SetTableValue('Pets', "experience_levels", Pets.experience_levels)
				CustomNetTables:SetTableValue('BattlePass', "dataReward", BattlePass.dataReward)
				CustomNetTables:SetTableValue('BattlePass', "ExpToLevelUp", BattlePass.ExpToLevelUp)
			end
		end)
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------

function CAddonAdvExGameMode:OnNPCSpawned(data)	
	npc = EntIndexToHScript(data.entindex)
	if npc:GetClassname() == "npc_dota_thinker" then
		return
	end
	local m = npc:FindModifierByName("modifier_fountain_invulnerability")
	if m then
		m:Destroy()
	end
end

function CAddonAdvExGameMode:BountyRunePickupFilter(data)
	local players = PlayerResource:GetPlayerCount()
	if EventHandler:IsInvokerKilled() then
		data.gold_bounty =  10000
		return true
	end
	local gold = {
		[0] = 60,
		[1] = 150,
		[2] = 250,
		[3] = 400,
		[4] = 550,
		[5] = 650,
		[6] = 1000,
		[7] = 1500,
		[8] = 3000,
		[9] = 4500,
		[10] = 6000,
	}
	data.gold_bounty = gold[_G.don_spawn_level] * 2 * 5 / players
	return true
end

function CAddonAdvExGameMode:OnRunePickup(data)
	EventHandler:OnRunePickup(data)
	if data.rune == DOTA_RUNE_XP then
		local hHero = PlayerResource:GetSelectedHeroEntity(data.PlayerID)
		local need_exp = (XP_PER_LEVEL_TABLE[hHero:GetLevel()] - XP_PER_LEVEL_TABLE[hHero:GetLevel() - 1])/ 2
		local t = {}
		hHero:AddExperience(need_exp, 0, false, false)
		for i = 0, PlayerResource:GetPlayerCount() - 1 do
			if PlayerResource:IsValidPlayer(i) and PlayerResource:GetConnectionState(i) == DOTA_CONNECTION_STATE_CONNECTED then
				local hero = PlayerResource:GetSelectedHeroEntity(i)
				if hero then
					t[hero:GetLevel()] = hero
				end
			end
		end
		for i = 0, 500 do
			if t[i] and t[i] ~= hHero then
				t[i]:AddExperience(need_exp, 0, false, false)
				break
			end
		end
		return
	end
	local runs = {
		[DOTA_RUNE_DOUBLEDAMAGE] = "modifier_rune_doubledamage",
		[DOTA_RUNE_HASTE] = "modifier_rune_haste",
		[DOTA_RUNE_INVISIBILITY] = "modifier_rune_invis",
		[DOTA_RUNE_REGENERATION] = "modifier_rune_regen",
		[DOTA_RUNE_BOUNTY] = nil,
		[DOTA_RUNE_ARCANE] = "modifier_rune_arcane",
		[DOTA_RUNE_WATER] = nil,
		[DOTA_RUNE_XP] = nil,
		-- [DOTA_RUNE_SHIELD] = "modifier_rune_shield",
	}
	local modifiers = {
		[DOTA_RUNE_DOUBLEDAMAGE] = "modifier_rune_crit",
		[DOTA_RUNE_HASTE] = "modifier_rune_phys_damage_increace",
		[DOTA_RUNE_INVISIBILITY] = "modifier_rune_all_damage_increace",
		[DOTA_RUNE_REGENERATION] = "modifier_rune_multicast",
		[DOTA_RUNE_BOUNTY] = nil,
		[DOTA_RUNE_ARCANE] = "modifier_rune_mage_damage_increace",
		[DOTA_RUNE_WATER] = nil,
		[DOTA_RUNE_XP] = nil,
		-- [DOTA_RUNE_SHIELD] = "modifier_rune_shield",
	}
	if runs[data.rune] ~= nil then
		local hHero = PlayerResource:GetSelectedHeroEntity(data.PlayerID)
		hHero:RemoveModifierByName(runs[data.rune])
		hHero:AddNewModifier(hHero, nil, modifiers[data.rune], {duration = 45})
	end
	Quests:UpdateCounter("daily", data.PlayerID, 45)
end

function CAddonAdvExGameMode:RDATestDisconnect()
	if not IS_TEST_MAP then return end
	local found = false
	for pid = 0, PlayerResource:GetPlayerCount() - 1 do
		local sid = PlayerResource:GetSteamAccountID(pid)
		if ChatCommands:IsAdmin(sid) or ChatCommands:IsTester(sid) then
			found = true
			break
		end
	end
	if not found then
		GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
	end
end

function create_runes()
	local target_bounty = Entities:FindAllByName("target_bounty")
	Timers:CreateTimer(0,function()
		for k,v in pairs(target_bounty) do
			if v.rune then
				UTIL_Remove(v.rune)
			end
			local point = v:GetAbsOrigin()
			v.rune = CreateRune(point, DOTA_RUNE_BOUNTY)
		end
		return 60
	end)
	local xp_rune = Entities:FindAllByName("xp_rune")
	Timers:CreateTimer(0,function()
		for k,v in pairs(xp_rune) do
			if v.rune then
				UTIL_Remove(v.rune)
			end
			local point = v:GetAbsOrigin()
			v.rune = CreateRune(point, DOTA_RUNE_XP)
		end
		return 60 * 3
	end)
	local power_rune = Entities:FindAllByName("power_rune")
	local r = {0,1,3,4,6}
	Timers:CreateTimer(20*60,function()
		for k,v in pairs(power_rune) do
			if v.rune then
				UTIL_Remove(v.rune)
			end
			local point = v:GetAbsOrigin()
			if EventHandler:IsInvokerKilled() == false then
				if RollPercentage(1) then
					local souls = {"item_dust_soul","item_swamp_soul","item_snow_soul","item_divine_soul","item_cemetery_soul","item_magma_soul"}
					local item_name = souls[RandomInt(1, #souls)]
					local newItem = CreateItem( item_name, nil, nil )
					local drop = CreateItemOnPositionForLaunch( point, newItem )
					newItem:LaunchLootInitialHeight( false, 0, 150, 0.5, point )
					newItem:SetContextThink( "KillLoot", function() return KillLoot( newItem, drop ) end, 60*2.5 )
				elseif RollPercentage(1) then
					local newItem = CreateItem( "item_points_big", nil, nil )
					local drop = CreateItemOnPositionForLaunch( point, newItem )
					newItem:LaunchLootInitialHeight( false, 0, 150, 0.5, point )
					newItem:SetContextThink( "KillLoot", function() return KillLoot( newItem, drop ) end, 60*2.5 )
				elseif RandomFloat(0, 100) < 0.1 then
					spawnPoint = point	
					local newItem = CreateItem( "item_crystal", nil, nil )
					local drop = CreateItemOnPositionForLaunch( point, newItem )
					newItem:LaunchLootInitialHeight( false, 0, 150, 0.5, point )
					if loot_duration then
						newItem:SetContextThink( "KillLoot", function() return KillLoot( newItem, drop ) end, 60*2.5 )
					end
				else
					v.rune = CreateRune(point, r[RandomInt(1, #r)])
				end
			else
				v.rune = CreateRune(point, r[RandomInt(1, #r)])
			end
		end
		return 60 * 2.5
	end)
end

--------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------

_G.rating_wave = 0
_G.mega_boss_bonus = 0
_G.raid_boss_2 = 0
_G.resp_time = 20
_G.rsh = 1


function bot(nPlayerID)
return PlayerResource:GetSteamAccountID(nPlayerID) < 1000
end

function rating_lose()
	for nPlayerID = 0, DOTA_MAX_PLAYERS - 1 do
		if PlayerResource:IsValidPlayer(nPlayerID) then
		local connectState = PlayerResource:GetConnectionState(nPlayerID)	
			if bot(nPlayerID) or connectState == DOTA_CONNECTION_STATE_ABANDONED or connectState == DOTA_CONNECTION_STATE_FAILED or connectState == DOTA_CONNECTION_STATE_UNKNOWN  then print("leave") else		
				if rat < 75 then
					-- DataBase:PointsChange(nPlayerID, ((-25 * diff_wave.rating_scale)+ mega_boss_bonus * diff_wave.rating_scale), true )
					DataBase:EndGameSession(nPlayerID, ((-25 * diff_wave.rating_scale)+ mega_boss_bonus * diff_wave.rating_scale))
				end
				if rat >= 75 then
					-- DataBase:PointsChange(nPlayerID, ((rating_wave * diff_wave.rating_scale) - (30 * diff_wave.rating_scale) + (mega_boss_bonus * diff_wave.rating_scale)), true)
					DataBase:EndGameSession(nPlayerID, ((rating_wave * diff_wave.rating_scale) - (30 * diff_wave.rating_scale) + (mega_boss_bonus * diff_wave.rating_scale)))
				end
			end	
		end			
	end
	CustomGameEventManager:Send_ServerToAllClients( "showEndScreen", {} )
end

_G.rating_win = function()
	for nPlayerID = 0, DOTA_MAX_PLAYERS - 1 do
		if PlayerResource:IsValidPlayer(nPlayerID) then
		local connectState = PlayerResource:GetConnectionState(nPlayerID)	
			if bot(nPlayerID) or connectState == DOTA_CONNECTION_STATE_ABANDONED or connectState == DOTA_CONNECTION_STATE_FAILED or connectState == DOTA_CONNECTION_STATE_UNKNOWN  then print("leave") else
				-- DataBase:PointsChange(nPlayerID, ((rating_wave * diff_wave.rating_scale) + (mega_boss_bonus * diff_wave.rating_scale)), true)
				DataBase:EndGameSession(nPlayerID, ((rating_wave * diff_wave.rating_scale) + (mega_boss_bonus * diff_wave.rating_scale)))
			end
		end
	end
	CustomGameEventManager:Send_ServerToAllClients( "showEndScreen", {game_reuslt = "win"} )
end

function full_win()
	for nPlayerID = 0, DOTA_MAX_PLAYERS - 1 do
		if PlayerResource:IsValidPlayer(nPlayerID) then
		local connectState = PlayerResource:GetConnectionState(nPlayerID)	
			if bot(nPlayerID) or connectState == DOTA_CONNECTION_STATE_ABANDONED or connectState == DOTA_CONNECTION_STATE_FAILED or connectState == DOTA_CONNECTION_STATE_UNKNOWN  then print("leave") elseif diff_wave.wavedef ~= "Easy" then
				-- DataBase:AddCoins(nPlayerID, 1)
				CustomShop:AddCoins(nPlayerID, 1, true, not DataBase:IsCheatMode())
			end
		end
	end
end

function check_insane_lives()
	local death_count = 0
	local total_count = 0
	for iPlayerID= 0,4 do
		if PlayerResource:IsValidPlayer(iPlayerID) then
			total_count = total_count + 1
			local hHero = PlayerResource:GetSelectedHeroEntity(iPlayerID)
			local mod
			if diff_wave.wavedef == "Unreal" then
				mod = hHero:FindModifierByName("modifier_unreal_lives")
			else
				mod = hHero:FindModifierByName("modifier_insane_lives")
			end
			if not hHero:IsReincarnating() and not hHero:WillReincarnate() and mod:GetStackCount() == 0 then
				death_count = death_count + 1
			end
		end
	end
	if death_count == total_count and EventHandler:IsInvokerKilled() == false and not DataBase:IsCheatMode() then
		rating_lose()
		Timers:CreateTimer(3,function()
			GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
		end)
	end
end

function kill_all_creeps()
	local enemies = FindUnitsInRadius(DOTA_TEAM_BADGUYS, Vector(0,0,0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )
	for _,unit in ipairs(enemies) do
		if unit:HasModifier("modifier_unit_on_death") then
			unit:ForceKill(false)		
		end	
	end	
end

function Add_bsa_hero()	
	if diff_wave.wavedef == "Normal" and not GameRules:IsCheatMode() then
		arr = {}
		players = {}
		for i = 0, PlayerResource:GetPlayerCount() - 1 do
			if PlayerResource:IsValidPlayer(i) then 
				players[i] = {sid = tostring(PlayerResource:GetSteamID(i))}
			end
		end
		arr['players'] = players
		arr = json.encode(arr)
		local req = CreateHTTPRequestScriptVM( "POST", "http://91.240.87.224/api_add_hero/?key=".._G.key )
		req:SetHTTPRequestGetOrPostParameter('arr',arr)
		req:SetHTTPRequestAbsoluteTimeoutMS(100000)
		req:Send(function(res)
			if res.StatusCode == 200 and res.Body ~= nil then
				print("DONE BSA HERO")
				print(res.StatusCode)
				print("DONE BSA HERO")
			else
				print("ERROR BSA HERO")
				print(res.StatusCode)
				print("ERROR BSA HERO")
			end
		end)
	end
end

function OnEndMiniGame(eventIndex, data)
	local hHero = PlayerResource:GetSelectedHeroEntity(data.PlayerID)
	local mod = hHero:FindModifierByName("modifier_cheack_afk")
	mod:EndMinigame()
end

function ItemBossSummonChoice(eventIndex, data)
	local bossTable = {
		[1] = "npc_forest_boss_fake",
		[2] = "npc_village_boss_fake",
		[3] = "npc_mines_boss_fake",
		[4] = "npc_dust_boss_fake",
		[5] = "npc_cemetery_boss_fake",
		[6] = "npc_swamp_boss_fake",
		[7] = "npc_snow_boss_fake",
		[8] = "npc_boss_location8_fake",
		[9] = "npc_boss_magma_fake"
	}
	local boss_spawn = bossTable[tonumber(data.index)+1]
	if boss_spawn then
		local point = Entities:FindByName(nil, "point_donate_creeps_"..data.PlayerID):GetAbsOrigin()
		local unit = CreateUnitByName(boss_spawn, point + RandomVector(RandomInt(0, 150)), true, nil, nil, DOTA_TEAM_BADGUYS)
		unit.PlayerID_Summoned = data.PlayerID
		_G.Boss_Donate[data.PlayerID][unit] = true
		unit:add_items()
		unit:AddNewModifier(unit, nil, "modifier_hp_regen_boss", {})
		Rules:difficality_modifier(unit)
	end
end















































------------------------------------------------------------------------------------
--переделанный энтити киллед
------------------------------------------------------------------------------------



local LaneCreep = {
    creep_1 = true,
    creep_2 = true,
    creep_3 = true,
    creep_4 = true,
    creep_5 = true,
    creep_6 = true,
    creep_7 = true,
    creep_8 = true,
    creep_9 = true,
    creep_10 = true,
}
local LaneComandirs = {
    comandir_creep_1 = true,
    comandir_creep_2 = true,
    comandir_creep_3 = true,
    comandir_creep_4 = true,
    comandir_creep_5 = true,
    comandir_creep_6 = true,
    comandir_creep_7 = true,
    comandir_creep_8 = true,
    comandir_creep_9 = true,
    comandir_creep_10 = true,
}
local LaneBosses = {
    boss_1 = true,
    boss_2 = true,
    boss_3 = true,
    boss_4 = true,
    boss_5 = true,
    boss_6 = true,
    boss_7 = true,
    boss_8 = true,
    boss_9 = true,
    boss_10 = true,
}

local BossLastLocation = {
    npc_sand_king_boss = true,
    npc_dota_monkey_king_boss = true,
    npc_titan_boss = true,
    npc_appariion_boss = true,
    npc_crystal_boss = true,
}

local ModifiersDifficult = {
    Easy = "modifier_easy",
    Normal = "modifier_normal",
    Hard = "modifier_hard",
    Ultra = "modifier_ultra",
    Insane = "modifier_insane",
    Impossible = "modifier_impossible",
    Unreal = "modifier_unreal",
}

local Boxes = {
    Easy = "box_1",
    Normal = "box_2",
    Hard = "box_3",
    Ultra = "box_3",
    Insane = "box_3",
    Impossible = "box_3",
    Unreal = "box_3",
}

local BossesSphere = {
    npc_forest_boss = {
        gold = 500,
        soul = "item_forest_soul",
    },
    npc_village_boss = {
        gold = 1000,
        soul = "item_village_soul",
    },
    npc_mines_boss = {
        gold = 1500,
        soul = "item_mines_soul",
    },
    npc_dust_boss = {
        gold = 2000,
        soul = "item_dust_soul",
    },
    npc_cemetery_boss = {
        gold = 2500,
        soul = "item_cemetery_soul",
    },
    npc_swamp_boss = {
        gold = 3000,
        soul = "item_swamp_soul",
    },
	npc_snow_boss = {
        gold = 3500,
        soul = "item_snow_soul",
    },
    npc_boss_location8 = {
        gold = 4000,
        soul = "item_divine_soul",
    },
    npc_boss_magma = {
        gold = 5000,
        soul = "item_magma_soul",
    },
    npc_mega_boss = {
        gold = 50000,
        soul = "item_antimage_soul",
    },
}

local BossesBarracks = {
    badguys_creeps = "npc_boss_barrack1",
    badguys_comandirs = "npc_boss_barrack2",
    badguys_boss = "npc_byrocktar",
}

function IsLaneCreep(killedUnit_name)
    return LaneCreep[killedUnit_name]
end

function IsLaneComandir(killedUnit_name)
    return LaneComandirs[killedUnit_name]
end

function IsLaneBoss(killedUnit_name)
    return LaneBosses[killedUnit_name]
end

function IsBossLastLocation(killedUnit_name)
    return BossLastLocation[killedUnit_name]
end

function RespawnWithDelay(delay, unit)
    local point = unit.vInitialSpawnPos
    unit:SetContextThink(DoUniqueString("respawn"), function(unit)
        FindClearSpaceForUnit(unit, point, false)
        unit:Stop()
        unit:RespawnUnit()
        -- Rules:difficality_modifier(unit)
    end, delay)
end

function set_max_stats(unit)
	local max_set = 2000000000
	if unit:GetBaseMagicalResistanceValue() >= 99 then unit:SetBaseMagicalResistanceValue(99) end
	if unit:GetBaseDamageMin() >= max_set then unit:SetBaseDamageMin(max_set) end
	if unit:GetBaseDamageMax() >= max_set then unit:SetBaseDamageMax(max_set) end
	if unit:GetMaxHealth() >= max_set then
		unit:SetBaseMaxHealth(max_set)
		unit:SetMaxHealth(max_set)
		unit:SetHealth(max_set)
	end  
end

function add_feed(id)
	if not GameRules:IsCheatMode() then
		Timers:CreateTimer(0.5, function()
			if id ~= nil then 
				if RandomInt(0,100) <= 8 then
					local hero = PlayerResource:GetSelectedHeroEntity(id)
					EmitSoundOn( "ui.treasure_03", hero )
					local effect_cast = ParticleManager:CreateParticle( "particles/econ/taunts/wisp/wisp_taunt_explosion_fireworks.vpcf", PATTACH_ABSORIGIN, hero )
					ParticleManager:SetParticleControl( effect_cast, 0, hero:GetOrigin() )
					ParticleManager:SetParticleControl( effect_cast, 1, Vector( 2, 0, 0 ) )
					ParticleManager:ReleaseParticleIndex( effect_cast )
					DataBase:AddFeed(id, RandomInt(25,75))
				end
			end
		end)
	end
end

function CAddonAdvExGameMode:OnEntityKilled( keys )
	local killedUnit = EntIndexToHScript( keys.entindex_killed )
	local killerEntity = EntIndexToHScript( keys.entindex_attacker )
	local killedUnit_name = killedUnit:GetUnitName()
	if killerEntity then
		killerEntity_playerID = killerEntity:GetPlayerOwnerID()
	end	
--визуал для панельки справа
	for _, name in pairs(bosses_names) do
		if name == killedUnit_name then
			if killerEntity:GetPlayerOwnerID() then
				local hero = PlayerResource:GetSelectedHeroEntity(killerEntity:GetPlayerOwnerID())
				hero:IncrementKills(1)
				break
			end
		end
	end
--босы в донатке
	if _G.Boss_Donate and killedUnit.PlayerID_Summoned and _G.Boss_Donate[killedUnit.PlayerID_Summoned] and _G.Boss_Donate[killedUnit.PlayerID_Summoned][killedUnit] then
		local t = {}
		for k,v in pairs(_G.Boss_Donate[killedUnit.PlayerID_Summoned]) do
			if not _G.Boss_Donate[killedUnit.PlayerID_Summoned][killedUnit] then
				t[k] = v
			end
		end
		_G.Boss_Donate[killedUnit.PlayerID_Summoned] = t
	end
--донатный талант и режимы сложности
    if killedUnit:IsHero() and not killedUnit:IsReincarnating() then
		if diff_wave.wavedef == "Insane" or diff_wave.wavedef == "Impossible" or diff_wave.wavedef == "Unreal" then
			local mod
			if diff_wave.wavedef == "Unreal" then
				mod = killedUnit:FindModifierByName("modifier_unreal_lives")
			else
				mod = killedUnit:FindModifierByName("modifier_insane_lives")
			end
			if not killedUnit:IsReincarnating() and not killedUnit:WillReincarnate() and killedUnit:UnitCanRespawn() and not EventHandler:IsInvokerKilled() then
				mod:DecrementStackCount()
			end
			if mod:GetStackCount() > 0 then
				if killedUnit:HasModifier("modifier_don5") then
					killedUnit:SetTimeUntilRespawn( 1.2 )
				else
					killedUnit:SetTimeUntilRespawn( 10 )
				end
			else
				killedUnit:SetRespawnsDisabled(true)
			end
			check_insane_lives()
		else
			if killedUnit:HasModifier("modifier_don5") then
				killedUnit:SetTimeUntilRespawn( 1.2 )
			else
				killedUnit:SetTimeUntilRespawn( 10 )
			end
		end
	end

--лайновые юниты
-------------------------------
	if IsLaneCreep(killedUnit_name) then
		local heroes = FindUnitsInRadius(killerEntity:GetTeamNumber(), killedUnit:GetAbsOrigin(), nil, 1100, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_ANY_ORDER, false )
		for i = 1, #heroes do
			local gold = golddrop / #heroes
            local playerID = heroes[i]:GetPlayerID()
            local player = PlayerResource:GetSelectedHeroEntity(playerID )
            player:ModifyGoldFiltered( gold, true, 0 )
			-- herogold:addGold(playerID,gold)
            SendOverheadEventMessage(player, OVERHEAD_ALERT_GOLD, player, gold, nil)
		end
		return
	end

	if IsLaneComandir(killedUnit_name) then
		local heroes = FindUnitsInRadius(killerEntity:GetTeamNumber(), killedUnit:GetAbsOrigin(), nil, 1100, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_ANY_ORDER, false )
		for i = 1, #heroes do
			local gold = (golddrop / #heroes) * 2
			local playerID = heroes[i]:GetPlayerID()
			local player = PlayerResource:GetSelectedHeroEntity(playerID )
			player:ModifyGoldFiltered( gold, true, 0 )
			SendOverheadEventMessage(player, OVERHEAD_ALERT_GOLD, player, gold, nil)
		end
		return
	end
	
	if IsLaneBoss(killedUnit_name) and (not killedUnit:HasModifier("modifier_health"))  then
	local heroes = FindUnitsInRadius(killerEntity:GetTeamNumber(), killedUnit:GetAbsOrigin(), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_ANY_ORDER, false )
		for i = 1, #heroes do
			local gold = 5000 / #heroes
            local playerID = heroes[i]:GetPlayerID()
            local player = PlayerResource:GetSelectedHeroEntity(playerID )
            player:ModifyGoldFiltered( gold, true, 0 )
            SendOverheadEventMessage(player, OVERHEAD_ALERT_GOLD, player, gold, nil)
			player:EmitSound("Hero_LegionCommander.Duel.Victory")
			local duel_victory_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_legion_commander/legion_commander_duel_victory.vpcf", PATTACH_ABSORIGIN_FOLLOW, player)
		end
		return
	end

-------------------------------
--босы бараков
    if BossesBarracks[killedUnit_name] ~= nil then
        _G.destroyed_barracks = true
        Spawner:SpawnBaracksBosses(BossesBarracks[killedUnit_name])
        return
    end
--убиство боса со сферами
    if BossesSphere[string.sub(killedUnit_name,0,-6)] ~= nil or BossesSphere[killedUnit_name] ~= nil then
		--не дропать душы для полиморфа
		if killedUnit.NoDropSoul and killedUnit:NoDropSoul() then
			return
		end
		--донатка
        if string.find( killedUnit_name, "_fake" ) then
			killedUnit_name = string.sub(killedUnit_name,0,-6)
            StashOfSouls:AddSoul(BossesSphere[killedUnit_name].soul, killerEntity_playerID)
            local unit = PlayerResource:GetSelectedHeroEntity(killerEntity_playerID)
            unit:ModifyGoldFiltered( BossesSphere[killedUnit_name].gold, true, 0 )
            SendOverheadEventMessage(unit, OVERHEAD_ALERT_GOLD, unit, BossesSphere[killedUnit_name].gold, nil)
			unit:ModifyStrength(BossesSphere[killedUnit_name].gold / 100)
			unit:ModifyAgility(BossesSphere[killedUnit_name].gold / 100)
			unit:ModifyIntellect(BossesSphere[killedUnit_name].gold / 100)
        --обычная лока
        else
            if killedUnit_name ~= "npc_mega_boss" then
                RespawnWithDelay(diff_wave.respawn, killedUnit)
            end
            add_feed(killerEntity_playerID)
            for iPlayerID = 0, PlayerResource:GetPlayerCount() - 1 do
                if PlayerResource:IsValidPlayer(iPlayerID) and PlayerResource:HasSelectedHero(iPlayerID) then
                    local unit = PlayerResource:GetSelectedHeroEntity(iPlayerID)
            		StashOfSouls:AddSoul(BossesSphere[killedUnit_name].soul, iPlayerID)
					unit:ModifyGoldFiltered( BossesSphere[killedUnit_name].gold, true, 0 )
                    SendOverheadEventMessage(unit, OVERHEAD_ALERT_GOLD, unit, BossesSphere[killedUnit_name].gold, nil)
					unit:ModifyStrength(BossesSphere[killedUnit_name].gold / 100)
					unit:ModifyAgility(BossesSphere[killedUnit_name].gold / 100)
					unit:ModifyIntellect(BossesSphere[killedUnit_name].gold / 100)
                end
            end
        end
		return
    end

    if killedUnit_name == "roshan_npc"  then
        _G.rsh = rsh + 1
        add_feed(killerEntity_playerID)
        local roshan = killedUnit
        Timers:CreateTimer(15, function()
            local ent = Entities:FindByName( nil, "roshan_npc_point")
            local point = ent:GetAbsOrigin()
            FindClearSpaceForUnit(roshan, point, false)
            roshan:Stop()
            roshan:RespawnUnit()			
            roshan:SetBaseDamageMin(roshan:GetBaseDamageMin() * 1.5)
            roshan:SetBaseDamageMax(roshan:GetBaseDamageMax() * 1.5)
            roshan:SetPhysicalArmorBaseValue(roshan:GetPhysicalArmorBaseValue() * 2)
            roshan:SetBaseMagicalResistanceValue(roshan:GetBaseMagicalResistanceValue() * 2)
            roshan:SetMaxHealth(roshan:GetMaxHealth() *2 )
            roshan:SetBaseMaxHealth(roshan:GetBaseMaxHealth() * 2)
            roshan:SetHealth(roshan:GetMaxHealth()* 2)		
            set_max_stats(roshan)
        end)
        return
    end	

    if string.find( killedUnit_name, "raid_boss" ) then
        add_feed(killerEntity_playerID)
        local point = killedUnit:GetAbsOrigin()
        if killedUnit_name == "raid_boss2" then
            point = Entities:FindByName( nil, "point_center2"):GetAbsOrigin()
        end
        if diff_wave.wavedef ~= "Easy" then
            CreateUnitByName(Boxes[diff_wave.wavedef], point + RandomVector( RandomFloat( 0, 150 )), true, nil, nil, DOTA_TEAM_BADGUYS)
        end
    end
	if killedUnit_name == "npc_dota_goodguys_fort" and not DataBase:IsCheatMode() then
			for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS - 1 do
				if PlayerResource:GetTeam( nPlayerID ) == DOTA_TEAM_GOODGUYS then
					if PlayerResource:HasSelectedHero( nPlayerID ) then
						local hero = PlayerResource:GetSelectedHeroEntity( nPlayerID )
						PlayerResource:SetCameraTarget(hero:GetPlayerOwnerID(), killedUnit)
						Timers:CreateTimer(0.1, function()
						PlayerResource:SetCameraTarget(hero:GetPlayerOwnerID(), nil)
					end)
				end
			end
		end
		if EventHandler:IsInvokerKilled() == false then
			rating_lose()
		end
		Timers:CreateTimer(3,function() 
			GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
		end)
		return
	end

	if killedUnit_name == "npc_invoker_boss" and (not killedUnit:HasModifier("modifier_health_voker")) then
		local point = Entities:FindByName( nil, "point_bara"):GetAbsOrigin()
		local unit = CreateUnitByName("npc_bara_boss_main", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		Rules:difficality_modifier(unit)
		kill_all_creeps()
		GameRules:SendCustomMessage("#invok_chat",0,0)
		Add_bsa_hero()
		local vok =  {"invoker_invo_death_02","invoker_invo_death_08","invoker_invo_death_10","invoker_invo_death_13","invoker_invo_death_01"}
		killedUnit:EmitSound(vok[RandomInt(1, #vok)])
		local hRelay = Entities:FindByName( nil, "belka_logic" )
		hRelay:Trigger(nil,nil)
		for iPlayerID =0,4 do
			local hHero = PlayerResource:GetSelectedHeroEntity(iPlayerID)
			if hHero and not hHero:IsAlive() then
				if diff_wave.wavedef == "Insane" or diff_wave.wavedef == "Impossible" or diff_wave.wavedef == "Unreal" then
					hHero:SetRespawnsDisabled(false)
				end
				hHero:RespawnHero(false,false)
			end
		end
		
		return
	end

	if killedUnit_name == "npc_bara_boss_main" and not DataBase:IsCheatMode() then
		local unit = CreateUnitByName("npc_sand_king_boss", Vector(7987, -11138, 652), true, nil, nil, DOTA_TEAM_BADGUYS)
		Rules:difficality_modifier(unit)
		local unit = CreateUnitByName("npc_dota_monkey_king_boss", Vector(7812, -9992, 652), true, nil, nil, DOTA_TEAM_BADGUYS)
		Rules:difficality_modifier(unit)
		local unit = CreateUnitByName("npc_titan_boss", Vector(8762, -9342, 653), true, nil, nil, DOTA_TEAM_BADGUYS)
		Rules:difficality_modifier(unit)
		local unit = CreateUnitByName("npc_appariion_boss", Vector(9779, -9990, 653), true, nil, nil, DOTA_TEAM_BADGUYS)
		Rules:difficality_modifier(unit)
		local unit = CreateUnitByName("npc_crystal_boss", Vector(9533, -11194, 653), true, nil, nil, DOTA_TEAM_BADGUYS)
		local antimage = Entities:FindByName( nil, "npc_mega_boss")
		if antimage then
			local m = antimage:FindModifierByName("modifier_invulnerable")
			if m then
				m:Destroy()
			end
		end
		Rules:difficality_modifier(antimage)
		for nPlayerID = 0, PlayerResource:GetPlayerCount() - 1 do
			if PlayerResource:IsValidPlayer(nPlayerID) then
				local hHero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
				if hHero then
					hHero:ModifyAgility(1000)
					hHero:ModifyStrength(1000)
					hHero:ModifyIntellect(1000)
				end
			end
		end
		--@todo: addbara revard
		return
	end

	if IsBossLastLocation(killedUnit_name) then
		--@todo: add 5 boss revard
		return
	end

	if killedUnit_name == "npc_boss_plague_squirrel" and not DataBase:IsCheatMode() then
		_G.npc_boss_plague_squirrel = killedUnit
		full_win()
		Timers:CreateTimer(3,function() 
			GameRules:SetGameWinner(DOTA_TEAM_GOODGUYS)
		end)
		return
	end
	

	if killedUnit_name == "badguys_fort"  then
		local vPoint1 = Entities:FindByName( nil, "line_spawner"):GetAbsOrigin()
		local invoker = CreateUnitByName( "npc_invoker_boss", vPoint1 + RandomVector( RandomInt( 0, 50 )), true, nil, nil, DOTA_TEAM_BADGUYS )
		local staki = math.floor(wave)
		if diff_wave:GetGameDifficulty() == "unreal" then
			invoker:SetBaseDamageMin(invoker:GetBaseDamageMin() * wave * 8)
			invoker:SetBaseDamageMax(invoker:GetBaseDamageMax() * wave * 8)
			invoker:SetPhysicalArmorBaseValue(invoker:GetPhysicalArmorBaseValue() * wave * 8)
			invoker:SetBaseMagicalResistanceValue(invoker:GetBaseMagicalResistanceValue() * wave * 1.8)
			invoker:SetMaxHealth(invoker:GetMaxHealth() * wave * 20)
			invoker:SetBaseMaxHealth(invoker:GetBaseMaxHealth() * wave * 20)
			invoker:SetHealth(invoker:GetMaxHealth() * wave * 20)
			staki = math.floor(wave * 20)
		else
			invoker:SetBaseDamageMin(invoker:GetBaseDamageMin() * wave * 4)
			invoker:SetBaseDamageMax(invoker:GetBaseDamageMax() * wave * 4)
			invoker:SetPhysicalArmorBaseValue(invoker:GetPhysicalArmorBaseValue() * wave * 3)
			invoker:SetBaseMagicalResistanceValue(invoker:GetBaseMagicalResistanceValue() * wave * 1.4)
			invoker:SetMaxHealth(invoker:GetMaxHealth() * wave * 10)
			invoker:SetBaseMaxHealth(invoker:GetBaseMaxHealth() * wave * 10)
			invoker:SetHealth(invoker:GetMaxHealth() * wave * 10)	
		end
		
		inv_item = 0
		while inv_item < 2 do
			add_item = items_level_inv[RandomInt(1,#items_level_inv)]
				while not invoker:HasItemInInventory(add_item) and add_item ~= "item_kaya_lua" do
				inv_item = inv_item + 1
				invoker:AddItemByName(add_item):SetLevel(10)
			end
		end
		local mg_resist = invoker:GetBaseMagicalResistanceValue()
		if diff_wave:GetGameDifficulty() ~= "unreal" then
			if mg_resist >= 99 then
				if invoker:FindItemInInventory("item_pipe_lua") then
					invoker:SetBaseMagicalResistanceValue(96)
				else
					invoker:SetBaseMagicalResistanceValue(98)
				end
			end
		end
		-- local armor = invoker:GetPhysicalArmorBaseValue()
		-- if armor >= 500 then invoker:SetPhysicalArmorBaseValue(500)
		-- end
		
		local total_hp = invoker:GetMaxHealth()
		local porog_hp = 100000000
		local stack_modifier = math.floor(total_hp/porog_hp)
		if total_hp < porog_hp then
			invoker:SetBaseMaxHealth(porog_hp)
			invoker:SetMaxHealth(porog_hp)
			invoker:SetHealth(porog_hp)
			invoker:AddNewModifier(invoker, nil, "modifier_spell_ampl_creep", nil):SetStackCount(staki)	 

			invoker:AddNewModifier(invoker, nil, ModifiersDifficult[diff_wave.wavedef], {})
			if diff_wave.wavedef == "Insane" or diff_wave.wavedef == "Impossible" or diff_wave.wavedef == "Unreal" then
				new_abil_passive = abiility_passive[RandomInt(1,#abiility_passive)]
				invoker:AddAbility(new_abil_passive):SetLevel(4)
			end	
		else
			invoker:SetBaseMaxHealth(porog_hp)
			invoker:SetMaxHealth(porog_hp)
			invoker:SetHealth(porog_hp)
			invoker:AddNewModifier(invoker, nil, "modifier_health_voker", nil):SetStackCount(stack_modifier)
			invoker:AddNewModifier(invoker, nil, "modifier_spell_ampl_creep", nil):SetStackCount(staki)	 
			invoker:AddNewModifier(invoker, nil, ModifiersDifficult[diff_wave.wavedef], {}) 
		end
		return
	end
	if killedUnit_name == "npc_raid_sheep" then
		for pid = 0, 4 do
			local items = Casino:GetRandomItemSheep()
			CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer(pid), "show_sheep_event", items )
		end
	end
end

_G.ApplyDamageRDA =  function(keys)
	local victim = keys.victim
	local attacker = keys.attacker
	local damage = keys.damage
	local damage_type = keys.damage_type
	local damage_flags = keys.damage_flags
	local ability = keys.ability
	local damage_amplification = keys.damage_amplification
	local damage = keys.damage
	local damage_bp_7 = 0
	local crit_damage = 0
	if attacker and attacker:IsRealHero() and victim:GetTeamNumber() == DOTA_TEAM_BADGUYS then
		local pid = attacker:GetPlayerID()
		local ability_pet = attacker:FindAbilityByName("spell_item_pet_rda_bp_7")
		if ability_pet and ability_pet:GetLevel() > 0 then -- pet additional damage from intelligence
			local bonus_spell_damage_intelligence = ability_pet:GetSpecialValueFor("bonus_spell_damage_intelligence")
			local bonus_damage_per_stack = ability_pet:GetSpecialValueFor("bonus_damage_per_stack")
			local modifier_item_pet_rda_bp_7 = attacker:FindModifierByName("modifier_item_pet_rda_bp_7")
			damage_bp_7 = damage_bp_7 + (attacker:GetIntellect(true) / 100 * bonus_spell_damage_intelligence)
			damage_bp_7 = damage_bp_7 + (attacker:GetIntellect(true) / 100 * modifier_item_pet_rda_bp_7:GetStackCount() * bonus_damage_per_stack)
			if Talents:CalculateLevelFromExperience(Talents.player[pid].totalexp) < ability_pet:GetSpecialValueFor("low_level_to") then
				damage_bp_7 = damage_bp_7 - damage_bp_7 / 100 * ability_pet:GetSpecialValueFor("low_level_damage_reduction")
			end
		end
		local magic_crit = {}
		local ability_rosh = attacker:FindAbilityByName("spell_item_pet_rda_roshan_3")
		if ability_rosh and ability_rosh:GetLevel() > 0 then -- roshan magic crit
			table.insert(magic_crit, {
				["chance"] = ability_rosh:GetSpecialValueFor("magic_crit_chance"),
				["crit_multiplier"] = ability_rosh:GetSpecialValueFor("magic_crit_damage"),
			})
		end
		local modifier_item_magic_crit_lua = attacker:FindModifierByName("modifier_item_magic_crit_lua")
		if modifier_item_magic_crit_lua then
			table.insert(magic_crit, {
				["chance"] = modifier_item_magic_crit_lua:GetAbility():GetSpecialValueFor("crit_chance"),
				["crit_multiplier"] = modifier_item_magic_crit_lua:GetAbility():GetSpecialValueFor("crit_multiplier"),
			})
		end
		if #magic_crit > 0 and damage_type == DAMAGE_TYPE_MAGICAL then
			table.sort(magic_crit, function(a, b)
				return a.crit_multiplier > b.crit_multiplier
			end)
			for i = 1, #magic_crit do
				if RandomInt(1, 100) <= magic_crit[i].chance then
					crit_damage = (damage + damage_bp_7) / 100 * (magic_crit[i].crit_multiplier - 100)
					local particle_damage = math.max(1, math.floor(damage + damage_bp_7 + crit_damage))
					local particle = ParticleManager:CreateParticle("particles/msg_fx/msg_crit.vpcf", PATTACH_OVERHEAD_FOLLOW, victim)
					ParticleManager:SetParticleControl(particle, 1, Vector(9, particle_damage, 4))
					ParticleManager:SetParticleControl(particle, 2, Vector(2, string.len(tostring(particle_damage)) + 1, 0))
					ParticleManager:SetParticleControl(particle, 3, Vector(19, 26, 166))
					ParticleManager:ReleaseParticleIndex(particle)
					-- victim:EmitSoundParams( "Item.Brooch.Target.Ranged", 0, 1, 0 )
					break
				end
			end
		end
	end
	-- Item.Brooch.Target.Ranged
	damage = damage + crit_damage + damage_bp_7
	if damage_amplification ~= nil and damage_amplification ~= 1 then
		damage = damage * damage_amplification
	end
	return ApplyDamage({
		victim = victim,
		attacker = attacker,
		damage = damage,
		damage_type = damage_type,
		damage_flags = damage_flags,
		ability = ability
	})
end

_G.HandleError = function (status, err)
	if not status and not IS_SERVER_LOADING then
		GameRules:SendCustomMessage(err,0,0)
		print(err)
	end
end

local PolymorphIndex = 1
local PolymorphNames = {
    "npc_forest_boss",
    "npc_village_boss",
    "npc_mines_boss",
    "npc_dust_boss",
    "npc_cemetery_boss",
    "npc_swamp_boss",
	"npc_snow_boss",
    "npc_boss_location8",
    "npc_boss_magma",
}

function SpawnPolymorph()
	if PolymorphIndex > #PolymorphNames then
		PolymorphIndex = 1
	end
	local unit = CreateUnitByName(PolymorphNames[PolymorphIndex], Vector(7987, -11138, 652), true, nil, nil, DOTA_TEAM_BADGUYS)
	unit:add_items()
	add_modifier(unit)
	return unit
end

function RespawnSquirrel()
	if _G.npc_boss_plague_squirrel then
		_G.npc_boss_plague_squirrel:RespawnUnit()
		--вся логика в модификаторе
		_G.npc_boss_plague_squirrel:AddNewModifier(_G.npc_boss_plague_squirrel, nil, "modifier_npc_boss_plague_squirrel_terminatorV2", {})
	end
end



















Convars:RegisterCommand( "chek_localize", function( cmd )
    local main_file = "addon_english"
    local compare_to = "addon_russian"
    local main_kv = LoadKeyValues("resource/" .. main_file..".txt")["Tokens"]
    local compare_kv = LoadKeyValues("resource/" .. compare_to..".txt")["Tokens"]
    for k,v in pairs(main_kv) do
        if compare_kv[k] == nil then
            print(k)
        end
    end
    end, "chek_localize", FCVAR_CHEAT
)
Convars:RegisterCommand( "test_console_command_1", function( cmd )
	local hHero = PlayerResource:GetSelectedHeroEntity(0)
	print(hHero:GetSecondsPerAttack(true))
	print(hHero:FindAbilityByName("bounty_hunter_track_lua"):GetSpecialValueFor("radius_passive"))
	print(hHero:HasModifier("modifier_bounty_hunter_track_lua_aura"))
    end, "chek_localize", FCVAR_CHEAT
)
Convars:RegisterCommand( "test_console_command_2", function( cmd )
	local hHero = PlayerResource:GetSelectedHeroEntity(0)
	if hHero then
		hHero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
		print(hHero:HasMovementCapability())
		hHero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_GROUND)
	end
    end, "chek_localize", FCVAR_CHEAT
)
Convars:RegisterCommand( "test_console_command_3", function( cmd )
	local hHero = PlayerResource:GetSelectedHeroEntity(0)
	if hHero then
		for i=0, hHero:GetAbilityCount()-1 do
			local abi = hHero:GetAbilityByIndex(i)
			if abi then
				print(abi:GetAbilityName())
				local mod
				if abi:GetIntrinsicModifierName() then
					mod = hHero:FindModifierByName(abi:GetIntrinsicModifierName())
				end
				if mod then
					mod:Destroy()
				end
				hHero:AddNewModifier(hHero, abi, abi:GetIntrinsicModifierName(), {})
			end
		end 
	end
    end, "chek_localize", FCVAR_CHEAT
)