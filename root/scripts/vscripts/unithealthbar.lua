if UnitHealthBar == nil then
    _G.UnitHealthBar = class({})
end

function UnitHealthBar:init()
    self.list = {}
end

function UnitHealthBar:AddUnit( unit )
    local index = unit:entindex()
    table.insert(self.list, unit:entindex())
    CustomNetTables:SetTableValue('UnitHealthBar', "units", self.list)
end

function UnitHealthBar:UpdateCustonHealth( unit, hp_float, hp_exponent )
    local index = unit:entindex()
    CustomNetTables:SetTableValue('UnitHealthBar', tostring(index), {
        ["hp_float"] = hp_float,
        ["hp_exponent"] = hp_exponent,
    })
end

UnitHealthBar:init()