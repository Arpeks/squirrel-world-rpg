if EventHandler == nil then
    _G.EventHandler = class(GameEventHandler)
end

function EventHandler:init()
    self:RegisterGameEventHandlers()
    self.killed_units = {}
end

function EventHandler:OnGameRulesStateChange()
    if QuestsRPG then
        QuestsRPG:OnGameRulesStateChange()
    end
end

function EventHandler:OnEntityKilled(keys)
    if QuestsRPG then
        QuestsRPG:OnEntityKilled(keys)
    end
    if Quests then
        Quests:OnEntityKilled(keys)
    end
    local killedUnit = EntIndexToHScript(keys.entindex_killed)
    local killedUnitName = killedUnit:GetUnitName()
    local killerEntity = EntIndexToHScript(keys.entindex_attacker)

    if killedUnitName == "npc_forest_boss" or killedUnitName == "npc_forest_boss_fake" then
        self:OnForestBossKilled()
    elseif killedUnitName == "npc_village_boss" or killedUnitName == "npc_village_boss_fake" then
        self:OnVillageBossKilled()
    elseif killedUnitName == "npc_mines_boss" or killedUnitName == "npc_mines_boss_fake" then
        self:OnMinesBossKilled()
    elseif killedUnitName == "npc_dust_boss" or killedUnitName == "npc_dust_boss_fake" then
        self:OnDustBossKilled()
    elseif killedUnitName == "npc_cemetery_boss" or killedUnitName == "npc_cemetery_boss_fake" then
        self:OnCemeteryBossKilled()
    elseif killedUnitName == "npc_swamp_boss" or killedUnitName == "npc_swamp_boss_fake" then
        self:OnSwampBossKilled()
    elseif killedUnitName == "npc_snow_boss" or killedUnitName == "npc_snow_boss_fake" then
        self:OnSnowBossKilled()
    elseif killedUnitName == "npc_boss_location8" or killedUnitName == "npc_boss_location8_fake" then
        self:OnDivineBossKilled()
    elseif killedUnitName == "npc_boss_magma" or killedUnitName == "npc_boss_magma_fake" then
        self:OnMagmaBossKilled()
    end

    local modifier_health = {"modifier_health_voker", "modifier_health", "modifier_unit_on_death", "modifier_unit_on_death2"}
    if not self.killed_units[killedUnitName] and not table.any(modifier_health, function(modifier_name) return killedUnit:HasModifier(modifier_name) end) then
        self.killed_units[killedUnitName] = true
    end

    if killedUnitName == "npc_invoker_boss" and not killedUnit:HasModifier("modifier_health_voker") then
        self:OnInvokerKilled()
    end
end

function EventHandler:IsInvokerKilled()
    return (self.killed_units["npc_invoker_boss"] == true)
end

function EventHandler:IsBarrackKilled()
	return (self.killed_units["badguys_creeps"] == true or self.killed_units["badguys_comandirs"] == true or self.killed_units["badguys_boss"] == true)
end

function EventHandler:IsKilled(name)
    return (self.killed_units[name] == true)
end

function EventHandler:IsForestBossKilled()
    return (self.killed_units["npc_forest_boss"] == true or self.killed_units["npc_forest_boss_fake"] == true)
end

function EventHandler:IsVillageBossKilled()
    return (self.killed_units["npc_village_boss"] == true or self.killed_units["npc_village_boss_fake"] == true)
end

function EventHandler:IsMinesBossKilled()
    return (self.killed_units["npc_mines_boss"] == true or self.killed_units["npc_mines_boss_fake"] == true)
end

function EventHandler:IsDustBossKilled()
    return (self.killed_units["npc_dust_boss"] == true or self.killed_units["npc_dust_boss_fake"] == true)
end

function EventHandler:IsCemeteryBossKilled()
    return (self.killed_units["npc_cemetery_boss"] == true or self.killed_units["npc_cemetery_boss_fake"] == true)
end

function EventHandler:IsSwampBossKilled()
    return (self.killed_units["npc_swamp_boss"] == true or self.killed_units["npc_swamp_boss_fake"] == true)
end

function EventHandler:IsDivineBossKilled()
    return (self.killed_units["npc_boss_location8"] == true or self.killed_units["npc_boss_location8_fake"] == true)
end

function EventHandler:IsMagmaBossKilled()
    return (self.killed_units["npc_boss_magma"] == true or self.killed_units["npc_boss_magma_fake"] == true)
end

function EventHandler:OnForestBossKilled()
    if QuestsRPG then
        QuestsRPG:OnForestBossKilled()
    end
end

function EventHandler:OnVillageBossKilled()
    if QuestsRPG then
        QuestsRPG:OnVillageBossKilled()
    end
end

function EventHandler:OnMinesBossKilled()
    if QuestsRPG then
        QuestsRPG:OnMinesBossKilled()
    end
end

function EventHandler:OnDustBossKilled()
    if QuestsRPG then
        QuestsRPG:OnDustBossKilled()
    end
end

function EventHandler:OnCemeteryBossKilled()
    if QuestsRPG then
        QuestsRPG:OnCemeteryBossKilled()
    end
end

function EventHandler:OnSwampBossKilled()
end

function EventHandler:OnSnowBossKilled()
end

function EventHandler:OnDivineBossKilled()
end

function EventHandler:OnMagmaBossKilled()
end

function EventHandler:OnInvokerKilled()
    if QuestsRPG then
        QuestsRPG:OnInvokerKilled()
    end
    if not DataBase:IsCheatMode() then
        for pid = 0, PlayerResource:GetPlayerCount()-1 do
            Quests:UpdateCounter("daily", pid, 28)
        end
        rating_win()
    end
end

function EventHandler:OnHeroInventoryItemChange(event)
    if QuestsRPG then
        QuestsRPG:ExploreInventory(event.player_id)
    end
end

function EventHandler:OnItemCombined(event)
    if QuestsRPG then
        QuestsRPG:ExploreInventory(event.PlayerID)
    end
end

function EventHandler:OnItemPickedUp(event)
    if QuestsRPG then
        QuestsRPG:ExploreInventory(event.PlayerID)
    end
end

function EventHandler:OnItemPurchased(event)
    if QuestsRPG then
        QuestsRPG:ExploreInventory(event.PlayerID)
    end
end

function EventHandler:OnWaveSpawned()
    if QuestsRPG then
        QuestsRPG:OnWaveSpawned()
    end
end

function EventHandler:OnDealDamage(t)
    if QuestsRPG then
        QuestsRPG:OnDealDamage(t)
    end
    if Quests then
        Quests:OnDealDamage(t)
    end
end

function EventHandler:OnTakeDamage(t)
    if QuestsRPG then
        QuestsRPG:OnTakeDamage(t)
    end
end

function EventHandler:OnMoving(t)
    if QuestsRPG then
        QuestsRPG:OnMoving(t)
    end
    if Quests then
        Quests:OnMoving(t)
    end
end

function EventHandler:OnRunePickup(data)
    if QuestsRPG then
        QuestsRPG:OnRunePickup(data)
    end
end

function EventHandler:OnItemForgeUpgrade(pid, item)
    if QuestsRPG then
        QuestsRPG:OnItemForgeUpgrade(pid, item)
    end
end

function EventHandler:OnGetCloseToTarget(pid, unit_name, name)
    if QuestsRPG then
        QuestsRPG:OnGetCloseToTarget(pid, unit_name, name)
    end
end

function EventHandler:OnPlayerGainedLevel(t)
    if QuestsRPG then
        QuestsRPG:OnPlayerGainedLevel(t)
    end
end

function EventHandler:GoldFilter(t)
    if QuestsRPG  then
        if t.gold > 0 then
            QuestsRPG:OnEarnedGold({
                ["pid"] = t.player_id_const,
                ["gold"] = t.gold,
            })
        elseif t.gold < 0 then
            QuestsRPG:OnSpentGold({
                ["pid"] = t.player_id_const,
                ["gold"] = t.gold,
            })
        end
    end
end

function EventHandler:OnQuestCompleted(t)
    if Quests then
        Quests:OnQuestCompleted(t)
    end
    if QuestsRPG then
        QuestsRPG:OnQuestCompleted(t)
    end
end

function EventHandler:OnQuestAccepted(t)

end