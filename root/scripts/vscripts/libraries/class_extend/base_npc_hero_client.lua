function C_DOTA_BaseNPC_Hero:GetAttributesValue()
    return self:GetIntellect(true) + self:GetStrength() + self:GetAgility()
end