if not CDOTA_BaseNPC_Hero.oldModifyGoldFiltered then
	CDOTA_BaseNPC_Hero.oldModifyGoldFiltered = CDOTA_BaseNPC_Hero.ModifyGoldFiltered
end

function CDOTA_BaseNPC_Hero:ModifyGoldFiltered(goldChange, reliable, reason)
    while goldChange < 0 do
        if (goldChange + 99999) < 0 then
            goldChange = goldChange + 99999
            self:oldModifyGoldFiltered(-99999, reliable, reason)
        else
            self:oldModifyGoldFiltered(goldChange, reliable, reason)
            return
        end
    end
	self:oldModifyGoldFiltered(goldChange, reliable, reason)
end

function CDOTA_BaseNPC_Hero:GetTotalGold()
	local mod = self:FindModifierByName("modifier_gold_bank")
	if mod and mod:GetStackCount() > 0 then
		totalgold = mod:GetStackCount() + self:GetGold()
	else
		totalgold = self:GetGold()
	end
	return totalgold
end

function CDOTA_BaseNPC_Hero:GetSecondaryAttributesValue()
    return self:GetIntellect(true) + self:GetStrength() + self:GetAgility() - self:GetPrimaryStatValue()
end

function CDOTA_BaseNPC_Hero:GetAttributesValue()
    return self:GetIntellect(true) + self:GetStrength() + self:GetAgility()
end
