function CDOTA_BaseNPC:IsBoss()
	if self.bossCheck == nil then
		local name = self:GetUnitName()
		-- bosses_names = {"npc_forest_boss","npc_village_boss","npc_mines_boss","npc_dust_boss","npc_swamp_boss","npc_snow_boss","npc_forest_boss_fake","npc_village_boss_fake","npc_mines_boss_fake","npc_dust_boss_fake","npc_swamp_boss_fake","npc_snow_boss_fake","boss_1","boss_2","boss_3","boss_4","boss_5","boss_6","boss_7","boss_8","boss_9","boss_10","boss_11","boss_12","boss_13","boss_14","boss_15","boss_16","boss_17","boss_18","boss_19","boss_20", "npc_boss_location8", "npc_boss_location8_fake"}
		local b = false
		for i = 1, #bosses_names do
			if name == bosses_names[i] then
				b = true
				break
			end
		end
		self.bossCheck = b
	end
	return self.bossCheck
end

function CDOTA_BaseNPC:IncreaceDamageDifficult(amp, type_increace)
	local ModifiersDifficult = {
		Easy = "modifier_easy",
		Normal = "modifier_normal",
		Hard = "modifier_hard",
		Ultra = "modifier_ultra",
		Insane = "modifier_insane",
		Impossible = "modifier_impossible",
		Unreal = "modifier_unreal",
	}
	local mod1 = self:FindModifierByName(ModifiersDifficult[diff_wave:GetGameDifficulty()])
	local v
	if mod1 and mod1.bonus_damage_perc_income then
		v = mod1:GetModifierIncomingDamage_Percentage()
		if v < 0 then
			if type_increace == "all" then
				mod1.bonus_damage_perc_income = -1 * (100 - (100 - math.abs(mod1.bonus_damage_perc_income)) * (1 + amp / 100))
				mod1.bonus_damage_perc_income_magic = -1 * (100 - (100 - math.abs(mod1.bonus_damage_perc_income_magic)) * (1 + amp / 100))
			elseif type_increace == "magic" then
				mod1.bonus_damage_perc_income_magic = -1 * (100 - (100 - math.abs(mod1.bonus_damage_perc_income_magic)) * (1 + amp / 100))
			elseif type_increace == "physical" then
				mod1.bonus_damage_perc_income = -1 * (100 - (100 - math.abs(mod1.bonus_damage_perc_income)) * (1 + amp / 100))
			end
		elseif v > 0 then
			if type_increace == "all" then
				mod1.bonus_damage_perc_income = (100 + math.abs(mod1.bonus_damage_perc_income) * (1 + amp / 100))
				mod1.bonus_damage_perc_income_magic = (100 + math.abs(mod1.bonus_damage_perc_income) * (1 + amp / 100))
			elseif type_increace == "magic" then
				mod1.bonus_damage_perc_income_magic = (100 + math.abs(mod1.bonus_damage_perc_income) * (1 + amp / 100))
			elseif type_increace == "physical" then
				mod1.bonus_damage_perc_income = (100 + math.abs(mod1.bonus_damage_perc_income) * (1 + amp / 100))
			end
		else
			if type_increace == "all" then
				mod1.bonus_damage_perc_income = amp
				mod1.bonus_damage_perc_income_magic = amp
			elseif type_increace == "magic" then
				mod1.bonus_damage_perc_income_magic = amp
			elseif type_increace == "physical" then
				mod1.bonus_damage_perc_income = amp
			end
		end
	return
	end
	local mod2 = self:FindModifierByName("modifier_unreal_lane")
	if mod2 and mod2.bonus_damage_perc_income then
		v = mod2:GetModifierIncomingDamage_Percentage()
		if v < 0 then
			if type_increace == "all" then
				mod2.bonus_damage_perc_income = -1 * (100 - (100 - math.abs(mod2.bonus_damage_perc_income)) * (1 + amp / 100))
				mod2.bonus_damage_perc_income_magic = -1 * (100 - (100 - math.abs(mod2.bonus_damage_perc_income_magic)) * (1 + amp / 100))
			elseif type_increace == "magic" then
				mod2.bonus_damage_perc_income_magic = -1 * (100 - (100 - math.abs(mod2.bonus_damage_perc_income_magic)) * (1 + amp / 100))
			elseif type_increace == "physical" then
				mod2.bonus_damage_perc_income = -1 * (100 - (100 - math.abs(mod2.bonus_damage_perc_income)) * (1 + amp / 100))
			end
		elseif v > 0 then
			if type_increace == "all" then
				mod2.bonus_damage_perc_income = (100 + math.abs(mod2.bonus_damage_perc_income) * (1 + amp / 100))
				mod2.bonus_damage_perc_income_magic = (100 + math.abs(mod2.bonus_damage_perc_income) * (1 + amp / 100))
			elseif type_increace == "magic" then
				mod2.bonus_damage_perc_income_magic = (100 + math.abs(mod2.bonus_damage_perc_income) * (1 + amp / 100))
			elseif type_increace == "physical" then
				mod2.bonus_damage_perc_income = (100 + math.abs(mod2.bonus_damage_perc_income) * (1 + amp / 100))
			end
		else
			if type_increace == "all" then
				mod2.bonus_damage_perc_income = amp
				mod2.bonus_damage_perc_income_magic = amp
			elseif type_increace == "magic" then
				mod2.bonus_damage_perc_income_magic = amp
			elseif type_increace == "physical" then
				mod2.bonus_damage_perc_income = amp
			end
		end
	end
end

function CDOTA_BaseNPC:RefreshDamageDifficult()
	local ModifiersDifficult = {
		Easy = "modifier_easy",
		Normal = "modifier_normal",
		Hard = "modifier_hard",
		Ultra = "modifier_ultra",
		Insane = "modifier_insane",
		Impossible = "modifier_impossible",
		Unreal = "modifier_unreal",
	}
	local mod = self:FindModifierByName(ModifiersDifficult[diff_wave:GetGameDifficulty()])
	if mod and mod.bonus_damage_perc_income then
		mod:OnRefresh()
	return
	end
	local mod = self:FindModifierByName("modifier_unreal_lane")
	if mod and mod.bonus_damage_perc_income then
		mod:OnRefresh()
	end
end

