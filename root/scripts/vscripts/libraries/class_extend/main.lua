if IsServer() then
    require("libraries/class_extend/base_npc")
    require("libraries/class_extend/base_npc_hero")
else
    require("libraries/class_extend/base_npc_hero_client")
end