if GameEventHandler == nil then
    GameEventHandler = class({})
end

-- обработка завершения канализации способности
function GameEventHandler:OnAbilityChannelFinishedEvent(event)
    if self.OnAbilityChannelFinished then
        self:OnAbilityChannelFinished(event)
    end
end

-- обработка изменения инвентаря героя
function GameEventHandler:OnHeroInventoryItemChangeEvent(event)
    if self.OnHeroInventoryItemChange then
        self:OnHeroInventoryItemChange(event)
    end
end

-- обработка создания иллюзий
function GameEventHandler:OnIllusionsCreatedEvent(event)
    if self.OnIllusionsCreated then
        self:OnIllusionsCreated(event)
    end
end

-- обработка объединения предметов
function GameEventHandler:OnItemCombinedEvent(event)
    if self.OnItemCombined then
        self:OnItemCombined(event)
    end
end

-- обработка поднятия предмета
function GameEventHandler:OnItemPickedUpEvent(event)
    if self.OnItemPickedUp then
        self:OnItemPickedUp(event)
    end
end

-- обработка покупки предмета
function GameEventHandler:OnItemPurchasedEvent(event)
    if self.OnItemPurchased then
        self:OnItemPurchased(event)
    end
end

-- обработка использования способности не игроком
function GameEventHandler:OnNonPlayerUsedAbilityEvent(event)
    if self.OnNonPlayerUsedAbility then
        self:OnNonPlayerUsedAbility(event)
    end
end

-- обработка достижения цели NPC
function GameEventHandler:OnNPCGoalReachedEvent(event)
    if self.OnNPCGoalReached then
        self:OnNPCGoalReached(event)
    end
end

-- обработка начала применения способности игроком
function GameEventHandler:OnPlayerBeginCastEvent(event)
    if self.OnPlayerBeginCast then
        self:OnPlayerBeginCast(event)
    end
end

-- обработка получения уровня игроком
function GameEventHandler:OnPlayerGainedLevelEvent(event)
    if self.OnPlayerGainedLevel then
        self:OnPlayerGainedLevel(event)
    end
end

-- обработка убийства игроком
function GameEventHandler:OnPlayerKillEvent(event)
    if self.OnPlayerKill then
        self:OnPlayerKill(event)
    end
end

-- обработка изучения способности игроком
function GameEventHandler:OnPlayerLearnedAbilityEvent(event)
    if self.OnPlayerLearnedAbility then
        self:OnPlayerLearnedAbility(event)
    end
end

-- обработка выбора героя игроком
function GameEventHandler:OnPlayerPickHeroEvent(event)
    if self.OnPlayerPickHero then
        self:OnPlayerPickHero(event)
    end
end

-- обработка выбора пользовательской команды игроком
function GameEventHandler:OnPlayerSelectedCustomTeamEvent(event)
    if self.OnPlayerSelectedCustomTeam then
        self:OnPlayerSelectedCustomTeam(event)
    end
end

-- обработка получения урона от башни игроком
function GameEventHandler:OnPlayerTakeTowerDamageEvent(event)
    if self.OnPlayerTakeTowerDamage then
        self:OnPlayerTakeTowerDamage(event)
    end
end

-- обработка обновления выбранного юнита игроком
function GameEventHandler:OnPlayerUpdateQueryUnitEvent(event)
    if self.OnPlayerUpdateQueryUnit then
        self:OnPlayerUpdateQueryUnit(event)
    end
end

-- обработка обновления выбранного юнита игроком
function GameEventHandler:OnPlayerUpdateSelectedUnitEvent(event)
    if self.OnPlayerUpdateSelectedUnit then
        self:OnPlayerUpdateSelectedUnit(event)
    end
end

-- обработка использования способности игроком
function GameEventHandler:OnPlayerUsedAbilityEvent(event)
    if self.OnPlayerUsedAbility then
        self:OnPlayerUsedAbility(event)
    end
end

-- обработка активации руны сервером
function GameEventHandler:OnRuneActivatedServerEvent(event)
    if self.OnRuneActivatedServer then
        self:OnRuneActivatedServer(event)
    end
end

-- обработка убийства командой
function GameEventHandler:OnTeamKillCreditEvent(event)
    if self.OnTeamKillCredit then
        self:OnTeamKillCredit(event)
    end
end

-- обработка убийства башни
function GameEventHandler:OnTowerKillEvent(event)
    if self.OnTowerKill then
        self:OnTowerKill(event)
    end
end

-- обработка повреждения сущности
function GameEventHandler:OnEntityHurtEvent(event)
    if self.OnEntityHurt then
        self:OnEntityHurt(event)
    end
end

-- обработка смерти сущности
function GameEventHandler:OnEntityKilledEvent(event)
    if self.OnEntityKilled then
        self:OnEntityKilled(event)
    end
end

-- обработка изменения состояния правил игры
function GameEventHandler:OnGameRulesStateChangeEvent(event)
    if self.OnGameRulesStateChange then
        self:OnGameRulesStateChange(event)
    end
end

-- обработка последнего удара
function GameEventHandler:OnLastHitEvent(event)
    if self.OnLastHit then
        self:OnLastHit(event)
    end
end

-- обработка спавна NPC
function GameEventHandler:OnNPCSpawnedEvent(event)
    if self.OnNPCSpawned then
        self:OnNPCSpawned(event)
    end
end

-- обработка чата игрока
function GameEventHandler:OnPlayerChatEvent(event)
    if self.OnPlayerChat then
        self:OnPlayerChat(event)
    end
end

-- обработка подключения игрока
function GameEventHandler:OnPlayerConnectEvent(event)
    if self.OnPlayerConnect then
        self:OnPlayerConnect(event)
    end
end

-- обработка полного подключения игрока
function GameEventHandler:OnPlayerConnectFullEvent(event)
    if self.OnPlayerConnectFull then
        self:OnPlayerConnectFull(event)
    end
end

-- обработка отключения игрока
function GameEventHandler:OnPlayerDisconnectEvent(event)
    if self.OnPlayerDisconnect then
        self:OnPlayerDisconnect(event)
    end
end

-- обработка повторного подключения игрока
function GameEventHandler:OnPlayerReconnectedEvent(event)
    if self.OnPlayerReconnected then
        self:OnPlayerReconnected(event)
    end
end

-- обработка сруба дерева
function GameEventHandler:OnTreeCutEvent(event)
    if self.OnTreeCut then
        self:OnTreeCut(event)
    end
end

-- регистрация обработчиков событий
function GameEventHandler:RegisterGameEventHandlers()
    -- регистрация слушателей для остальных событий
    ListenToGameEvent("dota_ability_channel_finished", Dynamic_Wrap(GameEventHandler, "OnAbilityChannelFinishedEvent"), self)
    ListenToGameEvent("dota_hero_inventory_item_change", Dynamic_Wrap(GameEventHandler, "OnHeroInventoryItemChangeEvent"), self)
    ListenToGameEvent("dota_illusions_created", Dynamic_Wrap(GameEventHandler, "OnIllusionsCreatedEvent"), self)
    ListenToGameEvent("dota_item_combined", Dynamic_Wrap(GameEventHandler, "OnItemCombinedEvent"), self)
    ListenToGameEvent("dota_item_picked_up", Dynamic_Wrap(GameEventHandler, "OnItemPickedUpEvent"), self)
    ListenToGameEvent("dota_item_purchased", Dynamic_Wrap(GameEventHandler, "OnItemPurchasedEvent"), self)
    ListenToGameEvent("dota_non_player_used_ability", Dynamic_Wrap(GameEventHandler, "OnNonPlayerUsedAbilityEvent"), self)
    ListenToGameEvent("dota_npc_goal_reached", Dynamic_Wrap(GameEventHandler, "OnNPCGoalReachedEvent"), self)
    ListenToGameEvent("dota_player_begin_cast", Dynamic_Wrap(GameEventHandler, "OnPlayerBeginCastEvent"), self)
    ListenToGameEvent("dota_player_gained_level", Dynamic_Wrap(GameEventHandler, "OnPlayerGainedLevelEvent"), self)
    ListenToGameEvent("dota_player_kill", Dynamic_Wrap(GameEventHandler, "OnPlayerKillEvent"), self)
    ListenToGameEvent("dota_player_learned_ability", Dynamic_Wrap(GameEventHandler, "OnPlayerLearnedAbilityEvent"), self)
    ListenToGameEvent("dota_player_pick_hero", Dynamic_Wrap(GameEventHandler, "OnPlayerPickHeroEvent"), self)
    ListenToGameEvent("dota_player_selected_custom_team", Dynamic_Wrap(GameEventHandler, "OnPlayerSelectedCustomTeamEvent"), self)
    ListenToGameEvent("dota_player_take_tower_damage", Dynamic_Wrap(GameEventHandler, "OnPlayerTakeTowerDamageEvent"), self)
    ListenToGameEvent("dota_player_update_query_unit", Dynamic_Wrap(GameEventHandler, "OnPlayerUpdateQueryUnitEvent"), self)
    ListenToGameEvent("dota_player_update_selected_unit", Dynamic_Wrap(GameEventHandler, "OnPlayerUpdateSelectedUnitEvent"), self)
    ListenToGameEvent("dota_player_used_ability", Dynamic_Wrap(GameEventHandler, "OnPlayerUsedAbilityEvent"), self)
    ListenToGameEvent("dota_rune_activated_server", Dynamic_Wrap(GameEventHandler, "OnRuneActivatedServerEvent"), self)
    ListenToGameEvent("dota_team_kill_credit", Dynamic_Wrap(GameEventHandler, "OnTeamKillCreditEvent"), self)
    ListenToGameEvent("dota_tower_kill", Dynamic_Wrap(GameEventHandler, "OnTowerKillEvent"), self)
    ListenToGameEvent("entity_hurt", Dynamic_Wrap(GameEventHandler, "OnEntityHurtEvent"), self)
    ListenToGameEvent("entity_killed", Dynamic_Wrap(GameEventHandler, "OnEntityKilledEvent"), self)
    ListenToGameEvent("game_rules_state_change", Dynamic_Wrap(GameEventHandler, "OnGameRulesStateChangeEvent"), self)
    ListenToGameEvent("last_hit", Dynamic_Wrap(GameEventHandler, "OnLastHitEvent"), self)
    ListenToGameEvent("npc_spawned", Dynamic_Wrap(GameEventHandler, "OnNPCSpawnedEvent"), self)
    ListenToGameEvent("player_chat", Dynamic_Wrap(GameEventHandler, "OnPlayerChatEvent"), self)
    ListenToGameEvent("player_connect", Dynamic_Wrap(GameEventHandler, "OnPlayerConnectEvent"), self)
    ListenToGameEvent("player_connect_full", Dynamic_Wrap(GameEventHandler, "OnPlayerConnectFullEvent"), self)
    ListenToGameEvent("player_disconnect", Dynamic_Wrap(GameEventHandler, "OnPlayerDisconnectEvent"), self)
    ListenToGameEvent("player_reconnected", Dynamic_Wrap(GameEventHandler, "OnPlayerReconnectedEvent"), self)
    ListenToGameEvent("tree_cut", Dynamic_Wrap(GameEventHandler, "OnTreeCutEvent"), self)
end

function GameEventHandler:RegisterHudListener(event_name, function_name)
    CustomGameEventManager:RegisterListener(event_name, function(_, kv)
        self[function_name](self, kv)
    end)
end

function GameEventHandler:SendHudError(pid, message)
    CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer(pid), "mountain_dota_hud_show_hud_error", { 
        ["message"] = "#dota_hud_error_" .. message, 
    })
    return false
end