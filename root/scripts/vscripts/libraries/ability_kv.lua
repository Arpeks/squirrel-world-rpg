local special_keys = {
	var_type = false,
	LinkedSpecialBonus = true,
	LinkedSpecialBonusField = true,
	LinkedSpecialBonusOperation = true,
	DamageTypeTooltip = true,
	CalculateSpellDamageTooltip = true,
	RequiresScepter = true,
	RequiresShard = true,
	levelkey = true,
	ad_linked_abilities = true,
}

local networked_values = {
	AbilityUnitDamageType = true,
	AbilityValues = true,
	AbilityDraftUltScepterAbility = true,
	AbilityDraftUltShardAbility = true,
	MaxLevel = true,
	HasShardUpgrade = true,
	HasScepterUpgrade = true,
	AbilityDamageCategory = true,
	AbilityDamageFlags = true,
	IsPassive = true, -- custom key
}

local convert_to_array = {
	value = true,
	special_bonus_shard = true,
	special_bonus_scepter = true,
}

local merge_into_ability_values = {
	AbilityDuration = true,
	AbilityCooldown = true,
	AbilityManaCost = true,
	AbilityCastRange = true,
	AbilityChannelTime = true,
	AbilityCastPoint = true,
	AbilityCharges = true,
	AbilityChargeRestoreTime = true,
}

Abilities_KV = Abilities_KV or {}

local abilities_items_kv = {}
function Abilities_KV:Init()
	self.ability_list_kv = LoadKeyValues("scripts/npc/npc_abilities_list.txt")
	self.linked_abilities = {}
	self.heroes_abilities = {}

	local ability_list = Abilities_KV:MakeAbilityList()
	Abilities_KV:InitHeroesAbilities()

	for ability, _ in pairs(ability_list) do
		local kv = GetAbilityKeyValuesByName(ability)
		if kv then
			local formatted_kv = Abilities_KV:FormatKV(kv) or {}
			if self.linked_abilities[ability] then
				formatted_kv.linked_abilities = self.linked_abilities[ability]
			end
			abilities_items_kv[ability] = formatted_kv
		end
	end

	local item_kvs = table.deepmerge(LoadKeyValues("scripts/npc/items.txt"), LoadKeyValues("scripts/npc/npc_items_custom.txt"))

	for item_name, _ in pairs(item_kvs) do
		local kv = GetAbilityKeyValuesByName(item_name)
		if kv then
			local formatted_kv = Abilities_KV:FormatKV(kv)

			if formatted_kv then
				abilities_items_kv[item_name] = formatted_kv
			end
		end
	end


	abilities_items_kv = json.encode(abilities_items_kv)
	--print("KV len:", abilities_items_kv:len())
	--local sys_time = GetSystemTimeMS()
	abilities_items_kv = LibDeflate:CompressDeflate(abilities_items_kv, { level = 5 })
	--print((GetSystemTimeMS() - sys_time), "ms")
	--print("Compressed KV len:", abilities_items_kv:len())
	abilities_items_kv = LibDeflate:EncodeForPrint(abilities_items_kv)
	--print("Encoded KV len:", abilities_items_kv:len())

	EventStream:Listen("get_abilities_items_kv", Abilities_KV.SendAbilitiesItemsKV, Abilities_KV)
end

function Abilities_KV:InitHeroesAbilities()
	for hero_name, abilities_info in pairs(self.ability_list_kv) do
		hero_name = "npc_dota_hero_" .. hero_name

		self.heroes_abilities[hero_name] = {}
		for key, value in pairs(abilities_info) do
			if value ~= "table" then
				self.heroes_abilities[hero_name][key] = value
			end
		end
	end
end

function Abilities_KV:SendAbilitiesItemsKV(event)
	local player_id = event.PlayerID
	if not player_id or not PlayerResource:IsValidPlayer(player_id) then return end

	local player = PlayerResource:GetPlayer(player_id)
	if not player or player:IsNull() or (player.kv_sended and not IsInToolsMode()) then return end

	CustomGameEventManager:Send_ServerToPlayer(player, "set_abilities_items_kv", abilities_items_kv, EVENT_COMPRESSION.ALREADY_COMPRESSED)
	player.kv_sended = true
end

function Abilities_KV:ConvertAbilitySpecialToValues(specials)
	local values = {}

	for _, special in pairs(specials) do
		local value = {}
		local value_name
		local only_value = true

		for k,v in pairs(special) do
			if special_keys[k] ~= false then
				if special_keys[k] == true then
					value[k] = v
					only_value = false
				else
					value_name = k
					value["value"] = v
				end
			end
		end

		if only_value then
			value = value.value
		end

		values[value_name] = value
	end

	return values
end

local function shrink_array(arr)
	local last_value = arr[#arr]

	for i = #arr - 1, 1, -1 do
		if last_value == arr[i] then 
			arr[i + 1] = nil 
		end
	end

	if #arr == 1 and arr[1] == 0 then return nil end

	return arr
end

function Abilities_KV:ValueToArray(value)
	if not value or value == "" then return nil end

	if type(value) == "number" then
		return { value }
	end

	local arr = string.split(value)

	if #arr == 0 then return nil end

	for k,v in ipairs(arr) do
		arr[k] = tonumber(v) or v
	end

	arr = shrink_array(arr)

	return arr
end

function Abilities_KV:MakeAbilityList()
	local ability_list = {}

	for hero, list in pairs(self.ability_list_kv) do
		for k,v in pairs(list) do
			if type(v) ~= "table" then
				ability_list[v] = true
			else
				self.linked_abilities[k] = {}
				for ability_name, _ in pairs(v) do
					table.insert(self.linked_abilities[k], ability_name);
					ability_list[ability_name] = true
				end
			end
		end
	end

	local shard_scepter_ability_list = {}

	for ability_name, _ in pairs(ability_list) do
		local shard_ability = GetAbilityKV(ability_name, "AbilityDraftUltShardAbility")
		if shard_ability and shard_ability ~= "" then
			shard_scepter_ability_list[shard_ability] = true
		end

		local scepter_ability = GetAbilityKV(ability_name, "AbilityDraftUltScepterAbility")
		if scepter_ability and scepter_ability ~= "" then
			shard_scepter_ability_list[scepter_ability] = true
		end
	end

	table.merge(ability_list, shard_scepter_ability_list)
	return ability_list
end

local function contains_key(t, key)
	if type(t) ~="table" then return false end

	for k,v in pairs(t) do
		if type(k) == "string" and k:lower():find(key) then return true end

		if type(v) == "table" then
			local is_v_contain_key = contains_key(v, key)
			if is_v_contain_key then return true end
		end
	end

	return false
end

function Abilities_KV:FormatKV(kv)
	if kv.AbilitySpecial then
		local converted_values = Abilities_KV:ConvertAbilitySpecialToValues(kv.AbilitySpecial)

		if kv.AbilityValues then
			table.deepmerge(converted_values, kv.AbilityValues)
		end

		kv.AbilityValues = converted_values
		kv.AbilitySpecial = nil
	end

	if kv.AbilityValues then
		for value_name, _ in pairs(merge_into_ability_values) do
			if kv[value_name] and not kv.AbilityValues[value_name] then
				kv.AbilityValues[value_name] = kv[value_name]
			end
		end

		local lowercase_ability_values = {}
		for k,v in pairs(kv.AbilityValues) do
			lowercase_ability_values[type(k) == "string" and k:lower() or k] = v
		end
		kv.AbilityValues = lowercase_ability_values

		-- Send AbilityValues only if ability have shard or scepter
		kv.AbilityValues = table.filter(kv.AbilityValues, function(v,k) 
			return kv.HasScepterUpgrade == 1 or kv.HasShardUpgrade == 1
				or k:lower():find("scepter") or k:lower():find("shard") 
				or contains_key(v, "shard") or contains_key(v, "scepter")
		end)

		for k,v in pairs(kv.AbilityValues) do
			if type(v) == "table" then
				for field_name, field_value in pairs(v) do
					if convert_to_array[field_name] and type(field_value) == "string" then
						v[field_name] = Abilities_KV:ValueToArray(field_value)
					end
				end
			elseif type(v) == "string" then
				kv.AbilityValues[k] = Abilities_KV:ValueToArray(v)
			end
		end
	end

	if kv.AbilityBehavior and string.find(kv.AbilityBehavior, "DOTA_ABILITY_BEHAVIOR_PASSIVE") then
		kv.IsPassive = true
	end

	kv = table.filter(kv, function(v,k)
		if networked_values[k] then
			if v == "" or v == 0 then return false 
			elseif type(v) == "table" and next(v) == nil then return false
			else return true end
		end

		return false
	end)

	return next(kv) and kv or nil
end

Abilities_KV:Init()
