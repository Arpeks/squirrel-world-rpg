item_vindicators_axe_rda = class({})

function item_vindicators_axe_rda:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_item_vindicators_axe_rda", {duration = self:GetSpecialValueFor("duration")})
end

LinkLuaModifier("modifier_item_vindicators_axe_rda", "items/custom_neutral_items/item_vindicators_axe_rda", LUA_MODIFIER_MOTION_NONE)

modifier_item_vindicators_axe_rda = class({})

function modifier_item_vindicators_axe_rda:IsHidden()
    return true
end

function modifier_item_vindicators_axe_rda:OnCreated()
    self.damage_prc = self:GetAbility():GetSpecialValueFor("damage_prc")
end

function modifier_item_vindicators_axe_rda:CustomOnAttackLanded(keys)
	if keys.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,400,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (self.damage_prc/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )

			end
		end
	end
end

function modifier_item_vindicators_axe_rda:PlayEffects1(direction )
	local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
	ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end