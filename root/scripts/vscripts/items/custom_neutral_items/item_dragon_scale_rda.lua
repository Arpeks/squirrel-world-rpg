LinkLuaModifier( "modifier_item_dragon_scale_rda", "items/custom_neutral_items/item_dragon_scale_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_dragon_scale_rda == nil then
	item_dragon_scale_rda = class({})
end
function item_dragon_scale_rda:GetIntrinsicModifierName()
	return "modifier_item_dragon_scale_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_dragon_scale_rda == nil then
	modifier_item_dragon_scale_rda = class({})
end
function modifier_item_dragon_scale_rda:IsHidden()
	return true
end
function modifier_item_dragon_scale_rda:OnCreated(params)
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_magic_resistance = self:GetAbility():GetSpecialValueFor("bonus_magic_resistance")

	if IsServer() then
	end
end
function modifier_item_dragon_scale_rda:OnRefresh(params)
	self.bonus_armor = self:GetAbility():GetSpecialValueFor("bonus_armor")
	self.bonus_magic_resistance = self:GetAbility():GetSpecialValueFor("bonus_magic_resistance")
	if IsServer() then
	end
end
function modifier_item_dragon_scale_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_dragon_scale_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS
	}
end

function modifier_item_dragon_scale_rda:GetModifierPhysicalArmorBonus()
	return self.bonus_armor
end
function modifier_item_dragon_scale_rda:GetModifierMagicalResistanceBonus()
	return self.bonus_magic_resistance
end