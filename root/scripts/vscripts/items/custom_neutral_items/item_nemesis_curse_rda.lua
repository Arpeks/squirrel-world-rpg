LinkLuaModifier( "modifier_item_nemesis_curse_rda", "items/custom_neutral_items/item_nemesis_curse_rda.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_nemesis_curse_rda_debuff", "items/custom_neutral_items/item_nemesis_curse_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_nemesis_curse_rda == nil then
	item_nemesis_curse_rda = class({})
end
function item_nemesis_curse_rda:Precache(context)
	PrecacheResource("particle", "particles/items3_fx/nemesis_curse_debuff.vpcf", context)
end
function item_nemesis_curse_rda:GetIntrinsicModifierName()
	return "modifier_item_nemesis_curse_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_nemesis_curse_rda == nil then
	modifier_item_nemesis_curse_rda = class({})
end
function modifier_item_nemesis_curse_rda:IsHidden()
	return true
end
function modifier_item_nemesis_curse_rda:OnCreated(params)
	self.debuff_self = self:GetAbility():GetSpecialValueFor("debuff_self")
	self.debuff_enemy_duration = self:GetAbility():GetSpecialValueFor("debuff_enemy_duration")
end
function modifier_item_nemesis_curse_rda:OnRefresh(params)
	self.debuff_self = self:GetAbility():GetSpecialValueFor("debuff_self")
	self.debuff_enemy_duration = self:GetAbility():GetSpecialValueFor("debuff_enemy_duration")
end
function modifier_item_nemesis_curse_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_nemesis_curse_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
	}
end

function modifier_item_nemesis_curse_rda:GetModifierIncomingDamage_Percentage()
	return self.debuff_self
end

function modifier_item_nemesis_curse_rda:CustomOnAttackLanded(keys)
	if not IsServer() then return end
	local target = keys.target
	local attacker = keys.attacker
    if self:GetParent() == attacker then
		if self.last_target and not self.last_target:IsNull() and self.last_target:IsAlive() then
			self.last_target:RemoveModifierByName("modifier_item_nemesis_curse_rda_debuff")
		end
		self.last_target = target
		target:AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_nemesis_curse_rda_debuff", {duration = self.debuff_enemy_duration})
	end
end

modifier_item_nemesis_curse_rda_debuff = class({})

function modifier_item_nemesis_curse_rda_debuff:OnCreated()
	self.debuff_enemy = self:GetAbility():GetSpecialValueFor("debuff_enemy")
	self:GetParent():IncreaceDamageDifficult(self.debuff_enemy, "all")
	self.effect_cast = ParticleManager:CreateParticleForPlayer( "particles/items3_fx/nemesis_curse_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent(), self:GetCaster() )
end

function modifier_item_nemesis_curse_rda_debuff:OnDestroy()
	if not IsServer() then
		return
	end
	ParticleManager:DestroyParticle( self.effect_cast, false )
	ParticleManager:ReleaseParticleIndex( self.effect_cast )
	self:GetParent():RefreshDamageDifficult()
end