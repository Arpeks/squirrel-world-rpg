LinkLuaModifier( "modifier_item_pirate_hat_rda", "items/custom_neutral_items/item_pirate_hat_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_pirate_hat_rda == nil then
	item_pirate_hat_rda = class({})
end
function item_pirate_hat_rda:GetIntrinsicModifierName()
	return "modifier_item_pirate_hat_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_pirate_hat_rda == nil then
	modifier_item_pirate_hat_rda = class({})
end
function modifier_item_pirate_hat_rda:IsHidden()
	return true
end
function modifier_item_pirate_hat_rda:OnCreated(params)
	self.gold_per_sec = self:GetAbility():GetSpecialValueFor("gold_per_sec")
	if IsServer() then
	end
    self:StartIntervalThink(1)
end
function modifier_item_pirate_hat_rda:OnRefresh(params)
	self.gold_per_sec = self:GetAbility():GetSpecialValueFor("gold_per_sec")
end
function modifier_item_pirate_hat_rda:OnIntervalThink()
    self:GetParent():ModifyGoldFiltered( self.gold_per_sec, true, DOTA_ModifyGold_Unspecified )
end