item_enhancement_mystical_rda = class({})

function item_enhancement_mystical_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_mystical_rda"
end

LinkLuaModifier("modifier_item_enhancement_mystical_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_mystical_rda = class({})

function modifier_item_enhancement_mystical_rda:IsHidden()
    return true
end

function modifier_item_enhancement_mystical_rda:OnCreated()
    self.mana_regen = self:GetAbility():GetSpecialValueFor("mana_regen")
    self.magic_res = self:GetAbility():GetSpecialValueFor("magic_res")
end

function modifier_item_enhancement_mystical_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
        MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS
    }    
end

function modifier_item_enhancement_mystical_rda:GetModifierConstantManaRegen()
    return self.mana_regen
end

function modifier_item_enhancement_mystical_rda:GetModifierMagicalResistanceBonus()
    return self.magic_res
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_brawny_rda = class({})

function item_enhancement_brawny_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_brawny_rda"
end

LinkLuaModifier("modifier_item_enhancement_brawny_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_brawny_rda = class({})

function modifier_item_enhancement_brawny_rda:IsHidden()
    return true
end

function modifier_item_enhancement_brawny_rda:OnCreated()
    self.health_bonus = self:GetAbility():GetSpecialValueFor("health_bonus")
    self.armor = self:GetAbility():GetSpecialValueFor("armor")
end

function modifier_item_enhancement_brawny_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_HEALTH_BONUS,
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
    }    
end

function modifier_item_enhancement_brawny_rda:GetModifierHealthBonus()
    return self.health_bonus
end

function modifier_item_enhancement_brawny_rda:GetModifierPhysicalArmorBonus()
    return self.armor
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_alert_rda = class({})

function item_enhancement_alert_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_alert_rda"
end

LinkLuaModifier("modifier_item_enhancement_alert_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_alert_rda = class({})

function modifier_item_enhancement_alert_rda:IsHidden()
    return true
end

function modifier_item_enhancement_alert_rda:OnCreated()
    self.bonus_attack_speed = self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
    self.movement_speed = self:GetAbility():GetSpecialValueFor("movement_speed")
end

function modifier_item_enhancement_alert_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE
    }   
end

function modifier_item_enhancement_alert_rda:GetModifierAttackSpeedBonus_Constant()
    return self.bonus_attack_speed
end

function modifier_item_enhancement_alert_rda:GetModifierMoveSpeedBonus_Percentage()
    return self.movement_speed
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_quickened_rda = class({})

function item_enhancement_quickened_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_quickened_rda"
end

LinkLuaModifier("modifier_item_enhancement_quickened_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_quickened_rda = class({})

function modifier_item_enhancement_quickened_rda:IsHidden()
    return true
end

function modifier_item_enhancement_quickened_rda:OnCreated()
    self.spell_amp = self:GetAbility():GetSpecialValueFor("spell_amp")
    self.manacost_reduction = self:GetAbility():GetSpecialValueFor("manacost_reduction")
end

function modifier_item_enhancement_quickened_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
        MODIFIER_PROPERTY_MANACOST_PERCENTAGE
    }   
end

function modifier_item_enhancement_quickened_rda:GetModifierSpellAmplify_Percentage()
    return self.spell_amp
end

function modifier_item_enhancement_quickened_rda:GetModifierPercentageManacost()
    return self.manacost_reduction
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_tough_rda = class({})

function item_enhancement_tough_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_tough_rda"
end

LinkLuaModifier("modifier_item_enhancement_tough_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_tough_rda = class({})

function modifier_item_enhancement_tough_rda:IsHidden()
    return true
end

function modifier_item_enhancement_tough_rda:OnCreated()
    self.all_stats = self:GetAbility():GetSpecialValueFor("all_stats")
end

function modifier_item_enhancement_tough_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
    }   
end

function modifier_item_enhancement_tough_rda:GetModifierBonusStats_Strength()
    return self.all_stats
end

function modifier_item_enhancement_tough_rda:GetModifierBonusStats_Agility()
    return self.all_stats
end

function modifier_item_enhancement_tough_rda:GetModifierBonusStats_Intellect()
    return self.all_stats
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_timeless_rda = class({})

function item_enhancement_timeless_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_timeless_rda"
end

LinkLuaModifier("modifier_item_enhancement_timeless_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_timeless_rda = class({})

function modifier_item_enhancement_timeless_rda:IsHidden()
    return true
end

function modifier_item_enhancement_timeless_rda:OnCreated()
    self.spell_amp = self:GetAbility():GetSpecialValueFor("spell_amp")
end

function modifier_item_enhancement_timeless_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE
    }   
end

function modifier_item_enhancement_timeless_rda:GetModifierSpellAmplify_Percentage()
    return self:GetCaster():GetIntellect(true) / 100 * self.spell_amp
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_evolved_rda = class({})

function item_enhancement_evolved_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_evolved_rda"
end

LinkLuaModifier("modifier_item_enhancement_evolved_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_evolved_rda = class({})

function modifier_item_enhancement_evolved_rda:IsHidden()
    return true
end

function modifier_item_enhancement_evolved_rda:OnCreated()
    self.primary_stat = self:GetAbility():GetSpecialValueFor("primary_stat")
    self.primary_stat_universal = self:GetAbility():GetSpecialValueFor("primary_stat_universal")
    if not IsServer() then
        return
    end
end


function modifier_item_enhancement_evolved_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
    }   
end

function modifier_item_enhancement_evolved_rda:GetModifierBonusStats_Strength()
    if self.stop_str then
        return 0
    end
    if IsServer() then
        local primary_attribute = self:GetParent():GetPrimaryAttribute()
        local bonus = 0
        if primary_attribute == DOTA_ATTRIBUTE_STRENGTH then
            self.stop_str = true
            bonus = self:GetCaster():GetStrength() / 100 * self.primary_stat
            self.stop_str = false
        elseif primary_attribute == DOTA_ATTRIBUTE_ALL then
            self.stop_str = true
            bonus = self:GetCaster():GetStrength() / 100 * self.primary_stat_universal
            self.stop_str = false
        end
        return bonus
    end
    return 0
end

function modifier_item_enhancement_evolved_rda:GetModifierBonusStats_Agility()
    if self.stop_agi then
        return 0
    end
    if IsServer() then
        local primary_attribute = self:GetParent():GetPrimaryAttribute()
        local bonus = 0
        if primary_attribute == DOTA_ATTRIBUTE_AGILITY then
            self.stop_agi = true
            bonus = self:GetCaster():GetAgility() / 100 * self.primary_stat
            self.stop_agi = false
        elseif primary_attribute == DOTA_ATTRIBUTE_ALL then
            self.stop_agi = true
            bonus = self:GetCaster():GetAgility() / 100 * self.primary_stat_universal
            self.stop_agi = false
        end
        return bonus
    end
    return 0
end

function modifier_item_enhancement_evolved_rda:GetModifierBonusStats_Intellect()
    if self.stop_int then
        return 0
    end
    if IsServer() then
        local primary_attribute = self:GetParent():GetPrimaryAttribute()
        local bonus = 0
        if primary_attribute == DOTA_ATTRIBUTE_INTELLECT then
            self.stop_int = true
            bonus = self:GetCaster():GetIntellect(true) / 100 * self.primary_stat
            self.stop_int = false
        elseif primary_attribute == DOTA_ATTRIBUTE_ALL then
            self.stop_int = true
            bonus = self:GetCaster():GetIntellect(true) / 100 * self.primary_stat_universal
            self.stop_int = false
        end
        return bonus
    end
    return 0
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

item_enhancement_boundless_rda = class({})

function item_enhancement_boundless_rda:GetIntrinsicModifierName()
    return "modifier_item_enhancement_boundless_rda"
end

LinkLuaModifier("modifier_item_enhancement_boundless_rda", "items/custom_neutral_items/item_enhancement_lua", LUA_MODIFIER_MOTION_NONE)

modifier_item_enhancement_boundless_rda = class({})

function modifier_item_enhancement_boundless_rda:IsHidden()
    return true
end

function modifier_item_enhancement_boundless_rda:OnCreated()
    self.phys_damage = self:GetAbility():GetSpecialValueFor("phys_damage")
end

function modifier_item_enhancement_boundless_rda:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE
    }   
end

function modifier_item_enhancement_boundless_rda:GetModifierSpellAmplify_Percentage()
    return self:GetCaster():GetIntellect(true) / 100 * self.spell_amp
end

function modifier_item_enhancement_boundless_rda:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_PHYSICAL then
		return self.phys_damage
	end
end