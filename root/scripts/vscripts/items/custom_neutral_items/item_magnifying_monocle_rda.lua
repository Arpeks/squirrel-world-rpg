LinkLuaModifier( "modifier_item_magnifying_monocle_rda", "items/custom_neutral_items/item_magnifying_monocle_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_magnifying_monocle_rda == nil then
	item_magnifying_monocle_rda = class({})
end
function item_magnifying_monocle_rda:GetIntrinsicModifierName()
	return "modifier_item_magnifying_monocle_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_magnifying_monocle_rda == nil then
	modifier_item_magnifying_monocle_rda = class({})
end
function modifier_item_magnifying_monocle_rda:IsHidden()
	return true
end
function modifier_item_magnifying_monocle_rda:OnCreated(params)
	self.bonus_cast_range = self:GetAbility():GetSpecialValueFor("bonus_cast_range")
	self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range")
	self.cooldown = self:GetAbility():GetSpecialValueFor("cooldown")
	if IsServer() then
	end
end
function modifier_item_magnifying_monocle_rda:OnRefresh(params)
	self.bonus_cast_range = self:GetAbility():GetSpecialValueFor("bonus_cast_range")
	self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range")
	self.cooldown = self:GetAbility():GetSpecialValueFor("cooldown")
	if IsServer() then
	end
end
function modifier_item_magnifying_monocle_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_magnifying_monocle_rda:DeclareFunctions()
	return {
        MODIFIER_PROPERTY_CAST_RANGE_BONUS,
        MODIFIER_PROPERTY_ATTACK_RANGE_BONUS
	}
end

function modifier_item_magnifying_monocle_rda:GetModifierCastRangeBonus()
    if self:GetAbility():IsFullyCastable() then
        return self.bonus_cast_range
    end
    return 0
end
function modifier_item_magnifying_monocle_rda:GetModifierAttackRangeBonus()
    if self:GetAbility():IsFullyCastable() then
        return self.bonus_attack_range
    end
    return 0
end
function modifier_item_magnifying_monocle_rda:CustomOnTakeDamage( params )
    if not IsServer() then return end
	if params.unit~=self.parent then return end
    if params.attacker:IsBoss() then 
        self:GetAbility():StartCooldown(self.cooldown)
    end
end