LinkLuaModifier( "modifier_item_the_leveller_rda", "items/custom_neutral_items/item_the_leveller_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_the_leveller_rda == nil then
	item_the_leveller_rda = class({})
end
function item_the_leveller_rda:GetIntrinsicModifierName()
	return "modifier_item_the_leveller_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_the_leveller_rda == nil then
	modifier_item_the_leveller_rda = class({})
end

function modifier_item_the_leveller_rda:IsHidden()
	return true
end

function modifier_item_the_leveller_rda:OnCreated(params)
	self.bonus_as = self:GetAbility():GetSpecialValueFor("bonus_as")
	if IsServer() then
	end
end
function modifier_item_the_leveller_rda:OnRefresh(params)
	self.bonus_as = self:GetAbility():GetSpecialValueFor("bonus_as")
	if IsServer() then
	end
end
function modifier_item_the_leveller_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_the_leveller_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_item_the_leveller_rda:GetModifierAttackSpeedBonus_Constant()
	return self.bonus_as
end