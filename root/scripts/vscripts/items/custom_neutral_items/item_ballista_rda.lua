LinkLuaModifier( "modifier_item_ballista_rda", "items/custom_neutral_items/item_ballista_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_ballista_rda == nil then
	item_ballista_rda = class({})
end
function item_ballista_rda:GetIntrinsicModifierName()
	return "modifier_item_ballista_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_ballista_rda == nil then
	modifier_item_ballista_rda = class({})
end
function modifier_item_ballista_rda:IsHidden()
	return true
end
function modifier_item_ballista_rda:OnCreated(params)
	self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range")

	if IsServer() then
	end
end
function modifier_item_ballista_rda:OnRefresh(params)
	self.bonus_attack_range = self:GetAbility():GetSpecialValueFor("bonus_attack_range")
	if IsServer() then
	end
end
function modifier_item_ballista_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_ballista_rda:DeclareFunctions()
	return {
        MODIFIER_PROPERTY_ATTACK_RANGE_BONUS
    }
end

function modifier_item_ballista_rda:GetModifierAttackRangeBonus()
    if self:GetCaster():IsRangedAttacker() then
        return self.bonus_attack_range
    end
    return 0
end