LinkLuaModifier( "modifier_item_imp_claw_rda", "items/custom_neutral_items/item_imp_claw_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_imp_claw_rda == nil then
	item_imp_claw_rda = class({})
end
function item_imp_claw_rda:GetIntrinsicModifierName()
	return "modifier_item_imp_claw_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_imp_claw_rda == nil then
	modifier_item_imp_claw_rda = class({})
end
function modifier_item_imp_claw_rda:IsHidden()
	return true
end
function modifier_item_imp_claw_rda:OnCreated(params)
	self.crit_damage = self:GetAbility():GetSpecialValueFor("crit_damage")

	if IsServer() then
	end
end
function modifier_item_imp_claw_rda:OnRefresh(params)
	self.crit_damage = self:GetAbility():GetSpecialValueFor("crit_damage")
	if IsServer() then
	end
end
function modifier_item_imp_claw_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_imp_claw_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
	}
end

function modifier_item_imp_claw_rda:GetModifierPreAttack_CriticalStrike( keys )
    if (keys.target and not keys.target:IsOther() and not keys.target:IsBuilding() and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber()) and self:GetAbility():IsFullyCastable() then
        if IsServer() then
            self:GetAbility():UseResources(true, true, true, true)
        end
        return self.crit_damage
    end
end