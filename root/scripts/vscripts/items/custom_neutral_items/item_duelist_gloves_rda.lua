LinkLuaModifier( "modifier_item_duelist_gloves_rda", "items/custom_neutral_items/item_duelist_gloves_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_duelist_gloves_rda == nil then
	item_duelist_gloves_rda = class({})
end
function item_duelist_gloves_rda:GetIntrinsicModifierName()
	return "modifier_item_duelist_gloves_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_duelist_gloves_rda == nil then
	modifier_item_duelist_gloves_rda = class({})
end
function modifier_item_duelist_gloves_rda:IsHidden()
	return true
end
function modifier_item_duelist_gloves_rda:OnCreated(params)
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	if IsServer() then
	end
end
function modifier_item_duelist_gloves_rda:OnRefresh(params)
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	if IsServer() then
	end
end
function modifier_item_duelist_gloves_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_duelist_gloves_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE
	}
end

function modifier_item_duelist_gloves_rda:GetModifierPreAttack_BonusDamage()
	return self:GetCaster():GetDisplayAttackSpeed() * self.bonus_damage
end