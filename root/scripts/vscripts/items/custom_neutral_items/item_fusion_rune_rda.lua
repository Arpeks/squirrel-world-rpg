LinkLuaModifier( "modifier_item_armor_aura", "items/other/book_armor", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_attack_speed_aura", "items/other/book_attack_speed", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_base_damage_aura", "items/other/book_base_damage", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_expiriance_aura", "items/other/book_exp", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_book_gold", "items/other/book_gold", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_hp_aura", "items/other/book_hp", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_book_mage_damage", "items/other/book_mage_damage", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_move_aura", "items/other/book_move", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_book_phys_damage", "items/other/book_phys_damage", LUA_MODIFIER_MOTION_NONE )

--Abilities
if item_fusion_rune_rda == nil then
	item_fusion_rune_rda = class({})
end

function item_fusion_rune_rda:OnSpellStart()
	local modifiers = {
		"modifier_item_armor_aura", 
		"modifier_item_attack_speed_aura", 
		"modifier_item_base_damage_aura", 
		"modifier_item_expiriance_aura", 
		"modifier_book_gold", 
		"modifier_item_hp_aura", 
		"modifier_book_mage_damage", 
		"modifier_item_move_aura", 
		"modifier_book_phys_damage",
	}

	for _,modifier in pairs(modifiers) do
		self:GetCaster():AddNewModifier(self:GetCaster(), self, modifier, {duration = self:GetSpecialValueFor("duration")})
	end

	self:GetCaster():EmitSound("Item.TomeOfKnowledge")
end