LinkLuaModifier( "modifier_item_essence_ring_rda", "items/custom_neutral_items/item_essence_ring_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_essence_ring_rda == nil then
	item_essence_ring_rda = class({})
end
function item_essence_ring_rda:GetIntrinsicModifierName()
	return "modifier_item_essence_ring_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_essence_ring_rda == nil then
	modifier_item_essence_ring_rda = class({})
end
function modifier_item_essence_ring_rda:IsHidden()
	return true
end
function modifier_item_essence_ring_rda:OnCreated(params)
	self.bonus_health = self:GetAbility():GetSpecialValueFor("bonus_health")
	if IsServer() then
	end
end
function modifier_item_essence_ring_rda:OnRefresh(params)
	self.bonus_health = self:GetAbility():GetSpecialValueFor("bonus_health")
	if IsServer() then
	end
end
function modifier_item_essence_ring_rda:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_essence_ring_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_HEALTH_BONUS
	}
end

function modifier_item_essence_ring_rda:GetModifierHealthBonus()
	return self.bonus_health
end