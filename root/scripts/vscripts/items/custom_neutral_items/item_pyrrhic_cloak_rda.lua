LinkLuaModifier( "modifier_item_pyrrhic_cloak_rda", "items/custom_neutral_items/item_pyrrhic_cloak_rda.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_pyrrhic_cloak_rda == nil then
	item_pyrrhic_cloak_rda = class({})
end
function item_pyrrhic_cloak_rda:GetIntrinsicModifierName()
	return "modifier_item_pyrrhic_cloak_rda"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_pyrrhic_cloak_rda == nil then
	modifier_item_pyrrhic_cloak_rda = class({})
end
function modifier_item_pyrrhic_cloak_rda:IsHidden()
	return true
end
function modifier_item_pyrrhic_cloak_rda:OnCreated(params)
	self.incoming_damage = self:GetAbility():GetSpecialValueFor("incoming_damage")
end
function modifier_item_pyrrhic_cloak_rda:OnRefresh(params)
	self.incoming_damage = self:GetAbility():GetSpecialValueFor("incoming_damage")
end
function modifier_item_pyrrhic_cloak_rda:OnDestroy()
	if IsServer() then
	end
end

function modifier_item_pyrrhic_cloak_rda:CheckState()
	return {
		[MODIFIER_STATE_DEBUFF_IMMUNE] = true
	}
end

function modifier_item_pyrrhic_cloak_rda:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
	}
end

function modifier_item_pyrrhic_cloak_rda:GetModifierIncomingDamage_Percentage()
    return self.incoming_damage
end