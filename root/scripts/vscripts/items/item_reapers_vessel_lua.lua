LinkLuaModifier( "modifier_item_reapers_vessel_lua", "items/item_reapers_vessel_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_reapers_vessel_buff", "items/item_reapers_vessel_lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_reapers_vessel_lua == nil then
	item_reapers_vessel_lua = class({})
end
function item_reapers_vessel_lua:Spawn()
	local bonus_damage_max = self:GetSpecialValueFor("bonus_damage_max")
	local bonus_agility_max = self:GetSpecialValueFor("bonus_agility_max")
	local bonus_intellect_max = self:GetSpecialValueFor("bonus_intellect_max")
	local bonus_strength_max = self:GetSpecialValueFor("bonus_strength_max")
	local rand = {}
	if IsServer() then
		rand[1] = RandomFloat(1, 97)
		rand[2] = RandomFloat(1, 98 - rand[1])
		rand[3] = RandomFloat(1, 99 - rand[1] - rand[2])
		rand[4] = 100 - rand[1] - rand[2] - rand[3]
		rand = table.shuffle(rand)
		local function roundToNearest5(num)
			return math.floor((num + 2.5) / 5) * 5
		end
		self.bonus_damage = roundToNearest5(bonus_damage_max * rand[1] / 100)
		self.bonus_agility = roundToNearest5(bonus_agility_max * rand[2] / 100)
		self.bonus_intellect = roundToNearest5(bonus_intellect_max * rand[3] / 100)
		self.bonus_strength = roundToNearest5(bonus_strength_max * rand[4] / 100)
	end
end
function item_reapers_vessel_lua:GetIntrinsicModifierName()
	return "modifier_item_reapers_vessel_lua"
end
function item_reapers_vessel_lua:OnKill()
    if not self.kills then self.kills = 0 end
    self.kills = self.kills + 1
    self:SetCurrentCharges(self.kills)
	if IsServer() and self:GetCurrentCharges() >= self:GetSpecialValueFor("target_kill") then
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_item_reapers_vessel_buff", {
			bonus_damage = self.bonus_damage,
			bonus_agility = self.bonus_agility,
			bonus_intellect = self.bonus_intellect,
			bonus_strength = self.bonus_strength
		})
		UTIL_Remove(self)
	end
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_reapers_vessel_lua == nil then
	modifier_item_reapers_vessel_lua = class({})
end

function modifier_item_reapers_vessel_lua:OnCreated()
	if not IsServer() then
		return
	end
	self.bonus_damage = self:GetAbility().bonus_damage
	self.bonus_agility = self:GetAbility().bonus_agility
	self.bonus_intellect = self:GetAbility().bonus_intellect
	self.bonus_strength = self:GetAbility().bonus_strength
	self:SetHasCustomTransmitterData( true )
end

function modifier_item_reapers_vessel_lua:OnRefresh()
	if not IsServer() then
		return
	end
	self.bonus_damage = self:GetAbility().bonus_damage
	self.bonus_agility = self:GetAbility().bonus_agility
	self.bonus_intellect = self:GetAbility().bonus_intellect
	self.bonus_strength = self:GetAbility().bonus_strength
	self:SendBuffRefreshToClients()
end

function modifier_item_reapers_vessel_lua:AddCustomTransmitterData()
	return {
		bonus_damage = self.bonus_damage,
		bonus_agility = self.bonus_agility,
		bonus_intellect = self.bonus_intellect,
		bonus_strength = self.bonus_strength,
	}
end

function modifier_item_reapers_vessel_lua:HandleCustomTransmitterData( data )
	self.bonus_damage = data.bonus_damage
	self.bonus_agility = data.bonus_agility
	self.bonus_intellect = data.bonus_intellect
	self.bonus_strength = data.bonus_strength
end

function modifier_item_reapers_vessel_lua:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
		-- MODIFIER_EVENT_ON_DEATH,
	}
	return funcs
end

function modifier_item_reapers_vessel_lua:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "bonus_damage" then
			return 1
		end
        if data.ability_special_value == "bonus_agility" then
			return 1
		end
		if data.ability_special_value == "bonus_intellect" then
			return 1
		end
		if data.ability_special_value == "bonus_strength" then
			return 1
		end
	end
	return 0
end

function modifier_item_reapers_vessel_lua:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
        if data.ability_special_value == "bonus_damage" then
            return self.bonus_damage or 0
		end
		if data.ability_special_value == "bonus_agility" then
            return self.bonus_agility or 0
		end
		if data.ability_special_value == "bonus_intellect" then
            return self.bonus_intellect or 0
		end
		if data.ability_special_value == "bonus_strength" then
            return self.bonus_strength or 0
		end
	end
	return 0
end

function modifier_item_reapers_vessel_lua:CustomOnDeath(params)
    if params.unit:GetTeamNumber() ~= DOTA_TEAM_BADGUYS then
        return false
    end
    if params.unit:GetTeamNumber() == DOTA_TEAM_BADGUYS and params.attacker == self:GetParent() then
        self:GetAbility():OnKill()
    end
end

modifier_item_reapers_vessel_buff = class({})

function modifier_item_reapers_vessel_buff:IsHidden()
	return true
end

function modifier_item_reapers_vessel_buff:IsPurgable()
	return false
end

function modifier_item_reapers_vessel_buff:RemoveOnDeath()
	return false
end

function modifier_item_reapers_vessel_buff:OnCreated(kv)
	self.bonus_damage = kv.bonus_damage
	self.bonus_agility = kv.bonus_agility
	self.bonus_intellect = kv.bonus_intellect
	self.bonus_strength = kv.bonus_strength
	self:GetParent():EmitSound("DOTA_Item.UrnOfShadows.Activate")
end

function modifier_item_reapers_vessel_buff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	}
end

function modifier_item_reapers_vessel_buff:GetModifierPreAttack_BonusDamage()
	return self.bonus_damage
end

function modifier_item_reapers_vessel_buff:GetModifierBonusStats_Strength()
	return self.bonus_strength
end

function modifier_item_reapers_vessel_buff:GetModifierBonusStats_Agility()
	return self.bonus_agility
end

function modifier_item_reapers_vessel_buff:GetModifierBonusStats_Intellect()
	return self.bonus_intellect
end