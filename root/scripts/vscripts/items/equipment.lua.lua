LinkLuaModifier( "modifier_equipment", "items/equipment.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if equipment == nil then
	equipment = class({})
end
function equipment:GetIntrinsicModifierName()
	return "modifier_equipment"
end
---------------------------------------------------------------------
--Modifiers
if modifier_equipment == nil then
	modifier_equipment = class({})
end
function modifier_equipment:OnCreated(params)
	if IsServer() then
	end
end
function modifier_equipment:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_equipment:OnDestroy()
	if IsServer() then
	end
end
function modifier_equipment:DeclareFunctions()
	return {
	}
end