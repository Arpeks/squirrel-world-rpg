LinkLuaModifier( "modifier_pet_rda_bp_9", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_cooldown", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pet_rda_250_minus_armor_debuff", "items/pets/item_pet_RDA_250_minus_armor", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_disarm", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_fear", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_stun", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_slow", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_9_reduce_attack_damage", "items/pets/item_pet_rda_bp_9", LUA_MODIFIER_MOTION_NONE )
spell_item_pet_rda_bp_9 = class({})

function spell_item_pet_rda_bp_9:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_9", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_9:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_9"
end

modifier_item_pet_rda_bp_9 = class({})

function modifier_item_pet_rda_bp_9:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_9:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9:IsPurgeException()
    return false
end

function modifier_item_pet_rda_bp_9:DestroyOnExpire()
    return false
end

if IsServer() then
	function modifier_item_pet_rda_bp_9:OnCreated( kv )
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_9", self:GetCaster():GetAbsOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
			self:DetermineRandomEvent()
			self.PlayerID = self:GetCaster():GetPlayerID()
		end
	end
	function modifier_item_pet_rda_bp_9:OnRefresh()

	end

	function modifier_item_pet_rda_bp_9:OnDestroy()
		UTIL_Remove(self.pet)
	end
end

function modifier_item_pet_rda_bp_9:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
	}
end

function modifier_item_pet_rda_bp_9:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_9:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_9:GetVisualZDelta()
	if self:GetParent():HasModifier("modifier_pet_rda_bp_9") then
		return 100
	end
	return 0
end

function modifier_item_pet_rda_bp_9:DetermineRandomEvent()
	local ability = self:GetAbility()
	local critical_attack_chance = ability:GetSpecialValueFor("critical_attack_chance")
	local armor_reduction_chance = critical_attack_chance + ability:GetSpecialValueFor("armor_reduction_chance")
	local chance_to_restore_health = armor_reduction_chance + ability:GetSpecialValueFor("chance_to_restore_health")
	local chance_to_deal_double_attack = chance_to_restore_health + ability:GetSpecialValueFor("chance_to_deal_double_attack")
	local chance_to_deal_pure_damage = chance_to_deal_double_attack + ability:GetSpecialValueFor("chance_to_deal_pure_damage")
	local chance_of_disarm_yourself = chance_to_deal_pure_damage + ability:GetSpecialValueFor("chance_of_disarm_yourself")
	local chance_to_get_fear = chance_of_disarm_yourself + ability:GetSpecialValueFor("chance_to_get_fear")

	local chance_to_strike_yourself_effects = chance_to_get_fear + ability:GetSpecialValueFor("chance_to_strike_yourself_effects")
	local chance_to_strike_yourself = chance_to_strike_yourself_effects + ability:GetSpecialValueFor("chance_to_strike_yourself")
	local chance_stun_yourself = chance_to_strike_yourself + ability:GetSpecialValueFor("chance_stun_yourself")
	local chance_to_slow_attack_yourself = chance_stun_yourself + ability:GetSpecialValueFor("chance_to_slow_attack_yourself")
	local chance_reduce_attack_damage_yourself = chance_to_slow_attack_yourself + ability:GetSpecialValueFor("chance_reduce_attack_damage_yourself")

	local chance_that_nothing_will_happen = chance_reduce_attack_damage_yourself + ability:GetSpecialValueFor("chance_that_nothing_will_happen")
	local rand = RandomFloat(0, 100)
	if rand < critical_attack_chance then
		self.randomEvent = "critical_attack"
	elseif rand < armor_reduction_chance then
		self.randomEvent = "armor_reduction"
	elseif rand < chance_to_restore_health then
		self.randomEvent = "chance_to_restore_health"
	elseif rand < chance_to_deal_double_attack then
		self.randomEvent = "chance_to_deal_double_attack"
	elseif rand < chance_to_deal_pure_damage then
		self.randomEvent = "chance_to_deal_pure_damage"
	elseif rand < chance_of_disarm_yourself then
		self.randomEvent = "chance_of_disarm_yourself"
	elseif rand < chance_to_get_fear then
		self.randomEvent = "chance_to_get_fear"
	elseif rand < chance_to_strike_yourself_effects then
		self.randomEvent = "chance_to_strike_yourself_effects"
	elseif rand < chance_to_strike_yourself then
		self.randomEvent = "chance_to_strike_yourself"
	elseif rand < chance_stun_yourself then
		self.randomEvent = "chance_stun_yourself"
	elseif rand < chance_to_slow_attack_yourself then
		self.randomEvent = "chance_to_slow_attack_yourself"
	elseif rand < chance_reduce_attack_damage_yourself then
		self.randomEvent = "chance_reduce_attack_damage_yourself"
	else
		self.randomEvent = "chance_that_nothing_will_happen"
	end
	self:GetParent():AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_cooldown", {duration = self:GetAbility():GetSpecialValueFor("reload_after_hit")})
end

function modifier_item_pet_rda_bp_9:PlayEffect()
	local caster = self:GetCaster()
	local dagon_pfx = ParticleManager:CreateParticle("particles/items_fx/dagon.vpcf", PATTACH_RENDERORIGIN_FOLLOW, caster)
	ParticleManager:SetParticleControlEnt(dagon_pfx, 0, self.pet, PATTACH_POINT_FOLLOW, "attach_attack1", self.pet:GetAbsOrigin(), false)
	ParticleManager:SetParticleControlEnt(dagon_pfx, 1, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), false)
	ParticleManager:SetParticleControl(dagon_pfx, 2, Vector(0, 0, 0))
	ParticleManager:SetParticleControl(dagon_pfx, 3, Vector(0.3, 0, 0))
	ParticleManager:ReleaseParticleIndex(dagon_pfx)
end

function modifier_item_pet_rda_bp_9:CustomOnAttackLanded( params )
	local target = params.target
	local attacker = params.attacker
	if attacker == self:GetParent() and not self:GetParent():HasModifier("modifier_item_pet_rda_bp_9_cooldown") then
		if self.randomEvent ~= "critical_attack" then
			if self.randomEvent ~= "chance_that_nothing_will_happen" then
				if self.randomEvent == "armor_reduction" then
					target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_pet_rda_250_minus_armor_debuff", {duration = self:GetAbility():GetSpecialValueFor("duration_of_armor_reduction")})
				elseif self.randomEvent == "chance_to_restore_health" then
					if (not params.target:IsBuilding()) and (not params.target:IsOther()) then
						local heal = params.damage * self:GetAbility():GetSpecialValueFor("health_recovery")/100
						self:GetParent():HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), false)
						local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
						local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						ParticleManager:SetParticleControl( effect_cast, 1, self:GetParent():GetOrigin() )
						ParticleManager:ReleaseParticleIndex( effect_cast )
						if self:GetParent():HasModifier("modifier_wisp_tether_lua") then
							local modifier = self:GetParent():FindModifierByName("modifier_wisp_tether_lua")
							modifier.heal = modifier.heal + heal
						end
					end
				elseif self.randomEvent == "chance_to_deal_double_attack" then
					Timers:CreateTimer(0.1, function()
						self:GetParent():PerformAttack(target, true, true, true, true, true, false, true)
						return nil
					end)
				elseif self.randomEvent == "chance_to_deal_pure_damage" then
					ApplyDamageRDA({
						victim = target,
						attacker = attacker,
						damage = attacker:GetDamageMax() * self:GetAbility():GetSpecialValueFor("pure_damage")/100,
						damage_type = DAMAGE_TYPE_PURE,
						ability = self:GetAbility(),
						damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
					})
				elseif self.randomEvent == "chance_of_disarm_yourself" then
					attacker:AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_disarm", {duration = self:GetAbility():GetSpecialValueFor("duration_of_disarm")})
				elseif self.randomEvent == "chance_to_get_fear" then
					attacker:AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_fear", {duration = self:GetAbility():GetSpecialValueFor("duration_of_fear")})
				elseif self.randomEvent == "chance_to_strike_yourself_effects" then
					Timers:CreateTimer(0.1, function()
						attacker:PerformAttack(attacker, true, true, true, true, true, false, true)
						return nil
					end)
					self:PlayEffect()
				elseif self.randomEvent == "chance_to_strike_yourself" then
					Timers:CreateTimer(0.1, function()
						attacker:PerformAttack(attacker, false, true, true, true, true, false, true)
						return nil
					end)
					self:PlayEffect()
				elseif self.randomEvent == "chance_stun_yourself" then
					attacker:AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_stun", {duration = self:GetAbility():GetSpecialValueFor("stun_duration")})
					self:PlayEffect()
				elseif self.randomEvent == "chance_to_slow_attack_yourself" then
					attacker:AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_slow", {duration = self:GetAbility():GetSpecialValueFor("slow_attack_duration")})
					self:PlayEffect()
				elseif self.randomEvent == "chance_reduce_attack_damage_yourself" then
					attacker:AddNewModifier(attacker, self:GetAbility(), "modifier_item_pet_rda_bp_9_reduce_attack_damage", {
						duration = self:GetAbility():GetSpecialValueFor("damage_reduction_duration"),
						damage_reduction = self:GetParent():GetAverageTrueAttackDamage(self:GetParent()) * (self:GetAbility():GetSpecialValueFor("damage_reduction") / 100)
					})
					self:PlayEffect()
				else
					
				end
			end
			self:DetermineRandomEvent()
		end
	end
end

function modifier_item_pet_rda_bp_9:GetModifierPreAttack_CriticalStrike( params )
	if self.randomEvent == "critical_attack" and not self:GetParent():HasModifier("modifier_item_pet_rda_bp_9_cooldown") then
		self:DetermineRandomEvent()
		return self:GetAbility():GetSpecialValueFor("critical_attack")
	end
end

modifier_item_pet_rda_bp_9_cooldown = class({})

function modifier_item_pet_rda_bp_9_cooldown:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_cooldown:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_cooldown:IsDebuff()
	return false
end

modifier_item_pet_rda_bp_9_disarm = class({})

function modifier_item_pet_rda_bp_9_disarm:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_disarm:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_disarm:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_9_disarm:CheckState()
	local state = {
		[MODIFIER_STATE_DISARMED] = true,
	}
  	return state
end

modifier_item_pet_rda_bp_9_fear = class({})

function modifier_item_pet_rda_bp_9_fear:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_fear:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_fear:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_9_fear:OnCreated( kv )
	if IsServer() then
		self:StartIntervalThink(0.1)
		self:OnIntervalThink()
	end
end

function modifier_item_pet_rda_bp_9_fear:OnIntervalThink()
	self:GetParent():MoveToPosition(Vector( -1174 , -10783 , 512 ))
	self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_pet_rda_bp_9", {})
end

function modifier_item_pet_rda_bp_9_fear:CheckState()
	local state = {
		[MODIFIER_STATE_FEARED] = true,
		[MODIFIER_STATE_IGNORING_MOVE_AND_ATTACK_ORDERS] = true,
	}
  	return state
end

function modifier_item_pet_rda_bp_9_fear:OnDestroy()
	self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_9_disarm", {duration = 3.0})
end

modifier_item_pet_rda_bp_9_stun = class({})

function modifier_item_pet_rda_bp_9_stun:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_stun:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_stun:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_9_stun:OnCreated( kv )

end

function modifier_item_pet_rda_bp_9_stun:OnIntervalThink()
end

function modifier_item_pet_rda_bp_9_stun:CheckState()
	local state = {
		[MODIFIER_STATE_STUNNED] = true,
	}
  	return state
end

function modifier_item_pet_rda_bp_9_stun:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}
end

function modifier_item_pet_rda_bp_9_stun:GetOverrideAnimation()
	return ACT_DOTA_STUN_STATUE
end

modifier_item_pet_rda_bp_9_slow = class({})

function modifier_item_pet_rda_bp_9_slow:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_slow:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_slow:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_9_slow:OnCreated( kv )
	if IsServer() then
		
	end
end

function modifier_item_pet_rda_bp_9_slow:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_item_pet_rda_bp_9_slow:GetModifierAttackSpeedBonus_Constant()
	return (-1) * self:GetParent():GetLevel() * self:GetAbility():GetSpecialValueFor("slow_attack_per_level")
end

modifier_item_pet_rda_bp_9_reduce_attack_damage = class({})

function modifier_item_pet_rda_bp_9_reduce_attack_damage:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:OnCreated( kv )
	if IsServer() then
		self.damage_reduction = kv.damage_reduction
		self:SetHasCustomTransmitterData( true )
	end
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:AddCustomTransmitterData()
    return {
        damage_reduction = self.damage_reduction,
    }
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:HandleCustomTransmitterData( data )
    self.damage_reduction = data.damage_reduction
end


function modifier_item_pet_rda_bp_9_reduce_attack_damage:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	}
end

function modifier_item_pet_rda_bp_9_reduce_attack_damage:GetModifierPreAttack_BonusDamage()
	return -self.damage_reduction
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_9 = class({})

function modifier_pet_rda_bp_9:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_pet_rda_bp_9:IsDebuff()
	return false
end

function modifier_pet_rda_bp_9:IsPurgable()
	return false
end

function modifier_pet_rda_bp_9:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_9:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_9:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_9:GetModifierModelScale()
	return 20
end

function modifier_pet_rda_bp_9:GetModifierModelChange(params)
 	return "models/courier/sw_donkey/sw_donkey_10th_anniversary_flying.vmdl"
end

function modifier_pet_rda_bp_9:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_9:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_9:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_9", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_9:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_9", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end