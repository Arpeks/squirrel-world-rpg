LinkLuaModifier( "modifier_pet_rda_bp_12", "items/pets/item_pet_rda_bp_12", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_12", "items/pets/item_pet_rda_bp_12", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_12_permanent", "items/pets/item_pet_rda_bp_12", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_12 = class({})

function spell_item_pet_rda_bp_12:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_12", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_12:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_12"
end

modifier_item_pet_rda_bp_12 = class({})

function modifier_item_pet_rda_bp_12:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_12:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_12:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_12", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
		self.modifier_permanent = self:GetCaster():FindModifierByName("modifier_item_pet_rda_bp_12_permanent")
		if not self.modifier_permanent then
			self.modifier_permanent = self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_item_pet_rda_bp_12_permanent", {})
		end
		self.modifier_permanent:OnRefresh({ is_active_pet = 1 })
	end
end
function modifier_item_pet_rda_bp_12:OnDestroy()
	UTIL_Remove(self.pet)
	if IsServer() then
		self.modifier_permanent:OnRefresh({ is_active_pet = 0 })
	end
end

function modifier_item_pet_rda_bp_12:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,
	}
end

function modifier_item_pet_rda_bp_12:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_12:GetModifierModelScaleUseInOutEase()
	return false
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_12 = class({})

function modifier_pet_rda_bp_12:IsHidden()
	return true
end

function modifier_pet_rda_bp_12:IsDebuff()
	return false
end

function modifier_pet_rda_bp_12:IsPurgable()
	return false
end

function modifier_pet_rda_bp_12:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_12:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_12:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_12:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_12:GetModifierModelChange(params)
 return "models/items/courier/hamster_courier/hamster_courier_lv2.vmdl"
end

function modifier_pet_rda_bp_12:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_12:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_12:CustomOnAttack( params )
	if IsServer() then
	if params.no_attack_cooldown then
		return
	end
	if params.attacker~=self:GetParent() then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_12", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_12:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_12", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end




LinkLuaModifier( "modifier_item_pet_rda_bp_10_bonus_attribute", "items/pets/item_pet_rda_bp_12", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_10_bonus_mage_damage", "items/pets/item_pet_rda_bp_11", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_10_bonus_phys_damage", "items/pets/item_pet_rda_bp_10", LUA_MODIFIER_MOTION_NONE )

modifier_item_pet_rda_bp_12_permanent = class({})

function modifier_item_pet_rda_bp_12_permanent:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_item_pet_rda_bp_12_permanent:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_12_permanent:RemoveOnDeath()
	return false
end

function modifier_item_pet_rda_bp_12_permanent:OnCreated( kv )
	self.caster = self:GetCaster()
	self.min_x = -12000
	self.max_x = 12000
	self.min_y = -12000
	self.max_y = 12000
	self.points = {}
	self:UpdateVariables()
	if IsServer() then
		-- self.interval = RandomFloat( self.interval_min, self.interval_max ) * self.caster:GetCooldownReduction()
		self.interval = RandomFloat( self.interval_min, self.interval_max )
		self:StartIntervalThink(self.interval)
		CustomGameEventManager:RegisterListener("pet_bp_12_click", function(_, kv)
			self:OnClickPanorama(kv)
		end)
	end
end

function modifier_item_pet_rda_bp_12_permanent:OnRefresh( kv )
	if self:GetAbility() ~= nil then
		self:UpdateVariables()
	end
	if kv.is_active_pet ~= nil then
		self.is_active_pet = kv.is_active_pet == 1 and true or false
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( self.caster:GetPlayerID() ), "UpdateDataPetBP10", self.points )
		-- CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( self.caster:GetPlayerID() ), "UpdateDataPetBP10Visibility", { is_active_pet = self.is_active_pet } )
	end
end

function modifier_item_pet_rda_bp_12_permanent:UpdateVariables()
	self.interval_min = self:GetAbility():GetSpecialValueFor( "interval_min" )
	self.interval_max = self:GetAbility():GetSpecialValueFor( "interval_max" )
	self.spawn_chance = self:GetAbility():GetSpecialValueFor( "spawn_chance" )
	self.spawn_distance = self:GetAbility():GetSpecialValueFor( "spawn_distance" )
	self.inactive = self:GetAbility():GetSpecialValueFor( "inactive" )

	self.bonus_attribute_min = self:GetAbility():GetSpecialValueFor("bonus_attribute_min")
	self.bonus_attribute_max = self:GetAbility():GetSpecialValueFor("bonus_attribute_max")
	self.bonus_damage_phys_min = self:GetAbility():GetSpecialValueFor("bonus_damage_phys_min")
	self.bonus_damage_phys_max = self:GetAbility():GetSpecialValueFor("bonus_damage_phys_max")
	self.bonus_damage_mage_min = self:GetAbility():GetSpecialValueFor( "bonus_damage_mage_min" )
	self.bonus_damage_mage_max = self:GetAbility():GetSpecialValueFor( "bonus_damage_mage_max" )

	local pet_bp_10 = Pets:HasItem(self.caster:GetPlayerID(), "pet_bp_10")
	if pet_bp_10 and Pets:CalculateLevelFromExperience(pet_bp_10.value) >= 10 then
		if not self.modifier_item_pet_rda_bp_10_bonus_phys_damage and IsServer() then
			self.modifier_item_pet_rda_bp_10_bonus_phys_damage = self:GetParent():FindModifierByName("modifier_item_pet_rda_bp_10_bonus_phys_damage")
			if not self.modifier_item_pet_rda_bp_10_bonus_phys_damage then
				self.modifier_item_pet_rda_bp_10_bonus_phys_damage = self:GetParent():AddNewModifier(self:GetParent(), nil, "modifier_item_pet_rda_bp_10_bonus_phys_damage", {})
			end
		end
	end
	local pet_bp_11 = Pets:HasItem(self.caster:GetPlayerID(), "pet_bp_11")
	if pet_bp_11 and Pets:CalculateLevelFromExperience(pet_bp_11.value) >= 10 then
		if not self.modifier_item_pet_rda_bp_10_bonus_mage_damage and IsServer() then
			self.modifier_item_pet_rda_bp_10_bonus_mage_damage = self:GetParent():FindModifierByName("modifier_item_pet_rda_bp_10_bonus_mage_damage")
			if not self.modifier_item_pet_rda_bp_10_bonus_mage_damage then
				self.modifier_item_pet_rda_bp_10_bonus_mage_damage = self:GetParent():AddNewModifier(self:GetParent(), nil, "modifier_item_pet_rda_bp_10_bonus_mage_damage", {})
			end
		end
	end

	if not self.modifier_item_pet_rda_bp_10_bonus_attribute and IsServer() then
		self.modifier_item_pet_rda_bp_10_bonus_attribute = self:GetParent():FindModifierByName("modifier_item_pet_rda_bp_10_bonus_attribute")
		if not self.modifier_item_pet_rda_bp_10_bonus_attribute then
			self.modifier_item_pet_rda_bp_10_bonus_attribute = self:GetParent():AddNewModifier(self:GetParent(), nil, "modifier_item_pet_rda_bp_10_bonus_attribute", {})
		end
	end
end

function modifier_item_pet_rda_bp_12_permanent:AddPoint()
	local GetMaxDistance = function()
		-- if RandomInt(1, 100) <= self.spawn_chance then
		-- 	return self.spawn_distance
		-- end
		-- return 999999
		return self.spawn_distance
	end
	local max_distance = GetMaxDistance()
	local x, y, distance, pos, position
	pos = self.caster:GetAbsOrigin()
	x = RandomInt( math.max( self.min_x, pos.x - max_distance ), math.min( self.max_x, pos.x + max_distance ))
	y = RandomInt( math.max( self.min_y, pos.y - max_distance ), math.min( self.max_y, pos.y + max_distance ))
	position = GetGroundPosition(Vector(x, y, 0), self.caster)
	distance = (Vector(x, y, 0) - pos):Length2D()
	local insertData = {
		x = position.x,
		y = position.y,
		z = position.z,
		create_time = math.floor(GameRules:GetDOTATime(false, false)),
		distance = distance,
		active = true,
		phys_damage =  0,
		mage_damage =  0,
	}
	if self.modifier_item_pet_rda_bp_10_bonus_mage_damage then
		insertData.mage_damage = math.floor(((self:GetCaster():GetSpellAmplification(false)-1) * 100 - self.modifier_item_pet_rda_bp_10_bonus_mage_damage:GetBonusDagameValue()) * (RandomFloat(self.bonus_damage_mage_min, self.bonus_damage_mage_max) / 100))
	end
	if self.modifier_item_pet_rda_bp_10_bonus_phys_damage then
		insertData.phys_damage = math.floor((self.caster:GetAverageTrueAttackDamage(self.caster) - self.modifier_item_pet_rda_bp_10_bonus_phys_damage:GetBonusDagameValue()) * (RandomFloat(self.bonus_damage_phys_min, self.bonus_damage_phys_max) / 100))
	end
	if self.modifier_item_pet_rda_bp_10_bonus_attribute then
		insertData.attribute = math.floor(self:GetCaster():GetLevel() * (RandomFloat(self.bonus_attribute_min, self.bonus_attribute_max) / 100))
	end
	table.insert(self.points, insertData)
	CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( self.caster:GetPlayerID() ), "UpdateDataPetBP10", self.points )
	CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( self.caster:GetPlayerID() ), "EmitSoundPanorama", {
        ["sound"] = "DOTA_Item.ObserverWard.Activate"
    })
end

function modifier_item_pet_rda_bp_12_permanent:OnIntervalThink()
	if self.is_active_pet then
		self:AddPoint()
	end 
	-- self.interval = RandomFloat(self.interval_min, self.interval_max) * self.caster:GetCooldownReduction()
	self.interval = RandomFloat(self.interval_min, self.interval_max)
	self:StartIntervalThink(self.interval)
end

function modifier_item_pet_rda_bp_12_permanent:OnClickPanorama(data)
	if data.PlayerID == self.caster:GetPlayerID() then
		local key = tonumber(data.key)
		if self.points[key].active then
			if self.modifier_item_pet_rda_bp_10_bonus_mage_damage and self.points[key].mage_damage > 0 then
				self.modifier_item_pet_rda_bp_10_bonus_mage_damage:SetStackCount(self.modifier_item_pet_rda_bp_10_bonus_mage_damage:GetStackCount() + self.points[key].mage_damage)
			end
			if self.modifier_item_pet_rda_bp_10_bonus_phys_damage and self.points[key].phys_damage > 0 then
				self.modifier_item_pet_rda_bp_10_bonus_phys_damage:SetStackCount(self.modifier_item_pet_rda_bp_10_bonus_phys_damage:GetStackCount() + self.points[key].phys_damage)
			end
			if self.modifier_item_pet_rda_bp_10_bonus_attribute and self.points[key].attribute > 0 then
				self.modifier_item_pet_rda_bp_10_bonus_attribute:SetStackCount(self.modifier_item_pet_rda_bp_10_bonus_attribute:GetStackCount() + self.points[key].attribute)
			end
			self.points[key].active = false
			CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( self.caster:GetPlayerID() ), "UpdateDataPetBP10", self.points )
		end
	end
end


modifier_item_pet_rda_bp_10_bonus_attribute = class({})

function modifier_item_pet_rda_bp_10_bonus_attribute:GetTexture()
	return "pets/bp12"
end

function modifier_item_pet_rda_bp_10_bonus_attribute:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_item_pet_rda_bp_10_bonus_attribute:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_10_bonus_attribute:RemoveOnDeath()
	return false
end

function modifier_item_pet_rda_bp_10_bonus_attribute:OnCreated( kv )
	self.inactive = 50
	self:StartIntervalThink(1)
	self:OnIntervalThink()
end

function modifier_item_pet_rda_bp_10_bonus_attribute:OnIntervalThink()
	self.is_active_pet = self:GetParent():FindAbilityByName("spell_item_pet_rda_bp_12") ~= nil
end

function modifier_item_pet_rda_bp_10_bonus_attribute:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
	}
	return funcs
end

function modifier_item_pet_rda_bp_10_bonus_attribute:GetBonusDagameValue()
	return self.is_active_pet and self:GetStackCount() or self:GetStackCount() * (self.inactive / 100)
end

function modifier_item_pet_rda_bp_10_bonus_attribute:GetModifierBonusStats_Strength()
	return self:GetBonusDagameValue()
end

function modifier_item_pet_rda_bp_10_bonus_attribute:GetModifierBonusStats_Agility()
	return self:GetBonusDagameValue()
end

function modifier_item_pet_rda_bp_10_bonus_attribute:GetModifierBonusStats_Intellect()
	return self:GetBonusDagameValue()
end