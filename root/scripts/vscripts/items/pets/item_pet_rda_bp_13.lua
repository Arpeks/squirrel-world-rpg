LinkLuaModifier( "modifier_pet_rda_bp_13", "items/pets/item_pet_rda_bp_13", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_13", "items/pets/item_pet_rda_bp_13", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_13_debuff", "items/pets/item_pet_rda_bp_13", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_13 = class({})

function spell_item_pet_rda_bp_13:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_13", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_13:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_13"
end

modifier_item_pet_rda_bp_13 = class({})

function modifier_item_pet_rda_bp_13:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_13:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_13:OnCreated( kv )
	self.is_ranged_attacker = self:GetParent():IsRangedAttacker()
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_13", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
		self:GetParent():SetAttackCapability( DOTA_UNIT_CAP_RANGED_ATTACK )
	end
end
function modifier_item_pet_rda_bp_13:OnDestroy()
	UTIL_Remove(self.pet)
	if IsServer() then
		if not self.is_ranged_attacker then
			self:GetParent():SetAttackCapability( DOTA_UNIT_CAP_MELEE_ATTACK )
		end
	end
end

function modifier_item_pet_rda_bp_13:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,

		MODIFIER_PROPERTY_PROJECTILE_NAME,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
		MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,

		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}
end

function modifier_item_pet_rda_bp_13:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_13:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_13:GetModifierProjectileName()
	if not self.is_ranged_attacker then
		return "particles/units/heroes/hero_witchdoctor/witchdoctor_ward_attack.vpcf"
	end
end

function modifier_item_pet_rda_bp_13:GetModifierProjectileSpeedBonus()
	if not self.is_ranged_attacker then
		return 1100
	end
end

function modifier_item_pet_rda_bp_13:GetModifierAttackRangeBonus()
	return self:GetAbility():GetSpecialValueFor("attack_range")
end

function modifier_item_pet_rda_bp_13:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("attack_speed")
end

function modifier_item_pet_rda_bp_13:CustomOnAttack( keys )

	if keys.no_attack_cooldown then
		return
	end
	if keys.attacker == self:GetParent() then
		local target = keys.target
		target:AddNewModifier(keys.attacker, self:GetAbility(), "modifier_item_pet_rda_bp_13_debuff", {duration = 10})
	end
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_13 = class({})

function modifier_pet_rda_bp_13:IsHidden()
	return true
end

function modifier_pet_rda_bp_13:IsDebuff()
	return false
end

function modifier_pet_rda_bp_13:IsPurgable()
	return false
end

function modifier_pet_rda_bp_13:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_13:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_13:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
	}
	return funcs
end

function modifier_pet_rda_bp_13:GetVisualZDelta()
	return 75
end

function modifier_pet_rda_bp_13:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_13:GetModifierModelChange(params)
 return "models/items/courier/kupu_courier/kupu_courier_flying.vmdl"
end

function modifier_pet_rda_bp_13:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_13:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_13:CustomOnAttack( params )
	if IsServer() then
	if params.no_attack_cooldown then
		return
	end
	if params.attacker~=self:GetParent() then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_13", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_13:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_13", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end


modifier_item_pet_rda_bp_13_debuff = class({})

function modifier_item_pet_rda_bp_13_debuff:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_13_debuff:OnCreated()
	self:SetStackCount( 1 )
end

function modifier_item_pet_rda_bp_13_debuff:OnRefresh(table)
	self:SetStackCount( math.min(self:GetStackCount() + 1, 5) )

	if not IsServer() then return end

	if self:GetStackCount() >= self:GetAbility():GetSpecialValueFor("min_stacks") then
		local damage = self:GetCaster():GetDamageMax() / 100 * self:GetAbility():GetSpecialValueFor("damage_pct")
		ApplyDamageRDA({
			attacker = self:GetCaster(),
			victim = self:GetParent(),
			ability = self:GetAbility(),
			damage = damage,
			damage_type = DAMAGE_TYPE_PURE,
			damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
		})
		SendOverheadEventMessage( self:GetCaster(), OVERHEAD_ALERT_DAMAGE , self:GetParent(), math.floor(damage), self:GetCaster() )
		self:Destroy()
	end

end