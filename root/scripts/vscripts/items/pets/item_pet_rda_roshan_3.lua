LinkLuaModifier( "modifier_pet_rda_roshan_3", "items/pets/item_pet_rda_roshan_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_roshan_3", "items/pets/item_pet_rda_roshan_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pet_rda_roshan_3_split_damage_reduction", "items/pets/item_pet_rda_roshan_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pet_rda_250_minus_armor_debuff", "items/pets/item_pet_RDA_250_minus_armor", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_roshan_3 = class({})

function spell_item_pet_rda_roshan_3:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_roshan_3", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
		-- self:GetCaster():SetMaterialGroup("2")
	end
end

function spell_item_pet_rda_roshan_3:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_roshan_3"
end

modifier_item_pet_rda_roshan_3 = class({})

function modifier_item_pet_rda_roshan_3:IsHidden()
	return true
end

function modifier_item_pet_rda_roshan_3:IsPurgable()
	return false
end

function modifier_item_pet_rda_roshan_3:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_roshan_3", point + Vector(500,500,500), true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
			-- self.pet:SetMaterialGroup("2")
			self:StartIntervalThink(60)
		end
		Timers:CreateTimer(1, function()
            self:GetParent():AddAbility("meepo_pack_rat"):SetLevel(1)
            return nil
        end)
		
	end
end

function modifier_item_pet_rda_roshan_3:OnIntervalThink()
	local gold = self:GetParent():GetGold()
	local totalgold = self:GetParent():GetTotalGold()
	local bonus = self:GetAbility():GetSpecialValueFor("increase_gold")
	local add = (gold / 100 * bonus) + ((totalgold - gold) / 100 * bonus / 2)
	self:GetParent():ModifyGoldFiltered(add, true, 0)
end

function modifier_item_pet_rda_roshan_3:OnDestroy()
	UTIL_Remove(self.pet)
	if IsServer() then
		self:GetParent():RemoveAbility("meepo_pack_rat")
	end
end

function modifier_item_pet_rda_roshan_3:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MANA_REGEN_TOTAL_PERCENTAGE,		--	+
		MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE,			--	+
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,	--	+
		-- MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,				--	+
		-- MODIFIER_EVENT_ON_ATTACK,							--	+
		MODIFIER_PROPERTY_EXP_RATE_BOOST,					--  +
		-- MODIFIER_EVENT_ON_TAKEDAMAGE, 						--  +
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,		--  +
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,				--  +
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,				--  +
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,			--  +
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,					--  +
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,					--  +
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,			--  +
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,		--  +
		MODIFIER_PROPERTY_AVOID_DAMAGE, 
	}
end

function modifier_item_pet_rda_roshan_3:CustomOnTakeDamage(keys)
	if keys.attacker == self:GetParent() and not keys.unit:IsBuilding() and not keys.unit:IsOther() and not FlagExist( keys.damage_flags, DOTA_DAMAGE_FLAG_REFLECTION ) then		
		if keys.damage_category == DOTA_DAMAGE_CATEGORY_SPELL and FlagExist( keys.damage_flags, DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL ) then
			self.lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker)
			ParticleManager:SetParticleControl(self.lifesteal_pfx, 0, keys.attacker:GetAbsOrigin())
			ParticleManager:ReleaseParticleIndex(self.lifesteal_pfx)
		elseif keys.damage_category == DOTA_DAMAGE_CATEGORY_ATTACK then
			local effect_cast = ParticleManager:CreateParticle( "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker )
			ParticleManager:SetParticleControl( effect_cast, 1, keys.unit:GetOrigin() )
			ParticleManager:ReleaseParticleIndex( effect_cast )
		end
		
		heal = keys.damage / 100 * self:GetAbility():GetSpecialValueFor("vampirism")
		keys.attacker:HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), true)
		if self:GetParent():HasModifier("modifier_wisp_tether_lua") then
			local modifier = self:GetParent():FindModifierByName("modifier_wisp_tether_lua")
			modifier.heal = math.min(modifier.heal + heal, 2^30)
		end
		
	end
end

function FlagExist(a,b)
    local p,c,d=1,0,b
    while a>0 and b>0 do
        local ra,rb=a%2,b%2
        if ra+rb>1 then c=c+p end
        a,b,p=(a-ra)/2,(b-rb)/2,p*2
    end
    return c==d
end

function modifier_item_pet_rda_roshan_3:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_roshan_3:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_roshan_3:GetModifierAvoidDamage(params)
	if params.target ~= self:GetParent() then return 0 end
	if self.dodge_time and self.dodge_time > GameRules:GetGameTime() - self:GetAbility():GetSpecialValueFor("dodge_cooldown") then return 0 end
	self.dodge_time = GameRules:GetGameTime()
	return self:GetAbility():GetSpecialValueFor("dodge_chance")
end


function modifier_item_pet_rda_roshan_3:GetModifierTotalPercentageManaRegen()
	return self:GetAbility():GetSpecialValueFor("regen")
end

function modifier_item_pet_rda_roshan_3:GetModifierHealthRegenPercentage()
	return self:GetAbility():GetSpecialValueFor("regen")
end

function modifier_item_pet_rda_roshan_3:CustomOnAttack(keys)
	self.cast = 0
	if keys.attacker == self:GetParent() and keys.target and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() and not keys.no_attack_cooldown and not self:GetParent():PassivesDisabled() and self:GetAbility():IsTrained() and self:GetParent():IsRangedAttacker() then	
		local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE, FIND_ANY_ORDER, false)
		local target_number = 0
		for _, enemy in pairs(enemies) do
			if enemy ~= keys.target then
				self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_pet_rda_roshan_3_split_damage_reduction", {})
				self:GetParent():PerformAttack(enemy, false, true, true, true, true, false, false)
				self:GetParent():RemoveModifierByName("modifier_pet_rda_roshan_3_split_damage_reduction")
				target_number = target_number + 1
				
				if target_number >= self:GetAbility():GetSpecialValueFor("attack_targets")-1 then
					break
				end
			end
		end
	end
end

function modifier_item_pet_rda_roshan_3:CustomOnAttackLanded( keys )
	if keys.attacker == self:GetParent() and ( not self:GetParent():IsIllusion() ) then
		keys.target:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_pet_rda_250_minus_armor_debuff", {duration = 5})	
		if not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (self:GetAbility():GetSpecialValueFor("cleave_damage")/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )
			end
		end
	end
end

function modifier_item_pet_rda_roshan_3:CustomOnAbilityFullyCast(keys)
	if keys.ability:IsItem() then
		return
	end
	
	if IsServer() then
		if keys.unit ~= self:GetParent() then
			return 0
		end
		if keys.ability.CanMulticast and keys.ability:CanMulticast() == false then
			return
		end
		local ability = keys.ability
		if ability == nil then
			return 0
		end
		if RandomInt(1,100) <= self:GetAbility():GetSpecialValueFor("chance_to_multicast") then
			if not string.find(ability:GetName(), "octarine_core") then
				if ability:GetAbilityChargeRestoreTime(ability:GetLevel()) > 0 then
					ability:SetCurrentAbilityCharges(ability:GetCurrentAbilityCharges() + 1)
				else
					ability:EndCooldown()
				end
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_multicast.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector( 1, 2, 1 ) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )
				EmitSoundOn( "Bogduggs.LuckyFemur", self:GetParent() )
			end
		end
	end
	return 0
end

function modifier_item_pet_rda_roshan_3:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_PHYSICAL then
		return self:GetAbility():GetSpecialValueFor("phys_percent")
	end
end

function modifier_item_pet_rda_roshan_3:GetModifierPercentageExpRateBoost()
	return self:GetAbility():GetSpecialValueFor("bonus_experience")
end

function modifier_item_pet_rda_roshan_3:GetModifierIncomingDamage_Percentage()
	return self:GetAbility():GetSpecialValueFor("reduction_incoming_damage")
end

function modifier_item_pet_rda_roshan_3:GetModifierBonusStats_Strength()
	return self:GetAbility():GetSpecialValueFor("stats_bonus") * self:GetParent():GetLevel()
end

function modifier_item_pet_rda_roshan_3:GetModifierBonusStats_Agility()
	return self:GetAbility():GetSpecialValueFor("stats_bonus") * self:GetParent():GetLevel()
end

function modifier_item_pet_rda_roshan_3:GetModifierBonusStats_Intellect()
	return self:GetAbility():GetSpecialValueFor("stats_bonus") * self:GetParent():GetLevel()
end

function modifier_item_pet_rda_roshan_3:GetVisualZDelta()
	if self:GetParent():HasModifier("modifier_pet_rda_roshan_3") then
		return 100
	end
	return 0
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_roshan_3 = class({})

function modifier_pet_rda_roshan_3:IsHidden()
	return true
end

function modifier_pet_rda_roshan_3:IsDebuff()
	return false
end

function modifier_pet_rda_roshan_3:IsPurgable()
	return false
end

function modifier_pet_rda_roshan_3:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )
	-- self:GetParent():SetRenderColor(255,0,0)
	-- self:GetParent():SetMaterialGroup("1")
	
	if not IsServer() then
		return
	end
	self:StartIntervalThink(0.5)
end

function modifier_pet_rda_roshan_3:OnIntervalThink()
	if self:GetAbility() == nil then
		self:Destroy()
	end
end

function modifier_pet_rda_roshan_3:OnDestroy()
	-- self:GetParent():SetRenderColor(255,255,255)
end

function modifier_pet_rda_roshan_3:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_roshan_3:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		-- MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
		MODIFIER_PROPERTY_MODEL_SCALE
	}
	return funcs
end

function modifier_pet_rda_roshan_3:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_roshan_3:GetModifierModelChange(params)
 	return "models/items/courier/azuremircourierfinal/azuremircourierfinal.vmdl"
end

function modifier_pet_rda_roshan_3:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_roshan_3:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_roshan_3:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end
	-- EmitSoundOn("DOTA_Item.BlackKingBar.Activate", self.caster)
	self.caster:AddNewModifier(self:GetParent(), nil, "modifier_custompet_bkb", {duration = self:GetAbility():GetSpecialValueFor("bkb")})
	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_roshan_3", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_roshan_3:CustomOnAbilityFullyCast( params )
	if IsServer() then
		if params.unit == self:GetParent() then
			if self:GetParent().pet_locked then return end
			local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_roshan_3", self:GetParent() )
			if not modifier then return end
			modifier:Destroy()
		end
	end
end
---------------------------------------------------------------------

modifier_pet_rda_roshan_3_split_damage_reduction = class({})

function modifier_pet_rda_roshan_3_split_damage_reduction:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_pet_rda_roshan_3_split_damage_reduction:GetModifierDamageOutgoing_Percentage()
	return -100 + self:GetAbility():GetSpecialValueFor("second_target_damage")
end