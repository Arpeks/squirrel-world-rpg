LinkLuaModifier( "modifier_accessories_squirrel", "items/pets/ability_accessories_squirrel", LUA_MODIFIER_MOTION_NONE )

ability_accessories_squirrel = class({})

function ability_accessories_squirrel:OnOwnerSpawned()
	if self.death_point and RDAItemActions:IsActivatedAcc(self:GetCaster():GetPlayerID(), "respawn_point") then
		self:GetCaster():SetAbsOrigin(self.death_point)
	end
end

function ability_accessories_squirrel:GetIntrinsicModifierName()
	return "modifier_accessories_squirrel"
end

modifier_accessories_squirrel = class({})

function modifier_accessories_squirrel:OnCreated( kv )
	self.PlayerID = self:GetCaster():GetPlayerID()
	self.move_bonus = 0
	self.respawn = false
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.acc = CreateUnitByName("accessories_squirrel", point + Vector(500,500,500), true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.acc:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.acc:SetOwner(self:GetCaster())
		end
		self:SetHasCustomTransmitterData( true )
		self:StartIntervalThink(1)
	end
end

function modifier_accessories_squirrel:AddCustomTransmitterData()
	return {
		move_bonus = self.move_bonus,
		respawn = self.respawn,
	}
end

function modifier_accessories_squirrel:HandleCustomTransmitterData( data )
	self.move_bonus = data.move_bonus
	self.respawn = data.respawn
end

function modifier_accessories_squirrel:OnIntervalThink()
	self.move_bonus = RDAItemActions:IsActivatedAcc(self.PlayerID, "move") and 50 or 0
	self.respawn = RDAItemActions:IsActivatedAcc(self.PlayerID, "respawn_point")
	self:SendBuffRefreshToClients()
end

function modifier_accessories_squirrel:OnDestroy()
	UTIL_Remove(self.acc)
end

function modifier_accessories_squirrel:IsHidden()
	return false
end

function modifier_accessories_squirrel:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		-- MODIFIER_EVENT_ON_DEATH,
	}
end

function modifier_accessories_squirrel:CustomOnAttackLanded(keys)
	if keys.attacker == self.acc then
		self:GetCaster():PerformAttack(
			keys.target, -- hTarget
			true, -- bUseCastAttackOrb
			true, -- bProcessProcs
			true, -- bSkipCooldown
			false, -- bIgnoreInvis
			false, -- bUseProjectile
			false, -- bFakeAttack
			true -- bNeverMiss
		)
	end
end

function modifier_accessories_squirrel:GetModifierMoveSpeedBonus_Percentage()
	return self.move_bonus
end

function modifier_accessories_squirrel:CustomOnDeath(keys)
	if keys.unit:GetTeam() == self:GetParent():GetTeam() and keys.unit == self:GetParent() then
		self:GetAbility().death_point = self.respawn and self:GetParent():GetAbsOrigin() or nil
	end
end