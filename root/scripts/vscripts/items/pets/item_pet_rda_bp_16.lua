LinkLuaModifier( "modifier_pet_rda_bp_16", "items/pets/item_pet_rda_bp_16", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_16", "items/pets/item_pet_rda_bp_16", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_16_buff", "items/pets/item_pet_rda_bp_16", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_16 = class({})

function spell_item_pet_rda_bp_16:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_16", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_16:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_16"
end

modifier_item_pet_rda_bp_16 = class({})

function modifier_item_pet_rda_bp_16:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_16:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_16:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_16", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end

		self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_16_buff", {})
	end
end

function modifier_item_pet_rda_bp_16:OnRefresh( kv )
	if IsServer() then
		self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_16_buff", {})
	end
end

function modifier_item_pet_rda_bp_16:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_rda_bp_16:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,

		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_16:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_16:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_16:GetModifierBaseAttack_BonusDamage()
	return self:GetAbility():GetSpecialValueFor("bonus_base_damage") * self:GetCaster():GetLevel()
end

function modifier_item_pet_rda_bp_16:GetModifierSpellAmplify_Percentage()
	return self:GetAbility():GetSpecialValueFor("spell_ampl") * self:GetCaster():GetLevel()
end

function modifier_item_pet_rda_bp_16:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_PHYSICAL then
		return self:GetAbility():GetSpecialValueFor("increased_outgoing_physical")
	end
end

function modifier_item_pet_rda_bp_16:GetModifierBonusStats_Strength()
	return self:GetAbility():GetSpecialValueFor("bonus_all_stats") * self:GetCaster():GetLevel()
end

function modifier_item_pet_rda_bp_16:GetModifierBonusStats_Intellect()
	return self:GetAbility():GetSpecialValueFor("bonus_all_stats") * self:GetCaster():GetLevel()
end

function modifier_item_pet_rda_bp_16:GetModifierBonusStats_Agility()
	return self:GetAbility():GetSpecialValueFor("bonus_all_stats") * self:GetCaster():GetLevel()
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_16 = class({})

function modifier_pet_rda_bp_16:IsHidden()
	return true
end

function modifier_pet_rda_bp_16:IsDebuff()
	return false
end

function modifier_pet_rda_bp_16:IsPurgable()
	return false
end

function modifier_pet_rda_bp_16:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_16:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_16:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
	}
	return funcs
end

function modifier_pet_rda_bp_16:GetVisualZDelta()
	return 100
end

function modifier_pet_rda_bp_16:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_16:GetModifierModelChange(params)
 return "models/items/courier/rayth_the_roaming_shade/rayth_the_roaming_shade_flying.vmdl"
end

function modifier_pet_rda_bp_16:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_16:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_16:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end
	
	if params.no_attack_cooldown then
        return
    end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_16", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_16:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_16", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end