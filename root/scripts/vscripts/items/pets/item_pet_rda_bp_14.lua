LinkLuaModifier( "modifier_pet_rda_bp_14", "items/pets/item_pet_rda_bp_14", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_14", "items/pets/item_pet_rda_bp_14", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_14_debuff", "items/pets/item_pet_rda_bp_14", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_14 = class({})

function spell_item_pet_rda_bp_14:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_14", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_14:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_14"
end

modifier_item_pet_rda_bp_14 = class({})

function modifier_item_pet_rda_bp_14:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_14:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_14:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_14", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
	end
end
function modifier_item_pet_rda_bp_14:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_rda_bp_14:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,

		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,

		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
	}
end

function modifier_item_pet_rda_bp_14:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_14:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_14:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("attack_speed")
end

function modifier_item_pet_rda_bp_14:CustomOnAttack( keys )
	if keys.no_attack_cooldown then
        return
    end
	if keys.attacker == self:GetParent() then
		local target = keys.target
		target:AddNewModifier(keys.attacker, self:GetAbility(), "modifier_item_pet_rda_bp_14_debuff", {duration = 3})
	end
end

function modifier_item_pet_rda_bp_14:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_PHYSICAL then
		return self:GetAbility():GetSpecialValueFor("increase_physical_damage")
	end
end

function modifier_item_pet_rda_bp_14:GetModifierBonusStats_Agility()
	return self:GetCaster():GetLevel() * self:GetAbility():GetSpecialValueFor("all_stats")
end

function modifier_item_pet_rda_bp_14:GetModifierBonusStats_Intellect()
	return self:GetCaster():GetLevel() * self:GetAbility():GetSpecialValueFor("all_stats")
end

function modifier_item_pet_rda_bp_14:GetModifierBonusStats_Strength()
	return self:GetCaster():GetLevel() * self:GetAbility():GetSpecialValueFor("all_stats")
end

function modifier_item_pet_rda_bp_14:CustomOnTakeDamage( params )
	if IsServer() then
		if params.attacker == self:GetCaster() then
			if pass then
				local lifesteal = self:GetAbility():GetSpecialValueFor("lifesteal_prc")
				local heal = params.damage * lifesteal/100
				self:GetParent():Heal( math.min(math.abs(heal), 2^30), self:GetAbility() )
				self:PlayEffects( self:GetParent() )
			end
		end
	end
end

function modifier_item_pet_rda_bp_14:GetModifierBaseAttack_BonusDamage( target )
    return self:GetCaster():GetLevel() * self:GetAbility():GetSpecialValueFor("bonus_damage")
end

function modifier_item_pet_rda_bp_14:PlayEffects( target )
	local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
	local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControl( effect_cast, 1, target:GetOrigin() )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_14 = class({})

function modifier_pet_rda_bp_14:IsHidden()
	return true
end

function modifier_pet_rda_bp_14:IsDebuff()
	return false
end

function modifier_pet_rda_bp_14:IsPurgable()
	return false
end

function modifier_pet_rda_bp_14:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_14:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_14:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
	}
	return funcs
end

function modifier_pet_rda_bp_14:GetVisualZDelta()
	return 75
end

function modifier_pet_rda_bp_14:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_14:GetModifierModelChange(params)
 return "models/items/courier/onibi_lvl_21/onibi_lvl_21_flying.vmdl"
end

function modifier_pet_rda_bp_14:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_14:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_14:CustomOnAttack( params )
	if IsServer() then
	if params.no_attack_cooldown then
		return
	end
	if params.attacker~=self:GetParent() then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_14", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_14:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_14", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end


modifier_item_pet_rda_bp_14_debuff = class({})

function modifier_item_pet_rda_bp_14_debuff:IsDebuff()
	return true
end

function modifier_item_pet_rda_bp_14_debuff:OnCreated( kv )
	self.armor_corruption = self:GetAbility():GetSpecialValueFor("armor_corruption") * -1
end

function modifier_item_pet_rda_bp_14_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
	}
end

function modifier_item_pet_rda_bp_14_debuff:GetModifierPhysicalArmorBonus()
	return self.armor_corruption
end