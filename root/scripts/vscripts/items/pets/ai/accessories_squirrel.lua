local items = {}

for _, cat in pairs(RDA_QUEST_RPG_DATA_ARRAY) do
    for _, quest in ipairs(cat) do
        for _, task in ipairs(quest) do
            if task.flags[RDA_QUEST_FLAG_FIND_ITEMS] then
                for _, item_name in pairs(task.targets) do
                    items[item_name] = true
                end
            end
        end
    end
end



function Precache(context)
	PrecacheModel(thisEntity:GetModelName(), context)
end

function Spawn( entityKeyValues )
	if not IsServer() then
		return
	end

	if thisEntity == nil then
		return
	end
	
	thisEntity:SetContextThink( "BanditArcherThink", BanditArcherThink, 0.5 )
end

--------------------------------------------------------------------------------
function BanditArcherThink()
	if not IsServer() then
		return
	end

	if ( not thisEntity:IsAlive() ) then
		return -1
	end

	if GameRules:IsGamePaused() == true then
		return 0.5
	end
	
    local owner = thisEntity:GetOwner()
	
	local flDist = ( thisEntity:GetOwner():GetOrigin() - thisEntity:GetOrigin() ):Length2D()

    if RDAItemActions:IsActivatedAcc(owner:GetPlayerID(), "pick_up") then
        local items_on_the_ground = Entities:FindAllByClassnameWithin("dota_item_drop", thisEntity:GetOrigin(), 900)
        for _,item in pairs(items_on_the_ground) do
            if item then
                local containedItem = item:GetContainedItem()	
                local item_name = containedItem:GetAbilityName()

                if items[item_name] then
                    ExecuteOrderFromTable({
                        UnitIndex = thisEntity:GetEntityIndex(),
                        TargetIndex = item:GetEntityIndex(),
                        OrderType = DOTA_UNIT_ORDER_PICKUP_ITEM,
                        Queue = true,
                    })
                end
            end
        end
    end

    for i = DOTA_ITEM_SLOT_1, DOTA_ITEM_SLOT_9 do
        local item = thisEntity:GetItemInSlot(i)
        if item then
            thisEntity:MoveToNPCToGiveItem(thisEntity:GetOwnerEntity(), item)
        end
    end

    if flDist > 1000 then
        return blink(owner)
    end
    if RDAItemActions:IsActivatedAcc(owner:GetPlayerID(), "attack") then
        if thisEntity:AttackReady() then
            local hEnemies = FindUnitsInRadius(thisEntity:GetTeamNumber(), thisEntity:GetAbsOrigin(), nil, 600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false )

            if #hEnemies > 0 then
                ExecuteOrderFromTable({
                    UnitIndex = thisEntity:GetEntityIndex(),
                    TargetIndex = hEnemies[1]:entindex(),
                    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
                    Queue = false,
                })
                return 0.5
            end
        end
    end

    if flDist < 400 then
        return Retreat(owner)
    end
    if flDist >= 400 and flDist < 800 then
        return Approach(owner)
    end
	return 0.5
end


function blink(unit)
	local vToEnemy = unit:GetOrigin() - thisEntity:GetOrigin()
	vToEnemy = vToEnemy:Normalized()
	
	ExecuteOrderFromTable({
		UnitIndex = thisEntity:entindex(),
		thisEntity:SetAbsOrigin( unit:GetOrigin() + RandomVector( RandomFloat(50, 50 ))  )
	})
	FindClearSpaceForUnit(thisEntity, unit:GetOrigin()+ RandomVector( RandomFloat(50, 50 )), false)
	return 1
end

function Approach(unit)
	local vToEnemy = unit:GetOrigin() - thisEntity:GetOrigin()
	vToEnemy = vToEnemy:Normalized()
	ExecuteOrderFromTable({
		UnitIndex = thisEntity:entindex(),
		OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
		Position = thisEntity:GetOrigin() + vToEnemy * thisEntity:GetIdealSpeed()
	})
	return 1
end

function Retreat(unit)
	local vAwayFromEnemy = thisEntity:GetOrigin() - unit:GetOrigin()
	vAwayFromEnemy = vAwayFromEnemy:Normalized()
	local vMoveToPos = thisEntity:GetOrigin() + vAwayFromEnemy * thisEntity:GetIdealSpeed()
	local nAttempts = 0
	while ( ( not GridNav:CanFindPath( thisEntity:GetOrigin(), vMoveToPos ) ) and ( nAttempts < 5 ) ) do
		vMoveToPos = thisEntity:GetOrigin() + RandomVector( thisEntity:GetIdealSpeed() )
		nAttempts = nAttempts + 1
	end
	
	thisEntity.fTimeOfLastRetreat = GameRules:GetGameTime()

	ExecuteOrderFromTable({
		UnitIndex = thisEntity:entindex(),
		OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
		Position = vMoveToPos,
	})
	return 1.5
end