LinkLuaModifier( "modifier_pet_rda_bp_15", "items/pets/item_pet_rda_bp_15", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_15", "items/pets/item_pet_rda_bp_15", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_15_buff", "items/pets/item_pet_rda_bp_15", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_15 = class({})

function spell_item_pet_rda_bp_15:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_15", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_15:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_15"
end

modifier_item_pet_rda_bp_15 = class({})

function modifier_item_pet_rda_bp_15:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_15:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_15:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_15", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end

		self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_15_buff", {})
	end
end

function modifier_item_pet_rda_bp_15:OnRefresh( kv )
	if IsServer() then
		self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_15_buff", {})
	end
end

function modifier_item_pet_rda_bp_15:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_rda_bp_15:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,

		MODIFIER_PROPERTY_PROJECTILE_SPEED_BONUS,
	}
end

function modifier_item_pet_rda_bp_15:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_15:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_15:CustomOnAttack(keys)
    if keys.no_attack_cooldown then
        return
    end
    if keys.attacker == self:GetParent() and self:GetParent():IsRangedAttacker() and keys.target and RollPseudoRandomPercentage(self:GetAbility():GetSpecialValueFor("bonus_attack_chance"), self:GetCaster():entindex(), self:GetCaster()) then	
	
        local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), keys.target:GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NOT_MAGIC_IMMUNE_ALLIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false)		
        local nTargetNumber = 0		
        for _, hEnemy in pairs(enemies) do
            if hEnemy ~= keys.target then

                self:GetParent():PerformAttack(hEnemy, true, true, true, true, true, false, false)		
                nTargetNumber = nTargetNumber + 1
                
                if nTargetNumber >= self:GetAbility():GetSpecialValueFor("bonus_attack_targets") then
                    break
                end
            end
        end
    end 
end

function modifier_item_pet_rda_bp_15:GetModifierProjectileSpeedBonus()
	return self:GetAbility():GetSpecialValueFor("projectile_speed")
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_15 = class({})

function modifier_pet_rda_bp_15:IsHidden()
	return true
end

function modifier_pet_rda_bp_15:IsDebuff()
	return false
end

function modifier_pet_rda_bp_15:IsPurgable()
	return false
end

function modifier_pet_rda_bp_15:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_15:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_15:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
	}
	return funcs
end

function modifier_pet_rda_bp_15:GetVisualZDelta()
	return 75
end

function modifier_pet_rda_bp_15:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_15:GetModifierModelChange(params)
 return "models/items/courier/nian_courier/nian_courier.vmdl"
end

function modifier_pet_rda_bp_15:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_15:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_15:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_15", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_15:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_15", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end


modifier_item_pet_rda_bp_15_buff = class({})

function modifier_item_pet_rda_bp_15_buff:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_15_buff:IsDebuff()
	return false
end

function modifier_item_pet_rda_bp_15_buff:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_15_buff:RemoveOnDeath()
	return false
end

function modifier_item_pet_rda_bp_15_buff:OnCreated( kv )
	self.bonus_damage_per_gold 		= self:GetAbility():GetSpecialValueFor( "bonus_damage_per_gold" )
	self.bonus_damage_gold_spent 	= self:GetAbility():GetSpecialValueFor( "bonus_damage_gold_spent" )
	self.bonus_ampl_per_gold 		= self:GetAbility():GetSpecialValueFor( "bonus_ampl_per_gold" )
	self.bonus_ampl_gold_spent 		= self:GetAbility():GetSpecialValueFor( "bonus_ampl_gold_spent" )
	self.residual_power 			= self:GetAbility():GetSpecialValueFor( "residual_power" )
	if IsServer() then
		self:SetHasCustomTransmitterData( true )
		self:OnIntervalThink()
	end
end

function modifier_item_pet_rda_bp_15_buff:OnRefresh( kv )
	self.bonus_damage_per_gold 		= self:GetAbility():GetSpecialValueFor( "bonus_damage_per_gold" )
	self.bonus_damage_gold_spent 	= self:GetAbility():GetSpecialValueFor( "bonus_damage_gold_spent" )
	self.bonus_ampl_per_gold 		= self:GetAbility():GetSpecialValueFor( "bonus_ampl_per_gold" )
	self.bonus_ampl_gold_spent 		= self:GetAbility():GetSpecialValueFor( "bonus_ampl_gold_spent" )
	self.residual_power 			= self:GetAbility():GetSpecialValueFor( "residual_power" )
end

function modifier_item_pet_rda_bp_15_buff:OnIntervalThink()
	local gold_spent = self:GetParent().gold_spent or 0
	
	self.bonus_damage = gold_spent / self.bonus_damage_gold_spent * self.bonus_damage_per_gold
	self.bonus_ampl = gold_spent / self.bonus_ampl_gold_spent * self.bonus_ampl_per_gold
	if self:GetParent():FindAbilityByName("spell_item_pet_rda_bp_15") == nil then
		self.bonus_damage = self.residual_power / 100 * self.bonus_damage
		self.bonus_ampl = self.residual_power / 100 * self.bonus_ampl
	end
	self:SendBuffRefreshToClients()
	self:StartIntervalThink(1)
end

function modifier_item_pet_rda_bp_15_buff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_15_buff:GetModifierPreAttack_BonusDamage()
	return self.bonus_damage
end

function modifier_item_pet_rda_bp_15_buff:GetModifierSpellAmplify_Percentage()
	return self.bonus_ampl
end

function modifier_item_pet_rda_bp_15_buff:AddCustomTransmitterData()
	return {
		bonus_damage = self.bonus_damage,
		bonus_ampl = self.bonus_ampl,
	}
end

function modifier_item_pet_rda_bp_15_buff:HandleCustomTransmitterData( data )
	self.bonus_damage = data.bonus_damage
	self.bonus_ampl = data.bonus_ampl
end