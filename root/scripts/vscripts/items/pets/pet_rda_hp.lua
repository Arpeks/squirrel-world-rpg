LinkLuaModifier( "modifier_rda_pet_hp", "items/pets/pet_rda_hp", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_RDA_hp", "items/pets/pet_rda_hp", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_RDA_hp = class({})

function spell_item_pet_RDA_hp:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		self.caster:AddNewModifier(self.caster,self,"modifier_rda_pet_hp", {})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_RDA_hp:GetIntrinsicModifierName()
	return "modifier_item_pet_RDA_hp"
end

modifier_item_pet_RDA_hp = class({})

function modifier_item_pet_RDA_hp:IsHidden()
	return true
end

function modifier_item_pet_RDA_hp:IsPurgable()
	return false
end

function modifier_item_pet_RDA_hp:OnCreated( kv )
	if not IsServer() then
		return
	end
	if self:GetCaster():IsRealHero() then
	local point = self:GetCaster():GetAbsOrigin()
	self.pet = CreateUnitByName("pet_rda_hp", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
	self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
	self.pet:SetOwner(self:GetCaster())
end

function modifier_item_pet_RDA_hp:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_RDA_hp:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_HEALTH_BONUS,
	}
end

function modifier_item_pet_RDA_hp:GetModifierHealthBonus( params )
	return self:GetAbility():GetSpecialValueFor( "hp" ) * self:GetCaster():GetLevel()
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_rda_pet_hp = class({})

function modifier_rda_pet_hp:IsHidden()
	return true
end

function modifier_rda_pet_hp:IsDebuff()
	return false
end

function modifier_rda_pet_hp:IsPurgable()
	return false
end

function modifier_rda_pet_hp:OnCreated( kv )
	self.caster = self:GetCaster()
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )
end

function modifier_rda_pet_hp:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
end

function modifier_rda_pet_hp:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
	}
end

function modifier_rda_pet_hp:GetModifierModelChange(params)
 	return "models/courier/minipudge/minipudge.vmdl"
end

function modifier_rda_pet_hp:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_rda_pet_hp:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_rda_pet_hp:CustomOnAttack( params )
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_rda_pet_hp", self:GetParent() )
	if not modifier then return end
	modifier:Destroy()
end

function modifier_rda_pet_hp:OnSpentMana( params )		
	if params.unit == parent and params.ability:GetManaCost() > 10 then
		local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_rda_pet_hp", self:GetParent() )
		if not modifier then return end
			modifier:Destroy()
		end
	end
end