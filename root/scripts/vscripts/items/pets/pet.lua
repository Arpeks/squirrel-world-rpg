LinkLuaModifier( "modifier_pet_simp", "items/pets/pet", LUA_MODIFIER_MOTION_NONE )

spell_item_pet = class({})

function spell_item_pet:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		self.caster:AddNewModifier(self.caster,self,"modifier_pet_simp", {})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_simp = class({})

function modifier_pet_simp:IsHidden()
	return true
end

function modifier_pet_simp:IsDebuff()
	return false
end

function modifier_pet_simp:IsPurgable()
	return false
end

function modifier_pet_simp:OnCreated( kv )
	self.caster = self:GetCaster()
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )
end

function modifier_pet_simp:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
end

function modifier_pet_simp:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
	}
end

function modifier_pet_simp:GetModifierModelChange(params)
 	return "models/courier/mech_donkey/mech_donkey.vmdl"
end

function modifier_pet_simp:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_simp:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_simp:CustomOnAttack( params )
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_simp", self:GetParent() )
	if not modifier then return end
	modifier:Destroy()
end

function modifier_pet_simp:OnSpentMana( params )
	if params.unit == parent and params.ability:GetManaCost() > 10 then
		local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_simp", self:GetParent() )
		if not modifier then return end
		modifier:Destroy()
	end
end