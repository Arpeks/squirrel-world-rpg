LinkLuaModifier( "modifier_pet_rda_bp_3", "items/pets/item_pet_rda_bp_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_3", "items/pets/item_pet_rda_bp_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_3_split_damage_reduction", "items/pets/item_pet_rda_bp_3", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_3 = class({})

function spell_item_pet_rda_bp_3:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_3", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_3:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_3"
end

modifier_item_pet_rda_bp_3 = class({})

function modifier_item_pet_rda_bp_3:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_3:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_3:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then 
			self.pet = CreateUnitByName("pet_rda_bp_3", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
	end
end
function modifier_item_pet_rda_bp_3:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_rda_bp_3:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,
		-- MODIFIER_EVENT_ON_ATTACK,
	}
end

function modifier_item_pet_rda_bp_3:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_3:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_3:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end

function modifier_item_pet_rda_bp_3:CustomOnAttack(keys)
	if not IsServer() then return end
	if keys.attacker == self:GetParent() and keys.target and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() and not keys.no_attack_cooldown and not self:GetParent():PassivesDisabled() and self:GetAbility():IsTrained() and self:GetParent():IsRangedAttacker() then	
		local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE, FIND_ANY_ORDER, false)
		local target_number = 0
		for _, enemy in pairs(enemies) do
			if enemy ~= keys.target then
				self:GetParent():AddNewModifier(self:GetParent(), self:GetAbility(), "modifier_item_pet_rda_bp_3_split_damage_reduction", {})
				self:GetParent():PerformAttack(enemy, false, true, true, true, true, false, false)
				self:GetParent():RemoveModifierByName("modifier_item_pet_rda_bp_3_split_damage_reduction")
				target_number = target_number + 1
				
				if target_number >= self:GetAbility():GetSpecialValueFor("attack_targets")-1 then
					break
				end
			end
		end
	end
end

function modifier_item_pet_rda_bp_3:CustomOnAttackLanded( keys )
	if keys.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,600,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (self:GetAbility():GetSpecialValueFor("cleave_damage")/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )
			end
		end
		local heal_amount = keys.damage / 100 * self:GetAbility():GetSpecialValueFor("attack_lifesteal")
		self:GetParent():Heal(math.min(math.abs(heal_amount), 2^30), self:GetAbility())
		local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
		local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		ParticleManager:SetParticleControl( effect_cast, 1, self:GetParent():GetOrigin() )
		ParticleManager:ReleaseParticleIndex( effect_cast )
	end
end

modifier_item_pet_rda_bp_3_split_damage_reduction = class({})

function modifier_item_pet_rda_bp_3_split_damage_reduction:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_3_split_damage_reduction:GetModifierDamageOutgoing_Percentage()
	return -100 + self:GetAbility():GetSpecialValueFor("second_target_damage")
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_3 = class({})

function modifier_pet_rda_bp_3:IsHidden()
	return true
end

function modifier_pet_rda_bp_3:IsDebuff()
	return false
end

function modifier_pet_rda_bp_3:IsPurgable()
	return false
end

function modifier_pet_rda_bp_3:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_3:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_3:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_3:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_3:GetModifierModelChange(params)
 return "models/items/courier/autumn_wards_courier/autumn_wards_courier.vmdl"
end

function modifier_pet_rda_bp_3:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_3:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_3:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_3", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_3:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_3", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end