LinkLuaModifier( "modifier_pet_rda_bp_4", "items/pets/item_pet_rda_bp_4", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_4", "items/pets/item_pet_rda_bp_4", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_4_split_damage_reduction", "items/pets/item_pet_rda_bp_4", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_4 = class({})

function spell_item_pet_rda_bp_4:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_4", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_4:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_4"
end

modifier_item_pet_rda_bp_4 = class({})

function modifier_item_pet_rda_bp_4:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_4:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_4:OnCreated( kv )
	if IsServer() then
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_4", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
	end
end
function modifier_item_pet_rda_bp_4:OnDestroy()
	UTIL_Remove(self.pet)
end

function modifier_item_pet_rda_bp_4:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,
	}
end

function modifier_item_pet_rda_bp_4:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_4:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_4:GetModifierSpellAmplify_Percentage( params )
	return self:GetParent():GetAgility() * self:GetAbility():GetSpecialValueFor("agility_spell_amplify") + self:GetParent():GetStrength() * self:GetAbility():GetSpecialValueFor("strength_spell_amplify")
end

modifier_item_pet_rda_bp_4_split_damage_reduction = class({})

function modifier_item_pet_rda_bp_4_split_damage_reduction:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_4_split_damage_reduction:GetModifierDamageOutgoing_Percentage()
	return -100 + self:GetAbility():GetSpecialValueFor("second_target_damage")
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_4 = class({})

function modifier_pet_rda_bp_4:IsHidden()
	return true
end

function modifier_pet_rda_bp_4:IsDebuff()
	return false
end

function modifier_pet_rda_bp_4:IsPurgable()
	return false
end

function modifier_pet_rda_bp_4:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_4:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_4:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_4:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_4:GetModifierModelChange(params)
 return "models/items/courier/waldi_the_faithful/waldi_the_faithful.vmdl"
end

function modifier_pet_rda_bp_4:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_4:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_4:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_4", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_4:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_4", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end