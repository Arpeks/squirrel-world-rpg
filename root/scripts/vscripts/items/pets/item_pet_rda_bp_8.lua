LinkLuaModifier( "modifier_pet_rda_bp_8", "items/pets/item_pet_rda_bp_8", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_8", "items/pets/item_pet_rda_bp_8", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_8_stack", "items/pets/item_pet_rda_bp_8", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_8 = class({})

function spell_item_pet_rda_bp_8:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_8", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_8:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_8"
end

modifier_item_pet_rda_bp_8 = class({})

function modifier_item_pet_rda_bp_8:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_8:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_8:IsPurgeException()
    return false
end

function modifier_item_pet_rda_bp_8:DestroyOnExpire()
    return false
end

if IsServer() then
	function modifier_item_pet_rda_bp_8:OnCreated( kv )
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_8", self:GetCaster():GetAbsOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
	end
	function modifier_item_pet_rda_bp_8:OnRefresh()
		if not self:GetParent():IsRangedAttacker() then
			return
		end
	end

	function modifier_item_pet_rda_bp_8:OnDestroy()
		UTIL_Remove(self.pet)
	end
end

function modifier_item_pet_rda_bp_8:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA,
		MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_item_pet_rda_bp_8:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_8:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_8:GetVisualZDelta()
	if self:GetParent():HasModifier("modifier_pet_rda_bp_8") then
		return 100
	end
	return 0
end

function modifier_item_pet_rda_bp_8:GetModifierBaseAttack_BonusDamage( params )
	return (self:GetCaster():GetIntellect(true) + self:GetCaster():GetAgility() + self:GetCaster():GetStrength()) * self:GetAbility():GetSpecialValueFor( "bonus_damage_attribute" )
end

function modifier_item_pet_rda_bp_8:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end

modifier_item_pet_rda_bp_8_stack = class({})

function modifier_item_pet_rda_bp_8_stack:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_8_stack:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_8_stack:IsDebuff()
	return false
end

function modifier_item_pet_rda_bp_8_stack:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_item_pet_rda_bp_8_stack:OnRemoved()
	if IsServer() then
		local modifier_item_pet_rda_bp_8 = self:GetCaster():FindModifierByName("modifier_item_pet_rda_bp_8")
		modifier_item_pet_rda_bp_8:SetStackCount(modifier_item_pet_rda_bp_8:GetStackCount() - 1)
	end
end

function modifier_item_pet_rda_bp_8_stack:OnDestroy()
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_8 = class({})

function modifier_pet_rda_bp_8:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_pet_rda_bp_8:IsDebuff()
	return false
end

function modifier_pet_rda_bp_8:IsPurgable()
	return false
end

function modifier_pet_rda_bp_8:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_8:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_8:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_8:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_8:GetModifierModelChange(params)
 	return "models/courier/winter2022/taffy_donkey_courier_wings.vmdl"
end

function modifier_pet_rda_bp_8:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_8:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_8:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_8", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_8:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_8", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end