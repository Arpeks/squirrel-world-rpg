LinkLuaModifier( "modifier_pet_rda_250_no_spell_phys_bonus", "items/pets/item_pet_RDA_250_no_spell_phys_bonus", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_RDA_250_no_spell_phys_bonus", "items/pets/item_pet_RDA_250_no_spell_phys_bonus", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_RDA_250_no_spell_phys_bonus = class({})

function spell_item_pet_RDA_250_no_spell_phys_bonus:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_item_pet_RDA_250_no_spell_phys_bonus", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_RDA_250_no_spell_phys_bonus:GetIntrinsicModifierName()
	return "modifier_pet_rda_250_no_spell_phys_bonus"
end

modifier_pet_rda_250_no_spell_phys_bonus = class({})

function modifier_pet_rda_250_no_spell_phys_bonus:IsHidden()
	return true
end

function modifier_pet_rda_250_no_spell_phys_bonus:IsPurgable()
	return false
end

function modifier_pet_rda_250_no_spell_phys_bonus:OnCreated( kv )
		if IsServer() then
	self.bonus = self:GetAbility():GetSpecialValueFor( "str" )
		local point = self:GetCaster():GetAbsOrigin()
		if self:GetCaster():IsRealHero() then
	self.pet = CreateUnitByName("pet_rda_250_no_spell_phys_bonus", point + Vector(500,500,500), true, nil, nil, DOTA_TEAM_GOODGUYS)
	self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
		self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
self.pet:SetOwner(self:GetCaster())
		end
end
end
function modifier_pet_rda_250_no_spell_phys_bonus:OnDestroy()
	UTIL_Remove(self.pet)
end
function modifier_pet_rda_250_no_spell_phys_bonus:DeclareFunctions()
	local funcs =
	{
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE
	}
	return funcs
end

function modifier_pet_rda_250_no_spell_phys_bonus:GetModifierTotalDamageOutgoing_Percentage(keys)
	if keys.damage_type == DAMAGE_TYPE_PHYSICAL then
		return self:GetAbility():GetSpecialValueFor("phys_percent")
	end
	if keys.damage_type == DAMAGE_TYPE_MAGICAL then return -200 end
	if keys.damage_type == DAMAGE_TYPE_PURE then return -200 end
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_item_pet_RDA_250_no_spell_phys_bonus = class({})

function modifier_item_pet_RDA_250_no_spell_phys_bonus:IsHidden()
	return true
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:IsDebuff()
	return false
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:IsPurgable()
	return false
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:DeclareFunctions()
	local funcs = {
	MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
	}
	return funcs
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:GetModifierModelChange(params)
 return "models/items/courier/lilnova/lilnova.vmdl"
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_item_pet_RDA_250_no_spell_phys_bonus", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_item_pet_RDA_250_no_spell_phys_bonus:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_item_pet_RDA_250_no_spell_phys_bonus", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end