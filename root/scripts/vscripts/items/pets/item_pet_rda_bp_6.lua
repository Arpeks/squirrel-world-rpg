LinkLuaModifier( "modifier_pet_rda_bp_6", "items/pets/item_pet_rda_bp_6", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_6", "items/pets/item_pet_rda_bp_6", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_6 = class({})

function spell_item_pet_rda_bp_6:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_6", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_6:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_6"
end

modifier_item_pet_rda_bp_6 = class({})

function modifier_item_pet_rda_bp_6:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_6:IsPurgable()
	return false
end

if IsServer() then
	function modifier_item_pet_rda_bp_6:OnCreated( kv )
		local point = self:GetCaster():GetAbsOrigin()
		local interval = self:GetAbility():GetSpecialValueFor("turell")
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_6", point, true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
			if not self:GetParent():IsRangedAttacker() then
				return
			end
			local interval = self:GetAbility():GetSpecialValueFor("turell")
			-- self:StartIntervalThink(interval)
		end
	end
	function modifier_item_pet_rda_bp_6:OnRefresh()
		if not self:GetParent():IsRangedAttacker() then
			return
		end
		local interval = self:GetAbility():GetSpecialValueFor("turell")
		-- self:StartIntervalThink(interval)
	end

	function modifier_item_pet_rda_bp_6:OnIntervalThink()
		if self:GetParent():IsAlive() then
			local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetParent():Script_GetAttackRange() + 100,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
			if #units > 0 then
				self:GetParent():PerformAttack(units[1], true, false, true, false, true, false, true)
			end
		end
	end
	function modifier_item_pet_rda_bp_6:OnDestroy()
		UTIL_Remove(self.pet)
	end
end

function modifier_item_pet_rda_bp_6:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MODEL_SCALE_ANIMATE_TIME,
		MODIFIER_PROPERTY_MODEL_SCALE_USE_IN_OUT_EASE,

		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_6:GetModifierModelScaleAnimateTime()
	return 0
end

function modifier_item_pet_rda_bp_6:GetModifierModelScaleUseInOutEase()
	return false
end

function modifier_item_pet_rda_bp_6:GetModifierDamageOutgoing_Percentage()
	return self:GetAbility():GetSpecialValueFor("increased_outgoing_damage")
end

function modifier_item_pet_rda_bp_6:GetModifierIncomingDamage_Percentage()
	if self:GetParent():IsRangedAttacker() then
		return self:GetAbility():GetSpecialValueFor("increased_incoming_damage_range")
	end
	return self:GetAbility():GetSpecialValueFor("increased_incoming_damage_melee")
end


modifier_item_pet_rda_bp_6_split_damage_reduction = class({})

function modifier_item_pet_rda_bp_6_split_damage_reduction:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
	}
end

function modifier_item_pet_rda_bp_6_split_damage_reduction:GetModifierDamageOutgoing_Percentage()
	return -100 + self:GetAbility():GetSpecialValueFor("second_target_damage")
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_6 = class({})

function modifier_pet_rda_bp_6:IsHidden()
	return true
end

function modifier_pet_rda_bp_6:IsDebuff()
	return false
end

function modifier_pet_rda_bp_6:IsPurgable()
	return false
end

function modifier_pet_rda_bp_6:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_6:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_6:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
		MODIFIER_PROPERTY_MODEL_SCALE,
	}
	return funcs
end

function modifier_pet_rda_bp_6:GetModifierModelScale()
	return 50
end

function modifier_pet_rda_bp_6:GetModifierModelChange(params)
 	return "models/items/courier/babka_bewitcher_blue/babka_bewitcher_blue_flying.vmdl"
end

function modifier_pet_rda_bp_6:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_6:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_6:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_6", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_6:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_6", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end