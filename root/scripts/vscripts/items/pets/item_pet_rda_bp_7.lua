LinkLuaModifier( "modifier_pet_rda_bp_7", "items/pets/item_pet_rda_bp_7", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_7", "items/pets/item_pet_rda_bp_7", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_take_drop_gem", "modifiers/modifier_take_drop_gem", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_pet_rda_bp_7_stack", "items/pets/item_pet_rda_bp_7", LUA_MODIFIER_MOTION_NONE )

spell_item_pet_rda_bp_7 = class({})

function spell_item_pet_rda_bp_7:OnSpellStart()
	if IsServer() then
		self.caster = self:GetCaster()
		
		self.caster:AddNewModifier(
		self.caster,
		self,
		"modifier_pet_rda_bp_7", 
		{})
		EmitSoundOn( "Hero_Lion.Voodoo", self:GetCaster() )
	end
end

function spell_item_pet_rda_bp_7:GetIntrinsicModifierName()
	return "modifier_item_pet_rda_bp_7"
end

modifier_item_pet_rda_bp_7 = class({})

function modifier_item_pet_rda_bp_7:IsHidden()
	return false
end

function modifier_item_pet_rda_bp_7:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_7:IsPurgeException()
    return false
end

function modifier_item_pet_rda_bp_7:DestroyOnExpire()
    return false
end

if IsServer() then
	function modifier_item_pet_rda_bp_7:OnCreated( kv )
		if self:GetCaster():IsRealHero() then
			self.pet = CreateUnitByName("pet_rda_bp_7", self:GetCaster():GetAbsOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)
			self.pet:AddNewModifier(self:GetParent(),nil,"modifier_take_drop_gem",{})
			self.pet:SetControllableByPlayer(self:GetCaster():GetPlayerID(), true)
			self.pet:SetOwner(self:GetCaster())
		end
	end
	function modifier_item_pet_rda_bp_7:OnRefresh()
		if not self:GetParent():IsRangedAttacker() then
			return
		end
	end

	function modifier_item_pet_rda_bp_7:OnDestroy()
		UTIL_Remove(self.pet)
	end
end

function modifier_item_pet_rda_bp_7:DeclareFunctions()
	return {
		-- MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
		MODIFIER_PROPERTY_TOOLTIP,
	}
end

function modifier_item_pet_rda_bp_7:CustomOnAbilityFullyCast(keys)
	if IsServer() then
		if keys.unit ~= self:GetParent() then
			return 0
		end
		local add_stack_func = function()
			if self:GetStackCount() < self:GetAbility():GetSpecialValueFor("max_stacks") then
				self:SetStackCount(self:GetStackCount() + 1)
				local modifier_stack = self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_item_pet_rda_bp_7_stack", {duration = self:GetAbility():GetSpecialValueFor("stack_duration")})
				self:SetDuration(self:GetAbility():GetSpecialValueFor("stack_duration"), true)
			end
		end
		add_stack_func()
		local spell_cooldown = keys.ability:GetCooldown( keys.ability:GetLevel() )
		if spell_cooldown and spell_cooldown >= 10 then
			add_stack_func()
		end
		if spell_cooldown and spell_cooldown >= 20 then
			add_stack_func()
		end
		if spell_cooldown and spell_cooldown >= 30 then
			add_stack_func()
		end
		if keys.ability:GetAbilityType() and keys.ability:GetAbilityType() == DOTA_ABILITY_TYPE_ULTIMATE then
			add_stack_func()
			add_stack_func()
		end
	end
	return 0
end

function modifier_item_pet_rda_bp_7:OnTooltip()
	return self:GetAbility():GetSpecialValueFor("bonus_spell_damage_intelligence") + (self:GetStackCount() * self:GetAbility():GetSpecialValueFor("bonus_damage_per_stack"))
end

modifier_item_pet_rda_bp_7_stack = class({})

function modifier_item_pet_rda_bp_7_stack:IsHidden()
	return true
end

function modifier_item_pet_rda_bp_7_stack:IsPurgable()
	return false
end

function modifier_item_pet_rda_bp_7_stack:IsDebuff()
	return false
end

function modifier_item_pet_rda_bp_7_stack:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_item_pet_rda_bp_7_stack:OnRemoved()
	if IsServer() then
		local modifier_item_pet_rda_bp_7 = self:GetCaster():FindModifierByName("modifier_item_pet_rda_bp_7")
		modifier_item_pet_rda_bp_7:SetStackCount(modifier_item_pet_rda_bp_7:GetStackCount() - 1)
	end
end

function modifier_item_pet_rda_bp_7_stack:OnDestroy()
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
modifier_pet_rda_bp_7 = class({})

function modifier_pet_rda_bp_7:IsHidden()
	return self:GetStackCount() == 0
end

function modifier_pet_rda_bp_7:IsDebuff()
	return false
end

function modifier_pet_rda_bp_7:IsPurgable()
	return false
end

function modifier_pet_rda_bp_7:OnCreated( kv ) 
	self.caster = self:GetCaster()
	
	self.speed = self:GetAbility():GetSpecialValueFor( "speed" )

end

function modifier_pet_rda_bp_7:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}
	return state
end

function modifier_pet_rda_bp_7:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_EVENT_ON_ATTACK,
		MODIFIER_EVENT_ON_SPENT_MANA,
	}
	return funcs
end

function modifier_pet_rda_bp_7:GetModifierModelChange(params)
 	return "models/courier/trapjaw/trapjaw.vmdl"
end

function modifier_pet_rda_bp_7:GetModifierMoveSpeed_Absolute()
	return self.speed
end

function modifier_pet_rda_bp_7:GetModifierIncomingDamage_Percentage()
	return 300
end

function modifier_pet_rda_bp_7:CustomOnAttack( params )
	if IsServer() then
	if params.attacker~=self:GetParent() then return end
	if params.no_attack_cooldown then return end
	if self:GetParent().pet_locked then return end
	--if params.target:GetTeamNumber()==self:GetParent():GetTeamNumber() then return end

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_7", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end

function modifier_pet_rda_bp_7:OnSpentMana( params )
	if IsServer() then
	local ability = self:GetAbility()
	local parent = self:GetParent()

	local cost = params.cost
	local unit = params.unit
			
	if unit == parent then
	if self:GetParent().pet_locked then return end
	self.mana_loss = 0
	
    self.mana_loss = self.mana_loss + params.cost
	if self.mana_loss >= 10 then

	local modifier = self:GetParent():FindModifierByNameAndCaster( "modifier_pet_rda_bp_7", self:GetParent() )
	if not modifier then return end
	
	modifier:Destroy()
end
end
end
end