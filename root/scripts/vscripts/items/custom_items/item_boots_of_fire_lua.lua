LinkLuaModifier( "modifier_item_boots_of_fire_lua", "items/custom_items/item_boots_of_fire_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_boots_of_fire_lua_thinker", "items/custom_items/item_boots_of_fire_lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_boots_of_fire_lua == nil then
	item_boots_of_fire_lua = class({})
end

function item_boots_of_fire_lua:GetAbilityTextureName()
	if self:GetSecondaryCharges() == 0 then
		return "all/item_lava_boots"
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_lava_boots"
	end
end

function item_boots_of_fire_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

function item_boots_of_fire_lua:GetIntrinsicModifierName()
	return "modifier_item_boots_of_fire_lua"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_boots_of_fire_lua == nil then
	modifier_item_boots_of_fire_lua = class({})
end
function modifier_item_boots_of_fire_lua:IsHidden()
	return true
end
function modifier_item_boots_of_fire_lua:OnCreated(params)
	self.movement_speed = self:GetAbility():GetSpecialValueFor("movement_speed")
	if IsServer() then
		self:StartIntervalThink(self:GetAbility():GetSpecialValueFor("thinker_interval"))
	end
end
function modifier_item_boots_of_fire_lua:OnRefresh(params)
	self.movement_speed = self:GetAbility():GetSpecialValueFor("movement_speed")
	if IsServer() then
	end
end
function modifier_item_boots_of_fire_lua:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_boots_of_fire_lua:DeclareFunctions()
	return {
	}
end
function modifier_item_boots_of_fire_lua:OnIntervalThink()
	local position = self:GetParent():GetAbsOrigin()
	local duration = self:GetAbility():GetSpecialValueFor("thinker_duration")
	if self:GetParent():IsAlive() then
		CreateModifierThinker(
			self:GetParent(), -- player source
			self:GetAbility(), -- ability source
			"modifier_item_boots_of_fire_lua_thinker", -- modifier name
			{ duration = duration }, -- kv
			position,
			self:GetParent():GetTeamNumber(),
			false
		)
	end
end

function modifier_item_boots_of_fire_lua:DeclareFunctions()
	return{
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
	}
end

function modifier_item_boots_of_fire_lua:GetModifierMoveSpeedBonus_Constant()
	return self.movement_speed
end








modifier_item_boots_of_fire_lua_thinker = class({})

function modifier_item_boots_of_fire_lua_thinker:IsHidden()
	return self.thinker
end

function modifier_item_boots_of_fire_lua_thinker:IsDebuff()
	return self.thinker == false
end

function modifier_item_boots_of_fire_lua_thinker:OnCreated( kv )
	self.radius = self:GetAbility():GetSpecialValueFor( "burning_radius" )
	self.damage = self:GetAbility():GetSpecialValueFor( "burning_damage_base" ) + self:GetCaster():GetIntellect(true) / 100 * self:GetAbility():GetSpecialValueFor( "burning_damage_int" )
	self.burn_interval = self:GetAbility():GetSpecialValueFor( "burning_interval" )
	self.movement_slow = self:GetAbility():GetSpecialValueFor( "movement_slow" )

	self.thinker = kv.isProvidedByAura~=1

	if not IsServer() then return end

	if not self.thinker then
		self.damageTable = {
			victim = self:GetParent(),
			damage = self.damage * self.burn_interval,
			attacker = self:GetCaster(),
			damage_type = self:GetAbility():GetAbilityDamageType(),
			ability = self:GetAbility(),
		}

		self:StartIntervalThink( self.burn_interval )
	else
		self:PlayEffects()
	end
end

function modifier_item_boots_of_fire_lua_thinker:OnDestroy()
	if IsServer() then
		if self.thinker then
			ParticleManager:DestroyParticle( self.effect_cast, false )
			UTIL_Remove( self:GetParent() )
		end
	end
end

--------------------------------------------------------------------------------
function modifier_item_boots_of_fire_lua_thinker:OnIntervalThink()
	ApplyDamageRDA( self.damageTable )
end

function modifier_item_boots_of_fire_lua_thinker:PlayEffects()
	local particle_cast =  "particles/wk_arc_toast_ground_fire_11.vpcf"
	self.effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( self.effect_cast, 0, self:GetCaster():GetOrigin() )
end

function modifier_item_boots_of_fire_lua_thinker:GetAuraRadius()
    return self.radius
end

function modifier_item_boots_of_fire_lua_thinker:GetAuraDuration()
    return 0.5
end

function modifier_item_boots_of_fire_lua_thinker:IsAura()
    return self.thinker
end

function modifier_item_boots_of_fire_lua_thinker:GetModifierAura()
    return "modifier_item_boots_of_fire_lua_thinker"
end

function modifier_item_boots_of_fire_lua_thinker:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_item_boots_of_fire_lua_thinker:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_item_boots_of_fire_lua_thinker:DeclareFunctions()
	return{
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_item_boots_of_fire_lua_thinker:GetModifierMoveSpeedBonus_Percentage()
	return self.thinker and 0 or -self.movement_slow
end