LinkLuaModifier( "modifier_item_divinex_lua", "items/custom_items/item_divinex_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_divinex_debuff_lua", "items/custom_items/item_divinex_lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_divinex_lua == nil then
	item_divinex_lua = class({})
end
function item_divinex_lua:Precache(context)
	PrecacheResource("particle", "particles/items4_fx/nullifier_proj.vpcf", context)
	PrecacheResource("particle", "particles/items4_fx/nullifier_mute_debuff.vpcf", context)
end
function item_divinex_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/divinex_" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/divinex_" .. level
	end
end

function item_divinex_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if (self:GetItemSlot() > 5 or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end
function item_divinex_lua:GetIntrinsicModifierName()
	return "modifier_item_divinex_lua"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_divinex_lua == nil then
	modifier_item_divinex_lua = class({})
end
function modifier_item_divinex_lua:IsHidden()
	return true
end
function modifier_item_divinex_lua:OnCreated(params)
	self.debuff_duration = self:GetAbility():GetSpecialValueFor("debuff_duration")
	self.debuff_crit_damage = self:GetAbility():GetSpecialValueFor("debuff_crit_damage")
	self.debuff_crit_chance = self:GetAbility():GetSpecialValueFor("debuff_crit_chance")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_agility = self:GetAbility():GetSpecialValueFor("bonus_agility")
	self.bonus_attack_speed = self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end
function modifier_item_divinex_lua:OnRefresh(params)
	self.debuff_duration = self:GetAbility():GetSpecialValueFor("debuff_duration")
	self.debuff_crit_damage = self:GetAbility():GetSpecialValueFor("debuff_crit_damage")
	self.debuff_crit_chance = self:GetAbility():GetSpecialValueFor("debuff_crit_chance")
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_agility = self:GetAbility():GetSpecialValueFor("bonus_agility")
	self.bonus_attack_speed = self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end
function modifier_item_divinex_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
	}
end
function modifier_item_divinex_lua:GetModifierBonusStats_Agility()
	return self.bonus_agility
end
function modifier_item_divinex_lua:GetModifierAttackSpeedBonus_Constant()
	return self.bonus_attack_speed
end
function modifier_item_divinex_lua:GetModifierPreAttack_BonusDamage()
	return self.bonus_damage
end
function modifier_item_divinex_lua:CustomOnAttackLanded(params)
	if params.attacker == self:GetParent() and self:GetAbility():IsCooldownReady() and not params.no_attack_cooldown then
		local divinex_debuff = params.target:FindModifierByNameAndCaster("modifier_item_divinex_debuff_lua", params.attacker)
		if divinex_debuff then
			divinex_debuff:Destroy()
		end
		self.modifier_debuff = params.target:AddNewModifier(params.attacker, self:GetAbility(), "modifier_item_divinex_debuff_lua", {duration = self.debuff_duration})
		params.target:EmitSound("DOTA_Item.Nullifier.Target")
		params.attacker:PerformAttack(params.target, true, true, true, true, false, false, true)
		self:GetAbility():UseResources(true, true, false, true)
		self.target = params.target
	end
end

function modifier_item_divinex_lua:GetModifierPreAttack_CriticalStrike(params)
	if self.target and params.target == self.target then
		if self.target:FindModifierByNameAndCaster("modifier_item_divinex_debuff_lua", params.attacker) then
			if RandomFloat(0, 100) <= self.debuff_crit_chance then
				return self.debuff_crit_damage
			end
		else
			self.target = nil
		end
	end
	return 0
end

modifier_item_divinex_debuff_lua = class({})
function modifier_item_divinex_debuff_lua:IsPurgable()
	return true
end
function modifier_item_divinex_debuff_lua:IsDebuff()
	return true
end
function modifier_item_divinex_debuff_lua:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end
function modifier_item_divinex_debuff_lua:OnCreated( kv )
	if IsServer() then
		self.effect_cast = ParticleManager:CreateParticleForPlayer( "particles/items4_fx/nullifier_mute_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent(), self:GetCaster() )
	end
end
function modifier_item_divinex_debuff_lua:OnDestroy()
	if IsServer() then
		ParticleManager:DestroyParticle( self.effect_cast, false )
		ParticleManager:ReleaseParticleIndex( self.effect_cast )
	end
end