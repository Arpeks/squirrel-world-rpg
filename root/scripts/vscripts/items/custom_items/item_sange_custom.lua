LinkLuaModifier( "modifier_item_sange_custom", "items/custom_items/item_sange_custom", LUA_MODIFIER_MOTION_NONE )

item_sange_custom = class({})

function item_sange_custom:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/item_sange" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_sange" .. level
	end
end

function item_sange_custom:GetIntrinsicModifierName()
return "modifier_item_sange_custom"
end

modifier_item_sange_custom = class({})

function modifier_item_sange_custom:IsHidden()

end

function modifier_item_sange_custom:DeclareFunctions()
return {
MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
MODIFIER_PROPERTY_HP_REGEN_AMPLIFY_PERCENTAGE,
}
end

function modifier_item_sange_custom:GetModifierBonusStats_Strength()
return self:GetAbility():GetSpecialValueFor("str_bonys")
end

function modifier_item_sange_custom:GetModifierPhysicalArmorBonus()
return self:GetAbility():GetSpecialValueFor("armor_bonus")
end

function modifier_item_sange_custom:GetModifierHPRegenAmplify_Percentage()
return self:GetAbility():GetSpecialValueFor("hpreg_bonus")
end