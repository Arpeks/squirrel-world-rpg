LinkLuaModifier( "modifier_item_bladeforge_gauntlet_lua", "items/custom_items/item_bladeforge_gauntlet_lua.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_item_bladeforge_gauntlet_active", "items/custom_items/item_bladeforge_gauntlet_lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_bladeforge_gauntlet_lua == nil then
	item_bladeforge_gauntlet_lua = class({})
end
function item_bladeforge_gauntlet_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/item_bladeforge_gauntlet_lua" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_bladeforge_gauntlet_lua" .. level
	end
end

function item_bladeforge_gauntlet_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end
function item_bladeforge_gauntlet_lua:GetIntrinsicModifierName()
	return "modifier_item_bladeforge_gauntlet_lua"
end
function item_bladeforge_gauntlet_lua:OnSpellStart()
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_item_bladeforge_gauntlet_active", {duration = self:GetSpecialValueFor("duration")})
	self:GetCaster():EmitSound("DOTA_Item.MaskOfMadness.Activate")
end
function item_bladeforge_gauntlet_lua:GetCooldown(iLevel)
	return self:GetSpecialValueFor("cooldown")
end
function item_bladeforge_gauntlet_lua:GetManaCost(iLevel)
	return 0
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_bladeforge_gauntlet_lua == nil then
	modifier_item_bladeforge_gauntlet_lua = class({})
end
function modifier_item_bladeforge_gauntlet_lua:OnCreated(params)
	self.parent = self:GetParent()
	self:OnRefresh()
	if self.parent:HasModifier("modifier_item_greater_crit_lua") then
		self:Destroy()
	end
end
function modifier_item_bladeforge_gauntlet_lua:OnRefresh(params)
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_attack_range_melee = self:GetAbility():GetSpecialValueFor("bonus_attack_range_melee")
	self.crit_chance_active = self:GetAbility():GetSpecialValueFor("crit_chance_active")
	self.crit_chance_passive = self:GetAbility():GetSpecialValueFor("crit_chance_passive")
	self.crit_multiplier_active = self:GetAbility():GetSpecialValueFor("crit_multiplier_active")
	self.crit_multiplier_passive = self:GetAbility():GetSpecialValueFor("crit_multiplier_passive")

	self.cleave_distance = self:GetAbility():GetSpecialValueFor("cleave_distance")
	self.cleave_chance = self:GetAbility():GetSpecialValueFor("cleave_chance")
	self.cleave_damage_percent = self:GetAbility():GetSpecialValueFor("cleave_damage_percent")
	self.cleave_starting_width = self:GetAbility():GetSpecialValueFor("cleave_starting_width")
	self.cleave_ending_width = self:GetAbility():GetSpecialValueFor("cleave_ending_width")
end
function modifier_item_bladeforge_gauntlet_lua:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_bladeforge_gauntlet_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_PREATTACK_CRITICALSTRIKE,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED,
		MODIFIER_PROPERTY_ATTACK_RANGE_BONUS,
	}
end

function modifier_item_bladeforge_gauntlet_lua:GetModifierPreAttack_BonusDamage()
	return self.bonus_damage
end

function modifier_item_bladeforge_gauntlet_lua:GetModifierAttackRangeBonus()
	if not self:GetParent():IsRangedAttacker() then
		return self.bonus_attack_range_melee
	end
	return 0
end

function modifier_item_bladeforge_gauntlet_lua:GetModifierPreAttack_CriticalStrike(keys)
	if (keys.target and not keys.target:IsOther() and not keys.target:IsBuilding() and keys.target:GetTeamNumber() ~= self.parent:GetTeamNumber()) then
		local chance = self.parent:HasModifier("modifier_item_bladeforge_gauntlet_active") and self.crit_chance_active or self.crit_chance_passive
		if RandomFloat(0, 100) <= chance then
			local multiplier = self.parent:HasModifier("modifier_item_bladeforge_gauntlet_active") and self.crit_multiplier_active or self.crit_multiplier_passive
			return multiplier
		end
	end
end

function modifier_item_bladeforge_gauntlet_lua:CustomOnAttackLanded(keys)
	if keys.attacker == self.parent then
		if ( not self.parent:IsIllusion() ) and not self.parent:IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self.parent:GetTeamNumber() and RandomInt(1, 100) <= self.cleave_chance then
				local direction = keys.target:GetOrigin()-self.parent:GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self.parent:GetOrigin() + direction*self.cleave_distance/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,self.cleave_distance,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (self.cleave_damage_percent/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )
			end
		end
	end
end

modifier_item_bladeforge_gauntlet_active = class({})

function modifier_item_bladeforge_gauntlet_active:IsHidden()
	return false
end

function modifier_item_bladeforge_gauntlet_active:IsPurgable()
	return false
end