LinkLuaModifier("modifier_item_magic_crit_lua", "items/custom_items/item_magic_crit_lua.lua", LUA_MODIFIER_MOTION_NONE)

item_magic_crit_lua = class({})

function item_magic_crit_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/mystic" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/mystic" .. level
	end
end

function item_magic_crit_lua:GetIntrinsicModifierName()
	return "modifier_item_magic_crit_lua"
end

function item_magic_crit_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

modifier_item_magic_crit_lua = class({})

function modifier_item_magic_crit_lua:IsHidden() return true end

function modifier_item_magic_crit_lua:OnCreated(kv)
	self.bonus_intellect = self:GetAbility():GetSpecialValueFor("bonus_intellect")
	self.bonus_attack_speed = self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end

function modifier_item_magic_crit_lua:OnRefresh(kv)
	self.bonus_intellect = self:GetAbility():GetSpecialValueFor("bonus_intellect")
	self.bonus_attack_speed = self:GetAbility():GetSpecialValueFor("bonus_attack_speed")
end

function modifier_item_magic_crit_lua:DeclareFunctions() 
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	}
end

function modifier_item_magic_crit_lua:GetModifierBonusStats_Intellect()
	return self.bonus_intellect
end

function modifier_item_magic_crit_lua:GetModifierAttackSpeedBonus_Constant()
	return self.bonus_attack_speed
end