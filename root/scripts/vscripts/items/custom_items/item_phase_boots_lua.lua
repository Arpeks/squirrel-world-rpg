if item_phase_boots_lua == nil then
	item_phase_boots_lua = class({})
end

LinkLuaModifier("modifier_item_phase_boots_lua","items/custom_items/item_phase_boots_lua",LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_phase_boots_three_active","items/custom_items/item_phase_boots_lua",LUA_MODIFIER_MOTION_NONE)

function item_phase_boots_lua:GetIntrinsicModifierName(  )
	return "modifier_item_phase_boots_lua"
end

function item_phase_boots_lua:OnSpellStart()
    print("on spell start")
	self:GetCaster():EmitSound("DOTA_Item.PhaseBoots.Activate")
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_phase_boots_three_active",{duration = self:GetSpecialValueFor("phase_duration")})
end

--------------------------------

if modifier_item_phase_boots_lua == nil then
	modifier_item_phase_boots_lua = class({})
end

function modifier_item_phase_boots_lua:IsPurgable(  )
	return false
end

function modifier_item_phase_boots_lua:GetAttributes(  )
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_item_phase_boots_lua:OnCreated(  )
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_movement_speed = self:GetAbility():GetSpecialValueFor("bonus_movement_speed")
end

function modifier_item_phase_boots_lua:OnRefresh(  )
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_movement_speed = self:GetAbility():GetSpecialValueFor("bonus_movement_speed")
end

function modifier_item_phase_boots_lua:IsHidden(  )
	return true
end

function modifier_item_phase_boots_lua:DeclareFunctions(  )
	local funcs = { 
        MODIFIER_PROPERTY_MOVESPEED_BONUS_UNIQUE, 
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
    }
	return funcs
end

function modifier_item_phase_boots_lua:GetModifierPreAttack_BonusDamage(  )
	return self.bonus_damage
end

function modifier_item_phase_boots_lua:GetModifierMoveSpeedBonus_Special_Boots(  )
	return self.bonus_movement_speed
end

-------------------------------

if modifier_phase_boots_three_active == nil then
	modifier_phase_boots_three_active = class({})
end

function modifier_phase_boots_three_active:DeclareFunctions(  )
	local funcs = { 
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE_UNIQUE 
    }
	return funcs 
end

function modifier_phase_boots_three_active:OnCreated(  )
	local caster = self:GetParent()
	self.phase_movement_speed = self:GetAbility():GetSpecialValueFor("phase_movement_speed")
end

function modifier_phase_boots_three_active:CheckState()
	local states = { [MODIFIER_STATE_NO_UNIT_COLLISION] = true }
	return states
end

function modifier_phase_boots_three_active:OnDestroy(  )
	local caster = self:GetParent()
	if IsServer() then FindClearSpaceForUnit(caster, caster:GetAbsOrigin(), true) end
end

function modifier_phase_boots_three_active:GetTexture(  )
	return "item_item_phase_boots_lua"
end

function modifier_phase_boots_three_active:GetEffectName(  )
	return "particles/econ/events/ti6/phase_boots_ti6.vpcf"
end

function modifier_phase_boots_three_active:GetModifierMoveSpeedBonus_Percentage_Unique(  )
	return self.phase_movement_speed
end