item_spectre_hood_lua = class({})

function item_spectre_hood_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/item_spectre_hood_lua" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_spectre_hood_lua" .. level
	end
end

function item_spectre_hood_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

function item_spectre_hood_lua:GetIntrinsicModifierName()
	return "modifier_item_spectre_hood_lua"
end

function item_spectre_hood_lua:DropItemChanceBonus()
    return self:GetSpecialValueFor("bonus_drop_chance")
end

LinkLuaModifier("modifier_item_spectre_hood_lua", 'items/custom_items/item_spectre_hood_lua', LUA_MODIFIER_MOTION_NONE)

modifier_item_spectre_hood_lua = class({})

function modifier_item_spectre_hood_lua:IsHidden()
    return true
end

function modifier_item_spectre_hood_lua:IsPurgable()
    return false
end

function modifier_item_spectre_hood_lua:OnCreated( kv )
    self.parent                     = self:GetParent()
    self.ability                    = self:GetAbility()
    self.bonus_main_attribute       = self.ability:GetSpecialValueFor("bonus_main_attribute")
    self.bonus_damage               = self.ability:GetSpecialValueFor("bonus_damage")
    self.bonus_mana_regen           = self.ability:GetSpecialValueFor("bonus_mana_regen")
    self.delay_until_invisibility   = self.ability:GetSpecialValueFor("delay_until_invisibility")
    self.bonus_drop_chance          = self.ability:GetSpecialValueFor("bonus_drop_chance")
    if IsServer() then
        self.primary_attribute      = self.parent:GetPrimaryAttribute()
        self:SetHasCustomTransmitterData( true )
        self:StartIntervalThink(self.delay_until_invisibility)
        self.ability:StartCooldown(self.delay_until_invisibility)
    end
end

function modifier_item_spectre_hood_lua:OnRefresh( kv )
    self.parent                     = self:GetParent()
    self.ability                    = self:GetAbility()
    self.bonus_main_attribute       = self.ability:GetSpecialValueFor("bonus_main_attribute")
    self.bonus_damage               = self.ability:GetSpecialValueFor("bonus_damage")
    self.bonus_mana_regen           = self.ability:GetSpecialValueFor("bonus_mana_regen")
    self.delay_until_invisibility   = self.ability:GetSpecialValueFor("delay_until_invisibility")
    self.bonus_drop_chance          = self.ability:GetSpecialValueFor("bonus_drop_chance")
    if IsServer() then
        self.primary_attribute      = self.parent:GetPrimaryAttribute()
        self:SendBuffRefreshToClients()
        self:StartIntervalThink(self.delay_until_invisibility)
        self.ability:StartCooldown(self.delay_until_invisibility)
    end
end

function modifier_item_spectre_hood_lua:OnDestroy()
    if IsServer() then
        self:RemoveInvisibility()
    end
end

function modifier_item_spectre_hood_lua:AddCustomTransmitterData()
	return {
		primary_attribute = self.primary_attribute,
	}
end

function modifier_item_spectre_hood_lua:HandleCustomTransmitterData( data )
	self.primary_attribute = data.primary_attribute
end

function modifier_item_spectre_hood_lua:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
        -- MODIFIER_EVENT_ON_ATTACK,
        MODIFIER_EVENT_ON_UNIT_MOVED,
        -- MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
    }
end

function modifier_item_spectre_hood_lua:GetModifierBonusStats_Intellect()
    if self.primary_attribute == DOTA_ATTRIBUTE_ALL then 
        return self.bonus_main_attribute * 0.5 end
    return self.primary_attribute == DOTA_ATTRIBUTE_INTELLECT and self.bonus_main_attribute or 0
end

function modifier_item_spectre_hood_lua:GetModifierBonusStats_Agility()
    if self.primary_attribute == DOTA_ATTRIBUTE_ALL then 
        return self.bonus_main_attribute * 0.5 end
    return self.primary_attribute == DOTA_ATTRIBUTE_AGILITY and self.bonus_main_attribute or 0
end

function modifier_item_spectre_hood_lua:GetModifierBonusStats_Strength()
    if self.primary_attribute == DOTA_ATTRIBUTE_ALL then 
        return self.bonus_main_attribute * 0.5 end
    return self.primary_attribute == DOTA_ATTRIBUTE_STRENGTH and self.bonus_main_attribute or 0
end

function modifier_item_spectre_hood_lua:GetModifierPreAttack_BonusDamage()
    return self.bonus_damage
end

function modifier_item_spectre_hood_lua:GetModifierConstantManaRegen()
    return self.bonus_mana_regen
end

function modifier_item_spectre_hood_lua:CustomOnAttack( params )
    if params.attacker == self.parent then
        self:RemoveInvisibility()
    end
end

function modifier_item_spectre_hood_lua:OnUnitMoved( params )
    if params.unit == self.parent then
        self:RemoveInvisibility()
    end
end

function modifier_item_spectre_hood_lua:CustomOnAbilityFullyCast( params )
    if params.unit == self.parent then
        self:RemoveInvisibility()
    end
end

function modifier_item_spectre_hood_lua:ApplyInvisibility()
    self.invisModifier = self.parent:AddNewModifier(self.parent, self.ability, "modifier_item_spectre_hood_invisibility", {})
    self:StartIntervalThink(-1)
end

function modifier_item_spectre_hood_lua:RemoveInvisibility()
    if self.invisModifier then
        self.invisModifier:Destroy()
        self.invisModifier = nil
    end
    self:StartIntervalThink(self.delay_until_invisibility)
    self.ability:StartCooldown(self.delay_until_invisibility)
end

function modifier_item_spectre_hood_lua:OnIntervalThink()
    self:ApplyInvisibility()
end

LinkLuaModifier("modifier_item_spectre_hood_invisibility", 'items/custom_items/item_spectre_hood_lua', LUA_MODIFIER_MOTION_NONE)

modifier_item_spectre_hood_invisibility = class({})

function modifier_item_spectre_hood_invisibility:IsHidden()
    return false
end

function modifier_item_spectre_hood_invisibility:IsPurgable()
    return false
end

function modifier_item_spectre_hood_invisibility:CheckState()
	return {
		[MODIFIER_STATE_INVISIBLE] = true,
	}
end

function modifier_item_spectre_hood_invisibility:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
	}
end

function modifier_item_spectre_hood_invisibility:GetModifierInvisibilityLevel()
	return 1
end