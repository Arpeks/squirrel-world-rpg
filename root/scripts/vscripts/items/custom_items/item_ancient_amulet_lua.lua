LinkLuaModifier("modifier_item_ancient_amulet_lua", "items/custom_items/item_ancient_amulet_lua.lua", LUA_MODIFIER_MOTION_NONE)

item_ancient_amulet_lua = class({})

-- function item_ancient_amulet_lua:GetAbilityTextureName()
-- 	if self:GetToggleState() then
-- 		return "all/dagon_off"
-- 	end
-- 	local level = self:GetLevel()
-- 	if self:GetSecondaryCharges() == 0 then
-- 		return "all/dagon" .. level
-- 	else
-- 		return "gem" .. self:GetSecondaryCharges() .. "/dagon" .. level
-- 	end
-- end

function item_ancient_amulet_lua:GetIntrinsicModifierName()
	return "modifier_item_ancient_amulet_lua"
end

-- function item_ancient_amulet_lua:OnUpgrade()
-- 	Timers:CreateTimer(0.03,function()
-- 		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
-- 			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
-- 			if m then
-- 				m:Destroy()
-- 			end
-- 		end
-- 	end)
-- end

modifier_item_ancient_amulet_lua = class({})

function modifier_item_ancient_amulet_lua:IsHidden() return true end

function modifier_item_ancient_amulet_lua:DeclareFunctions() 
	return {
        MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE,
	} 
end

function modifier_item_ancient_amulet_lua:GetModifierBaseAttack_BonusDamage()
	return self:GetAbility():GetCurrentCharges() or 0
end

function modifier_item_ancient_amulet_lua:OnCreated(kv)
	if not IsServer() then return end
	self:StartIntervalThink(1)
    self:OnIntervalThink()
end

function modifier_item_ancient_amulet_lua:OnIntervalThink()
    local attack_interval = 1 / self:GetCaster():GetAttacksPerSecond(false)
    self:GetAbility():SetCurrentCharges(self:GetAbility():GetSpecialValueFor("bonus_damage_interval") * attack_interval)
end