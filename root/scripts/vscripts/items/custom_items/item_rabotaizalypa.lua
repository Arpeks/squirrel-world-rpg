LinkLuaModifier( "modifier_item_rabotaizalypa", "items/custom_items/item_rabotaizalypa", LUA_MODIFIER_MOTION_NONE )

item_rabotaizalypa = class({})

function item_rabotaizalypa:GetIntrinsicModifierName()
return "modifier_item_rabotaizalypa"
end

modifier_item_rabotaizalypa = class({})

function modifier_item_rabotaizalypa:IsHidden()

end

function modifier_item_rabotaizalypa:DeclareFunctions()
return {
MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
}
end

function modifier_item_rabotaizalypa:GetModifierPhysicalArmorBonus()
return self:GetAbility():GetSpecialValueFor("arm_bonus")
end

function modifier_item_rabotaizalypa:GetModifierMagicalResistanceBonus()
return self:GetAbility():GetSpecialValueFor("magr_bonus")
end

function modifier_item_rabotaizalypa:GetModifierBonusStats_Strength()
return self:GetAbility():GetSpecialValueFor("st_bonus")
end