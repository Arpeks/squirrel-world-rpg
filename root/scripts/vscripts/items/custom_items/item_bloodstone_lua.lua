item_bloodstone_lua = class({})

LinkLuaModifier("modifier_item_bloodstone_lua", 'items/custom_items/item_bloodstone_lua.lua', LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_item_blodstone_active_lua", 'items/custom_items/item_bloodstone_lua.lua', LUA_MODIFIER_MOTION_NONE)

function item_bloodstone_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/bloodstone_" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_bloodstone_lua" .. level
	end
end

function item_bloodstone_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

function item_bloodstone_lua:GetIntrinsicModifierName()
	return "modifier_item_bloodstone_lua"
end

function item_bloodstone_lua:GetManaCost()
	return 0
	-- if self and not self:IsNull() and self.GetCaster and self:GetCaster() ~= nil then
	-- 	return self:GetCaster():GetMaxMana() * 0.3
	-- end
end

function item_bloodstone_lua:OnSpellStart()
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_item_blodstone_active_lua", {duration = self:GetSpecialValueFor("buff_duration")})
end

function item_bloodstone_lua:OnKill()
    if not self.kills then self.kills = 0 end
    self.kills = self.kills + 1
    self.charges = math.min(self.kills * self:GetSpecialValueFor("mana_regen_per_kill"), self:GetSpecialValueFor("max_mana_regen_kill")) 
    self:SetCurrentCharges(self.charges)
end

----------------------------------------------------------------------------------

modifier_item_bloodstone_lua = class({})

function modifier_item_bloodstone_lua:IsHidden()
	return true
end

function modifier_item_bloodstone_lua:IsPurgable()
	return false
end

function modifier_item_bloodstone_lua:RemoveOnDeath()	
	return false 
end

function modifier_item_bloodstone_lua:OnCreated()
	self.parent = self:GetParent()

	self.spell_amp = self:GetAbility():GetSpecialValueFor("spell_amp") 
	self.bonus_aoe = self:GetAbility():GetSpecialValueFor("bonus_aoe")
	self.bonus_health = self:GetAbility():GetSpecialValueFor("bonus_health")
	self.bonus_mana = self:GetAbility():GetSpecialValueFor("bonus_mana")
	self.bonus_mp_regen = self:GetAbility():GetSpecialValueFor("bonus_mp_regen")
	self.spell_lifesteal = self:GetAbility():GetSpecialValueFor("spell_lifesteal")
	self.lifesteal_multiplier = self:GetAbility():GetSpecialValueFor("lifesteal_multiplier")

	RegisterSpecialValuesModifier(self)
end

function modifier_item_bloodstone_lua:OnRefresh()
	self.spell_amp = self:GetAbility():GetSpecialValueFor("spell_amp")
	self.bonus_aoe = self:GetAbility():GetSpecialValueFor("bonus_aoe")
	self.bonus_health = self:GetAbility():GetSpecialValueFor("bonus_health")
	self.bonus_mana = self:GetAbility():GetSpecialValueFor("bonus_mana")
	self.bonus_mp_regen = self:GetAbility():GetSpecialValueFor("bonus_mp_regen")
	self.spell_lifesteal = self:GetAbility():GetSpecialValueFor("spell_lifesteal")
	self.lifesteal_multiplier = self:GetAbility():GetSpecialValueFor("lifesteal_multiplier")
end

function modifier_item_bloodstone_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_HEALTH_BONUS,
		MODIFIER_PROPERTY_MANA_BONUS,
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE,
		-- MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
	}
end

function modifier_item_bloodstone_lua:GetModifierHealthBonus()
	return self.bonus_health
end

function modifier_item_bloodstone_lua:GetModifierManaBonus()
	return self.bonus_mana
end

function modifier_item_bloodstone_lua:GetModifierSpellAmplify_Percentage()
	return self.spell_amp
end

function modifier_item_bloodstone_lua:GetModifierConstantManaRegen()
	return self.bonus_mp_regen + self:GetAbility():GetCurrentCharges()
end

function modifier_item_bloodstone_lua:CustomOnTakeDamage(keys)
	if keys.attacker == self:GetParent() and not keys.unit:IsBuilding() and not keys.unit:IsOther() then		
		if self:GetParent():FindAllModifiersByName(self:GetName())[1] == self and keys.damage_category == 0 and keys.inflictor and bit.band(keys.damage_flags, DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL) ~= DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL then
		
			-- Particle effect
			self.lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker)
			ParticleManager:SetParticleControl(self.lifesteal_pfx, 0, keys.attacker:GetAbsOrigin())
			ParticleManager:ReleaseParticleIndex(self.lifesteal_pfx)
			local lifesteal = self.spell_lifesteal
			if self.parent:HasModifier("modifier_item_blodstone_active_lua") then
				lifesteal = lifesteal * self.lifesteal_multiplier
			end
			heal = keys.damage * (lifesteal / 100)
			keys.attacker:HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), true)
			if self:GetParent():HasModifier("modifier_wisp_tether_lua") then
				local modifier = self:GetParent():FindModifierByName("modifier_wisp_tether_lua")
				modifier.heal = math.min(modifier.heal + heal, 2^30)
			end
		end
	end
end

function modifier_item_bloodstone_lua:CustomOnDeath(params)
    if params.unit:GetTeamNumber() ~= DOTA_TEAM_BADGUYS then
        return false
    end
    if params.unit:GetTeamNumber() == DOTA_TEAM_BADGUYS and params.attacker == self.parent then
        self:GetAbility():OnKill()
    end
end

function modifier_item_bloodstone_lua:GetAbilitySpecialValueBonus(keys)
	if (not keys.ability) or (not keys.ability_special_value) then return 0 end
	local ability_name = keys.ability.GetAbilityName and keys.ability:GetAbilityName()
	local kv = GetAbilityKV(ability_name)

	if kv["AbilityValues"] and kv["AbilityValues"][keys.ability_special_value] then
		if kv["AbilityValues"][keys.ability_special_value]["affected_by_aoe_increase"] and tonumber(kv["AbilityValues"][keys.ability_special_value]["affected_by_aoe_increase"]) == 1 then
			if kv["AbilityValues"][keys.ability_special_value]["value"] and (tonumber(kv["AbilityValues"][keys.ability_special_value]["value"]) == 0) then
				return 0
			end
			
			return self.bonus_aoe
		end
	end

end

-----------------------------------------------------------------------

modifier_item_blodstone_active_lua = class({})

function modifier_item_blodstone_active_lua:OnCreated()
	EmitSoundOn("DOTA_Item.Bloodstone.Cast",self:GetCaster())
	self.particle = ParticleManager:CreateParticle("particles/items_fx/bloodstone_heal.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControlEnt(self.particle, 2, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
	self:AddParticle(self.particle, false, false, -1, false, false)
end