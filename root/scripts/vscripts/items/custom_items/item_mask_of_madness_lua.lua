item_mask_of_madness_lua = class({})
LinkLuaModifier("modifier_mask_of_madness_lua", "items/custom_items/item_mask_of_madness_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_mask_of_madness_lua_berserk", "items/custom_items/item_mask_of_madness_lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_mask_of_madness_lua_rage", "items/custom_items/item_mask_of_madness_lua", LUA_MODIFIER_MOTION_NONE)

function item_mask_of_madness_lua:GetAbilityTextureName()
	return "item_mask_of_madness"
end

function item_mask_of_madness_lua:GetIntrinsicModifierName()
	return "modifier_mask_of_madness_lua"
end

function item_mask_of_madness_lua:OnSpellStart()
	-- Play cast sound
	EmitSoundOn("DOTA_Item.MaskOfMadness.Activate", self:GetCaster())

	-- Berserk!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_mask_of_madness_lua_berserk", {duration = self:GetSpecialValueFor("berserk_duration")})
end

-- Passive MoM modifier
modifier_mask_of_madness_lua = class({})

function modifier_mask_of_madness_lua:IsHidden()		return true end
function modifier_mask_of_madness_lua:IsPurgable()		return false end
function modifier_mask_of_madness_lua:RemoveOnDeath()	return false end
function modifier_mask_of_madness_lua:GetAttributes()	return MODIFIER_ATTRIBUTE_MULTIPLE end

function modifier_mask_of_madness_lua:OnCreated()	
	if IsServer() then
        if not self:GetAbility() then self:Destroy() end
    end

	-- Ability properties
	self.caster = self:GetCaster()
	self.ability = self:GetAbility()
	--self.modifier_rage = "modifier_mask_of_madness_lua_rage"

	-- Ability specials
	-- self.rage_damage_bonus = self.ability:GetSpecialValueFor("rage_damage_bonus")
	-- self.rage_lifesteal_bonus_pct = self.ability:GetSpecialValueFor("rage_lifesteal_bonus_pct")
 
	if self:GetAbility() and IsServer() then
		-- Change to lifesteal projectile, if there's nothing "stronger"
		-- ChangeAttackProjectileImba(self.caster)
	end
end

function modifier_mask_of_madness_lua:OnDestroy()
	if IsServer() then
		-- If it is the last MoM in inventory, remove unique modiifer
		if not self.caster:IsNull() and not self.caster:HasModifier("modifier_mask_of_madness_lua") then
			-- Remove lifesteal projectile
			-- ChangeAttackProjectileImba(self.caster)
		end
	end
end

function modifier_mask_of_madness_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        -- MODIFIER_EVENT_ON_ATTACK_LANDED
	}
end

function modifier_mask_of_madness_lua:GetModifierPreAttack_BonusDamage()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("damage_bonus")
	end

	-- Check for rage!!
	-- if self.caster:HasModifier(self.modifier_rage) then
	--     damage_bonus = damage_bonus + self.rage_damage_bonus
	-- end
end

function modifier_mask_of_madness_lua:GetModifierAttackSpeedBonus_Constant()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("attack_speed_bonus")
	end
end

function modifier_mask_of_madness_lua:GetModifierLifesteal()
	if self:GetAbility() and self:GetParent():FindAllModifiersByName(self:GetName())[1] == self then
		return self:GetAbility():GetSpecialValueFor("lifesteal_pct")
	end
end

function modifier_mask_of_madness_lua:CustomOnAttackLanded( params )	
	if self:GetParent() ~= params.attacker then
		return
	end
	local heal = params.damage * self:GetAbility():GetSpecialValueFor("lifesteal_pct")/100
	self:GetParent():HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), false)
	self:PlayEffects( self:GetParent() )
	if self:GetParent():HasModifier("modifier_wisp_tether_lua") then
		local modifier = self:GetParent():FindModifierByName("modifier_wisp_tether_lua")
		modifier.heal = modifier.heal + heal
	end
end

function modifier_mask_of_madness_lua:PlayEffects( target )
	local particle_cast = "particles/units/heroes/hero_skeletonking/wraith_king_vampiric_aura_lifesteal.vpcf"
	local effect_cast = ParticleManager:CreateParticle( particle_cast, PATTACH_ABSORIGIN_FOLLOW, target )
	ParticleManager:SetParticleControl( effect_cast, 1, target:GetOrigin() )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end
-- Berserk modifier
modifier_mask_of_madness_lua_berserk = class({})

function modifier_mask_of_madness_lua_berserk:OnCreated()
	if IsServer() then
        if not self:GetAbility() then self:Destroy() end
    end

	-- Ability properties
	self.caster = self:GetCaster()
	self.ability = self:GetAbility()
	-- self.modifier_rage = "modifier_mask_of_madness_lua_rage"
	-- self.sound_rage = "Hero_Clinkz.Strafe"

	-- Ability specials
	self.berserk_attack_speed = self.ability:GetSpecialValueFor("berserk_attack_speed")
	self.berserk_ms_bonus_pct = self.ability:GetSpecialValueFor("berserk_ms_bonus_pct")
	self.berserk_armor_reduction = self.ability:GetSpecialValueFor("berserk_armor_reduction")
	-- self.rage_ms_bonus_pct = self.ability:GetSpecialValueFor("rage_ms_bonus_pct")
	-- self.rage_max_distance = self.ability:GetSpecialValueFor("rage_max_distance")

	-- if IsServer() then
	--     self:StartIntervalThink(0.2)
	-- end
end

function modifier_mask_of_madness_lua_berserk:OnIntervalThink()
	if IsServer() then
	-- -- If caster isn't raging, do nothing
	-- if not self.caster:HasModifier(self.modifier_rage) then
	--     return nil
	-- end

	-- -- Check if rage target is dead - if he is, remove rage modifier and go out
	-- if not self.rage_target or not self.rage_target:IsAlive() then
	--     self.caster:RemoveModifierByName(self.modifier_rage)
	--     return nil
	-- end

	-- -- If the target cannot be seen by the caster, remove rage modifier
	-- if not self.caster:CanEntityBeSeenByMyTeam(self.rage_target) then
	--     self.caster:RemoveModifierByName(self.modifier_rage)
	--     return nil
	-- end

	-- -- Check distance between the caster and his rage target
	-- local distance = (self.caster:GetAbsOrigin() - self.rage_target:GetAbsOrigin()):Length2D()

	-- -- If distance between them is too high, stop raging
	-- if distance >= self.rage_max_distance then
	--     self.caster:RemoveModifierByName(self.modifier_rage)
	-- end
	end
end

function modifier_mask_of_madness_lua_berserk:GetEffectName()
	return "particles/items2_fx/mask_of_madness.vpcf"
end

function modifier_mask_of_madness_lua_berserk:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_mask_of_madness_lua_berserk:DeclareFunctions()
	local decFunc = {MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED}

	return decFunc
end

function modifier_mask_of_madness_lua_berserk:GetModifierMoveSpeedBonus_Percentage()
	return self.berserk_ms_bonus_pct

		-- In a rage!! Get move speed bonus
		-- if self.caster:HasModifier(self.modifier_rage) then
		--     ms_bonus = ms_bonus + self.rage_ms_bonus_pct
		-- end
end

function modifier_mask_of_madness_lua_berserk:GetModifierAttackSpeedBonus_Constant()
	return self.berserk_attack_speed
end

function modifier_mask_of_madness_lua_berserk:GetModifierPhysicalArmorBonus()
	return self.berserk_armor_reduction * (-1)
end

function modifier_mask_of_madness_lua_berserk:CheckState()
	local state = {[MODIFIER_STATE_SILENCED] = true}
	return state
end

function modifier_mask_of_madness_lua_berserk:CustomOnAttackLanded(keys)
-- RAGE IS DISBALED RIGHT NOW
-- if IsServer() then
--     local target = keys.target
--     local attacker = keys.attacker

--     -- Only apply on caster being the attacker, on other team, and when he doesn't have Rage
--     if self.caster == attacker and self.caster:GetTeamNumber() ~= target:GetTeamNumber() and not self.caster:HasModifier(self.modifier_rage) then

--         -- Also, only apply if the target is a hero
--         if target:IsHero() then
--             -- Mark target for rage distance checks
--             self.rage_target = target

--             -- Start a rage! (disables commands)
--             self.caster:AddNewModifier(self.caster, self.ability, self.modifier_rage, {})

--             -- Sound a rage!
--             EmitSoundOn(self.sound_rage, self.caster)

--             -- Force attacking the target
--             self.caster:MoveToTargetToAttack(target)
--         end
--     end
-- end
end

function modifier_mask_of_madness_lua_berserk:OnDestroy()
	if IsServer() then
	-- Remove rage
	-- if self.caster:HasModifier(self.modifier_rage) then
	--     self.caster:RemoveModifierByName(self.modifier_rage)
	-- end
	end
end

function modifier_mask_of_madness_lua_berserk:IsHidden()
	return false
end

function modifier_mask_of_madness_lua_berserk:IsPurgable()
	return false
end

function modifier_mask_of_madness_lua_berserk:IsDebuff()
	return false
end