item_bfury_lua = class({})

LinkLuaModifier("modifier_item_bfury_lua", 'items/custom_items/item_bfury_lua.lua', LUA_MODIFIER_MOTION_NONE)

function item_bfury_lua:GetAbilityTextureName()
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/fury_" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/item_bfury_lua" .. level
	end
end

function item_bfury_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

function item_bfury_lua:GetIntrinsicModifierName()
	return "modifier_item_bfury_lua"
end

modifier_item_bfury_lua = class({})

function modifier_item_bfury_lua:IsHidden()
	return true
end

function modifier_item_bfury_lua:IsPurgable()
	return false
end

function modifier_item_bfury_lua:DestroyOnExpire()
	return false
end

function modifier_item_bfury_lua:RemoveOnDeath()	
	return false 
end

function modifier_item_bfury_lua:OnCreated()
	self.parent = self:GetParent()
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_health_regen = self:GetAbility():GetSpecialValueFor("bonus_health_regen")
	self.bonus_mana_regen = self:GetAbility():GetSpecialValueFor("bonus_mana_regen")
	self.cleave_damage_percent = self:GetAbility():GetSpecialValueFor("cleave_damage_percent")
	self.cleave_ending_width = self:GetAbility():GetSpecialValueFor("cleave_ending_width")
	self.quelling_bonus = self:GetAbility():GetSpecialValueFor("quelling_bonus")
	self.quelling_bonus_ranged = self:GetAbility():GetSpecialValueFor("quelling_bonus_ranged")
end

function modifier_item_bfury_lua:OnRefresh()
	self.bonus_damage = self:GetAbility():GetSpecialValueFor("bonus_damage")
	self.bonus_health_regen = self:GetAbility():GetSpecialValueFor("bonus_health_regen")
	self.bonus_mana_regen = self:GetAbility():GetSpecialValueFor("bonus_mana_regen")
	self.cleave_damage_percent = self:GetAbility():GetSpecialValueFor("cleave_damage_percent")
	self.cleave_ending_width = self:GetAbility():GetSpecialValueFor("cleave_ending_width")
	self.quelling_bonus = self:GetAbility():GetSpecialValueFor("quelling_bonus")
	self.quelling_bonus_ranged = self:GetAbility():GetSpecialValueFor("quelling_bonus_ranged")
end

function modifier_item_bfury_lua:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
		MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
		-- MODIFIER_EVENT_ON_ATTACK_LANDED
	}
end

function modifier_item_bfury_lua:GetModifierConstantManaRegen()
	return self.bonus_mana_regen
end

function modifier_item_bfury_lua:GetModifierConstantHealthRegen()
	return self.bonus_health_regen
end

function modifier_item_bfury_lua:GetModifierPreAttack_BonusDamage(keys)
	if keys.target and not keys.target:IsHero() and not keys.target:IsOther() and not keys.target:IsBuilding() and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
		if not self:GetParent():IsRangedAttacker() then
			return self.bonus_damage + self.quelling_bonus
		else
			return self.bonus_damage + self.quelling_bonus_ranged
		end
	else
		return self.bonus_damage
	end
end

function modifier_item_bfury_lua:CustomOnAttackLanded(keys)
	if keys.attacker == self:GetParent() then
		if ( not self:GetParent():IsIllusion() ) and not self:GetParent():IsRangedAttacker() then
			if keys.target ~= nil and keys.target:GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
				local direction = keys.target:GetOrigin()-self:GetParent():GetOrigin()
				direction.z = 0
				direction = direction:Normalized()
				local range = self:GetParent():GetOrigin() + direction*650/2

				local facing_direction = keys.attacker:GetAnglesAsVector().y
				local cleave_targets = FindUnitsInRadius(keys.attacker:GetTeamNumber(),keys.attacker:GetAbsOrigin(),nil,400,DOTA_UNIT_TARGET_TEAM_ENEMY,DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,DOTA_UNIT_TARGET_FLAG_NONE,FIND_ANY_ORDER,false)
				for _,target in pairs(cleave_targets) do
					if target ~= keys.target and not table.has_value(bosses_names, target:GetUnitName()) and not target:IsRealHero() then
						local attacker_vector = (target:GetOrigin() - keys.attacker:GetOrigin())
						local attacker_direction = VectorToAngles( attacker_vector ).y
						local angle_diff = math.abs( AngleDiff( facing_direction, attacker_direction ) )
						if angle_diff < 45 then
							ApplyDamageRDA({
								victim = target,
								attacker = keys.attacker,
								damage = keys.damage * (self.cleave_damage_percent/100),
								damage_type = DAMAGE_TYPE_PHYSICAL,
								damage_flags = DOTA_DAMAGE_FLAG_IGNORES_PHYSICAL_ARMOR + DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION + DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL,
								ability = self:GetAbility()
							})
						end
					end
				end
				local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
				ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
				ParticleManager:ReleaseParticleIndex( effect_cast )

			end
		end
	end
end

function modifier_item_bfury_lua:PlayEffects1(direction )
	local effect_cast = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( effect_cast, 0, self:GetCaster():GetOrigin() )
	ParticleManager:SetParticleControlForward( effect_cast, 0, direction )
	ParticleManager:ReleaseParticleIndex( effect_cast )
end

--------------------дерево

function item_bfury_lua:OnSpellStart()
	local caster = self:GetCaster()
	local target_point = self:GetCursorPosition()
	local pos = Vector(6745, -2921, 261)
	if (target_point - pos):Length2D() < 400 then
		local trees = GridNav:GetAllTreesAroundPoint(target_point, 400, false)
		for _, tree in pairs(trees) do
			SetContextThink("self_destroy", function(tree) 
				if tree:IsStanding() then 
					tree:CutDown(DOTA_TEAM_GOODGUYS)
				end
				return 0.1
			end, 0.1)
		end
	else
		GridNav:DestroyTreesAroundPoint(target_point, 1, false)
	end
end

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function check_desolator(target)
local desolator_dict = { 
	["modifier_item_desolator_lua"] = 7,
	["modifier_item_desolator_lua_2"] = 15,
	["modifier_item_desolator_lua_3"] = 20,
	["modifier_item_desolator_lua_4"] = 40,
	["modifier_item_desolator_lua_5"] = 80,
	["modifier_item_desolator_lua_6"] = 120,
	["modifier_item_desolator_lua_7"] = 200,
	["modifier_item_desolator_lua_8"] = 300,
	}
	
	for key,val in pairs(desolator_dict) do
		local modifier = target:FindModifierByName(key)
		if modifier then
			local item = modifier:GetAbility()
			return val, item
		end
	end
end

function FindUnitsInCone( nTeamNumber, vCenterPos, vStartPos, vEndPos, fStartRadius, fEndRadius, hCacheUnit, nTeamFilter, nTypeFilter, nFlagFilter, nOrderFilter, bCanGrowCache )
	local direction = vEndPos-vStartPos
	direction.z = 0

	local distance = direction:Length2D()
	direction = direction:Normalized()

	local big_radius = distance + math.max(fStartRadius, fEndRadius)

	local units = FindUnitsInRadius(
		nTeamNumber,	-- int, your team number
		vCenterPos,	-- point, center point
		nil,	-- handle, cacheUnit. (not known)
		big_radius,	-- float, radius. or use FIND_UNITS_EVERYWHERE
		nTeamFilter,	-- int, team filter
		nTypeFilter,	-- int, type filter
		nFlagFilter,	-- int, flag filter
		nOrderFilter,	-- int, order filter
		bCanGrowCache	-- bool, can grow cache
	)

	local targets = {}
	for _,unit in pairs(units) do
		local vUnitPos = unit:GetOrigin()-vStartPos
		local fProjection = vUnitPos.x*direction.x + vUnitPos.y*direction.y + vUnitPos.z*direction.z
		fProjection = math.max(math.min(fProjection,distance),0)
		local vProjection = direction*fProjection
		local fUnitRadius = (vUnitPos - vProjection):Length2D()
		local fInterpRadius = (fProjection/distance)*(fEndRadius-fStartRadius) + fStartRadius
		if fUnitRadius<=fInterpRadius then
			table.insert( targets, unit )
		end
	end
	return targets
end

--------------------------------------

LinkLuaModifier("modifier_item_bfury_lua_debuff", 'items/custom_items/item_bfury_lua.lua', LUA_MODIFIER_MOTION_NONE)

modifier_item_bfury_lua_debuff = class({})

function modifier_item_bfury_lua_debuff:IsHidden() return false end
function modifier_item_bfury_lua_debuff:IsDebuff() return true end
function modifier_item_bfury_lua_debuff:IsPurgable() return true end

function modifier_item_bfury_lua_debuff:OnCreated(kv)
	self.count = (self:GetAbility():GetSpecialValueFor("corruption_armor") * (-1)) / 2
end

function modifier_item_bfury_lua_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
	}
end

function modifier_item_bfury_lua_debuff:GetModifierPhysicalArmorBonus()
	return self.count
end