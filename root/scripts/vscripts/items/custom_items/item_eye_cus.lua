LinkLuaModifier( "modifier_item_eye_cus", "items/custom_items/item_eye_cus", LUA_MODIFIER_MOTION_NONE )

item_eye_cus = class({})

function item_eye_cus:GetIntrinsicModifierName()
return "modifier_item_eye_cus"
end

modifier_item_eye_cus = class({})

function modifier_item_eye_cus:DeclareFunctions()
return {
	MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
	MODIFIER_PROPERTY_MANACOST_PERCENTAGE,
}
end

function modifier_item_eye_cus:GetModifierConstantManaRegen()
return self:GetAbility():GetSpecialValueFor("mana_reg")
end

function modifier_item_eye_cus:GetModifierPercentageManacost()
	return self:GetAbility():GetSpecialValueFor("min_man")
end