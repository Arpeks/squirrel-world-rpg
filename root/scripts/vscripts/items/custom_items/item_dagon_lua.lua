LinkLuaModifier("modifier_item_dagon_lua_passive", "items/custom_items/item_dagon_lua.lua", LUA_MODIFIER_MOTION_NONE)

item_dagon_lua = class({})

function item_dagon_lua:init()

end

function item_dagon_lua:GetAbilityTextureName()
	if self:GetToggleState() then
		return "all/dagon_off"
	end
	local level = self:GetLevel()
	if self:GetSecondaryCharges() == 0 then
		return "all/dagon" .. level
	else
		return "gem" .. self:GetSecondaryCharges() .. "/dagon" .. level
	end
end

function item_dagon_lua:GetIntrinsicModifierName()
	return "modifier_item_dagon_lua_passive"
end

function item_dagon_lua:OnUpgrade()
	Timers:CreateTimer(0.03,function()
		if ((self:GetItemSlot() > 5 and self:GetItemSlot() ~= DOTA_ITEM_NEUTRAL_SLOT) or self:GetItemSlot() == -1) then
			local m = self:GetCaster():FindModifierByName(self:GetIntrinsicModifierName())
			if m then
				m:Destroy()
			end
		end
	end)
end

function item_dagon_lua:OnSpellStart(damage, target)
	if self:GetSecondaryCharges() ~= 1000 then
		self.secondaryCharges = self:GetSecondaryCharges()
		self:SetSecondaryCharges(1000)
	else
		self:SetSecondaryCharges(self.secondaryCharges or 0)
	end
end

function item_dagon_lua:OnToggle()
	-- print(self:GetToggleState())
end

modifier_item_dagon_lua_passive = class({})

function modifier_item_dagon_lua_passive:IsHidden() return true end

function modifier_item_dagon_lua_passive:DeclareFunctions() 
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		-- MODIFIER_EVENT_ON_TAKEDAMAGE
	} 
end

function modifier_item_dagon_lua_passive:GetModifierBonusStats_Intellect()
	return self:GetAbility():GetSpecialValueFor("bonus_intellect")
end

function modifier_item_dagon_lua_passive:CustomOnTakeDamage(keys)
	if keys.attacker == self:GetParent() and not keys.unit:IsBuilding() and not keys.unit:IsOther() then		
		if self:GetParent():FindAllModifiersByName(self:GetName())[1] == self and keys.damage_category == 0 and keys.inflictor and bit.band(keys.damage_flags, DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL) ~= DOTA_DAMAGE_FLAG_NO_SPELL_LIFESTEAL then
		
			-- Particle effect
			self.lifesteal_pfx = ParticleManager:CreateParticle("particles/items3_fx/octarine_core_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.attacker)
			ParticleManager:SetParticleControl(self.lifesteal_pfx, 0, keys.attacker:GetAbsOrigin())
			ParticleManager:ReleaseParticleIndex(self.lifesteal_pfx)
			heal = keys.damage * (self:GetAbility():GetSpecialValueFor("spell_lifesteal") / 100)
			keys.attacker:HealWithParams(math.min(math.abs(heal), 2^30), self:GetAbility(), true, true, self:GetParent(), true)
			if self:GetParent():HasModifier("modifier_wisp_tether_lua") then
				local modifier = self:GetParent():FindModifierByName("modifier_wisp_tether_lua")
				modifier.heal = math.min(modifier.heal + heal, 2^30)
			end
		end
	end
end

function modifier_item_dagon_lua_passive:OnCreated(kv)
	if not IsServer() then return end
	self.ability = self:GetAbility()
	self.target_number = self:GetAbility():GetSpecialValueFor("target_number")
	self.cooldownReductionKill = self:GetAbility():GetSpecialValueFor("cooldown_reduction_kill")
	self.abilityCooldown =  self:GetAbility():GetSpecialValueFor("cooldown")
	if self.ability.abilityCooldownCurrent == nil then
		self.ability.abilityCooldownCurrent = 0
	end
	if self.ability.castTime == nil then
		self.ability.castTime = GameRules:GetGameTime() - self.abilityCooldown
	end
	self.interval = 0.2
	self:StartIntervalThink(self.interval)
end

function modifier_item_dagon_lua_passive:OnIntervalThink()
	local currentCooldown = math.max(0, self.ability.abilityCooldownCurrent - (GameRules:GetGameTime() - self.ability.castTime))
	if self:GetAbility():GetToggleState() then
		self.ability.castTime = self.ability.castTime + self.interval
	end
	self:GetAbility():SetCurrentCharges(math.ceil(currentCooldown))
	if currentCooldown > 0 or self:GetAbility():GetToggleState() or self:GetCaster():IsAlive() == false then
		return
	end
	local units = FindUnitsInRadius(
		self:GetParent():GetTeamNumber(),	-- int, your team number
		self:GetParent():GetOrigin(),	-- point, center point
		nil,	-- handle, cacheUnit. (not known)
		self:GetAbility():GetCastRange(self:GetParent():GetOrigin(), nil),	-- float, radius. or use FIND_UNITS_EVERYWHERE
		DOTA_UNIT_TARGET_TEAM_ENEMY,	-- int, team filter
		DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,	-- int, type filter
		DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE,	-- int, flag filter
		FIND_ANY_ORDER,	-- int, order filter
		false	-- bool, can grow cache
	)
	if #units > 0 then
		local maxHealth = 0
		local maxUnit = nil
		self.ability.abilityCooldownCurrent = self.abilityCooldown
		for i,unit in ipairs(units) do
			if unit:GetHealth() > maxHealth then
				maxHealth = unit:GetHealth()
				maxUnit = unit
			end
		end
		self:AbilityHit(self:AbilityDamage(true), maxUnit)
		local count = 1
		for i = 1, #units do
			if count >= self.target_number then break end
			local unit = units[i]
			if unit:IsAlive() and unit:entindex() ~= maxUnit:entindex() then
				self:AbilityHit(self:AbilityDamage(false), unit)
				count = count + 1
			end
		end
		self.ability.abilityCooldownCurrent = self.ability.abilityCooldownCurrent * self:GetCaster():GetCooldownReduction()
		self.ability.castTime = GameRules:GetGameTime()
		self:GetCaster():EmitSoundParams( "DOTA_Item.Dagon5.Target", 0, 0.1, 0 )
		self:OnIntervalThink()
		self:StartIntervalThink(self.interval)
	end
end

function modifier_item_dagon_lua_passive:AbilityDamage(isMain)
	local damage = self:GetAbility():GetSpecialValueFor("spell_damage") + self:GetCaster():GetIntellect(true) * self:GetSpecialValueFor("intelligence_damage") / 100
	if isMain == true then
		damage = damage + damage / 100 * self:GetAbility():GetSpecialValueFor("damage_first")
	end
	return damage
end

function modifier_item_dagon_lua_passive:AbilityHit(damage, target)
	local caster = self:GetCaster()

	local dagon_pfx = ParticleManager:CreateParticle("particles/items_fx/dagon.vpcf", PATTACH_RENDERORIGIN_FOLLOW, caster)
	ParticleManager:SetParticleControlEnt(dagon_pfx, 0, caster, PATTACH_POINT_FOLLOW, "attach_attack1", caster:GetAbsOrigin(), false)
	ParticleManager:SetParticleControlEnt(dagon_pfx, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), false)
	ParticleManager:SetParticleControl(dagon_pfx, 2, Vector(damage, 0, 0))
	ParticleManager:SetParticleControl(dagon_pfx, 3, Vector(0.3, 0, 0))
	ParticleManager:ReleaseParticleIndex(dagon_pfx)
	ApplyDamageRDA({
		attacker = caster,
		victim = target,
		ability = self:GetAbility(),
		damage = damage,
		damage_type = DAMAGE_TYPE_MAGICAL
	})
	if not target:IsAlive() then
		self.ability.abilityCooldownCurrent = self.ability.abilityCooldownCurrent -self.cooldownReductionKill 
	end
end