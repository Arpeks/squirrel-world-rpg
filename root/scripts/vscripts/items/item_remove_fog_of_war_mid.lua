if item_remove_fog_of_war_mid == nil then
	item_remove_fog_of_war_mid = class({})
end

function item_remove_fog_of_war_mid:OnSpellStart()
	local A = {-1331.951660, 5193.255371, 420.980743}
	local B = {-1300.612305, -6971.299805, 512.000000}
	local stepSize = 300 -- желаемое расстояние между точками

	-- Функция для вычисления расстояния между точками
	function distance(v1, v2)
		local dx = v2[1] - v1[1]
		local dy = v2[2] - v1[2]
		local dz = v2[3] - v1[3]
		return math.sqrt(dx*dx + dy*dy + dz*dz)
	end

	-- Функция для нормализации вектора
	function normalize(v)
		local dist = distance({0, 0, 0}, v)
		return {v[1]/dist, v[2]/dist, v[3]/dist}
	end

	-- Функция для получения точек на линии
	function getPointsOnLine(A, B, step)
		local direction = {B[1] - A[1], B[2] - A[2], B[3] - A[3]}
		local normDirection = normalize(direction)
		local points = {}
		local currentPoint = {A[1], A[2], A[3]}
		local endPointDistance = distance(A, B)

		table.insert(points, {currentPoint[1], currentPoint[2], currentPoint[3]})

		while distance(A, currentPoint) + step < endPointDistance do
			currentPoint[1] = currentPoint[1] + normDirection[1] * step
			currentPoint[2] = currentPoint[2] + normDirection[2] * step
			currentPoint[3] = currentPoint[3] + normDirection[3] * step
			table.insert(points, {currentPoint[1], currentPoint[2], currentPoint[3]})
		end

		-- Добавление конечной точки, если нужно точно остановиться на ней
		table.insert(points, {B[1], B[2], B[3]})

		return points
	end

	-- Получение и печать точек
	local points = getPointsOnLine(A, B, stepSize)
	for _, p in ipairs(points) do
		AddFOWViewer(DOTA_TEAM_GOODGUYS, Vector(p[1], p[2], p[3]), 500, self:GetSpecialValueFor("duration"), false)
	end
	self:GetCaster():EmitSound("Item.Gleipnir.Cast")
	UTIL_Remove(self)
end