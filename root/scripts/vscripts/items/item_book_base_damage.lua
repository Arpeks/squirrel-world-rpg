LinkLuaModifier("modifier_book_base_damage", "items/item_book_base_damage.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_book_base_damage_bonus", "items/item_book_base_damage.lua", LUA_MODIFIER_MOTION_NONE )

item_book_base_damage = class({})

function item_book_base_damage:GetIntrinsicModifierName()
    return "modifier_book_base_damage"
end

function item_book_base_damage:OnSpellStart()
    self.caster = self:GetCaster()
    local m = self.caster:FindModifierByName("modifier_book_base_damage_bonus")
    if m then
        m:SetStackCount(m:GetStackCount() + self:GetCurrentCharges())
    else
        self.caster:AddNewModifier(self.caster, nil, "modifier_book_base_damage_bonus", {}):SetStackCount(self:GetCurrentCharges())
    end
    self:SetCurrentCharges(0)
    self.caster:EmitSound("Item.TomeOfKnowledge")
    self.caster:CalculateStatBonus(true)
end

--------------------------------------------------------------------------

modifier_book_base_damage = class({})

function modifier_book_base_damage:IsHidden()
    return true
end

function modifier_book_base_damage:IsDebuff()
    return false
end

function modifier_book_base_damage:IsPurgable()
    return false
end

function modifier_book_base_damage:OnCreated()
    if not IsServer() then
        return
    end
    self:StartIntervalThink(1)
end

function modifier_book_base_damage:OnIntervalThink()
    if not IsServer() then return end
    local caster = self:GetCaster()
    local parent = self:GetParent()
    local ability = self:GetAbility()
    local charges = self:GetAbility():GetCurrentCharges()
    local gold = caster:GetGold()
    local gold_bank = caster:FindModifierByName("modifier_gold_bank")
    gold = gold + gold_bank:GetStackCount()
    gold_bank:SetStackCount(0)
    if gold >= 20000 then
        count = math.min( ( gold - ( gold % 20000 ) ) / 20000, 10000000 / 20000 )
        gold = gold - count * 20000
        caster:SetGold(0 , false) 
        caster:ModifyGoldFiltered( gold, true, 0 )
        ability:SetCurrentCharges(charges + count)
        caster:EmitSound("DOTA_Item.Hand_Of_Midas")
    end
end




















modifier_book_base_damage_bonus = class({})
--Classifications template
function modifier_book_base_damage_bonus:IsHidden()
    return true
end

function modifier_book_base_damage_bonus:IsDebuff()
    return false
end

function modifier_book_base_damage_bonus:IsPurgable()
    return false
end

function modifier_book_base_damage_bonus:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_book_base_damage_bonus:IsStunDebuff()
    return false
end

function modifier_book_base_damage_bonus:RemoveOnDeath()
    return false
end

function modifier_book_base_damage_bonus:DestroyOnExpire()
    return false
end

function modifier_book_base_damage_bonus:OnCreated()
    self.bonus_stat = 0
    if not IsServer() then
        return
    end
    self.bonus_stat = 30
    if diff_wave:GetGameDifficulty() == "unreal" then
        self.bonus_stat = self.bonus_stat / 5
    end
    self:SetHasCustomTransmitterData( true )
    -- self:SendBuffRefreshToClients()
end

function modifier_book_base_damage_bonus:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE
    }
end

function modifier_book_base_damage_bonus:GetModifierBaseAttack_BonusDamage()
    return self:GetStackCount() * self.bonus_stat
end

function modifier_book_base_damage_bonus:AddCustomTransmitterData()
    return {
        bonus_stat = self.bonus_stat,
    }
end

function modifier_book_base_damage_bonus:HandleCustomTransmitterData( data )
    self.bonus_stat = data.bonus_stat
end
