LinkLuaModifier( "modifier_item_quest_blue_stone", "items/item_quest_all", LUA_MODIFIER_MOTION_NONE )
item_quest_blue_stone = class({})

function item_quest_blue_stone:GetIntrinsicModifierName()
	return "modifier_item_quest_blue_stone"
end

modifier_item_quest_blue_stone = class({})

function modifier_item_quest_blue_stone:IsHidden()
	return true
end

function modifier_item_quest_blue_stone:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
	}
end

function modifier_item_quest_blue_stone:GetModifierBonusStats_Strength()
	return self:GetAbility():GetSpecialValueFor("bonus_attributes") * self:GetAbility():GetCurrentCharges()
end

function modifier_item_quest_blue_stone:GetModifierBonusStats_Agility()
	return self:GetAbility():GetSpecialValueFor("bonus_attributes") * self:GetAbility():GetCurrentCharges()
end

function modifier_item_quest_blue_stone:GetModifierBonusStats_Intellect()
	return self:GetAbility():GetSpecialValueFor("bonus_attributes") * self:GetAbility():GetCurrentCharges()
end



LinkLuaModifier( "modifier_item_quest_furion_staff", "items/item_quest_all", LUA_MODIFIER_MOTION_NONE )
item_quest_furion_staff = class({})

function item_quest_furion_staff:GetIntrinsicModifierName()
	return "modifier_item_quest_furion_staff"
end

modifier_item_quest_furion_staff = class({})

function modifier_item_quest_furion_staff:IsHidden()
	return true
end

function modifier_item_quest_furion_staff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
	}
end

function modifier_item_quest_furion_staff:GetModifierPreAttack_BonusDamage()
	return self:GetAbility():GetSpecialValueFor("bonus_damage")
end

function modifier_item_quest_furion_staff:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_attack")
end

function modifier_item_quest_furion_staff:GetModifierMoveSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_move")
end



LinkLuaModifier( "modifier_item_quest_miners_titanite", "items/item_quest_all", LUA_MODIFIER_MOTION_NONE )
item_quest_miners_titanite = class({})

function item_quest_miners_titanite:GetIntrinsicModifierName()
	return "modifier_item_quest_miners_titanite"
end

modifier_item_quest_miners_titanite = class({})

function modifier_item_quest_miners_titanite:IsHidden()
	return true
end

function modifier_item_quest_miners_titanite:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
		MODIFIER_PROPERTY_HEALTH_BONUS,
	}
end

function modifier_item_quest_miners_titanite:GetModifierPhysicalArmorBonus()
	return self:GetAbility():GetSpecialValueFor("bonus_armor") * self:GetAbility():GetCurrentCharges()
end

function modifier_item_quest_miners_titanite:GetModifierHealthBonus()
	return self:GetAbility():GetSpecialValueFor("bonus_healt") * self:GetAbility():GetCurrentCharges()
end



LinkLuaModifier( "modifier_item_quest_ancient_rune", "items/item_quest_all", LUA_MODIFIER_MOTION_NONE )
item_quest_ancient_rune = class({})

function item_quest_ancient_rune:GetIntrinsicModifierName()
	return "modifier_item_quest_ancient_rune"
end

modifier_item_quest_ancient_rune = class({})

function modifier_item_quest_ancient_rune:IsHidden()
	return true
end

function modifier_item_quest_ancient_rune:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
	}
end

function modifier_item_quest_ancient_rune:GetModifierBonusStats_Intellect()
	return self:GetAbility():GetSpecialValueFor("bonus_intellect") * self:GetAbility():GetCurrentCharges()
end

function modifier_item_quest_ancient_rune:GetModifierConstantManaRegen()
	return self:GetAbility():GetSpecialValueFor("bonus_mana_regen") * self:GetAbility():GetCurrentCharges()
end