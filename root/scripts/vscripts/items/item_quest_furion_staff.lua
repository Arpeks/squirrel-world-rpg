LinkLuaModifier( "modifier_item_quest_furion_staff", "items/item_quest_furion_staff.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_quest_furion_staff == nil then
	item_quest_furion_staff = class({})
end
function item_quest_furion_staff:GetIntrinsicModifierName()
	return "modifier_item_quest_furion_staff"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_quest_furion_staff == nil then
	modifier_item_quest_furion_staff = class({})
end

function modifier_item_quest_furion_staff:IsHidden() return true end

function modifier_item_quest_furion_staff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
	}
end

function modifier_item_quest_furion_staff:GetModifierPreAttack_BonusDamage()
	return self:GetAbility():GetSpecialValueFor("bonus_damage")
end

function modifier_item_quest_furion_staff:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_attack")
end

function modifier_item_quest_furion_staff:GetModifierMoveSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("bonus_move")
end