LinkLuaModifier( "modifier_item_lesser_crit_aom", "items/aom/item_lesser_crit_aom.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_lesser_crit_aom == nil then
	item_lesser_crit_aom = class({})
end
function item_lesser_crit_aom:GetIntrinsicModifierName()
	return "modifier_item_lesser_crit_aom"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_lesser_crit_aom == nil then
	modifier_item_lesser_crit_aom = class({})
end
function modifier_item_lesser_crit_aom:OnCreated(params)
	if IsServer() then
	end
end
function modifier_item_lesser_crit_aom:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_item_lesser_crit_aom:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_lesser_crit_aom:DeclareFunctions()
	return {
	}
end