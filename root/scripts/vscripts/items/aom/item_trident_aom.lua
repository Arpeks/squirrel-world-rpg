LinkLuaModifier( "modifier_item_trident_aom", "items/aom/item_trident_aom.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if item_trident_aom == nil then
	item_trident_aom = class({})
end
function item_trident_aom:GetIntrinsicModifierName()
	return "modifier_item_trident_aom"
end
---------------------------------------------------------------------
--Modifiers
if modifier_item_trident_aom == nil then
	modifier_item_trident_aom = class({})
end
function modifier_item_trident_aom:OnCreated(params)
	if IsServer() then
	end
end
function modifier_item_trident_aom:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_item_trident_aom:OnDestroy()
	if IsServer() then
	end
end
function modifier_item_trident_aom:DeclareFunctions()
	return {
	}
end