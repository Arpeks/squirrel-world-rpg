LinkLuaModifier( "modifier_blue_stone", "items/blue_stone.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if blue_stone == nil then
	blue_stone = class({})
end
function blue_stone:GetIntrinsicModifierName()
	return "modifier_blue_stone"
end
---------------------------------------------------------------------
--Modifiers
if modifier_blue_stone == nil then
	modifier_blue_stone = class({})
end
function modifier_blue_stone:OnCreated(params)
	if IsServer() then
	end
end
function modifier_blue_stone:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_blue_stone:OnDestroy()
	if IsServer() then
	end
end
function modifier_blue_stone:DeclareFunctions()
	return {
	}
end