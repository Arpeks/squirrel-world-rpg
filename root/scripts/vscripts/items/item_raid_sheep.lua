item_raid_sheep = class({})

LinkLuaModifier("modifier_item_raid_sheep", "items/item_raid_sheep.lua", LUA_MODIFIER_MOTION_NONE)

function item_raid_sheep:GetBehavior()
    if self:GetCaster():HasModifier("modifier_item_raid_sheep") then
        return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
    end
    return DOTA_ABILITY_BEHAVIOR_NO_TARGET
end

function item_raid_sheep:OnSpellStart()
    if EventHandler:IsInvokerKilled() ~= false then
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", {message="dota_hud_error_kill_invoker"})
        return
    end
    if not self:GetCaster():HasModifier("modifier_item_raid_sheep") then
        if self:GetCaster():GetTotalGold() > 1000000 then
            self:GetCaster():ModifyGoldFiltered(-1000000, false, DOTA_ModifyGold_AbilityCost)
            self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_item_raid_sheep", {})
            local pcf = ParticleManager:CreateParticle("models/heroes/muerta/debut/particles/revenant/muerta_debut_revenant_spawn_portal.vpcf", PATTACH_POINT, self:GetCaster())
            ParticleManager:ReleaseParticleIndex(pcf)
            EmitSoundOn("Hero_Oracle.FortunesEnd.Target", self:GetCaster())
            return
        else
            local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
            CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", {message="dota_hud_error_not_enough_gold"})
            return
        end
    end
    if self:GetCursorTarget():GetUnitName() == "sheep" then
        local unit = CreateUnitByName("npc_raid_sheep", Vector(-1399, 3636, 389), true, nil, nil, DOTA_TEAM_BADGUYS)
        unit:SetForwardVector(Vector(0,1,0))
        UTIL_Remove(self:GetCursorTarget())
        UTIL_Remove(self)
    else
        local player = PlayerResource:GetPlayer(self:GetCaster():GetPlayerOwnerID())
        CustomGameEventManager:Send_ServerToPlayer(player, "CreateIngameErrorMessage", {message="dota_hud_error_not_valid_target"})
        return
    end
end

modifier_item_raid_sheep = class({})
--Classifications template
function modifier_item_raid_sheep:IsHidden()
    return true
end

function modifier_item_raid_sheep:IsDebuff()
    return false
end

function modifier_item_raid_sheep:IsPurgable()
    return false
end

function modifier_item_raid_sheep:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_item_raid_sheep:IsStunDebuff()
    return false
end

function modifier_item_raid_sheep:RemoveOnDeath()
    return false
end

function modifier_item_raid_sheep:DestroyOnExpire()
    return false
end
