item_creep_spawn_control_location_1 = class({})

function item_creep_spawn_control_location_1:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(1)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_2 = class({})

function item_creep_spawn_control_location_2:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(2)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_3 = class({})

function item_creep_spawn_control_location_3:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(3)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_4 = class({})

function item_creep_spawn_control_location_4:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(4)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_5 = class({})

function item_creep_spawn_control_location_5:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(5)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_6 = class({})

function item_creep_spawn_control_location_6:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(6)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_7 = class({})

function item_creep_spawn_control_location_7:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(7)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_8 = class({})

function item_creep_spawn_control_location_8:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(8)
        UTIL_Remove(self)
    end
end

item_creep_spawn_control_location_9 = class({})

function item_creep_spawn_control_location_9:OnSpellStart()
    self:GetCaster():EmitSound("DOTA_Item.Orchid.Activate")
    if IsServer() then
        LocationSpawnControl:AddBotton(9)
        UTIL_Remove(self)
    end
end