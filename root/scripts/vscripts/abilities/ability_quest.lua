ability_quest = class({})

function ability_quest:GetIntrinsicModifierName()	
    return "modifier_ability_quest"
end

function ability_quest:OnSpellStart()
    self.target = self:GetCursorTarget()
    self.pid = self:GetCaster():GetPlayerID()
    self:SetHidden(true)
end

function ability_quest:GetChannelTime()
    return self:GetSpecialValueFor("channel_time")
end

function ability_quest:OnChannelThink(interval)
    if self.target["activated"..self.pid] then
        self:SetChanneling(false)
    end
end

function ability_quest:OnChannelFinish(interrupted)
    if IsServer() then
        if interrupted == false then
            local quest = QuestsRPG:FindQuestById(self.quest_id)
            local task = quest[self.task_id]
            if task.functions and task.functions.OnChannelFinish then
                local function_name = task.functions.OnChannelFinish
                local func = QuestsRPG[function_name]
                if func then
                    func(QuestsRPG, {
                        ["caster"] = self:GetCaster(),
                        ["ability_quest"] = self,
                    })
                end
            else
                self.target["activated"..self.pid] = true
                QuestsRPG:UpdateCounter(self.pid, self.quest_id, self.task_id)
            end
        end
        self:SetChanneling(false)
    end
end

function ability_quest:GetCastRange(vLocation, hTarget)
    return 300
end

function ability_quest:GetChannelAnimation()
    return ACT_DOTA_TELEPORT
end

LinkLuaModifier("modifier_ability_quest", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
modifier_ability_quest = class({})

function modifier_ability_quest:IsHidden()
    return true
end

function modifier_ability_quest:OnCreated( kv )
    self.parent = self:GetParent()
    if IsServer() then
        self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ability_quest_allies", {})
        self.pid = self.parent:GetPlayerOwnerID()
        self.prev_position = self.parent:GetAbsOrigin()
        self.new_position = self.parent:GetAbsOrigin()
        self.damage_deal = {}
        self.damage_take = {}
        self:StartIntervalThink(0.25)
        self.channel_time = self:GetAbility():GetSpecialValueFor("channel_time")
        self:SetHasCustomTransmitterData( true )
    end
end

function modifier_ability_quest:OnRefresh( kv )
    if IsServer() then
        self.channel_time = self:GetAbility().channel_time
        self:SendBuffRefreshToClients()
    end
end

function modifier_ability_quest:AddCustomTransmitterData()
	return {
		channel_time = self.channel_time,
	}
end

function modifier_ability_quest:HandleCustomTransmitterData( data )
	self.channel_time = data.channel_time
end

function modifier_ability_quest:OnIntervalThink()
    self.prev_position = self.new_position
    self.new_position = self.parent:GetAbsOrigin()
    if QuestsRPG then
        EventHandler:OnDealDamage({
            ["pid"] = self.pid,
            ["damage_table"] = self.damage_deal,
        })
        EventHandler:OnTakeDamage({
            ["pid"] = self.pid,
            ["damage_table"] = self.damage_take,
        })
        EventHandler:OnMoving({
            ["pid"] = self.pid,
            ["distance"] = nil,
            ["prev_position"] = self.prev_position,
            ["new_position"] = self.new_position,
        })
    end
    self.damage_deal = {}
    self.damage_take = {}
end

function modifier_ability_quest:DeclareFunctions()
    local funcs = {
        -- MODIFIER_EVENT_ON_TAKEDAMAGE,
        MODIFIER_EVENT_ON_RESPAWN,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
		MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,
    }
    return funcs
end

function modifier_ability_quest:GetModifierOverrideAbilitySpecial(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "channel_time" then
			return 1
		end
	end
	return 0
end

function modifier_ability_quest:GetModifierOverrideAbilitySpecialValue(data)
	if data.ability and data.ability == self:GetAbility() then
		if data.ability_special_value == "channel_time" then
            return self.channel_time or self:GetAbility():GetLevelSpecialValueNoOverride( "channel_time", data.ability_special_level )
		end
	end
	return 0
end

function modifier_ability_quest:CustomOnTakeDamage( params )
    if params.attacker:GetTeamNumber() == self.parent:GetTeamNumber() and params.attacker == self.parent and params.unit:GetUnitName() ~= "npc_dota_hero_target_dummy" and params.unit:GetTeamNumber() ~= self.parent:GetTeamNumber() then
        local unit_name = params.unit:GetName()
        if self.damage_deal[unit_name] == nil then 
            self.damage_deal[unit_name] = {
                ["damage"] = math.min(params.damage, params.unit:GetMaxHealth()),
                ["original_damage"] = params.original_damage,
            }
        else
            self.damage_deal[unit_name]["damage"] = math.min(self.damage_deal[unit_name]["damage"] + params.damage, params.unit:GetMaxHealth())
            self.damage_deal[unit_name]["original_damage"] = self.damage_deal[unit_name]["original_damage"] + params.original_damage
        end
    elseif params.unit:GetTeamNumber() == self.parent:GetTeamNumber() and params.unit == self.parent and params.attacker:GetTeamNumber() ~= self.parent:GetTeamNumber() then
        local unit_name = params.attacker:GetName()
        if self.damage_take[unit_name] == nil then 
            self.damage_take[unit_name] = {
                ["damage"] = math.min(params.damage, params.unit:GetMaxHealth()),
                ["original_damage"] = params.original_damage,
            }
        else
            self.damage_take[unit_name]["damage"] = math.min(self.damage_take[unit_name]["damage"] + params.damage, params.unit:GetMaxHealth())
            self.damage_take[unit_name]["original_damage"] = self.damage_take[unit_name]["original_damage"] + params.original_damage
        end
    end
end

function modifier_ability_quest:OnRespawn( params )
    if params.unit:GetTeamNumber() == self.parent:GetTeamNumber() and params.unit == self.parent then
        self.prev_position = self.parent:GetAbsOrigin()
        self.new_position = self.parent:GetAbsOrigin()
    end
end

function modifier_ability_quest:IsAura()
    return true
end

function modifier_ability_quest:GetModifierAura()
    return "modifier_ability_quest_aura"
end

function modifier_ability_quest:GetAuraRadius()
    return RDA_QUEST_NPC_AURA_RADIUS
end

function modifier_ability_quest:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_CREEP + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING + DOTA_UNIT_TARGET_COURIER + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HEROES_AND_CREEPS + DOTA_UNIT_TARGET_OTHER + DOTA_UNIT_TARGET_ALL + DOTA_UNIT_TARGET_CUSTOM + DOTA_UNIT_TARGET_SELF
end

function modifier_ability_quest:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_BOTH
end

function modifier_ability_quest:GetAuraSearchFlags()
    return DOTA_UNIT_TARGET_FLAG_DEAD + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE
end

function modifier_ability_quest:GetAuraDuration()
    return 0.1
end

LinkLuaModifier("modifier_ability_quest_aura", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)

modifier_ability_quest_aura = class({})

function modifier_ability_quest_aura:IsHidden()
    return true
end

function modifier_ability_quest_aura:IsPurgable()
    return false
end

function modifier_ability_quest_aura:OnCreated( kv )
    if not IsServer() then 
        return
    end
    EventHandler:OnGetCloseToTarget(self:GetCaster():GetPlayerOwnerID(), self:GetParent():GetUnitName(), self:GetParent():GetName())
end

LinkLuaModifier("modifier_ability_quest_allies", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
modifier_ability_quest_allies = class({})

function modifier_ability_quest_allies:IsHidden()
    return true
end

function modifier_ability_quest_allies:RemoveOnDeath()
    return false
end

function modifier_ability_quest_allies:IsPurgable()
    return false
end

function modifier_ability_quest_allies:IsPurgeException()
    return false
end

function modifier_ability_quest_allies:IsPermanent()
    return true
end

function modifier_ability_quest_allies:GetAttributes()
    return MODIFIER_ATTRIBUTE_PERMANENT
end

function modifier_ability_quest_allies:OnCreated(kv)
    if IsServer() then 
        self:GetCaster().allies_aura = {}
        self.modifier_aura_name = "modifier_ability_quest_allies_aura_" .. self:GetCaster():GetPlayerOwnerID()
    end
end

function modifier_ability_quest_allies:IsAura()
    return true
end

function modifier_ability_quest_allies:GetModifierAura()
    return self.modifier_aura_name
end

function modifier_ability_quest_allies:GetAuraRadius()
    return RDA_QUEST_KILL_COUNTS_RADIUS
end

function modifier_ability_quest_allies:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO
end

function modifier_ability_quest_allies:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_FRIENDLY
end

function modifier_ability_quest_allies:GetAuraSearchFlags()
    return DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS
end

function modifier_ability_quest_allies:GetAuraDuration()
    return 0.1
end

LinkLuaModifier("modifier_ability_quest_allies_aura_0", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ability_quest_allies_aura_1", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ability_quest_allies_aura_2", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ability_quest_allies_aura_3", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_ability_quest_allies_aura_4", "abilities/ability_quest", LUA_MODIFIER_MOTION_NONE)
modifier_ability_quest_allies_aura_0 = class({})

function modifier_ability_quest_allies_aura_0:IsHidden()
    return true
end

function modifier_ability_quest_allies_aura_0:IsPurgable()
    return false
end

function modifier_ability_quest_allies_aura_0:OnCreated()
    if IsServer() then
        self.caster = self:GetCaster()
        self.parent_id = self:GetParent():GetPlayerOwnerID()
        self.caster.allies_aura[self.parent_id] = true
    end
end

function modifier_ability_quest_allies_aura_0:OnDestroy()
    if IsServer() then
        self.caster.allies_aura[self.parent_id] = nil
    end
end

modifier_ability_quest_allies_aura_1 = class(modifier_ability_quest_allies_aura_0)
modifier_ability_quest_allies_aura_2 = class(modifier_ability_quest_allies_aura_0)
modifier_ability_quest_allies_aura_3 = class(modifier_ability_quest_allies_aura_0)
modifier_ability_quest_allies_aura_4 = class(modifier_ability_quest_allies_aura_0)