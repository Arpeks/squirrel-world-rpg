LinkLuaModifier( "modifier_spawn_creep", "abilities/spawn_creep.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if spawn_creep == nil then
	spawn_creep = class({})
end
function spawn_creep:GetIntrinsicModifierName()
	return "modifier_spawn_creep"
end
---------------------------------------------------------------------
--Modifiers
if modifier_spawn_creep == nil then
	modifier_spawn_creep = class({})
end
function modifier_spawn_creep:OnCreated(params)
	if IsServer() then
	end
end
function modifier_spawn_creep:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_spawn_creep:OnDestroy()
	if IsServer() then
	end
end
function modifier_spawn_creep:DeclareFunctions()
	return {
	}
end