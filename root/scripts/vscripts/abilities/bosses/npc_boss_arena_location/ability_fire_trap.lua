ability_fire_trap = class({})

LinkLuaModifier("modifier_ability_fire_trap_thinker", "abilities/bosses/npc_boss_arena_location/ability_fire_trap", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_ability_fire_trap_root", "abilities/bosses/npc_boss_arena_location/ability_fire_trap", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_ability_fire_trap_cooldown", "abilities/bosses/npc_boss_arena_location/ability_fire_trap", LUA_MODIFIER_MOTION_NONE )

function ability_fire_trap:OnSpellStart()
	CreateModifierThinker(self:GetCaster(), self, "modifier_ability_fire_trap_thinker", {}, self:GetCaster():GetCursorPosition(), self:GetCaster():GetTeamNumber(), false)
end

modifier_ability_fire_trap_thinker = class({})

function modifier_ability_fire_trap_thinker:IsHidden()
	return false
end

function modifier_ability_fire_trap_thinker:IsDebuff()
	return false
end

function modifier_ability_fire_trap_thinker:IsPurgable()
	return false
end

function modifier_ability_fire_trap_thinker:OnCreated( kv )
	if not IsServer() then return end
	print("1")
	self.root_duration = 1
	self.root_interval = 3
	self.radius = 600
	self.duration = 5
	self.level = self:GetCaster():GetLevel()
	self.damage = 2000 * self.level^2
	self:StartIntervalThink( FrameTime() )
	self:OnIntervalThink()
	self:SetDuration(self.duration, true)
	local effect_cast = ParticleManager:CreateParticle( "particles/invoker_magicka/invoker_root_aoe/root_aoe.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )
	ParticleManager:SetParticleControl( effect_cast, 0, self:GetParent():GetOrigin() )
	ParticleManager:SetParticleControl( effect_cast, 1, Vector( self.radius, 1, 1 ) )
	ParticleManager:SetParticleControl( effect_cast, 2, Vector( self:GetDuration(), 0, 0 ) )

	self:AddParticle(effect_cast,false,false,-1,false,false)

	EmitSoundOn( "Hero_AbyssalUnderlord.PitOfMalice", self:GetParent() )
end

function modifier_ability_fire_trap_thinker:OnDestroy()
	if not IsServer() then return end
	UTIL_Remove( self:GetParent() )
end

function modifier_ability_fire_trap_thinker:OnIntervalThink()
	local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false)

	for _,enemy in pairs(enemies) do
		local modifier = enemy:FindModifierByNameAndCaster( "modifier_ability_fire_trap_cooldown", self:GetCaster() )
		if not modifier then
            ApplyDamage({
                victim = enemy,
                attacker = self:GetCaster(),
                damage = self.damage,
                damage_type = DAMAGE_TYPE_MAGICAL,
                damage_flags = 0,
                ability = self:GetAbility()
            })
			if self.root_duration > 0 then
				enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ability_fire_trap_root", { duration = self.root_duration, damage = self.damage })
			end
			enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ability_fire_trap_cooldown", { duration = self.root_interval, damage = self.damage, x = self:GetParent():GetAbsOrigin().x, y = self:GetParent():GetAbsOrigin().y, radius = self.radius, level = self.level, parent = self:GetParent():entindex() })
		end
	end
end

modifier_ability_fire_trap_root = class({})

function modifier_ability_fire_trap_root:IsHidden()
	return false
end

function modifier_ability_fire_trap_root:IsDebuff()
	return true
end

function modifier_ability_fire_trap_root:IsStunDebuff()
	return false
end

function modifier_ability_fire_trap_root:IsPurgable()
	return true
end

function modifier_ability_fire_trap_root:GetPriority()
	return MODIFIER_PRIORITY_HIGH
end

function modifier_ability_fire_trap_root:OnCreated( kv )
	if not IsServer() then return end

	local hero = self:GetParent():IsHero()
	local sound_cast = "Hero_AbyssalUnderlord.Pit.TargetHero"
	if not hero then
		sound_cast = "Hero_AbyssalUnderlord.Pit.Target"
	end
	EmitSoundOn( sound_cast, self:GetParent() )
end

function modifier_ability_fire_trap_root:CheckState()
	return {
		[MODIFIER_STATE_INVISIBLE] = false,
		[MODIFIER_STATE_ROOTED] = not self:GetParent():IsDebuffImmune(),
	}
end

function modifier_ability_fire_trap_root:GetEffectName()
	return "particles/units/heroes/hero_ember_spirit/ember_spirit_searing_chains_debuff.vpcf"
end

function modifier_ability_fire_trap_root:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

modifier_ability_fire_trap_cooldown = class({})
--Classifications template
function modifier_ability_fire_trap_cooldown:IsHidden()
    return true
end

function modifier_ability_fire_trap_cooldown:IsDebuff()
    return false
end

function modifier_ability_fire_trap_cooldown:IsPurgable()
    return false
end

function modifier_ability_fire_trap_cooldown:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ability_fire_trap_cooldown:IsStunDebuff()
    return false
end

function modifier_ability_fire_trap_cooldown:RemoveOnDeath()
    return true
end

function modifier_ability_fire_trap_cooldown:DestroyOnExpire()
    return true
end

function modifier_ability_fire_trap_cooldown:OnCreated(kv)
	if not IsServer() then
		return
	end
	self.center = Vector(kv.x, kv.y, 0)
	self.parent = EntIndexToHScript(kv.parent)
	self.damage = kv.damage
	self.level = kv.level
	self.radius = kv.radius + 50
	self:StartIntervalThink(0.5)
	self:OnIntervalThink()
end

function modifier_ability_fire_trap_cooldown:OnIntervalThink()
	if self.parent:IsNull() then
		self:Destroy()
		return
	end
	if (self:GetParent():GetAbsOrigin() - self.center):Length2D() <= self.radius then
		ApplyDamage({
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self.damage,
			damage_type = DAMAGE_TYPE_MAGICAL,
			damage_flags = 0,
			ability = self:GetAbility()
		})
	end
end

function modifier_ability_fire_trap_cooldown:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
end

function modifier_ability_fire_trap_cooldown:GetModifierAttackSpeedBonus_Constant()
	return 20 * self.level
end