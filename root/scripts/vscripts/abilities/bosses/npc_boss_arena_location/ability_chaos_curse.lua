ability_chaos_curse = class({})

LinkLuaModifier("modifier_ability_chaos_curse", "abilities/bosses/npc_boss_arena_location/ability_chaos_curse", LUA_MODIFIER_MOTION_NONE )

function ability_chaos_curse:Spawn()
    if not IsServer() then return end
    Timers:CreateTimer(FrameTime(),function()
        self:GetCaster():SetMaterialGroup("demon")
    end)
end

function ability_chaos_curse:OnSpellStart()
    local count = 0
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 99999, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    if #units > 0 then
        while count == 10 do
            local unit = RandomInt(1, #units)
            if unit then
                unit:AddNewModifier(self:GetCaster(), self, "modifier_ability_chaos_curse", {duration = 10})
                count = count + 1
            end
        end
    end
end

modifier_ability_chaos_curse = class({})
function modifier_ability_chaos_curse:IsHidden() return false end
function modifier_ability_chaos_curse:IsDebuff() return true end
function modifier_ability_chaos_curse:IsPurgable() return false end
function modifier_ability_chaos_curse:IsPurgeException() return false end
function modifier_ability_chaos_curse:IsStunDebuff() return false end
function modifier_ability_chaos_curse:RemoveOnDeath() return true end
function modifier_ability_chaos_curse:DestroyOnExpire() return true end

function modifier_ability_chaos_curse:OnCreated()
    if not IsServer() then return end
    
    local damage = 1000 * self:GetCaster():GetLevel()^2
    self:SetStackCount(1)

    EmitSoundOn("Hero_DoomBringer.InfernalBlade.Target", self:GetParent())
    EmitSoundOn("Hero_DoomBringer.InfernalBlade.ShredLayer", self:GetParent())
    local pcf = ParticleManager:CreateParticle("particles/econ/items/doom/doom_2021_immortal_weapon/doom_2021_immortal_weapon_infernalblade_debuff.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
    self:AddParticle(pcf, false, false, -1, false, false)
    local pcf = ParticleManager:CreateParticle("particles/econ/items/doom/doom_2021_immortal_weapon/doom_2021_immortal_weapon_infernalblade_impact.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
    self:AddParticle(pcf, false, false, -1, false, false)
    self:StartIntervalThink(1)
end

function modifier_ability_chaos_curse:OnRefresh()
    if not IsServer() then return end

    self:IncrementStackCount()
end

function modifier_ability_chaos_curse:OnDestroy()
    if not IsServer() then return end
    StopSoundOn("Hero_DoomBringer.InfernalBlade.ShredLayer", self:GetParent())
end

function modifier_ability_chaos_curse:OnIntervalThink()
    ApplyDamage({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = damage,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = DOTA_DAMAGE_FLAG_NONE,
        ability = self:GetAbility()
    })
end

function modifier_ability_chaos_curse:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
    }
end

function modifier_ability_chaos_curse:GetModifierMoveSpeedBonus_Percentage()
    return self:GetStackCount() * -20
end