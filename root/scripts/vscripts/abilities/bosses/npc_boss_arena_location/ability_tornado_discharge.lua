ability_tornado_discharge = class({})

LinkLuaModifier("modifier_ability_tornado_discharge_thinker", "abilities/bosses/npc_boss_arena_location/ability_tornado_discharge", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_ability_tornado_discharge_thinker_pool", "abilities/bosses/npc_boss_arena_location/ability_tornado_discharge", LUA_MODIFIER_MOTION_NONE )

function ability_tornado_discharge:OnSpellStart()
    self.damage = 0
    CreateModifierThinker(self:GetCaster(), self, "modifier_ability_tornado_discharge_thinker", {}, self:GetCaster():GetAbsOrigin(), self:GetCaster():GetTeamNumber(), false)
end

modifier_ability_tornado_discharge_thinker = class({})
--Classifications template
function modifier_ability_tornado_discharge_thinker:IsHidden()
    return true
end

function modifier_ability_tornado_discharge_thinker:IsDebuff()
    return false
end

function modifier_ability_tornado_discharge_thinker:IsPurgable()
    return false
end

function modifier_ability_tornado_discharge_thinker:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ability_tornado_discharge_thinker:IsStunDebuff()
    return false
end

function modifier_ability_tornado_discharge_thinker:RemoveOnDeath()
    return true
end

function modifier_ability_tornado_discharge_thinker:DestroyOnExpire()
    return true
end

function modifier_ability_tornado_discharge_thinker:OnCreated(kv)
    if not IsServer() then
        return
    end
    self:SetDuration(6, true)
    self.radius = 600

	self.effect_cast = ParticleManager:CreateParticle( "particles/invoker_magicka/attraction/attraction.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( self.effect_cast, 0, self:GetParent():GetOrigin() )
	-- ParticleManager:SetParticleControl( self.effect_cast, 1, Vector( self.radius, 0, 0 ) )
	EmitSoundOnLocationWithCaster( self:GetParent():GetOrigin(), "Hero_Invoker.EMP.Charge", self:GetCaster() )	
    self:AddParticle(self.effect_cast, false, false, -1, false, false)

    self.center = self:GetParent():GetAbsOrigin()
    self:StartIntervalThink(FrameTime())
end

function modifier_ability_tornado_discharge_thinker:OnIntervalThink()
    if not IsServer() then
        return
    end
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        local mod = unit:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_ability_tornado_discharge_thinker_pool", {ceter_x = self.center.x, center_y = self.center.y, center_z = self.center.z})
        mod.mod = self
    end
end

function modifier_ability_tornado_discharge_thinker:OnDestroy()
    if not IsServer() then
        return
    end
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.radius,  DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        ApplyDamage({
            victim = unit,
            attacker = self:GetCaster(),
            damage = self:GetAbility().damage / 2,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self:GetAbility()
        })
        local pcf = ParticleManager:CreateParticle("particles/econ/events/coal/coal_projectile_explosion.vpcf", PATTACH_POINT_FOLLOW, unit)
        ParticleManager:SetParticleControl(pcf, 0, unit:GetAbsOrigin())
        ParticleManager:SetParticleControl(pcf, 3, unit:GetAbsOrigin())
        ParticleManager:ReleaseParticleIndex(pcf)
    end
	EmitSoundOnLocationWithCaster( self:GetParent():GetOrigin(), "Hero_Invoker.EMP.Discharge", self:GetCaster() )
end

modifier_ability_tornado_discharge_thinker_pool = class({})
--Classifications template

function modifier_ability_tornado_discharge_thinker_pool:IsHidden()
    return false
end

function modifier_ability_tornado_discharge_thinker_pool:IsDebuff()
    return true
end

function modifier_ability_tornado_discharge_thinker_pool:IsPurgable()
    return false
end

function modifier_ability_tornado_discharge_thinker_pool:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_ability_tornado_discharge_thinker_pool:IsStunDebuff()
    return false
end

function modifier_ability_tornado_discharge_thinker_pool:RemoveOnDeath()
    return true
end

function modifier_ability_tornado_discharge_thinker_pool:DestroyOnExpire()
    return true
end

function modifier_ability_tornado_discharge_thinker_pool:OnCreated(kv)
    if not IsServer() then
        return
    end
    self.center = Vector(kv.ceter_x, kv.center_y, kv.center_z)
    local pcf = ParticleManager:CreateParticle("particles/invoker_magicka/attraction/attraction_pull_debuff.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControlEnt(pcf, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
    ParticleManager:SetParticleControl(pcf, 1, self.center)
    ParticleManager:SetParticleControl(pcf, 4, self.center)
    ParticleManager:SetParticleControl(pcf, 6, Vector(800,0,0))
    self:AddParticle(pcf, false, false, -1, false, false)
    self:StartIntervalThink(0.1)
end

function modifier_ability_tornado_discharge_thinker_pool:OnRefresh(kv)
    if not IsServer() then
        return
    end
    local damage = ApplyDamage({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = self:GetParent():GetMaxHealth() * 0.01,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = DOTA_DAMAGE_FLAG_NONE,
        ability = self:GetAbility()
    })
    self:GetAbility().damage = self:GetAbility().damage + damage
    self:StartIntervalThink(0.1)
end

function modifier_ability_tornado_discharge_thinker_pool:OnIntervalThink()
    self:Destroy()
end