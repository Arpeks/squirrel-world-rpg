ability_soul_fusion = class({})

LinkLuaModifier("modifier_ability_soul_fusion", "abilities/bosses/npc_boss_arena_location/ability_soul_fusion", LUA_MODIFIER_MOTION_NONE )

function ability_soul_fusion:OnSpellStart()
    self.units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 99999, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(self.units) do
        if unit:GetManaPercent() <= 50 then
            unit:AddNewModifier(self:GetCaster(), self, "modifier_ability_soul_fusion", {})
            self.mod = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_invulnerable", {})
        end
    end
end

function ability_soul_fusion:OnChannelFinish(bInterrupt)
    for _,unit in pairs(self.units) do
        unit:RemoveModifierByName("modifier_ability_soul_fusion")
    end
    self.mod:Destroy()
end

modifier_ability_soul_fusion = class({})
--Classifications template
function modifier_ability_soul_fusion:IsHidden()
	return false
end

function modifier_ability_soul_fusion:IsDebuff()
	return true
end

function modifier_ability_soul_fusion:IsPurgable()
	return false
end

function modifier_ability_soul_fusion:IsPurgeException()
	return true
end

-- Optional Classifications
function modifier_ability_soul_fusion:IsStunDebuff()
	return true
end

function modifier_ability_soul_fusion:RemoveOnDeath()
	return true
end

function modifier_ability_soul_fusion:DestroyOnExpire()
	return true
end

function modifier_ability_soul_fusion:OnCreated()
	self.damage = (0.03 + self:GetCaster():GetLevel() * 0.5) * self:GetParent():GetMaxHealth()
	self.drain = -0.2
	self.tick = 0.5
	if not IsServer() then
		return
	end
	EmitSoundOn("Hero_Bane.FiendsGrip", self:GetParent())
	self:StartIntervalThink(self.tick)
end

function modifier_ability_soul_fusion:OnRefresh()
	self:OnCreated()
end

function modifier_ability_soul_fusion:OnIntervalThink()
	local damage = ApplyDamage({
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = self.damage,
		damage_type = DAMAGE_TYPE_MAGICAL,
		damage_flags = DOTA_DAMAGE_FLAG_NONE,
		ability = self:GetAbility()
	})
	local drain = self.drain * self:GetParent():GetMaxMana()
	self:GetCaster():GiveMana(drain)
    if self:GetParent():GetMana() <=0 then
        self:Destroy()
    end
end

function modifier_ability_soul_fusion:OnDestroy()
	if not IsServer() then
		return
	end
	StopSoundOn("Hero_Bane.FiendsGrip", self:GetParent())
end

function modifier_ability_soul_fusion:GetEffectName()
	return "particles/units/heroes/hero_bane/bane_fiends_grip.vpcf"
end

function modifier_ability_soul_fusion:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION	
	}
end

function modifier_ability_soul_fusion:GetOverrideAnimation()
	return ACT_DOTA_FLAIL
end

function modifier_ability_soul_fusion:CheckState()
	return {
		[MODIFIER_STATE_STUNNED] = true
	}
end