ability_fire_shield = class({})

LinkLuaModifier("modifier_ability_fire_shield", "abilities/bosses/npc_boss_arena_location/ability_fire_shield", LUA_MODIFIER_MOTION_NONE )

function ability_fire_shield:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_ability_fire_shield", {duration = 5})
end

modifier_ability_fire_shield = class({})
function modifier_ability_fire_shield:IsHidden() return false end
function modifier_ability_fire_shield:IsDebuff() return false end
function modifier_ability_fire_shield:IsPurgable() return true end
function modifier_ability_fire_shield:IsPurgeException() return false end
function modifier_ability_fire_shield:IsStunDebuff() return false end
function modifier_ability_fire_shield:RemoveOnDeath() return true end
function modifier_ability_fire_shield:DestroyOnExpire() return true end

function modifier_ability_fire_shield:OnCreated()
    if not IsServer() then return end
    local pcf = ParticleManager:CreateParticle("particles/invoker_magicka/invoker_lotus_shield/invoker_lotus_shield.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
    ParticleManager:SetParticleControlEnt(pcf, 0, self:GetCaster(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
    ParticleManager:SetParticleControlEnt(pcf, 1, self:GetCaster(), PATTACH_ABSORIGIN_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
    self:AddParticle(pcf, false, false, -1, false, false)
    self.total_damage = 0
end

function modifier_ability_fire_shield:OnDestroy()
    if not IsServer() then return end
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        local direction = (self:GetCaster():GetAbsOrigin() - unit:GetAbsOrigin()):Normalized() * -1
        ApplyDamage({
            victim = unit,
            attacker = self:GetCaster(),
            damage = self.total_damage,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = DOTA_DAMAGE_FLAG_NONE,
            ability = self:GetAbility()
        })
        local arc = unit:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_generic_arc_lua",{
            dir_x = direction.x,
            dir_y = direction.y,
            speed = 1200,
            fix_end = false,
            duration = 0.2,
            isStun = true,
        })
    end
end

function modifier_ability_fire_shield:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_TOTAL_CONSTANT_BLOCK
    }
end

function modifier_ability_fire_shield:GetModifierTotal_ConstantBlock(data)
    if not IsServer() then return end
    print(data.damage)
    print(self.total_damage)
    self.total_damage = self.total_damage + data.damage / 2
    return data.damage / -2
end