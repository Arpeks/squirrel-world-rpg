ability_boss_areena_zone = class({})

LinkLuaModifier("modifier_ability_boss_areena_zone_active", "abilities/bosses/npc_boss_arena_location/ability_boss_areena_zone", LUA_MODIFIER_MOTION_NONE )

function ability_boss_areena_zone:OnSpellStart()
	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_ability_boss_areena_zone_active", {})
end

modifier_ability_boss_areena_zone_active = class({})
--Classifications template
function modifier_ability_boss_areena_zone_active:IsHidden()
	return true
end

function modifier_ability_boss_areena_zone_active:IsDebuff()
	return false
end

function modifier_ability_boss_areena_zone_active:IsPurgable()
	return false
end

function modifier_ability_boss_areena_zone_active:IsPurgeException()
	return false
end

-- Optional Classifications
function modifier_ability_boss_areena_zone_active:IsStunDebuff()
	return false
end

function modifier_ability_boss_areena_zone_active:RemoveOnDeath()
	return false
end

function modifier_ability_boss_areena_zone_active:DestroyOnExpire()
	return false
end

function modifier_ability_boss_areena_zone_active:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_ability_boss_areena_zone_active:OnCreated()
	if not IsServer() then
		return
	end
	self.caster	= self:GetCaster()
	self.ability	= self:GetAbility()
	self.blast_radius = 600
	self.blast_speed = 200
	self.damage = 10 + self:GetCaster():GetLevel() / 10
	self.blast_duration = self.blast_radius / self.blast_speed
	self.current_loc = self:GetCaster():GetAbsOrigin()
	
	self:GetCaster():EmitSound("DOTA_Item.ShivasGuard.Activate")

	local blast_pfx = ParticleManager:CreateParticle("particles/econ/events/ti9/shivas_guard_ti9_active.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
	ParticleManager:SetParticleControl(blast_pfx, 0, self:GetCaster():GetAbsOrigin())
	ParticleManager:SetParticleControl(blast_pfx, 1, Vector(self.blast_radius, self.blast_duration * 1.33, self.blast_speed))
	ParticleManager:ReleaseParticleIndex(blast_pfx)

	self.targets_hit = {}

	self.current_radius = 0
	self.tick_interval = 0.1
	self:StartIntervalThink(0.1)
end

function modifier_ability_boss_areena_zone_active:OnIntervalThink()
	AddFOWViewer(self:GetCaster():GetTeamNumber(), self.current_loc, self.current_radius, 0.1, false)

	self.current_radius = self.current_radius + self.blast_speed * self.tick_interval
	self.current_loc = self:GetCaster():GetAbsOrigin()

	local nearby_enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self.current_loc, nil, self.current_radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,enemy in pairs(nearby_enemies) do
		local enemy_has_been_hit = false
		for _,enemy_hit in pairs(self.targets_hit) do
			if enemy == enemy_hit then enemy_has_been_hit = true end
		end

		if not enemy_has_been_hit then
			local hit_pfx = ParticleManager:CreateParticle("particles/econ/events/ti9/shivas_guard_ti9_impact.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy)
			ParticleManager:SetParticleControl(hit_pfx, 0, enemy:GetAbsOrigin())
			ParticleManager:SetParticleControl(hit_pfx, 1, enemy:GetAbsOrigin())
			ParticleManager:ReleaseParticleIndex(hit_pfx)

			ApplyDamageRDA({
                victim = enemy, 
                attacker = self.caster, 
                damage = self.damage * enemy:GetMaxHealth(), 
                damage_type = DAMAGE_TYPE_MAGICAL,
                ability = self.ability
            })

			self.targets_hit[#self.targets_hit + 1] = enemy
		end
	end
	if self.current_radius > self.blast_radius then
		self:Destroy()
	end
end