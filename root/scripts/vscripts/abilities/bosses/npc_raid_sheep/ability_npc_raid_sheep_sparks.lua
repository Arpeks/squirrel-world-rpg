ability_npc_raid_sheep_sparks = class({})

LinkLuaModifier("modifier_raid_sheep_sparks", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_sparks", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_raid_sheep_sparks_thinker", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_sparks", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_raid_sheep_sparks_thinker_aura", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_sparks", LUA_MODIFIER_MOTION_NONE)

function ability_npc_raid_sheep_sparks:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_raid_sheep_sparks", {})
    self:GetCaster():EmitSound("Hero_ArcWarden.SparkWraith.Cast")
end

function ability_npc_raid_sheep_sparks:OnProjectileHit_ExtraData(hTarget, vLocation, table)
    if not hTarget then
        return
    end
    ApplyDamageRDA({
        victim = hTarget,
        attacker = self:GetCaster(),
        damage = hTarget:GetHealth() * 0.15,
        damage_type = DAMAGE_TYPE_MAGICAL,
        damage_flags = 0,
        ability = self
    })
    hTarget:EmitSound("Hero_ArcWarden.SparkWraith.Damage")
    UTIL_Remove(EntIndexToHScript(table.thinker))
end

modifier_raid_sheep_sparks = class({})
--Classifications template
function modifier_raid_sheep_sparks:IsHidden()
    return true
end

function modifier_raid_sheep_sparks:IsDebuff()
    return false
end

function modifier_raid_sheep_sparks:IsPurgable()
    return false
end

function modifier_raid_sheep_sparks:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_raid_sheep_sparks:IsStunDebuff()
    return false
end

function modifier_raid_sheep_sparks:RemoveOnDeath()
    return true
end

function modifier_raid_sheep_sparks:DestroyOnExpire()
    return true
end

function modifier_raid_sheep_sparks:OnCreated()
    if not IsServer() then
        return
    end
    self.value = 1
    self.radius = 700
    self.vectors = {
        self:GetParent():GetForwardVector(),
        self:GetParent():GetRightVector(),
        self:GetParent():GetForwardVector() * -1,
        self:GetParent():GetRightVector() * -1,
    }
    self:StartIntervalThink(0.1)
end

function modifier_raid_sheep_sparks:OnIntervalThink()
    if self.radius < 124 then
        self:Destroy()
        return
    end
    for i = 1, 4 do
        local new_vector = RotatePosition(Vector(0, 0, 0), QAngle(0, 10 * self.value, 0), self.vectors[i])
        local position = self:GetParent():GetOrigin() + new_vector * self.radius
        CreateModifierThinker(self:GetParent(), self:GetAbility(), "modifier_raid_sheep_sparks_thinker", {duration = 15}, position, self:GetCaster():GetTeamNumber(), false)
    end
    self.value = self.value + 1
    self.radius = self.radius - 36
end
        
modifier_raid_sheep_sparks_thinker = class({})
--Classifications template
function modifier_raid_sheep_sparks_thinker:IsHidden()
    return true
end

function modifier_raid_sheep_sparks_thinker:IsDebuff()
    return false
end

function modifier_raid_sheep_sparks_thinker:IsPurgable()
    return false
end

function modifier_raid_sheep_sparks_thinker:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_raid_sheep_sparks_thinker:IsStunDebuff()
    return false
end

function modifier_raid_sheep_sparks_thinker:RemoveOnDeath()
    return true
end

function modifier_raid_sheep_sparks_thinker:DestroyOnExpire()
    return true
end

function modifier_raid_sheep_sparks_thinker:OnCreated()
    if not IsServer() then
        return
    end
    self.active = false
    self.radius = 375
    local wraith_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_arc_warden/arc_warden_wraith.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(wraith_particle, 1, Vector(self.radius, 1, 1))
    self:AddParticle(wraith_particle, false, false, -1, false, false)
    self:StartIntervalThink(2)
end

function modifier_raid_sheep_sparks_thinker:OnIntervalThink()
    self.active = true
    self:StartIntervalThink(-1)
end

function modifier_raid_sheep_sparks_thinker:OnDestroy()
    if not IsServer() then
        return
    end
end

--------------------------------------------------------------------------------
-- Aura Effects
function modifier_raid_sheep_sparks_thinker:IsAura()
    return self.active
end

function modifier_raid_sheep_sparks_thinker:GetModifierAura()
    return "modifier_raid_sheep_sparks_thinker_aura"
end

function modifier_raid_sheep_sparks_thinker:GetAuraRadius()
    return self.radius
end

function modifier_raid_sheep_sparks_thinker:GetAuraDuration()
    return 0.1
end

function modifier_raid_sheep_sparks_thinker:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_raid_sheep_sparks_thinker:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC
end

function modifier_raid_sheep_sparks_thinker:GetAuraSearchFlags()
    return 0
end

modifier_raid_sheep_sparks_thinker_aura = class({})
--Classifications template
function modifier_raid_sheep_sparks_thinker_aura:IsHidden()
    return true
end

function modifier_raid_sheep_sparks_thinker_aura:IsDebuff()
    return true
end

function modifier_raid_sheep_sparks_thinker_aura:IsPurgable()
    return false
end

function modifier_raid_sheep_sparks_thinker_aura:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_raid_sheep_sparks_thinker_aura:IsStunDebuff()
    return false
end

function modifier_raid_sheep_sparks_thinker_aura:RemoveOnDeath()
    return true
end

function modifier_raid_sheep_sparks_thinker_aura:DestroyOnExpire()
    return true
end

function modifier_raid_sheep_sparks_thinker_aura:OnCreated()
    if not IsServer() then
        return
    end
    local thinker = self:GetAuraOwner()
    local mod = thinker:FindModifierByName("modifier_raid_sheep_sparks_thinker")
    if mod then
        self:GetParent():EmitSound("Hero_ArcWarden.SparkWraith.Activate")
        ProjectileManager:CreateTrackingProjectile({
            EffectName			= "particles/units/heroes/hero_arc_warden/arc_warden_wraith_prj.vpcf",
			Ability				= self:GetAbility(),
			Source				= thinker,
			vSourceLoc			= thinker:GetAbsOrigin(),
			Target				= self:GetParent(),
			iMoveSpeed			= 300,
			bDodgeable			= false,
			bIsAttack			= false,
			bReplaceExisting	= false,
			bVisibleToEnemies	= true,
            ExtraData = {
                thinker = thinker:entindex()
            }
		})
        mod:Destroy()
        self:Destroy()
    end
end