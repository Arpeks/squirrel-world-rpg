ability_npc_raid_sheep_levitate = class({})

LinkLuaModifier("modifier_npc_raid_sheep_levitate", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_levitate", LUA_MODIFIER_MOTION_BOTH)

function ability_npc_raid_sheep_levitate:OnSpellStart()
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 900,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        local mod = unit:FindModifierByName("modifier_npc_raid_sheep_levitate")
        if mod then
            mod:Destroy()
        end
        unit:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_levitate", {duration = 10})
    end
    self:GetCaster():EmitSound("Hero_Rubick.Telekinesis.Cast")
end

modifier_npc_raid_sheep_levitate = class({})
--Classifications template
function modifier_npc_raid_sheep_levitate:IsHidden()
    return false
end

function modifier_npc_raid_sheep_levitate:IsDebuff()
    return true
end

function modifier_npc_raid_sheep_levitate:IsPurgable()
    return false
end

function modifier_npc_raid_sheep_levitate:IsPurgeException()
    return true
end

-- Optional Classifications
function modifier_npc_raid_sheep_levitate:IsStunDebuff()
    return true
end

function modifier_npc_raid_sheep_levitate:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_levitate:DestroyOnExpire()
    return true
end

function modifier_npc_raid_sheep_levitate:OnCreated()
    if not IsServer() then
        return
    end
    self.parent = self:GetParent()
    self.state = "outmoving"
    local dir = self:GetParent():GetAbsOrigin() - self:GetCaster():GetAbsOrigin()
    dir.z = 0
    self.direction = dir:Normalized()
    self.random = Vector(RandomFloat(0, 1),RandomFloat(0, 1), 0):Normalized()
    self.radius = dir:Length2D()
    self.value = 1
    self.high = 1
    local pcf = ParticleManager:CreateParticle("particles/boses/ability_npc_raid_sheep_levitate.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.parent)
    ParticleManager:SetParticleControlEnt(pcf, 0, self.parent, PATTACH_POINT_FOLLOW, "attach_hitloc", self.parent:GetAbsOrigin(), true);
    ParticleManager:SetParticleControlEnt(pcf, 1, self.parent, PATTACH_POINT_FOLLOW, "attach_hitloc", self.parent:GetAbsOrigin(), true);
    ParticleManager:SetParticleControl(pcf, 2, Vector(100,0,0))
    self:AddParticle(pcf, false, false, -1, false, false)
    self:ApplyHorizontalMotionController()
    self:ApplyVerticalMotionController()
end

function modifier_npc_raid_sheep_levitate:OnDestroy()
    if not IsServer() then
        return
    end
    self:GetParent():RemoveHorizontalMotionController( self )
	self:GetParent():RemoveVerticalMotionController( self )
    FindClearSpaceForUnit(self:GetParent(),self:GetParent():GetOrigin(), false)
    self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_stunned", {duration = 5})
    local pcf = ParticleManager:CreateParticle("particles/units/heroes/hero_rubick/rubick_telekinesis_land.vpcf", PATTACH_CUSTOMORIGIN, nil)
    ParticleManager:SetParticleControl(pcf, 0, self:GetParent():GetOrigin())
    ParticleManager:ReleaseParticleIndex(pcf)
    self.parent:EmitSound("Hero_Rubick.Telekinesis.Target.Land")
    self.parent:StopSound("Hero_Rubick.Telekinesis.Target")
end

function modifier_npc_raid_sheep_levitate:UpdateHorizontalMotion(me, dt)
    if self.state == "outmoving" then
        local position = self:GetCaster():GetOrigin() + self.direction * self.radius
        self.radius = self.radius + 16
        if self.radius > 700 then
            self.radius = 700
            self.state = "rotate"
        end
        self:GetParent():SetAbsOrigin(position)
    elseif self.state == "rotate" then
        local new_vector = RotatePosition(Vector(0, 0, 0), QAngle(0, 5 * self.value, 0), self.direction)
        local position = self:GetCaster():GetOrigin() + new_vector * self.radius
        self.value = self.value + 1
        self.radius = self.radius - 8
        if self.radius < 0 then
            self.value = 1
            self.radius = 0
            self.state = "throw"
        end
        self:GetParent():SetAbsOrigin(position)
    elseif self.state == "throw" then
        local position = self:GetCaster():GetOrigin() + self.random * self.value * self.radius
        self.value = self.value + 0.02
        self.radius = self.radius + 16
        if self.radius > 600 then
            self:Destroy()
        end
        self:GetParent():SetAbsOrigin(position)
    end
end

function modifier_npc_raid_sheep_levitate:UpdateVerticalMotion(me, dt)
    if self.state == "rotate" then
        local pos = self:GetParent():GetOrigin()
        local high = GetGroundHeight( pos, me ) + self.high
        self.high = self.high + 4
        if self.high > 300 then
            self.high = 300
        end
        self:GetParent():SetOrigin(Vector(pos.x, pos.y, high))
    elseif self.state == "throw" then
        self.high = self.high - 10
        if self.high < 0 then
            self.high = 0
        end
        local pos = self:GetParent():GetOrigin()
        local high = GetGroundHeight( pos, me ) + self.high
        self:GetParent():SetOrigin(Vector(pos.x, pos.y, high))
    end
end

function modifier_npc_raid_sheep_levitate:OnHorizontalMotionInterrupted()
	self:Destroy()
end

function modifier_npc_raid_sheep_levitate:OnVerticalMotionInterrupted()
	self:Destroy()
end

function modifier_npc_raid_sheep_levitate:CheckState()
    return {
        [MODIFIER_STATE_STUNNED] = true,
        [MODIFIER_STATE_COMMAND_RESTRICTED] = true,
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
    }
end


function modifier_npc_raid_sheep_levitate:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_OVERRIDE_ANIMATION
    }
end

function modifier_npc_raid_sheep_levitate:GetOverrideAnimation()
	return ACT_DOTA_FLAIL
end