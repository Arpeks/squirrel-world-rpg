ability_npc_raid_sheep_armor = class({})

LinkLuaModifier("modifier_npc_raid_sheep_armor", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_armor", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_npc_raid_sheep_heal_state", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_armor", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("modifier_npc_raid_sheep_heal_state_target", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_armor", LUA_MODIFIER_MOTION_NONE )

function ability_npc_raid_sheep_armor:GetIntrinsicModifierName()
    return "modifier_npc_raid_sheep_armor"
end

modifier_npc_raid_sheep_armor = class({})
--Classifications template
function modifier_npc_raid_sheep_armor:IsHidden()
    return false
end

function modifier_npc_raid_sheep_armor:IsDebuff()
    return false
end

function modifier_npc_raid_sheep_armor:IsPurgable()
    return false
end

function modifier_npc_raid_sheep_armor:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_npc_raid_sheep_armor:IsStunDebuff()
    return false
end

function modifier_npc_raid_sheep_armor:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_armor:DestroyOnExpire()
    return false
end

function modifier_npc_raid_sheep_armor:OnStackCountChanged(iStackCount)
    if not IsServer() then
        return
    end
    if self:GetParent():HasModifier("modifier_npc_raid_sheep_heal_state") then
        return
    end
    if self:GetStackCount() / (self.max*0.1) < 2 then
        if not self:GetParent():HasAbility("ability_npc_raid_sheep_levitate") then
            self:GetParent():AddAbility("ability_npc_raid_sheep_levitate")
        end
        self:GetParent():SetContextThink(DoUniqueString("s"), function(unit)
            unit.bSearchedForSpells = false
        end, 0.1)
    elseif self:GetStackCount() / (self.max*0.1) < 4 then
        if not self:GetParent():HasAbility("ability_npc_raid_sheep_spears") then
            self:GetParent():AddAbility("ability_npc_raid_sheep_spears")
        end
        self:GetParent().bSearchedForSpells = false
    elseif self:GetStackCount() / (self.max*0.1) < 6 then
        if not self:GetParent():HasAbility("ability_npc_raid_sheep_chain") then
            self:GetParent():AddAbility("ability_npc_raid_sheep_chain")
        end
        self:GetParent().bSearchedForSpells = false
    elseif self:GetStackCount() / (self.max*0.1) < 8 then
        if not self:GetParent():HasAbility("ability_npc_raid_sheep_sparks") then
            self:GetParent():AddAbility("ability_npc_raid_sheep_sparks")
        end
        self:GetParent().bSearchedForSpells = false
    end
    if self:GetStackCount() <= 0  then
        self:SetStackCount(1)
    end
    if RollPercentage(5) then
        Notifications:TopToAll({text="raid_sheep_heal_state",style={color="red",["font-size"]="60px"}, duration=10})
        local rand = RandomInt(1,3)
        local t = {}
        local units = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 9999, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_ANY_ORDER, false)
        repeat
            local r = units[RandomInt(1,#units)]
            if not table.has_value(t) and r ~= self:GetCaster() and r:GetUnitName() ~= "npc_dota_monkey_clone_custom" then
                table.insert( t, r )
            end
        until #t == rand
        local modifier = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_heal_state", {duration = 60})
        modifier:SetStackCount(rand)
        for _,u in pairs(t) do
            local mod = u:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_heal_state_target", {duration = 60})
            mod.armor_modifier = self
            mod.invul_modifier = modifier
            pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_wisp/wisp_tether.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
            ParticleManager:SetParticleControlEnt(pfx, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
            ParticleManager:SetParticleControlEnt(pfx, 1, u, PATTACH_POINT_FOLLOW, "attach_hitloc", u:GetAbsOrigin(), true)
            mod:AddParticle( pfx, false, false, -1, false, false )
        end
    end
end

function modifier_npc_raid_sheep_armor:OnCreated()
    self.multiplier = 1
    if not IsServer() then
        return
    end   
    self.abilities = {
        ["ability_npc_raid_sheep_sparks"] = 8,
        ["ability_npc_raid_sheep_chain"] = 6,
        ["ability_npc_raid_sheep_spears"] = 4,
        ["ability_npc_raid_sheep_levitate"] = 2,
    }
    self.stacks = {
        ["Easy"] = 20000,
        ["Normal"] = 10000,
        ["Hard"] = 10000,
        ["Ultra"] = 15000,
        ["Insane"] = 17500,
        ["Impossible"] = 20000,
        ["Unreal"] = 20000,
    }
    print(self.stacks[diff_wave.wavedef])
    self.max = self.stacks[diff_wave.wavedef]
    self:SetStackCount(self.stacks[diff_wave.wavedef])
    -- if IsInToolsMode() then
    --     self.max = 10
    --     self:SetStackCount(10)
    -- end
    self.spheres = {
        ["item_forest_soul"] = 1,
        ["item_village_soul"] = 2,
        ["item_mines_soul"] = 3,
        ["item_dust_soul"] = 4,
        ["item_cemetery_soul"] = 5,
        ["item_swamp_soul"] = 6,
        ["item_snow_soul"] = 7,
        ["item_divine_soul"] = 8,
        ["item_magma_soul"] = 9,
        ["item_antimage_soul"] = 25,
        ["item_dragon_soul"] = 15,
        ["item_dragon_soul_2"] = 15,
        ["item_dragon_soul_3"] = 15,
    }
    self.radius = 500

    local pcf = ParticleManager:CreateParticle("particles/boses/radius_collector.vpcf", PATTACH_POINT_FOLLOW, self:GetParent())
    ParticleManager:SetParticleControl(pcf, 1, Vector(self.radius, 0, 0))
    self:AddParticle(pcf, false, false, -1, false, false)
    self:StartIntervalThink(0.5)
    self:SetHasCustomTransmitterData( true )

    Notifications:TopToAll({text="raid_sheep_heal_state",style={color="red",["font-size"]="60px"}, duration=10})
    local rand = RandomInt(1,3)
    local t = {}
    local units = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 9999, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_ANY_ORDER, false)
    for k,v in pairs(units) do
        print(v:GetUnitName())
    end
    repeat
        local r = units[RandomInt(1,#units)]
        if not table.has_value(t) and r ~= self:GetCaster() and r:GetUnitName() ~= "npc_dota_monkey_clone_custom" then
            table.insert( t, r )
        end
    until #t == rand
    local modifier = self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_heal_state", {duration = 60})
    modifier:SetStackCount(rand)
    for _,u in pairs(t) do
        local mod = u:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_heal_state_target", {duration = 60})
        mod.armor_modifier = self
        mod.invul_modifier = modifier
        pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_wisp/wisp_tether.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
        ParticleManager:SetParticleControlEnt(pfx, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
        ParticleManager:SetParticleControlEnt(pfx, 1, u, PATTACH_POINT_FOLLOW, "attach_hitloc", u:GetAbsOrigin(), true)
        mod:AddParticle( pfx, false, false, -1, false, false )
    end
end

function modifier_npc_raid_sheep_armor:OnIntervalThink()
    if self:GetParent():HasModifier("modifier_npc_raid_sheep_heal_state") then
        return
    end
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.radius,  DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,u in pairs(units) do
        local soul = StashOfSouls:RemoveRandomSoul(u:GetPlayerID())
        if soul ~= nil then
            self:SetStackCount(self:GetStackCount() - math.floor(self.spheres[soul] * self.multiplier))
            self.multiplier = self.multiplier + 0.01
            break
        end
    end
    self:SendBuffRefreshToClients()
end

function modifier_npc_raid_sheep_armor:GetPriority()
    return MODIFIER_PRIORITY_SUPER_ULTRA
end

function modifier_npc_raid_sheep_armor:CheckState()
    return {
        [MODIFIER_STATE_NO_UNIT_COLLISION] = true,
        [MODIFIER_STATE_NO_HEALTH_BAR] = true,
        [MODIFIER_STATE_MAGIC_IMMUNE] = self:OnTooltip() > 100,
        [MODIFIER_STATE_ATTACK_IMMUNE] = self:OnTooltip() > 100
    }
end

function modifier_npc_raid_sheep_armor:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL,
        MODIFIER_PROPERTY_OVERRIDE_ABILITY_SPECIAL_VALUE,

        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,

        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_MAGICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PHYSICAL,
        MODIFIER_PROPERTY_ABSOLUTE_NO_DAMAGE_PURE,

        MODIFIER_PROPERTY_TOOLTIP,
        MODIFIER_EVENT_ON_MODIFIER_ADDED
    }
end

function modifier_npc_raid_sheep_armor:OnModifierAdded(data)
    if data.unit == self:GetParent() and data.added_buff ~= self and (data.added_buff:HasFunction(MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE) or (data.added_buff:GetName() == "modifier_broodmother_steal_debuff")) then
        data.added_buff:Destroy()
    end
end

function modifier_npc_raid_sheep_armor:GetAbsoluteNoDamagePhysical()
    return self:OnTooltip() > 100 and 1 or 0
end

function modifier_npc_raid_sheep_armor:GetAbsoluteNoDamageMagical()
    return self:OnTooltip() > 100 and 1 or 0
end

function modifier_npc_raid_sheep_armor:GetAbsoluteNoDamagePure()
    return self:OnTooltip() > 100 and 1 or 0
end

function modifier_npc_raid_sheep_armor:GetModifierTotalDamageOutgoing_Percentage()
    return self:GetStackCount() / (self.max*0.1) * 10 + 30
end

function modifier_npc_raid_sheep_armor:OnTooltip()
    return (0.15 * self:GetStackCount()) / (0.01 + 0.15 * self:GetStackCount()) * -1 * 100 - 0.1
end

function modifier_npc_raid_sheep_armor:GetModifierIncomingDamage_Percentage()
    return (0.15 * self:GetStackCount()) / (0.01 + 0.15 * self:GetStackCount()) * -1 * 100 - 0.1
end

function modifier_npc_raid_sheep_armor:AddCustomTransmitterData()
    return {
        multiplier = self.multiplier,
    }
end

function modifier_npc_raid_sheep_armor:HandleCustomTransmitterData( data )
    self.multiplier = data.multiplier
end

function modifier_npc_raid_sheep_armor:GetModifierOverrideAbilitySpecial(data)
    if data.ability == self:GetAbility() and data.ability_special_value == "multiplier" then
        return 1
    end
end

function modifier_npc_raid_sheep_armor:GetModifierOverrideAbilitySpecialValue(data)
    if data.ability == self:GetAbility() and data.ability_special_value == "multiplier" then
        return self.multiplier
    end
end























modifier_npc_raid_sheep_heal_state = class({})
--Classifications template
function modifier_npc_raid_sheep_heal_state:IsHidden()
    return false
end

function modifier_npc_raid_sheep_heal_state:IsDebuff()
    return false
end

function modifier_npc_raid_sheep_heal_state:IsPurgable()
    return false
end

function modifier_npc_raid_sheep_heal_state:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_npc_raid_sheep_heal_state:IsStunDebuff()
    return false
end

function modifier_npc_raid_sheep_heal_state:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_heal_state:DestroyOnExpire()
    return true
end

function modifier_npc_raid_sheep_heal_state:OnStackCountChanged()
    if not IsServer() then
        return
    end
    if self:GetStackCount() == 0 then
        self:Destroy()
    end
end

function modifier_npc_raid_sheep_heal_state:OnCreated()
    if not IsServer() then
        return
    end
    EmitSoundOn("Hero_Winter_Wyvern.ColdEmbrace.Cast", self:GetParent())
    EmitSoundOn("Hero_Winter_Wyvern.ColdEmbrace", self:GetParent())
end

function modifier_npc_raid_sheep_heal_state:OnDestroy()
    if not IsServer() then
        return
    end
    StopSoundOn("Hero_Winter_Wyvern.ColdEmbrace", self:GetParent())
end

function modifier_npc_raid_sheep_heal_state:CheckState()
    return {
        [MODIFIER_STATE_INVULNERABLE] = true
    }
end

function modifier_npc_raid_sheep_heal_state:GetEffectName()
    return "particles/econ/items/winter_wyvern/winter_wyvern_ti7/wyvern_cold_embrace_ti7buff.vpcf"
end

function modifier_npc_raid_sheep_heal_state:GetEffectAttachType()
    return PATTACH_POINT_FOLLOW
end























modifier_npc_raid_sheep_heal_state_target = class({})
--Classifications template
function modifier_npc_raid_sheep_heal_state_target:IsHidden()
    return true
end

function modifier_npc_raid_sheep_heal_state_target:IsDebuff()
    return false
end

function modifier_npc_raid_sheep_heal_state_target:IsPurgable()
    return false
end

function modifier_npc_raid_sheep_heal_state_target:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_npc_raid_sheep_heal_state_target:IsStunDebuff()
    return false
end

function modifier_npc_raid_sheep_heal_state_target:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_heal_state_target:DestroyOnExpire()
    return false
end

function modifier_npc_raid_sheep_heal_state_target:OnCreated()
    if not IsServer() then
        return
    end
    self:GetParent():RemoveModifierByName("modifier_creep_antilag")
    self.fow = AddFOWViewer(DOTA_TEAM_GOODGUYS, self:GetParent():GetAbsOrigin(), 300, 100, true)
    self:StartIntervalThink(0.5)
end

function modifier_npc_raid_sheep_heal_state_target:OnIntervalThink()
    self.armor_modifier:IncrementStackCount()
end

function modifier_npc_raid_sheep_heal_state_target:OnDestroy()
    if not IsServer() then
        return
    end
    self.invul_modifier:DecrementStackCount()
    RemoveFOWViewer(DOTA_TEAM_GOODGUYS,self.fow)
end