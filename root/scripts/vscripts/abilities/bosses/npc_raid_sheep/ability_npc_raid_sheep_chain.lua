ability_npc_raid_sheep_chain = class({})

LinkLuaModifier("modifier_npc_raid_sheep_chain_jump", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_chain", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_npc_raid_sheep_chain_slow", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_chain", LUA_MODIFIER_MOTION_NONE)

function ability_npc_raid_sheep_chain:OnSpellStart()
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 700,  DOTA_UNIT_TARGET_TEAM_ENEMY,  DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for i,unit in pairs(units) do
        if i == 5 then 
            break
        end
        self.projectile_info = {
            Target = unit,
            Source = self:GetCaster(),
            Ability = self,	
            
            EffectName = "particles/econ/items/lich/lich_ti8_immortal_arms/lich_ti8_chain_frost.vpcf",
            iMoveSpeed = 850,
            bDodgeable = false,
        
            bVisibleToEnemies = true,
            ExtraData = {
                ProjectileID = i,
                Jumps = 0
            }
        }
        ProjectileManager:CreateTrackingProjectile(self.projectile_info)
    end
	EmitSoundOn( "Hero_Lich.ChainFrost", self:GetCaster() )
    self.spires = {}
    for i = 1,4 do
        local direction = RotatePosition(Vector(0, 0, 0), QAngle(0, 45 + 90 * i, 0), self:GetCaster():GetForwardVector()) * 550
        local unit = CreateUnitByName("npc_dota_lich_ice_spire", direction + self:GetCaster():GetOrigin(), true, nil, nil, self:GetCaster():GetTeamNumber())
	    unit:AddNewModifier(self:GetCaster(), nil, "modifier_invulnerable", {})
        unit:AddNewModifier(self:GetCaster(), self, "modifier_kill", {duration = 40})
        table.insert( self.spires, unit )
        unit:SetContextThink(DoUniqueString("aaa"), function(unit)
            unit:SetAbsOrigin(direction + self:GetCaster():GetOrigin())
            return FrameTime()
        end, FrameTime())
    end
end

function ability_npc_raid_sheep_chain:OnProjectileHit_ExtraData(hTarget, vLocation, table)
    if not hTarget then
        return
    end
    local ProjectileID = table.ProjectileID
    local Jumps = table.Jumps + 1
    hTarget:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_chain_jump", {duration = 0.2, ProjectileID = ProjectileID, Jumps = Jumps})
    hTarget:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_chain_slow", {duration = 3})
    local sound_target = "Hero_Lich.ChainFrostImpact.Creep"
	if hTarget:IsConsideredHero() then
		sound_target = "Hero_Lich.ChainFrostImpact.Hero"
	end
	EmitSoundOn( sound_target, hTarget )
end

modifier_npc_raid_sheep_chain_jump = class({})
--Classifications template
function modifier_npc_raid_sheep_chain_jump:IsHidden()
    return true
end

function modifier_npc_raid_sheep_chain_jump:IsDebuff()
    return true
end

function modifier_npc_raid_sheep_chain_jump:IsPurgable()
    return true
end

function modifier_npc_raid_sheep_chain_jump:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_npc_raid_sheep_chain_jump:IsStunDebuff()
    return false
end

function modifier_npc_raid_sheep_chain_jump:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_chain_jump:DestroyOnExpire()
    return true
end

function modifier_npc_raid_sheep_chain_jump:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_npc_raid_sheep_chain_jump:OnCreated(data)
    if not IsServer() then
        return
    end
    self.ProjectileID = data.ProjectileID
    self.Jumps = data.Jumps
    self.projectile_info = {
        -- Target = unit,
        Source = self:GetCaster(),
        Ability = self:GetAbility(),	
        
        EffectName = "particles/econ/items/lich/lich_ti8_immortal_arms/lich_ti8_chain_frost.vpcf",
        iMoveSpeed = 850,
        bDodgeable = false,
    
        bVisibleToEnemies = true,
        ExtraData = {
            ProjectileID = self.ProjectileID,
            Jumps = self.Jumps
        }
    }
    self.jumped = false
    if self:GetParent():GetUnitName() ~= "npc_dota_lich_ice_spire" then
        ApplyDamageRDA({
            victim = self:GetParent(),
            attacker = self:GetCaster(),
            damage = self:GetParent():GetHealth() * self.Jumps / 100 * 1.5,
            damage_type = DAMAGE_TYPE_MAGICAL,
            damage_flags = 0,
            ability = self:GetAbility()
        })
    end
end

function modifier_npc_raid_sheep_chain_jump:OnDestroy()
    if not IsServer() then
        return
    end
    if self.Jumps > 150 then
        return
    end
    local units = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, 700,  DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
    for _,unit in pairs(units) do
        if unit ~= self:GetParent() then
            self.projectile_info.Target = unit
            self.projectile_info.Source = self:GetParent()
            ProjectileManager:CreateTrackingProjectile(self.projectile_info)
            return
        end
    end
    local spire = self:GetAbility():GetClosedSpire(self:GetParent())
    self.projectile_info.Target = spire
    self.projectile_info.Source = self:GetParent()
    ProjectileManager:CreateTrackingProjectile(self.projectile_info)
end

function ability_npc_raid_sheep_chain:GetClosedSpire(current_target)
    local len = 2500
    local pos = current_target:GetAbsOrigin()
    local closest = nil
    for i,spire in pairs(self.spires) do
        if spire ~= current_target then
            local dist = (spire:GetAbsOrigin() - pos):Length2D()
            if dist < len then
                len = dist
                closest = spire
            end
        end
    end
    return closest
end

modifier_npc_raid_sheep_chain_slow = class({})
--Classifications template
function modifier_npc_raid_sheep_chain_slow:IsHidden()
    return false
end

function modifier_npc_raid_sheep_chain_slow:IsDebuff()
    return true
end

function modifier_npc_raid_sheep_chain_slow:IsPurgable()
    return true
end

function modifier_npc_raid_sheep_chain_slow:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_npc_raid_sheep_chain_slow:IsStunDebuff()
    return false
end

function modifier_npc_raid_sheep_chain_slow:RemoveOnDeath()
    return false
end

function modifier_npc_raid_sheep_chain_slow:DestroyOnExpire()
    return true
end

function modifier_npc_raid_sheep_chain_slow:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
        MODIFIER_PROPERTY_ATTACKSPEED_REDUCTION_PERCENTAGE,
    }
end

function modifier_npc_raid_sheep_chain_slow:GetModifierMoveSpeedBonus_Percentage()
    return -80
end

function modifier_npc_raid_sheep_chain_slow:GetModifierAttackSpeedReductionPercentage()
    return 80
end