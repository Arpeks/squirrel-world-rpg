ability_npc_raid_sheep_spears = class({})

LinkLuaModifier("modifier_npc_raid_sheep_spears", "abilities/bosses/npc_raid_sheep/ability_npc_raid_sheep_spears", LUA_MODIFIER_MOTION_NONE)

function ability_npc_raid_sheep_spears:OnSpellStart()
    local direction = self:GetCaster():GetForwardVector() * -1
    self.pinned = {}
    for i=1,8 do 
        local new_direction = RotatePosition(Vector(0, 0, 0), QAngle(0, (360/8) * i, 0), direction)
        local info = {
            Source = self:GetCaster(),
            Ability = self,
            vSpawnOrigin = self:GetCaster():GetOrigin() + new_direction * 900,
            
            bDeleteOnHit = false,
            
            iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
            iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
            
            EffectName = "particles/units/heroes/hero_mars/mars_spear.vpcf",
            fDistance = 900,
            fStartRadius = 125,
            fEndRadius = 125,
            vVelocity = new_direction * 900 * -1,
        
            bHasFrontalCone = false,
            bReplaceExisting = false,
            -- fExpireTime = GameRules:GetGameTime() + 10.0,
        }
        local id = ProjectileManager:CreateLinearProjectile(info)
        self.pinned[id] = nil
    end
	EmitSoundOn( "Hero_Mars.Spear.Cast", self:GetCaster() )
	EmitSoundOn( "Hero_Mars.Spear", self:GetCaster() )
end

function ability_npc_raid_sheep_spears:OnProjectileHitHandle(hTarget, vLocation, iProjectileHandle)
    if not hTarget then
        return
    end
    if not self:IsPinned(hTarget) then
        self.pinned[iProjectileHandle] = hTarget
        hTarget:AddNewModifier(self:GetCaster(), self, "modifier_npc_raid_sheep_spears", {Projectile = iProjectileHandle})
    end
end

function ability_npc_raid_sheep_spears:OnProjectileThinkHandle(iProjectileHandle)
    if self.pinned[iProjectileHandle] ~= nil then
        self.pinned[iProjectileHandle]:SetAbsOrigin(ProjectileManager:GetLinearProjectileLocation(iProjectileHandle))
    end
end

function ability_npc_raid_sheep_spears:IsPinned(hTarget)
    for _,unit in pairs(self.pinned) do
        if unit == hTarget then
            return true
        end
    end
    return false
end

modifier_npc_raid_sheep_spears = class({})
--Classifications template
function modifier_npc_raid_sheep_spears:IsHidden()
    return false
end

function modifier_npc_raid_sheep_spears:IsDebuff()
    return true
end

function modifier_npc_raid_sheep_spears:IsPurgable()
    return false
end

function modifier_npc_raid_sheep_spears:IsPurgeException()
    return true
end

-- Optional Classifications
function modifier_npc_raid_sheep_spears:IsStunDebuff()
    return true
end

function modifier_npc_raid_sheep_spears:RemoveOnDeath()
    return true
end

function modifier_npc_raid_sheep_spears:DestroyOnExpire()
    return true
end

function modifier_npc_raid_sheep_spears:OnCreated(data)
    if not IsServer() then
        return
    end
    self.Projectile = data.Projectile
    self:StartIntervalThink(FrameTime())
end

function modifier_npc_raid_sheep_spears:OnIntervalThink()
    if not ProjectileManager:IsValidProjectile(self.Projectile) then
        self:Destroy()
    end
end

function modifier_npc_raid_sheep_spears:OnDestroy()
    if not IsServer() then
        return
    end
    FindClearSpaceForUnit(self:GetParent(), self:GetParent():GetAbsOrigin(), false)
end

function modifier_npc_raid_sheep_spears:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}
end

function modifier_npc_raid_sheep_spears:GetOverrideAnimation( params )
	return ACT_DOTA_DISABLED
end

function modifier_npc_raid_sheep_spears:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_STUNNED] = true,
		[MODIFIER_STATE_INVISIBLE] = false,
	}
end

function modifier_npc_raid_sheep_spears:GetEffectName()
	return "particles/units/heroes/hero_mars/mars_spear_impact_debuff.vpcf"
end

function modifier_npc_raid_sheep_spears:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end

function modifier_npc_raid_sheep_spears:GetStatusEffectName()
	return "particles/status_fx/status_effect_mars_spear.vpcf"
end