LinkLuaModifier("modifier_summoned_unit_lifetime", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_3", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_dynamic_attack_target", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_3", LUA_MODIFIER_MOTION_NONE)

cemetery_secret_boss_spell_3 = class({})

function cemetery_secret_boss_spell_3:OnSpellStart()
    local caster = self:GetCaster()
    local summon_duration = self:GetSpecialValueFor("duration")
    local summon_distance = 200

    -- Вычисляем позиции для призыва юнитов (немного справа и слева за спиной)
    local left_summon_position = caster:GetAbsOrigin() - caster:GetForwardVector() * summon_distance + caster:GetRightVector() * (-summon_distance / 2)
    local right_summon_position = caster:GetAbsOrigin() - caster:GetForwardVector() * summon_distance + caster:GetRightVector() * (summon_distance / 2)

    local unit_left = CreateUnitByName("npc_quest_cemetery_secret_boss_ghost", left_summon_position, true, caster, caster, caster:GetTeamNumber())
    unit_left:SetControllableByPlayer(caster:GetPlayerID(), true)
    unit_left:SetOwner(caster)
    unit_left:AddNewModifier(caster, self, "modifier_summoned_unit_lifetime", {duration = summon_duration})
    unit_left:AddNewModifier(caster, self, "modifier_dynamic_attack_target", {duration = summon_duration, unit_left = true})

    local unit_right = CreateUnitByName("npc_quest_cemetery_secret_boss_ghost", right_summon_position, true, caster, caster, caster:GetTeamNumber())
    unit_right:SetControllableByPlayer(caster:GetPlayerID(), true)
    unit_right:SetOwner(caster)
    unit_right:AddNewModifier(caster, self, "modifier_summoned_unit_lifetime", {duration = summon_duration})
    unit_right:AddNewModifier(caster, self, "modifier_dynamic_attack_target", {duration = summon_duration, unit_right = true})
end

modifier_dynamic_attack_target = class({})

function modifier_dynamic_attack_target:IsHidden()
    return true
end

function modifier_dynamic_attack_target:IsPurgable()
    return false
end

function modifier_dynamic_attack_target:OnCreated(kv)
    self.unit_left = kv.unit_left
    self.unit_right = kv.unit_right
    if IsServer() then
        self:StartIntervalThink(0.5)
    end
end

function modifier_dynamic_attack_target:OnIntervalThink()
    if IsServer() then
        local caster = self:GetCaster()
        local parent = self:GetParent()
        local target = caster:GetAggroTarget()

        if target then
            parent:SetForceAttackTarget(target)
        else
            parent:SetForceAttackTarget(nil)
            -- Определяем позиции позади призывателя с учетом смещения
            local left_position = caster:GetAbsOrigin() - caster:GetForwardVector() * 200 + caster:GetRightVector() * (-100)
            local right_position = caster:GetAbsOrigin() - caster:GetForwardVector() * 200 + caster:GetRightVector() * (100)

            if self.unit_left then
                parent:MoveToPosition(left_position)
            else
                parent:MoveToPosition(right_position)
            end
        end
    end
end

modifier_summoned_unit_lifetime = class({})

function modifier_summoned_unit_lifetime:OnCreated(kv)
    if IsServer() then
        self:StartIntervalThink(self:GetDuration())
    end
end

function modifier_summoned_unit_lifetime:OnIntervalThink()
    if IsServer() then
        self:GetParent():ForceKill(false)
    end
end

function modifier_summoned_unit_lifetime:IsHidden()
    return true
end

function modifier_summoned_unit_lifetime:IsPurgable()
    return false
end

function modifier_summoned_unit_lifetime:RemoveOnDeath()
    return true
end