modifier_cemetery_secret_boss_ai = class({})

function modifier_cemetery_secret_boss_ai:IsHidden()
    return true
end

function modifier_cemetery_secret_boss_ai:IsDebuff()
    return false
end

function modifier_cemetery_secret_boss_ai:IsPurgable()
    return false
end

function modifier_cemetery_secret_boss_ai:IsPurgeException()
    return false
end

-- Optional Classifications
function modifier_cemetery_secret_boss_ai:IsStunDebuff()
    return false
end

function modifier_cemetery_secret_boss_ai:RemoveOnDeath()
    return false
end

function modifier_cemetery_secret_boss_ai:DestroyOnExpire()
    return false
end

function modifier_cemetery_secret_boss_ai:OnCreated()
    self.parent = self:GetParent()
    if not IsServer() then
        return
    end
    self.spawnPoint = self:GetParent():GetAbsOrigin()
    self.abi1 = self.parent:FindAbilityByName("cemetery_secret_boss_spell_1")
    self.abi2 = self.parent:FindAbilityByName("cemetery_secret_boss_spell_2")
    self.abi3 = self.parent:FindAbilityByName("cemetery_secret_boss_spell_3")
    self.abi4 = self.parent:FindAbilityByName("cemetery_secret_boss_spell_4")
    self:StartIntervalThink(1)
end

function modifier_cemetery_secret_boss_ai:OnIntervalThink()
    if self.parent:IsChanneling() then
        return
    end
    local units = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 900, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)
    if #units > 0 then
        if self.abi1:IsFullyCastable() then
            self:GetParent():CastAbilityOnTarget(units[RandomInt(1, #units)], self.abi1, -1)
            return
        end
        if self.abi2:IsFullyCastable() then
            self:GetParent():CastAbilityNoTarget(self.abi2, -1)
            return
        end
        if self.abi3:IsFullyCastable() then
            self:GetParent():CastAbilityNoTarget(self.abi3, -1)
            return
        end
        if self.abi4:IsFullyCastable() then
            self:GetParent():CastAbilityNoTarget(self.abi4, -1)
            return
        end
        self.parent:MoveToTargetToAttack(units[1])
    else
        self.parent:MoveToPosition(self.spawnPoint)
    end
end