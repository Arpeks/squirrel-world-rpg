LinkLuaModifier( "modifier_generic_stunned_lua", "heroes/generic/modifier_generic_stunned_lua", LUA_MODIFIER_MOTION_NONE )
cemetery_secret_boss_spell_1 = class({})

function cemetery_secret_boss_spell_1:OnSpellStart()
    ProjectileManager:CreateTrackingProjectile({
        Ability = self,
        Target = self:GetCursorTarget(),
        Source = self:GetCaster(),
        EffectName = "particles/units/heroes/hero_demonartist/demonartist_unstable_concoction_projectile.vpcf",
        iMoveSpeed = 1000,
        iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_HITLOCATION,
        bDodgeable = false,
        fExpireTime = GameRules:GetGameTime() + 20.0,
        ExtraData = {
			
		}
    })
end

function cemetery_secret_boss_spell_1:OnProjectileHit_ExtraData( target, location, ExtraData )
    EmitSoundOn("Hero_Oracle.PreAttack", target)
    target:AddNewModifier(self:GetCaster(), self, "modifier_cemetery_secret_boss_spell_1", {duration = self:GetSpecialValueFor("duration")})
    ApplyDamageRDA({
        victim = target,
        attacker = self:GetCaster(),
        damage = self:GetSpecialValueFor("instant_damage_magic"),
        damage_type = self:GetAbilityDamageType(),
        damage_flags = 0,
        ability = self,
    })
    ApplyDamageRDA({
        victim = target,
        attacker = self:GetCaster(),
        damage = target:GetMaxHealth() * (self:GetSpecialValueFor("instant_damage_pure_perc") / 100),
        damage_type = DAMAGE_TYPE_HP_REMOVAL,
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        ability = self,
    })
end

LinkLuaModifier("modifier_cemetery_secret_boss_spell_1", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_1", LUA_MODIFIER_MOTION_NONE )

modifier_cemetery_secret_boss_spell_1 = class({})

function modifier_cemetery_secret_boss_spell_1:IsDebuff()
    return true
end

function modifier_cemetery_secret_boss_spell_1:IsHidden()
    return false
end

function modifier_cemetery_secret_boss_spell_1:IsPurgable()
    return true
end

function modifier_cemetery_secret_boss_spell_1:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_cemetery_secret_boss_spell_1:OnCreated( kv )
    self.minus_armor_prc = self:GetParent():GetPhysicalArmorBaseValue() * (self:GetAbility():GetSpecialValueFor( "minus_armor_prc" ) / 100)
    self.minus_magic_armor_prc = self:GetAbility():GetSpecialValueFor( "minus_magic_armor_prc" )
    self.movespeed_slow = self:GetAbility():GetSpecialValueFor( "movespeed_slow" )
    self.damage_per_sec_magic = self:GetAbility():GetSpecialValueFor( "damage_per_sec_magic" )
    self.damage_per_sec_pure_perc = self:GetAbility():GetSpecialValueFor( "damage_per_sec_pure_perc" )
    self.interval = 0.2
    self.particle = ParticleManager:CreateParticle( "particles/units/heroes/hero_night_stalker/night_stalker_void_zone.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
    if not IsServer() then
        return
    end
    self:StartIntervalThink( self.interval )
end

function modifier_cemetery_secret_boss_spell_1:OnDestroy()
    ParticleManager:DestroyParticle(self.particle, false)
    ParticleManager:ReleaseParticleIndex(self.particle)
    self:GetParent():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_generic_stunned_lua", {duration = self:GetAbility():GetSpecialValueFor("stun_duration")})
end

function modifier_cemetery_secret_boss_spell_1:OnIntervalThink()
    local damage = self.damage_per_sec_magic * self.interval
    local pure_damage = target:GetMaxHealth() * (self.damage_per_sec_pure_perc / 100) * self.interval
    ApplyDamageRDA({
        victim = self:GetParent(),
        attacker = self:GetCaster(),
        damage = damage,
        damage_type = self:GetAbility():GetAbilityDamageType(),
        damage_flags = 0,
        ability = self:GetAbility()
    })
    ApplyDamageRDA({
        victim = target,
        attacker = self:GetCaster(),
        damage = pure_damage,
        damage_type = DAMAGE_TYPE_HP_REMOVAL,
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        ability = self:GetAbility(),
    })
end

function modifier_cemetery_secret_boss_spell_1:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
        MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
    }
    return funcs
end

function modifier_cemetery_secret_boss_spell_1:GetModifierPhysicalArmorBonus()
    return self.minus_armor_prc
end

function modifier_cemetery_secret_boss_spell_1:GetModifierMagicalResistanceBonus()
    return self.minus_magic_armor_prc
end

function modifier_cemetery_secret_boss_spell_1:GetModifierMoveSpeedBonus_Percentage()
	return self.movespeed_slow
end