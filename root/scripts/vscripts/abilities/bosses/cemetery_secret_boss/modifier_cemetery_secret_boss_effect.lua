modifier_cemetery_secret_boss_effect = class({})

function modifier_cemetery_secret_boss_effect:IsHidden()
    return true
end

function modifier_cemetery_secret_boss_effect:IsPurgable()
    return false
end

function modifier_cemetery_secret_boss_effect:OnCreated( kv )
    self.particle_disruption = "particles/chc/creature/venom_weapon.vpcf"
	self.particle_disruption_fx = ParticleManager:CreateParticle(self.particle_disruption, PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
end

function modifier_cemetery_secret_boss_effect:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
    }
    return funcs
end

function modifier_cemetery_secret_boss_effect:GetModifierSpellAmplify_Percentage()
    return self:GetStackCount()
end