LinkLuaModifier("modifier_cemetery_secret_boss_spell_5", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_5", LUA_MODIFIER_MOTION_NONE)

cemetery_secret_boss_spell_5 = class({})

function cemetery_secret_boss_spell_5:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_cemetery_secret_boss_spell_5", {})
end

modifier_cemetery_secret_boss_spell_5 = class({})

function modifier_cemetery_secret_boss_spell_5:IsHidden()
    return false
end

function modifier_cemetery_secret_boss_spell_5:IsPurgable()
    return false
end

function modifier_cemetery_secret_boss_spell_5:OnCreated()
	self.particle_disruption = "particles/chc/creature/venom_weapon.vpcf"
	self.particle_disruption_fx = ParticleManager:CreateParticle(self.particle_disruption, PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
	-- ParticleManager:SetParticleControl(self.particle_disruption_fx, 0, self:GetParent():GetAbsOrigin() + Vector(0,0,100))
	-- ParticleManager:SetParticleControl(self.particle_disruption_fx, 4, Vector(0,0,0))
    self:StartIntervalThink(FrameTime())
end

function modifier_cemetery_secret_boss_spell_5:OnIntervalThink()
	-- ParticleManager:SetParticleControl(self.particle_disruption_fx, 0, self:GetParent():GetAbsOrigin() + Vector(0,0,100))
end

function modifier_cemetery_secret_boss_spell_5:OnIntervalThink()
    -- self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_cemetery_secret_boss_spell_5_thinker", {duration = self:GetSpecialValueFor("radius") / self:GetSpecialValueFor("speed") })
end