LinkLuaModifier("modifier_cemetery_secret_boss_spell_4", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_4", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_cemetery_secret_boss_spell_4_thinker", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_4", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_cemetery_secret_boss_spell_4_debuff", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_4", LUA_MODIFIER_MOTION_NONE)

cemetery_secret_boss_spell_4 = class({})

function cemetery_secret_boss_spell_4:OnSpellStart()
    self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_cemetery_secret_boss_spell_4", {duration = self:GetSpecialValueFor("duration") + 0.5})
end

modifier_cemetery_secret_boss_spell_4 = class({})

function modifier_cemetery_secret_boss_spell_4:IsHidden()
    return false
end

function modifier_cemetery_secret_boss_spell_4:IsPurgable()
    return false
end
function modifier_cemetery_secret_boss_spell_4:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_cemetery_secret_boss_spell_4:OnCreated()
    self.particle_debuff_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_shadow_demon/shadow_demon_soul_catcher_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
	ParticleManager:SetParticleControl(self.particle_debuff_fx, 0, self:GetParent():GetAbsOrigin())
	self:AddParticle(self.particle_debuff_fx, false, false, -1, false, false)
    if IsServer() then
        self:OnIntervalThink()
        self:StartIntervalThink(1)
    end
end

function modifier_cemetery_secret_boss_spell_4:OnIntervalThink()
    self:GetCaster():AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_cemetery_secret_boss_spell_4_thinker", {duration = self:GetSpecialValueFor("radius") / self:GetSpecialValueFor("speed") })
end

modifier_cemetery_secret_boss_spell_4_thinker = class({})

function modifier_cemetery_secret_boss_spell_4_thinker:IsPurgable()
    return false
end

function modifier_cemetery_secret_boss_spell_4_thinker:IsHidden()
    return true
end

function modifier_cemetery_secret_boss_spell_4_thinker:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_cemetery_secret_boss_spell_4_thinker:OnCreated()
	if not self:GetAbility() then self:Destroy() return end
	
	self.fear_duration	= self:GetAbility():GetSpecialValueFor("fear_duration")
	self.radius			= self:GetAbility():GetSpecialValueFor("radius")
	self.speed			= self:GetAbility():GetSpecialValueFor("speed")
	
	if not IsServer() then return end
	
	self.bLaunched		= false
	self.feared_units	= {}
	self.fear_modifier	= nil
	
	self:StartIntervalThink(FrameTime())
end

-- Once again, wiki says nothing about a width (might be 1 for all I know, but I'll arbitrarily make it 50)
function modifier_cemetery_secret_boss_spell_4_thinker:OnIntervalThink()
	if not self.bLaunched then
		self.bLaunched = true
		
		local wave_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_terrorblade/terrorblade_scepter.vpcf", PATTACH_WORLDORIGIN, self:GetParent())
		ParticleManager:SetParticleControl(wave_particle, 0, self:GetParent():GetAbsOrigin())
		-- Yeah, this particle CP doesn't actually match the speed (vanilla uses 1400 as CP value, while the speed is 1600)
		ParticleManager:SetParticleControl(wave_particle, 1, Vector(self.speed, self.speed, self.speed))
		ParticleManager:SetParticleControl(wave_particle, 2, Vector(self.speed, self.speed, self.speed))
		ParticleManager:ReleaseParticleIndex(wave_particle)
	else
		for _, enemy in pairs(FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, math.min(self.speed * self:GetElapsedTime(), self.radius), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)) do
			if not self.feared_units[enemy:entindex()] and (enemy:GetAbsOrigin() - self:GetParent():GetAbsOrigin()):Length2D() >= math.min(self.speed * self:GetElapsedTime(), self.radius) - 50 then
				enemy:EmitSound("Hero_Terrorblade.Metamorphosis.Fear")
				
				-- Vanilla fear modifier
				self.fear_modifier = enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_cemetery_secret_boss_spell_4_debuff", {duration = self.fear_duration})
				
				if self.fear_modifier then
					-- self.fear_modifier:SetDuration(self.fear_duration * (1 - enemy:GetStatusResistance()), true)
				end
				
				self.feared_units[enemy:entindex()] = true
			end
		end
	end
end

modifier_cemetery_secret_boss_spell_4_debuff = class({})

function modifier_cemetery_secret_boss_spell_4_debuff:IsDebuff()
    return true
end

function modifier_cemetery_secret_boss_spell_4_debuff:IsPurgable()
    return true
end

function modifier_cemetery_secret_boss_spell_4_thinker:GetAttributes()
    return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_cemetery_secret_boss_spell_4_debuff:OnCreated( kv )
    if not IsServer() then
        return
    end
    ApplyDamageRDA({
        attacker = self:GetCaster(),
        victim = self:GetParent(),
        damage = self:GetParent():GetMaxHealth() * (self:GetAbility():GetSpecialValueFor("damage_healt_prc") / 100),
        damage_type = DAMAGE_TYPE_HP_REMOVAL,
        ability = self:GetAbility(),
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION
    })
    ApplyDamageRDA({
        attacker = self:GetCaster(),
        victim = self:GetParent(),
        damage = self:GetAbility():GetSpecialValueFor("damage"),
        damage_type = DAMAGE_TYPE_MAGICAL,
        ability = self:GetAbility(),
        damage_flags = 0
    })
    self:GetCaster():HealWithParams(self:GetParent():GetMaxHealth() * (self:GetAbility():GetSpecialValueFor("damage_healt_prc") / 100), self:GetAbility(), false, true, self:GetCaster(), false)
    local lifePfx = ParticleManager:CreateParticle("particles/generic_gameplay/generic_lifesteal.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster())
    ParticleManager:ReleaseParticleIndex(lifePfx)
end

function modifier_cemetery_secret_boss_spell_4_debuff:CheckState()
	local state = {
		[MODIFIER_STATE_SILENCED] = true,
	}

	return state
end