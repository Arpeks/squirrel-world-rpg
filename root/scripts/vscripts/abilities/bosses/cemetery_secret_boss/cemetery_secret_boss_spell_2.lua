LinkLuaModifier( "modifier_generic_stunned_lua", "heroes/generic/modifier_generic_stunned_lua", LUA_MODIFIER_MOTION_NONE )
cemetery_secret_boss_spell_2 = class({})

function cemetery_secret_boss_spell_2:OnSpellStart()
    local count = 0
    Timers:CreateTimer({
		endTime = .03,
		callback = function()
            count = count + 1
            self:LaunchTornado()
            if count < self:GetSpecialValueFor("tornado_count") then
                return self:GetSpecialValueFor("tornado_interval")
            end
            return nil
		end
	})
end

function cemetery_secret_boss_spell_2:LaunchTornado()
    EmitSoundOn("Hero_Oracle.PreAttack", self:GetCaster())
    local projectile_information =  
	{
		EffectName = "particles/econ/items/invoker/invoker_ti6/invoker_tornado_ti6.vpcf",
		Ability = self,
		vSpawnOrigin = self:GetCaster():GetOrigin(),
		fDistance = 2000,
		fStartRadius = 200,
		fEndRadius = 200,
		Source = self:GetCaster(),
		bHasFrontalCone = false,
		iMoveSpeed = 1000,
		bReplaceExisting = false,
		iVisionTeamNumber = self:GetCaster():GetTeam(),
		bDrawsOnMinimap = false,
		bVisibleToEnemies = true, 
		iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
		iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_NONE,
		iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		fExpireTime = GameRules:GetGameTime() + 20.0,
	}
    local enemies = FindUnitsInRadius(
        self:GetCaster():GetTeamNumber(),	-- int, your team number
        self:GetCaster():GetOrigin(),	-- point, center point
        nil,	-- handle, cacheUnit. (not known)
        self:GetCaster():GetAttackRange() + 50,	-- float, radius. or use FIND_UNITS_EVERYWHERE
        DOTA_UNIT_TARGET_TEAM_ENEMY,	-- int, team filter
        DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,	-- int, type filter
        0,	-- int, flag filter
        0,	-- int, order filter
        false	-- bool, can grow cache
    )
    local target = self:GetCaster()
    if RandomInt(1, 100) <= 70 and #enemies > 0 then
        print("RandomInt")
        target = enemies[RandomInt(1, #enemies)]
    end
    local angle = math.rad(math.random(0, 360))
    local distance = math.random() * 200
    local x = target:GetOrigin().x + distance * math.cos(angle)
    local y = target:GetOrigin().y + distance * math.sin(angle)
    local z = target:GetOrigin().z
    local point_difference_normalized = (Vector(x, y, z) - self:GetCaster():GetOrigin()):Normalized()
    projectile_information.vVelocity = point_difference_normalized * 1000
    ProjectileManager:CreateLinearProjectile(projectile_information)
end

function cemetery_secret_boss_spell_2:OnProjectileHit_ExtraData( target, location, ExtraData )
    EmitSoundOn("Hero_Invoker.Tornado.LandDamage", target)
    target:AddNewModifier(self:GetCaster(), self, "modifier_generic_stunned_lua", {duration = self:GetSpecialValueFor("stun_duration")})
    ApplyDamageRDA({
        victim = target,
        attacker = self:GetCaster(),
        damage = self:GetSpecialValueFor("hit_damage_magic"),
        damage_type = self:GetAbilityDamageType(),
        damage_flags = 0,
        ability = self
    })
    ApplyDamageRDA({
        victim = target,
        attacker = self:GetCaster(),
        damage = target:GetMaxHealth() * (self:GetSpecialValueFor("hit_damage_pure_prc") / 100),
        damage_type = DAMAGE_TYPE_HP_REMOVAL,
        damage_flags = DOTA_DAMAGE_FLAG_NO_SPELL_AMPLIFICATION,
        ability = self
    })
end