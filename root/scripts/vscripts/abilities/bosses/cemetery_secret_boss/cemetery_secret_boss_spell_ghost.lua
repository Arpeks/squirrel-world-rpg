LinkLuaModifier("modifier_cemetery_secret_boss_spell_ghost", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_ghost", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_cemetery_secret_boss_spell_ghost_stacks", "abilities/bosses/cemetery_secret_boss/cemetery_secret_boss_spell_ghost", LUA_MODIFIER_MOTION_NONE)

cemetery_secret_boss_spell_ghost = class({})

function cemetery_secret_boss_spell_ghost:GetIntrinsicModifierName()
    return "modifier_cemetery_secret_boss_spell_ghost"
end

modifier_cemetery_secret_boss_spell_ghost = class({})

function modifier_cemetery_secret_boss_spell_ghost:IsHidden()
    return true
end

function modifier_cemetery_secret_boss_spell_ghost:IsPurgable()
    return false
end

function modifier_cemetery_secret_boss_spell_ghost:DeclareFunctions()
    return { -- MODIFIER_EVENT_ON_ATTACK_LANDED 
    }
end

function modifier_cemetery_secret_boss_spell_ghost:CustomOnAttackLanded(params)
    if IsServer() then
        local parent = self:GetParent()
        local target = params.target
        local ability = self:GetAbility()

        if parent == params.attacker and ability and target:IsAlive() then
            target:AddNewModifier(parent, ability, "modifier_cemetery_secret_boss_spell_ghost_stacks", {duration = ability:GetSpecialValueFor("stack_duration")})
        end
    end
end

modifier_cemetery_secret_boss_spell_ghost_stacks = class({})

function modifier_cemetery_secret_boss_spell_ghost_stacks:IsDebuff()
    return true
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:IsPurgable()
    return true
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:OnCreated(kv)
    if IsServer() then
        self:SetStackCount(1)
    end
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:OnRefresh(kv)
    if IsServer() then
        self:IncrementStackCount()
    end
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:DeclareFunctions()
    return {
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
        MODIFIER_PROPERTY_TURN_RATE_PERCENTAGE
    }
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:GetModifierIncomingDamage_Percentage()
    return self:GetStackCount() * self:GetAbility():GetSpecialValueFor("stack_damage_increase")
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:GetModifierMoveSpeedBonus_Percentage()
    return -self:GetStackCount() * self:GetAbility():GetSpecialValueFor("stack_move_slow_perc")
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:GetModifierTurnRate_Percentage()
    return -self:GetStackCount() * self:GetAbility():GetSpecialValueFor("stack_turning_slow_perc")
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:GetEffectName()
    return "particles/generic_gameplay/generic_slowed_cold.vpcf"
end

function modifier_cemetery_secret_boss_spell_ghost_stacks:GetEffectAttachType()
    return PATTACH_ABSORIGIN_FOLLOW
end
