"DOTAAbilities"
{
"boss_9_pssive"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"                   	"abilities/bosses/line/boss_9/boss_9_pssive"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityValues"
		{
			"interval"					"10"
		}
	}
	
	
"boss_9_orbs"
	{
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"AbilityTextureName"			"crystal_maiden_crystal_nova"

		"AbilityCooldown"				"12"

		"AbilityValues"
		{
			"damage"
			{
				"value"						"10000"
			}
			"delay"
			{
				"value"							"4.0"
			}	
			"range"
			{
				"value"							"1000"
			}
			"damage_radius"
			{
				"value"					"250"
				"affected_by_aoe_increase"	"1"
			}
		}
		"OnSpellStart"
		{
			"RunScript"
			{
				"ScriptFile"		"abilities/bosses/line/boss_9/boss_9_orbs"
				"Function"			"arctic_field"
			}
			"FireSound"
			{
				"EffectName"		"lich_lich_move_12"
				"Target"			"CASTER"
			}
		}
		"Modifiers"
		{
			"modifier_dummy"
			{
				"ThinkInterval"		"0.01"
				"OnIntervalThink"	
				{
					"Damage"
					{
						"Damage"		"9999"
						"Type"			"DAMAGE_TYPE_PURE"
						"Target"		"TARGET"
					}
				}
				"States"
				{
					"MODIFIER_STATE_NO_HEALTH_BAR"			"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_UNIT_COLLISION"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NOT_ON_MINIMAP"			"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_UNSELECTABLE"			"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_COMMAND_RESTRICTED"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_ATTACK_IMMUNE"			"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}	

"boss_9_drain"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"                   	"abilities/bosses/line/boss_9/boss_9_drain"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"AbilityTextureName"			"crystal_maiden_crystal_nova"
		
		"AbilityCastRange"				"500"

		"AbilityCooldown"				"12"

		"AbilityValues"
		{
			"steal"						"1"
			"duration"						"6"
		}
	}		

"boss_9_aura"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"abilities/bosses/line/boss_9/boss_9_aura"
		"AbilityTextureName"			"elder_titan_natural_order"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_AURA"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"AbilityCastAnimation"			"ACT_INVALID"

		"AbilityCastRange"				"450"
		
		"AbilityValues"
		{
			"radius"					"450"
			"armor_reduction_pct"	    "-200"
			"magic_resistance_pct"		"-50"
		}
	}	

}	