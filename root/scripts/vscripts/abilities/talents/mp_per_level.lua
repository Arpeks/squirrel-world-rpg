LinkLuaModifier( "modifier_mp_per_level", "abilities/talents/mp_per_level.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if mp_per_level == nil then
	mp_per_level = class({})
end
function mp_per_level:GetIntrinsicModifierName()
	return "modifier_mp_per_level"
end
---------------------------------------------------------------------
--Modifiers
if modifier_mp_per_level == nil then
	modifier_mp_per_level = class({})
end
function modifier_mp_per_level:OnCreated(params)
	if IsServer() then
	end
end
function modifier_mp_per_level:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_mp_per_level:OnDestroy()
	if IsServer() then
	end
end
function modifier_mp_per_level:DeclareFunctions()
	return {
	}
end