LinkLuaModifier( "modifier_dmg_per_level", "abilities/talents/dmg_per_level.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if dmg_per_level == nil then
	dmg_per_level = class({})
end
function dmg_per_level:GetIntrinsicModifierName()
	return "modifier_dmg_per_level"
end
---------------------------------------------------------------------
--Modifiers
if modifier_dmg_per_level == nil then
	modifier_dmg_per_level = class({})
end
function modifier_dmg_per_level:OnCreated(params)
	if IsServer() then
	end
end
function modifier_dmg_per_level:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_dmg_per_level:OnDestroy()
	if IsServer() then
	end
end
function modifier_dmg_per_level:DeclareFunctions()
	return {
	}
end