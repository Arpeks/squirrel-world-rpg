modifier_don4 = class({})

function modifier_don4:IsHidden()
	return true
end

function modifier_don4:IsPurgable()
	return false
end

function modifier_don4:RemoveOnDeath()
	return false
end

function modifier_don4:OnCreated( kv )
    self.parent = self:GetParent()
	self:StartIntervalThink(1)
end

function modifier_don4:OnIntervalThink()
	if IsServer() then
		local baseValue = 0.005
		local gold = math.min(self.parent:GetGold(), 99999)
		local additionValue = GameRules:GetDOTATime(false, false) / (60 * 3) * 0.005
		
		self.parent:ModifyGoldFiltered(self.parent:GetGold() * (baseValue + additionValue), true, 0)
		-- end
	end
end