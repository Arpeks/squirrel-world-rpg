-- modifier_don13 = class({})

-- function modifier_don13:IsHidden()
-- 	return true
-- end

-- function modifier_don13:IsPurgable()
-- 	return false
-- end

-- function modifier_don13:RemoveOnDeath()
-- 	return false
-- end

-- function modifier_don13:DeclareFunctions()
-- 	return {
-- 		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
-- 		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
-- 		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
--         MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
--         MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
--         MODIFIER_PROPERTY_TOOLTIP
-- 	}
-- end

-- function modifier_don13:GetTexture()
--     return 'don13'
-- end

-- function modifier_don13:OnCreated()
--     self.bonus_stats = 300
--     -- if IsServer() then
--     --     self:StartIntervalThink(1)
--     --     self.main_attribute = self:GetCaster():GetPrimaryAttribute()
--     --     if self.main_attribute == DOTA_ATTRIBUTE_ALL then
--     --         local don6 = Talents:GetHeroTalentByName(self:GetCaster():GetPlayerOwnerID(), 'don6')
--     --         local don7 = Talents:GetHeroTalentByName(self:GetCaster():GetPlayerOwnerID(), 'don7')
--     --         local don8 = Talents:GetHeroTalentByName(self:GetCaster():GetPlayerOwnerID(), 'don8')
--     --         if don6 and don6.value > 0 then
--     --             self.main_attribute = DOTA_ATTRIBUTE_STRENGTH
--     --         elseif don7 and don7.value > 0 then
--     --             self.main_attribute = DOTA_ATTRIBUTE_AGILITY
--     --         elseif don8 and don8.value > 0 then
--     --             self.main_attribute = DOTA_ATTRIBUTE_INTELLECT
--     --         end
--     --     end
--     --     self:SetHasCustomTransmitterData( true )
--     -- end
-- end

-- function modifier_don13:OnIntervalThink()
--     self.bonus_stats = 0
--     self.bonus_spel_amp = 0
--     self.bonus_attack = 0
--     if self.main_attribute == DOTA_ATTRIBUTE_STRENGTH then
--         self.bonus_stats = math.floor(self:GetCaster():GetLevel() / 10) * 50
--     elseif self.main_attribute == DOTA_ATTRIBUTE_AGILITY then
--         self.bonus_attack = math.floor(self:GetCaster():GetLevel() / 10) * 0.2 * self:GetCaster():GetAgility()
--     elseif self.main_attribute == DOTA_ATTRIBUTE_INTELLECT then
--         self.bonus_spel_amp = math.floor(self:GetCaster():GetLevel() / 10) * 0.05 * self:GetCaster():GetIntellect(false)
--     end
--     self:SendBuffRefreshToClients()
-- end

-- function modifier_don13:OnRefresh( kv )
--     if IsServer() then
--         self:SendBuffRefreshToClients()
--     end
-- end

-- function modifier_don13:AddCustomTransmitterData()
-- 	return {
-- 		bonus_stats = self.bonus_stats,
-- 		bonus_spel_amp = self.bonus_spel_amp,
-- 		bonus_attack = self.bonus_attack,
-- 	}
-- end

-- function modifier_don13:HandleCustomTransmitterData( data )
-- 	self.bonus_stats = data.bonus_stats
-- 	self.bonus_spel_amp = data.bonus_spel_amp
-- 	self.bonus_attack = data.bonus_attack
-- end

-- function modifier_don13:GetModifierBonusStats_Strength()
--     return self.bonus_stats
-- end

-- function modifier_don13:GetModifierBonusStats_Agility()
-- 	return self.bonus_stats
-- end

-- function modifier_don13:GetModifierBonusStats_Intellect()
-- 	return self.bonus_stats
-- end

-- function modifier_don13:GetModifierSpellAmplify_Percentage( params )
-- 	return self.bonus_spel_amp
-- end

-- function modifier_don13:GetModifierPreAttack_BonusDamage()
--     return self.bonus_attack
-- end



modifier_don13 = class({})

function modifier_don13:IsHidden()
	return false
end

function modifier_don13:IsPurgable()
	return false
end

function modifier_don13:RemoveOnDeath()
	return false
end

function modifier_don13:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
        MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
        MODIFIER_PROPERTY_TOOLTIP
	}
end

function modifier_don13:GetTexture()
    return 'don13'
end

function modifier_don13:OnCreated()
    self.multiplier = 1
    if IsServer() then
        self.stats = 0
        if diff_wave.wavedef == 'Ultra' then
            self.stats = 100
        end
        if diff_wave.wavedef == 'Insane' then
            self.stats = 200
        end
        if diff_wave.wavedef == 'Impossible' then
            self.stats = 300
        end
        if diff_wave.wavedef == 'Unreal' then
            self.stats = 600
        end
    end
    self:SetStackCount(0)
end
function modifier_don13:OnRefresh()
    self:SetStackCount(0)
end

function modifier_don13:GetModifierBonusStats_Strength()
	return self.stats
end

function modifier_don13:GetModifierBonusStats_Agility()
	return self.stats
end

function modifier_don13:GetModifierBonusStats_Intellect()
	return self.stats
end

function modifier_don13:GetModifierTotalDamageOutgoing_Percentage()
	return self:GetCaster():GetLevel() * 0.6 * self.multiplier
end

function modifier_don13:GetModifierIncomingDamage_Percentage()
	return self:GetCaster():GetLevel() * -0.05 * self.multiplier
end

function modifier_don13:OnTooltip()
	return 100 + self:GetCaster():GetLevel() * 0.6 * self.multiplier
end