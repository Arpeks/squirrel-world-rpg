LinkLuaModifier( "modifier_hp_regen_level", "abilities/talents/hp_regen_level.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if hp_regen_level == nil then
	hp_regen_level = class({})
end
function hp_regen_level:GetIntrinsicModifierName()
	return "modifier_hp_regen_level"
end
---------------------------------------------------------------------
--Modifiers
if modifier_hp_regen_level == nil then
	modifier_hp_regen_level = class({})
end
function modifier_hp_regen_level:OnCreated(params)
	if IsServer() then
	end
end
function modifier_hp_regen_level:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_hp_regen_level:OnDestroy()
	if IsServer() then
	end
end
function modifier_hp_regen_level:DeclareFunctions()
	return {
	}
end