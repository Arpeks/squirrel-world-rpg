LinkLuaModifier( "modifier_status", "abilities/talents/status.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if status == nil then
	status = class({})
end
function status:GetIntrinsicModifierName()
	return "modifier_status"
end
---------------------------------------------------------------------
--Modifiers
if modifier_status == nil then
	modifier_status = class({})
end
function modifier_status:OnCreated(params)
	if IsServer() then
	end
end
function modifier_status:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_status:OnDestroy()
	if IsServer() then
	end
end
function modifier_status:DeclareFunctions()
	return {
	}
end