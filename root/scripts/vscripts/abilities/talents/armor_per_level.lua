LinkLuaModifier( "modifier_armor_per_level", "abilities/talents/armor_per_level.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if armor_per_level == nil then
	armor_per_level = class({})
end
function armor_per_level:GetIntrinsicModifierName()
	return "modifier_armor_per_level"
end
---------------------------------------------------------------------
--Modifiers
if modifier_armor_per_level == nil then
	modifier_armor_per_level = class({})
end
function modifier_armor_per_level:OnCreated(params)
	if IsServer() then
	end
end
function modifier_armor_per_level:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_armor_per_level:OnDestroy()
	if IsServer() then
	end
end
function modifier_armor_per_level:DeclareFunctions()
	return {
	}
end