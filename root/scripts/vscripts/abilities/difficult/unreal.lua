if modifier_unreal == nil then
	modifier_unreal = class({})
end

function modifier_unreal:IsHidden()
	return false
end

function modifier_unreal:IsPurgable()
	return false
end

function modifier_unreal:RemoveOnDeath()
	return false
end

function modifier_unreal:GetPriority()
	return MODIFIER_PRIORITY_SUPER_ULTRA
end

function modifier_unreal:OnCreated()	    
	self.caster = self:GetCaster()
	
    self.bonus_damage_perc_income = -97 - math.min(math.ceil((GameRules:GetGameTime() / 54)) * 0.03, 2.99)
    self.bonus_damage_perc_income_magic = -97 - math.min(math.ceil((GameRules:GetGameTime() / 54)) * 0.03, 2.99)
    self.bonus_damage_perc_outgoing = 400 + math.ceil((GameRules:GetGameTime() / 4))
    self.health = 400 + math.ceil((GameRules:GetGameTime() / 6))
	self.cd = 70
	self.armor = math.min(math.ceil((GameRules:GetGameTime() / 30)) + 5, 200)
end
--запускается только для босов в гейм моде
function modifier_unreal:OnIntervalThink()	    
	self:OnRefresh()
	self:GetParent():CalculateGenericBonuses()
end

function modifier_unreal:OnRefresh()
	self.caster = self:GetCaster()
	
    self.bonus_damage_perc_income = -97 - math.min(math.ceil((GameRules:GetGameTime() / 54)) * 0.03, 2.99)
    self.bonus_damage_perc_income_magic = -97 - math.min(math.ceil((GameRules:GetGameTime() / 54)) * 0.03, 2.99)
    self.bonus_damage_perc_outgoing = 400 + math.ceil((GameRules:GetGameTime() / 4))
    self.health = 400 + math.ceil((GameRules:GetGameTime() / 6))
	self.cd = 70
	self.armor = math.min(math.ceil((GameRules:GetGameTime() / 30)) + 5, 200)
end

function modifier_unreal:GetTexture()
    return "unreal"
end

function modifier_unreal:DeclareFunctions()
    local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		MODIFIER_PROPERTY_EXTRA_HEALTH_PERCENTAGE,
		MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
    }
    return funcs
end

function modifier_unreal:GetModifierDamageOutgoing_Percentage()	 		
	return self.bonus_damage_perc_outgoing	
end

function modifier_unreal:GetModifierIncomingDamage_Percentage(data)	 	
	if not data then
		return self.bonus_damage_perc_income
	end 	
	if data.damage_type == DAMAGE_TYPE_PURE or data.damage_type == DAMAGE_TYPE_MAGICAL then	 		
		return self.bonus_damage_perc_income_magic	
	else
		return self.bonus_damage_perc_income
	end	
end

function modifier_unreal:GetModifierExtraHealthPercentage()
	if self:GetCaster():GetMaxHealth() >= 2000000000 then return 0 end
	return self.health
end

function modifier_unreal:GetModifierPercentageCooldown()
	return self.cd	
end

function modifier_unreal:GetModifierPhysicalArmorBonus()
	return self.armor	
end