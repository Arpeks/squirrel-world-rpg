if modifier_normal == nil then
	modifier_normal = class({})
end

function modifier_normal:IsHidden()
	return false
end

function modifier_normal:IsPurgable()
	return false
end

function modifier_normal:RemoveOnDeath()
	return false
end

function modifier_normal:OnCreated()
	self.bonus_damage_perc_income = 0
    self.bonus_damage_perc_income_magic = 0       
    self.bonus_damage_perc_outgoing = 0  	    
end

function modifier_normal:OnRefresh()
	self.bonus_damage_perc_income = 0
    self.bonus_damage_perc_outgoing = 0  	    
end

function modifier_normal:GetTexture()
    return "normal"
end

function modifier_normal:DeclareFunctions()
    local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		MODIFIER_PROPERTY_EXTRA_HEALTH_PERCENTAGE,
		MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE
    }
    return funcs
end

function modifier_normal:GetModifierDamageOutgoing_Percentage()	 		
	return self.bonus_damage_perc_outgoing	
end

function modifier_normal:GetModifierIncomingDamage_Percentage(data)	 	
	if not data then
		return self.bonus_damage_perc_income
	end 	
	if data.damage_type == DAMAGE_TYPE_PURE or data.damage_type == DAMAGE_TYPE_MAGICAL then	 		
		return self.bonus_damage_perc_income_magic	
	else
		return self.bonus_damage_perc_income
	end	
end