if modifier_unreal_lane == nil then
	modifier_unreal_lane = class({})
end

function modifier_unreal_lane:IsHidden()
	return false
end

function modifier_unreal_lane:IsPurgable()
	return false
end

function modifier_unreal_lane:RemoveOnDeath()
	return false
end

function modifier_unreal_lane:GetPriority()
	return MODIFIER_PRIORITY_SUPER_ULTRA
end

function modifier_unreal_lane:OnCreated()	    
	self.caster = self:GetCaster()
	
    self.bonus_damage_perc_income = -92 - math.min(math.ceil((GameRules:GetGameTime() / 15)) * 0.05, 7.99)
    self.bonus_damage_perc_income_magic = -92 - math.min(math.ceil((GameRules:GetGameTime() / 15)) * 0.05, 7.99)       
    self.bonus_damage_perc_outgoing = 100 + math.ceil((GameRules:GetGameTime() / 4))
    -- self.health = 400 + math.ceil((GameRules:GetGameTime() / 10)) * 5
	self.cd = 70
	-- self.armor = math.min(math.ceil((GameRules:GetGameTime() / 30)) + 5, 200)
end

function modifier_unreal_lane:OnRefresh()    
    self.bonus_damage_perc_income = -92 - math.min(math.ceil((GameRules:GetGameTime() / 15)) * 0.05, 7.99)
    self.bonus_damage_perc_income_magic = -92 - math.min(math.ceil((GameRules:GetGameTime() / 15)) * 0.05, 7.99)       
    self.bonus_damage_perc_outgoing = 100 + math.ceil((GameRules:GetGameTime() / 4))
    -- self.health = 400 + math.ceil((GameRules:GetGameTime() / 10)) * 5
	self.cd = 70
	-- self.armor = math.min(math.ceil((GameRules:GetGameTime() / 30)) + 5, 200)
end

function modifier_unreal_lane:GetTexture()
    return "unreal"
end

function modifier_unreal_lane:DeclareFunctions()
    local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
		-- MODIFIER_PROPERTY_EXTRA_HEALTH_PERCENTAGE,
		MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
		-- MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
    }
    return funcs
end

function modifier_unreal_lane:GetModifierDamageOutgoing_Percentage()	 		
	return self.bonus_damage_perc_outgoing	
end

function modifier_unreal_lane:GetModifierIncomingDamage_Percentage(data)
	if not data then
		return self.bonus_damage_perc_income
	end 	 		
	if data.damage_type == DAMAGE_TYPE_PURE or data.damage_type == DAMAGE_TYPE_MAGICAL then	 		
		return self.bonus_damage_perc_income_magic	
	else
		return self.bonus_damage_perc_income
	end	
end

function modifier_unreal_lane:GetModifierExtraHealthPercentage()
	if self:GetCaster():GetMaxHealth() >= 2000000000 then return 0 end
	return self.health
end

function modifier_unreal_lane:GetModifierPercentageCooldown()
	return self.cd	
end

function modifier_unreal_lane:GetModifierPhysicalArmorBonus()
	return self.armor	
end