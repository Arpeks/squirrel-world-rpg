LinkLuaModifier( "modifier_wave_boss_enigma_hole", "abilities/wave_bosses/wave_boss_enigma/wave_boss_enigma_hole.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_enigma_hole == nil then
	wave_boss_enigma_hole = class({})
end
function wave_boss_enigma_hole:GetIntrinsicModifierName()
	return "modifier_wave_boss_enigma_hole"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_enigma_hole == nil then
	modifier_wave_boss_enigma_hole = class({})
end
function modifier_wave_boss_enigma_hole:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_hole:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_hole:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_hole:DeclareFunctions()
	return {
	}
end