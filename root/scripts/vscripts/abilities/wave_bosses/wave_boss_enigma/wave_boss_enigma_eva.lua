LinkLuaModifier( "modifier_wave_boss_enigma_eva", "abilities/wave_bosses/wave_boss_enigma/wave_boss_enigma_eva.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_enigma_eva == nil then
	wave_boss_enigma_eva = class({})
end
function wave_boss_enigma_eva:GetIntrinsicModifierName()
	return "modifier_wave_boss_enigma_eva"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_enigma_eva == nil then
	modifier_wave_boss_enigma_eva = class({})
end
function modifier_wave_boss_enigma_eva:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_eva:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_eva:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_enigma_eva:DeclareFunctions()
	return {
	}
end