LinkLuaModifier( "modifier_boss_ursa_fury_swipes_lua", "abilities/wave_bosses/boss_1/boss_ursa_fury_swipes_lua.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if boss_ursa_fury_swipes_lua == nil then
	boss_ursa_fury_swipes_lua = class({})
end
function boss_ursa_fury_swipes_lua:GetIntrinsicModifierName()
	return "modifier_boss_ursa_fury_swipes_lua"
end
---------------------------------------------------------------------
--Modifiers
if modifier_boss_ursa_fury_swipes_lua == nil then
	modifier_boss_ursa_fury_swipes_lua = class({})
end
function modifier_boss_ursa_fury_swipes_lua:OnCreated(params)
	if IsServer() then
	end
end
function modifier_boss_ursa_fury_swipes_lua:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_boss_ursa_fury_swipes_lua:OnDestroy()
	if IsServer() then
	end
end
function modifier_boss_ursa_fury_swipes_lua:DeclareFunctions()
	return {
	}
end