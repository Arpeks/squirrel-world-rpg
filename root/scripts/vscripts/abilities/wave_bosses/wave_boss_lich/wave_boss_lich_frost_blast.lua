LinkLuaModifier( "modifier_wave_boss_lich_frost_blast", "abilities/wave_bosses/wave_boss_lich/wave_boss_lich_frost_blast.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_lich_frost_blast == nil then
	wave_boss_lich_frost_blast = class({})
end
function wave_boss_lich_frost_blast:GetIntrinsicModifierName()
	return "modifier_wave_boss_lich_frost_blast"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_lich_frost_blast == nil then
	modifier_wave_boss_lich_frost_blast = class({})
end
function modifier_wave_boss_lich_frost_blast:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_frost_blast:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_frost_blast:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_lich_frost_blast:DeclareFunctions()
	return {
	}
end