LinkLuaModifier( "modifier_wave_boss_lich_slow", "abilities/wave_bosses/wave_boss_lich/wave_boss_lich_slow.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_lich_slow == nil then
	wave_boss_lich_slow = class({})
end
function wave_boss_lich_slow:GetIntrinsicModifierName()
	return "modifier_wave_boss_lich_slow"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_lich_slow == nil then
	modifier_wave_boss_lich_slow = class({})
end
function modifier_wave_boss_lich_slow:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_slow:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_slow:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_lich_slow:DeclareFunctions()
	return {
	}
end