LinkLuaModifier( "modifier_wave_boss_lich_chain_frost", "abilities/wave_bosses/wave_boss_lich/wave_boss_lich_chain_frost.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_lich_chain_frost == nil then
	wave_boss_lich_chain_frost = class({})
end
function wave_boss_lich_chain_frost:GetIntrinsicModifierName()
	return "modifier_wave_boss_lich_chain_frost"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_lich_chain_frost == nil then
	modifier_wave_boss_lich_chain_frost = class({})
end
function modifier_wave_boss_lich_chain_frost:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_chain_frost:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_chain_frost:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_lich_chain_frost:DeclareFunctions()
	return {
	}
end