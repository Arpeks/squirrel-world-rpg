LinkLuaModifier( "modifier_wave_boss_lich_shards", "abilities/wave_bosses/wave_boss_lich/wave_boss_lich_shards.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_lich_shards == nil then
	wave_boss_lich_shards = class({})
end
function wave_boss_lich_shards:GetIntrinsicModifierName()
	return "modifier_wave_boss_lich_shards"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_lich_shards == nil then
	modifier_wave_boss_lich_shards = class({})
end
function modifier_wave_boss_lich_shards:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_shards:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_lich_shards:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_lich_shards:DeclareFunctions()
	return {
	}
end