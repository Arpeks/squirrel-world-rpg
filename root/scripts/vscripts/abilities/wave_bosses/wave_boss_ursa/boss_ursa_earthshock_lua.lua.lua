LinkLuaModifier( "modifier_boss_ursa_earthshock_lua", "abilities/wave_bosses/wave_boss_ursa/boss_ursa_earthshock_lua.lua.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if boss_ursa_earthshock_lua == nil then
	boss_ursa_earthshock_lua = class({})
end
function boss_ursa_earthshock_lua:GetIntrinsicModifierName()
	return "modifier_boss_ursa_earthshock_lua"
end
---------------------------------------------------------------------
--Modifiers
if modifier_boss_ursa_earthshock_lua == nil then
	modifier_boss_ursa_earthshock_lua = class({})
end
function modifier_boss_ursa_earthshock_lua:OnCreated(params)
	if IsServer() then
	end
end
function modifier_boss_ursa_earthshock_lua:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_boss_ursa_earthshock_lua:OnDestroy()
	if IsServer() then
	end
end
function modifier_boss_ursa_earthshock_lua:DeclareFunctions()
	return {
	}
end