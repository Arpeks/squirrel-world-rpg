LinkLuaModifier( "modifier_boss_golem_split", "abilities/wave_bosses/wave_boss_granite_golem/boss_golem_split.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if boss_golem_split == nil then
	boss_golem_split = class({})
end
function boss_golem_split:GetIntrinsicModifierName()
	return "modifier_boss_golem_split"
end
---------------------------------------------------------------------
--Modifiers
if modifier_boss_golem_split == nil then
	modifier_boss_golem_split = class({})
end
function modifier_boss_golem_split:OnCreated(params)
	if IsServer() then
	end
end
function modifier_boss_golem_split:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_boss_golem_split:OnDestroy()
	if IsServer() then
	end
end
function modifier_boss_golem_split:DeclareFunctions()
	return {
	}
end