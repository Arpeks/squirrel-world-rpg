LinkLuaModifier( "modifier_wave_boss_undying_heart", "abilities/wave_bosses/wave_boss_undying/wave_boss_undying_heart.lua", LUA_MODIFIER_MOTION_NONE )
--Abilities
if wave_boss_undying_heart == nil then
	wave_boss_undying_heart = class({})
end
function wave_boss_undying_heart:GetIntrinsicModifierName()
	return "modifier_wave_boss_undying_heart"
end
---------------------------------------------------------------------
--Modifiers
if modifier_wave_boss_undying_heart == nil then
	modifier_wave_boss_undying_heart = class({})
end
function modifier_wave_boss_undying_heart:OnCreated(params)
	if IsServer() then
	end
end
function modifier_wave_boss_undying_heart:OnRefresh(params)
	if IsServer() then
	end
end
function modifier_wave_boss_undying_heart:OnDestroy()
	if IsServer() then
	end
end
function modifier_wave_boss_undying_heart:DeclareFunctions()
	return {
	}
end