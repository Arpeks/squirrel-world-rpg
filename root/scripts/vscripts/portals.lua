LinkLuaModifier("modifier_teleport_state", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_teleport_vipzone", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_teleport_vipzone_out", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_teleport_vipzone_out_hidden", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_teleport_roshan", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_teleport_cannot", "modifiers/modifier_teleport_aura", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_other2", "modifiers/modifier_other2", LUA_MODIFIER_MOTION_NONE)

if Portals == nil then
    _G.Portals = class({})
end

function Portals:Init()
    local npc_dota_teleport_donate_in = Entities:FindByName(nil, "npc_dota_teleport_donate_in")
    npc_dota_teleport_donate_in:AddNewModifier(nil, nil, "modifier_teleport_state", {})
    npc_dota_teleport_donate_in:AddNewModifier(nil, nil, "modifier_teleport_vipzone", {})
    for i = 0, 4 do
        local npc_dota_teleport_donate_out = Entities:FindByName(nil, "npc_dota_teleport_donate_out_"..i)
        npc_dota_teleport_donate_out:AddNewModifier(nil, nil, "modifier_teleport_state", {})
        npc_dota_teleport_donate_out:AddNewModifier(nil, nil, "modifier_teleport_vipzone_out", {})
    end
    local npc_dota_teleport_roshan = Entities:FindByName(nil, "npc_dota_teleport_roshan")
    npc_dota_teleport_roshan:AddNewModifier(nil, nil, "modifier_teleport_state", {})
    npc_dota_teleport_roshan:AddNewModifier(nil, nil, "modifier_teleport_roshan", {})
    for _, point_name in pairs({"npc_dota_teleport_rune_point_aghanim","npc_dota_teleport_rune_point_ice","npc_dota_teleport_rune_point_lufi","npc_dota_teleport_rune_point_ursa"}) do
        local unit = Entities:FindByName(nil, point_name)
        unit:AddNewModifier(nil, nil, "modifier_teleport_state", {})
        unit:AddNewModifier(nil, nil, "modifier_teleport_vipzone_out", {})
    end
    CustomGameEventManager:RegisterListener("tp_check_lua",function(_, keys)
        self:TeleportToRoshan(keys)
    end)
end

function Portals:TeleportToVipzone(unit)
    if unit:IsInvulnerable() then 
        return 
    end
    local pid = unit:GetPlayerID()
    local Key = unit:FindItemInInventory("item_ticket")
    local pass = false
    if Key then
        local ticket_charges = Key:GetCurrentCharges()
        if ticket_charges > 1 then
            Key:SetCurrentCharges(ticket_charges - 1)
        else
            UTIL_Remove(Key)
        end
        pass = true
    end
    if not Key and unit:IsHero() and pid then
        local shop = Shop.pShop[pid]
        for categoryKey, category in pairs(shop) do
            if type(category) == 'table' then
                for itemKey, item in ipairs(category) do
                    if type(item) == 'table' and item.itemname and item.itemname == "item_ticket" and item.now > 0 then
                        item.now = item.now - 1
                        CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( unit:GetPlayerID() ), "UpdateStore", {
                            {categoryKey = categoryKey, productKey = itemKey, itemname = item.itemname, count = item.now},
                        })
                        pass = true
                        break
                    end
                end
            end
        end
    end
    if pass then
        ProjectileManager:ProjectileDodge(unit)
        ParticleManager:CreateParticle("particles/items_fx/blink_dagger_start.vpcf", PATTACH_ABSORIGIN, unit)
        unit:EmitSound("DOTA_Item.BlinkDagger.Activate")
        local ent = Entities:FindByName(nil, 'npc_dota_teleport_donate_out_'..pid)
        ent:RemoveModifierByName("modifier_teleport_vipzone_out")
        ent:AddNewModifier(ent, nil, "modifier_teleport_vipzone_out_hidden", {})
        unit:AddNewModifier(nil, nil, "modifier_teleport_cannot", {})
        local point = ent:GetAbsOrigin()
        FindClearSpaceForUnit(unit, point, false)
        unit:Stop()
        PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), unit)
        Timers:CreateTimer(0.1, function()
            PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), nil)
            return nil
        end)
    end
end

function Portals:TeleportHome(unit)
	if unit:IsInvulnerable() then 
		return 
	end
	ProjectileManager:ProjectileDodge(unit)
	ParticleManager:CreateParticle("particles/items_fx/blink_dagger_start.vpcf", PATTACH_ABSORIGIN, unit)
	unit:EmitSound("DOTA_Item.BlinkDagger.Activate")
	local wws= "home_out"
	local ent = Entities:FindByName( nil, wws)
	local point = ent:GetAbsOrigin()
	unit:SetAbsOrigin( point )
	FindClearSpaceForUnit(unit, point, false)
	unit:Stop()
	PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), unit)
		Timers:CreateTimer(0.1, function()
		PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), nil)
		return nil
	end)
end

function Portals:ActivateTeleportToRoshan(unit)
	CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(unit:GetPlayerID()),"ivint999",{})
end

function Portals:TeleportToRoshan(t)

    local settings = {
        boss = {item = "item_raid_ticket", place = "npc_dota_teleport_rune_point_aghanim"},
        brewmaster = {item = "item_raid_ticket2", place = "npc_dota_teleport_rune_point_lufi"},
        Wyvern = {item = "item_raid_ticket3", place = "npc_dota_teleport_rune_point_ursa"},
        Ursa = {item = "item_raid_ticket4", place = "npc_dota_teleport_rune_point_ice"},
        roshan = {item = "item_kristal", place = "roshan_out"},
        traps = {item = "item_trap_ticket", place = "traps_out"},
    }
	local item_name = settings[ t[ 'type' ] ][ 'item' ] -- название предмета 
    local place = settings[ t[ 'type' ] ][ 'place' ] -- местность
    --делает тп если есть шмотка
    local activator = PlayerResource:GetSelectedHeroEntity(t.PlayerID)
    local item = nil
    for ITEM_SLOT = DOTA_ITEM_SLOT_1, DOTA_STASH_SLOT_6 do --DOTA_ITEM_NEUTRAL_SLOT
        local current_item = activator:GetItemInSlot(ITEM_SLOT)
        if current_item and current_item:GetName() == item_name then
            item = current_item
            break
        end
    end
    if item then
        if item:GetCurrentCharges() <= 1 then
            UTIL_Remove(item)
        else
            item:SetCurrentCharges(item:GetCurrentCharges() - 1)
        end
        activator:AddNewModifier( activator, nil, "modifier_other2", {} )
        local part = ParticleManager:CreateParticle("particles/econ/events/fall_major_2015/teleport_end_fallmjr_2015_lvl2.vpcf", PATTACH_ABSORIGIN, activator) 
        activator:EmitSound("Portal.Loop_Appear")
        StartAnimation(activator, {duration = 2.5, activity = ACT_DOTA_TELEPORT})
        Timers:CreateTimer({
        endTime = 2.5,
        callback = function()
            ParticleManager:DestroyParticle( part, false )
            ParticleManager:ReleaseParticleIndex( part )
            activator:StopSound("Portal.Loop_Appear")        
            activator:Stop()
            activator:RemoveModifierByName( "modifier_other2")
            local ent = Entities:FindByName( nil, place)
            if table.has_value({"npc_dota_teleport_rune_point_aghanim","npc_dota_teleport_rune_point_lufi","npc_dota_teleport_rune_point_ursa","npc_dota_teleport_rune_point_ice"}, place) and ent:HasModifier("modifier_teleport_vipzone_out") and not ent:HasModifier("modifier_teleport_vipzone_out_hidden") then
                ent:RemoveModifierByName("modifier_teleport_vipzone_out")
                ent:AddNewModifier(ent, nil, "modifier_teleport_vipzone_out_hidden", {})
                activator:AddNewModifier(nil, nil, "modifier_teleport_cannot", {})
            end
            local point = ent:GetAbsOrigin()
            FindClearSpaceForUnit(activator, point, false)
            activator:SetAbsOrigin( point ) 
            --local playerID = t.PlayerID:GetPlayerOwnerID() если не будет работать заменить на локалку    
            PlayerResource:SetCameraTarget(t.PlayerID , activator)
            Timers:CreateTimer(0.1, function()
                PlayerResource:SetCameraTarget(t.PlayerID , nil)
            end)
        end})
        CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( t.PlayerID ), "tp_check_js", { successfully = true } )
    else
        --сделай красиво ежи
        CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( t.PlayerID ), "tp_check_js", { successfully = false } )
        local message = (t[ 'type' ] == "roshan" or t[ 'type' ] == "traps") and "#dota_hud_error_teleport_roshan" or "#dota_hud_error_teleport_raid_boss"
        CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer(t.PlayerID), "mountain_dota_hud_show_hud_error", { 
            ["message"] = message, 
        })
    end
end
