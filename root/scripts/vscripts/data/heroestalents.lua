function GetHeroTalentsData(heroname)
    local data = {}
    for key, value in pairs(basicTalentsData) do
        data[key] = value
    end
    for key, value in pairs(heroTalentsData[heroname]) do
        data[key] = value
    end
    if heroname == "npc_dota_hero_spectre" then
        data = table.remove_key(data,"modifier_talent_sheeld")
    end
    return data
end

basicTalentsData = {
    -------------------- STR -----------------------------------------------------------	
    modifier_talent_hp_per_level = {
        place = {"str 1"}, url = "/basic/str/str-talant-1.png", name = "modifier_talent_hp_per_level",
    },
    modifier_talent_hp_regen_level = {
        place = {"str 2"}, url = "/basic/str/str-talant-2.png", name = "modifier_talent_hp_regen_level",
    },
    modifier_talent_sheeld = {
        place = {"str 3"}, url = "/basic/str/str-talant-3.png", name = "modifier_talent_sheeld",
    },
    modifier_talent_armor_per_level = {
        place = {"str 4"}, url = "/basic/str/str-talant-4.png", name = "modifier_talent_armor_per_level",
    },
    modifier_talent_increase_str = {
        place = {"str 5"}, url = "/basic/str/str-talant-5.png", name = "modifier_talent_increase_str",
    },
    -------------------- AGI -----------------------------------------------------------	
    modifier_talent_armor_curruption = {
        place = {"agi 1"}, url = "/basic/agi/agi-talant-1.png", name = "modifier_talent_armor_curruption",
    },
    modifier_talent_dmg_per_level = {
        place = {"agi 2"}, url = "/basic/agi/agi-talant-2.png", name = "modifier_talent_dmg_per_level",
    },
    modifier_talent_all_evasion = {
        place = {"agi 3"}, url = "/basic/agi/agi-talant-3.png", name = "modifier_talent_all_evasion",
    },
    modifier_talent_base_attack_time = {
        place = {"agi 4"}, url = "/basic/agi/agi-talant-4.png", name = "modifier_talent_base_attack_time",
    },
    modifier_talent_increase_agi = {
        place = {"agi 5"}, url = "/basic/agi/agi-talant-5.png", name = "modifier_talent_increase_agi",
    },
    -------------------- INT -----------------------------------------------------------			
    modifier_talent_magic_damage = {
        place = {"int 1"}, url = "/basic/int/int-talant-1.png", name = "modifier_talent_magic_damage",
    },
    modifier_talent_mp_regen_level = {
        place = {"int 2"}, url = "/basic/int/int-talant-2.png", name = "modifier_talent_mp_regen_level",
    },
    modifier_talent_m_resist = {
        place = {"int 3"}, url = "/basic/int/int-talant-3.png", name = "modifier_talent_m_resist",
    },
    modifier_talent_manacost = {
        place = {"int 4"}, url = "/basic/int/int-talant-4.png", name = "modifier_talent_manacost",
    },
    modifier_talent_increase_int = {
        place = {"int 5"}, url = "/basic/int/int-talant-5.png", name = "modifier_talent_increase_int",
    },
    -------------------- DON -----------------------------------------------------------	
    modifier_don1 = {
        place = {"don 1"}, url = "/basic/don/don-talant-1.png", name = "modifier_don1",
    },
    modifier_don2 = {
        place = {"don 2"}, url = "/basic/don/don-talant-2.png", name = "modifier_don2",
    },
    modifier_don3 = {
        place = {"don 3"}, url = "/basic/don/don-talant-3.png", name = "modifier_don3",
    },
    modifier_don4 = {
        place = {"don 4"}, url = "/basic/don/don-talant-4.png", name = "modifier_don4",
    },
    modifier_don5 = {
        place = {"don 5"}, url = "/basic/don/don-talant-5.png", name = "modifier_don5",
    },
    modifier_don6 = {
        place = {"don 6"}, url = "/basic/don/don-talant-6.png", name = "modifier_don6",
    },
    modifier_don7 = {
        place = {"don 7"}, url = "/basic/don/don-talant-7.png", name = "modifier_don7",
    },
    modifier_don8 = {
        place = {"don 8"}, url = "/basic/don/don-talant-8.png", name = "modifier_don8",
    },
    modifier_don9 = {
        place = {"don 9"}, url = "/basic/don/don-talant-9.png", name = "modifier_don9",
    },
    modifier_don10 = {
        place = {"don 10"}, url = "/basic/don/don-talant-10.png", name = "modifier_don10",
    },
    modifier_don11 = {
        place = {"don 11"}, url = "/basic/don/don-talant-11.png", name = "modifier_don11",
    }, 
    modifier_don_last = {
        place = {"don 12"}, url = "/basic/don/don-talant-12.png", name = "modifier_don12",
    },
    modifier_don13 = {
        place = {"don 13"}, url = "/basic/don/don13.png", name = "modifier_don13",
    },
}

heroTalentsData = {
    npc_dota_hero_axe = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_axe_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_axe/str/str-talant-6.png", name = "npc_dota_hero_axe_str6",
        },
        npc_dota_hero_axe_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_axe/str/str-talant-7.png", name = "npc_dota_hero_axe_str7",
        },
        npc_dota_hero_axe_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_axe/str/str-talant-13.png", name = "npc_dota_hero_axe_str8",
        },
        npc_dota_hero_axe_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_axe/str/str-talant-13.png", name = "npc_dota_hero_axe_str9",
        },
        npc_dota_hero_axe_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_axe/str/str-talant-10.png", name = "npc_dota_hero_axe_str10",
        },
        npc_dota_hero_axe_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_axe/str/str-talant-11.png", name = "npc_dota_hero_axe_str11",
        },
        npc_dota_hero_axe_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_axe/str/str-talant-13.png", name = "npc_dota_hero_axe_str12",
        },       
        special_bonus_unique_npc_dota_hero_axe_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_axe/str/str-talant-13.png", name = "npc_dota_hero_axe_str13",
        },       
        -------------------- AGI -----------------------------------------------------------	
        npc_dota_hero_axe_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_axe/agi/agi-talant-9.png", name = "npc_dota_hero_axe_agi6", 
        },
        npc_dota_hero_axe_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_axe/agi/agi-talant-7.png", name = "npc_dota_hero_axe_agi7", 
        },
        npc_dota_hero_axe_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_axe/agi/agi-talant-12.png", name = "npc_dota_hero_axe_agi8", 
        }, 
        npc_dota_hero_axe_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_axe/agi/agi-talant-9.png", name = "npc_dota_hero_axe_agi9", 
        },
        npc_dota_hero_axe_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_axe/agi/agi-talant-12.png", name = "npc_dota_hero_axe_agi10",
        },
        npc_dota_hero_axe_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_axe/agi/agi-talant-11.png", name = "npc_dota_hero_axe_agi11", 
        },
        npc_dota_hero_axe_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_axe/agi/agi-talant-12.png", name = "npc_dota_hero_axe_agi12",
        },
        special_bonus_unique_npc_dota_hero_axe_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_axe/agi/agi-talant-12.png", name = "npc_dota_hero_axe_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_axe_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_axe/int/int-talant-6.png", name = "npc_dota_hero_axe_int6",
        },
        npc_dota_hero_axe_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_axe/int/int-talant-7.png", name = "npc_dota_hero_axe_int7",
        },
        npc_dota_hero_axe_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_axe/int/int-talant-6.png", name = "npc_dota_hero_axe_int8",
        }, 
        npc_dota_hero_axe_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_axe/int/int-talant-9.png", name = "npc_dota_hero_axe_int9",
        },
        npc_dota_hero_axe_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_axe/int/int-talant-10.png", name = "npc_dota_hero_axe_int10",
        },
        npc_dota_hero_axe_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_axe/int/int-talant-6.png", name = "npc_dota_hero_axe_int11",
        },
        npc_dota_hero_axe_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_axe/int/int-talant-12.png", name = "npc_dota_hero_axe_int12",
        },
        special_bonus_unique_npc_dota_hero_axe_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_axe/int/int-talant-6.png", name = "npc_dota_hero_axe_int13",
        },
    },
    npc_dota_hero_arc_warden = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_arc_warden_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_arc_warden/str/str-talant-6.png", name = "npc_dota_hero_arc_warden_str6",
        },
        npc_dota_hero_arc_warden_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_arc_warden/str/str-talant-7.png", name = "npc_dota_hero_arc_warden_str7",
        },
        npc_dota_hero_arc_warden_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_arc_warden/str/str-talant-8.png", name = "npc_dota_hero_arc_warden_str8",
        },
        npc_dota_hero_arc_warden_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_arc_warden/str/str-talant-9.png", name = "npc_dota_hero_arc_warden_str9",
        },
        npc_dota_hero_arc_warden_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_arc_warden/str/str-talant-10.png", name = "npc_dota_hero_arc_warden_str10",
        },
        npc_dota_hero_arc_warden_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_arc_warden/str/str-talant-11.png", name = "npc_dota_hero_arc_warden_str11",
        },
        npc_dota_hero_arc_warden_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_arc_warden/str/str-talant-12.png", name = "npc_dota_hero_arc_warden_str12",
        },
        special_bonus_unique_npc_dota_hero_arc_warden_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_arc_warden/str/str-talant-8.png", name = "npc_dota_hero_arc_warden_str13",
        },

        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_arc_warden_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-6.png", name = "npc_dota_hero_arc_warden_agi6",
        },
        npc_dota_hero_arc_warden_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-7.png", name = "npc_dota_hero_arc_warden_agi7",
        },
        npc_dota_hero_arc_warden_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-8.png", name = "npc_dota_hero_arc_warden_agi8",
        },
        npc_dota_hero_arc_warden_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-9.png", name = "npc_dota_hero_arc_warden_agi9",
        },
        npc_dota_hero_arc_warden_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-10.png", name = "npc_dota_hero_arc_warden_agi10",
        },
        npc_dota_hero_arc_warden_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-11.png", name = "npc_dota_hero_arc_warden_agi11",
        },
        npc_dota_hero_arc_warden_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-12.png", name = "npc_dota_hero_arc_warden_agi12",
        },
        special_bonus_unique_npc_dota_hero_arc_warden_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_arc_warden/agi/agi-talant-12.png", name = "npc_dota_hero_arc_warden_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_arc_warden_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_arc_warden/int/int-talant-6.png", name = "npc_dota_hero_arc_warden_int6",
        },
        npc_dota_hero_arc_warden_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_arc_warden/int/int-talant-7.png", name = "npc_dota_hero_arc_warden_int7",
        },
        npc_dota_hero_arc_warden_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_arc_warden/int/int-talant-8.png", name = "npc_dota_hero_arc_warden_int8",
        },
        npc_dota_hero_arc_warden_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_arc_warden/int/int-talant-9.png", name = "npc_dota_hero_arc_warden_int9",
        },
        npc_dota_hero_arc_warden_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_arc_warden/int/int-talant-10.png", name = "npc_dota_hero_arc_warden_int10",
        },
        npc_dota_hero_arc_warden_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_arc_warden/int/int-talant-11.png", name = "npc_dota_hero_arc_warden_int11",
        },
        npc_dota_hero_arc_warden_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_arc_warden/int/int-talant-12.png", name = "npc_dota_hero_arc_warden_int12",
        },
        special_bonus_unique_npc_dota_hero_arc_warden_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_arc_warden/int/int-talant-12.png", name = "npc_dota_hero_arc_warden_int13",
        },
    },
    npc_dota_hero_bristleback = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_bristleback_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_bristleback/str/str-talant-6.png", name = "npc_dota_hero_bristleback_str6",
        },
        npc_dota_hero_bristleback_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_bristleback/str/str-talant-7.png", name = "npc_dota_hero_bristleback_str7",
        },
        npc_dota_hero_bristleback_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_bristleback/str/str-talant-8.png", name = "npc_dota_hero_bristleback_str8",
        },
        npc_dota_hero_bristleback_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_bristleback/str/str-talant-9.png", name = "npc_dota_hero_bristleback_str9",
        },
        npc_dota_hero_bristleback_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_bristleback/str/str-talant-10.png", name = "npc_dota_hero_bristleback_str10",
        },
        npc_dota_hero_bristleback_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_bristleback/str/str-talant-11.png", name = "npc_dota_hero_bristleback_str11",
        },
        npc_dota_hero_bristleback_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_bristleback/str/str-talant-12.png", name = "npc_dota_hero_bristleback_str12",
        },
        special_bonus_unique_npc_dota_hero_bristleback_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_bristleback/str/str-talant-10.png", name = "npc_dota_hero_bristleback_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_bristleback_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-6.png", name = "npc_dota_hero_bristleback_agi6",
        },
        npc_dota_hero_bristleback_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-7.png", name = "npc_dota_hero_bristleback_agi7",
        },
        npc_dota_hero_bristleback_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-8.png", name = "npc_dota_hero_bristleback_agi8",
        },
        npc_dota_hero_bristleback_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-9.png", name = "npc_dota_hero_bristleback_agi9",
        },
        npc_dota_hero_bristleback_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-10.png", name = "npc_dota_hero_bristleback_agi10",
        },
        npc_dota_hero_bristleback_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-11.png", name = "npc_dota_hero_bristleback_agi11",
        },
        npc_dota_hero_bristleback_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-12.png", name = "npc_dota_hero_bristleback_agi12",
        },
        special_bonus_unique_npc_dota_hero_bristleback_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_bristleback/agi/agi-talant-12.png", name = "npc_dota_hero_bristleback_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_bristleback_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_bristleback/int/int-talant-6.png", name = "npc_dota_hero_bristleback_int6",
        },
        npc_dota_hero_bristleback_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_bristleback/int/int-talant-7.png", name = "npc_dota_hero_bristleback_int7",
        },
        npc_dota_hero_bristleback_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_bristleback/int/int-talant-8.png", name = "npc_dota_hero_bristleback_int8",
        },
        npc_dota_hero_bristleback_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_bristleback/int/int-talant-9.png", name = "npc_dota_hero_bristleback_int9",
        },
        npc_dota_hero_bristleback_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_bristleback/int/int-talant-10.png", name = "npc_dota_hero_bristleback_int10",
        },
        npc_dota_hero_bristleback_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_bristleback/int/int-talant-11.png", name = "npc_dota_hero_bristleback_int11",
        },
        npc_dota_hero_bristleback_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_bristleback/int/int-talant-12.png", name = "npc_dota_hero_bristleback_int12",
        },
        special_bonus_unique_npc_dota_hero_bristleback_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_bristleback/int/int-talant-12.png", name = "npc_dota_hero_bristleback_int13",
        },
    },
    npc_dota_hero_centaur = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_centaur_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_centaur/str/str-talant-6.png", name = "npc_dota_hero_centaur_str6",
        },
        npc_dota_hero_centaur_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_centaur/str/str-talant-7.png", name = "npc_dota_hero_centaur_str7",
        },
        npc_dota_hero_centaur_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_centaur/str/str-talant-8.png", name = "npc_dota_hero_centaur_str8",
        },
        npc_dota_hero_centaur_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_centaur/str/str-talant-9.png", name = "npc_dota_hero_centaur_str9",
        },
        npc_dota_hero_centaur_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_centaur/str/str-talant-10.png", name = "npc_dota_hero_centaur_str10",
        },
        npc_dota_hero_centaur_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_centaur/str/str-talant-11.png", name = "npc_dota_hero_centaur_str11",
        },
        npc_dota_hero_centaur_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_centaur/str/str-talant-12.png", name = "npc_dota_hero_centaur_str12",
        },
        special_bonus_unique_npc_dota_hero_centaur_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_centaur/str/str-talant-8.png", name = "npc_dota_hero_centaur_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_centaur_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_centaur/agi/agi-talant-6.png", name = "npc_dota_hero_centaur_agi6",
        },
        npc_dota_hero_centaur_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_centaur/agi/agi-talant-7.png", name = "npc_dota_hero_centaur_agi7",
        },
        npc_dota_hero_centaur_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_centaur/agi/agi-talant-8.png", name = "npc_dota_hero_centaur_agi8",
        },
        npc_dota_hero_centaur_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_centaur/agi/agi-talant-9.png", name = "npc_dota_hero_centaur_agi9",
        },
        npc_dota_hero_centaur_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_centaur/agi/agi-talant-10.png", name = "npc_dota_hero_centaur_agi10",
        },
        npc_dota_hero_centaur_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_centaur/agi/agi-talant-11.png", name = "npc_dota_hero_centaur_agi11",
        },
        npc_dota_hero_centaur_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_centaur/agi/agi-talant-12.png", name = "npc_dota_hero_centaur_agi12",
        },
        special_bonus_unique_npc_dota_hero_centaur_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_centaur/agi/agi-talant-12.png", name = "npc_dota_hero_centaur_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_centaur_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_centaur/int/int-talant-6.png", name = "npc_dota_hero_centaur_int6",
        },
        npc_dota_hero_centaur_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_centaur/int/int-talant-7.png", name = "npc_dota_hero_centaur_int7",
        },
        npc_dota_hero_centaur_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_centaur/int/int-talant-8.png", name = "npc_dota_hero_centaur_int8",
        },
        npc_dota_hero_centaur_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_centaur/int/int-talant-9.png", name = "npc_dota_hero_centaur_int9",
        },
        npc_dota_hero_centaur_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_centaur/int/int-talant-10.png", name = "npc_dota_hero_centaur_int10",
        },
        npc_dota_hero_centaur_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_centaur/int/int-talant-11.png", name = "npc_dota_hero_centaur_int11",
        },
        npc_dota_hero_centaur_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_centaur/int/int-talant-12.png", name = "npc_dota_hero_centaur_int12",
        },
        special_bonus_unique_npc_dota_hero_centaur_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_centaur/int/int-talant-12.png", name = "npc_dota_hero_centaur_int13",
        },
    },
    npc_dota_hero_dazzle = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_dazzle_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_dazzle/str/str-talant-6.png", name = "npc_dota_hero_dazzle_str6",
        },
        npc_dota_hero_dazzle_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_dazzle/str/str-talant-7.png", name = "npc_dota_hero_dazzle_str7",
        },
        npc_dota_hero_dazzle_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_dazzle/str/str-talant-8.png", name = "npc_dota_hero_dazzle_str8",
        },
        npc_dota_hero_dazzle_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_dazzle/str/str-talant-9.png", name = "npc_dota_hero_dazzle_str9",
        },
        npc_dota_hero_dazzle_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_dazzle/str/str-talant-10.png", name = "npc_dota_hero_dazzle_str10",
        },
        npc_dota_hero_dazzle_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_dazzle/str/str-talant-11.png", name = "npc_dota_hero_dazzle_str11",
        },
        npc_dota_hero_dazzle_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_dazzle/str/str-talant-12.png", name = "npc_dota_hero_dazzle_str12",
        },
        special_bonus_unique_npc_dota_hero_dazzle_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_dazzle/str/str-talant-12.png", name = "npc_dota_hero_dazzle_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_dazzle_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-6.png", name = "npc_dota_hero_dazzle_agi6",
        },
        npc_dota_hero_dazzle_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-7.png", name = "npc_dota_hero_dazzle_agi7",
        },
        npc_dota_hero_dazzle_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-8.png", name = "npc_dota_hero_dazzle_agi8",
        },
        npc_dota_hero_dazzle_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-9.png", name = "npc_dota_hero_dazzle_agi9",
        },
        npc_dota_hero_dazzle_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-10.png", name = "npc_dota_hero_dazzle_agi10",
        },
        npc_dota_hero_dazzle_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-11.png", name = "npc_dota_hero_dazzle_agi11",
        },
        npc_dota_hero_dazzle_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-12.png", name = "npc_dota_hero_dazzle_agi12",
        },
        special_bonus_unique_npc_dota_hero_dazzle_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_dazzle/agi/agi-talant-6.png", name = "npc_dota_hero_dazzle_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_dazzle_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_dazzle/int/int-talant-6.png", name = "npc_dota_hero_dazzle_int6",
        },
        npc_dota_hero_dazzle_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_dazzle/int/int-talant-7.png", name = "npc_dota_hero_dazzle_int7",
        },
        npc_dota_hero_dazzle_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_dazzle/int/int-talant-8.png", name = "npc_dota_hero_dazzle_int8",
        },
        npc_dota_hero_dazzle_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_dazzle/int/int-talant-9.png", name = "npc_dota_hero_dazzle_int9",
        },
        npc_dota_hero_dazzle_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_dazzle/int/int-talant-10.png", name = "npc_dota_hero_dazzle_int10",
        },
        npc_dota_hero_dazzle_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_dazzle/int/int-talant-11.png", name = "npc_dota_hero_dazzle_int11",
        },
        npc_dota_hero_dazzle_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_dazzle/int/int-talant-12.png", name = "npc_dota_hero_dazzle_int12",
        },
        special_bonus_unique_npc_dota_hero_dazzle_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_dazzle/int/int-talant-6.png", name = "npc_dota_hero_dazzle_int13",
        },
    },
    npc_dota_hero_dragon_knight = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_dragon_knight_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-6.png", name = "npc_dota_hero_dragon_knight_str6",
        },
        npc_dota_hero_dragon_knight_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-7.png", name = "npc_dota_hero_dragon_knight_str7",
        },
        npc_dota_hero_dragon_knight_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-8.png", name = "npc_dota_hero_dragon_knight_str8",
        },
        npc_dota_hero_dragon_knight_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-9.png", name = "npc_dota_hero_dragon_knight_str9",
        },
        npc_dota_hero_dragon_knight_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-10.png", name = "npc_dota_hero_dragon_knight_str10",
        },
        npc_dota_hero_dragon_knight_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-11.png", name = "npc_dota_hero_dragon_knight_str11",
        },
        npc_dota_hero_dragon_knight_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-12.png", name = "npc_dota_hero_dragon_knight_str12",
        },
        special_bonus_unique_npc_dota_hero_dragon_knight_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_dragon_knight/str/str-talant-12.png", name = "npc_dota_hero_dragon_knight_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_dragon_knight_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-6.png", name = "npc_dota_hero_dragon_knight_agi6",
        },
        npc_dota_hero_dragon_knight_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-7.png", name = "npc_dota_hero_dragon_knight_agi7",
        },
        npc_dota_hero_dragon_knight_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-8.png", name = "npc_dota_hero_dragon_knight_agi8",
        },
        npc_dota_hero_dragon_knight_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-9.png", name = "npc_dota_hero_dragon_knight_agi9",
        },
        npc_dota_hero_dragon_knight_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-10.png", name = "npc_dota_hero_dragon_knight_agi10",
        },
        npc_dota_hero_dragon_knight_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-12.png", name = "npc_dota_hero_dragon_knight_agi11",
        },
        npc_dota_hero_dragon_knight_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-11.png", name = "npc_dota_hero_dragon_knight_agi12",
        },
        special_bonus_unique_npc_dota_hero_dragon_knight_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_dragon_knight/agi/agi-talant-11.png", name = "npc_dota_hero_dragon_knight_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_dragon_knight_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-6.png", name = "npc_dota_hero_dragon_knight_int6",
        },
        npc_dota_hero_dragon_knight_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-7.png", name = "npc_dota_hero_dragon_knight_int7",
        },
        npc_dota_hero_dragon_knight_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-8.png", name = "npc_dota_hero_dragon_knight_int8",
        },
        npc_dota_hero_dragon_knight_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-9.png", name = "npc_dota_hero_dragon_knight_int9",
        },
        npc_dota_hero_dragon_knight_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-10.png", name = "npc_dota_hero_dragon_knight_int10",
        },
        npc_dota_hero_dragon_knight_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-11.png", name = "npc_dota_hero_dragon_knight_int11",
        },
        npc_dota_hero_dragon_knight_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-12.png", name = "npc_dota_hero_dragon_knight_int12",
        },
        special_bonus_unique_npc_dota_hero_dragon_knight_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_dragon_knight/int/int-talant-12.png", name = "npc_dota_hero_dragon_knight_int13",
        },
    },
    npc_dota_hero_drow_ranger = {      
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_drow_ranger_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-6.png", name = "npc_dota_hero_drow_ranger_str6",
        },
        npc_dota_hero_drow_ranger_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-7.png", name = "npc_dota_hero_drow_ranger_str7",
        },
        npc_dota_hero_drow_ranger_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-8.png", name = "npc_dota_hero_drow_ranger_str8",
        },
        npc_dota_hero_drow_ranger_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-9.png", name = "npc_dota_hero_drow_ranger_str9",
        },
        npc_dota_hero_drow_ranger_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-10.png", name = "npc_dota_hero_drow_ranger_str10",
        },
        npc_dota_hero_drow_ranger_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-11.png", name = "npc_dota_hero_drow_ranger_str11",
        },
        npc_dota_hero_drow_ranger_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-12.png", name = "npc_dota_hero_drow_ranger_str12",
        },
        special_bonus_unique_npc_dota_hero_drow_ranger_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_drow_ranger/str/str-talant-12.png", name = "npc_dota_hero_drow_ranger_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_drow_ranger_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-6.png", name = "npc_dota_hero_drow_ranger_agi6",
        },
        npc_dota_hero_drow_ranger_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-7.png", name = "npc_dota_hero_drow_ranger_agi7",
        },
        npc_dota_hero_drow_ranger_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-8.png", name = "npc_dota_hero_drow_ranger_agi8",
        },
        npc_dota_hero_drow_ranger_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-9.png", name = "npc_dota_hero_drow_ranger_agi9",
        },
        npc_dota_hero_drow_ranger_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-10.png", name = "npc_dota_hero_drow_ranger_agi10",
        },
        npc_dota_hero_drow_ranger_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-11.png", name = "npc_dota_hero_drow_ranger_agi11",
        },
        npc_dota_hero_drow_ranger_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-12.png", name = "npc_dota_hero_drow_ranger_agi12",
        },
        special_bonus_unique_npc_dota_hero_drow_ranger_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_drow_ranger/agi/agi-talant-12.png", name = "npc_dota_hero_drow_ranger_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_drow_ranger_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-6.png", name = "npc_dota_hero_drow_ranger_int6", 
        },
        npc_dota_hero_drow_ranger_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-7.png", name = "npc_dota_hero_drow_ranger_int7", 
        },
        npc_dota_hero_drow_ranger_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-8.png", name = "npc_dota_hero_drow_ranger_int8", 
        },
        npc_dota_hero_drow_ranger_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-9.png", name = "npc_dota_hero_drow_ranger_int9", 
        },
        npc_dota_hero_drow_ranger_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-10.png", name = "npc_dota_hero_drow_ranger_int10", 
        }, 
        npc_dota_hero_drow_ranger_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-11.png", name = "npc_dota_hero_drow_ranger_int11", 
        },  
        npc_dota_hero_drow_ranger_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-12.png", name = "npc_dota_hero_drow_ranger_int12",
        }, 
        special_bonus_unique_npc_dota_hero_drow_ranger_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_drow_ranger/int/int-talant-12.png", name = "npc_dota_hero_drow_ranger_int13",
        }, 
    },
    npc_dota_hero_enchantress = {      
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_enchantress_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_enchantress/str/str-talant-6.png", name = "npc_dota_hero_enchantress_str6",
        },
        npc_dota_hero_enchantress_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_enchantress/str/str-talant-7.png", name = "npc_dota_hero_enchantress_str7",
        },
        npc_dota_hero_enchantress_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_enchantress/str/str-talant-8.png", name = "npc_dota_hero_enchantress_str8",
        },
        npc_dota_hero_enchantress_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_enchantress/str/str-talant-9.png", name = "npc_dota_hero_enchantress_str9",
        },
        npc_dota_hero_enchantress_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_enchantress/str/str-talant-10.png", name = "npc_dota_hero_enchantress_str10",
        },
        npc_dota_hero_enchantress_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_enchantress/str/str-talant-11.png", name = "npc_dota_hero_enchantress_str11",
        },
        npc_dota_hero_enchantress_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_enchantress/str/str-talant-12.png", name = "npc_dota_hero_enchantress_str12",
        },
        special_bonus_unique_npc_dota_hero_enchantress_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_enchantress/str/str-talant-12.png", name = "npc_dota_hero_enchantress_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_enchantress_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-6.png", name = "npc_dota_hero_enchantress_agi6",
        },
        npc_dota_hero_enchantress_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-7.png", name = "npc_dota_hero_enchantress_agi7",
        },
        npc_dota_hero_enchantress_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-8.png", name = "npc_dota_hero_enchantress_agi8",
        },
        npc_dota_hero_enchantress_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-9.png", name = "npc_dota_hero_enchantress_agi9",
        },
        npc_dota_hero_enchantress_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-10.png", name = "npc_dota_hero_enchantress_agi10",
        },
        npc_dota_hero_enchantress_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-11.png", name = "npc_dota_hero_enchantress_agi11",
        },
        npc_dota_hero_enchantress_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-12.png", name = "npc_dota_hero_enchantress_agi12",
        },
        special_bonus_unique_npc_dota_hero_enchantress_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_enchantress/agi/agi-talant-12.png", name = "npc_dota_hero_enchantress_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_enchantress_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_enchantress/int/int-talant-6.png", name = "npc_dota_hero_enchantress_int6", 
        },
        npc_dota_hero_enchantress_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_enchantress/int/int-talant-7.png", name = "npc_dota_hero_enchantress_int7", 
        },
        npc_dota_hero_enchantress_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_enchantress/int/int-talant-8.png", name = "npc_dota_hero_enchantress_int8", 
        },
        npc_dota_hero_enchantress_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_enchantress/int/int-talant-9.png", name = "npc_dota_hero_enchantress_int9", 
        },
        npc_dota_hero_enchantress_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_enchantress/int/int-talant-10.png", name = "npc_dota_hero_enchantress_int10", 
        }, 
        npc_dota_hero_enchantress_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_enchantress/int/int-talant-11.png", name = "npc_dota_hero_enchantress_int11", 
        },  
        npc_dota_hero_enchantress_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_enchantress/int/int-talant-12.png", name = "npc_dota_hero_enchantress_int12",
        },  
        special_bonus_unique_npc_dota_hero_enchantress_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_enchantress/int/int-talant-12.png", name = "npc_dota_hero_enchantress_int13",
        },  
    },
    npc_dota_hero_juggernaut = {      
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_juggernaut_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_juggernaut/str/str-talant-6.png", name = "npc_dota_hero_juggernaut_str6",
        },
        npc_dota_hero_juggernaut_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_juggernaut/str/str-talant-7.png", name = "npc_dota_hero_juggernaut_str7",
        },
        npc_dota_hero_juggernaut_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_juggernaut/str/str-talant-8.png", name = "npc_dota_hero_juggernaut_str8",
        },
        npc_dota_hero_juggernaut_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_juggernaut/str/str-talant-9.png", name = "npc_dota_hero_juggernaut_str9",
        },
        npc_dota_hero_juggernaut_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_juggernaut/str/str-talant-10.png", name = "npc_dota_hero_juggernaut_str10",
        },
        npc_dota_hero_juggernaut_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_juggernaut/str/str-talant-11.png", name = "npc_dota_hero_juggernaut_str11",
        },
        npc_dota_hero_juggernaut_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_juggernaut/str/str-talant-12.png", name = "npc_dota_hero_juggernaut_str12",
        },
        special_bonus_unique_npc_dota_hero_juggernaut_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_juggernaut/str/str-talant-12.png", name = "npc_dota_hero_juggernaut_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_juggernaut_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-6.png", name = "npc_dota_hero_juggernaut_agi6",
        },
        npc_dota_hero_juggernaut_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-7.png", name = "npc_dota_hero_juggernaut_agi7",
        },
        npc_dota_hero_juggernaut_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-8.png", name = "npc_dota_hero_juggernaut_agi8",
        },
        npc_dota_hero_juggernaut_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-9.png", name = "npc_dota_hero_juggernaut_agi9",
        },
        npc_dota_hero_juggernaut_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-10.png", name = "npc_dota_hero_juggernaut_agi10",
        },
        npc_dota_hero_juggernaut_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-11.png", name = "npc_dota_hero_juggernaut_agi11",
        },
        npc_dota_hero_juggernaut_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-12.png", name = "npc_dota_hero_juggernaut_agi12",
        },
        special_bonus_unique_npc_dota_hero_juggernaut_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_juggernaut/agi/agi-talant-12.png", name = "npc_dota_hero_juggernaut_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_juggernaut_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_juggernaut/int/int-talant-6.png", name = "npc_dota_hero_juggernaut_int6", 
        },
        npc_dota_hero_juggernaut_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_juggernaut/int/int-talant-7.png", name = "npc_dota_hero_juggernaut_int7", 
        },
        npc_dota_hero_juggernaut_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_juggernaut/int/int-talant-8.png", name = "npc_dota_hero_juggernaut_int8", 
        },
        npc_dota_hero_juggernaut_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_juggernaut/int/int-talant-9.png", name = "npc_dota_hero_juggernaut_int9", 
        },
        npc_dota_hero_juggernaut_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_juggernaut/int/int-talant-10.png", name = "npc_dota_hero_juggernaut_int10", 
        }, 
        npc_dota_hero_juggernaut_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_juggernaut/int/int-talant-11.png", name = "npc_dota_hero_juggernaut_int11", 
        },  
        npc_dota_hero_juggernaut_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_juggernaut/int/int-talant-12.png", name = "npc_dota_hero_juggernaut_int12",
        },  
        special_bonus_unique_npc_dota_hero_juggernaut_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_juggernaut/int/int-talant-12.png", name = "npc_dota_hero_juggernaut_int_13",
        },  
    },
    npc_dota_hero_lina = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_lina_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_lina/str/str-talant-6.png", name = "npc_dota_hero_lina_str6",
        },
        npc_dota_hero_lina_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_lina/str/str-talant-7.png", name = "npc_dota_hero_lina_str7",
        },
        npc_dota_hero_lina_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_lina/str/str-talant-8.png", name = "npc_dota_hero_lina_str8",
        },
        npc_dota_hero_lina_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_lina/str/str-talant-9.png", name = "npc_dota_hero_lina_str9",
        },
        npc_dota_hero_lina_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_lina/str/str-talant-10.png", name = "npc_dota_hero_lina_str10",
        },
        npc_dota_hero_lina_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_lina/str/str-talant-11.png", name = "npc_dota_hero_lina_str11",
        },
        npc_dota_hero_lina_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_lina/str/str-talant-12.png", name = "npc_dota_hero_lina_str12",
        },
        special_bonus_unique_npc_dota_hero_lina_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_lina/str/str-talant-12.png", name = "npc_dota_hero_lina_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_lina_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_lina/agi/agi-talant-6.png", name = "npc_dota_hero_lina_agi6",
        },
        npc_dota_hero_lina_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_lina/agi/agi-talant-7.png", name = "npc_dota_hero_lina_agi7",
        },
        npc_dota_hero_lina_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_lina/agi/agi-talant-8.png", name = "npc_dota_hero_lina_agi8",
        },
        npc_dota_hero_lina_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_lina/agi/agi-talant-9.png", name = "npc_dota_hero_lina_agi9",
        },
        npc_dota_hero_lina_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_lina/agi/agi-talant-10.png", name = "npc_dota_hero_lina_agi10",
        },
        npc_dota_hero_lina_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_lina/agi/agi-talant-11.png", name = "npc_dota_hero_lina_agi11",
        },
        npc_dota_hero_lina_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_lina/agi/agi-talant-12.png", name = "npc_dota_hero_lina_agi12",
        },
        special_bonus_unique_npc_dota_hero_lina_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_lina/agi/agi-talant-12.png", name = "npc_dota_hero_lina_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_lina_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_lina/int/int-talant-6.png", name = "npc_dota_hero_lina_int6", 
        },
        npc_dota_hero_lina_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_lina/int/int-talant-7.png", name = "npc_dota_hero_lina_int7", 
        },
        npc_dota_hero_lina_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_lina/int/int-talant-8.png", name = "npc_dota_hero_lina_int8", 
        },
        npc_dota_hero_lina_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_lina/int/int-talant-9.png", name = "npc_dota_hero_lina_int9", 
        },
        npc_dota_hero_lina_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_lina/int/int-talant-10.png", name = "npc_dota_hero_lina_int10", 
        }, 
        npc_dota_hero_lina_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_lina/int/int-talant-11.png", name = "npc_dota_hero_lina_int11", 
        },  
        npc_dota_hero_lina_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_lina/int/int-talant-12.png", name = "npc_dota_hero_lina_int12",
        },  
        special_bonus_unique_npc_dota_hero_lina_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_lina/int/int-talant-7.png", name = "npc_dota_hero_lina_int13",
        },
    },
    npc_dota_hero_lion = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_lion_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_lion/str/str-talant-6.png", name = "npc_dota_hero_lion_str6",
        },
        npc_dota_hero_lion_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_lion/str/str-talant-7.png", name = "npc_dota_hero_lion_str7",
        },
        npc_dota_hero_lion_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_lion/str/str-talant-8.png", name = "npc_dota_hero_lion_str8",
        },
        npc_dota_hero_lion_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_lion/str/str-talant-9.png", name = "npc_dota_hero_lion_str9",
        },
        npc_dota_hero_lion_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_lion/str/str-talant-10.png", name = "npc_dota_hero_lion_str10",
        },
        npc_dota_hero_lion_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_lion/str/str-talant-11.png", name = "npc_dota_hero_lion_str11",
        },
        npc_dota_hero_lion_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_lion/str/str-talant-12.png", name = "npc_dota_hero_lion_str12",
        },
        special_bonus_unique_npc_dota_hero_lion_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_lion/str/str-talant-8.png", name = "npc_dota_hero_lion_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_lion_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_lion/agi/agi-talant-6.png", name = "npc_dota_hero_lion_agi6",
        },
        npc_dota_hero_lion_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_lion/agi/agi-talant-7.png", name = "npc_dota_hero_lion_agi7",
        },
        npc_dota_hero_lion_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_lion/agi/agi-talant-8.png", name = "npc_dota_hero_lion_agi8",
        },
        npc_dota_hero_lion_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_lion/agi/agi-talant-9.png", name = "npc_dota_hero_lion_agi9",
        },
        npc_dota_hero_lion_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_lion/agi/agi-talant-10.png", name = "npc_dota_hero_lion_agi10",
        },
        npc_dota_hero_lion_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_lion/agi/agi-talant-11.png", name = "npc_dota_hero_lion_agi11",
        },
        npc_dota_hero_lion_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_lion/agi/Finger_of_Death_icon.png", name = "npc_dota_hero_lion_agi12",
        },
        special_bonus_unique_npc_dota_hero_lion_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_lion/agi/agi-talant-9.png", name = "npc_dota_hero_lion_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_lion_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_lion/int/int-talant-6.png", name = "npc_dota_hero_lion_int6", 
        },
        npc_dota_hero_lion_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_lion/int/int-talant-7.png", name = "npc_dota_hero_lion_int7", 
        },
        npc_dota_hero_lion_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_lion/int/int-talant-8.png", name = "npc_dota_hero_lion_int8", 
        },
        npc_dota_hero_lion_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_lion/int/int-talant-9.png", name = "npc_dota_hero_lion_int9", 
        },
        npc_dota_hero_lion_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_lion/int/int-talant-10.png", name = "npc_dota_hero_lion_int10", 
        }, 
        npc_dota_hero_lion_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_lion/int/int-talant-11.png", name = "npc_dota_hero_lion_int11", 
        },  
        npc_dota_hero_lion_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_lion/int/int-talant-12.png", name = "npc_dota_hero_lion_int12",
        }, 
        special_bonus_unique_npc_dota_hero_lion_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_lion/int/int-talant-8.png", name = "npc_dota_hero_lion_int13",
        }, 
        
    },
    npc_dota_hero_luna = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_luna_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_luna/str/str-talant-6.png", name = "npc_dota_hero_luna_str6",
        },
        npc_dota_hero_luna_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_luna/str/str-talant-7.png", name = "npc_dota_hero_luna_str7",
        },
        npc_dota_hero_luna_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_luna/str/str-talant-8.png", name = "npc_dota_hero_luna_str8",
        },
        npc_dota_hero_luna_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_luna/str/str-talant-9.png", name = "npc_dota_hero_luna_str9",
        },
        npc_dota_hero_luna_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_luna/str/str-talant-10.png", name = "npc_dota_hero_luna_str10",
        },
        npc_dota_hero_luna_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_luna/str/str-talant-11.png", name = "npc_dota_hero_luna_str11",
        },
        npc_dota_hero_luna_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_luna/str/str-talant-12.png", name = "npc_dota_hero_luna_str12",
        },
        special_bonus_unique_npc_dota_hero_luna_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_luna/str/ult_luna.png", name = "npc_dota_hero_luna_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_luna_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_luna/agi/agi-talant-6.png", name = "npc_dota_hero_luna_agi6",
        },
        npc_dota_hero_luna_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_luna/agi/agi-talant-7.png", name = "npc_dota_hero_luna_agi7",
        },
        npc_dota_hero_luna_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_luna/agi/agi-talant-8.png", name = "npc_dota_hero_luna_agi8",
        },
        npc_dota_hero_luna_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_luna/agi/agi-talant-9.png", name = "npc_dota_hero_luna_agi9",
        },
        npc_dota_hero_luna_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_luna/agi/agi-talant-10.png", name = "npc_dota_hero_luna_agi10",
        },
        npc_dota_hero_luna_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_luna/agi/agi-talant-11.png", name = "npc_dota_hero_luna_agi11",
        },
        npc_dota_hero_luna_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_luna/agi/agi-talant-12.png", name = "npc_dota_hero_luna_agi12",
        },
        special_bonus_unique_npc_dota_hero_luna_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_luna/agi/agi-talant-11.png", name = "npc_dota_hero_luna_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_luna_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_luna/int/int-talant-6.png", name = "npc_dota_hero_luna_int6", 
        },
        npc_dota_hero_luna_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_luna/int/int-talant-7.png", name = "npc_dota_hero_luna_int7", 
        },
        npc_dota_hero_luna_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_luna/int/int-talant-8.png", name = "npc_dota_hero_luna_int8", 
        },
        npc_dota_hero_luna_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_luna/int/int-talant-9.png", name = "npc_dota_hero_luna_int9", 
        },
        npc_dota_hero_luna_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_luna/int/int-talant-10.png", name = "npc_dota_hero_luna_int10", 
        }, 
        npc_dota_hero_luna_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_luna/int/int-talant-11.png", name = "npc_dota_hero_luna_int11", 
        },  
        npc_dota_hero_luna_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_luna/int/int-talant-12.png", name = "npc_dota_hero_luna_int12",
        },  
        special_bonus_unique_npc_dota_hero_luna_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_luna/int/int-talant-12.png", name = "npc_dota_hero_luna_int13",
        },  
    },
    npc_dota_hero_mars = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_mars_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_mars/str/str-talant-6.png", name = "npc_dota_hero_mars_str6",
        },
        npc_dota_hero_mars_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_mars/str/str-talant-7.png", name = "npc_dota_hero_mars_str7",
        },
        npc_dota_hero_mars_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_mars/str/str-talant-8.png", name = "npc_dota_hero_mars_str8",
        },
        npc_dota_hero_mars_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_mars/str/str-talant-9.png", name = "npc_dota_hero_mars_str9",
        },
        npc_dota_hero_mars_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_mars/str/str-talant-10.png", name = "npc_dota_hero_mars_str10",
        },
        npc_dota_hero_mars_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_mars/str/str-talant-11.png", name = "npc_dota_hero_mars_str11",
        },
        npc_dota_hero_mars_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_mars/str/str-talant-12.png", name = "npc_dota_hero_mars_str12",
        },
        special_bonus_unique_npc_dota_hero_mars_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_mars/str/str-talant-12.png", name = "npc_dota_hero_mars_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_mars_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_mars/agi/agi-talant-6.png", name = "npc_dota_hero_mars_agi6",
        },
        npc_dota_hero_mars_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_mars/agi/agi-talant-7.png", name = "npc_dota_hero_mars_agi7",
        },
        npc_dota_hero_mars_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_mars/agi/agi-talant-8.png", name = "npc_dota_hero_mars_agi8",
        },
        npc_dota_hero_mars_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_mars/agi/agi-talant-9.png", name = "npc_dota_hero_mars_agi9",
        },
        npc_dota_hero_mars_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_mars/agi/agi-talant-10.png", name = "npc_dota_hero_mars_agi10",
        },
        npc_dota_hero_mars_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_mars/agi/agi-talant-11.png", name = "npc_dota_hero_mars_agi11",
        },
        npc_dota_hero_mars_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_mars/agi/agi-talant-12.png", name = "npc_dota_hero_mars_agi12",
        },
        special_bonus_unique_npc_dota_hero_mars_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_mars/agi/agi-talant-8.png", name = "npc_dota_hero_mars_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_mars_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_mars/int/int-talant-6.png", name = "npc_dota_hero_mars_int6", 
        },
        npc_dota_hero_mars_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_mars/int/int-talant-7.png", name = "npc_dota_hero_mars_int7", 
        },
        npc_dota_hero_mars_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_mars/int/int-talant-8.png", name = "npc_dota_hero_mars_int8", 
        },
        npc_dota_hero_mars_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_mars/int/int-talant-9.png", name = "npc_dota_hero_mars_int9", 
        },
        npc_dota_hero_mars_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_mars/int/int-talant-10.png", name = "npc_dota_hero_mars_int10", 
        }, 
        npc_dota_hero_mars_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_mars/int/int-talant-11.png", name = "npc_dota_hero_mars_int11", 
        },  
        npc_dota_hero_mars_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_mars/int/int-talant-12.png", name = "npc_dota_hero_mars_int12",
        },  
        special_bonus_unique_npc_dota_hero_mars_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_mars/int/int-talant-12.png", name = "npc_dota_hero_mars_int13",
        },
    },
    npc_dota_hero_nevermore = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_nevermore_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_nevermore/str/str-talant-6.png", name = "npc_dota_hero_nevermore_str6",
        },
        npc_dota_hero_nevermore_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_nevermore/str/str-talant-7.png", name = "npc_dota_hero_nevermore_str7",
        },
        npc_dota_hero_nevermore_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_nevermore/str/str-talant-8.png", name = "npc_dota_hero_nevermore_str8",
        },
        npc_dota_hero_nevermore_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_nevermore/str/str-talant-9.png", name = "npc_dota_hero_nevermore_str9",
        },
        npc_dota_hero_nevermore_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_nevermore/str/str-talant-10.png", name = "npc_dota_hero_nevermore_str10",
        },
        npc_dota_hero_nevermore_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_nevermore/str/str-talant-11.png", name = "npc_dota_hero_nevermore_str11",
        },
        npc_dota_hero_nevermore_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_nevermore/str/str-talant-12.png", name = "npc_dota_hero_nevermore_str12",
        },
        special_bonus_unique_npc_dota_hero_nevermore_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_nevermore/str/str-talant-12.png", name = "npc_dota_hero_nevermore_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_nevermore_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-6.png", name = "npc_dota_hero_nevermore_agi6",
        },
        npc_dota_hero_nevermore_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-7.png", name = "npc_dota_hero_nevermore_agi7",
        },
        npc_dota_hero_nevermore_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-8.png", name = "npc_dota_hero_nevermore_agi12",
        },
        npc_dota_hero_nevermore_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-9.png", name = "npc_dota_hero_nevermore_agi9",
        },
        npc_dota_hero_nevermore_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-10.png", name = "npc_dota_hero_nevermore_agi10",
        },
        npc_dota_hero_nevermore_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-11.png", name = "npc_dota_hero_nevermore_agi11",
        },
        npc_dota_hero_nevermore_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-10.png", name = "npc_dota_hero_nevermore_agi8",
        },
        special_bonus_unique_npc_dota_hero_nevermore_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_nevermore/agi/agi-talant-10.png", name = "npc_dota_hero_nevermore_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_nevermore_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_nevermore/int/int-talant-6.png", name = "npc_dota_hero_nevermore_int6",
        },
        npc_dota_hero_nevermore_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_nevermore/int/int-talant-7.png", name = "npc_dota_hero_nevermore_int7",
        },
        npc_dota_hero_nevermore_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_nevermore/int/int-talant-8.png", name = "npc_dota_hero_nevermore_int8",
        },
        npc_dota_hero_nevermore_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_nevermore/int/int-talant-9.png", name = "npc_dota_hero_nevermore_int9",
        },
        npc_dota_hero_nevermore_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_nevermore/int/int-talant-10.png", name = "npc_dota_hero_nevermore_int10",
        },
        npc_dota_hero_nevermore_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_nevermore/int/int-talant-11.png", name = "npc_dota_hero_nevermore_int11",
        },
        npc_dota_hero_nevermore_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_nevermore/int/int-talant-10.png", name = "npc_dota_hero_nevermore_int12",
        },
        special_bonus_unique_npc_dota_hero_nevermore_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_nevermore/int/int-talant-10.png", name = "npc_dota_hero_nevermore_int13",
        },
    },
    npc_dota_hero_phantom_assassin = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_phantom_assassin_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-6.png", name = "npc_dota_hero_phantom_assassin_str6",
        },
        npc_dota_hero_phantom_assassin_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-7.png", name = "npc_dota_hero_phantom_assassin_str7",
        },
        npc_dota_hero_phantom_assassin_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-8.png", name = "npc_dota_hero_phantom_assassin_str8",
        },
        npc_dota_hero_phantom_assassin_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-9.png", name = "npc_dota_hero_phantom_assassin_str9",
        },
        npc_dota_hero_phantom_assassin_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-10.png", name = "npc_dota_hero_phantom_assassin_str10",
        },
        npc_dota_hero_phantom_assassin_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-11.png", name = "npc_dota_hero_phantom_assassin_str11",
        },
        npc_dota_hero_phantom_assassin_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-12.png", name = "npc_dota_hero_phantom_assassin_str12",
        },
        special_bonus_unique_npc_dota_hero_phantom_assassin_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_phantom_assassin/str/str-talant-6.png", name = "npc_dota_hero_phantom_assassin_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_phantom_assassin_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-6.png", name = "npc_dota_hero_phantom_assassin_agi6",
        },
        npc_dota_hero_phantom_assassin_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-7.png", name = "npc_dota_hero_phantom_assassin_agi7",
        },
        npc_dota_hero_phantom_assassin_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-8.png", name = "npc_dota_hero_phantom_assassin_agi8",
        },
        npc_dota_hero_phantom_assassin_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-9.png", name = "npc_dota_hero_phantom_assassin_agi9",
        },
        npc_dota_hero_phantom_assassin_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-10.png", name = "npc_dota_hero_phantom_assassin_agi10",
        },
        npc_dota_hero_phantom_assassin_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-11.png", name = "npc_dota_hero_phantom_assassin_agi11",
        },
        npc_dota_hero_phantom_assassin_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-12.png", name = "npc_dota_hero_phantom_assassin_agi12",
        },
        special_bonus_unique_npc_dota_hero_phantom_assassin_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_phantom_assassin/agi/agi-talant-12.png", name = "npc_dota_hero_phantom_assassin_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_phantom_assassin_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-6.png", name = "npc_dota_hero_phantom_assassin_int6",
        },
        npc_dota_hero_phantom_assassin_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-7.png", name = "npc_dota_hero_phantom_assassin_int7",
        },
        npc_dota_hero_phantom_assassin_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-8.png", name = "npc_dota_hero_phantom_assassin_int8",
        },
        npc_dota_hero_phantom_assassin_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-9.png", name = "npc_dota_hero_phantom_assassin_int9",
        },
        npc_dota_hero_phantom_assassin_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-10.png", name = "npc_dota_hero_phantom_assassin_int10",
        },
        npc_dota_hero_phantom_assassin_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-11.png", name = "npc_dota_hero_phantom_assassin_int11",
        },
        npc_dota_hero_phantom_assassin_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-12.png", name = "npc_dota_hero_phantom_assassin_int12",
        },
        special_bonus_unique_npc_dota_hero_phantom_assassin_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_phantom_assassin/int/int-talant-12.png", name = "npc_dota_hero_phantom_assassin_int13",
        },
    },
    npc_dota_hero_pugna = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_pugna_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_pugna/str/str-talant-6.png", name = "npc_dota_hero_pugna_str6",
        },
        npc_dota_hero_pugna_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_pugna/str/str-talant-7.png", name = "npc_dota_hero_pugna_str7",
        },
        npc_dota_hero_pugna_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_pugna/str/str-talant-8.png", name = "npc_dota_hero_pugna_str8",
        },
        npc_dota_hero_pugna_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_pugna/str/str-talant-9.png", name = "npc_dota_hero_pugna_str9",
        },
        npc_dota_hero_pugna_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_pugna/str/str-talant-10.png", name = "npc_dota_hero_pugna_str10",
        },
        npc_dota_hero_pugna_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_pugna/str/str-talant-11.png", name = "npc_dota_hero_pugna_str11",
        },
        npc_dota_hero_pugna_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_pugna/str/str-talant-12.png", name = "npc_dota_hero_pugna_str12",
        },
        special_bonus_unique_npc_dota_hero_pugna_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_pugna/str/str-talant-12.png", name = "npc_dota_hero_pugna_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_pugna_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_pugna/agi/agi-talant-6.png", name = "npc_dota_hero_pugna_agi6",
        },
        npc_dota_hero_pugna_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_pugna/agi/agi-talant-7.png", name = "npc_dota_hero_pugna_agi7",
        },
        npc_dota_hero_pugna_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_pugna/agi/agi-talant-8.png", name = "npc_dota_hero_pugna_agi8",
        },
        npc_dota_hero_pugna_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_pugna/agi/agi-talant-9.png", name = "npc_dota_hero_pugna_agi9",
        },
        npc_dota_hero_pugna_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_pugna/agi/agi-talant-10.png", name = "npc_dota_hero_pugna_agi10",
        },
        npc_dota_hero_pugna_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_pugna/agi/agi-talant-11.png", name = "npc_dota_hero_pugna_agi11",
        },
        npc_dota_hero_pugna_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_pugna/agi/agi-talant-12.png", name = "npc_dota_hero_pugna_agi12",
        },
        special_bonus_unique_npc_dota_hero_pugna_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_pugna/agi/agi-talant-12.png", name = "npc_dota_hero_pugna_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_pugna_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_pugna/int/int-talant-6.png", name = "npc_dota_hero_pugna_int6",
        },
        npc_dota_hero_pugna_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_pugna/int/int-talant-7.png", name = "npc_dota_hero_pugna_int7",
        },
        npc_dota_hero_pugna_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_pugna/int/int-talant-8.png", name = "npc_dota_hero_pugna_int8",
        },
        npc_dota_hero_pugna_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_pugna/int/int-talant-9.png", name = "npc_dota_hero_pugna_int9",
        },
        npc_dota_hero_pugna_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_pugna/int/int-talant-10.png", name = "npc_dota_hero_pugna_int10",
        },
        npc_dota_hero_pugna_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_pugna/int/int-talant-11.png", name = "npc_dota_hero_pugna_int11",
        },
        npc_dota_hero_pugna_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_pugna/int/int-talant-12.png", name = "npc_dota_hero_pugna_int12",
        },
        special_bonus_unique_npc_dota_hero_pugna_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_pugna/int/int-talant-12.png", name = "npc_dota_hero_pugna_int13",
        },
    },
    npc_dota_hero_sand_king = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_sand_king_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_sand_king/str/str-talant-6.png", name = "npc_dota_hero_sand_king_str6",
        },
        npc_dota_hero_sand_king_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_sand_king/str/str-talant-7.png", name = "npc_dota_hero_sand_king_str7",
        },
        npc_dota_hero_sand_king_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_sand_king/str/str-talant-8.png", name = "npc_dota_hero_sand_king_str8",
        },
        npc_dota_hero_sand_king_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_sand_king/str/str-talant-9.png", name = "npc_dota_hero_sand_king_str9",
        },
        npc_dota_hero_sand_king_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_sand_king/str/str-talant-10.png", name = "npc_dota_hero_sand_king_str10",
        },
        npc_dota_hero_sand_king_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_sand_king/str/str-talant-11.png", name = "npc_dota_hero_sand_king_str11",
        },
        npc_dota_hero_sand_king_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_sand_king/str/str-talant-12.png", name = "npc_dota_hero_sand_king_str12",
        },
        special_bonus_unique_npc_dota_hero_sand_king_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_sand_king/str/str-talant-12.png", name = "npc_dota_hero_sand_king_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_sand_king_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-6.png", name = "npc_dota_hero_sand_king_agi6",
        },
        npc_dota_hero_sand_king_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-7.png", name = "npc_dota_hero_sand_king_agi7",
        },
        npc_dota_hero_sand_king_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-8.png", name = "npc_dota_hero_sand_king_agi8",
        },
        npc_dota_hero_sand_king_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-9.png", name = "npc_dota_hero_sand_king_agi9",
        },
        npc_dota_hero_sand_king_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-10.png", name = "npc_dota_hero_sand_king_agi10",
        },
        npc_dota_hero_sand_king_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-11.png", name = "npc_dota_hero_sand_king_agi11",
        },
        npc_dota_hero_sand_king_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-12.png", name = "npc_dota_hero_sand_king_agi12",
        },
        special_bonus_unique_npc_dota_hero_sand_king_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_sand_king/agi/agi-talant-12.png", name = "npc_dota_hero_sand_king_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_sand_king_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_sand_king/int/int-talant-6.png", name = "npc_dota_hero_sand_king_int6",
        },
        npc_dota_hero_sand_king_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_sand_king/int/int-talant-7.png", name = "npc_dota_hero_sand_king_int7",
        },
        npc_dota_hero_sand_king_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_sand_king/int/int-talant-8.png", name = "npc_dota_hero_sand_king_int8",
        },
        npc_dota_hero_sand_king_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_sand_king/int/int-talant-9.png", name = "npc_dota_hero_sand_king_int9",
        },
        npc_dota_hero_sand_king_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_sand_king/int/int-talant-10.png", name = "npc_dota_hero_sand_king_int10",
        },
        npc_dota_hero_sand_king_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_sand_king/int/int-talant-11.png", name = "npc_dota_hero_sand_king_int11",
        },
        npc_dota_hero_sand_king_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_sand_king/int/int-talant-12.png", name = "npc_dota_hero_sand_king_int12",
        },
        special_bonus_unique_npc_dota_hero_sand_king_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_sand_king/int/int-talant-6.png", name = "npc_dota_hero_sand_king_int13",
        },
    },
    npc_dota_hero_shadow_shaman = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_shadow_shaman_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-6.png", name = "npc_dota_hero_shadow_shaman_str6",
        },
        npc_dota_hero_shadow_shaman_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-7.png", name = "npc_dota_hero_shadow_shaman_str7",
        },
        npc_dota_hero_shadow_shaman_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-8.png", name = "npc_dota_hero_shadow_shaman_str8",
        },
        npc_dota_hero_shadow_shaman_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-9.png", name = "npc_dota_hero_shadow_shaman_str9",
        },
        npc_dota_hero_shadow_shaman_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-10.png", name = "npc_dota_hero_shadow_shaman_str10",
        },
        npc_dota_hero_shadow_shaman_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-11.png", name = "npc_dota_hero_shadow_shaman_str11",
        },
        npc_dota_hero_shadow_shaman_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-12.png", name = "npc_dota_hero_shadow_shaman_str12",
        },
        special_bonus_unique_npc_dota_hero_shadow_shaman_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_shadow_shaman/str/str-talant-12.png", name = "npc_dota_hero_shadow_shaman_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_shadow_shaman_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-6.png", name = "npc_dota_hero_shadow_shaman_agi6",
        },
        npc_dota_hero_shadow_shaman_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-7.png", name = "npc_dota_hero_shadow_shaman_agi7",
        },
        npc_dota_hero_shadow_shaman_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-8.png", name = "npc_dota_hero_shadow_shaman_agi8",
        },
        npc_dota_hero_shadow_shaman_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-9.png", name = "npc_dota_hero_shadow_shaman_agi9",
        },
        npc_dota_hero_shadow_shaman_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-10.png", name = "npc_dota_hero_shadow_shaman_agi10",
        },
        npc_dota_hero_shadow_shaman_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-11.png", name = "npc_dota_hero_shadow_shaman_agi11",
        },
        npc_dota_hero_shadow_shaman_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-12.png", name = "npc_dota_hero_shadow_shaman_agi12",
        },
        special_bonus_unique_npc_dota_hero_shadow_shaman_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_shadow_shaman/agi/agi-talant-12.png", name = "npc_dota_hero_shadow_shaman_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_shadow_shaman_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-6.png", name = "npc_dota_hero_shadow_shaman_int6",
        },
        npc_dota_hero_shadow_shaman_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-7.png", name = "npc_dota_hero_shadow_shaman_int7",
        },
        npc_dota_hero_shadow_shaman_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-8.png", name = "npc_dota_hero_shadow_shaman_int8",
        },
        npc_dota_hero_shadow_shaman_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-9.png", name = "npc_dota_hero_shadow_shaman_int9",
        },
        npc_dota_hero_shadow_shaman_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-10.png", name = "npc_dota_hero_shadow_shaman_int10",
        },
        npc_dota_hero_shadow_shaman_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-11.png", name = "npc_dota_hero_shadow_shaman_int11",
        },
        npc_dota_hero_shadow_shaman_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-12.png", name = "npc_dota_hero_shadow_shaman_int12",
        },
        special_bonus_unique_npc_dota_hero_shadow_shaman_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_shadow_shaman/int/int-talant-12.png", name = "npc_dota_hero_shadow_shaman_int13",
        },
    },
    npc_dota_hero_skeleton_king = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_skeleton_king_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-6.png", name = "npc_dota_hero_skeleton_king_str6",
        },
        npc_dota_hero_skeleton_king_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-7.png", name = "npc_dota_hero_skeleton_king_str7",
        },
        npc_dota_hero_skeleton_king_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-8.png", name = "npc_dota_hero_skeleton_king_str8",
        },
        npc_dota_hero_skeleton_king_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-9.png", name = "npc_dota_hero_skeleton_king_str9",
        },
        npc_dota_hero_skeleton_king_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-10.png", name = "npc_dota_hero_skeleton_king_str10",
        },
        npc_dota_hero_skeleton_king_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-11.png", name = "npc_dota_hero_skeleton_king_str11",
        },
        npc_dota_hero_skeleton_king_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-12.png", name = "npc_dota_hero_skeleton_king_str12",
        },
        special_bonus_unique_npc_dota_hero_skeleton_king_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_skeleton_king/str/str-talant-12.png", name = "npc_dota_hero_skeleton_king_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_skeleton_king_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-6.png", name = "npc_dota_hero_skeleton_king_agi6",
        },
        npc_dota_hero_skeleton_king_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-7.png", name = "npc_dota_hero_skeleton_king_agi7",
        },
        npc_dota_hero_skeleton_king_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-8.png", name = "npc_dota_hero_skeleton_king_agi8",
        },
        npc_dota_hero_skeleton_king_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-9.png", name = "npc_dota_hero_skeleton_king_agi9",
        },
        npc_dota_hero_skeleton_king_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-10.png", name = "npc_dota_hero_skeleton_king_agi10",
        },
        npc_dota_hero_skeleton_king_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-11.png", name = "npc_dota_hero_skeleton_king_agi11",
        },
        npc_dota_hero_skeleton_king_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-12.png", name = "npc_dota_hero_skeleton_king_agi12",
        },
        special_bonus_unique_npc_dota_hero_skeleton_king_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_skeleton_king/agi/agi-talant-12.png", name = "npc_dota_hero_skeleton_king_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_skeleton_king_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-6.png", name = "npc_dota_hero_skeleton_king_int6",
        },
        npc_dota_hero_skeleton_king_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-7.png", name = "npc_dota_hero_skeleton_king_int7",
        },
        npc_dota_hero_skeleton_king_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-8.png", name = "npc_dota_hero_skeleton_king_int8",
        },
        npc_dota_hero_skeleton_king_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-9.png", name = "npc_dota_hero_skeleton_king_int9",
        },
        npc_dota_hero_skeleton_king_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-10.png", name = "npc_dota_hero_skeleton_king_int10",
        },
        npc_dota_hero_skeleton_king_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-11.png", name = "npc_dota_hero_skeleton_king_int11",
        },
        npc_dota_hero_skeleton_king_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-12.png", name = "npc_dota_hero_skeleton_king_int12",
        },
        special_bonus_unique_npc_dota_hero_skeleton_king_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_skeleton_king/int/int-talant-6.png", name = "npc_dota_hero_skeleton_king_int13",
        },
    },
    npc_dota_hero_slark = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_slark_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_slark/str/str-talant-6.png", name = "npc_dota_hero_slark_str6",
        },
        npc_dota_hero_slark_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_slark/str/str-talant-7.png", name = "npc_dota_hero_slark_str7",
        },
        npc_dota_hero_slark_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_slark/str/str-talant-8.png", name = "npc_dota_hero_slark_str8",
        },
        npc_dota_hero_slark_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_slark/str/str-talant-9.png", name = "npc_dota_hero_slark_str9",
        },
        npc_dota_hero_slark_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_slark/str/str-talant-10.png", name = "npc_dota_hero_slark_str10",
        },
        npc_dota_hero_slark_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_slark/str/str-talant-11.png", name = "npc_dota_hero_slark_str11",
        },
        npc_dota_hero_slark_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_slark/str/str-talant-12.png", name = "npc_dota_hero_slark_str12",
        },
        special_bonus_unique_npc_dota_hero_slark_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_slark/str/Shadow_Dance_icon.png", name = "npc_dota_hero_slark_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_slark_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_slark/agi/agi-talant-6.png", name = "npc_dota_hero_slark_agi6",
        },
        npc_dota_hero_slark_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_slark/agi/agi-talant-7.png", name = "npc_dota_hero_slark_agi7",
        },
        npc_dota_hero_slark_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_slark/agi/agi-talant-8.png", name = "npc_dota_hero_slark_agi8",
        },
        npc_dota_hero_slark_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_slark/agi/agi-talant-9.png", name = "npc_dota_hero_slark_agi9",
        },
        npc_dota_hero_slark_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_slark/agi/agi-talant-10.png", name = "npc_dota_hero_slark_agi10",
        },
        npc_dota_hero_slark_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_slark/agi/agi-talant-11.png", name = "npc_dota_hero_slark_agi11",
        },
        npc_dota_hero_slark_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_slark/agi/agi-talant-12.png", name = "npc_dota_hero_slark_agi12",
        },
        special_bonus_unique_npc_dota_hero_slark_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_slark/agi/agi-talant-12.png", name = "npc_dota_hero_slark_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_slark_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_slark/int/int-talant-6.png", name = "npc_dota_hero_slark_int6",
        },
        npc_dota_hero_slark_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_slark/int/int-talant-7.png", name = "npc_dota_hero_slark_int7",
        },
        npc_dota_hero_slark_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_slark/int/int-talant-8.png", name = "npc_dota_hero_slark_int8",
        },
        npc_dota_hero_slark_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_slark/int/int-talant-9.png", name = "npc_dota_hero_slark_int9",
        },
        npc_dota_hero_slark_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_slark/int/int-talant-10.png", name = "npc_dota_hero_slark_int10",
        },
        npc_dota_hero_slark_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_slark/int/int-talant-11.png", name = "npc_dota_hero_slark_int11",
        },
        npc_dota_hero_slark_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_slark/int/int-talant-12.png", name = "npc_dota_hero_slark_int12",
        },
        special_bonus_unique_npc_dota_hero_slark_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_slark/int/int-talant-12.png", name = "npc_dota_hero_slark_int13",
        },
    },
    npc_dota_hero_sniper = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_sniper_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_sniper/str/str-talant-6.png", name = "npc_dota_hero_sniper_str6",
        },
        npc_dota_hero_sniper_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_sniper/str/str-talant-7.png", name = "npc_dota_hero_sniper_str7",
        },
        npc_dota_hero_sniper_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_sniper/str/str-talant-8.png", name = "npc_dota_hero_sniper_str8",
        },
        npc_dota_hero_sniper_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_sniper/str/str-talant-9.png", name = "npc_dota_hero_sniper_str9",
        },
        npc_dota_hero_sniper_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_sniper/str/str-talant-10.png", name = "npc_dota_hero_sniper_str10",
        },
        npc_dota_hero_sniper_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_sniper/str/str-talant-11.png", name = "npc_dota_hero_sniper_str11",
        },
        npc_dota_hero_sniper_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_sniper/str/str-talant-12.png", name = "npc_dota_hero_sniper_str12",
        },
        special_bonus_unique_npc_dota_hero_sniper_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_sniper/str/str-talant-12.png", name = "npc_dota_hero_sniper_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_sniper_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_sniper/agi/agi-talant-6.png", name = "npc_dota_hero_sniper_agi6",
        },
        npc_dota_hero_sniper_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_sniper/agi/agi-talant-7.png", name = "npc_dota_hero_sniper_agi7",
        },
        npc_dota_hero_sniper_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_sniper/agi/agi-talant-8.png", name = "npc_dota_hero_sniper_agi8",
        },
        npc_dota_hero_sniper_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_sniper/agi/agi-talant-9.png", name = "npc_dota_hero_sniper_agi9",
        },
        npc_dota_hero_sniper_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_sniper/agi/agi-talant-10.png", name = "npc_dota_hero_sniper_agi10",
        },
        npc_dota_hero_sniper_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_sniper/agi/agi-talant-11.png", name = "npc_dota_hero_sniper_agi11",
        },
        npc_dota_hero_sniper_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_sniper/agi/agi-talant-12.png", name = "npc_dota_hero_sniper_agi12",
        },
        special_bonus_unique_npc_dota_hero_sniper_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_sniper/agi/assault.png", name = "npc_dota_hero_sniper_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_sniper_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_sniper/int/int-talant-6.png", name = "npc_dota_hero_sniper_int6",
        },
        npc_dota_hero_sniper_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_sniper/int/int-talant-7.png", name = "npc_dota_hero_sniper_int7",
        },
        npc_dota_hero_sniper_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_sniper/int/int-talant-8.png", name = "npc_dota_hero_sniper_int8",
        },
        npc_dota_hero_sniper_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_sniper/int/int-talant-9.png", name = "npc_dota_hero_sniper_int9",
        },
        npc_dota_hero_sniper_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_sniper/int/int-talant-10.png", name = "npc_dota_hero_sniper_int10",
        },
        npc_dota_hero_sniper_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_sniper/int/int-talant-11.png", name = "npc_dota_hero_sniper_int11",
        },
        npc_dota_hero_sniper_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_sniper/int/int-talant-12.png", name = "npc_dota_hero_sniper_int12",
        },
        special_bonus_unique_npc_dota_hero_sniper_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_sniper/int/int-talant-12.png", name = "npc_dota_hero_sniper_int13",
        },
    },
    npc_dota_hero_spectre = {
        modifier_talent_spectre_str3 = {
            place = {"str 3"}, url = "/basic/str/str-talant-3.png", name = "modifier_talent_spectre_str3",
        },
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_spectre_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_spectre/str/3.png", name = "npc_dota_hero_spectre_str6",
        },
        npc_dota_hero_spectre_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_spectre/str/2.png", name = "npc_dota_hero_spectre_str7",
        },
        npc_dota_hero_spectre_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_spectre/str/5.png", name = "npc_dota_hero_spectre_str8",
        },
        npc_dota_hero_spectre_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_spectre/str/3.png", name = "npc_dota_hero_spectre_str9",
        },
        npc_dota_hero_spectre_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_spectre/str/2.png", name = "npc_dota_hero_spectre_str10",
        },
        npc_dota_hero_spectre_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_spectre/str/4.png", name = "npc_dota_hero_spectre_str11",
        },
        npc_dota_hero_spectre_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_spectre/str/2.png", name = "npc_dota_hero_spectre_str12",
        },
        special_bonus_unique_npc_dota_hero_spectre_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_spectre/str/2.png", name = "npc_dota_hero_spectre_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_spectre_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_spectre/agi/2.png", name = "npc_dota_hero_spectre_agi6",
        },
        npc_dota_hero_spectre_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_spectre/agi/5.png", name = "npc_dota_hero_spectre_agi7",
        },
        npc_dota_hero_spectre_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_spectre/agi/1.png", name = "npc_dota_hero_spectre_agi8",
        },
        npc_dota_hero_spectre_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_spectre/agi/4.png", name = "npc_dota_hero_spectre_agi9",
        },
        npc_dota_hero_spectre_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_spectre/agi/1.png", name = "npc_dota_hero_spectre_agi10", tooltip = "npc_dota_hero_spectre_agi10_tooltip",
        },
        npc_dota_hero_spectre_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_spectre/agi/1.png", name = "npc_dota_hero_spectre_agi11",
        },
        npc_dota_hero_spectre_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_spectre/agi/1.png", name = "npc_dota_hero_spectre_agi12",
        },
        special_bonus_unique_npc_dota_hero_spectre_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_spectre/agi/1.png", name = "npc_dota_hero_spectre_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_spectre_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_spectre/int/1.png", name = "npc_dota_hero_spectre_int6",
        },
        npc_dota_hero_spectre_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_spectre/int/4.png", name = "npc_dota_hero_spectre_int7",
        },
        npc_dota_hero_spectre_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_spectre/int/2.png", name = "npc_dota_hero_spectre_int8", tooltip = "npc_dota_hero_spectre_int8_tooltip",
        },
        npc_dota_hero_spectre_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_spectre/int/1.png", name = "npc_dota_hero_spectre_int9",
        },
        npc_dota_hero_spectre_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_spectre/int/4.png", name = "npc_dota_hero_spectre_int10",
        },
        npc_dota_hero_spectre_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_spectre/int/2.png", name = "npc_dota_hero_spectre_int11",
        },
        npc_dota_hero_spectre_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_spectre/int/1.png", name = "npc_dota_hero_spectre_int12", tooltip = "npc_dota_hero_spectre_int_last_tooltip",
        },
        special_bonus_unique_npc_dota_hero_spectre_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_spectre/int/1.png", name = "npc_dota_hero_spectre_int13", tooltip = "npc_dota_hero_spectre_int_last_tooltip",
        },
    },
    npc_dota_hero_sven = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_sven_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_sven/str/str-talant-6.png", name = "npc_dota_hero_sven_str6",
        },
        npc_dota_hero_sven_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_sven/str/str-talant-7.png", name = "npc_dota_hero_sven_str7",
        },
        npc_dota_hero_sven_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_sven/str/str-talant-8.png", name = "npc_dota_hero_sven_str8",
        },
        npc_dota_hero_sven_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_sven/str/str-talant-9.png", name = "npc_dota_hero_sven_str9",
        },
        npc_dota_hero_sven_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_sven/str/str-talant-10.png", name = "npc_dota_hero_sven_str10",
        },
        npc_dota_hero_sven_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_sven/str/str-talant-11.png", name = "npc_dota_hero_sven_str11",
        },
        npc_dota_hero_sven_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_sven/str/str-talant-12.png", name = "npc_dota_hero_sven_str12",
        },
        special_bonus_unique_npc_dota_hero_sven_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_sven/str/str-talant-12.png", name = "npc_dota_hero_sven_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_sven_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_sven/agi/agi-talant-6.png", name = "npc_dota_hero_sven_agi6",
        },
        npc_dota_hero_sven_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_sven/agi/agi-talant-7.png", name = "npc_dota_hero_sven_agi7",
        },
        npc_dota_hero_sven_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_sven/agi/agi-talant-8.png", name = "npc_dota_hero_sven_agi8",
        },
        npc_dota_hero_sven_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_sven/agi/agi-talant-9.png", name = "npc_dota_hero_sven_agi9",
        },
        npc_dota_hero_sven_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_sven/agi/agi-talant-10.png", name = "npc_dota_hero_sven_agi10",
        },
        npc_dota_hero_sven_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_sven/agi/agi-talant-11.png", name = "npc_dota_hero_sven_agi11",
        },
        npc_dota_hero_sven_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_sven/agi/agi-talant-12.png", name = "npc_dota_hero_sven_agi12",
        },
        special_bonus_unique_npc_dota_hero_sven_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_sven/agi/agi-talant-7.png", name = "npc_dota_hero_sven_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_sven_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_sven/int/int-talant-6.png", name = "npc_dota_hero_sven_int6",
        },
        npc_dota_hero_sven_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_sven/int/int-talant-7.png", name = "npc_dota_hero_sven_int7",
        },
        npc_dota_hero_sven_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_sven/int/int-talant-8.png", name = "npc_dota_hero_sven_int8",
        },
        npc_dota_hero_sven_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_sven/int/int-talant-9.png", name = "npc_dota_hero_sven_int9",
        },
        npc_dota_hero_sven_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_sven/int/int-talant-10.png", name = "npc_dota_hero_sven_int10",
        },
        npc_dota_hero_sven_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_sven/int/int-talant-11.png", name = "npc_dota_hero_sven_int11",
        },
        npc_dota_hero_sven_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_sven/int/int-talant-12.png", name = "npc_dota_hero_sven_int12",
        },
        special_bonus_unique_npc_dota_hero_sven_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_sven/int/int-talant-12.png", name = "npc_dota_hero_sven_int13",
        },
    },
    npc_dota_hero_terrorblade = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_terrorblade_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_terrorblade/str/str-talant-6.png", name = "npc_dota_hero_terrorblade_str6",
        },
        npc_dota_hero_terrorblade_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_terrorblade/str/str-talant-7.png", name = "npc_dota_hero_terrorblade_str7",
        },
        npc_dota_hero_terrorblade_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_terrorblade/str/str-talant-8.png", name = "npc_dota_hero_terrorblade_str8",
        },
        npc_dota_hero_terrorblade_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_terrorblade/str/str-talant-9.png", name = "npc_dota_hero_terrorblade_str9",
        },
        npc_dota_hero_terrorblade_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_terrorblade/str/str-talant-10.png", name = "npc_dota_hero_terrorblade_str10",
        },
        npc_dota_hero_terrorblade_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_terrorblade/str/str-talant-11.png", name = "npc_dota_hero_terrorblade_str11",
        },
        npc_dota_hero_terrorblade_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_terrorblade/str/str-talant-12.png", name = "npc_dota_hero_terrorblade_str12",
        },
        special_bonus_unique_npc_dota_hero_terrorblade_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_terrorblade/str/str-talant-12.png", name = "npc_dota_hero_terrorblade_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_terrorblade_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-6.png", name = "npc_dota_hero_terrorblade_agi6",
        },
        npc_dota_hero_terrorblade_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-7.png", name = "npc_dota_hero_terrorblade_agi7",
        },
        npc_dota_hero_terrorblade_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-8.png", name = "npc_dota_hero_terrorblade_agi8",
        },
        npc_dota_hero_terrorblade_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-9.png", name = "npc_dota_hero_terrorblade_agi9",
        },
        npc_dota_hero_terrorblade_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-10.png", name = "npc_dota_hero_terrorblade_agi10",
        },
        npc_dota_hero_terrorblade_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-11.png", name = "npc_dota_hero_terrorblade_agi11",
        },
        npc_dota_hero_terrorblade_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-12.png", name = "npc_dota_hero_terrorblade_agi12",
        },
        special_bonus_unique_npc_dota_hero_terrorblade_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_terrorblade/agi/agi-talant-12.png", name = "npc_dota_hero_terrorblade_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_terrorblade_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_terrorblade/int/int-talant-6.png", name = "npc_dota_hero_terrorblade_int6",
        },
        npc_dota_hero_terrorblade_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_terrorblade/int/int-talant-7.png", name = "npc_dota_hero_terrorblade_int7",
        },
        npc_dota_hero_terrorblade_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_terrorblade/int/int-talant-8.png", name = "npc_dota_hero_terrorblade_int8",
        },
        npc_dota_hero_terrorblade_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_terrorblade/int/int-talant-9.png", name = "npc_dota_hero_terrorblade_int9",
        },
        npc_dota_hero_terrorblade_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_terrorblade/int/int-talant-10.png", name = "npc_dota_hero_terrorblade_int10",
        },
        npc_dota_hero_terrorblade_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_terrorblade/int/int-talant-11.png", name = "npc_dota_hero_terrorblade_int11",
        },
        npc_dota_hero_terrorblade_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_terrorblade/int/int-talant-12.png", name = "npc_dota_hero_terrorblade_int12",
        },
        special_bonus_unique_npc_dota_hero_terrorblade_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_terrorblade/int/int-talant-12.png", name = "npc_dota_hero_terrorblade_int13",
        },
    },
    npc_dota_hero_tinker = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_tinker_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_tinker/str/str-talant-6.png", name = "npc_dota_hero_tinker_str6",
        },
        npc_dota_hero_tinker_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_tinker/str/str-talant-7.png", name = "npc_dota_hero_tinker_str7",
        },
        npc_dota_hero_tinker_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_tinker/str/str-talant-8.png", name = "npc_dota_hero_tinker_str8",
        },
        npc_dota_hero_tinker_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_tinker/str/str-talant-9.png", name = "npc_dota_hero_tinker_str9",
        },
        npc_dota_hero_tinker_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_tinker/str/str-talant-10.png", name = "npc_dota_hero_tinker_str10",
        },
        npc_dota_hero_tinker_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_tinker/str/str-talant-11.png", name = "npc_dota_hero_tinker_str11",
        },
        npc_dota_hero_tinker_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_tinker/str/str-talant-12.png", name = "npc_dota_hero_tinker_str12",
        },
        special_bonus_unique_npc_dota_hero_tinker_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_tinker/str/str-talant-12.png", name = "npc_dota_hero_tinker_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_tinker_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_tinker/agi/agi-talant-6.png", name = "npc_dota_hero_tinker_agi6",
        },
        npc_dota_hero_tinker_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_tinker/agi/agi-talant-7.png", name = "npc_dota_hero_tinker_agi7",
        },
        npc_dota_hero_tinker_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_tinker/agi/agi-talant-8.png", name = "npc_dota_hero_tinker_agi8",
        },
        npc_dota_hero_tinker_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_tinker/agi/agi-talant-9.png", name = "npc_dota_hero_tinker_agi9",
        },
        npc_dota_hero_tinker_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_tinker/agi/agi-talant-10.png", name = "npc_dota_hero_tinker_agi10",
        },
        npc_dota_hero_tinker_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_tinker/agi/agi-talant-11.png", name = "npc_dota_hero_tinker_agi11",
        },
        npc_dota_hero_tinker_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_tinker/agi/agi-talant-12.png", name = "npc_dota_hero_tinker_agi12",
        },
        special_bonus_unique_npc_dota_hero_tinker_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_tinker/agi/agi-talant-12.png", name = "npc_dota_hero_tinker_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_tinker_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_tinker/int/int-talant-6.png", name = "npc_dota_hero_tinker_int6",
        },
        npc_dota_hero_tinker_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_tinker/int/int-talant-7.png", name = "npc_dota_hero_tinker_int7",
        },
        npc_dota_hero_tinker_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_tinker/int/int-talant-8.png", name = "npc_dota_hero_tinker_int8",
        },
        npc_dota_hero_tinker_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_tinker/int/int-talant-9.png", name = "npc_dota_hero_tinker_int9",
        },
        npc_dota_hero_tinker_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_tinker/int/int-talant-10.png", name = "npc_dota_hero_tinker_int10",
        },
        npc_dota_hero_tinker_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_tinker/int/int-talant-11.png", name = "npc_dota_hero_tinker_int11",
        },
        npc_dota_hero_tinker_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_tinker/int/int-talant-12.png", name = "npc_dota_hero_tinker_int12",
        },
        special_bonus_unique_npc_dota_hero_tinker_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_tinker/int/int-talant-8.png", name = "npc_dota_hero_tinker_int13",
        },
    },
    npc_dota_hero_treant = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_treant_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_treant/str/str-talant-6.png", name = "npc_dota_hero_treant_str6",
        },
        npc_dota_hero_treant_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_treant/str/str-talant-7.png", name = "npc_dota_hero_treant_str7",
        },
        npc_dota_hero_treant_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_treant/str/str-talant-8.png", name = "npc_dota_hero_treant_str8",
        },
        npc_dota_hero_treant_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_treant/str/str-talant-9.png", name = "npc_dota_hero_treant_str9",
        },
        npc_dota_hero_treant_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_treant/str/str-talant-10.png", name = "npc_dota_hero_treant_str10",
        },
        npc_dota_hero_treant_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_treant/str/str-talant-11.png", name = "npc_dota_hero_treant_str11",
        },
        npc_dota_hero_treant_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_treant/str/str-talant-12.png", name = "npc_dota_hero_treant_str12",
        },
        special_bonus_base_npc_dota_hero_treant_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_treant/str/str-talant-12.png", name = "npc_dota_hero_treant_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_treant_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_treant/agi/agi-talant-6.png", name = "npc_dota_hero_treant_agi6",
        },
        npc_dota_hero_treant_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_treant/agi/agi-talant-7.png", name = "npc_dota_hero_treant_agi7",
        },
        npc_dota_hero_treant_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_treant/agi/agi-talant-8.png", name = "npc_dota_hero_treant_agi8",
        },
        npc_dota_hero_treant_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_treant/agi/agi-talant-9.png", name = "npc_dota_hero_treant_agi9",
        },
        npc_dota_hero_treant_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_treant/agi/agi-talant-10.png", name = "npc_dota_hero_treant_agi10",
        },
        npc_dota_hero_treant_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_treant/agi/agi-talant-11.png", name = "npc_dota_hero_treant_agi11",
        },
        npc_dota_hero_treant_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_treant/agi/agi-talant-12.png", name = "npc_dota_hero_treant_agi12",
        },
        special_bonus_base_npc_dota_hero_treant_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_treant/agi/agi-talant-12.png", name = "npc_dota_hero_treant_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_treant_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_treant/int/int-talant-6.png", name = "npc_dota_hero_treant_int6",
        },
        npc_dota_hero_treant_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_treant/int/int-talant-7.png", name = "npc_dota_hero_treant_int7",
        },
        npc_dota_hero_treant_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_treant/int/int-talant-8.png", name = "npc_dota_hero_treant_int8",
        },
        npc_dota_hero_treant_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_treant/int/int-talant-9.png", name = "npc_dota_hero_treant_int9",
        },
        npc_dota_hero_treant_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_treant/int/int-talant-10.png", name = "npc_dota_hero_treant_int10",
        },
        npc_dota_hero_treant_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_treant/int/int-talant-11.png", name = "npc_dota_hero_treant_int11",
        },
        npc_dota_hero_treant_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_treant/int/int-talant-12.png", name = "npc_dota_hero_treant_int12",
        },
        special_bonus_unique_npc_dota_hero_treant_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_treant/int/int-talant-12.png", name = "npc_dota_hero_treant_int13",
        },
    },
    npc_dota_hero_warlock = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_warlock_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_warlock/str/str-talant-6.png", name = "npc_dota_hero_warlock_str6",
        },
        npc_dota_hero_warlock_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_warlock/str/str-talant-7.png", name = "npc_dota_hero_warlock_str7",
        },
        npc_dota_hero_warlock_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_warlock/str/str-talant-8.png", name = "npc_dota_hero_warlock_str8",
        },
        npc_dota_hero_warlock_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_warlock/str/str-talant-9.png", name = "npc_dota_hero_warlock_str9",
        },
        npc_dota_hero_warlock_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_warlock/str/str-talant-10.png", name = "npc_dota_hero_warlock_str10",
        },
        npc_dota_hero_warlock_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_warlock/str/str-talant-11.png", name = "npc_dota_hero_warlock_str11",
        },
        npc_dota_hero_warlock_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_warlock/str/str-talant-12.png", name = "npc_dota_hero_warlock_str12",
        },
        special_bonus_unique_npc_dota_hero_warlock_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_warlock/str/str-talant-12.png", name = "npc_dota_hero_warlock_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_warlock_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_warlock/agi/agi-talant-6.png", name = "npc_dota_hero_warlock_agi6",
        },
        npc_dota_hero_warlock_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_warlock/agi/agi-talant-7.png", name = "npc_dota_hero_warlock_agi7",
        },
        npc_dota_hero_warlock_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_warlock/agi/agi-talant-8.png", name = "npc_dota_hero_warlock_agi8",
        },
        npc_dota_hero_warlock_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_warlock/agi/agi-talant-9.png", name = "npc_dota_hero_warlock_agi9",
        },
        npc_dota_hero_warlock_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_warlock/agi/agi-talant-10.png", name = "npc_dota_hero_warlock_agi10",
        },
        npc_dota_hero_warlock_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_warlock/agi/agi-talant-11.png", name = "npc_dota_hero_warlock_agi11",
        },
        npc_dota_hero_warlock_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_warlock/agi/agi-talant-12.png", name = "npc_dota_hero_warlock_agi12",
        },
        special_bonus_unique_npc_dota_hero_warlock_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_warlock/agi/agi-talant-12.png", name = "npc_dota_hero_warlock_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_warlock_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_warlock/int/int-talant-6.png", name = "npc_dota_hero_warlock_int6",
        },
        npc_dota_hero_warlock_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_warlock/int/int-talant-7.png", name = "npc_dota_hero_warlock_int7",
        },
        npc_dota_hero_warlock_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_warlock/int/int-talant-8.png", name = "npc_dota_hero_warlock_int8",
        },
        npc_dota_hero_warlock_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_warlock/int/int-talant-9.png", name = "npc_dota_hero_warlock_int9",
        },
        npc_dota_hero_warlock_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_warlock/int/int-talant-10.png", name = "npc_dota_hero_warlock_int10",
        },
        npc_dota_hero_warlock_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_warlock/int/int-talant-11.png", name = "npc_dota_hero_warlock_int11",
        },
        npc_dota_hero_warlock_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_warlock/int/int-talant-12.png", name = "npc_dota_hero_warlock_int12",
        },
        special_bonus_unique_npc_dota_hero_warlock_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_warlock/int/int-talant-10.png", name = "npc_dota_hero_warlock_int13",
        },
    },
    npc_dota_hero_windrunner = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_windrunner_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_windrunner/str/str-talant-6.png", name = "npc_dota_hero_windrunner_str6",
        },
        npc_dota_hero_windrunner_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_windrunner/str/str-talant-7.png", name = "npc_dota_hero_windrunner_str7",
        },
        npc_dota_hero_windrunner_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_windrunner/str/str-talant-8.png", name = "npc_dota_hero_windrunner_str8",
        },
        npc_dota_hero_windrunner_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_windrunner/str/str-talant-9.png", name = "npc_dota_hero_windrunner_str9",
        },
        npc_dota_hero_windrunner_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_windrunner/str/str-talant-10.png", name = "npc_dota_hero_windrunner_str10",
        },
        npc_dota_hero_windrunner_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_windrunner/str/str-talant-11.png", name = "npc_dota_hero_windrunner_str11",
        },
        npc_dota_hero_windrunner_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_windrunner/str/str-talant-12.png", name = "npc_dota_hero_windrunner_str12",
        },
        special_bonus_unique_npc_dota_hero_windrunner_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_windrunner/str/str-talant-9.png", name = "npc_dota_hero_windrunner_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_windrunner_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-6.png", name = "npc_dota_hero_windrunner_agi6",
        },
        npc_dota_hero_windrunner_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-7.png", name = "npc_dota_hero_windrunner_agi7",
        },
        npc_dota_hero_windrunner_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-8.png", name = "npc_dota_hero_windrunner_agi8",
        },
        npc_dota_hero_windrunner_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-9.png", name = "npc_dota_hero_windrunner_agi9",
        },
        npc_dota_hero_windrunner_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-10.png", name = "npc_dota_hero_windrunner_agi10",
        },
        npc_dota_hero_windrunner_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-11.png", name = "npc_dota_hero_windrunner_agi11",
        },
        npc_dota_hero_windrunner_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-12.png", name = "npc_dota_hero_windrunner_agi12",
        },
        special_bonus_unique_npc_dota_hero_windrunner_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_windrunner/agi/agi-talant-12.png", name = "npc_dota_hero_windrunner_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_windrunner_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_windrunner/int/int-talant-6.png", name = "npc_dota_hero_windrunner_int6",
        },
        npc_dota_hero_windrunner_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_windrunner/int/int-talant-7.png", name = "npc_dota_hero_windrunner_int7",
        },
        npc_dota_hero_windrunner_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_windrunner/int/int-talant-8.png", name = "npc_dota_hero_windrunner_int8",
        },
        npc_dota_hero_windrunner_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_windrunner/int/int-talant-9.png", name = "npc_dota_hero_windrunner_int9",
        },
        npc_dota_hero_windrunner_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_windrunner/int/int-talant-10.png", name = "npc_dota_hero_windrunner_int10",
        },
        npc_dota_hero_windrunner_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_windrunner/int/int-talant-11.png", name = "npc_dota_hero_windrunner_int11",
        },
        npc_dota_hero_windrunner_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_windrunner/int/int-talant-12.png", name = "npc_dota_hero_windrunner_int12",
        },
        special_bonus_unique_npc_dota_hero_windrunner_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_windrunner/int/int-talant-12.png", name = "npc_dota_hero_windrunner_int13",
        },
    },
    npc_dota_hero_wisp = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_wisp_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_wisp/str/str-talant-6.png", name = "npc_dota_hero_wisp_str6",
        },
        npc_dota_hero_wisp_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_wisp/str/str-talant-7.png", name = "npc_dota_hero_wisp_str7",
        },
        npc_dota_hero_wisp_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_wisp/str/str-talant-8.png", name = "npc_dota_hero_wisp_str8",
        },
        npc_dota_hero_wisp_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_wisp/str/str-talant-9.png", name = "npc_dota_hero_wisp_str9",
        },
        npc_dota_hero_wisp_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_wisp/str/str-talant-10.png", name = "npc_dota_hero_wisp_str10",
        },
        npc_dota_hero_wisp_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_wisp/str/str-talant-11.png", name = "npc_dota_hero_wisp_str11",
        },
        npc_dota_hero_wisp_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_wisp/str/life_stealer_infest.png", name = "npc_dota_hero_wisp_str12",
        },
        special_bonus_unique_npc_dota_hero_wisp_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_wisp/str/life_stealer_infest.png", name = "lifestealer_infest_hp",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_wisp_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_wisp/agi/agi-talant-6.png", name = "npc_dota_hero_wisp_agi6",
        },
        npc_dota_hero_wisp_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_wisp/agi/agi-talant-7.png", name = "npc_dota_hero_wisp_agi7",
        },
        npc_dota_hero_wisp_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_wisp/agi/agi-talant-8.png", name = "npc_dota_hero_wisp_agi8",
        },
        npc_dota_hero_wisp_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_wisp/agi/agi-talant-9.png", name = "npc_dota_hero_wisp_agi9",
        },
        npc_dota_hero_wisp_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_wisp/agi/agi-talant-10.png", name = "npc_dota_hero_wisp_agi10",
        },
        npc_dota_hero_wisp_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_wisp/agi/agi-talant-11.png", name = "npc_dota_hero_wisp_agi11",
        },
        npc_dota_hero_wisp_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_wisp/agi/agi-talant-12.png", name = "npc_dota_hero_wisp_agi12",
        },
        special_bonus_unique_npc_dota_hero_wisp_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_wisp/agi/agi-talant-12.png", name = "npc_dota_hero_wisp_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_wisp_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_wisp/int/int-talant-6.png", name = "npc_dota_hero_wisp_int6",
        },
        npc_dota_hero_wisp_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_wisp/int/int-talant-7.png", name = "npc_dota_hero_wisp_int7",
        },
        npc_dota_hero_wisp_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_wisp/int/int-talant-8.png", name = "npc_dota_hero_wisp_int8",
        },
        npc_dota_hero_wisp_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_wisp/int/int-talant-9.png", name = "npc_dota_hero_wisp_int9",
        },
        npc_dota_hero_wisp_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_wisp/int/int-talant-10.png", name = "npc_dota_hero_wisp_int10",
        },
        npc_dota_hero_wisp_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_wisp/int/int-talant-11.png", name = "npc_dota_hero_wisp_int11",
        },
        npc_dota_hero_wisp_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_wisp/int/int-talant-12.png", name = "npc_dota_hero_wisp_int12",
        },
        special_bonus_unique_npc_dota_hero_wisp_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_wisp/int/int-talant-12.png", name = "npc_dota_hero_wisp_int13",
        },
    },
    npc_dota_hero_zuus = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_zuus_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_zuus/str/3.png", name = "npc_dota_hero_zuus_str6",
        },
        npc_dota_hero_zuus_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_zuus/str/2.png", name = "npc_dota_hero_zuus_str7",
        },
        npc_dota_hero_zuus_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_zuus/str/4.png", name = "npc_dota_hero_zuus_str8",
        },
        npc_dota_hero_zuus_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_zuus/str/3.png", name = "npc_dota_hero_zuus_str9",
        },
        npc_dota_hero_zuus_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_zuus/str/4.png", name = "npc_dota_hero_zuus_str10",
        },
        npc_dota_hero_zuus_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_zuus/str/4.png", name = "npc_dota_hero_zuus_str11",
        },
        npc_dota_hero_zuus_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_zuus/str/3.png", name = "npc_dota_hero_zuus_str12",
        },
        npc_dota_hero_zuus_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_zuus/str/4.png", name = "npc_dota_hero_zuus_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_zuus_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_zuus/agi/1.png", name = "npc_dota_hero_zuus_agi6",
        },
        npc_dota_hero_zuus_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_zuus/agi/2.png", name = "npc_dota_hero_zuus_agi7",
        },
        npc_dota_hero_zuus_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_zuus/agi/6.png", name = "npc_dota_hero_zuus_agi8",
        },
        npc_dota_hero_zuus_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_zuus/agi/1.png", name = "npc_dota_hero_zuus_agi9",
        },
        npc_dota_hero_zuus_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_zuus/agi/4.png", name = "npc_dota_hero_zuus_agi10",
        },
        npc_dota_hero_zuus_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_zuus/agi/2.png", name = "npc_dota_hero_zuus_agi11",
        },
        npc_dota_hero_zuus_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_zuus/agi/5.png", name = "npc_dota_hero_zuus_agi12",
        },
        npc_dota_hero_zuus_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_zuus/agi/5.png", name = "npc_dota_hero_zuus_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_zuus_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_zuus/int/1.png", name = "npc_dota_hero_zuus_int6",
        },
        npc_dota_hero_zuus_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_zuus/int/2.png", name = "npc_dota_hero_zuus_int7",
        },
        npc_dota_hero_zuus_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_zuus/int/4.png", name = "npc_dota_hero_zuus_int8",
        },
        npc_dota_hero_zuus_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_zuus/int/1.png", name = "npc_dota_hero_zuus_int9",
        },
        npc_dota_hero_zuus_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_zuus/int/2.png", name = "npc_dota_hero_zuus_int10",
        },
        npc_dota_hero_zuus_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_zuus/int/3.png", name = "npc_dota_hero_zuus_int11",
        },
        npc_dota_hero_zuus_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_zuus/int/3.png", name = "npc_dota_hero_zuus_int12",
        },
        npc_dota_hero_zuus_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_zuus/int/5.png", name = "npc_dota_hero_zuus_int13",
        },
    },
    npc_dota_hero_medusa = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_medusa_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_medusa/str/str-talant-13.png", name = "npc_dota_hero_medusa_str6",
        },
        npc_dota_hero_medusa_str10 = {
            place = {"str 7"}, url = "/npc_dota_hero_medusa/str/str-talant-10.png", name = "npc_dota_hero_medusa_str10",
        },
        npc_dota_hero_medusa_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_medusa/str/str-talant-9.png", name = "npc_dota_hero_medusa_str8",
        },
        npc_dota_hero_medusa_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_medusa/str/str-talant-9.png", name = "npc_dota_hero_medusa_str9",
        },
        npc_dota_hero_medusa_str7 = {
            place = {"str 10"}, url = "/npc_dota_hero_medusa/str/str-talant-9.png", name = "npc_dota_hero_medusa_str7",
        },
        npc_dota_hero_medusa_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_medusa/str/str-talant-11.png", name = "npc_dota_hero_medusa_str11",
        },
        npc_dota_hero_medusa_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_medusa/str/str-talant-12.png", name = "npc_dota_hero_medusa_str12",
        },
        special_bonus_unique_npc_dota_hero_medusa_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_medusa/str/str-talant-12.png", name = "npc_dota_hero_medusa_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_medusa_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_medusa/agi/agi-talant-12.png", name = "npc_dota_hero_medusa_agi12",
        },
        npc_dota_hero_medusa_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_medusa/agi/agi-talant-13.png", name = "npc_dota_hero_medusa_agi7",
        },
        npc_dota_hero_medusa_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_medusa/agi/agi-talant-8.png", name = "npc_dota_hero_medusa_agi8",
        },
        npc_dota_hero_medusa_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_medusa/agi/agi-talant-9.png", name = "npc_dota_hero_medusa_agi9",
        },
        npc_dota_hero_medusa_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_medusa/agi/agi-talant-10.png", name = "npc_dota_hero_medusa_agi10",
        },
        npc_dota_hero_medusa_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_medusa/agi/agi-talant-11.png", name = "npc_dota_hero_medusa_agi11",
        },
        npc_dota_hero_medusa_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_medusa/agi/agi-talant-6.png", name = "npc_dota_hero_medusa_agi6",
        },
        special_bonus_unique_npc_dota_hero_medusa_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_medusa/agi/agi-talant-6.png", name = "npc_dota_hero_medusa_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_medusa_int7 = {
            place = {"int 6"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int7",
        },
        npc_dota_hero_medusa_int9 = {
            place = {"int 7"}, url = "/npc_dota_hero_medusa/int/int-talant-9.png", name = "npc_dota_hero_medusa_int9",
        },
        npc_dota_hero_medusa_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int8",
        },
        npc_dota_hero_medusa_int6 = {
            place = {"int 9"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int6",
        },
        npc_dota_hero_medusa_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_medusa/int/int-talant-10.png", name = "npc_dota_hero_medusa_int10",
        },
        npc_dota_hero_medusa_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int11",
        },
        npc_dota_hero_medusa_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int12",
        },
        special_bonus_unique_npc_dota_hero_medusa_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_medusa/int/int-talant-6.png", name = "npc_dota_hero_medusa_int13",
        },
    },
    npc_dota_hero_legion_commander = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_legion_commander_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_legion_commander/str/str-talant-6.png", name = "npc_dota_hero_legion_commander_str6",
        },
        npc_dota_hero_legion_commander_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_legion_commander/str/str-talant-7.png", name = "npc_dota_hero_legion_commander_str7",
        },
        npc_dota_hero_legion_commander_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_legion_commander/str/str-talant-8.png", name = "npc_dota_hero_legion_commander_str8",
        },
        npc_dota_hero_legion_commander_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_legion_commander/str/str-talant-9.png", name = "npc_dota_hero_legion_commander_str9",
        },
        npc_dota_hero_legion_commander_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_legion_commander/str/str-talant-10.png", name = "npc_dota_hero_legion_commander_str10",
        },
        npc_dota_hero_legion_commander_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_legion_commander/str/str-talant-11.png", name = "npc_dota_hero_legion_commander_str11",
        },
        npc_dota_hero_legion_commander_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_legion_commander/str/str-talant-12.png", name = "npc_dota_hero_legion_commander_str12",
        },
        special_bonus_unique_npc_dota_hero_legion_commander_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_legion_commander/str/str-talant-12.png", name = "npc_dota_hero_legion_commander_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_legion_commander_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-6.png", name = "npc_dota_hero_legion_commander_agi6",
        },
        npc_dota_hero_legion_commander_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-7.png", name = "npc_dota_hero_legion_commander_agi7",
        },
        npc_dota_hero_legion_commander_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-8.png", name = "npc_dota_hero_legion_commander_agi8",
        },
        npc_dota_hero_legion_commander_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-9.png", name = "npc_dota_hero_legion_commander_agi9",
        },
        npc_dota_hero_legion_commander_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-10.png", name = "npc_dota_hero_legion_commander_agi10",
        },
        npc_dota_hero_legion_commander_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-11.png", name = "npc_dota_hero_legion_commander_agi11",
        },
        npc_dota_hero_legion_commander_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-12.png", name = "npc_dota_hero_legion_commander_agi12",
        },
        special_bonus_unique_npc_dota_hero_legion_commander_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_legion_commander/agi/agi-talant-12.png", name = "npc_dota_hero_legion_commander_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_legion_commander_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_legion_commander/int/int-talant-6.png", name = "npc_dota_hero_legion_commander_int6",
        },
        npc_dota_hero_legion_commander_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_legion_commander/int/int-talant-7.png", name = "npc_dota_hero_legion_commander_int7",
        },
        npc_dota_hero_legion_commander_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_legion_commander/int/int-talant-8.png", name = "npc_dota_hero_legion_commander_int8",
        },
        npc_dota_hero_legion_commander_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_legion_commander/int/int-talant-9.png", name = "npc_dota_hero_legion_commander_int9",
        },
        npc_dota_hero_legion_commander_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_legion_commander/int/int-talant-10.png", name = "npc_dota_hero_legion_commander_int10",
        },
        npc_dota_hero_legion_commander_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_legion_commander/int/int-talant-11.png", name = "npc_dota_hero_legion_commander_int11",
        },
        npc_dota_hero_legion_commander_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_legion_commander/int/int-talant-8.png", name = "npc_dota_hero_legion_commander_int12",
        },
        special_bonus_unique_npc_dota_hero_legion_commander_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_legion_commander/int/int-talant-10.png", name = "npc_dota_hero_legion_commander_int13",
        },
        
    },
    npc_dota_hero_skywrath_mage = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_skywrath_mage_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-6.png", name = "npc_dota_hero_skywrath_mage_str6",
        },
        npc_dota_hero_skywrath_mage_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-7.png", name = "npc_dota_hero_skywrath_mage_str7",
        },
        npc_dota_hero_skywrath_mage_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-8.png", name = "npc_dota_hero_skywrath_mage_str8",
        },
        npc_dota_hero_skywrath_mage_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-9.png", name = "npc_dota_hero_skywrath_mage_str9",
        },
        npc_dota_hero_skywrath_mage_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-10.png", name = "npc_dota_hero_skywrath_mage_str10",
        },
        npc_dota_hero_skywrath_mage_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-11.png", name = "npc_dota_hero_skywrath_mage_str11",
        },
        npc_dota_hero_skywrath_mage_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-12.png", name = "npc_dota_hero_skywrath_mage_str12",
        },
        special_bonus_unique_npc_dota_hero_skywrath_mage_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_skywrath_mage/str/str-talant-12.png", name = "npc_dota_hero_skywrath_mage_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_skywrath_mage_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-6.png", name = "npc_dota_hero_skywrath_mage_agi6",
        },
        npc_dota_hero_skywrath_mage_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-7.png", name = "npc_dota_hero_skywrath_mage_agi7",
        },
        npc_dota_hero_skywrath_mage_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-8.png", name = "npc_dota_hero_skywrath_mage_agi8",
        },
        npc_dota_hero_skywrath_mage_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-9.png", name = "npc_dota_hero_skywrath_mage_agi9",
        },
        npc_dota_hero_skywrath_mage_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-10.png", name = "npc_dota_hero_skywrath_mage_agi10",
        },
        npc_dota_hero_skywrath_mage_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-11.png", name = "npc_dota_hero_skywrath_mage_agi11",
        },
        npc_dota_hero_skywrath_mage_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-12.png", name = "npc_dota_hero_skywrath_mage_agi12",
        },
        special_bonus_unique_npc_dota_hero_skywrath_mage_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_skywrath_mage/agi/agi-talant-12.png", name = "npc_dota_hero_skywrath_mage_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_skywrath_mage_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-6.png", name = "npc_dota_hero_skywrath_mage_int6",
        },
        npc_dota_hero_skywrath_mage_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-7.png", name = "npc_dota_hero_skywrath_mage_int7",
        },
        npc_dota_hero_skywrath_mage_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-8.png", name = "npc_dota_hero_skywrath_mage_int8",
        },
        npc_dota_hero_skywrath_mage_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-9.png", name = "npc_dota_hero_skywrath_mage_int9",
        },
        npc_dota_hero_skywrath_mage_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-10.png", name = "npc_dota_hero_skywrath_mage_int10",
        },
        npc_dota_hero_skywrath_mage_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-11.png", name = "npc_dota_hero_skywrath_mage_int11",
        },
        npc_dota_hero_skywrath_mage_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-12.png", name = "npc_dota_hero_skywrath_mage_int12",
        },
        special_bonus_unique_npc_dota_hero_skywrath_mage_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_skywrath_mage/int/int-talant-12.png", name = "npc_dota_hero_skywrath_mage_int13",
        },
    },
    npc_dota_hero_doom_bringer = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_doom_bringer_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_doom_bringer/str/4.png", name = "npc_dota_hero_doom_bringer_str6",
        },
        npc_dota_hero_doom_bringer_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_doom_bringer/str/2.png", name = "npc_dota_hero_doom_bringer_str7",
        },
        npc_dota_hero_doom_bringer_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_doom_bringer/str/3.png", name = "npc_dota_hero_doom_bringer_str8",
        },
        npc_dota_hero_doom_bringer_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_doom_bringer/str/4.png", name = "npc_dota_hero_doom_bringer_str9",
        },
        npc_dota_hero_doom_bringer_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_doom_bringer/str/1.png", name = "npc_dota_hero_doom_bringer_str10",
        },
        npc_dota_hero_doom_bringer_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_doom_bringer/str/3.png", name = "npc_dota_hero_doom_bringer_str11",
        },
        npc_dota_hero_doom_bringer_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_doom_bringer/str/1.png", name = "npc_dota_hero_doom_bringer_str12",
        },
        npc_dota_hero_doom_bringer_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_doom_bringer/str/1.png", name = "npc_dota_hero_doom_bringer_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_doom_bringer_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_doom_bringer/agi/1.png", name = "npc_dota_hero_doom_bringer_agi6",
        },
        npc_dota_hero_doom_bringer_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_doom_bringer/agi/3.png", name = "npc_dota_hero_doom_bringer_agi7",
        },
        npc_dota_hero_doom_bringer_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_doom_bringer/agi/1.png", name = "npc_dota_hero_doom_bringer_agi8",
        },
        npc_dota_hero_doom_bringer_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_doom_bringer/agi/5.png", name = "npc_dota_hero_doom_bringer_agi9",
        },
        npc_dota_hero_doom_bringer_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_doom_bringer/agi/3.png", name = "npc_dota_hero_doom_bringer_agi10",
        },
        npc_dota_hero_doom_bringer_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_doom_bringer/agi/1.png", name = "npc_dota_hero_doom_bringer_agi11",
        },
        npc_dota_hero_doom_bringer_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_doom_bringer/agi/1.png", name = "npc_dota_hero_doom_bringer_agi12",
        },
        npc_dota_hero_doom_bringer_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_doom_bringer/agi/1.png", name = "npc_dota_hero_doom_bringer_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_doom_bringer_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_doom_bringer/int/1.png", name = "npc_dota_hero_doom_bringer_int6",
        },
        npc_dota_hero_doom_bringer_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_doom_bringer/int/2.png", name = "npc_dota_hero_doom_bringer_int7",
        },
        npc_dota_hero_doom_bringer_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_doom_bringer/int/4.png", name = "npc_dota_hero_doom_bringer_int8",
        },
        npc_dota_hero_doom_bringer_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_doom_bringer/int/4.png", name = "npc_dota_hero_doom_bringer_int9",
        },
        npc_dota_hero_doom_bringer_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_doom_bringer/int/3.png", name = "npc_dota_hero_doom_bringer_int10",
        },
        npc_dota_hero_doom_bringer_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_doom_bringer/int/3.png", name = "npc_dota_hero_doom_bringer_int11",
        },
        npc_dota_hero_doom_bringer_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_doom_bringer/int/5.png", name = "npc_dota_hero_doom_bringer_int12",
        },
        npc_dota_hero_doom_bringer_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_doom_bringer/int/5.png", name = "npc_dota_hero_doom_bringer_int13",
        },
    },
    npc_dota_hero_pudge = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_pudge_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_pudge/str/str-talant-6.png", name = "npc_dota_hero_pudge_str6",
        },
        npc_dota_hero_pudge_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_pudge/str/str-talant-7.png", name = "npc_dota_hero_pudge_str7",
        },
        npc_dota_hero_pudge_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_pudge/str/str-talant-8.png", name = "npc_dota_hero_pudge_str8",
        },
        npc_dota_hero_pudge_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_pudge/str/str-talant-9.png", name = "npc_dota_hero_pudge_str9",
        },
        npc_dota_hero_pudge_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_pudge/str/str-talant-10.png", name = "npc_dota_hero_pudge_str10",
        },
        npc_dota_hero_pudge_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_pudge/str/str-talant-11.png", name = "npc_dota_hero_pudge_str11",
        },
        npc_dota_hero_pudge_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_pudge/str/str-talant-12.png", name = "npc_dota_hero_pudge_str12",
        },
        special_bonus_unique_npc_dota_hero_pudge_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_pudge/str/str-talant-12.png", name = "npc_dota_hero_pudge_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_pudge_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_pudge/agi/agi-talant-6.png", name = "npc_dota_hero_pudge_agi6",
        },
        npc_dota_hero_pudge_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_pudge/agi/agi-talant-7.png", name = "npc_dota_hero_pudge_agi7",
        },
        npc_dota_hero_pudge_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_pudge/agi/agi-talant-8.png", name = "npc_dota_hero_pudge_agi8",
        },
        npc_dota_hero_pudge_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_pudge/agi/agi-talant-9.png", name = "npc_dota_hero_pudge_agi9",
        },
        npc_dota_hero_pudge_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_pudge/agi/agi-talant-10.png", name = "npc_dota_hero_pudge_agi10",
        },
        npc_dota_hero_pudge_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_pudge/agi/agi-talant-11.png", name = "npc_dota_hero_pudge_agi11",
        },
        npc_dota_hero_pudge_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_pudge/agi/agi-talant-12.png", name = "npc_dota_hero_pudge_agi12",
        },
        special_bonus_unique_npc_dota_hero_pudge_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_pudge/agi/agi-talant-12.png", name = "npc_dota_hero_pudge_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_pudge_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_pudge/int/int-talant-6.png", name = "npc_dota_hero_pudge_int6",
        },
        npc_dota_hero_pudge_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_pudge/int/int-talant-7.png", name = "npc_dota_hero_pudge_int7",
        },
        npc_dota_hero_pudge_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_pudge/int/int-talant-8.png", name = "npc_dota_hero_pudge_int8",
        },
        npc_dota_hero_pudge_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_pudge/int/int-talant-9.png", name = "npc_dota_hero_pudge_int9",
        },
        npc_dota_hero_pudge_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_pudge/int/int-talant-10.png", name = "npc_dota_hero_pudge_int10",
        },
        npc_dota_hero_pudge_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_pudge/int/int-talant-11.png", name = "npc_dota_hero_pudge_int11",
        },
        npc_dota_hero_pudge_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_pudge/int/int-talant-12.png", name = "npc_dota_hero_pudge_int12",
        },
        special_bonus_unique_npc_dota_hero_pudge_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_pudge/int/int-talant-12.png", name = "npc_dota_hero_pudge_int13",
        },
    },
    npc_dota_hero_techies = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_techies_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_techies/str/str-talant-6.png", name = "npc_dota_hero_techies_str6",
        },
        npc_dota_hero_techies_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_techies/str/str-talant-7.png", name = "npc_dota_hero_techies_str7",
        },
        npc_dota_hero_techies_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_techies/str/str-talant-8.png", name = "npc_dota_hero_techies_str8",
        },
        npc_dota_hero_techies_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_techies/str/str-talant-9.png", name = "npc_dota_hero_techies_str9",
        },
        npc_dota_hero_techies_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_techies/str/str-talant-10.png", name = "npc_dota_hero_techies_str10",
        },
        npc_dota_hero_techies_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_techies/str/str-talant-11.png", name = "npc_dota_hero_techies_str11",
        },
        npc_dota_hero_techies_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_techies/str/str-talant-12.png", name = "npc_dota_hero_techies_str12",
        },
        special_bonus_unique_npc_dota_hero_techies_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_techies/str/str-talant-12.png", name = "npc_dota_hero_techies_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_techies_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_techies/agi/agi-talant-6.png", name = "npc_dota_hero_techies_agi6",
        },
        npc_dota_hero_techies_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_techies/agi/agi-talant-7.png", name = "npc_dota_hero_techies_agi7",
        },
        npc_dota_hero_techies_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_techies/agi/agi-talant-8.png", name = "npc_dota_hero_techies_agi8",
        },
        npc_dota_hero_techies_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_techies/agi/agi-talant-9.png", name = "npc_dota_hero_techies_agi9",
        },
        npc_dota_hero_techies_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_techies/agi/agi-talant-10.png", name = "npc_dota_hero_techies_agi10",
        },
        npc_dota_hero_techies_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_techies/agi/agi-talant-11.png", name = "npc_dota_hero_techies_agi11",
        },
        npc_dota_hero_techies_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_techies/agi/agi-talant-12.png", name = "npc_dota_hero_techies_agi12",
        },
        special_bonus_unique_npc_dota_hero_techies_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_techies/agi/agi-talant-12.png", name = "npc_dota_hero_techies_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_techies_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_techies/int/int-talant-6.png", name = "npc_dota_hero_techies_int6",
        },
        npc_dota_hero_techies_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_techies/int/int-talant-7.png", name = "npc_dota_hero_techies_int7",
        },
        npc_dota_hero_techies_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_techies/int/int-talant-8.png", name = "npc_dota_hero_techies_int8",
        },
        npc_dota_hero_techies_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_techies/int/int-talant-9.png", name = "npc_dota_hero_techies_int9",
        },
        npc_dota_hero_techies_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_techies/int/int-talant-10.png", name = "npc_dota_hero_techies_int10",
        },
        npc_dota_hero_techies_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_techies/int/int-talant-11.png", name = "npc_dota_hero_techies_int11",
        },
        npc_dota_hero_techies_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_techies/int/int-talant-12.png", name = "npc_dota_hero_techies_int12",
        },
        special_bonus_unique_npc_dota_hero_techies_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_techies/int/int-talant-6.png", name = "npc_dota_hero_techies_int13",
        },
    },
    npc_dota_hero_marci = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_marci_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str6.png", name = "npc_dota_hero_marci_str6",
        },
        npc_dota_hero_marci_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str7.png", name = "npc_dota_hero_marci_str7",
        },
        npc_dota_hero_marci_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str8.png", name = "npc_dota_hero_marci_str8",
        },
        npc_dota_hero_marci_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str9.png", name = "npc_dota_hero_marci_str9",
        },
        npc_dota_hero_marci_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str10.png", name = "npc_dota_hero_marci_str10",
        },
        npc_dota_hero_marci_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str11.png", name = "npc_dota_hero_marci_str11",
        },
        npc_dota_hero_marci_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str_last.png", name = "npc_dota_hero_marci_str12",
        },
        special_bonus_unique_npc_dota_hero_marci_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_marci/str/npc_dota_hero_marci_str_last.png", name = "npc_dota_hero_marci_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_marci_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi6.png", name = "npc_dota_hero_marci_agi6",
        },
        npc_dota_hero_marci_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi7.png", name = "npc_dota_hero_marci_agi7",
        },
        npc_dota_hero_marci_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi8.png", name = "npc_dota_hero_marci_agi8",
        },
        npc_dota_hero_marci_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi9.png", name = "npc_dota_hero_marci_agi9",
        },
        npc_dota_hero_marci_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi10.png", name = "npc_dota_hero_marci_agi10",
        },
        npc_dota_hero_marci_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi11.png", name = "npc_dota_hero_marci_agi11",
        },
        npc_dota_hero_marci_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi_last.png", name = "npc_dota_hero_marci_agi12",
        },
        special_bonus_unique_npc_dota_hero_marci_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_marci/agi/npc_dota_hero_marci_agi_last.png", name = "npc_dota_hero_marci_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_marci_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int6.png", name = "npc_dota_hero_marci_int6",
        },
        npc_dota_hero_marci_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int7.png", name = "npc_dota_hero_marci_int7",
        },
        npc_dota_hero_marci_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int8.png", name = "npc_dota_hero_marci_int8",
        },
        npc_dota_hero_marci_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int9.png", name = "npc_dota_hero_marci_int9",
        },
        npc_dota_hero_marci_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int10.png", name = "npc_dota_hero_marci_int10",
        },
        npc_dota_hero_marci_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int11.png", name = "npc_dota_hero_marci_int11",
        },
        npc_dota_hero_marci_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int_last.png", name = "npc_dota_hero_marci_int12",
        },
        special_bonus_unique_npc_dota_hero_marci_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_marci/int/npc_dota_hero_marci_int_last.png", name = "npc_dota_hero_marci_int13",
        },
    },
    npc_dota_hero_crystal_maiden = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_crystal_maiden_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_crystal/str/str-talant-6.png", name = "npc_dota_hero_crystal_maiden_str6",
        },
        npc_dota_hero_crystal_maiden_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_crystal/str/str-talant-7.png", name = "npc_dota_hero_crystal_maiden_str7",
        },
        npc_dota_hero_crystal_maiden_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_crystal/str/str-talant-8.png", name = "npc_dota_hero_crystal_maiden_str8",
        },
        npc_dota_hero_crystal_maiden_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_crystal/str/str-talant-9.png", name = "npc_dota_hero_crystal_maiden_str9",
        },
        npc_dota_hero_crystal_maiden_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_crystal/str/str-talant-10.png", name = "npc_dota_hero_crystal_maiden_str10",
        },
        npc_dota_hero_crystal_maiden_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_crystal/str/str-talant-11.png", name = "npc_dota_hero_crystal_maiden_str11",
        },
        npc_dota_hero_crystal_maiden_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_crystal/str/str-talant-12.png", name = "npc_dota_hero_crystal_maiden_str12",
        },
        special_bonus_unique_npc_dota_hero_crystal_maiden_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_crystal/str/str-talant-11.png", name = "npc_dota_hero_crystal_maiden_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_crystal_maiden_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_crystal/agi/agi-talant-6.png", name = "npc_dota_hero_crystal_maiden_agi6",
        },
        npc_dota_hero_crystal_maiden_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_crystal/agi/agi-talant-7.png", name = "npc_dota_hero_crystal_maiden_agi7",
        },
        npc_dota_hero_crystal_maiden_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_crystal/agi/agi-talant-8.png", name = "npc_dota_hero_crystal_maiden_agi8",
        },
        npc_dota_hero_crystal_maiden_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_crystal/agi/agi-talant-9.png", name = "npc_dota_hero_crystal_maiden_agi9",
        },
        npc_dota_hero_crystal_maiden_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_crystal/agi/agi-talant-10.png", name = "npc_dota_hero_crystal_maiden_agi10",
        },
        npc_dota_hero_crystal_maiden_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_crystal/agi/agi-talant-11.png", name = "npc_dota_hero_crystal_maiden_agi11",
        },
        npc_dota_hero_crystal_maiden_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_crystal/agi/agi-talant-12.png", name = "npc_dota_hero_crystal_maiden_agi12",
        },
        special_bonus_unique_npc_dota_hero_crystal_maiden_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_crystal/agi/agi-talant-12.png", name = "npc_dota_hero_crystal_maiden_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_crystal_maiden_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_crystal/int/int-talant-6.png", name = "npc_dota_hero_crystal_maiden_int6",
        },
        npc_dota_hero_crystal_maiden_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_crystal/int/int-talant-7.png", name = "npc_dota_hero_crystal_maiden_int7",
        },
        npc_dota_hero_crystal_maiden_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_crystal/int/int-talant-8.png", name = "npc_dota_hero_crystal_maiden_int8",
        },
        npc_dota_hero_crystal_maiden_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_crystal/int/int-talant-9.png", name = "npc_dota_hero_crystal_maiden_int9",
        },
        npc_dota_hero_crystal_maiden_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_crystal/int/int-talant-10.png", name = "npc_dota_hero_crystal_maiden_int10",
        },
        npc_dota_hero_crystal_maiden_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_crystal/int/int-talant-11.png", name = "npc_dota_hero_crystal_maiden_int11",
        },
        npc_dota_hero_crystal_maiden_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_crystal/int/int-talant-12.png", name = "npc_dota_hero_crystal_maiden_int12",
        },
        special_bonus_unique_npc_dota_hero_crystal_maiden_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_crystal/int/int-talant-12.png", name = "npc_dota_hero_crystal_maiden_int13",
        },
    },
    npc_dota_hero_alchemist = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_alchemist_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_alchemist/str/str-talant-6.png", name = "npc_dota_hero_alchemist_str6",
        },
        npc_dota_hero_alchemist_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_alchemist/str/str-talant-7.png", name = "npc_dota_hero_alchemist_str7",
        },
        npc_dota_hero_alchemist_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_alchemist/str/str-talant-8.png", name = "npc_dota_hero_alchemist_str8",
        },
        npc_dota_hero_alchemist_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_alchemist/str/str-talant-9.png", name = "npc_dota_hero_alchemist_str9",
        },
        npc_dota_hero_alchemist_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_alchemist/str/str-talant-10.png", name = "npc_dota_hero_alchemist_str10",
        },
        npc_dota_hero_alchemist_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_alchemist/str/str-talant-11.png", name = "npc_dota_hero_alchemist_str11",
        },
        npc_dota_hero_alchemist_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_alchemist/str/str-talant-12.png", name = "npc_dota_hero_alchemist_str12",
        },
        special_bonus_unique_npc_dota_hero_alchemist_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_alchemist/str/str-talant-7.png", name = "npc_dota_hero_alchemist_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_alchemist_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-6.png", name = "npc_dota_hero_alchemist_agi6",
        },
        npc_dota_hero_alchemist_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-7.png", name = "npc_dota_hero_alchemist_agi7",
        },
        npc_dota_hero_alchemist_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-8.png", name = "npc_dota_hero_alchemist_agi8",
        },
        npc_dota_hero_alchemist_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-9.png", name = "npc_dota_hero_alchemist_agi9",
        },
        npc_dota_hero_alchemist_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-10.png", name = "npc_dota_hero_alchemist_agi10",
        },
        npc_dota_hero_alchemist_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-11.png", name = "npc_dota_hero_alchemist_agi11",
        },
        npc_dota_hero_alchemist_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-12.png", name = "npc_dota_hero_alchemist_agi12",
        },
        special_bonus_unique_npc_dota_hero_alchemist_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_alchemist/agi/agi-talant-12.png", name = "npc_dota_hero_alchemist_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_alchemist_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_alchemist/int/int-talant-6.png", name = "npc_dota_hero_alchemist_int6",
        },
        npc_dota_hero_alchemist_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_alchemist/int/int-talant-7.png", name = "npc_dota_hero_alchemist_int7",
        },
        npc_dota_hero_alchemist_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_alchemist/int/int-talant-8.png", name = "npc_dota_hero_alchemist_int8",
        },
        npc_dota_hero_alchemist_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_alchemist/int/int-talant-9.png", name = "npc_dota_hero_alchemist_int9",
        },
        npc_dota_hero_alchemist_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_alchemist/int/int-talant-10.png", name = "npc_dota_hero_alchemist_int10",
        },
        npc_dota_hero_alchemist_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_alchemist/int/int-talant-11.png", name = "npc_dota_hero_alchemist_int11",
        },
        npc_dota_hero_alchemist_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_alchemist/int/int-talant-12.png", name = "npc_dota_hero_alchemist_int12",
        },
        special_bonus_unique_npc_dota_hero_alchemist_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_alchemist/int/alchemist_corrosive_weaponry.png", name = "npc_dota_hero_alchemist_int13",
        },
    },
    npc_dota_hero_troll_warlord = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_troll_warlord_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_troll_warlord/str/4.png", name = "npc_dota_hero_troll_warlord_str6",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_troll_warlord/str/1.png", name = "npc_dota_hero_troll_warlord_str7",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_troll_warlord/str/3.png", name = "npc_dota_hero_troll_warlord_str8",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_troll_warlord/str/4.png", name = "npc_dota_hero_troll_warlord_str9",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_troll_warlord/str/4.png", name = "npc_dota_hero_troll_warlord_str10",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_troll_warlord/str/3.png", name = "npc_dota_hero_troll_warlord_str11",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_troll_warlord/str/1.png", name = "npc_dota_hero_troll_warlord_str12",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_troll_warlord/str/1.png", name = "npc_dota_hero_troll_warlord_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_troll_warlord_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_troll_warlord/agi/4.png", name = "npc_dota_hero_troll_warlord_agi6",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_troll_warlord/agi/4.png", name = "npc_dota_hero_troll_warlord_agi7",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_troll_warlord/agi/2.png", name = "npc_dota_hero_troll_warlord_agi8",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_troll_warlord/agi/1.png", name = "npc_dota_hero_troll_warlord_agi9",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_troll_warlord/agi/1.png", name = "npc_dota_hero_troll_warlord_agi10",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_troll_warlord/agi/3.png", name = "npc_dota_hero_troll_warlord_agi11",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_troll_warlord/agi/4.png", name = "npc_dota_hero_troll_warlord_agi12",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_troll_warlord/agi/4.png", name = "npc_dota_hero_troll_warlord_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_troll_warlord_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_troll_warlord/int/2.png", name = "npc_dota_hero_troll_warlord_int6",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_troll_warlord/int/3.png", name = "npc_dota_hero_troll_warlord_int7",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_troll_warlord/int/4.png", name = "npc_dota_hero_troll_warlord_int8",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_troll_warlord/int/2.png", name = "npc_dota_hero_troll_warlord_int9",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_troll_warlord/int/3.png", name = "npc_dota_hero_troll_warlord_int10",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_troll_warlord/int/4.png", name = "npc_dota_hero_troll_warlord_int11",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_troll_warlord/int/3.png", name = "npc_dota_hero_troll_warlord_int12",
        },
        special_bonus_unique_npc_dota_hero_troll_warlord_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_troll_warlord/int/3.png", name = "npc_dota_hero_troll_warlord_int13",
        },
    },
    npc_dota_hero_gyrocopter = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_gyrocopter_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-6.png", name = "npc_dota_hero_gyrocopter_str6",
        },
        npc_dota_hero_gyrocopter_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-7.png", name = "npc_dota_hero_gyrocopter_str7",
        },
        npc_dota_hero_gyrocopter_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-8.png", name = "npc_dota_hero_gyrocopter_str8",
        },
        npc_dota_hero_gyrocopter_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-9.png", name = "npc_dota_hero_gyrocopter_str9",
        },
        npc_dota_hero_gyrocopter_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-10.png", name = "npc_dota_hero_gyrocopter_str10",
        },
        npc_dota_hero_gyrocopter_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-11.png", name = "npc_dota_hero_gyrocopter_str11",
        },
        npc_dota_hero_gyrocopter_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-12.png", name = "npc_dota_hero_gyrocopter_str12",
        },
        special_bonus_unique_npc_dota_hero_gyrocopter_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_gyrocopter/str/str-talant-12.png", name = "npc_dota_hero_gyrocopter_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_gyrocopter_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-6.png", name = "npc_dota_hero_gyrocopter_agi6",
        },
        npc_dota_hero_gyrocopter_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-7.png", name = "npc_dota_hero_gyrocopter_agi7",
        },
        npc_dota_hero_gyrocopter_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-8.png", name = "npc_dota_hero_gyrocopter_agi8",
        },
        npc_dota_hero_gyrocopter_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-9.png", name = "npc_dota_hero_gyrocopter_agi9",
        },
        npc_dota_hero_gyrocopter_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-10.png", name = "npc_dota_hero_gyrocopter_agi10",
        },
        npc_dota_hero_gyrocopter_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-11.png", name = "npc_dota_hero_gyrocopter_agi11",
        },
        npc_dota_hero_gyrocopter_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-12.png", name = "npc_dota_hero_gyrocopter_agi12",
        },
        special_bonus_unique_npc_dota_hero_gyrocopter_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_gyrocopter/agi/agi-talant-12.png", name = "npc_dota_hero_gyrocopter_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_gyrocopter_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-6.png", name = "npc_dota_hero_gyrocopter_int6",
        },
        npc_dota_hero_gyrocopter_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-7.png", name = "npc_dota_hero_gyrocopter_int7",
        },
        npc_dota_hero_gyrocopter_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-8.png", name = "npc_dota_hero_gyrocopter_int8",
        },
        npc_dota_hero_gyrocopter_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-9.png", name = "npc_dota_hero_gyrocopter_int9",
        },
        npc_dota_hero_gyrocopter_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-10.png", name = "npc_dota_hero_gyrocopter_int10",
        },
        npc_dota_hero_gyrocopter_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-11.png", name = "npc_dota_hero_gyrocopter_int11",
        },
        npc_dota_hero_gyrocopter_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-12.png", name = "npc_dota_hero_gyrocopter_int12",
        },
        special_bonus_unique_npc_dota_hero_gyrocopter_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_gyrocopter/int/int-talant-12.png", name = "npc_dota_hero_gyrocopter_int13",
        },
    },
    npc_dota_hero_ancient_apparition = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_ancient_apparition_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-6.png", name = "npc_dota_hero_ancient_apparition_str6", 
        },
        npc_dota_hero_ancient_apparition_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-7.png", name = "npc_dota_hero_ancient_apparition_str7", 
        },
        npc_dota_hero_ancient_apparition_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-8.png", name = "npc_dota_hero_ancient_apparition_str8", 
        },
        npc_dota_hero_ancient_apparition_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-9.png", name = "npc_dota_hero_ancient_apparition_str9", 
        },
        npc_dota_hero_ancient_apparition_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-10.png", name = "npc_dota_hero_ancient_apparition_str10", 
        },
        npc_dota_hero_ancient_apparition_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-11.png", name = "npc_dota_hero_ancient_apparition_str11", 
        },
        npc_dota_hero_ancient_apparition_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_ancient_apparition/str/str-talant-12.png", name = "npc_dota_hero_ancient_apparition_str12", 
        },
        special_bonus_unique_npc_dota_hero_ancient_apparition_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_ancient_apparition/str/0YeIx11.png", name = "npc_dota_hero_ancient_apparition_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_ancient_apparition_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-6.png", name = "npc_dota_hero_ancient_apparition_agi6", 
        },
        npc_dota_hero_ancient_apparition_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-7.png", name = "npc_dota_hero_ancient_apparition_agi7", 
        },
        npc_dota_hero_ancient_apparition_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-8.png", name = "npc_dota_hero_ancient_apparition_agi8", 
        },
        npc_dota_hero_ancient_apparition_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-9.png", name = "npc_dota_hero_ancient_apparition_agi9", 
        },
        npc_dota_hero_ancient_apparition_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-10.png", name = "npc_dota_hero_ancient_apparition_agi10", 
        },
        npc_dota_hero_ancient_apparition_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-11.png", name = "npc_dota_hero_ancient_apparition_agi11", 
        },
        npc_dota_hero_ancient_apparition_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-12.png", name = "npc_dota_hero_ancient_apparition_agi12", 
        },
        special_bonus_unique_npc_dota_hero_ancient_apparition_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_ancient_apparition/agi/agi-talant-12.png", name = "npc_dota_hero_ancient_apparition_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_ancient_apparition_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-6.png", name = "npc_dota_hero_ancient_apparition_int6", 
        },
        npc_dota_hero_ancient_apparition_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-7.png", name = "npc_dota_hero_ancient_apparition_int7", 
        },
        npc_dota_hero_ancient_apparition_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-8.png", name = "npc_dota_hero_ancient_apparition_int8", 
        },
        npc_dota_hero_ancient_apparition_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-9.png", name = "npc_dota_hero_ancient_apparition_int9", 
        },
        npc_dota_hero_ancient_apparition_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-10.png", name = "npc_dota_hero_ancient_apparition_int10", 
        },
        npc_dota_hero_ancient_apparition_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-11.png", name = "npc_dota_hero_ancient_apparition_int11", 
        },
        npc_dota_hero_ancient_apparition_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-12.png", name = "npc_dota_hero_ancient_apparition_int12", 
        },
        special_bonus_unique_npc_dota_hero_ancient_apparition_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_ancient_apparition/int/int-talant-7.png", name = "npc_dota_hero_ancient_apparition_int13", 
        },
    },
    npc_dota_hero_bloodseeker = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_bloodseeker_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-6.png", name = "npc_dota_hero_bloodseeker_str6", 
        },
        npc_dota_hero_bloodseeker_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_bloodseeker/str/bloodseeker_thirst.png", name = "npc_dota_hero_bloodseeker_str7", 
        },
        npc_dota_hero_bloodseeker_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-8.png", name = "npc_dota_hero_bloodseeker_str8", 
        },
        npc_dota_hero_bloodseeker_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-9.png", name = "npc_dota_hero_bloodseeker_str9", 
        },
        npc_dota_hero_bloodseeker_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_bloodseeker/str/bloodseeker_thirst.png", name = "npc_dota_hero_bloodseeker_str10", 
        },
        npc_dota_hero_bloodseeker_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-11.png", name = "npc_dota_hero_bloodseeker_str11", 
        },
        npc_dota_hero_bloodseeker_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-12.png", name = "npc_dota_hero_bloodseeker_str12", 
        },
        special_bonus_unique_npc_dota_hero_bloodseeker_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_bloodseeker/str/str-talant-12.png", name = "npc_dota_hero_bloodseeker_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_bloodseeker_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-6.png", name = "npc_dota_hero_bloodseeker_agi6", 
        },
        npc_dota_hero_bloodseeker_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-7.png", name = "npc_dota_hero_bloodseeker_agi7", 
        },
        npc_dota_hero_bloodseeker_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-8.png", name = "npc_dota_hero_bloodseeker_agi8", 
        },
        npc_dota_hero_bloodseeker_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-9.png", name = "npc_dota_hero_bloodseeker_agi9", 
        },
        npc_dota_hero_bloodseeker_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-10.png", name = "npc_dota_hero_bloodseeker_agi10", 
        },
        npc_dota_hero_bloodseeker_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-6.png", name = "npc_dota_hero_bloodseeker_agi11",
        },
        npc_dota_hero_bloodseeker_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-10.png", name = "npc_dota_hero_bloodseeker_agi12",
        },
        special_bonus_unique_npc_dota_hero_bloodseeker_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_bloodseeker/agi/agi-talant-12.png", name = "npc_dota_hero_bloodseeker_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_bloodseeker_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-9.png", name = "npc_dota_hero_bloodseeker_int6",
        },
        npc_dota_hero_bloodseeker_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-7.png", name = "npc_dota_hero_bloodseeker_int7",
        },
        npc_dota_hero_bloodseeker_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-8.png", name = "npc_dota_hero_bloodseeker_int8",
        },
        npc_dota_hero_bloodseeker_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-9.png", name = "npc_dota_hero_bloodseeker_int9",
        },
        npc_dota_hero_bloodseeker_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-12.png", name = "npc_dota_hero_bloodseeker_int10",
        },
        npc_dota_hero_bloodseeker_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-11.png", name = "npc_dota_hero_bloodseeker_int11",
        },
        npc_dota_hero_bloodseeker_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-12.png", name = "npc_dota_hero_bloodseeker_int12",
        },
        special_bonus_unique_npc_dota_hero_bloodseeker_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_bloodseeker/int/int-talant-9.png", name = "npc_dota_hero_bloodseeker_int13",
        },
    },
    npc_dota_hero_magnataur = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_magnataur_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_magnataur/str/str-talant-11.png", name = "npc_dota_hero_magnataur_str6",
        },
        npc_dota_hero_magnataur_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_magnataur/str/str-talant-7.png", name = "npc_dota_hero_magnataur_str7",
        },
        npc_dota_hero_magnataur_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_magnataur/str/str-talant-8.png", name = "npc_dota_hero_magnataur_str8",
        },
        npc_dota_hero_magnataur_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_magnataur/str/str-talant-12.png", name = "npc_dota_hero_magnataur_str9",
        },
        npc_dota_hero_magnataur_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_magnataur/str/str-talant-11.png", name = "npc_dota_hero_magnataur_str10",
        },
        npc_dota_hero_magnataur_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_magnataur/str/str-talant-11.png", name = "npc_dota_hero_magnataur_str11",
        },
        npc_dota_hero_magnataur_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_magnataur/str/str-talant-9.png", name = "npc_dota_hero_magnataur_str12",
        },
        special_bonus_unique_npc_dota_hero_magnataur_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_magnataur/str/str-talant-9.png", name = "npc_dota_hero_magnataur_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_magnataur_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-6.png", name = "npc_dota_hero_magnataur_agi6",
        },
        npc_dota_hero_magnataur_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-6.png", name = "npc_dota_hero_magnataur_agi7",
        },
        npc_dota_hero_magnataur_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-8.png", name = "npc_dota_hero_magnataur_agi8",
        },
        npc_dota_hero_magnataur_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-9.png", name = "npc_dota_hero_magnataur_agi9",
        },
        npc_dota_hero_magnataur_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-10.png", name = "npc_dota_hero_magnataur_agi10",
        },
        npc_dota_hero_magnataur_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-11.png", name = "npc_dota_hero_magnataur_agi11",
        },
        npc_dota_hero_magnataur_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-12.png", name = "npc_dota_hero_magnataur_agi12",
        },
        special_bonus_unique_npc_dota_hero_magnataur_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_magnataur/agi/agi-talant-12.png", name = "npc_dota_hero_magnataur_agi13",
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_magnataur_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_magnataur/int/int-talant-6.png", name = "npc_dota_hero_magnataur_int6",
        },
        npc_dota_hero_magnataur_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_magnataur/int/int-talant-12.png", name = "npc_dota_hero_magnataur_int7", 
        },
        npc_dota_hero_magnataur_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_magnataur/int/int-talant-8.png", name = "npc_dota_hero_magnataur_int8", 
        },
        npc_dota_hero_magnataur_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_magnataur/int/int-talant-9.png", name = "npc_dota_hero_magnataur_int9", 
        },
        npc_dota_hero_magnataur_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_magnataur/int/int-talant-11.png", name = "npc_dota_hero_magnataur_int10", 
        },
        npc_dota_hero_magnataur_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_magnataur/int/int-talant-11.png", name = "npc_dota_hero_magnataur_int11", 
        },
        npc_dota_hero_magnataur_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_magnataur/int/int-talant-12.png", name = "npc_dota_hero_magnataur_int12", 
        },
        special_bonus_unique_npc_dota_hero_magnataur_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_magnataur/int/int-talant-10.png", name = "npc_dota_hero_magnataur_int13", 
        },
    },
    npc_dota_hero_huskar = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_huskar_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_huskar/str/1.png", name = "npc_dota_hero_huskar_str6", 
        },
        npc_dota_hero_huskar_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_huskar/str/1.png", name = "npc_dota_hero_huskar_str7", 
        },
        npc_dota_hero_huskar_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_huskar/str/2.png", name = "npc_dota_hero_huskar_str8", 
        },
        npc_dota_hero_huskar_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_huskar/str/1.png", name = "npc_dota_hero_huskar_str9", 
        },
        npc_dota_hero_huskar_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_huskar/str/1.png", name = "npc_dota_hero_huskar_str10", 
        },
        npc_dota_hero_huskar_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_huskar/str/1.png", name = "npc_dota_hero_huskar_str11", 
        },
        npc_dota_hero_huskar_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_huskar/str/2.png", name = "npc_dota_hero_huskar_str12", 
        },
        special_bonus_unique_npc_dota_hero_huskar_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_huskar/str/4.png", name = "npc_dota_hero_huskar_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_huskar_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_huskar/agi/2.png", name = "npc_dota_hero_huskar_agi6", 
        },
        npc_dota_hero_huskar_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi7", 
        },
        npc_dota_hero_huskar_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi8", 
        },
        npc_dota_hero_huskar_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi9", 
        },
        npc_dota_hero_huskar_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_huskar/agi/2.png", name = "npc_dota_hero_huskar_agi10", 
        },
        npc_dota_hero_huskar_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi11", 
        },
        npc_dota_hero_huskar_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi12", 
        },
        special_bonus_unique_npc_dota_hero_huskar_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_huskar/agi/1.png", name = "npc_dota_hero_huskar_agi13", 
        },
        
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_huskar_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_huskar/int/1.png", name = "npc_dota_hero_huskar_int6", 
        },
        npc_dota_hero_huskar_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_huskar/int/2.png", name = "npc_dota_hero_huskar_int7", 
        },
        npc_dota_hero_huskar_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_huskar/int/4.png", name = "npc_dota_hero_huskar_int8", 
        },
        npc_dota_hero_huskar_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_huskar/int/1.png", name = "npc_dota_hero_huskar_int9", 
        },
        npc_dota_hero_huskar_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_huskar/int/3.png", name = "npc_dota_hero_huskar_int10", 
        },
        npc_dota_hero_huskar_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_huskar/int/3.png", name = "npc_dota_hero_huskar_int11", 
        },
        npc_dota_hero_huskar_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_huskar/int/3.png", name = "npc_dota_hero_huskar_int12", 
        },
        special_bonus_unique_npc_dota_hero_huskar_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_huskar/int/3.png", name = "npc_dota_hero_huskar_int13", 
        },
    },
    npc_dota_hero_broodmother = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_broodmother_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_str6", 
        },
        npc_dota_hero_broodmother_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_str7", 
        },
        npc_dota_hero_broodmother_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_2.png", name = "npc_dota_hero_broodmother_str8", 
        },
        npc_dota_hero_broodmother_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_str9", 
        },
        npc_dota_hero_broodmother_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_str10", 
        },
        npc_dota_hero_broodmother_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_2.png", name = "npc_dota_hero_broodmother_str11", 
        },
        npc_dota_hero_broodmother_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_str12", 
        },
        special_bonus_unique_npc_dota_hero_broodmother_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_broodmother/str/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_broodmother_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_agi6", 
        },
        npc_dota_hero_broodmother_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_2.png", name = "npc_dota_hero_broodmother_agi7", 
        },
        npc_dota_hero_broodmother_agi8 = {
            place = {"agi 10"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_agi8", 
        },
        npc_dota_hero_broodmother_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_agi9", 
        },
        npc_dota_hero_broodmother_agi10 = {
            place = {"agi 8"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_2.png", name = "npc_dota_hero_broodmother_agi10", 
        },
        npc_dota_hero_broodmother_agi11 = {
            place = {"agi 12"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_agi11", 
        },
        npc_dota_hero_broodmother_agi_last = {
            place = {"agi 11"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_agi12", 
        },
        special_bonus_unique_npc_dota_hero_broodmother_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_broodmother/agi/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_broodmother_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_int6", 
        },
        npc_dota_hero_broodmother_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_int7", 
        },
        npc_dota_hero_broodmother_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_broodmother/int/int-talant-8.png", name = "npc_dota_hero_broodmother_int8", 
        },
        npc_dota_hero_broodmother_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_3.png", name = "npc_dota_hero_broodmother_int9", 
        },
        npc_dota_hero_broodmother_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_1.png", name = "npc_dota_hero_broodmother_int10", 
        },
        npc_dota_hero_broodmother_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_int11", 
        },
        npc_dota_hero_broodmother_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_int12", 
        },
        special_bonus_unique_npc_dota_hero_broodmother_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_broodmother/int/broodmother_v2_4.png", name = "npc_dota_hero_broodmother_int13", 
        },
    },
    npc_dota_hero_silencer = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_silencer_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_silencer/str/2.png", name = "npc_dota_hero_silencer_str6", 
        },
        npc_dota_hero_silencer_str7 = {
            place = {"str 9"}, url = "/npc_dota_hero_silencer/str/3.png", name = "npc_dota_hero_silencer_str7", 
        },
        npc_dota_hero_silencer_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_silencer/str/2.png", name = "npc_dota_hero_silencer_str8", 
        },
        npc_dota_hero_silencer_str9 = {
            place = {"str 7"}, url = "/npc_dota_hero_silencer/str/1.png", name = "npc_dota_hero_silencer_str9", 
        },
        npc_dota_hero_silencer_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_silencer/str/1.png", name = "npc_dota_hero_silencer_str10", 
        },
        npc_dota_hero_silencer_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_silencer/str/2.png", name = "npc_dota_hero_silencer_str11", 
        },
        npc_dota_hero_silencer_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_silencer/str/4.png", name = "npc_dota_hero_silencer_str12", 
        },
        special_bonus_unique_npc_dota_hero_silencer_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_silencer/str/4.png", name = "npc_dota_hero_silencer_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_silencer_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi6", 
        },
        npc_dota_hero_silencer_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi7", 
        },
        npc_dota_hero_silencer_agi8 = {
            place = {"agi 10"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi8", 
        },
        npc_dota_hero_silencer_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi9", 
        },
        npc_dota_hero_silencer_agi10 = {
            place = {"agi 8"}, url = "/npc_dota_hero_silencer/agi/1.png", name = "npc_dota_hero_silencer_agi10", 
        },
        npc_dota_hero_silencer_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_silencer/agi/1.png", name = "npc_dota_hero_silencer_agi11", 
        },
        npc_dota_hero_silencer_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi12", 
        },
        special_bonus_unique_npc_dota_hero_silencer_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_silencer/agi/2.png", name = "npc_dota_hero_silencer_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_silencer_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int6", 
        },
        npc_dota_hero_silencer_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int7", 
        },
        npc_dota_hero_silencer_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_silencer/int/3.png", name = "npc_dota_hero_silencer_int8", 
        },
        npc_dota_hero_silencer_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int9", 
        },
        npc_dota_hero_silencer_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int10", 
        },
        npc_dota_hero_silencer_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_silencer/int/3.png", name = "npc_dota_hero_silencer_int11", 
        },
        npc_dota_hero_silencer_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int12", 
        },
        special_bonus_unique_npc_dota_hero_silencer_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_silencer/int/2.png", name = "npc_dota_hero_silencer_int13", 
        },
    },
    npc_dota_hero_vengefulspirit = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_vengefulspirit_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_vengefulspirit/str/1.png", name = "npc_dota_hero_vengefulspirit_str6", 
        },
        npc_dota_hero_vengefulspirit_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_vengefulspirit/str/3.png", name = "npc_dota_hero_vengefulspirit_str7", 
        },
        npc_dota_hero_vengefulspirit_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_vengefulspirit/str/4.png", name = "npc_dota_hero_vengefulspirit_str8", 
        },
        npc_dota_hero_vengefulspirit_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_vengefulspirit/str/2.png", name = "npc_dota_hero_vengefulspirit_str9", 
        },
        npc_dota_hero_vengefulspirit_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_vengefulspirit/str/3.png", name = "npc_dota_hero_vengefulspirit_str10", 
        },
        npc_dota_hero_vengefulspirit_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_vengefulspirit/str/4.png", name = "npc_dota_hero_vengefulspirit_str11", 
        },
        npc_dota_hero_vengefulspirit_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_vengefulspirit/str/4.png", name = "npc_dota_hero_vengefulspirit_str12", 
        },
        npc_dota_hero_vengefulspirit_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_vengefulspirit/str/4.png", name = "npc_dota_hero_vengefulspirit_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_vengefulspirit_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_vengefulspirit/agi/2.png", name = "npc_dota_hero_vengefulspirit_agi6", 
        },
        npc_dota_hero_vengefulspirit_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_vengefulspirit/agi/4.png", name = "npc_dota_hero_vengefulspirit_agi7", 
        },
        npc_dota_hero_vengefulspirit_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_vengefulspirit/agi/2.png", name = "npc_dota_hero_vengefulspirit_agi8", 
        },
        npc_dota_hero_vengefulspirit_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_vengefulspirit/agi/4.png", name = "npc_dota_hero_vengefulspirit_agi9", 
        },
        npc_dota_hero_vengefulspirit_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_vengefulspirit/agi/1.png", name = "npc_dota_hero_vengefulspirit_agi10", 
        },
        npc_dota_hero_vengefulspirit_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_vengefulspirit/agi/2.png", name = "npc_dota_hero_vengefulspirit_agi11", 
        },
        npc_dota_hero_vengefulspirit_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_vengefulspirit/agi/4.png", name = "npc_dota_hero_vengefulspirit_agi12", 
        },
        npc_dota_hero_vengefulspirit_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_vengefulspirit/agi/3.png", name = "npc_dota_hero_vengefulspirit_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_vengefulspirit_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_vengefulspirit/int/4.png", name = "npc_dota_hero_vengefulspirit_int6", 
        },
        npc_dota_hero_vengefulspirit_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_vengefulspirit/int/2.png", name = "npc_dota_hero_vengefulspirit_int7", 
        },
        npc_dota_hero_vengefulspirit_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_vengefulspirit/int/1.png", name = "npc_dota_hero_vengefulspirit_int8", 
        },
        npc_dota_hero_vengefulspirit_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_vengefulspirit/int/3.png", name = "npc_dota_hero_vengefulspirit_int9", 
        },
        npc_dota_hero_vengefulspirit_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_vengefulspirit/int/2.png", name = "npc_dota_hero_vengefulspirit_int10", 
        },
        npc_dota_hero_vengefulspirit_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_vengefulspirit/int/1.png", name = "npc_dota_hero_vengefulspirit_int11", 
        },
        npc_dota_hero_vengefulspirit_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_vengefulspirit/int/1.png", name = "npc_dota_hero_vengefulspirit_int12", 
        },
        npc_dota_hero_vengefulspirit_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_vengefulspirit/int/1.png", name = "npc_dota_hero_vengefulspirit_int13", 
        },
    },
    npc_dota_hero_razor = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_razor_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_razor/str/1.png", name = "npc_dota_hero_razor_str6", 
        },
        npc_dota_hero_razor_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_razor/str/4.png", name = "npc_dota_hero_razor_str7", 
        },
        npc_dota_hero_razor_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_razor/str/3.png", name = "npc_dota_hero_razor_str8", 
        },
        npc_dota_hero_razor_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_razor/str/1.png", name = "npc_dota_hero_razor_str9", 
        },
        npc_dota_hero_razor_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_razor/str/4.png", name = "npc_dota_hero_razor_str10", 
        },
        npc_dota_hero_razor_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_razor/str/3.png", name = "npc_dota_hero_razor_str11", 
        },
        npc_dota_hero_razor_str_last = {
            place = {"str 12"}, url = "/npc_dota_hero_razor/str/4.png", name = "npc_dota_hero_razor_str12", 
        },
        special_bonus_unique_npc_dota_hero_razor_str50 = {
            place = {"str 13"}, url = "/npc_dota_hero_razor/str/4.png", name = "npc_dota_hero_razor_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_razor_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_razor/agi/4.png", name = "npc_dota_hero_razor_agi6", 
        },
        npc_dota_hero_razor_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi7", 
        },
        npc_dota_hero_razor_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi8", 
        },
        npc_dota_hero_razor_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi9", 
        },
        npc_dota_hero_razor_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi10", 
        },
        npc_dota_hero_razor_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_razor/agi/4.png", name = "npc_dota_hero_razor_agi11", 
        },
        npc_dota_hero_razor_agi_last = {
            place = {"agi 12"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi12", 
        },
        special_bonus_unique_npc_dota_hero_razor_agi50 = {
            place = {"agi 13"}, url = "/npc_dota_hero_razor/agi/2.png", name = "npc_dota_hero_razor_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_razor_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_razor/int/4.png", name = "npc_dota_hero_razor_int6", 
        },
        npc_dota_hero_razor_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_razor/int/3.png", name = "npc_dota_hero_razor_int7", 
        },
        npc_dota_hero_razor_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_razor/int/1.png", name = "npc_dota_hero_razor_int8", 
        },
        npc_dota_hero_razor_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_razor/int/4.png", name = "npc_dota_hero_razor_int9", 
        },
        npc_dota_hero_razor_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_razor/int/3.png", name = "npc_dota_hero_razor_int10", 
        },
        npc_dota_hero_razor_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_razor/int/1.png", name = "npc_dota_hero_razor_int11", 
        },
        npc_dota_hero_razor_int_last = {
            place = {"int 12"}, url = "/npc_dota_hero_razor/int/1.png", name = "npc_dota_hero_razor_int12", 
        },
        special_bonus_unique_npc_dota_hero_razor_int50 = {
            place = {"int 13"}, url = "/npc_dota_hero_razor/int/1.png", name = "npc_dota_hero_razor_int13", 
        },
    },
    npc_dota_hero_death_prophet = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_death_prophet_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_death_prophet/str/1.png", name = "npc_dota_hero_death_prophet_str6", 
        },
        npc_dota_hero_death_prophet_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_death_prophet/str/4.png", name = "npc_dota_hero_death_prophet_str7", 
        },
        npc_dota_hero_death_prophet_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_death_prophet/str/3.png", name = "npc_dota_hero_death_prophet_str8", 
        },
        npc_dota_hero_death_prophet_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_death_prophet/str/1.png", name = "npc_dota_hero_death_prophet_str9", 
        },
        npc_dota_hero_death_prophet_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_death_prophet/str/4.png", name = "npc_dota_hero_death_prophet_str10", 
        },
        npc_dota_hero_death_prophet_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_death_prophet/str/3.png", name = "npc_dota_hero_death_prophet_str11", 
        },
        npc_dota_hero_death_prophet_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_death_prophet/str/4.png", name = "npc_dota_hero_death_prophet_str12",
        },
        npc_dota_hero_death_prophet_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_death_prophet/str/4.png", name = "npc_dota_hero_death_prophet_str13",
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_death_prophet_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi6", 
        },
        npc_dota_hero_death_prophet_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_death_prophet/agi/1.png", name = "npc_dota_hero_death_prophet_agi7", 
        },
        npc_dota_hero_death_prophet_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi8", 
        },
        npc_dota_hero_death_prophet_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi9", 
        },
        npc_dota_hero_death_prophet_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_death_prophet/agi/1.png", name = "npc_dota_hero_death_prophet_agi10", 
        },
        npc_dota_hero_death_prophet_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi11", 
        },
        npc_dota_hero_death_prophet_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi12", 
        },
        npc_dota_hero_death_prophet_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_death_prophet/agi/4.png", name = "npc_dota_hero_death_prophet_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_death_prophet_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_death_prophet/int/1.png", name = "npc_dota_hero_death_prophet_int6", 
        },
        npc_dota_hero_death_prophet_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_death_prophet/int/3.png", name = "npc_dota_hero_death_prophet_int7", 
        },
        npc_dota_hero_death_prophet_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_death_prophet/int/1.png", name = "npc_dota_hero_death_prophet_int8", 
        },
        npc_dota_hero_death_prophet_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_death_prophet/int/4.png", name = "npc_dota_hero_death_prophet_int9", 
        },
        npc_dota_hero_death_prophet_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_death_prophet/int/2.png", name = "npc_dota_hero_death_prophet_int10", 
        },
        npc_dota_hero_death_prophet_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_death_prophet/int/1.png", name = "npc_dota_hero_death_prophet_int11", 
        },  
        npc_dota_hero_death_prophet_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_death_prophet/int/4.png", name = "npc_dota_hero_death_prophet_int12", 
        },
        npc_dota_hero_death_prophet_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_death_prophet/int/4.png", name = "npc_dota_hero_death_prophet_int13", 
        },
    },
    npc_dota_hero_viper = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_viper_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_viper/str/4.png", name = "npc_dota_hero_viper_str6",
        },
        npc_dota_hero_viper_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_viper/str/3.png", name = "npc_dota_hero_viper_str7",
        },
        npc_dota_hero_viper_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_viper/str/3.png", name = "npc_dota_hero_viper_str8", 
        },
        npc_dota_hero_viper_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_viper/str/2.png", name = "npc_dota_hero_viper_str9", 
        },
        npc_dota_hero_viper_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_viper/str/1.png", name = "npc_dota_hero_viper_str10", 
        },
        npc_dota_hero_viper_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_viper/str/3.png", name = "npc_dota_hero_viper_str11", 
        },
        npc_dota_hero_viper_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_viper/str/3.png", name = "npc_dota_hero_viper_str12", 
        },
        npc_dota_hero_viper_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_viper/str/3.png", name = "npc_dota_hero_viper_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_viper_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_viper/agi/1.png", name = "npc_dota_hero_viper_agi6", 
        },
        npc_dota_hero_viper_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_viper/agi/2.png", name = "npc_dota_hero_viper_agi7", 
        },
        npc_dota_hero_viper_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_viper/agi/3.png", name = "npc_dota_hero_viper_agi8", 
        },
        npc_dota_hero_viper_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_viper/agi/4.png", name = "npc_dota_hero_viper_agi9", 
        },
        npc_dota_hero_viper_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_viper/agi/2.png", name = "npc_dota_hero_viper_agi10", 
        },
        npc_dota_hero_viper_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_viper/agi/3.png", name = "npc_dota_hero_viper_agi11", 
        },
        npc_dota_hero_viper_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_viper/agi/2.png", name = "npc_dota_hero_viper_agi12", 
        },
        npc_dota_hero_viper_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_viper/agi/2.png", name = "npc_dota_hero_viper_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_viper_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_viper/int/1.png", name = "npc_dota_hero_viper_int6", 
        },
        npc_dota_hero_viper_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_viper/int/2.png", name = "npc_dota_hero_viper_int7", 
        },
        npc_dota_hero_viper_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_viper/int/4.png", name = "npc_dota_hero_viper_int8", 
        },
        npc_dota_hero_viper_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_viper/int/1.png", name = "npc_dota_hero_viper_int9", 
        },
        npc_dota_hero_viper_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_viper/int/2.png", name = "npc_dota_hero_viper_int10", 
        }, 
        npc_dota_hero_viper_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_viper/int/4.png", name = "npc_dota_hero_viper_int11", 
        },  
        npc_dota_hero_viper_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_viper/int/1.png", name = "npc_dota_hero_viper_int12", 
        },	
        npc_dota_hero_viper_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_viper/int/1.png", name = "npc_dota_hero_viper_int13", 
        },	
    },
    npc_dota_hero_spirit_breaker = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_spirit_breaker_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_spirit_breaker/str/2.png", name = "npc_dota_hero_spirit_breaker_str6", 
        },
        npc_dota_hero_spirit_breaker_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_spirit_breaker/str/3.png", name = "npc_dota_hero_spirit_breaker_str7", 
        },
        npc_dota_hero_spirit_breaker_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_spirit_breaker/str/2.png", name = "npc_dota_hero_spirit_breaker_str8", 
        },
        npc_dota_hero_spirit_breaker_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_spirit_breaker/str/2.png", name = "npc_dota_hero_spirit_breaker_str9", 
        },
        npc_dota_hero_spirit_breaker_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_spirit_breaker/str/2.png", name = "npc_dota_hero_spirit_breaker_str10", 
        },
        npc_dota_hero_spirit_breaker_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_spirit_breaker/str/2.png", name = "npc_dota_hero_spirit_breaker_str11", 
        },
        npc_dota_hero_spirit_breaker_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_spirit_breaker/str/3.png", name = "npc_dota_hero_spirit_breaker_str12", 
        },
        npc_dota_hero_spirit_breaker_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_spirit_breaker/str/3.png", name = "npc_dota_hero_spirit_breaker_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_spirit_breaker_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_spirit_breaker/agi/1.png", name = "npc_dota_hero_spirit_breaker_agi6", 
        },
        npc_dota_hero_spirit_breaker_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_spirit_breaker/agi/2.png", name = "npc_dota_hero_spirit_breaker_agi7", 
        },
        npc_dota_hero_spirit_breaker_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_spirit_breaker/agi/3.png", name = "npc_dota_hero_spirit_breaker_agi8", 
        },
        npc_dota_hero_spirit_breaker_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_spirit_breaker/agi/4.png", name = "npc_dota_hero_spirit_breaker_agi9", 
        },
        npc_dota_hero_spirit_breaker_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_spirit_breaker/agi/1.png", name = "npc_dota_hero_spirit_breaker_agi10", 
        },
        npc_dota_hero_spirit_breaker_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_spirit_breaker/agi/3.png", name = "npc_dota_hero_spirit_breaker_agi11", 
        },
        npc_dota_hero_spirit_breaker_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_spirit_breaker/agi/3.png", name = "npc_dota_hero_spirit_breaker_agi12", 
        },
        npc_dota_hero_spirit_breaker_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_spirit_breaker/agi/3.png", name = "npc_dota_hero_spirit_breaker_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_spirit_breaker_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_spirit_breaker/int/2.png", name = "npc_dota_hero_spirit_breaker_int6", 
        },
        npc_dota_hero_spirit_breaker_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_spirit_breaker/int/3.png", name = "npc_dota_hero_spirit_breaker_int7", 
        },
        npc_dota_hero_spirit_breaker_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_spirit_breaker/int/4.png", name = "npc_dota_hero_spirit_breaker_int8", 
        }, 
        npc_dota_hero_spirit_breaker_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_spirit_breaker/int/2.png", name = "npc_dota_hero_spirit_breaker_int9", 
        },
        npc_dota_hero_spirit_breaker_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_spirit_breaker/int/3.png", name = "npc_dota_hero_spirit_breaker_int10", 
        }, 
        npc_dota_hero_spirit_breaker_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_spirit_breaker/int/1.png", name = "npc_dota_hero_spirit_breaker_int11", 
        }, 
        npc_dota_hero_spirit_breaker_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_spirit_breaker/int/1.png", name = "npc_dota_hero_spirit_breaker_int12", 
        },	
        npc_dota_hero_spirit_breaker_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_spirit_breaker/int/3.png", name = "npc_dota_hero_spirit_breaker_int13", 
        },	
    },
    npc_dota_hero_dawnbreaker = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_dawnbreaker_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_dawnbreaker/str/3.png", name = "npc_dota_hero_dawnbreaker_str6", 
        },
        npc_dota_hero_dawnbreaker_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_dawnbreaker/str/4.png", name = "npc_dota_hero_dawnbreaker_str7", 
        },
        npc_dota_hero_dawnbreaker_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_dawnbreaker/str/3.png", name = "npc_dota_hero_dawnbreaker_str8", 
        },
        npc_dota_hero_dawnbreaker_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_dawnbreaker/str/1.png", name = "npc_dota_hero_dawnbreaker_str9", 
        },
        npc_dota_hero_dawnbreaker_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_dawnbreaker/str/4.png", name = "npc_dota_hero_dawnbreaker_str10", 
        },
        npc_dota_hero_dawnbreaker_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_dawnbreaker/str/1.png", name = "npc_dota_hero_dawnbreaker_str11", 
        },
        npc_dota_hero_dawnbreaker_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_dawnbreaker/str/4.png", name = "npc_dota_hero_dawnbreaker_str12", 
        },
        npc_dota_hero_dawnbreaker_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_dawnbreaker/str/4.png", name = "npc_dota_hero_dawnbreaker_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_dawnbreaker_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_dawnbreaker/agi/4.png", name = "npc_dota_hero_dawnbreaker_agi6", 
        },
        npc_dota_hero_dawnbreaker_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_dawnbreaker/agi/3.png", name = "npc_dota_hero_dawnbreaker_agi7", 
        },
        npc_dota_hero_dawnbreaker_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_dawnbreaker/agi/2.png", name = "npc_dota_hero_dawnbreaker_agi8", 
        }, 
        npc_dota_hero_dawnbreaker_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_dawnbreaker/agi/2.png", name = "npc_dota_hero_dawnbreaker_agi9", 
        },
        npc_dota_hero_dawnbreaker_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_dawnbreaker/agi/1.png", name = "npc_dota_hero_dawnbreaker_agi10", 
        },
        npc_dota_hero_dawnbreaker_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_dawnbreaker/agi/4.png", name = "npc_dota_hero_dawnbreaker_agi11", 
        },
        npc_dota_hero_dawnbreaker_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_dawnbreaker/agi/2.png", name = "npc_dota_hero_dawnbreaker_agi12", 
        },
        npc_dota_hero_dawnbreaker_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_dawnbreaker/agi/2.png", name = "npc_dota_hero_dawnbreaker_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_dawnbreaker_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_dawnbreaker/int/4.png", name = "npc_dota_hero_dawnbreaker_int6", 
        },
        npc_dota_hero_dawnbreaker_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_dawnbreaker/int/3.png", name = "npc_dota_hero_dawnbreaker_int7", 
        },
        npc_dota_hero_dawnbreaker_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_dawnbreaker/int/1.png", name = "npc_dota_hero_dawnbreaker_int8", 
        }, 
        npc_dota_hero_dawnbreaker_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_dawnbreaker/int/4.png", name = "npc_dota_hero_dawnbreaker_int9", 
        },
        npc_dota_hero_dawnbreaker_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_dawnbreaker/int/3.png", name = "npc_dota_hero_dawnbreaker_int10", 
        },
        npc_dota_hero_dawnbreaker_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_dawnbreaker/int/1.png", name = "npc_dota_hero_dawnbreaker_int11", 
        },  
        npc_dota_hero_dawnbreaker_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_dawnbreaker/int/1.png", name = "npc_dota_hero_dawnbreaker_int12", 
        },	
        npc_dota_hero_dawnbreaker_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_dawnbreaker/int/1.png", name = "npc_dota_hero_dawnbreaker_int13", 
        },	
    },
    npc_dota_hero_riki = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_riki_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_riki/str/4.png", name = "npc_dota_hero_riki_str6", 
        },
        npc_dota_hero_riki_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_riki/str/4.png", name = "npc_dota_hero_riki_str7", 
        },
        npc_dota_hero_riki_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_riki/str/4.png", name = "npc_dota_hero_riki_str8", 
        },  
        npc_dota_hero_riki_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_riki/str/2.png", name = "npc_dota_hero_riki_str9", 
        },
        npc_dota_hero_riki_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_riki/str/1.png", name = "npc_dota_hero_riki_str10", 
        },
        npc_dota_hero_riki_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_riki/str/1.png", name = "npc_dota_hero_riki_str11", 
        },
        npc_dota_hero_riki_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_riki/str/3.png", name = "npc_dota_hero_riki_str12", 
        },
        npc_dota_hero_riki_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_riki/str/3.png", name = "npc_dota_hero_riki_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_riki_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_riki/agi/3.png", name = "npc_dota_hero_riki_agi6", 
        },
        npc_dota_hero_riki_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_riki/agi/3.png", name = "npc_dota_hero_riki_agi7", 
        },
        npc_dota_hero_riki_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_riki/agi/4.png", name = "npc_dota_hero_riki_agi8", 
        },
        npc_dota_hero_riki_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_riki/agi/1.png", name = "npc_dota_hero_riki_agi9", 
        },
        npc_dota_hero_riki_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_riki/agi/1.png", name = "npc_dota_hero_riki_agi10", 
        },
        npc_dota_hero_riki_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_riki/agi/2.png", name = "npc_dota_hero_riki_agi11", 
        },
        npc_dota_hero_riki_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_riki/agi/3.png", name = "npc_dota_hero_riki_agi12", 
        },
        npc_dota_hero_riki_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_riki/agi/3.png", name = "npc_dota_hero_riki_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_riki_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_riki/int/3.png", name = "npc_dota_hero_riki_int6", 
        },
        npc_dota_hero_riki_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_riki/int/1.png", name = "npc_dota_hero_riki_int7", 
        },
        npc_dota_hero_riki_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_riki/int/3.png", name = "npc_dota_hero_riki_int8", 
        },
        npc_dota_hero_riki_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_riki/int/5.png", name = "npc_dota_hero_riki_int9", 
        },
        npc_dota_hero_riki_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_riki/int/4.png", name = "npc_dota_hero_riki_int10", 
        },
        npc_dota_hero_riki_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_riki/int/3.png", name = "npc_dota_hero_riki_int11", 
        },  
        npc_dota_hero_riki_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_riki/int/4.png", name = "npc_dota_hero_riki_int12", 
        },
        npc_dota_hero_riki_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_riki/int/4.png", name = "npc_dota_hero_riki_int13", 
        },
    },
    npc_dota_hero_leshrac = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_leshrac_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_leshrac/str/1.png", name = "npc_dota_hero_leshrac_str6", 
        },
        npc_dota_hero_leshrac_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_leshrac/str/4.png", name = "npc_dota_hero_leshrac_str7", 
        },
        npc_dota_hero_leshrac_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_leshrac/str/1.png", name = "npc_dota_hero_leshrac_str8", 
        },  
        npc_dota_hero_leshrac_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_leshrac/str/1.png", name = "npc_dota_hero_leshrac_str9", 
        },
        npc_dota_hero_leshrac_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_leshrac/str/4.png", name = "npc_dota_hero_leshrac_str10", 
        },
        npc_dota_hero_leshrac_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_leshrac/str/3.png", name = "npc_dota_hero_leshrac_str11", 
        },
        npc_dota_hero_leshrac_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_leshrac/str/3.png", name = "npc_dota_hero_leshrac_str12", 
        },
        npc_dota_hero_leshrac_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_leshrac/str/3.png", name = "npc_dota_hero_leshrac_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_leshrac_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_leshrac/agi/4.png", name = "npc_dota_hero_leshrac_agi6", 
        },
        npc_dota_hero_leshrac_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_leshrac/agi/1.png", name = "npc_dota_hero_leshrac_agi7", 
        },
        npc_dota_hero_leshrac_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_leshrac/agi/2.png", name = "npc_dota_hero_leshrac_agi8", 
        },
        npc_dota_hero_leshrac_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_leshrac/agi/3.png", name = "npc_dota_hero_leshrac_agi9", 
        },
        npc_dota_hero_leshrac_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_leshrac/agi/1.png", name = "npc_dota_hero_leshrac_agi10", 
        },
        npc_dota_hero_leshrac_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_leshrac/agi/2.png", name = "npc_dota_hero_leshrac_agi11", 
        },
        npc_dota_hero_leshrac_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_leshrac/agi/2.png", name = "npc_dota_hero_leshrac_agi12", 
        },
        npc_dota_hero_leshrac_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_leshrac/agi/2.png", name = "npc_dota_hero_leshrac_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_leshrac_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_leshrac/int/4.png", name = "npc_dota_hero_leshrac_int6", 
        },
        npc_dota_hero_leshrac_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_leshrac/int/4.png", name = "npc_dota_hero_leshrac_int7", 
        },
        npc_dota_hero_leshrac_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_leshrac/int/4.png", name = "npc_dota_hero_leshrac_int8", 
        },
        npc_dota_hero_leshrac_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_leshrac/int/1.png", name = "npc_dota_hero_leshrac_int9", 
        },
        npc_dota_hero_leshrac_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_leshrac/int/3.png", name = "npc_dota_hero_leshrac_int10", 
        },
        npc_dota_hero_leshrac_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_leshrac/int/2.png", name = "npc_dota_hero_leshrac_int11", 
        },  
        npc_dota_hero_leshrac_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_leshrac/int/4.png", name = "npc_dota_hero_leshrac_int12", 
        },	
        npc_dota_hero_leshrac_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_leshrac/int/4.png", name = "npc_dota_hero_leshrac_int13", 
        },	
    },
    npc_dota_hero_necrolyte = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_necrolyte_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_necrolyte/str/3.png", name = "npc_dota_hero_necrolyte_str6", 
        },
        npc_dota_hero_necrolyte_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_necrolyte/str/1.png", name = "npc_dota_hero_necrolyte_str7", 
        },
        npc_dota_hero_necrolyte_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_necrolyte/str/2.png", name = "npc_dota_hero_necrolyte_str8", 
        },  
        npc_dota_hero_necrolyte_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_necrolyte/str/3.png", name = "npc_dota_hero_necrolyte_str9", 
        },
        npc_dota_hero_necrolyte_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_necrolyte/str/1.png", name = "npc_dota_hero_necrolyte_str10", 
        },
        npc_dota_hero_necrolyte_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_necrolyte/str/2.png", name = "npc_dota_hero_necrolyte_str11", 
        },
        npc_dota_hero_necrolyte_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_necrolyte/str/2.png", name = "npc_dota_hero_necrolyte_str12", 
        },
        npc_dota_hero_necrolyte_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_necrolyte/str/3.png", name = "npc_dota_hero_necrolyte_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_necrolyte_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_necrolyte/agi/3.png", name = "npc_dota_hero_necrolyte_agi6", 
        },
        npc_dota_hero_necrolyte_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_necrolyte/agi/1.png", name = "npc_dota_hero_necrolyte_agi7", 
        },
        npc_dota_hero_necrolyte_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_necrolyte/agi/2.png", name = "npc_dota_hero_necrolyte_agi8", 
        },
        npc_dota_hero_necrolyte_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_necrolyte/agi/3.png", name = "npc_dota_hero_necrolyte_agi9", 
        },
        npc_dota_hero_necrolyte_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_necrolyte/agi/1.png", name = "npc_dota_hero_necrolyte_agi10", 
        },
        npc_dota_hero_necrolyte_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_necrolyte/agi/2.png", name = "npc_dota_hero_necrolyte_agi11", 
        },
        npc_dota_hero_necrolyte_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_necrolyte/agi/4.png", name = "npc_dota_hero_necrolyte_agi12", 
        },
        npc_dota_hero_necrolyte_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_necrolyte/agi/4.png", name = "npc_dota_hero_necrolyte_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_necrolyte_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_necrolyte/int/1.png", name = "npc_dota_hero_necrolyte_int6", 
        },
        npc_dota_hero_necrolyte_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_necrolyte/int/4.png", name = "npc_dota_hero_necrolyte_int7", 
        },
        npc_dota_hero_necrolyte_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_necrolyte/int/2.png", name = "npc_dota_hero_necrolyte_int8", 
        },
        npc_dota_hero_necrolyte_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_necrolyte/int/1.png", name = "npc_dota_hero_necrolyte_int9", 
        },
        npc_dota_hero_necrolyte_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_necrolyte/int/4.png", name = "npc_dota_hero_necrolyte_int10", 
        },
        npc_dota_hero_necrolyte_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_necrolyte/int/2.png", name = "npc_dota_hero_necrolyte_int11", 
        },  
        npc_dota_hero_necrolyte_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_necrolyte/int/4.png", name = "npc_dota_hero_necrolyte_int12", 
        },	
        npc_dota_hero_necrolyte_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_necrolyte/int/4.png", name = "npc_dota_hero_necrolyte_int13", 
        },	
    },
    npc_dota_hero_jakiro = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_jakiro_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_jakiro/str/1.png", name = "npc_dota_hero_jakiro_str6", 
        },
        npc_dota_hero_jakiro_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_jakiro/str/4.png", name = "npc_dota_hero_jakiro_str7", 
        },
        npc_dota_hero_jakiro_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_jakiro/str/2.png", name = "npc_dota_hero_jakiro_str8", 
        },  
        npc_dota_hero_jakiro_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_jakiro/str/1.png", name = "npc_dota_hero_jakiro_str9", 
        },
        npc_dota_hero_jakiro_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_jakiro/str/4.png", name = "npc_dota_hero_jakiro_str10", 
        },
        npc_dota_hero_jakiro_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_jakiro/str/2.png", name = "npc_dota_hero_jakiro_str11", 
        },
        npc_dota_hero_jakiro_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_jakiro/str/5.png", name = "npc_dota_hero_jakiro_str12", 
        },
        npc_dota_hero_jakiro_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_jakiro/str/5.png", name = "npc_dota_hero_jakiro_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_jakiro_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_jakiro/agi/3.png", name = "npc_dota_hero_jakiro_agi6", 
        },
        npc_dota_hero_jakiro_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_jakiro/agi/3.png", name = "npc_dota_hero_jakiro_agi7", 
        },
        npc_dota_hero_jakiro_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_jakiro/agi/1.png", name = "npc_dota_hero_jakiro_agi8", 
        },
        npc_dota_hero_jakiro_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_jakiro/agi/3.png", name = "npc_dota_hero_jakiro_agi9", 
        },
        npc_dota_hero_jakiro_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_jakiro/agi/1.png", name = "npc_dota_hero_jakiro_agi10", 
        },
        npc_dota_hero_jakiro_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_jakiro/agi/4.png", name = "npc_dota_hero_jakiro_agi11", 
        },
        npc_dota_hero_jakiro_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_jakiro/agi/3.png", name = "npc_dota_hero_jakiro_agi12", 
        },
        npc_dota_hero_jakiro_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_jakiro/agi/3.png", name = "npc_dota_hero_jakiro_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_jakiro_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_jakiro/int/3.png", name = "npc_dota_hero_jakiro_int6", 
        },
        npc_dota_hero_jakiro_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_jakiro/int/1.png", name = "npc_dota_hero_jakiro_int7", 
        },
        npc_dota_hero_jakiro_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_jakiro/int/4.png", name = "npc_dota_hero_jakiro_int8", 
        },
        npc_dota_hero_jakiro_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_jakiro/int/3.png", name = "npc_dota_hero_jakiro_int9", 
        },
        npc_dota_hero_jakiro_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_jakiro/int/1.png", name = "npc_dota_hero_jakiro_int10", 
        },
        npc_dota_hero_jakiro_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_jakiro/int/4.png", name = "npc_dota_hero_jakiro_int11", 
        },  
        npc_dota_hero_jakiro_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_jakiro/int/1.png", name = "npc_dota_hero_jakiro_int12", 
        },	
        npc_dota_hero_jakiro_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_jakiro/int/5.png", name = "npc_dota_hero_jakiro_int13", 
        },
    },
    npc_dota_hero_ursa = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_ursa_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_ursa/str/5.png", name = "npc_dota_hero_ursa_str6", 
        },
        npc_dota_hero_ursa_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_ursa/str/1.png", name = "npc_dota_hero_ursa_str7", 
        },
        npc_dota_hero_ursa_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_ursa/str/4.png", name = "npc_dota_hero_ursa_str8", 
        },  
        npc_dota_hero_ursa_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_ursa/str/3.png", name = "npc_dota_hero_ursa_str9", 
        },
        npc_dota_hero_ursa_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_ursa/str/1.png", name = "npc_dota_hero_ursa_str10", 
        },
        npc_dota_hero_ursa_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_ursa/str/4.png", name = "npc_dota_hero_ursa_str11", 
        },
        npc_dota_hero_ursa_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_ursa/str/1.png", name = "npc_dota_hero_ursa_str12", 
        },
        npc_dota_hero_ursa_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_ursa/str/4.png", name = "npc_dota_hero_ursa_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_ursa_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_ursa/agi/3.png", name = "npc_dota_hero_ursa_agi6", 
        },
        npc_dota_hero_ursa_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_ursa/agi/3.png", name = "npc_dota_hero_ursa_agi7", 
        },
        npc_dota_hero_ursa_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_ursa/agi/2.png", name = "npc_dota_hero_ursa_agi8", 
        },
        npc_dota_hero_ursa_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_ursa/agi/4.png", name = "npc_dota_hero_ursa_agi9", 
        },
        npc_dota_hero_ursa_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_ursa/agi/4.png", name = "npc_dota_hero_ursa_agi10", 
        },
        npc_dota_hero_ursa_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_ursa/agi/2.png", name = "npc_dota_hero_ursa_agi11", 
        },
        npc_dota_hero_ursa_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_ursa/agi/3.png", name = "npc_dota_hero_ursa_agi12", 
        },
        npc_dota_hero_ursa_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_ursa/agi/3.png", name = "npc_dota_hero_ursa_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_ursa_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_ursa/int/2.png", name = "npc_dota_hero_ursa_int6", 
        },
        npc_dota_hero_ursa_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_ursa/int/2.png", name = "npc_dota_hero_ursa_int7", 
        },
        npc_dota_hero_ursa_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_ursa/int/1.png", name = "npc_dota_hero_ursa_int8", 
        },
        npc_dota_hero_ursa_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_ursa/int/2.png", name = "npc_dota_hero_ursa_int9", 
        },
        npc_dota_hero_ursa_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_ursa/int/2.png", name = "npc_dota_hero_ursa_int10", 
        },
        npc_dota_hero_ursa_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_ursa/int/1.png", name = "npc_dota_hero_ursa_int11", 
        },  
        npc_dota_hero_ursa_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_ursa/int/3.png", name = "npc_dota_hero_ursa_int12", 
        },	
        npc_dota_hero_ursa_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_ursa/int/3.png", name = "npc_dota_hero_ursa_int13", 
        },
    },
    npc_dota_hero_weaver = {
        -------------------- STR -----------------------------------------------------------
        npc_dota_hero_weaver_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_weaver/str/2.png", name = "npc_dota_hero_weaver_str6", 
        },
        npc_dota_hero_weaver_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_weaver/str/4.png", name = "npc_dota_hero_weaver_str7", 
        },
        npc_dota_hero_weaver_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_weaver/str/1.png", name = "npc_dota_hero_weaver_str8", 
        },  
        npc_dota_hero_weaver_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_weaver/str/2.png", name = "npc_dota_hero_weaver_str9", 
        },
        npc_dota_hero_weaver_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_weaver/str/5.png", name = "npc_dota_hero_weaver_str10", 
        },
        npc_dota_hero_weaver_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_weaver/str/1.png", name = "npc_dota_hero_weaver_str11", 
        },
        npc_dota_hero_weaver_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_weaver/str/2.png", name = "npc_dota_hero_weaver_str12", 
        },
        npc_dota_hero_weaver_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_weaver/str/2.png", name = "npc_dota_hero_weaver_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        npc_dota_hero_weaver_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_weaver/agi/2.png", name = "npc_dota_hero_weaver_agi6", 
        },
        npc_dota_hero_weaver_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_weaver/agi/3.png", name = "npc_dota_hero_weaver_agi7", 
        },
        npc_dota_hero_weaver_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_weaver/agi/3.png", name = "npc_dota_hero_weaver_agi8", 
        },
        npc_dota_hero_weaver_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_weaver/agi/1.png", name = "npc_dota_hero_weaver_agi9", 
        },
        npc_dota_hero_weaver_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_weaver/agi/1.png", name = "npc_dota_hero_weaver_agi10", 
        },
        npc_dota_hero_weaver_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_weaver/agi/5.png", name = "npc_dota_hero_weaver_agi11", 
        },
        npc_dota_hero_weaver_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_weaver/agi/3.png", name = "npc_dota_hero_weaver_agi12", 
        },
        npc_dota_hero_weaver_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_weaver/agi/3.png", name = "npc_dota_hero_weaver_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        npc_dota_hero_weaver_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_weaver/int/2.png", name = "npc_dota_hero_weaver_int6", 
        },
        npc_dota_hero_weaver_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_weaver/int/1.png", name = "npc_dota_hero_weaver_int7", 
        },
        npc_dota_hero_weaver_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_weaver/int/3.png", name = "npc_dota_hero_weaver_int8", 
        },
        npc_dota_hero_weaver_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_weaver/int/2.png", name = "npc_dota_hero_weaver_int9", 
        },
        npc_dota_hero_weaver_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_weaver/int/1.png", name = "npc_dota_hero_weaver_int10", 
        },
        npc_dota_hero_weaver_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_weaver/int/1.png", name = "npc_dota_hero_weaver_int11", 
        },  
        npc_dota_hero_weaver_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_weaver/int/2.png", name = "npc_dota_hero_weaver_int12", 
        },	
        npc_dota_hero_weaver_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_weaver/int/2.png", name = "npc_dota_hero_weaver_int13", 
        },
    },
    npc_dota_hero_monkey_king = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_monkey_king_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_monkey_king/str/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_str6", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_monkey_king/str/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_str7", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_monkey_king/str/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_str8", 
        },  
        special_bonus_unique_npc_dota_hero_monkey_king_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_monkey_king/str/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_str9", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_monkey_king/str/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_str10", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_monkey_king/str/ti9_monkeyking_bananaphone_icon_png.png", name = "npc_dota_hero_monkey_king_str11", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_monkey_king/str/ti9_monkeyking_bananaphone_icon_png.png", name = "npc_dota_hero_monkey_king_str12", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_monkey_king/str/ti9_monkeyking_bananaphone_icon_png.png", name = "npc_dota_hero_monkey_king_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_monkey_king_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_monkey_king/agi/Boundless_Strike_icon.png", name = "npc_dota_hero_monkey_king_agi6", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_monkey_king/agi/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_agi7", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_monkey_king/agi/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_agi8", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_monkey_king/agi/Boundless_Strike_icon.png", name = "npc_dota_hero_monkey_king_agi9", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_monkey_king/agi/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_agi10", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_monkey_king/agi/Jingu_Mastery_icon.png", name = "npc_dota_hero_monkey_king_agi11", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_monkey_king/agi/Boundless_Strike_icon.png", name = "npc_dota_hero_monkey_king_agi12", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_monkey_king/agi/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_monkey_king_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_monkey_king/int/Boundless_Strike_icon.png", name = "npc_dota_hero_monkey_king_int6", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_monkey_king/int/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_int7", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_monkey_king/int/ti9_monkeyking_bananaphone_icon_png.png", name = "npc_dota_hero_monkey_king_int8", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_monkey_king/int/Boundless_Strike_icon.png", name = "npc_dota_hero_monkey_king_int9", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_monkey_king/int/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_int10", 
        },
        special_bonus_unique_npc_dota_hero_monkey_king_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_monkey_king/int/ti9_monkeyking_bananaphone_icon_png.png", name = "npc_dota_hero_monkey_king_int11", 
        },  
        special_bonus_unique_npc_dota_hero_monkey_king_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_monkey_king/int/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_int12", 
        },	
        special_bonus_unique_npc_dota_hero_monkey_king_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_monkey_king/int/Wukong's_Command_icon.png", name = "npc_dota_hero_monkey_king_int13", 
        },
    },
    npc_dota_hero_undying = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_undying_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_undying/str/Decay.png", name = "npc_dota_hero_undying_str6", 
        },
        special_bonus_unique_npc_dota_hero_undying_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_undying/str/Decay.png", name = "npc_dota_hero_undying_str7", 
        },
        special_bonus_unique_npc_dota_hero_undying_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_undying/str/Soul_Rip.png", name = "npc_dota_hero_undying_str8", 
        },  
        special_bonus_unique_npc_dota_hero_undying_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_undying/str/Decay.png", name = "npc_dota_hero_undying_str9", 
        },
        special_bonus_unique_npc_dota_hero_undying_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_undying/str/Tombstone.png", name = "npc_dota_hero_undying_str10", 
        },
        special_bonus_unique_npc_dota_hero_undying_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_undying/str/Soul_Rip.png", name = "npc_dota_hero_undying_str11", 
        },
        special_bonus_unique_npc_dota_hero_undying_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_undying/str/Decay.png", name = "npc_dota_hero_undying_str12", 
        },
        special_bonus_unique_npc_dota_hero_undying_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_undying/str/Tombstone.png", name = "npc_dota_hero_undying_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_undying_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_undying/agi/Decay.png", name = "npc_dota_hero_undying_agi6", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_undying/agi/Tombstone.png", name = "npc_dota_hero_undying_agi7", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi8", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi9", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi10", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi11", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi12", 
        },
        special_bonus_unique_npc_dota_hero_undying_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_undying/agi/Flesh_Golem.png", name = "npc_dota_hero_undying_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_undying_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_undying/int/Flesh_Golem.png", name = "npc_dota_hero_undying_int6", 
        },
        special_bonus_unique_npc_dota_hero_undying_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_undying/int/Flesh_Golem.png", name = "npc_dota_hero_undying_int7", 
        },
        special_bonus_unique_npc_dota_hero_undying_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_undying/int/Soul_Rip.png", name = "npc_dota_hero_undying_int8", 
        },
        special_bonus_unique_npc_dota_hero_undying_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_undying/int/Decay.png", name = "npc_dota_hero_undying_int9", 
        },
        special_bonus_unique_npc_dota_hero_undying_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_undying/int/Tombstone.png", name = "npc_dota_hero_undying_int10", 
        },
        special_bonus_unique_npc_dota_hero_undying_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_undying/int/Decay.png", name = "npc_dota_hero_undying_int11", 
        },  
        special_bonus_unique_npc_dota_hero_undying_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_undying/int/Flesh_Golem.png", name = "npc_dota_hero_undying_int12", 
        },	
        special_bonus_unique_npc_dota_hero_undying_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_undying/int/Decay.png", name = "npc_dota_hero_undying_int13", 
        },
    },
    npc_dota_hero_queenofpain = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_queenofpain_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str6", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str7", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str8", 
        },  
        special_bonus_unique_npc_dota_hero_queenofpain_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_queenofpain/str/Scream_of_Pain.png", name = "npc_dota_hero_queenofpain_str9", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str10", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str11", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_queenofpain/str/Blink.png", name = "npc_dota_hero_queenofpain_str12", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_queenofpain/str/5.png", name = "npc_dota_hero_queenofpain_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_queenofpain_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_queenofpain/agi/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_agi6", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_queenofpain/agi/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_agi7", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_queenofpain/agi/Scream_of_Pain.png", name = "npc_dota_hero_queenofpain_agi8", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_queenofpain/agi/Scream_of_Pain.png", name = "npc_dota_hero_queenofpain_agi9", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_queenofpain/agi/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_agi10", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_queenofpain/agi/Sonic_Wave.png", name = "npc_dota_hero_queenofpain_agi11", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_queenofpain/agi/5.png", name = "npc_dota_hero_queenofpain_agi12", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_queenofpain/agi/5.png", name = "npc_dota_hero_queenofpain_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_queenofpain_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_queenofpain/int/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_int6", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_queenofpain/int/Sonic_Wave.png", name = "npc_dota_hero_queenofpain_int7", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_queenofpain/int/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_int8", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_queenofpain/int/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_int9", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_queenofpain/int/Sonic_Wave.png", name = "npc_dota_hero_queenofpain_int10", 
        },
        special_bonus_unique_npc_dota_hero_queenofpain_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_queenofpain/int/Shadow_Strike.png", name = "npc_dota_hero_queenofpain_int11", 
        },  
        special_bonus_unique_npc_dota_hero_queenofpain_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_queenofpain/int/5.png", name = "npc_dota_hero_queenofpain_int12", 
        },	
        special_bonus_unique_npc_dota_hero_queenofpain_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_queenofpain/int/5.png", name = "npc_dota_hero_queenofpain_int13", 
        },
    },
    npc_dota_hero_obsidian_destroyer = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_obsidian_destroyer/str/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_str6", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_obsidian_destroyer/str/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_str7", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_obsidian_destroyer/str/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_str8", 
        },  
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_obsidian_destroyer/str/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_str9", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_obsidian_destroyer/str/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_str10", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_obsidian_destroyer/str/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_str11", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_obsidian_destroyer/str/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_str12", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_obsidian_destroyer/str/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_obsidian_destroyer/agi/obsidian_destroyer_sanity_eclipse_immortal_png.png", name = "npc_dota_hero_obsidian_destroyer_agi6", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi7", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi8", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi9", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi10", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi11", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi12", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_obsidian_destroyer/agi/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_obsidian_destroyer/int/Sanity's_Eclipse_icon.png", name = "npc_dota_hero_obsidian_destroyer_int6", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_obsidian_destroyer/int/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_int7", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_obsidian_destroyer/int/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_int8", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_obsidian_destroyer/int/Sanity's_Eclipse_icon.png", name = "npc_dota_hero_obsidian_destroyer_int9", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_obsidian_destroyer/int/Astral_Imprisonment_icon.png", name = "npc_dota_hero_obsidian_destroyer_int10", 
        },
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_obsidian_destroyer/int/Essence_Flux_icon.png", name = "npc_dota_hero_obsidian_destroyer_int11", 
        },  
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_obsidian_destroyer/int/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_int12", 
        },	
        special_bonus_unique_npc_dota_hero_obsidian_destroyer_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_obsidian_destroyer/int/Arcane_Orb_icon.png", name = "npc_dota_hero_obsidian_destroyer_int13", 
        },
    },
    npc_dota_hero_templar_assassin = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_templar_assassin_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str6", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str7", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str8", 
        },  
        special_bonus_unique_npc_dota_hero_templar_assassin_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str9", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str10", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str11", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str12", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_templar_assassin/str/Refraction_icon.png", name = "npc_dota_hero_templar_assassin_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_templar_assassin_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi6", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi7", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi8", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi9", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi10", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi11", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi12", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_templar_assassin/agi/Psi_Blades_icon.png", name = "npc_dota_hero_templar_assassin_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_templar_assassin_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int6", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int7", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int8", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int9", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int10", 
        },
        special_bonus_unique_npc_dota_hero_templar_assassin_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int11", 
        },  
        special_bonus_unique_npc_dota_hero_templar_assassin_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int12", 
        },	
        special_bonus_unique_npc_dota_hero_templar_assassin_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_templar_assassin/int/Meld_icon.png", name = "npc_dota_hero_templar_assassin_int13", 
        },
    },
    npc_dota_hero_tiny = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_tiny_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str6", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str7", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str8", 
        },  
        special_bonus_unique_npc_dota_hero_tiny_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str9", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str10", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str11", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str12", 
        },
        special_bonus_unique_npc_dota_hero_tiny_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_tiny/str/Grow_icon.png", name = "npc_dota_hero_tiny_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_tiny_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi6", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi9", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi7", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi10", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi8", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi11", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi12", 
        },
        special_bonus_unique_npc_dota_hero_tiny_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_tiny/agi/Tree_Grab_icon.png", name = "npc_dota_hero_tiny_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_tiny_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int6", 
        },
        special_bonus_unique_npc_dota_hero_tiny_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int9", 
        },
        special_bonus_unique_npc_dota_hero_tiny_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int7", 
        },
        special_bonus_unique_npc_dota_hero_tiny_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int10", 
        },
        special_bonus_unique_npc_dota_hero_tiny_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int8", 
        },
        special_bonus_unique_npc_dota_hero_tiny_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int11", 
        },  
        special_bonus_unique_npc_dota_hero_tiny_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int12", 
        },	
        special_bonus_unique_npc_dota_hero_tiny_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_tiny/int/Avalanche_icon.png", name = "npc_dota_hero_tiny_int13", 
        },
    },
    npc_dota_hero_bounty_hunter = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_bounty_hunter_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_bounty_hunter/str/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_str6", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_bounty_hunter/str/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_str7", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_bounty_hunter/str/Track_icon.png", name = "npc_dota_hero_bounty_hunter_str8", 
        },  
        special_bonus_unique_npc_dota_hero_bounty_hunter_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_bounty_hunter/str/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_str9", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_bounty_hunter/str/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_str10", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_bounty_hunter/str/Track_icon.png", name = "npc_dota_hero_bounty_hunter_str11", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_bounty_hunter/str/Track_icon.png", name = "npc_dota_hero_bounty_hunter_str12", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_bounty_hunter/str/Track_icon.png", name = "npc_dota_hero_bounty_hunter_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_bounty_hunter/agi/Track_icon.png", name = "npc_dota_hero_bounty_hunter_agi6", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi9", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi7", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi8", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_bounty_hunter/agi/Track_icon.png", name = "npc_dota_hero_bounty_hunter_agi10", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi11", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi12", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_bounty_hunter/agi/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_bounty_hunter_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_bounty_hunter/int/Track_icon.png", name = "npc_dota_hero_bounty_hunter_int6", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_bounty_hunter/int/Track_icon.png", name = "npc_dota_hero_bounty_hunter_int9", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_bounty_hunter/int/Track_icon.png", name = "npc_dota_hero_bounty_hunter_int7", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_bounty_hunter/int/Jinada_icon.png", name = "npc_dota_hero_bounty_hunter_int10", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_bounty_hunter/int/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_int8", 
        },
        special_bonus_unique_npc_dota_hero_bounty_hunter_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_bounty_hunter/int/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_int11", 
        },  
        special_bonus_unique_npc_dota_hero_bounty_hunter_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_bounty_hunter/int/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_int12", 
        },	
        special_bonus_unique_npc_dota_hero_bounty_hunter_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_bounty_hunter/int/Shuriken_Toss_icon.png", name = "npc_dota_hero_bounty_hunter_int13", 
        },
    },
    npc_dota_hero_faceless_void = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_faceless_void_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_faceless_void/str/2.png", name = "npc_dota_hero_faceless_void_str6", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_faceless_void/str/4.png", name = "npc_dota_hero_faceless_void_str7", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_faceless_void/str/3.png", name = "npc_dota_hero_faceless_void_str8", 
        },  
        special_bonus_unique_npc_dota_hero_faceless_void_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_faceless_void/str/2.png", name = "npc_dota_hero_faceless_void_str9",
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_faceless_void/str/4.png", name = "npc_dota_hero_faceless_void_str10", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_faceless_void/str/3.png", name = "npc_dota_hero_faceless_void_str11", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_faceless_void/str/4.png", name = "npc_dota_hero_faceless_void_str13", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_faceless_void/str/4.png", name = "npc_dota_hero_faceless_void_str12", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_faceless_void_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_faceless_void/agi/2.png", name = "npc_dota_hero_faceless_void_agi6", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_faceless_void/agi/2.png", name = "npc_dota_hero_faceless_void_agi9", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_faceless_void/agi/3.png", name = "npc_dota_hero_faceless_void_agi7", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_faceless_void/agi/4.png", name = "npc_dota_hero_faceless_void_agi8", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_faceless_void/agi/3.png", name = "npc_dota_hero_faceless_void_agi10", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_faceless_void/agi/4.png", name = "npc_dota_hero_faceless_void_agi11", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_faceless_void/agi/3.png", name = "npc_dota_hero_faceless_void_agi12", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_faceless_void/agi/3.png", name = "npc_dota_hero_faceless_void_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_faceless_void_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_faceless_void/int/2.png", name = "npc_dota_hero_faceless_void_int9", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_faceless_void/int/2.png", name = "npc_dota_hero_faceless_void_int6", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_faceless_void/int/4.png", name = "npc_dota_hero_faceless_void_int7", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_faceless_void/int/4.png", name = "npc_dota_hero_faceless_void_int10", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_faceless_void/int/3.png", name = "npc_dota_hero_faceless_void_int8", 
        },
        special_bonus_unique_npc_dota_hero_faceless_void_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_faceless_void/int/3.png", name = "npc_dota_hero_faceless_void_int11",
        },  
        special_bonus_unique_npc_dota_hero_faceless_void_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_faceless_void/int/2.png", name = "npc_dota_hero_faceless_void_int12", 
        },	
        special_bonus_unique_npc_dota_hero_faceless_void_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_faceless_void/int/2.png", name = "npc_dota_hero_faceless_void_int13", 
        },
    },
    npc_dota_hero_lich = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_lich_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_lich/str/4.png", name = "npc_dota_hero_lich_str6", 
        },
        special_bonus_unique_npc_dota_hero_lich_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_lich/str/2.png", name = "npc_dota_hero_lich_str7", 
        },
        special_bonus_unique_npc_dota_hero_lich_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_lich/int/2.png", name = "npc_dota_hero_lich_str8",
        },  
        special_bonus_unique_npc_dota_hero_lich_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_lich/str/4.png", name = "npc_dota_hero_lich_str9",
        },
        special_bonus_unique_npc_dota_hero_lich_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_lich/str/2.png", name = "npc_dota_hero_lich_str10", 
        },
        special_bonus_unique_npc_dota_hero_lich_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_lich/str/2.png", name = "npc_dota_hero_lich_str11", 
        },
        special_bonus_unique_npc_dota_hero_lich_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_lich/str/2.png", name = "npc_dota_hero_lich_str12", 
        },
        special_bonus_unique_npc_dota_hero_lich_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_lich/str/2.png", name = "npc_dota_hero_lich_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_lich_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_lich/agi/2.png", name = "npc_dota_hero_lich_agi6", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_lich/agi/3.png", name = "npc_dota_hero_lich_agi7", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_lich/agi/3.png", name = "npc_dota_hero_lich_agi8", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_lich/agi/2.png", name = "npc_dota_hero_lich_agi9", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_lich/agi/2.png", name = "npc_dota_hero_lich_agi10", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_lich/agi/3.png", name = "npc_dota_hero_lich_agi11", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_lich/agi/3.png", name = "npc_dota_hero_lich_agi12", 
        },
        special_bonus_unique_npc_dota_hero_lich_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_lich/agi/3.png", name = "npc_dota_hero_lich_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_lich_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_lich/int/2.png", name = "npc_dota_hero_lich_int6", 
        },
        special_bonus_unique_npc_dota_hero_lich_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_lich/int/3.png", name = "npc_dota_hero_lich_int7", 
        },
        special_bonus_unique_npc_dota_hero_lich_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_lich/int/4.png", name = "npc_dota_hero_lich_int8", 
        },
        special_bonus_unique_npc_dota_hero_lich_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_lich/int/2.png", name = "npc_dota_hero_lich_int9", 
        },
        special_bonus_unique_npc_dota_hero_lich_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_lich/int/3.png", name = "npc_dota_hero_lich_int10", 
        },
        special_bonus_unique_npc_dota_hero_lich_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_lich/int/4.png", name = "npc_dota_hero_lich_int11", 
        },
        special_bonus_unique_npc_dota_hero_lich_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_lich/int/4.png", name = "npc_dota_hero_lich_int12", 
        },	
        special_bonus_unique_npc_dota_hero_lich_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_lich/int/4.png", name = "npc_dota_hero_lich_int13", 
        },
    },
    npc_dota_hero_ember_spirit = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_ember_spirit_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_ember_spirit/str/2.png", name = "npc_dota_hero_ember_spirit_str6", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_ember_spirit/str/3.png", name = "npc_dota_hero_ember_spirit_str7", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_ember_spirit/str/4.png", name = "npc_dota_hero_ember_spirit_str8",
        },  
        special_bonus_unique_npc_dota_hero_ember_spirit_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_ember_spirit/str/2.png", name = "npc_dota_hero_ember_spirit_str9",
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_ember_spirit/str/3.png", name = "npc_dota_hero_ember_spirit_str10", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_ember_spirit/str/4.png", name = "npc_dota_hero_ember_spirit_str11", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_ember_spirit/str/3.png", name = "npc_dota_hero_ember_spirit_str12", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_ember_spirit/str/3.png", name = "npc_dota_hero_ember_spirit_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_ember_spirit_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_ember_spirit/agi/2.png", name = "npc_dota_hero_ember_spirit_agi6", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_ember_spirit/agi/3.png", name = "npc_dota_hero_ember_spirit_agi7", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_ember_spirit/agi/4.png", name = "npc_dota_hero_ember_spirit_agi8", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_ember_spirit/agi/2.png", name = "npc_dota_hero_ember_spirit_agi9", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_ember_spirit/agi/3.png", name = "npc_dota_hero_ember_spirit_agi10", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_ember_spirit/agi/4.png", name = "npc_dota_hero_ember_spirit_agi11", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_ember_spirit/agi/2.png", name = "npc_dota_hero_ember_spirit_agi12", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_ember_spirit/agi/2.png", name = "npc_dota_hero_ember_spirit_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_ember_spirit_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_ember_spirit/int/2.png", name = "npc_dota_hero_ember_spirit_int6", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_ember_spirit/int/3.png", name = "npc_dota_hero_ember_spirit_int7", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_ember_spirit/int/4.png", name = "npc_dota_hero_ember_spirit_int8", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_ember_spirit/int/2.png", name = "npc_dota_hero_ember_spirit_int9", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_ember_spirit/int/3.png", name = "npc_dota_hero_ember_spirit_int10", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_ember_spirit/int/4.png", name = "npc_dota_hero_ember_spirit_int11", 
        },
        special_bonus_unique_npc_dota_hero_ember_spirit_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_ember_spirit/int/4.png", name = "npc_dota_hero_ember_spirit_int12", 
        },	
        special_bonus_unique_npc_dota_hero_ember_spirit_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_ember_spirit/int/4.png", name = "npc_dota_hero_ember_spirit_int13", 
        },
    },
    npc_dota_hero_clinkz = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_clinkz_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str6", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str7",
        },
        special_bonus_unique_npc_dota_hero_clinkz_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_clinkz/str/1.png", name = "npc_dota_hero_clinkz_str8", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_clinkz/str/4.png", name = "npc_dota_hero_clinkz_str9", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str10",
        },
        special_bonus_unique_npc_dota_hero_clinkz_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str11", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str12", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_clinkz/str/3.png", name = "npc_dota_hero_clinkz_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_clinkz_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_clinkz/agi/1.png", name = "npc_dota_hero_clinkz_agi6", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_clinkz/agi/3.png", name = "npc_dota_hero_clinkz_agi7", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_clinkz/agi/1.png", name = "npc_dota_hero_clinkz_agi8", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_clinkz/agi/4.png", name = "npc_dota_hero_clinkz_agi9", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_clinkz/agi/3.png", name = "npc_dota_hero_clinkz_agi10", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_clinkz/agi/2.png", name = "npc_dota_hero_clinkz_agi11", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_clinkz/agi/2.png", name = "npc_dota_hero_clinkz_agi12",
        },
        special_bonus_unique_npc_dota_hero_clinkz_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_clinkz/agi/1.png", name = "npc_dota_hero_clinkz_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_clinkz_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_clinkz/int/2.png", name = "npc_dota_hero_clinkz_int6", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_clinkz/int/3.png", name = "npc_dota_hero_clinkz_int7", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_clinkz/int/1.png", name = "npc_dota_hero_clinkz_int8", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_clinkz/int/4.png", name = "npc_dota_hero_clinkz_int9", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_clinkz/int/3.png", name = "npc_dota_hero_clinkz_int10", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_clinkz/int/3.png", name = "npc_dota_hero_clinkz_int11", 
        },
        special_bonus_unique_npc_dota_hero_clinkz_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_clinkz/int/4.png", name = "npc_dota_hero_clinkz_int12", 
        },	
        special_bonus_unique_npc_dota_hero_clinkz_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_clinkz/int/4.png", name = "npc_dota_hero_clinkz_int13", 
        },
    },
    npc_dota_hero_witch_doctor = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_witch_doctor_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_witch_doctor/str/4.png", name = "npc_dota_hero_witch_doctor_str6", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_witch_doctor/str/2.png", name = "npc_dota_hero_witch_doctor_str7",
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_witch_doctor/str/3.png", name = "npc_dota_hero_witch_doctor_str8", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_witch_doctor/str/4.png", name = "npc_dota_hero_witch_doctor_str9", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_witch_doctor/str/2.png", name = "npc_dota_hero_witch_doctor_str10",
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_witch_doctor/str/4.png", name = "npc_dota_hero_witch_doctor_str11", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_witch_doctor/str/2.png", name = "npc_dota_hero_witch_doctor_str12", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_witch_doctor/str/2.png", name = "npc_dota_hero_witch_doctor_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_witch_doctor_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_witch_doctor/agi/4.png", name = "npc_dota_hero_witch_doctor_agi6", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_witch_doctor/agi/2.png", name = "npc_dota_hero_witch_doctor_agi7", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_witch_doctor/agi/2.png", name = "npc_dota_hero_witch_doctor_agi8", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_witch_doctor/agi/4.png", name = "npc_dota_hero_witch_doctor_agi9", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_witch_doctor/agi/2.png", name = "npc_dota_hero_witch_doctor_agi10", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_witch_doctor/agi/3.png", name = "npc_dota_hero_witch_doctor_agi11", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_witch_doctor/agi/3.png", name = "npc_dota_hero_witch_doctor_agi12", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_witch_doctor/agi/1.png", name = "npc_dota_hero_witch_doctor_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_witch_doctor_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_witch_doctor/int/4.png", name = "npc_dota_hero_witch_doctor_int6", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_witch_doctor/int/2.png", name = "npc_dota_hero_witch_doctor_int7", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_witch_doctor/int/3.png", name = "npc_dota_hero_witch_doctor_int8", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_witch_doctor/int/4.png", name = "npc_dota_hero_witch_doctor_int9", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_witch_doctor/int/2.png", name = "npc_dota_hero_witch_doctor_int10", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_witch_doctor/int/3.png", name = "npc_dota_hero_witch_doctor_int11", 
        },
        special_bonus_unique_npc_dota_hero_witch_doctor_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_witch_doctor/int/4.png", name = "npc_dota_hero_witch_doctor_int12", 
        },	
        special_bonus_unique_npc_dota_hero_witch_doctor_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_witch_doctor/int/4.png", name = "npc_dota_hero_witch_doctor_int13", 
        },
    },
    npc_dota_hero_void_spirit = {
        -------------------- STR -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_void_spirit_str6 = {
            place = {"str 6"}, url = "/npc_dota_hero_void_spirit/str/3.png", name = "npc_dota_hero_void_spirit_str6", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str7 = {
            place = {"str 7"}, url = "/npc_dota_hero_void_spirit/str/2.png", name = "npc_dota_hero_void_spirit_str7",
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str8 = {
            place = {"str 8"}, url = "/npc_dota_hero_void_spirit/str/4.png", name = "npc_dota_hero_void_spirit_str8", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str9 = {
            place = {"str 9"}, url = "/npc_dota_hero_void_spirit/str/3.png", name = "npc_dota_hero_void_spirit_str9", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str10 = {
            place = {"str 10"}, url = "/npc_dota_hero_void_spirit/str/2.png", name = "npc_dota_hero_void_spirit_str10",
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str11 = {
            place = {"str 11"}, url = "/npc_dota_hero_void_spirit/str/3.png", name = "npc_dota_hero_void_spirit_str11", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str12 = {
            place = {"str 12"}, url = "/npc_dota_hero_void_spirit/str/3.png", name = "npc_dota_hero_void_spirit_str12", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_str13 = {
            place = {"str 13"}, url = "/npc_dota_hero_void_spirit/str/3.png", name = "npc_dota_hero_void_spirit_str13", 
        },
        -------------------- AGI -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_void_spirit_agi6 = {
            place = {"agi 6"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi6", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi7 = {
            place = {"agi 7"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi7", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi8 = {
            place = {"agi 8"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi8", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi9 = {
            place = {"agi 9"}, url = "/npc_dota_hero_void_spirit/str/4.png", name = "npc_dota_hero_void_spirit_agi9", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi10 = {
            place = {"agi 10"}, url = "/npc_dota_hero_void_spirit/str/4.png", name = "npc_dota_hero_void_spirit_agi10", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi11 = {
            place = {"agi 11"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi11", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi12 = {
            place = {"agi 12"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi12", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_agi13 = {
            place = {"agi 13"}, url = "/npc_dota_hero_void_spirit/agi/4.png", name = "npc_dota_hero_void_spirit_agi13", 
        },
        -------------------- INT -----------------------------------------------------------
        special_bonus_unique_npc_dota_hero_void_spirit_int6 = {
            place = {"int 6"}, url = "/npc_dota_hero_void_spirit/int/3.png", name = "npc_dota_hero_void_spirit_int6", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int7 = {
            place = {"int 7"}, url = "/npc_dota_hero_void_spirit/int/2.png", name = "npc_dota_hero_void_spirit_int7", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int8 = {
            place = {"int 8"}, url = "/npc_dota_hero_void_spirit/int/4.png", name = "npc_dota_hero_void_spirit_int8", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int9 = {
            place = {"int 9"}, url = "/npc_dota_hero_void_spirit/int/3.png", name = "npc_dota_hero_void_spirit_int9", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int10 = {
            place = {"int 10"}, url = "/npc_dota_hero_void_spirit/int/2.png", name = "npc_dota_hero_void_spirit_int10", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int11 = {
            place = {"int 11"}, url = "/npc_dota_hero_void_spirit/int/4.png", name = "npc_dota_hero_void_spirit_int11", 
        },
        special_bonus_unique_npc_dota_hero_void_spirit_int12 = {
            place = {"int 12"}, url = "/npc_dota_hero_void_spirit/int/2.png", name = "npc_dota_hero_void_spirit_int12", 
        },	
        special_bonus_unique_npc_dota_hero_void_spirit_int13 = {
            place = {"int 13"}, url = "/npc_dota_hero_void_spirit/int/2.png", name = "npc_dota_hero_void_spirit_int13", 
        },
    },
}