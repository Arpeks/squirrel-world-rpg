
-- Constants for the quests rpg system.

-- Distance between the player and the quest that triggers the aura.
RDA_QUEST_NPC_AURA_RADIUS = 350

-- Distance in which the kill counts are checked.
RDA_QUEST_KILL_COUNTS_RADIUS = 1000

-- Enum for the different map overlays.
MAP_OVERLAY_LOCATION_FOREST         = 1
MAP_OVERLAY_LOCATION_FOREST_BOSS    = 2
MAP_OVERLAY_LOCATION_VILLAGE        = 3
MAP_OVERLAY_LOCATION_VILLAGE_BOSS   = 4
MAP_OVERLAY_LOCATION_MINES          = 5
MAP_OVERLAY_LOCATION_MINES_BOSS     = 6
MAP_OVERLAY_LOCATION_DUST           = 7
MAP_OVERLAY_LOCATION_DUST_BOSS      = 8
MAP_OVERLAY_LOCATION_CEMETERY       = 9
MAP_OVERLAY_LOCATION_CEMETERY_BOSS  = 10
MAP_OVERLAY_LOCATION_SWAMP          = 11
MAP_OVERLAY_LOCATION_SWAMP_BOSS     = 12
MAP_OVERLAY_LOCATION_SNOW           = 13
MAP_OVERLAY_LOCATION_SNOW_BOSS      = 14
MAP_OVERLAY_LOCATION_ANCIENT        = 15
MAP_OVERLAY_LOCATION_ANCIENT_BOSS   = 16
MAP_OVERLAY_LOCATION_MAGMA          = 17
MAP_OVERLAY_LOCATION_MAGMA_BOSS     = 18
MAP_OVERLAY_LOCATION_BOSS_AGHANIM   = 19
MAP_OVERLAY_LOCATION_BOSS_WYVERN    = 20
MAP_OVERLAY_LOCATION_BOSS_LUFIGAN   = 21
MAP_OVERLAY_LOCATION_BOSS_URSA      = 22
MAP_OVERLAY_LOCATION_BOSS_SQUIRREL  = 23
MAP_OVERLAY_LOCATION_ROSHAN         = 24
MAP_OVERLAY_LOCATION_BOSS_ANTI_MAGE = 25
MAP_OVERLAY_LOCATION_ARENA          = 26
MAP_OVERLAY_LOCATION_ENEMY_BASE     = 27
MAP_OVERLAY_LOCATION_MID            = 28

-- Enum for the different quest flags.
RDA_QUEST_FLAG_KILL_UNIT                        = 1
RDA_QUEST_FLAG_GET_CLOSE_TO_TARGET              = 2
RDA_QUEST_FLAG_FIND_ITEMS                       = 3
RDA_QUEST_FLAG_FIND_RUNES                       = 4
RDA_QUEST_FLAG_ITEMS_DROP                       = 5
RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION    = 6
RDA_QUEST_FLAG_IMPACTS_WITH_ABILITY_QUEST       = 7
RDA_QUEST_FLAG_TYPE_YELLOW                      = 8
RDA_QUEST_FLAG_TYPE_BLUE                        = 9
RDA_QUEST_FLAG_LOCKED                           = 10
RDA_QUEST_FLAG_RANDOM_ONE                       = 11
RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT               = 12
RDA_QUEST_FLAG_DEFEND_WAVES                     = 13
RDA_QUEST_FLAG_LEVEL_UP                         = 14
RDA_QUEST_FLAG_DEAL_DAMAGE                      = 15
RDA_QUEST_FLAG_TAKE_DAMAGE                      = 16
RDA_QUEST_FLAG_PERFORM_ANY_OTHER_QUEST          = 17
RDA_QUEST_FLAG_IMPROVE_ITEM_FORGE               = 18
RDA_QUEST_FLAG_CUSTOM                           = 19
RDA_QUEST_FLAG_DISTANCE_TRAVELED                = 20
RDA_QUEST_FLAG_TYPE_FOR_ONE_ONLY                = 21
RDA_QUEST_FLAG_RANDOM_TWO                       = 22
RDA_QUEST_FLAG_ITEM_REWARD_CHOICE               = 23
RDA_QUEST_FLAG_ITEM_REWARD                      = 24
RDA_QUEST_FLAG_FIND_RUNE_DOUBLEDAMAGE           = 25
RDA_QUEST_FLAG_FIND_RUNE_HASTE                  = 26
RDA_QUEST_FLAG_FIND_RUNE_INVISIBILITY           = 27
RDA_QUEST_FLAG_FIND_RUNE_REGENERATION           = 28
RDA_QUEST_FLAG_FIND_RUNE_ARCANE                 = 29
RDA_QUEST_FLAG_FIND_RUNE_XP                     = 30
RDA_QUEST_FLAG_FIND_RUNE_BOUNTY                 = 31
RDA_QUEST_FLAG_KILL_UNIT_GLOBAL                 = 32
RDA_QUEST_FLAG_DAMAGE_TARGET                    = 33
RDA_QUEST_FLAG_RECEIVED_DAMAGE                  = 34
RDA_QUEST_FLAG_ORIGINAL_DAMAGE                  = 35
RDA_QUEST_FLAG_GAME_EVENT_UNLOCK                = 36
RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE         = 37
RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION     = 38
RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION = 39
RDA_QUEST_FLAG_CAN_NOT_UNLOCK                   = 40
RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_0          = 41
RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_1          = 42
RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_2          = 43
RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_3          = 44
RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_4          = 45
RDA_QUEST_FLAG_EARNED_GOLD                      = 46
RDA_QUEST_FLAG_SPENT_GOLD                       = 47
RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE         = 48

-- Enum for the different quest unlock events.
RDA_QUEST_UNLOCK_EVENT_FLAG_LARGE_ORE_SPAWNED   = 1
RDA_QUEST_UNLOCK_EVENT_FLAG_INVOKER_KILLED      = 2
RDA_QUEST_UNLOCK_EVENT_FLAG_PUDGE_KILLED        = 3
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_FURION = 4
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_PUDGE = 5
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_EARTH = 6
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_NIX = 7
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_CEMETERY = 8
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_VENO = 9
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_TINY = 10
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_SNEP = 11
RDA_QUEST_UNLOCK_EVENT_FLAG_EVERYONE_COMPLETED_KILL_DOOM = 12
RDA_QUEST_UNLOCK_EVENT_FLAG_FIND_MAGMA_LETTER            = 13


-- Enum for the different quest indices.
RDA_QUEST_INDEX_MAIN_CATEGORY   = 1
RDA_QUEST_INDEX_FOREST          = 2
RDA_QUEST_INDEX_VILLAGE         = 3  
RDA_QUEST_INDEX_MINES           = 4
RDA_QUEST_INDEX_DUST            = 5
RDA_QUEST_INDEX_CEMETERY        = 6
RDA_QUEST_INDEX_SWAMP           = 7
RDA_QUEST_INDEX_SNOW            = 8
RDA_QUEST_INDEX_ANCIENT         = 9
RDA_QUEST_INDEX_MAGMA           = 10
RDA_QUEST_INDEX_MIDDLE          = 11
RDA_QUEST_INDEX_MIDDLE_2        = 12
RDA_QUEST_INDEX_NPC_24          = 13
RDA_QUEST_INDEX_DRAGON_ZONE     = 14
RDA_QUEST_INDEX_VIP_ZONE        = 15
RDA_QUEST_INDEX_VIP_ZONE_PLAYER_0 = 16
RDA_QUEST_INDEX_VIP_ZONE_PLAYER_1 = 17
RDA_QUEST_INDEX_VIP_ZONE_PLAYER_2 = 18
RDA_QUEST_INDEX_VIP_ZONE_PLAYER_3 = 19
RDA_QUEST_INDEX_VIP_ZONE_PLAYER_4 = 20
RDA_QUEST_INDEX_LOCATION_SPAWN  = 21
RDA_QUEST_MAGMA_SECRET          = 22

-- Prices for upgrading items.
ITEM_UPGRADE_PRICE_FOREST       = 5000
ITEM_UPGRADE_PRICE_VILLAGE      = 10000
ITEM_UPGRADE_PRICE_MINES        = 20000
ITEM_UPGRADE_PRICE_DUST         = 30000
ITEM_UPGRADE_PRICE_CEMETERY     = 40000
ITEM_UPGRADE_PRICE_SWAMP        = 50000
ITEM_UPGRADE_PRICE_SNOW         = 75000
ITEM_UPGRADE_PRICE_ANCIENT      = 100000
ITEM_UPGRADE_PRICE_MAGMA        = 180000

-- Table for storing experience rewards.
local RDA_QUEST_EXPERIENCE_REWARD = class({})

-- Function for getting the experience reward for a quest based on the price.
function RDA_QUEST_EXPERIENCE_REWARD:VeryLow( price )         return price * 0.15 end
function RDA_QUEST_EXPERIENCE_REWARD:Low( price )             return price * 0.3 end
function RDA_QUEST_EXPERIENCE_REWARD:Medium( price )          return price * 0.5 end
function RDA_QUEST_EXPERIENCE_REWARD:High( price )            return price * 0.75 end
function RDA_QUEST_EXPERIENCE_REWARD:VeryHigh( price )        return price * 1.1 end
function RDA_QUEST_EXPERIENCE_REWARD:Legendary( price )       return price * 1.75 end

-- Table for storing gold rewards.
local RDA_QUEST_GOLD_REWARD = class({})

-- Function for getting the gold reward for a quest based on the price.
function RDA_QUEST_GOLD_REWARD:VeryLow( price )               return price * 0.2 end
function RDA_QUEST_GOLD_REWARD:Low( price )                   return price * 0.4 end
function RDA_QUEST_GOLD_REWARD:Medium( price )                return price * 0.7 end
function RDA_QUEST_GOLD_REWARD:High( price )                  return price * 1.0 end
function RDA_QUEST_GOLD_REWARD:VeryHigh( price )              return price * 1.5 end
function RDA_QUEST_GOLD_REWARD:Legendary( price )             return price * 2.0 end

REWARD_TALENT_EXPERIENCE_FOREST     = 50
REWARD_TALENT_EXPERIENCE_VILLAGE    = 80
REWARD_TALENT_EXPERIENCE_MINES      = 110
REWARD_TALENT_EXPERIENCE_DUST       = 140
REWARD_TALENT_EXPERIENCE_CEMETERY   = 170
REWARD_TALENT_EXPERIENCE_SWAMP      = 200
REWARD_TALENT_EXPERIENCE_SNOW       = 230
REWARD_TALENT_EXPERIENCE_ANCIENT    = 260
REWARD_TALENT_EXPERIENCE_MAGMA      = 290

REWARD_TALENT_EXPERIENCE_BLUE_VERY_LOW    = 20
REWARD_TALENT_EXPERIENCE_BLUE_LOW         = 40
REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM      = 70
REWARD_TALENT_EXPERIENCE_BLUE_HIGH        = 100
REWARD_TALENT_EXPERIENCE_BLUE_VERY_HIGH   = 120
REWARD_TALENT_EXPERIENCE_BLUE_LEGENDARY   = 300

REWARD_TALENT_EXPERIENCE_MIDDLE_1         = 50
REWARD_TALENT_EXPERIENCE_MIDDLE_2         = 60
REWARD_TALENT_EXPERIENCE_MIDDLE_3         = 70
REWARD_TALENT_EXPERIENCE_MIDDLE_4         = 80
REWARD_TALENT_EXPERIENCE_MIDDLE_5         = 90
REWARD_TALENT_EXPERIENCE_MIDDLE_6         = 100
REWARD_TALENT_EXPERIENCE_MIDDLE_7         = 110
REWARD_TALENT_EXPERIENCE_MIDDLE_8         = 120
REWARD_TALENT_EXPERIENCE_MIDDLE_9         = 130
REWARD_TALENT_EXPERIENCE_MIDDLE_10        = 140

REWARD_EXPERIENCE_MIDDLE_1      = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_FOREST )
REWARD_EXPERIENCE_MIDDLE_2      = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST )
REWARD_EXPERIENCE_MIDDLE_3      = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_VILLAGE )
REWARD_EXPERIENCE_MIDDLE_4      = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES )
REWARD_EXPERIENCE_MIDDLE_5      = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_DUST )
REWARD_EXPERIENCE_MIDDLE_6      = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY )
REWARD_EXPERIENCE_MIDDLE_7      = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_SWAMP )
REWARD_EXPERIENCE_MIDDLE_8      = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_SNOW )
REWARD_EXPERIENCE_MIDDLE_9      = RDA_QUEST_EXPERIENCE_REWARD:VeryHigh( ITEM_UPGRADE_PRICE_ANCIENT )
REWARD_EXPERIENCE_MIDDLE_10     = RDA_QUEST_EXPERIENCE_REWARD:VeryHigh( ITEM_UPGRADE_PRICE_MAGMA )

REWARD_GOLD_MIDDLE_1            = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_FOREST )
REWARD_GOLD_MIDDLE_2            = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST )
REWARD_GOLD_MIDDLE_3            = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_VILLAGE )
REWARD_GOLD_MIDDLE_4            = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES )
REWARD_GOLD_MIDDLE_5            = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_DUST )
REWARD_GOLD_MIDDLE_6            = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY )
REWARD_GOLD_MIDDLE_7            = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_SWAMP )
REWARD_GOLD_MIDDLE_8            = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_SNOW )
REWARD_GOLD_MIDDLE_9            = RDA_QUEST_GOLD_REWARD:VeryHigh( ITEM_UPGRADE_PRICE_ANCIENT )
REWARD_GOLD_MIDDLE_10           = RDA_QUEST_GOLD_REWARD:VeryHigh( ITEM_UPGRADE_PRICE_MAGMA )

LOCALIZATION_STRING_MID_QUEST_DESCRIPTION           = "quests_rpg_constants_mid_quest_description"
LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP         = "quests_rpg_constants_mid_task_kill_any_creep"
LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS          = "quests_rpg_constants_mid_task_kill_any_boss"
LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES           = "quests_rpg_constants_mid_task_defend_waves"
LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP   = "quests_rpg_constants_mid_task_kill_small_mid_creep"
LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP     = "quests_rpg_constants_mid_task_kill_big_mid_creep"
LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP     = "quests_rpg_constants_mid_task_kill_any_mid_creep"
LOCALIZATION_STRING_MID_TASK_ITEMS_DROP             = "quests_rpg_constants_mid_task_items_drop"
LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE            = "quests_rpg_constants_mid_task_deal_damage"
LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE            = "quests_rpg_constants_mid_task_take_damage"

RDA_QUEST_RPG_DATA_ARRAY = {
    [RDA_QUEST_INDEX_MAIN_CATEGORY] = { -- MAIN -------------------------------------------
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_YELLOW] = true,
        },
        [1] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
            }, -- flags
            ["npc_giver"] = "npc_1", -- UnitName
            ["npc_receiver"] = "npc_28", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_FOREST, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"forest_creep_mini_2", "forest_creep_big_2"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"forest_creep_big_1", "forest_creep_mini_1"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"forest_creep_big_3", "forest_creep_mini_3"},
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+15"},
                ["targets"] = {"forest_creep_big_1", "forest_creep_mini_1","forest_creep_mini_2","forest_creep_big_2","forest_creep_big_3","forest_creep_mini_3"},
            },
        },
        [2] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_28",
            ["npc_receiver"] = "npc_5",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_FOREST,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_FOREST]            = { 5 },
                [RDA_QUEST_INDEX_VILLAGE]           = { 1, 3 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 1 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_forest_boss", "npc_forest_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"forest_creep_big_1", "forest_creep_mini_1","forest_creep_mini_2","forest_creep_big_2","forest_creep_big_3","forest_creep_mini_3"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            },
        },
        [3] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_5", -- UnitName
            ["npc_receiver"] = "npc_7", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_VILLAGE, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"village_creep_1"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"village_creep_2"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"village_creep_3"},
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+20"},
                ["targets"] = {"village_creep_1", "village_creep_2","village_creep_3"},
            },
        },
        [4] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_7",
            ["npc_receiver"] = "npc_10",
            ["experience"] = ITEM_UPGRADE_PRICE_VILLAGE / 2,
            ["gold"] = ITEM_UPGRADE_PRICE_VILLAGE,
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_VILLAGE,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_MINES]             = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 2 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_village_boss", "npc_village_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"village_creep_1", "village_creep_2","village_creep_3"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE,
            },
        },
        [5] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_10", -- UnitName
            ["npc_receiver"] = "npc_11", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MINES, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"mines_creep_1"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"mines_creep_2"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"mines_creep_3"},
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+20"},
                ["targets"] = {"mines_creep_1", "mines_creep_2","mines_creep_3"},
            },
        },
        [6] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_11",
            ["npc_receiver"] = "npc_13",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MINES,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_DUST]              = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 3 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_mines_boss", "npc_mines_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"mines_creep_1", "mines_creep_2","mines_creep_3"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES,
            },
        },
        [7] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_13", -- UnitName
            ["npc_receiver"] = "npc_17", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_DUST, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_DUST,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"dust_creep_1","dust_creep_2"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"dust_creep_3","dust_creep_4"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"dust_creep_5","dust_creep_6"},
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+20"},
                ["targets"] = {"dust_creep_1","dust_creep_2","dust_creep_3","dust_creep_4","dust_creep_5","dust_creep_6"},
            },
        },
        [8] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_17",
            ["npc_receiver"] = "npc_15",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_DUST,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_CEMETERY]          = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 4 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_dust_boss", "npc_dust_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_DUST_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"dust_creep_1","dust_creep_2","dust_creep_3","dust_creep_4","dust_creep_5","dust_creep_6"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_DUST,
            },
        },
        [9] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_15", -- UnitName
            ["npc_receiver"] = "npc_22", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_CEMETERY, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_CEMETERY,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"cemetery_creep_1","cemetery_creep_2"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"cemetery_creep_3","cemetery_creep_4"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+20"},
                ["targets"] = {"cemetery_creep_1","cemetery_creep_2","cemetery_creep_3","cemetery_creep_4"},
            },
        },
        [10] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_22",
            ["npc_receiver"] = "npc_21",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_CEMETERY,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_SWAMP]             = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 5 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_cemetery_boss", "npc_cemetery_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_CEMETERY_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"cemetery_creep_1","cemetery_creep_2","cemetery_creep_3","cemetery_creep_4"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_CEMETERY,
            },
        },
        [11] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_21", -- UnitName
            ["npc_receiver"] = "npc_25", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_SWAMP, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_SWAMP,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"swamp_creep_1","swamp_creep_2"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"swamp_creep_3","swamp_creep_4"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+20"},
                ["targets"] = {"swamp_creep_1","swamp_creep_2","swamp_creep_3","swamp_creep_4"},
            },
        },
        [12] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_25",
            ["npc_receiver"] = "npc_27",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_SWAMP,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_SNOW]              = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 6 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_swamp_boss", "npc_swamp_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_SWAMP_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"swamp_creep_1","swamp_creep_2","swamp_creep_3","swamp_creep_4"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_SWAMP,
            },
        },
        [13] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            }, -- flags
            ["npc_giver"] = "npc_27", -- UnitName
            ["npc_receiver"] = "npc_30", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_SNOW, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_SNOW,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"snow_creep_1","snow_creep_2","snow_creep_3","snow_creep_4"},
            },
        },
        [14] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_30",
            ["npc_receiver"] = "npc_31",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_SNOW,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_ANCIENT]           = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 7 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_snow_boss", "npc_snow_boss_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_SNOW_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"snow_creep_1","snow_creep_2","snow_creep_3","snow_creep_4"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_SNOW,
            },
        },
        [15] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_31", -- UnitName
            ["npc_receiver"] = "npc_32", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_ANCIENT, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_ANCIENT,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"last_creep_1","last_creep_2","last_creep_3","last_creep_4"},
            },
        },
        [16] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_32",
            ["npc_receiver"] = "npc_34",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_ANCIENT,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_ANCIENT_BOSS,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MAIN_CATEGORY]     = {"+1"},
                [RDA_QUEST_INDEX_NPC_24]        = { 1 },
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 8 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_boss_location8", "npc_boss_location8_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_ANCIENT_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"last_creep_1","last_creep_2","last_creep_3","last_creep_4"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_ANCIENT,
            },
        },
        [17] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- flags
            ["npc_giver"] = "npc_34", -- UnitName
            ["npc_receiver"] = "npc_35", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MAGMA, -- taltntExperience
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_MAGMA,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"magma_creep_1","magma_creep_2"},
            },
        },
        [18] = { --------------------------------------------------------------------------------------------------------------------------
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            },
            ["npc_giver"] = "npc_35",
            ["npc_receiver"] = "npc_35",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MAGMA,
            ["unlock"] = {
                [RDA_QUEST_INDEX_LOCATION_SPAWN]    = { 9 },
            },
            [1] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_boss_magma", "npc_boss_magma_fake"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MAGMA_BOSS,
            },
            [2] = { 
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                },
                ["amount"] = {20, "+10"},
                ["targets"] = {"magma_creep_1","magma_creep_2"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MAGMA,
            },
        },
    },
    
    [RDA_QUEST_INDEX_FOREST] = { -- Синие Локация Лес
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Шрайны
            ["flags"] = {}, --"locked"
            ["npc_giver"] = "npc_1",
            ["npc_receiver"] = "npc_3",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            ["unlock"] = {
                [RDA_QUEST_INDEX_FOREST] = {"+1"},
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_GET_CLOSE_TO_TARGET] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"goodguys_healers_1"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_GET_CLOSE_TO_TARGET] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"goodguys_healers_2"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_GET_CLOSE_TO_TARGET] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"goodguys_healers_3"},
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_GET_CLOSE_TO_TARGET] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"goodguys_healers_4"},
            },
        },
        [2] = { -- Голубые кристаллы
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, -- "locked"
            ["npc_giver"] = "npc_3",
            ["npc_receiver"] = "npc_3",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {5, "+0"},
                ["targets"] = {"item_quest_blue_stone"},
            },
        },
        [3] = { -- Руна
            ["flags"] = {}, --"locked"
            ["npc_giver"] = "npc_2",
            ["npc_receiver"] = "npc_2",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            ["unlock"] = {
                [RDA_QUEST_INDEX_FOREST] = {"+1"},
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_RUNES] = true,
                }, -- flags
                ["amount"] = {1, "+1"},
            },
        },
        [4] = { -- убить белку
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_2",
            ["npc_receiver"] = "npc_2",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"belka"},
            },
        },
        [5] = { -- Посох фуриона
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            },
            ["npc_giver"] = "npc_5",
            ["npc_receiver"] = "npc_5",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_reapers_vessel_lua",
                },
            },
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_FOREST_BOSS,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"item_quest_furion_staff"},
                ["drop_from"] = {"npc_forest_boss", "npc_forest_boss_fake"},
                ["drop_chance"] = 75,
            },
        },
    },

    [RDA_QUEST_INDEX_VILLAGE] = { -- Синие Локация Деревня
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Освободить заложников
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            },
            ["npc_giver"] = "npc_6",
            ["npc_receiver"] = "npc_6",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE,
            ["unlock"] = {
                [RDA_QUEST_INDEX_VILLAGE] = {"+1"},
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_IMPACTS_WITH_ABILITY_QUEST] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_crystal_maiden_quest_1"},
                ["functions"] = {
                    ["OnChannelFinish"] = "OnHostageReleased",
                },
                ["channel_time"] = 3,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_IMPACTS_WITH_ABILITY_QUEST] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_crystal_maiden_quest_2"},
                ["functions"] = {
                    ["OnChannelFinish"] = "OnHostageReleased",
                },
                ["channel_time"] = 3,
            },
        },
        [2] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_6",
            ["npc_receiver"] = "npc_6",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {5, "+0"},
                ["targets"] = {"npc_breakingcrate_quest"},
            },
        },
        [3] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_6",
            ["npc_receiver"] = "npc_6",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_VILLAGE,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {6, "+4"},
                ["targets"] = {"item_quest_assasin_knife"},
                ["drop_from"] = {"village_creep_2"},
                ["drop_chance"] = 40,
            },
        },
    },

    [RDA_QUEST_INDEX_MINES] = { -- Синие Локация Шахты
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Выбить драгоценные камни из шахтеров
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_14",
            ["npc_receiver"] = "npc_14",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MINES]     = {"+1"},
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {7, "+3"},
                ["targets"] = {"npc_mines_ore_quest", "npc_mines_ore_quest_mega"},
            },
        },
        [2] = { -- Использовать кирку
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_14",
            ["npc_receiver"] = "npc_14",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"item_smithy_pickaxe"},
            },
        },
        [3] = { -- убить мега кучу
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_GAME_EVENT_UNLOCK] = true,
            }, --"locked"
            ["unlock_event_flags"] ={
                [RDA_QUEST_UNLOCK_EVENT_FLAG_LARGE_ORE_SPAWNED] = true,
            },
            ["npc_giver"] = "npc_12",
            ["npc_receiver"] = "npc_12",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Legendary( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Legendary( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LEGENDARY,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_MINES,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_mines_ore_quest_mega"},
            },
        },
    },

    [RDA_QUEST_INDEX_DUST] = { -- Синие Локация Пустыня
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Найди фрагменты древних письмен
            ["flags"] = {
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_17",
            ["npc_receiver"] = "npc_17",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_DUST,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"item_quest_ancient_rune"},
                ["drop_from"] = {"dust_creep_1","dust_creep_2"},
                ["drop_chance"] = 50,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"item_quest_lizard_tail"},
                ["drop_from"] = {"dust_creep_3","dust_creep_4"},
                ["drop_chance"] = 50,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"item_quest_golem_shard"},
                ["drop_from"] = {"dust_creep_5","dust_creep_6"},
                ["drop_chance"] = 50,
            },
        },
        [2] = { -- Найди фрагменты древних письмен
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_17",
            ["npc_receiver"] = "npc_17",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_RUNES] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_BOUNTY] = true,
                }, -- flags
                ["amount"] = {1, "+1"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_RUNES] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_XP] = true,
                }, -- flags
                ["amount"] = {1, "+1"},
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_RUNES] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_ARCANE] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_REGENERATION] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_INVISIBILITY] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_HASTE] = true,
                    [RDA_QUEST_FLAG_FIND_RUNE_DOUBLEDAMAGE] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
            },
        },
        [3] = { -- Найди фрагменты древних письмен
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_17",
            ["npc_receiver"] = "npc_17",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_HIGH,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_DUST,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"item_quest_scarab"},
            },
        },
    },

    [RDA_QUEST_INDEX_CEMETERY] = { -- Синие Локация Кладбища
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Исследовать могилы
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_19",
            ["npc_receiver"] = "npc_19",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_CEMETERY,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_IMPACTS_WITH_ABILITY_QUEST] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_tombstone_quest", "item_quest_lost_letter"},
                ["functions"] = {
                    ["OnChannelFinish"] = "OnTombstoneChannelFinish",
                },
                ["channel_time"] = 2,
            },
        },
        [2] = { -- Убить секретного босса кладбища
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_19",
            ["npc_receiver"] = "npc_19",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_HIGH,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_CEMETERY,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_quest_cemetery_secret_boss"},
                ["functions"] = {
                    ["OnEntityKilled"] = "SpawnBossNecro",
                },
            },
        },
    },

    [RDA_QUEST_INDEX_SWAMP] = { -- Синие Локация Вода
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Найти настоящую нагу
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_26",
            ["npc_receiver"] = "npc_26",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_SWAMP,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_IMPACTS_WITH_ABILITY_QUEST] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"npc_naga_quest_1","npc_naga_quest_2","npc_naga_quest_3","npc_naga_quest_4"},
                ["functions"] = {
                    ["OnAcceptance"] = "OnNagaAcceptance",
                    ["OnCompletion"] = "OnNagaCompletion",
                    ["OnChannelFinish"] = "OnNagaChannelFinish",
                },
                ["channel_time"] = 3,
            },
        },
        [2] = { -- Получить рыбу за убийство крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_26",
            ["npc_receiver"] = "npc_26",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_HIGH,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_SWAMP,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"item_quest_swamp_fish_tail"},
                ["drop_from"] = {"swamp_creep_1","swamp_creep_2","swamp_creep_3","swamp_creep_4"},
                ["drop_chance"] = 35,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {8, "+5"},
                ["targets"] = {"item_quest_swamp_pearl"},
                ["drop_from"] = {"swamp_creep_1","swamp_creep_2","swamp_creep_3","swamp_creep_4"},
                ["drop_chance"] = 12,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"item_quest_swamp_hydra_head"},
                ["drop_from"] = {"npc_swamp_boss", "npc_swamp_boss_fake"},
                ["drop_chance"] = 45,
            },
        },
    },

    [RDA_QUEST_INDEX_SNOW] = { -- Синие Локация Снег
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Собрать снежинки за убийство крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_29",
            ["npc_receiver"] = "npc_29",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_SNOW,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {7, "+5"},
                ["targets"] = {"item_quest_ice_skulls"},
                ["drop_from"] = {"snow_creep_1","snow_creep_2","snow_creep_3","snow_creep_4"},
                ["drop_chance"] = 30,
            },
        },
        [2] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_29",
            ["npc_receiver"] = "npc_29",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DISTANCE_TRAVELED] = true,
                }, -- flags
                ["amount"] = {500000, "+0"},
            },
        },
    },

    [RDA_QUEST_INDEX_ANCIENT] = { -- Синие Локация Снег
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_31",
            ["npc_receiver"] = "npc_31",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_HIGH,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {6, "+0"},
                ["targets"] = bosses_names,
                ["functions"] = {
                    ["OnAcceptance"] = "OnKillingBossesInTimeAcceptance",
                },
            },
        },
    },

    [RDA_QUEST_INDEX_MAGMA] = { -- Синие Локация Снег
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
    },

    [RDA_QUEST_INDEX_MIDDLE] = { -- Мид квесты -------------------------------------------
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_RANDOM_ONE]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_ITEM_REWARD_CHOICE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_1,
            ["gold"] = REWARD_GOLD_MIDDLE_1,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_1,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = {
                [RDA_QUEST_INDEX_MIDDLE]    = {"+1"},
                [RDA_QUEST_INDEX_MIDDLE_2]  = { 1 },
            },
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_tank_cuirass",
                    ["item_level"] = 1,
                },
                [2] = {
                    ["item_name"] = "item_tank_crimson",
                    ["item_level"] = 1,
                },
                [3] = {
                    ["item_name"] = "item_tank_hell",
                    ["item_level"] = 1,
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {60, "+0"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
        },
        [2] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_ONE]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_2,
            ["gold"] = REWARD_GOLD_MIDDLE_2,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_2,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {60, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {20, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {5, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {40, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {100000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {100000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [3] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_ONE]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_3,
            ["gold"] = REWARD_GOLD_MIDDLE_3,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_3,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {70, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {25, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {10, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {45, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {200000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {200000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
            
        },
        [4] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_RANDOM_ONE]  = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_4,
            ["gold"] = REWARD_GOLD_MIDDLE_4,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_4,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {80, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {30, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {50, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {300000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {300000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [5] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_ONE]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_5,
            ["gold"] = REWARD_GOLD_MIDDLE_5,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_5,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {90, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {55, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {400000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {400000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [6] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_TWO]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_6,
            ["gold"] = REWARD_GOLD_MIDDLE_6,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_6,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {100, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {60, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {500000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {500000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [7] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_TWO]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_7,
            ["gold"] = REWARD_GOLD_MIDDLE_7,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_7,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {110, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {65, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {600000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {600000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [8] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_TWO]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_8,
            ["gold"] = REWARD_GOLD_MIDDLE_8,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_8,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {120, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {70, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {700000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {700000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [9] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_TWO]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_9,
            ["gold"] = REWARD_GOLD_MIDDLE_9,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_9,
            ["mapoverlay"] = 1,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {130, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {70, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {800000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {800000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
        [10] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_TWO]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = REWARD_EXPERIENCE_MIDDLE_10,
            ["gold"] = REWARD_GOLD_MIDDLE_10,
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_MIDDLE_10,
            ["description"] = LOCALIZATION_STRING_MID_QUEST_DESCRIPTION,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_middle_soul",
                },
                [2] = {
                    ["item_name"] = "item_remove_fog_of_war_mid",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEFEND_WAVES] = true,
                }, -- flags
                ["amount"] = {2, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEFEND_WAVES,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {140, "+30"},
                ["targets"] = {"any_creep"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_CREEP,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {35, "+10"},
                ["targets"] = {"creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_SMALL_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {15, "+5"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_BIG_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [5] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {70, "+10"},
                ["targets"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_MID_CREEP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [6] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {2, "+1"},
                ["targets"] = bosses_names,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_KILL_ANY_BOSS,
            },
            [7] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                    [RDA_QUEST_FLAG_ITEMS_DROP] = true,
                }, -- flags
                ["amount"] = {15, "+10"},
                ["targets"] = {"item_quest_mid_drop"},
                ["drop_from"] = {"comandir_creep_1", "comandir_creep_2", "comandir_creep_3", "comandir_creep_4", "comandir_creep_5", "comandir_creep_6", "comandir_creep_7", "comandir_creep_8", "comandir_creep_9", "comandir_creep_10", "creep_1", "creep_2", "creep_3", "creep_4", "creep_5", "creep_6", "creep_7", "creep_8", "creep_9", "creep_10"},
                ["drop_chance"] = 50,
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_ITEMS_DROP,
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MID,
            },
            [8] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {900000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_DEAL_DAMAGE,
            },
            [9] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {900000, "+0"},
                ["track_description"] = LOCALIZATION_STRING_MID_TASK_TAKE_DAMAGE,
            },
        },
    },

    [RDA_QUEST_INDEX_MIDDLE_2] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                }, -- flags
                ["amount"] = {70000, "+0"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_TAKE_DAMAGE] = true,
                    [RDA_QUEST_FLAG_ORIGINAL_DAMAGE] = true,
                }, -- flags
                ["amount"] = {70000, "+0"},
            },
        },
        [2] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_LEVEL_UP] = true,
                }, -- flags
                ["amount"] = {5, "+0"},
            },
        },
        [3] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_IMPROVE_ITEM_FORGE] = true,
                }, -- flags
                ["amount"] = {5, "+0"},
            },
        },
        [4] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {240, "+0"},
                ["functions"] = {
                    ["OnAcceptance"] = "OnAcceptanceMiddleLifetime",
                    ["OnEntityKilled"] = "OnHeroDiedMiddleLifetime",
                },
            },
        },
        [5] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_NEXT_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_kristal",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {1, "+1"},
                ["targets"] = {"npc_dota_custom_tower_dire_1","npc_dota_custom_tower_dire_2","npc_dota_custom_tower_dire_3","npc_dota_custom_tower_dire_4","npc_dota_custom_tower_dire_5"},
            },
        },
        [6] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_4",
            ["npc_receiver"] = "npc_4",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:High( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:High( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["mapoverlay"] = MAP_OVERLAY_LOCATION_ROSHAN,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                }, -- flags
                ["amount"] = {5, "+0"},
                ["targets"] = {"roshan_npc"},
                ["functions"] = {
                    ["OnAcceptance"] = "OnAcceptanceRoshanKill",
                }
            },
        },
    },

    [RDA_QUEST_INDEX_NPC_24] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_24",
            ["npc_receiver"] = "npc_24",
            ["experience"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_VERY_HIGH,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"raid_boss"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_BOSS_AGHANIM,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"raid_boss2"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_BOSS_WYVERN,
            },
            [3] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"raid_boss3"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_BOSS_LUFIGAN,
            },
            [4] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"raid_boss4"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_BOSS_URSA,
                ["functions"] = {
                    ["OnAcceptance"] = "OnAcceptanceRaidBoss",
                },
            },
        },
    },

    [RDA_QUEST_INDEX_DRAGON_ZONE] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = { -- Убить любых крипов
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_GAME_EVENT_UNLOCK] = true,
            }, --"locked"
            ["unlock_event_flags"] ={
                [RDA_QUEST_UNLOCK_EVENT_FLAG_INVOKER_KILLED] = true,
            },
            ["npc_giver"] = "npc_33",
            ["npc_receiver"] = "npc_33",
            ["experience"] = 100000,
            ["gold"] = 35000000,
            ["talent_experience"] = 0,
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {7000, "+0"},
                ["targets"] = {"farm_zone_dragon"},
            },
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK] = true,
        },
        [1] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_GAME_EVENT_UNLOCK] = true,
            }, --"locked"
            ["unlock_event_flags"] = {
                [RDA_QUEST_UNLOCK_EVENT_FLAG_PUDGE_KILLED] = true,
            },
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_LAST_HIT] = true,
                }, -- flags
                ["amount"] = {500, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [2] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
            }, --"locked"
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_LOW,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_DEAL_DAMAGE] = true,
                    [RDA_QUEST_FLAG_RECEIVED_DAMAGE] = true,
                    [RDA_QUEST_FLAG_DAMAGE_TARGET] = true,
                }, -- flags
                ["amount"] = {1000000, "+0"},
                ["targets"] = bosses_names,
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_KILL_UNIT_GLOBAL] = true,
                }, -- flags
                ["amount"] = {20, "+5"},
                ["targets"] = bosses_names,
            },
        },
        [3] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_RANDOM_ONE] = true,
            }, --"locked"
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["unlock"] = "+1",
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_EARNED_GOLD] = true,
                }, -- flags
                ["amount"] = {600000, "+200000"},
            },
            [2] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_SPENT_GOLD] = true,
                }, -- flags
                ["amount"] = {500000, "+200000"},
            },
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE_PLAYER_0] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_1] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_2] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_3] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_4] = true,
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE_PLAYER_1] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_0] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_2] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_3] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_4] = true,
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE_PLAYER_2] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_0] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_1] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_3] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_4] = true,
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE_PLAYER_3] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_0] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_1] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_2] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_4] = true,
        },
    },

    [RDA_QUEST_INDEX_VIP_ZONE_PLAYER_4] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_0] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_1] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_2] = true,
            [RDA_QUEST_FLAG_CAN_NOT_UNLOCK_PLAYER_3] = true,
        },
    },

    [RDA_QUEST_INDEX_LOCATION_SPAWN] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_3", -- UnitName
            ["npc_receiver"] = "npc_3", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_FOREST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_FOREST ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_1",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [2] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_8", -- UnitName
            ["npc_receiver"] = "npc_8", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_VILLAGE ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_2",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [3] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_12", -- UnitName
            ["npc_receiver"] = "npc_12", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_MINES ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_MINES ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_3",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [4] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_15", -- UnitName
            ["npc_receiver"] = "npc_15", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_DUST ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_DUST ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_4",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [5] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_23", -- UnitName
            ["npc_receiver"] = "npc_23", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_CEMETERY ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_5",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [6] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_25", -- UnitName
            ["npc_receiver"] = "npc_25", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_SWAMP ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_SWAMP ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_6",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [7] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_29", -- UnitName
            ["npc_receiver"] = "npc_29", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_SNOW ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_SNOW ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_7",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [8] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_31", -- UnitName
            ["npc_receiver"] = "npc_31", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_ANCIENT ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_8",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
        [9] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_CLOSE_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_DELETE_FROM_OTHERS_AFTER_FIRST_COMPLETION]  = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
                [RDA_QUEST_FLAG_UNLOCK_THIS_FOR_EVERYONE] = true,
            }, --"locked"
            ["npc_giver"] = "npc_34", -- UnitName
            ["npc_receiver"] = "npc_34",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:VeryLow( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:VeryLow( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = 0,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_creep_spawn_control_location_9",
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                }, -- flags
                ["amount"] = {100, "+0"},
                ["targets"] = {"any_creep"},
            },
        },
    },

    [RDA_QUEST_MAGMA_SECRET] = {
        ["flags"] = {
            [RDA_QUEST_FLAG_TYPE_BLUE]  = true,
        },
        [1] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_GAME_EVENT_UNLOCK] = true,
            }, --"locked"
            ["npc_giver"] = "npc_35", -- UnitName
            ["npc_receiver"] = "npc_35", 
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Low( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Low( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = 0,
            ["unlock"] = {
                [RDA_QUEST_MAGMA_SECRET]     = {"+1"},
            },
            ["unlock_event_flags"] = {
                [RDA_QUEST_UNLOCK_EVENT_FLAG_FIND_MAGMA_LETTER] = true,
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_FIND_ITEMS] = true,
                    [RDA_QUEST_FLAG_REMOVE_ITEMS_AFTER_COMPLETION] = true,
                }, -- flags
                ["amount"] = {1, "+0"},
                ["targets"] = {"item_quest_list_magma_drop"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MAGMA,
            },
        },
        [2] = {
            ["flags"] = {
                [RDA_QUEST_FLAG_LOCKED] = true,
                [RDA_QUEST_FLAG_ITEM_REWARD] = true,
            }, --"locked"
            ["npc_giver"] = "npc_35", -- UnitName
            ["npc_receiver"] = "npc_35",
            ["experience"] = RDA_QUEST_EXPERIENCE_REWARD:Medium( ITEM_UPGRADE_PRICE_MAGMA ),
            ["gold"] = RDA_QUEST_GOLD_REWARD:Medium( ITEM_UPGRADE_PRICE_MAGMA ),
            ["talent_experience"] = REWARD_TALENT_EXPERIENCE_BLUE_MEDIUM,
            ["items"] = {
                [1] = {
                    ["item_name"] = "item_boots_of_fire_lua",
                    ["item_level"] = 1,
                },
            },
            [1] = {
                ["flags"] = {
                    [RDA_QUEST_FLAG_KILL_UNIT] = true,
                    [RDA_QUEST_FLAG_CUSTOM] = true,
                }, -- flags
                ["amount"] = {3, "+0"},
                ["targets"] = {"npc_boss_magma_fake", "npc_boss_magma"},
                ["mapoverlay"] = MAP_OVERLAY_LOCATION_MAGMA_BOSS,
                ["functions"] = {
                    ["OnHeroDied"] = "OnHeroDiedSecretLavaBoots",
                },
            },
        },
    }
}