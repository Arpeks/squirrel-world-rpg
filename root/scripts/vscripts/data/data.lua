forest_mini = {"forest_creep_mini_1","forest_creep_mini_2","forest_creep_mini_3"}
forest_big = {"forest_creep_big_1","forest_creep_big_2","forest_creep_big_3"}

village_mini = {"village_creep_1","village_creep_2"}
village_big = {"village_creep_3"}

mines_mini = {"mines_creep_1","mines_creep_2"}
mines_big = {"mines_creep_3"}

dust_mini = {"dust_creep_1","dust_creep_3","dust_creep_3"}
dust_big = {"dust_creep_2","dust_creep_4","dust_creep_6"}

cemetery_mini = {"cemetery_creep_1","cemetery_creep_3"}
cemetery_big = {"cemetery_creep_2","cemetery_creep_4"}

swamp_mini = {"swamp_creep_1","swamp_creep_3"}
swamp_big = {"swamp_creep_2","swamp_creep_4"}

snow_mini = {"snow_creep_1","snow_creep_2"}
snow_big = {"snow_creep_3","snow_creep_4"}

last_mini = {"last_creep_1","last_creep_2"}
last_big = {"last_creep_3","last_creep_4"}

magma_mini = {"magma_creep_1"}
magma_big = {"magma_creep_2"}

scroll_item_lvl_1 = {"item_up_ability_tower"}
scroll_item_lvl_2 = {"item_up_ability_tower2"}
scroll_item_lvl_3 = {"item_up_ability_tower3"}
scroll_item_lvl_4 = {"item_up_ability_tower4"}

bosses_names = {"npc_forest_boss","npc_village_boss","npc_mines_boss","npc_dust_boss","npc_swamp_boss","npc_snow_boss","npc_forest_boss_fake","npc_village_boss_fake","npc_mines_boss_fake","npc_dust_boss_fake","npc_swamp_boss_fake","npc_snow_boss_fake","boss_1","boss_2","boss_3","boss_4","boss_5","boss_6","boss_7","boss_8","boss_9","boss_10","boss_11","boss_12","boss_13","boss_14","boss_15","boss_16","boss_17","boss_18","boss_19","boss_20", "npc_boss_location8", "npc_boss_location8_fake", "npc_invoker_boss", "npc_bara_boss_main","npc_sand_king_boss","npc_dota_monkey_king_boss","npc_titan_boss","npc_appariion_boss","npc_crystal_boss","npc_mega_boss","npc_boss_plague_squirrel","npc_cemetery_boss_fake","npc_cemetery_boss","npc_boss_magma_fake","npc_boss_magma","raid_boss","raid_boss2","raid_boss3","raid_boss4","npc_mega_boss_fake","npc_quest_cemetery_secret_boss", "npc_miniboss_darkseer", "npc_miniboss_enigma", "npc_miniboss_ursa", "npc_miniboss_void", "npc_miniboss_marci", "npc_miniboss_beast","npc_miniboss_muerta","npc_miniboss_titan","npc_miniboss_timber","npc_miniboss_nyx","npc_miniboss_lycan",}
bosses_ability = {"boss_ursa_earthshock_lua","boss_ursa_overpower_lua","ursa_enrage_lua","boss_2_spray","boss_2_heart","boss_2_spawn","boss_2_poison","boss_3_frost_blast","boss_3_chain_frost","boss_3_shards","boss_3_slow","boss_4_hole",
"boss_4_eva","boss_4_midnignt","boss_4_eidolon","boss_5_lighting","boss_5_passive","boss_5_bolt","boss_5_passive2","boss_6_torrent","boss_6_armor","boss_6_ravage","boss_6_gush","boss_7_flux","boss_7_magnetic_field","boss_7_spark_wraith","boss_7_hide",
"boss_8_chrono","boss_8_time_delay","boss_8_illusion","boss_8_lock","boss_9_pssive","boss_9_orbs","boss_9_drain","boss_9_aura","boss_10_passive","boss_10_flame","boss_10_macropyre","boss_10_chain", "ability_npc_boss_barrack1_spell1", "ability_npc_boss_barrack1_spell2", "ability_npc_boss_barrack1_spell3", "ability_npc_boss_barrack1_spell4", "ability_npc_boss_barrack2_spell1", "ability_npc_boss_barrack2_spell2", "ability_npc_boss_barrack2_spell3", "ability_npc_boss_barrack2_spell4","npc_byorrocktar_spell1","npc_byorrocktar_spell2","npc_byorrocktar_spell3","npc_byorrocktar_spell4","npc_byorrocktar_spell5","zuus_boss_circle_lua","zuus_boss_nimbus", "hero_destroyer_first_skill_totem", "hero_destroyer_second_skill", "hero_destroyer_third_skill", "hero_destroyer_ult", "hero_destroyer_fourth_skill", "hero_destroyer_fifth_skill",
"earthshaker_fissure_lua", "earthshaker_enchant_totem_lua", "earthshaker_aftershock_lua", "earthshaker_echo_slam_lua"}

boss_belka = {"ability_npc_boss_plague_squirrel_spell1", "ability_npc_boss_plague_squirrel_spell2","ability_npc_boss_plague_squirrel_spell3","ability_npc_boss_plague_squirrel_spell4","ability_npc_boss_plague_squirrel_spell5", "ability_npc_boss_plague_squirrel_spell6", "ability_npc_boss_plague_squirrel_spell7", "ability_npc_boss_plague_squirrel_spell8"}

creep_ability = {"polar_furbolg_ursa_warrior_thunder_clap_lane_creep","ogreseal_flop_by","centaur_hoof_stomp_lane_creep","lycan_summon_wolves_critical_strike_lane_creep","skeleton_king_reincarnation_lane_creep","crystal_maiden_crystal_nova_lane_creep","venomancer_venomous_gale_lane_creep","bristleback_quill_spray_lane_creep","elder_titan_natural_order_lane_creep","mars_bulwark_lane_creep"}

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- item_shivas_guard_lua
-- avaliable_creeps_items = {"item_radiance_lua","item_hurricane_pike_lua","item_sabre_blade","item_bloodstone_lua","item_magic_crit_lua"}
avaliable_creeps_items = {"item_desolator_lua","item_skadi_lua","item_mjollnir_lua","item_veil_of_discord_lua","item_monkey_king_bar_lua","item_bfury_lua","item_greater_crit_lua","item_kaya_lua","item_ethereal_blade_lua","item_pipe_lua"}

-- item_shivas_guard_lua
items_level_inv = {"item_desolator_lua","item_skadi_lua","item_mjollnir_lua","item_veil_of_discord_lua","item_monkey_king_bar_lua","item_kaya_lua","item_ethereal_blade_lua","item_pipe_lua"}
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Ability_tower_passive_lvl_1 = {"nevermore_dark_lord","vengefulspirit_command_aura","sniper_headshot","venomancer_poison_sting","abyssal_underlord_atrophy_aura","shredder_reactive_armor","meepo_ransack","meepo_geostrike","tidehunter_kraken_shell","faceless_void_backtrack"}

Ability_tower_passive_lvl_2 = {"viper_corrosive_skin","beastmaster_inner_beast","huskar_berserkers_blood","weaver_geminate_attack","warlock_golem_flaming_fists","abaddon_frostmourne",
"legion_commander_moment_of_courage","brewmaster_fire_permanent_immolation","chaos_knight_chaos_strike","tiny_grow","beastmaster_boar_poison","juggernaut_blade_dance","axe_counter_helix"}

Ability_tower_passive_lvl_3 = {"spectre_desolate","troll_warlord_fervor","lycan_feral_impulse","warlock_golem_permanent_immolation","ursa_fury_swipes","tiny_craggy_exterior","broodmother_incapacitating_bite","antimage_mana_break"}

Ability_tower_passive_lvl_4 = {"phantom_assassin_coup_de_grace","elder_titan_natural_order","sven_great_cleave","luna_moon_glaive","life_stealer_feast","templar_assassin_psi_blades","enchantress_untouchable","sandking_caustic_finale"}

AutoCastItem = {"item_black_king_bar","item_lotus_orb","item_ethereal_blade_lua"}

abiility_passive = {"huskar_berserkers_blood","shredder_reactive_armor"}

Ability_active_final = {"mars_arena_of_blood","custom_forest","tusk_snowball_meteor","custom_earth_splitter","earth_spirit_magnetize_datadriven","wisp_spirits_datadriven","custom_nyx_skill","sand_king_burrowstrike_lua","lich_sinister_gaze","custom_solar_flare","night_stalker_crippling_fear","creature_fire_breath"}


point_forest = {
	[1] = {-2674, 3032, 304},
	[2] = {-3791, 2546, 416},
	[3] = {-2752, 4800, 288},
	[4] = {-4972, 2769, 544},
	[5] = {-4832, 4627, 416},
	[6] = {-4194, 5909, 544},
	[7] = {-4632, 7252, 544},
	[8] = {-6222, 3065, 304},
	[9] = {-5893, 4548, 544},
	[10] = {-5888, 6034, 416},
	[11] = {-6333, 7165, 544},
	[12] = {-6831, 5676, 544}
}

point_village = {
	[1] = {-4152, -2796, 544},
	[2] = {-4455, -1744, 544},
	[3] = {-5526, -1693, 544},
	[4] = {-3630, -907, 416},
	[5] = {-4576, -96, 544}
}

point_mines = {
	[1] = {-10080, 1568, 416},
	[2] = {-11168, 576, 416},
	[3] = {-10080, 32, 416},
	[4] = {-9952, -1280, 416},
	[5] = {-8768, -928, 544},
	[6] = {-10784, -2304, 416},
	[7] = {-9472, -2656, 416},
}

point_dust = {
	[1] = {896, 3712, 544},
	[2] = {2624, 3840, 544},
	[3] = {4480, 4160, 416},
	[4] = {2375, 5785, 416},
	[5] = {1783, 4916, 544},
	[6] = {3613, 5658, 416},
}

point_cemetery = {
	[1] = {7552, 7584, 416},
	[2] = {7919, 8457, 416},
	[3] = {8983, 8380, 544},
	[4] = {9360, 9325, 544},
	[5] = {10146, 9689, 544},
	[6] = {9348, 10825, 416},
}

point_swamp = {
	[1] = {7552, 2496, 288},
	[2] = {9312, 2720, 288},
	[3] = {8352, 4032, 288},
	[4] = {9472, 5152, 288},
	[5] = {10144, 3872, 288},
	[6] = {11200, 2880, 288},
}

point_snow = {
	[1] = {8928, -320, 544},
	[2] = {11136, 128, 544},
	[3] = {10752, -928, 544},
	[4] = {10624, -1952, 416},
	[5] = {9920, -2112, 544},
	[6] = {9248, -1312, 288},
}

point_last = {
	[1] = {5856, -3552, 448},
	[2] = {4416, -3296, 448},
	[3] = {5088, -1536, 448},
	[4] = {3104, -2624, 448},
	[5] = {3136, -1376, 448},
	[6] = {4288, -32, 448},
}

point_magma = {
	[1] = {9920, -4352, 672},
	[2] = {9696, -5664, 544},
	[3] = {7935, -4707, 448},
	[4] = {6944, -5504, 448},
	[5] = {8000, -6464, 448},
	[6] = {9120, -7744, 448},
}

PatroolWave = {
	[0] = {"npc_patrool1_zoomby","npc_patrool1_zoomby","npc_patrool1_zoomby","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt",},
	[1] = {"npc_patrool1_zoomby","npc_patrool1_zoomby","npc_patrool1_zoomby","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt","npc_patrool1_zoomby_alt",},
	[2] = {"npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt",},
	[3] = {"npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt","npc_patrool2_zoomby_alt",},
	[4] = {"npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon",},
	[5] = {"npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_siege","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon","npc_patrool3_dragon",},
	[6] = {"npc_patrool4_zoomby","npc_patrool4_zoomby","npc_patrool4_zoomby",},
	[7] = {"npc_patrool4_zoomby","npc_patrool4_zoomby","npc_patrool4_zoomby",},
	[8] = {"npc_patrool5_necro","npc_patrool5_summons", "npc_patrool5_summons"},
	[9] = {"npc_patrool5_necro","npc_patrool5_summons", "npc_patrool5_summons"},
	[10] = {"npc_patrool5_necro","npc_patrool5_summons", "npc_patrool5_summons"},
}

PatroolPoints = {point_forest,point_village,point_mines,point_dust,point_cemetery,point_swamp,point_snow,point_last,point_magma}
battlePassTasks = {
	[1] = { event = "kill", target = {"any"}, count = 10000, count_after_victory = true},
	[2] = { event = "assistance", target = bosses_names, count = 250, count_after_victory = true},
}
dailyTasks = {
	[1] = { event = "kill", target = {"npc_forest_boss", "npc_forest_boss_fake"}, count = 25, count_after_victory = true},
	[2] = { event = "kill", target = {"npc_village_boss", "npc_village_boss_fake"}, count = 20, count_after_victory = true},
	[3] = { event = "kill", target = {"npc_mines_boss", "npc_mines_boss_fake"}, count = 20, count_after_victory = true},
	[4] = { event = "kill", target = {"npc_dust_boss", "npc_dust_boss_fake"}, count = 15, count_after_victory = true},
	[5] = { event = "kill", target = {"npc_swamp_boss", "npc_swamp_boss_fake"}, count = 15, count_after_victory = true},
	[6] = { event = "kill", target = {"npc_snow_boss", "npc_snow_boss_fake"}, count = 10, count_after_victory = true},
	[7] = { event = "assistance", target = {"raid_boss"}, count = 1},
	[8] = { event = "assistance", target = {"raid_boss2"}, count = 1},
	[9] = { event = "assistance", target = {"raid_boss3"}, count = 1},
	[10] = { event = "assistance", target = {"raid_boss4"}, count = 1},
	[11] = { event = "kill", target = {"any"}, count = 2000, count_after_victory = true},
	[12] = { event = "kill", target = {"any"}, count = 4000, count_after_victory = true},
	[13] = { event = "kill", target = {"any"}, count = 6000, count_after_victory = true},
	[14] = { event = "kill", target = {"forest_creep_big_1","forest_creep_mini_1","forest_creep_mini_2","forest_creep_big_2","forest_creep_big_3","forest_creep_mini_3"}, count = 500},
	[15] = { event = "kill", target = {"village_creep_1","village_creep_2","village_creep_3"}, count = 500},
	[16] = { event = "kill", target = {"mines_creep_1","mines_creep_2","mines_creep_3"}, count = 500},
	[17] = { event = "kill", target = {"dust_creep_1","dust_creep_2","dust_creep_3","dust_creep_4","dust_creep_5","dust_creep_6"}, count = 500},
	[18] = { event = "kill", target = {"cemetery_creep_1","cemetery_creep_2","cemetery_creep_3","cemetery_creep_4"}, count = 500},
	[19] = { event = "kill", target = {"swamp_creep_1","swamp_creep_2","swamp_creep_3","swamp_creep_4"}, count = 500},
	[20] = { event = "kill", target = {"snow_creep_1","snow_creep_2","snow_creep_3","snow_creep_4"}, count = 500},
	[21] = { event = "kill", target = {"last_creep_1","last_creep_2","last_creep_3","last_creep_4"}, count = 500},
	[22] = { event = "kill", target = {"farm_zone_dragon"}, count = 1000, count_after_victory = true},
	[23] = { event = "kill", target = bosses_names, count = 20, dont_save_unfinished_progress = true},
	[24] = { event = "kill", target = bosses_names, count = 30, dont_save_unfinished_progress = true},
	[25] = { event = "kill", target = {"roshan_npc"}, count = 3},
	[26] = { event = "assistance", target = {"npc_bara_boss_main"}, count = 1, count_after_victory = true},
	[27] = { event = "assistance", target = {"npc_mega_boss"}, count = 1, count_after_victory = true},
	[28] = { event = "", count = 1, count_after_victory = true},
	[29] = { event = "", count = 1},
	[30] = { event = "", count = 10, count_after_victory = true},
	[31] = { event = "", count = 1, count_after_victory = true},
	[32] = { event = "", count = 1, count_after_victory = true},
	[33] = { event = "", count = 1, count_after_victory = true},
	[34] = { event = "", count = 70, count_after_victory = true},
	[35] = { event = "", count = 100},
	[36] = { event = "assistance", target = {"npc_sand_king_boss"}, count = 1, count_after_victory = true},
	[37] = { event = "assistance", target = {"npc_dota_monkey_king_boss"}, count = 1, count_after_victory = true},
	[38] = { event = "assistance", target = {"npc_titan_boss"}, count = 1, count_after_victory = true},
	[39] = { event = "assistance", target = {"npc_appariion_boss"}, count = 1, count_after_victory = true},
	[40] = { event = "assistance", target = {"npc_crystal_boss"}, count = 1, count_after_victory = true},
	[41] = { event = "", count = 25},
	[42] = { event = "", count = 1},
	[43] = { event = "", count = 1},
	[44] = { event = "", count = 1},
	[45] = { event = "", count = 10, count_after_victory = true},
	[46] = { event = "", count = 20, count_after_victory = true},
	[47] = { event = "kill", target = {"badguys_creeps", "badguys_comandirs", "badguys_boss"}, count = 1, count_after_victory = true},
	[48] = { event = "kill", target = {"npc_dota_custom_tower_dire_1", "npc_dota_custom_tower_dire_2", "npc_dota_custom_tower_dire_3", "npc_dota_custom_tower_dire_4", "npc_dota_custom_tower_dire_5"}, count = 1, count_after_victory = true},
	[49] = { event = "", count = 25},
	[50] = { event = "", count = 5},
	[51] = { event = "assistance", target = {"npc_boss_plague_squirrel"}, count = 1, count_after_victory = true},
}
_G.Miniboses = {
	"npc_miniboss_darkseer", 
	"npc_miniboss_enigma", 
	"npc_miniboss_ursa", 
	"npc_miniboss_void", 
	"npc_miniboss_marci", 
	"npc_miniboss_beast",
	"npc_miniboss_muerta",
	"npc_miniboss_titan",
	"npc_miniboss_timber",
	"npc_miniboss_nyx",
	"npc_miniboss_lycan",
}

Ability_Impossible = {
	["alchemist_acid_spray_lua"] = {"npc_dota_hero_alchemist_str8", "npc_dota_hero_alchemist_str11", "npc_dota_hero_alchemist_str_last"},
	["alchemist_chemical_rage_lua"] = {"npc_dota_hero_alchemist_str6", "npc_dota_hero_alchemist_str9", "npc_dota_hero_alchemist_str10","npc_dota_hero_alchemist_agi11"},
	["axe_berserkers_call_lua"] = {"special_bonus_unique_npc_dota_hero_axe_agi50"},
	["axe_blood_lua"] = {"npc_dota_hero_axe_str7"},
	["bristleback_bristleback_lua"] = {"npc_dota_hero_bristleback_str10"},
	["dragon_fire_ball_lua"] = {"npc_dota_hero_dragon_knight_str7","npc_dota_hero_dragon_knight_int_last"},
	["dragon_form_lua"] = {"special_bonus_unique_npc_dota_hero_dragon_knight_agi50"},
	["mars_bulwark_lua"] = {"npc_dota_hero_mars_str9","npc_dota_hero_mars_str10","npc_dota_hero_mars_str_last","special_bonus_unique_npc_dota_hero_mars_str50"},
	["mars_lil"] = {"npc_dota_hero_mars_agi11","npc_dota_hero_mars_agi7","npc_dota_hero_mars_agi_last"},
	["legion_courage"] = {"special_bonus_unique_npc_dota_hero_legion_commander_agi50","npc_dota_hero_legion_commander_agi_last","npc_dota_hero_legion_commander_agi10"},
	["pudge_meat_hook_lua"] = {"npc_dota_hero_pudge_agi10"},
	["sven_bringer"] = {"special_bonus_unique_npc_dota_hero_sven_agi50"},
	["sven_gods_strength_lua"] = {"npc_dota_hero_sven_int6","npc_dota_hero_sven_int9","npc_dota_hero_sven_int_last","special_bonus_unique_npc_dota_hero_sven_int50","npc_dota_hero_sven_str10"},
	["arc_warden_magnetic_field_lua"] = {"npc_dota_hero_arc_warden_agi8","npc_dota_hero_arc_warden_str8","special_bonus_unique_npc_dota_hero_arc_warden_str50"},
	["arc_geminate_attack"] = {"npc_dota_hero_arc_warden_agi_last","npc_dota_hero_arc_warden_agi9","npc_dota_hero_arc_warden_str9"},
	["bloodseeker_bloodrage_lua"] = {"npc_dota_hero_bloodseeker_str11","npc_dota_hero_bloodseeker_str8"},
	["juggernaut_blade_dance_lua"] = {"npc_dota_hero_juggernaut_agi11","npc_dota_hero_juggernaut_str9"},
	["juggernaut_fervor_lua"] = {"npc_dota_hero_juggernaut_agi_last","npc_dota_hero_juggernaut_agi9","npc_dota_hero_juggernaut_str8","npc_dota_hero_juggernaut_str11"},
	["juggernaut_omni_slash_lua"] = {"npc_dota_hero_juggernaut_str10","npc_dota_hero_juggernaut_str_last","special_bonus_unique_npc_dota_hero_juggernaut_str50","npc_dota_hero_juggernaut_int8"},
	["luna_starfall_lua"] = {"npc_dota_hero_luna_int7","npc_dota_hero_luna_int_last"},
	["luna_moon"] = {"npc_dota_hero_luna_int9","npc_dota_hero_luna_int10","npc_dota_hero_luna_int11","special_bonus_unique_npc_dota_hero_luna_str50"},
	["phantom_assassin_blur_lua"] = {"npc_dota_hero_phantom_assassin_str11","npc_dota_hero_phantom_assassin_str_last"},
	["phantom_assassin_phantom_strike_lua"] = {"npc_dota_hero_phantom_assassin_str6"},
	["phantom_assassin_coup_de_grace_lua"] = {"npc_dota_hero_phantom_assassin_agi9","npc_dota_hero_phantom_assassin_agi10","npc_dota_hero_phantom_assassin_agi_last"},
	["razor_eye_of_the_storm_lua"] = {"npc_dota_hero_razor_str7","npc_dota_hero_razor_str_last","special_bonus_unique_npc_dota_hero_razor_str50"},
	["riki_smoke_screen_lua"] = {"npc_dota_hero_riki_agi10","npc_dota_hero_riki_str11"},
	["nevermore_aura"] = {"npc_dota_hero_nevermore_agi6"},
	-- ["slark_dark_pact_lua"] = {"npc_dota_hero_slark_str10"},
	["slark_shadow_dance_lua"] = {"special_bonus_unique_npc_dota_hero_slark_str50"},
	["sniper_headshot_lua"] = {"npc_dota_hero_sniper_agi9","npc_dota_hero_sniper_agi8"},
	["sniper_ult"] = {"npc_dota_hero_sniper_agi_last","npc_dota_hero_sniper_agi11"},
	["spectre_desolate_lua"] = {"npc_dota_hero_spectre_agi8","npc_dota_hero_spectre_agi10","npc_dota_hero_spectre_agi11","npc_dota_hero_spectre_agi_last","special_bonus_unique_npc_dota_hero_spectre_agi50"},
	-- ["spectre_dispersion_lua"] = {"npc_dota_hero_spectre_str7","npc_dota_hero_spectre_str_last"},
	["terrorblade_metamorphosis_lua"] = {"npc_dota_hero_terrorblade_str7","npc_dota_hero_terrorblade_agi7","npc_dota_hero_terrorblade_agi10","npc_dota_hero_terrorblade_int_last"},
	["ursa_overpower_lua"] = {"npc_dota_hero_ursa_agi8","npc_dota_hero_ursa_agi11","npc_dota_hero_ursa_int7","npc_dota_hero_ursa_int10"},
	["ursa_fury_swipes_lua"] = {"npc_dota_hero_ursa_str9","npc_dota_hero_ursa_str13","npc_dota_hero_ursa_agi6","npc_dota_hero_ursa_agi7","npc_dota_hero_ursa_agi10"},
	["ursa_enrage_lua"] = {"npc_dota_hero_ursa_str11","npc_dota_hero_ursa_agi9"},
	["viper_poison_attack_lua"] = {"npc_dota_hero_viper_str10","npc_dota_hero_viper_agi6","npc_dota_hero_viper_int13"},
	["viper_nethertoxin_lua"] = {"npc_dota_hero_viper_str9","npc_dota_hero_viper_agi7","npc_dota_hero_viper_agi10","npc_dota_hero_viper_agi13"},
	["viper_corrosive_skin_lua"] = {"npc_dota_hero_viper_str7","npc_dota_hero_viper_str8","npc_dota_hero_viper_str12","npc_dota_hero_viper_str13","npc_dota_hero_viper_agi8","npc_dota_hero_viper_agi11"},
	["viper_viper_strike_lua"] = {"npc_dota_hero_viper_str6","npc_dota_hero_viper_int11"},
	["geminate_attack_lua"] = {"npc_dota_hero_weaver_agi7","npc_dota_hero_weaver_agi8","npc_dota_hero_weaver_agi12","npc_dota_hero_weaver_agi13"},
	["ancient_apparition_cold_feet_lua"] = {"npc_dota_hero_ancient_apparition_str10","npc_dota_hero_ancient_apparition_str11","npc_dota_hero_ancient_apparition_agi8","npc_dota_hero_ancient_apparition_agi9"},
	["ancient_apparition_ice_vortex_lua"] = {"npc_dota_hero_ancient_apparition_str6","npc_dota_hero_ancient_apparition_int8","npc_dota_hero_ancient_apparition_agi7"},
	["ancient_apparition_ice_blast_lua"] = {"npc_dota_hero_ancient_apparition_int_last"},
	["death_prophet_crypt_swarm_bh"] = {"npc_dota_hero_death_prophet_str6", "npc_dota_hero_death_prophet_str13", "npc_dota_hero_death_prophet_agi10", "npc_dota_hero_death_prophet_int11"},
	["death_prophet_ghastly_haunting"] = {},
	-- ["death_prophet_exorcism_bh"] = {"npc_dota_hero_death_prophet_str12","npc_dota_hero_death_prophet_agi9","npc_dota_hero_death_prophet_agi12","npc_dota_hero_death_prophet_agi13"},
	["enchantress_natures_attendants_lua"] = {"npc_dota_hero_enchantress_str11"},
	["enchantress_natures"] = {"npc_dota_hero_enchantress_str10", "npc_dota_hero_enchantress_int11"},
	["enchantress_impetus_lua"] = {"special_bonus_unique_npc_dota_hero_enchantress_int50","npc_dota_hero_enchantress_agi11"},
	["jakiro_dual_breath_lua"] = {"npc_dota_hero_jakiro_str6","npc_dota_hero_jakiro_str9","npc_dota_hero_jakiro_agi10","npc_dota_hero_jakiro_int10","npc_dota_hero_jakiro_int12"},
	["jakiro_ice_path_lua"] = {"npc_dota_hero_jakiro_str11"},
	["jakiro_liquid_fire_lua"] = {"npc_dota_hero_jakiro_agi6","npc_dota_hero_jakiro_agi12","npc_dota_hero_jakiro_int9"},
	["leshrac_diabolic_edict_lua"] = {"npc_dota_hero_leshrac_agi8","npc_dota_hero_leshrac_agi11","npc_dota_hero_leshrac_agi12","npc_dota_hero_leshrac_agi13"},
	["leshrac_pulse_nova_lua"] = {"npc_dota_hero_leshrac_str7","npc_dota_hero_leshrac_str10","npc_dota_hero_leshrac_agi6","npc_dota_hero_leshrac_int12","npc_dota_hero_leshrac_int13"},
	["leshrac_split_earth_lua"] = {"npc_dota_hero_leshrac_str6","npc_dota_hero_leshrac_str9","npc_dota_hero_leshrac_agi7","npc_dota_hero_leshrac_agi10"},
	["lina_stun"] = {"npc_dota_hero_lina_str8","npc_dota_hero_lina_int6"},
	["lina_laguna_blade_lua"] = {"npc_dota_hero_lina_str11"},
	["lion_earth_spike_lua"] = {"npc_dota_hero_lion_agi_last","npc_dota_hero_lion_int6"},
	["lion_finger_of_death_lua"] = {"npc_dota_hero_lion_str6","npc_dota_hero_lion_str_last", "special_bonus_unique_npc_dota_hero_lion_int50"},
	["necrolyte_ghost_shroud_lua"] = {"npc_dota_hero_necrolyte_str8","npc_dota_hero_necrolyte_str12", "npc_dota_hero_necrolyte_agi8","npc_dota_hero_necrolyte_agi11","npc_dota_hero_necrolyte_int11"},
	["necrolyte_reapers_scythe_lua"] = {"npc_dota_hero_necrolyte_agi12","npc_dota_hero_necrolyte_agi13", "npc_dota_hero_necrolyte_int12","npc_dota_hero_necrolyte_int13"},
	-- ["shaman_hex"] = {"npc_dota_hero_shadow_shaman_int9","special_bonus_unique_npc_dota_hero_shadow_shaman_int50", "special_bonus_unique_npc_dota_hero_shadow_shaman_str50","npc_dota_hero_shadow_shaman_str7"},
	["silencer_global_silence_lua"] = {},
	["skywrath_mage_ancient_seal_lua"] = {"npc_dota_hero_skywrath_mage_str10","npc_dota_hero_skywrath_mage_agi6","npc_dota_hero_skywrath_mage_int7"},
	["dazzle_poison_touch_lua"] = {"npc_dota_hero_dazzle_str11","npc_dota_hero_dazzle_str_last","special_bonus_unique_npc_dota_hero_dazzle_str50","npc_dota_hero_dazzle_agi9","special_bonus_unique_npc_dota_hero_dazzle_int50"},
	["dazzle_custom_badjuju"] = {"npc_dota_hero_dazzle_int_last"},
	["dazzle_shallow_grave_lua"] = {"npc_dota_hero_dazzle_str7"},
	["magnataur_empower_lua"] = {"npc_dota_hero_magnataur_str7","npc_dota_hero_magnataur_str8","npc_dota_hero_magnataur_str9","npc_dota_hero_magnataur_agi10","npc_dota_hero_magnataur_agi_last","special_bonus_unique_npc_dota_hero_magnataur_agi50"},
	["magnataur_reverse_polarity_lua"] = {"npc_dota_hero_magnataur_str_last","npc_dota_hero_magnataur_agi7","npc_dota_hero_magnataur_agi6","npc_dota_hero_magnataur_int8"},
	["sand_stun"] = {"npc_dota_hero_sand_king_int7","npc_dota_hero_sand_king_int10","special_bonus_unique_npc_dota_hero_sand_king_str50"},
	["techies_land_mines_lua"] = {"npc_dota_hero_techies_str8","npc_dota_hero_techies_str11","npc_dota_hero_techies_str_last","special_bonus_unique_npc_dota_hero_techies_str50","npc_dota_hero_techies_agi7","npc_dota_hero_techies_agi11","special_bonus_unique_npc_dota_hero_techies_agi50"},
	["techies_stasis_trap_lua"] = {"npc_dota_hero_techies_str7","npc_dota_hero_techies_str10"},
	["vengeful_spirit_magic_missile"] = {"npc_dota_hero_vengefulspirit_agi10","npc_dota_hero_vengefulspirit_int11","npc_dota_hero_vengefulspirit_int12","npc_dota_hero_vengefulspirit_int13"},
	["vengeful_spirit_command_aura"] = {"npc_dota_hero_vengefulspirit_str10","npc_dota_hero_vengefulspirit_agi13"},
} 
-- for key,_ in pairs(Ability_Impossible) do
-- 	table.insert(bosses_ability, key)
-- 	table.insert(creep_ability, key)
-- end
function GetRandomAbilities()
	local abilities = {}
	while table.count(abilities) < 6 do
		local talents, ability = table.random(Ability_Impossible)
		if abilities[ability] == nil then
			abilities[ability] = talents
		end
	end
	return abilities
end

Ability_Impossible_Settings = {
	------ forest -------------------------------------------------------------------------
	{ ["creeps"] = forest_mini, 	["abilityes"] = 0, 	["level"] = 3, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 1 },
	{ ["creeps"] = forest_big, 		["abilityes"] = 0, 	["level"] = 4, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 1 },
	{ ["creeps"] = {
		"npc_forest_boss", "npc_forest_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 5, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	------ village ------------------------------------------------------------------------
	{ ["creeps"] = village_mini, 	["abilityes"] = 0, 	["level"] = 6, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 2 },
	{ ["creeps"] = forest_big, 		["abilityes"] = 0, 	["level"] = 9, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 2 },
	{ ["creeps"] = {
		"npc_village_boss", "npc_village_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 8, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- mines ----------------------------------------------------------------------
	{ ["creeps"] = mines_mini, 		["abilityes"] = 0, 	["level"] = 7, 	["talents"] = 0, ["items"] = 0, ["items_level"] = 3 },
	{ ["creeps"] = mines_big, 		["abilityes"] = 0, 	["level"] = 10, ["talents"] = 0, ["items"] = 0, ["items_level"] = 3 },
	{ ["creeps"] = {
		"npc_mines_boss", "npc_mines_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- dust ----------------------------------------------------------------------
	{ ["creeps"] = dust_mini, 		["abilityes"] = 0, 	["level"] = 10, ["talents"] = 0, ["items"] = 0, ["items_level"] = 4 },
	{ ["creeps"] = dust_big, 		["abilityes"] = 0, 	["level"] = 13, ["talents"] = 0, ["items"] = 0, ["items_level"] = 4 },
	{ ["creeps"] = {
		"npc_dust_boss", "npc_dust_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- cemetery ----------------------------------------------------------------------
	{ ["creeps"] = cemetery_mini, 	["abilityes"] = 0, 	["level"] = 13, ["talents"] = 0, ["items"] = 0, ["items_level"] = 5 },
	{ ["creeps"] = cemetery_big, 	["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 5 },
	{ ["creeps"] = {
		"npc_cemetery_boss", "npc_cemetery_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- swamp ----------------------------------------------------------------------
	{ ["creeps"] = swamp_mini, 		["abilityes"] = 0, 	["level"] = 13, ["talents"] = 0, ["items"] = 0, ["items_level"] = 6 },
	{ ["creeps"] = swamp_big, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 6 },
	{ ["creeps"] = {
		"npc_swamp_boss", "npc_swamp_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- snow ----------------------------------------------------------------------
	{ ["creeps"] = snow_mini, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 7 },
	{ ["creeps"] = snow_big, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 7 },
	{ ["creeps"] = {
		"npc_snow_boss", "npc_snow_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- last ----------------------------------------------------------------------
	{ ["creeps"] = last_mini, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 8 },
	{ ["creeps"] = last_big, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 8 },
	{ ["creeps"] = {
		"npc_boss_location8", "npc_boss_location8_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
	-------- magma ----------------------------------------------------------------------
	{ ["creeps"] = magma_mini, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 9 },
	{ ["creeps"] = magma_big, 		["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 9 },
	{ ["creeps"] = {
		"npc_mega_boss", "npc_mega_boss_fake"
	}, 								["abilityes"] = 0, 	["level"] = 15, ["talents"] = 0, ["items"] = 0, ["items_level"] = 0 },
}


















creep_name_to_ability_list = {
	["forest_creep_big_2"] = {"alpha_wolf_critical_strike", "alpha_wolf_command_aura"},
	["village_creep_1"] = {"troll_warlord_fervor"},
	["swamp_creep_3"] = {"troll_warlord_fervor","elder_titan_natural_order"},
	["mines_creep_1"] = {"venomancer_poison_sting"},
	["mines_creep_2"] = {"meepo_ransack"},
	["dust_creep_2"] = {"huskar_berserkers_blood"},
	["cemetery_creep_3"] = {"skeleton_king_vampiric_aura","huskar_berserkers_blood"},
	["snow_creep_4"] = {"huskar_berserkers_blood"},
	["swamp_creep_2"] = {"elder_titan_natural_order","huskar_berserkers_blood"},
	["dust_creep_3"] = {"big_thunder_lizard_wardrums_aura"},
	["dust_creep_4"] = {"faceless_void_time_lock"},
	["dust_creep_6"] = {"sven_great_cleave"},
	["cemetery_creep_1"] = {"skeleton_king_mortal_strike"},
	["cemetery_creep_2"] = {"warlock_golem_permanent_immolation"},
	["cemetery_creep_4"] = {"necrolyte_heartstopper_aura"},
	["comandir_creep_9"] = {"elder_titan_natural_order_lane_creep"},
	["swamp_creep_1"] = {"elder_titan_natural_order"},
	["swamp_creep_4"] = {"elder_titan_natural_order"},
	["snow_creep_2"] = {"spirit_breaker_greater_bash"},
	["magma_creep_1"] = {"ability_magma_creep_1"},
	["dust_creep_1"] = {"lycan_summon_wolves_critical_strike_lane_creep"},
	["comandir_creep_5"] = {"skeleton_king_reincarnation_lane_creep"},
	["comandir_creep_10"] = {"mars_bulwark_lane_creep"},
	["comandir_creep_4"] = {"lycan_summon_wolves_critical_strike_lane_creep"},
}